namespace NopTalkCore
{
	public enum EditorQueryType
	{
		ProductTableSchema,
		CategoryTableSchema,
		ManufacturerTableSchema
	}
}
