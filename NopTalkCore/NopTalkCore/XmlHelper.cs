using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Text;

namespace NopTalkCore
{
    public class XmlHelper
    {
        public static Dictionary<string, string> GetXmlElements(string fileContentString)
        {
            XmlTextReader xmlTextReader = new XmlTextReader(new StringReader(fileContentString));
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(fileContentString);
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            do
            {
                XmlNodeType nodeType = xmlTextReader.NodeType;
                XmlNodeType xmlNodeType = nodeType;
                if (xmlNodeType != XmlNodeType.Element)
                {
                    continue;
                }
                string name = xmlTextReader.Name;
                if (!dictionary.ContainsKey(name))
                {
                    string text = xmlDocument.SelectSingleNode("//" + name)?.InnerText;
                    string value = (text != null && text.Length > 80) ? (text.Substring(0, 80) + "...") : text;
                    dictionary.Add(name, value);
                }
                if (!xmlTextReader.HasAttributes)
                {
                    continue;
                }
                for (int i = 0; i < xmlTextReader.AttributeCount; i++)
                {
                    xmlTextReader.MoveToAttribute(i);
                    string key = name + "/@" + xmlTextReader.Name;
                    if (!dictionary.ContainsKey(key))
                    {
                        dictionary.Add(key, xmlTextReader.Value);
                    }
                }
                xmlTextReader.MoveToElement();
            }
            while (xmlTextReader.Read());
            return dictionary;
        }



        public static Dictionary<string, string> GetXmlElementsModiftyTicimax(string fileContentString)
        {
           
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(fileContentString);

            xmlDocument.DocumentElement.SetAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");

            //Resimler Attibute Ekleniyor
            XmlNodeList _variantsNodeList = xmlDocument.SelectNodes("//Urunler//Urun");
            foreach (XmlNode node in _variantsNodeList)
            {             
                List<string> imageRevize = new List<string>();
                if (node.SelectNodes("Resimler") != null)
                {
                    foreach (XmlNode imageList in node.SelectNodes("Resimler"))
                    {                        
                        if (imageList.SelectSingleNode("Resim") != null)
                        {
                            int say = 0;
                            foreach (XmlNode imagesss in imageList.ChildNodes)
                            {
                                XmlAttribute attr = imagesss.Attributes.Append(xmlDocument.CreateAttribute(string.Concat("Resim", say++)));
                                attr.Value = imagesss.InnerText;
                            }
                        }
                    }
                }
            }

            //Bedenlere Attibute Ekleniyor
            XmlNodeList _variantsNodeListAttribute = xmlDocument.SelectNodes("//Urunler//Urun//Stoklar");
            foreach (XmlNode node in _variantsNodeListAttribute)
            {
                List<string> imageRevize = new List<string>();
                if (node.SelectNodes("Stok") != null)
                {
                    foreach (XmlNode imageList in node.SelectNodes("Stok"))
                    {
                        if (imageList.SelectSingleNode("Ozellik") != null)
                        {
                            int varyantsay = 0;
                            foreach (XmlNode varyants in imageList.SelectNodes("Ozellik"))
                            {
                                XmlAttribute attr = varyants.Attributes.Append(xmlDocument.CreateAttribute(string.Concat(varyants.Attributes.GetNamedItem("isim").Value)));
                                attr.Value = varyants.InnerText;

                                XmlAttribute attr2 = varyants.Attributes.Append(xmlDocument.CreateAttribute(string.Concat("varyant", varyantsay++)));
                                attr2.Value = string.Concat(varyants.Attributes.GetNamedItem("isim").Value);

                            }
                        }
                    }
                }
            }

            //Bedenlere Attibute Ekleniyor
            XmlNodeList _variantsNodeListStokkodu = xmlDocument.SelectNodes("//Urunler//Urun//Stoklar");
            foreach (XmlNode node in _variantsNodeListStokkodu)
            {
                List<string> imageRevize = new List<string>();
                if (node.SelectNodes("Stok") != null)
                {
                    foreach (XmlNode imageListstoks in node.SelectNodes("Stok"))
                    {
                        if (imageListstoks.SelectSingleNode("StokKodu") != null)
                        {
                            int varyantsay = 0;
                            foreach (XmlNode varyants in imageListstoks.SelectNodes("StokKodu"))
                            {
                                XmlAttribute attr = varyants.Attributes.Append(xmlDocument.CreateAttribute(string.Concat("stokodu", varyantsay++)));
                                attr.Value = varyants.InnerText.Split('-').First();

                            }
                        }
                    }
                }
            }


            

            string lastSave = @"Temp\\lingerium.xml";

            using (TextWriter sw = new StreamWriter(lastSave, false, Encoding.UTF8)) //Set encoding
            {
                xmlDocument.Save(sw);
            }

            XmlTextReader xmlTextReader = new XmlTextReader(lastSave);

            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            do
            {
                XmlNodeType nodeType = xmlTextReader.NodeType;
                XmlNodeType xmlNodeType = nodeType;
                if (xmlNodeType != XmlNodeType.Element)
                {
                    continue;
                }
                string name = xmlTextReader.Name;
                if (!dictionary.ContainsKey(name))
                {
                    string text = xmlDocument.SelectSingleNode("//" + name)?.InnerText;
                    string value = (text != null && text.Length > 80) ? (text.Substring(0, 80) + "...") : text;
                    dictionary.Add(name, value);
                }
                if (!xmlTextReader.HasAttributes)
                {
                    continue;
                }
                for (int i = 0; i < xmlTextReader.AttributeCount; i++)
                {
                    xmlTextReader.MoveToAttribute(i);
                    string key = name + "/@" + xmlTextReader.Name;
                    if (!dictionary.ContainsKey(key))
                    {
                        dictionary.Add(key, xmlTextReader.Value);
                    }
                }
                xmlTextReader.MoveToElement();
            }
            while (xmlTextReader.Read());
            return dictionary;


        }




        public static XElement StripNS(XElement root)
        {
            XElement xElement = new XElement(root.Name.LocalName, root.HasElements ? ((IEnumerable)(from el in root.Elements()
                                                                                                    select StripNS(el))) : ((IEnumerable)root.Value));
            xElement.ReplaceAttributes(from attr in root.Attributes()
                                       where !attr.IsNamespaceDeclaration
                                       select attr);
            return xElement;
        }

    }
}
