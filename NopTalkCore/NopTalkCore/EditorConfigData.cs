using System.Collections.Generic;

namespace NopTalkCore
{
	public class EditorConfigData
	{
		public List<string> ProductColumns
		{
			get;
			set;
		}

		public List<string> CategoryColumns
		{
			get;
			set;
		}

		public List<string> ManufacturerColumns
		{
			get;
			set;
		}

		public EditorConfigData()
		{
			ProductColumns = new List<string>();
			CategoryColumns = new List<string>();
			ManufacturerColumns = new List<string>();
		}
	}
}
