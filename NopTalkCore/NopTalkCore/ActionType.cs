namespace NopTalkCore
{
	public enum ActionType
	{
		Insert,
		Update,
		InsertAndUpdate
	}
}
