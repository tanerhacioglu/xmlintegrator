namespace NopTalkCore
{
	public enum BackorderMode
	{
		NoBackorders,
		AllowQtyBelow0,
		AllowQtyBelow0AndNotifyCustomer
	}
}
