using Excel;
using NopTalk;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Xml.Linq;

namespace NopTalkCore
{
	public class SourceHelper
	{
		public static DataTable GetDataTableFromMSSQL(string connString, string sqlString)
		{
			DataTable dataTable = new DataTable();
			using (SqlConnection sqlConnection = new SqlConnection(connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection))
				{
					sqlConnection.Open();
					dataTable.Load(sqlCommand.ExecuteReader());
				}
			}
			return dataTable;
		}

		public static string GetDataAsSummaryString(byte[] pDataContent, FileFormat dataFormatType, string csvDelimeter = null)
		{
			string result = null;
			if (dataFormatType == FileFormat.XML)
			{
				XElement dataAsXmlDocument = GetDataAsXmlDocument(pDataContent);
			}
			return result;
		}

		public static XElement GetDataAsXmlDocument(byte[] pDataContent)
		{
			XElement xElement = null;
			bool flag = pDataContent[0] == 239 && pDataContent[1] == 187 && pDataContent[2] == 191;
			int num = flag ? 3 : 0;
			string @string = Encoding.UTF8.GetString(pDataContent, flag ? num : 0, pDataContent.Length - num);
			xElement = XElement.Parse(@string);
			return XmlHelper.StripNS(xElement);
		}

		public static DataTable GetDataAsDataTable(byte[] pDataContent, FileFormat dataFormatType, int rowIndex = 1, string csvDelimeter = null)
		{
			DataTable result = new DataTable();
			switch (dataFormatType)
			{
			case FileFormat.CSV:
				result = ConvertCSVtoDataTable(pDataContent, csvDelimeter, rowIndex);
				break;
			case FileFormat.XLSX:
				result = ConvertXLSXtoDataTable(pDataContent, rowIndex);
				break;
			}
			return result;
		}

		public static Dictionary<string, string> GetDataTableColumns(DataTable dataTable, bool columnsInRow)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			if (dataTable != null)
			{
				int num = 0;
				foreach (DataColumn column in dataTable.Columns)
				{
					if (columnsInRow)
					{
						dictionary.Add(num.ToString(), dataTable.Rows[0][column.ColumnName].ToString());
					}
					else
					{
						dictionary.Add(num.ToString(), column.ColumnName);
					}
					num++;
				}
			}
			return dictionary;
		}

		private static DataTable ConvertXLSXtoDataTable(byte[] fileContent, int rowIndex = 1)
		{
			using (MemoryStream fileStream = new MemoryStream(fileContent))
			{
				IExcelDataReader excelDataReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
				excelDataReader.IsFirstRowAsColumnNames = true;
				DataSet dataSet = excelDataReader.AsDataSet();
				return dataSet.Tables[0];
			}
		}

		private static DataTable ConvertCSVtoDataTable(byte[] fileContent, string delimeter, int rowIndex = 1)
		{
			DataTable dataTable = new DataTable();
			DataRow dataRow = null;
			using (StreamReader streamReader = new StreamReader(new MemoryStream(fileContent)))
			{
				string[] array = streamReader.ReadLine().Split(new string[1]
				{
					delimeter
				}, StringSplitOptions.None);
				int num = 0;
				string[] array2 = array;
				foreach (string colName in array2)
				{
					dataTable.Columns.Add(IndexedColumnName(colName, dataTable.Columns));
					if (rowIndex == 1)
					{
						if (num == 0)
						{
							dataRow = dataTable.NewRow();
						}
						dataRow[0] = array[num];
						num++;
					}
				}
				if (dataRow != null)
				{
					dataTable.Rows.Add(dataRow);
				}
				while (!streamReader.EndOfStream)
				{
					string[] array3 = streamReader.ReadLine().Split(new string[1]
					{
						delimeter
					}, StringSplitOptions.None);
					dataRow = dataTable.NewRow();
					for (int j = 0; j < array.Length && j < array3.Length; j++)
					{
						dataRow[j] = array3[j];
					}
					dataTable.Rows.Add(dataRow);
				}
			}
			return dataTable;
		}

		private static string IndexedColumnName(string colName, DataColumnCollection dtColumns)
		{
			int num = 0;
			string text = colName;
			while (dtColumns.Contains(text))
			{
				num++;
				text = $"{colName}{num}";
			}
			return text;
		}
	}
}
