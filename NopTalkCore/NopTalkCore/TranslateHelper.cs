using Newtonsoft.Json;
using NopTalkCore.Struct.Translation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace NopTalkCore
{
	public class TranslateHelper
	{
		private HttpClient client;

		public TranslateHelper()
		{
			client = new HttpClient();
		}

		public Dictionary<string, string> ReplaceText(List<string> texts)
		{
			Dictionary<string, string> result = new Dictionary<string, string>();
			foreach (string text in texts)
			{
			}
			return result;
		}

		public Dictionary<string, string> TranslateText(string apiUrl, string subcriptionKey, List<string> texts)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			if (string.IsNullOrEmpty(apiUrl) || string.IsNullOrEmpty(subcriptionKey) || texts == null || !texts.Any() || !texts.Any((string x) => x.Any((char y) => char.IsLetter(y))))
			{
				return dictionary;
			}
			texts = texts.Distinct().ToList();
			List<RequestTranslate> list = new List<RequestTranslate>();
			foreach (string text in texts)
			{
				list.Add(new RequestTranslate
				{
					Text = text
				});
			}
			string content = JsonConvert.SerializeObject(list);
			if (client == null)
			{
				client = new HttpClient();
			}
			using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage())
			{
				httpRequestMessage.Method = HttpMethod.Post;
				httpRequestMessage.RequestUri = new Uri(apiUrl);
				httpRequestMessage.Content = new StringContent(content, Encoding.UTF8, "application/json");
				httpRequestMessage.Headers.Add("Ocp-Apim-Subscription-Key", subcriptionKey);
				HttpResponseMessage result = client.SendAsync(httpRequestMessage).Result;
				string result2 = result.Content.ReadAsStringAsync().Result;
				List<ReponseTranslate> list2 = new List<ReponseTranslate>();
				try
				{
					list2 = JsonConvert.DeserializeObject<List<ReponseTranslate>>(result2);
				}
				catch
				{
					throw new Exception(result2);
				}
				int num = 0;
				if (list2 != null)
				{
					foreach (ReponseTranslate item in list2)
					{
						if (!dictionary.ContainsKey(texts[num]))
						{
							dictionary.Add(texts[num], item.translations.FirstOrDefault().text?.Replace("< ", "<").Replace(" >", ">").Replace(" = ", "="));
						}
						num++;
					}
				}
			}
			return dictionary;
		}
	}
}
