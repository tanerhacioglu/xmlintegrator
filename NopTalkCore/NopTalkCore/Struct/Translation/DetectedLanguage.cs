namespace NopTalkCore.Struct.Translation
{
	public class DetectedLanguage
	{
		public string language
		{
			get;
			set;
		}

		public double score
		{
			get;
			set;
		}
	}
}
