using System.Collections.Generic;

namespace NopTalkCore.Struct.Translation
{
	public class ReponseTranslate
	{
		public DetectedLanguage detectedLanguage
		{
			get;
			set;
		}

		public List<Translation> translations
		{
			get;
			set;
		}
	}
}
