namespace NopTalkCore.Struct
{
	public class ReplaceTextItem
	{
		public string KeyText
		{
			get;
			set;
		}

		public string ReplaceText
		{
			get;
			set;
		}
	}
}
