namespace NopTalkCore.Struct.nopCommerce
{
	public class NopTierPrice
	{
		public int Id
		{
			get;
			set;
		}

		public int ProductId
		{
			get;
			set;
		}

		public int StoreId
		{
			get;
			set;
		}

		public int? CustomerRoleId
		{
			get;
			set;
		}

		public int Quantity
		{
			get;
			set;
		}

		public decimal Price
		{
			get;
			set;
		}

		public string StartDateTimeUtc
		{
			get;
			set;
		}

		public string EndDateTimeUtc
		{
			get;
			set;
		}

		public string ACCID
		{
			get;
			set;
		}

		public bool Updated
		{
			get;
			set;
		}

		public bool Inserted
		{
			get;
			set;
		}
	}
}
