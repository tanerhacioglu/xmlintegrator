namespace NopTalkCore.Struct.Common
{
	public class ListItem
	{
		public string Value
		{
			get;
			set;
		}

		public string Title
		{
			get;
			set;
		}
	}
}
