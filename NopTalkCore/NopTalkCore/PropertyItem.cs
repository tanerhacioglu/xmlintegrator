namespace NopTalkCore
{
	public class PropertyItem
	{
		public string PropertyName
		{
			get;
			set;
		}

		public string PropertyValue
		{
			get;
			set;
		}
	}
}
