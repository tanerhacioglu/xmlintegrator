using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.IO;

namespace NopTalkCore
{
	public static class JsonExtensions
	{
		public static string ToJson(this object entity)
		{
			JsonSerializer jsonSerializer = new JsonSerializer();
			jsonSerializer.Converters.Add(new StringEnumConverter());
			using (StringWriter stringWriter = new StringWriter())
			{
				jsonSerializer.Serialize(stringWriter, entity);
				return stringWriter.ToString();
			}
		}
	}
}
