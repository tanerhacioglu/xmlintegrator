using System;
using System.ServiceProcess;

namespace NopTalkCore
{
	public class ServiceManager
	{
		public static string StartService(string serviceName, int timeoutMilliseconds)
		{
			string result = "";
			ServiceController serviceController = new ServiceController(serviceName);
			try
			{
				TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);
				serviceController.Start();
				serviceController.WaitForStatus(ServiceControllerStatus.Running, timeout);
			}
			catch (Exception ex)
			{
				result = ex.ToString();
			}
			return result;
		}

		public static string StopService(string serviceName, int timeoutMilliseconds)
		{
			string result = "";
			ServiceController serviceController = new ServiceController(serviceName);
			try
			{
				TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);
				serviceController.Stop();
				serviceController.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
			}
			catch (Exception ex)
			{
				result = ex.ToString();
			}
			return result;
		}

		public static string RestartService(string serviceName, int timeoutMilliseconds)
		{
			string result = "";
			ServiceController serviceController = new ServiceController(serviceName);
			try
			{
				int tickCount = Environment.TickCount;
				TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);
				serviceController.Stop();
				serviceController.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
				int tickCount2 = Environment.TickCount;
				timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (tickCount2 - tickCount));
				serviceController.Start();
				serviceController.WaitForStatus(ServiceControllerStatus.Running, timeout);
			}
			catch (Exception ex)
			{
				result = ex.ToString();
			}
			return result;
		}

		public static string IsRunning(string serviceName)
		{
			string result = "0";
			ServiceController serviceController = new ServiceController(serviceName);
			try
			{
				if (serviceController.Status == ServiceControllerStatus.Running)
				{
					result = "1";
				}
			}
			catch (Exception ex)
			{
				result = ex.ToString();
			}
			return result;
		}
	}
}
