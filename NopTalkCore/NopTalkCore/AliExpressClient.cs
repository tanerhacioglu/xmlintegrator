using HtmlAgilityPack;
using Newtonsoft.Json;
using NopTalk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace NopTalkCore
{
	public class AliExpressClient
	{
		public static AliExpressProduct GetProductDetails(string productUrl)
		{
			AliExpressProduct aliExpressProduct = new AliExpressProduct();
			HtmlWeb htmlWeb = new HtmlWeb();
			HtmlDocument htmlDocument = htmlWeb.Load(productUrl);
			string xpath = "//h1[@class='product-name']";
			aliExpressProduct.ProductName = htmlDocument.DocumentNode.SelectSingleNode(xpath)?.InnerText;
			string xpath2 = "//meta[@itemprop='description']/@content";
			aliExpressProduct.ProductShortDescription = htmlDocument.DocumentNode.SelectSingleNode(xpath2)?.Attributes["content"].Value;
			string ınnerText = htmlDocument.DocumentNode.InnerText;
			aliExpressProduct.ProductId = GetProductId(ınnerText);
			aliExpressProduct.ProductSku = aliExpressProduct.ProductId;
			aliExpressProduct.ImageItems = GetImagesPaths(ınnerText);
			aliExpressProduct.VariantionItems = GetVariants(ınnerText);
			aliExpressProduct.AttributeItems = GetAttributes(htmlDocument);
			aliExpressProduct.PropertyItems = GetProperties(htmlDocument);
			aliExpressProduct.ProductFullDescription = GetProductFullDescription(aliExpressProduct.PropertyItems);
			aliExpressProduct.ProductPrice = GetProductPrice(ınnerText);
			if (aliExpressProduct.ProductPrice == 0m)
			{
				aliExpressProduct.ProductPrice = GetMinProductPrice(ınnerText);
			}
			aliExpressProduct.ProductQuantity = GetProductQuantity(ınnerText);
			return aliExpressProduct;
		}

		private static string GetProductFullDescription(List<PropertyItem> propItems)
		{
			string str = null;
			str += "<p><p>";
			foreach (PropertyItem propItem in propItems)
			{
				str += $"<span><strong>{propItem.PropertyName}</strong>{propItem.PropertyValue}</span><br>";
			}
			return str + "</p></p>";
		}

		private static string GetProductId(string content)
		{
			string text = null;
			string text2 = "window.runParams.productId=";
			string value = ";";
			int num = content.IndexOf(text2) + text2.Length;
			int num2 = content.IndexOf(value, num);
			string input = content.Substring(num, num2 - num);
			return Regex.Replace(input, "\\r\\n?|\\n|\\t|\"", string.Empty);
		}

		private static decimal GetProductPrice(string content)
		{
			decimal num = default(decimal);
			string text = "window.runParams.actMinPrice=";
			string value = ";";
			int num2 = content.IndexOf(text) + text.Length;
			int num3 = content.IndexOf(value, num2);
			string input = content.Substring(num2, num3 - num2);
			return GlobalClass.StringToDecimal(Regex.Replace(input, "\\r\\n?|\\n|\\t|\"", string.Empty));
		}

		private static decimal GetMinProductPrice(string content)
		{
			decimal num = default(decimal);
			string text = "window.runParams.minPrice=";
			string value = ";";
			int num2 = content.IndexOf(text) + text.Length;
			int num3 = content.IndexOf(value, num2);
			string input = content.Substring(num2, num3 - num2);
			return GlobalClass.StringToDecimal(Regex.Replace(input, "\\r\\n?|\\n|\\t|\"", string.Empty));
		}

		private static int GetProductQuantity(string content)
		{
			int num = 0;
			string text = "window.runParams.totalAvailQuantity=";
			string value = ";";
			int num2 = content.IndexOf(text) + text.Length;
			int num3 = content.IndexOf(value, num2);
			string input = content.Substring(num2, num3 - num2);
			return GlobalClass.StringToInteger(Regex.Replace(input, "\\r\\n?|\\n|\\t|\"", string.Empty));
		}

		private static List<ImageItem> GetImagesPaths(string content)
		{
			List<ImageItem> list = new List<ImageItem>();
			string text = "window.runParams.imageBigViewURL=[";
			string value = "];";
			int num = content.IndexOf(text) + text.Length;
			int num2 = content.IndexOf(value, num);
			string input = content.Substring(num, num2 - num);
			input = Regex.Replace(input, "\\r\\n?|\\n|\\t|\"", string.Empty);
			List<string> source = input.Split(',').ToList();
			return source.Select((string x) => new ImageItem
			{
				ImageUrl = x
			}).ToList();
		}

		private static List<PropertyItem> GetProperties(HtmlDocument doc)
		{
			List<PropertyItem> list = new List<PropertyItem>();
			string xpath = "//li[@class='property-item']";
			HtmlNodeCollection htmlNodeCollection = doc.DocumentNode.SelectNodes(xpath);
			if (htmlNodeCollection != null)
			{
				foreach (HtmlNode item in (IEnumerable<HtmlNode>)htmlNodeCollection)
				{
					PropertyItem propertyItem = new PropertyItem();
					string xpath2 = "span[@class='propery-title']";
					string propertyName = item.SelectSingleNode(xpath2)?.InnerText;
					string xpath3 = "span[@class='propery-des']";
					string propertyValue = item.SelectSingleNode(xpath3)?.InnerText;
					propertyItem.PropertyName = propertyName;
					propertyItem.PropertyValue = propertyValue;
					list.Add(propertyItem);
				}
			}
			return list;
		}

		private static List<AliExpressProductVariantStruct> GetVariants(string content)
		{
			List<AliExpressProductVariantStruct> result = new List<AliExpressProductVariantStruct>();
			try
			{
				string text = "var skuProducts=[";
				string value = "];";
				int num = content.IndexOf(text) + text.Length;
				int num2 = content.IndexOf(value, num);
				string input = content.Substring(num, num2 - num);
				input = Regex.Replace(input, "\\r\\n?|\\n|\\t", string.Empty);
				input = "[" + input + "]";
				result = JsonConvert.DeserializeObject<List<AliExpressProductVariantStruct>>(input);
			}
			catch (Exception)
			{
			}
			return result;
		}

		private static List<AttributeItem> GetAttributes(HtmlDocument doc)
		{
			List<AttributeItem> list = new List<AttributeItem>();
			string xpath = "//div[@id='j-product-info-sku']/dl[@class='p-property-item']";
			HtmlNodeCollection htmlNodeCollection = doc.DocumentNode.SelectNodes(xpath);
			if (htmlNodeCollection != null)
			{
				foreach (HtmlNode item in (IEnumerable<HtmlNode>)htmlNodeCollection)
				{
					AttributeItem attributeItem = new AttributeItem();
					string xpath2 = "dt[@class='p-item-title']";
					string text2 = attributeItem.AttributeName = item.SelectSingleNode(xpath2)?.InnerText;
					string xpath3 = "dd[@class='p-item-main']/ul/li/a[@data-role='sku']/@data-sku-id";
					HtmlNodeCollection htmlNodeCollection2 = item.SelectNodes(xpath3);
					foreach (HtmlNode item2 in (IEnumerable<HtmlNode>)htmlNodeCollection2)
					{
						string value = item2.Attributes["data-sku-id"].Value;
						string xpath4 = "span";
						string optionValue = item2.SelectSingleNode(xpath4)?.InnerText;
						string xpath5 = "img";
						HtmlNode htmlNode = item2.SelectSingleNode(xpath5);
						string title = htmlNode?.Attributes["title"]?.Value;
						string ımageUrl = htmlNode?.Attributes["bigpic"]?.Value;
						attributeItem.OptionItems.Add(new OptionItem
						{
							OptionId = value,
							OptionValue = optionValue,
							Title = title,
							ImageUrl = ımageUrl
						});
					}
					list.Add(attributeItem);
				}
			}
			return list;
		}
	}
}
