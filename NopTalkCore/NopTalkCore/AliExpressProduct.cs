using System.Collections.Generic;

namespace NopTalkCore
{
	public class AliExpressProduct
	{
		public string ProductId
		{
			get;
			set;
		}

		public string ProductSku
		{
			get;
			set;
		}

		public string ProductName
		{
			get;
			set;
		}

		public string ProductShortDescription
		{
			get;
			set;
		}

		public string ProductFullDescription
		{
			get;
			set;
		}

		public decimal ProductPrice
		{
			get;
			set;
		}

		public int ProductQuantity
		{
			get;
			set;
		}

		public List<AttributeItem> AttributeItems
		{
			get;
			set;
		}

		public List<ImageItem> ImageItems
		{
			get;
			set;
		}

		public List<PropertyItem> PropertyItems
		{
			get;
			set;
		}

		public List<AliExpressProductVariantStruct> VariantionItems
		{
			get;
			set;
		}
	}
}
