using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace NopTalkCore
{
	public class ReplaceTextHelper
	{
		public Dictionary<string, string> ReplaceTextItems
		{
			get;
			set;
		}

		public Dictionary<string, string> TranslateTextItems
		{
			get;
			set;
		}

		private int _vendorSourceId
		{
			get;
			set;
		}

		public ReplaceTextHelper(int vendorSourceId)
		{
			try
			{
				TranslateTextItems = new Dictionary<string, string>();
				ReplaceTextItems = new Dictionary<string, string>();
				_vendorSourceId = vendorSourceId;
				string path = $".\\Translations\\TranslateText_{vendorSourceId}.json";
				if (File.Exists(path))
				{
					string value = File.ReadAllText(path);
					TranslateTextItems = JsonConvert.DeserializeObject<Dictionary<string, string>>(value);
				}
				string path2 = $".\\Translations\\ReplaceText_{vendorSourceId}.json";
				if (File.Exists(path2))
				{
					string value2 = File.ReadAllText(path2);
					ReplaceTextItems = JsonConvert.DeserializeObject<Dictionary<string, string>>(value2);
				}
			}
			catch
			{
			}
		}

		public void SetReplacedText(string text, string translatedText)
		{
			if (!string.IsNullOrEmpty(text) && !TranslateTextItems.ContainsKey(text))
			{
				TranslateTextItems.Add(text, translatedText);
			}
		}

		public string GetReplacedText(string text)
		{
			if (string.IsNullOrEmpty(text) || ((ReplaceTextItems == null || !ReplaceTextItems.Any()) && (TranslateTextItems == null || !TranslateTextItems.Any())))
			{
				return null;
			}
			if (TranslateTextItems.ContainsKey(text))
			{
				text = TranslateTextItems[text];
				return text;
			}
			if (ReplaceTextItems != null && ReplaceTextItems.Any())
			{
				bool flag = false;
				text = text.Replace("<Span Class='Pdbdescsectiontitle'>", " ");
				text = text.Replace("</Span>", " ");
				text = text.Replace("<Span>", " ");
				text = text.Replace("<Span Class='Pdbdescsectiontext'>", " ");
				text = text.Replace("<Span Class='Pdbdescsectionlist'>", " ");
				text = text.Replace("<Span Class='Pdbdescsectionitem'>", " ");
				IOrderedEnumerable<KeyValuePair<string, string>> orderedEnumerable = ReplaceTextItems.OrderByDescending((KeyValuePair<string, string> i) => i.Key.Length);
				foreach (KeyValuePair<string, string> item in orderedEnumerable)
				{
					if (text.IndexOf(item.Key, StringComparison.OrdinalIgnoreCase) >= 0 || text.Contains(item.Key))
					{
						text = Regex.Replace(text, item.Key, item.Value, RegexOptions.IgnoreCase);
						text = text.Replace(item.Key, item.Value);
						flag = true;
					}
				}
				if (flag)
				{
					return text;
				}
			}
			return null;
		}

		public void SaveReplacedFile()
		{
			if (TranslateTextItems != null && TranslateTextItems.Any())
			{
				string path = $".\\Translations\\TranslateText_{_vendorSourceId}.json";
				File.WriteAllText(path, JsonConvert.SerializeObject(TranslateTextItems));
			}
		}
	}
}
