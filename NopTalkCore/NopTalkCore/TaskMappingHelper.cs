using System;
using System.Collections.Generic;

namespace NopTalkCore
{
	public class TaskMappingHelper
	{
		public static List<TaskItemMapping> GetTierPricingNewItem()
		{
			Guid ıd = Guid.NewGuid();
			List<TaskItemMapping> list = new List<TaskItemMapping>();
			list.Add(new TaskItemMapping
			{
				Id = ıd,
				MappingItemType = 0,
				FieldName = "Quantity For Tier Price"
			});
			list.Add(new TaskItemMapping
			{
				Id = ıd,
				MappingItemType = 1,
				FieldName = "Price For Tier Price"
			});
			list.Add(new TaskItemMapping
			{
				Id = ıd,
				MappingItemType = 2,
				FieldName = "Customer Role For Tier Price"
			});
			list.Add(new TaskItemMapping
			{
				Id = ıd,
				MappingItemType = 13,
				FieldName = "Start DateTime"
			});
			list.Add(new TaskItemMapping
			{
				Id = ıd,
				MappingItemType = 14,
				FieldName = "End DateTime"
			});
			return list;
		}
	}
}
