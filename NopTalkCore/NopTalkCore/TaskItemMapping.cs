using System;

namespace NopTalkCore
{
	public class TaskItemMapping
	{
		public Guid Id
		{
			get;
			set;
		}

		public int FieldIndex
		{
			get;
			set;
		}

		public string FieldName
		{
			get;
			set;
		}

		public string FieldNameForDisplay
		{
			get;
			set;
		}

		public int MappingItemType
		{
			get;
			set;
		}

		public bool MappedToSource
		{
			get;
			set;
		}

		public string FieldRule
		{
			get;
			set;
		}

		public int FieldRule1
		{
			get;
			set;
		}

		public decimal FieldRule2
		{
			get;
			set;
		}

		public bool FieldRule3
		{
			get;
			set;
		}

		public bool FieldRule4
		{
			get;
			set;
		}

		public int FieldRule5
		{
			get;
			set;
		}

		public bool FieldImport
		{
			get;
			set;
		}
	}
}
