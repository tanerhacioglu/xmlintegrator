using HtmlAgilityPack;
using NopTalk;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace NopTalkCore
{
	public class BestSecretClient
	{
		private HttpClient hc = null;

		public BestSecretClient(string loginUrl, string username, string password)
		{
			hc = new HttpClient();
			HttpResponseMessage result = hc.PostAsync(loginUrl, new StringContent("j_username=" + username + "&j_password=" + password, Encoding.UTF8, "application/x-www-form-urlencoded")).Result;
		}

		public async Task<List<string>> GetProductsListUrls(string productListPageUrl)
		{
			List<string> urlList = new List<string>();
			HttpResponseMessage resultPlaylist = hc.GetAsync(productListPageUrl).Result;
			Stream stream = await resultPlaylist.Content.ReadAsStreamAsync();
			HtmlDocument doc = new HtmlDocument();
			doc.Load(stream);
			string pageUrls2 = "//div[@class='pager-pagination'][1]/a";
			HtmlNodeCollection pageUrlsValues = doc.DocumentNode.SelectNodes(pageUrls2);
			if (pageUrlsValues == null)
			{
				pageUrls2 = "//div[@class='figure-container']/a";
				pageUrlsValues = doc.DocumentNode.SelectNodes(pageUrls2);
			}
			int i = 1;
			if (pageUrlsValues != null)
			{
				foreach (HtmlNode item in (IEnumerable<HtmlNode>)pageUrlsValues)
				{
					if (i < pageUrlsValues.Count() && item.Attributes != null && item.Attributes.Contains("href"))
					{
						string itemValue = item.Attributes["href"].Value;
						if (!urlList.Contains(itemValue))
						{
							urlList.Add(itemValue);
						}
					}
					i++;
				}
			}
			return urlList;
		}

		public async Task<List<string>> GetProductsUrls(string productListPageUrl)
		{
			List<string> urlList = new List<string>();
			HttpResponseMessage resultPlaylist = hc.GetAsync(productListPageUrl).Result;
			Stream stream = await resultPlaylist.Content.ReadAsStreamAsync();
			HtmlDocument doc = new HtmlDocument();
			doc.Load(stream);
			string productsUrls2 = "//div[@class='figure-container']/a[@class='figure-image figure-image-hover']";
			HtmlNodeCollection productUrlsValues = doc.DocumentNode.SelectNodes(productsUrls2);
			if (productUrlsValues == null)
			{
				productsUrls2 = "//div[@class='figure-container']/a";
				productUrlsValues = doc.DocumentNode.SelectNodes(productsUrls2);
			}
			if (productUrlsValues != null)
			{
				foreach (HtmlNode item in (IEnumerable<HtmlNode>)productUrlsValues)
				{
					if (item.Attributes != null && item.Attributes.Contains("href"))
					{
						string itemValue = item.Attributes["href"].Value;
						urlList.Add(itemValue);
					}
				}
			}
			return urlList;
		}

		public async Task<BestSecretProduct> GetProductDetails(string productUrl)
		{
			BestSecretProduct bestSecretProduct = new BestSecretProduct();
			string param1 = HttpUtility.ParseQueryString(HttpUtility.UrlDecode(productUrl)).Get("amp;code");
			productUrl = $"https://www.bestsecret.com/product.htm?code={param1}";
			HttpResponseMessage resultPlaylist = hc.GetAsync(productUrl).Result;
			Stream stream = await resultPlaylist.Content.ReadAsStreamAsync();
			HtmlDocument doc = new HtmlDocument();
			doc.Load(stream);
			bestSecretProduct.ProductSku = param1.Trim();
			if (doc.DocumentNode.InnerText.Contains("This item is no longer available in this size."))
			{
				bestSecretProduct.Unavailable = true;
			}
			string itemNamePath = "//head/title";
			string itemNameValue = HttpUtility.UrlDecode(doc.DocumentNode.SelectSingleNode(itemNamePath)?.InnerText);
			bestSecretProduct.ProductName = ((itemNameValue != null) ? itemNameValue.Split(new string[1]
			{
				"&ndash;"
			}, StringSplitOptions.None)[1].Trim() : null);
			string itemFullDescPath = "//div[@class='collapsible collapsible-block']/div[@class='collapsible-container type-formatted']";
			string itemFullDescValue = bestSecretProduct.ProductFullDescription = HttpUtility.UrlDecode(doc.DocumentNode.SelectSingleNode(itemFullDescPath)?.InnerHtml);
			string itemPricePath = "//h2[@class='t-price']/span";
			string itemPriceValue = doc.DocumentNode.SelectSingleNode(itemPricePath)?.InnerText.Trim();
			bestSecretProduct.ProductPrice = GlobalClass.StringToDecimal(Regex.Match(itemPriceValue, "\\d+.+\\d").Value);
			bestSecretProduct.ProductCostPrice = bestSecretProduct.ProductPrice;
			string itemOldPricePath = "//span[@class='t-value-old']";
			doc.DocumentNode.SelectSingleNode(itemOldPricePath)?.InnerText.Trim();
			bestSecretProduct.ProductPrice = GlobalClass.StringToDecimal(Regex.Match(itemPriceValue, "\\d+.+\\d").Value);
			string itemManufacturerPath = "//h2[@class='t-strong']/a";
			string itemManufacturerValue = doc.DocumentNode.SelectSingleNode(itemManufacturerPath)?.InnerText;
			bestSecretProduct.Manufacturer = FormatString(itemManufacturerValue);
			string itemShortDescriptionPath = "//h5[@class='t-subheading']";
			string itemShortDescriptionValue = doc.DocumentNode.SelectSingleNode(itemShortDescriptionPath)?.InnerHtml;
			bestSecretProduct.ProductShortDescription = FormatString((itemShortDescriptionValue != null) ? itemShortDescriptionValue.Split(new string[1]
			{
				"<br>"
			}, StringSplitOptions.None)[1].Trim() : null);
			bestSecretProduct.ImageItems = GetImagesPaths(doc);
			bestSecretProduct.AttributeItems = GetAttributes(doc);
			bestSecretProduct.CategoriesItems = GetCategories(doc);
			return bestSecretProduct;
		}

		private static List<ImageItem> GetImagesPaths(HtmlDocument doc)
		{
			List<ImageItem> list = new List<ImageItem>();
			string xpath = "//div[@class='carousel']/div[@class='carousel-inner']/ul/li/a/img";
			HtmlNodeCollection htmlNodeCollection = doc.DocumentNode.SelectNodes(xpath);
			if (htmlNodeCollection != null)
			{
				foreach (HtmlNode item in (IEnumerable<HtmlNode>)htmlNodeCollection)
				{
					string value = item.Attributes["src"].Value;
					list.Add(new ImageItem
					{
						ImageUrl = value
					});
				}
			}
			return list;
		}

		private static List<AttributeItem> GetAttributes(HtmlDocument doc)
		{
			List<AttributeItem> list = new List<AttributeItem>();
			string xpath = "//div[@class='control-group pull-left']/label";
			HtmlNode htmlNode = doc.DocumentNode.SelectSingleNode(xpath);
			AttributeItem attributeItem = new AttributeItem();
			attributeItem.AttributeName = htmlNode.InnerText;
			string xpath2 = "//div[@class='control-group pull-left']/div[@class='control']/select/option";
			HtmlNodeCollection htmlNodeCollection = doc.DocumentNode.SelectNodes(xpath2);
			if (htmlNodeCollection != null)
			{
				foreach (HtmlNode item in (IEnumerable<HtmlNode>)htmlNodeCollection)
				{
					string value = item.Attributes["value"].Value;
					string value2 = item.InnerText.Trim();
					value2 = FormatString(value2);
					int quantity = 5;
					if (value2.Contains("left"))
					{
						quantity = 1;
					}
					if (value2.Contains("sold out"))
					{
						quantity = 0;
					}
					attributeItem.OptionItems.Add(new OptionItem
					{
						OptionId = value,
						OptionValue = value2,
						Title = value2,
						Quantity = quantity
					});
				}
				list.Add(attributeItem);
			}
			return list;
		}

		private static List<CategoryItem> GetCategories(HtmlDocument doc)
		{
			List<CategoryItem> list = new List<CategoryItem>();
			string xpath = "//div[@class='gender-switch']/a[@class='active']";
			string text = FormatString(doc.DocumentNode.SelectSingleNode(xpath)?.InnerText);
			if (!string.IsNullOrEmpty(text))
			{
				list.Add(new CategoryItem
				{
					Category = text
				});
			}
			string xpath2 = "//div[@class='hint-elements']/a";
			HtmlNodeCollection htmlNodeCollection = doc.DocumentNode.SelectNodes(xpath2);
			if (htmlNodeCollection != null)
			{
				int num = 0;
				foreach (HtmlNode item in (IEnumerable<HtmlNode>)htmlNodeCollection)
				{
					if (num > 0)
					{
						string category = FormatString(item.InnerText);
						list.Add(new CategoryItem
						{
							Category = category
						});
					}
					num++;
				}
			}
			return list;
		}

		public static string FormatString(string value)
		{
			if (string.IsNullOrEmpty(value))
			{
				return null;
			}
			string input = Regex.Replace(value, "<[^>]+>", "").Trim();
			string input2 = Regex.Replace(input, "&nbsp;", " ");
			string input3 = Regex.Replace(input2, "\\s{2,}", " ");
			string text = Regex.Replace(input3, "\\t|\\n|\\r", "");
			text = text.Replace("&#034;", "\"");
			return text.Trim();
		}
	}
}
