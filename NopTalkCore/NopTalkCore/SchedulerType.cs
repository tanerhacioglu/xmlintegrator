namespace NopTalkCore
{
	public enum SchedulerType
	{
		Minutely,
		Hourly,
		Daily
	}
}
