namespace NopTalkCore
{
	public class AliExpressProductVariantStruct
	{
		public string skuAttr
		{
			get;
			set;
		}

		public string skuPropIds
		{
			get;
			set;
		}

		public SkuVal skuVal
		{
			get;
			set;
		}
	}
}
