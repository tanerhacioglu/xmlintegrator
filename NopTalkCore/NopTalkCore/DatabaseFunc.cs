using System.Data;
using System.Data.SqlServerCe;

namespace NopTalkCore
{
	public class DatabaseFunc
	{
		public static void DatabaseInitial()
		{
			UpdateToVersion12();
		}

		public static void SamplesLoad(string fileContent)
		{
		}

		public static void UpdateToVersion12()
		{
			UpdateSource();
			UpdateSourceMapping();
			UpdateTaskMapping();
			UpdateSource2();
			UpdateSource3();
			UpdateSourceMapping2();
			UpdateTaskMapping2();
			UpdateTaskMapping3();
			UpdateSourceMapping4();
			UpdateSourceMapping5();
			UpdateTaskMapping4();
			AddTaxCategoryToSourceMapping();
			AddTaxCategoryToTaskMapping();
			AddGtinWeigtToSourceMapping();
			AddGtinWeigtToTaskMapping();
			AddPicturesSaveToTaskMapping();
			ChangeTypeTaskMapping();
			AddProdSkuValToSourceMapping();
			AddJsonConfigToSourceMapping();
			AddJsonConfigToTaskMapping();
			UpdateSourceAddEntityType();
			UpdateJsonConfigToSourceMapping();
			UpdateJsonConfigToTaskMapping();
			AddJsonEditorConfigToStore();
			UpdateSourceAddEncoding();
			UpdateSourceAddContent();
			UpdateSourceAddAuthHeader();
			AddDatabaseType();
		}

		private static void UpdateSource()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				sqlCeConnection.Open();
				string commandText = "select SoapAction, SoapRequest from VendorSource";
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [VendorSource] ADD SoapAction nvarchar(4000) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [VendorSource] ADD SoapRequest nvarchar(4000) NULL;";
				using (SqlCeCommand sqlCeCommand3 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand3.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void UpdateSourceMapping()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select ProdSkuMap from SourceMapping";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [SourceMapping] ADD ProdSkuMap nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void UpdateTaskMapping()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select Cat1Parent, Cat2Parent, Cat3Parent, Cat4Parent from Task";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [Task] ADD Cat1Parent int NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [Task] ADD Cat2Parent int NULL;";
				using (SqlCeCommand sqlCeCommand3 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand3.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [Task] ADD Cat3Parent int NULL;";
				using (SqlCeCommand sqlCeCommand4 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand4.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [Task] ADD Cat4Parent int NULL;";
				using (SqlCeCommand sqlCeCommand5 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand5.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void UpdateSource2()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				sqlCeConnection.Open();
				string commandText = "select ProductIdInStore from VendorSource";
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [VendorSource] ADD ProductIdInStore int NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void UpdateSourceMapping2()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select VendorMap, VendorVal, CatIsId, Cat1IsId, Cat2IsId, Cat3IsId, Cat4IsId, ManufacturerIsId, VendorIsId from SourceMapping";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [SourceMapping] ADD VendorMap nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD VendorVal nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand3 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand3.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD CatIsId int NULL;";
				using (SqlCeCommand sqlCeCommand4 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand4.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD Cat1IsId int NULL;";
				using (SqlCeCommand sqlCeCommand5 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand5.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD Cat2IsId int NULL;";
				using (SqlCeCommand sqlCeCommand6 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand6.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD Cat3IsId int NULL;";
				using (SqlCeCommand sqlCeCommand7 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand7.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD Cat4IsId int NULL;";
				using (SqlCeCommand sqlCeCommand8 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand8.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD ManufacturerIsId int NULL;";
				using (SqlCeCommand sqlCeCommand9 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand9.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD VendorIsId int NULL;";
				using (SqlCeCommand sqlCeCommand10 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand10.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void UpdateTaskMapping2()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select DeleteSourceFile, VendorVal, VendorAction from Task";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [Task] ADD VendorVal nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [Task] ADD DeleteSourceFile int NULL;";
				using (SqlCeCommand sqlCeCommand3 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand3.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [Task] ADD VendorAction int NULL;";
				using (SqlCeCommand sqlCeCommand4 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand4.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void UpdateTaskMapping3()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select Type from EShop";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [Task] ALTER COLUMN EShopId int NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [EShop] ADD Type int NULL;";
				using (SqlCeCommand sqlCeCommand3 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand3.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void UpdateSourceMapping4()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select ImageIsBinary, Image1IsBinary, Image2IsBinary, Image3IsBinary, Image4IsBinary from SourceMapping";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [SourceMapping] ADD ImageIsBinary int NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD Image1IsBinary int NULL;";
				using (SqlCeCommand sqlCeCommand3 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand3.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD Image2IsBinary int NULL;";
				using (SqlCeCommand sqlCeCommand4 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand4.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD Image3IsBinary int NULL;";
				using (SqlCeCommand sqlCeCommand5 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand5.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD Image4IsBinary int NULL;";
				using (SqlCeCommand sqlCeCommand6 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand6.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void UpdateSourceMapping5()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select ImageVal, Image1Val, Image2Val, Image3Val, Image4Val from SourceMapping";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [SourceMapping] ADD ImageVal nvarchar(500) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD Image1Val nvarchar(500) NULL;";
				using (SqlCeCommand sqlCeCommand3 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand3.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD Image2Val nvarchar(500) NULL;";
				using (SqlCeCommand sqlCeCommand4 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand4.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD Image3Val nvarchar(500) NULL;";
				using (SqlCeCommand sqlCeCommand5 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand5.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD Image4Val nvarchar(500) NULL;";
				using (SqlCeCommand sqlCeCommand6 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand6.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void UpdateTaskMapping4()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select RelatedTaskId, RelatedSourceAddress from Task";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [Task] ADD RelatedTaskId int NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [Task] ADD RelatedSourceAddress nvarchar(500) NULL;";
				using (SqlCeCommand sqlCeCommand3 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand3.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void AddTaxCategoryToSourceMapping()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select TaxCategoryMap, TaxCategoryVal from SourceMapping";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [SourceMapping] ADD TaxCategoryMap nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD TaxCategoryVal nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand3 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand3.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void AddTaxCategoryToTaskMapping()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select TaxCategoryVal, TaxCategoryAction from Task";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [Task] ADD TaxCategoryVal nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [Task] ADD TaxCategoryAction int NULL;";
				using (SqlCeCommand sqlCeCommand3 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand3.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void AddGtinWeigtToSourceMapping()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select GtinMap, GtinVal, WeightMap, WeightVal from SourceMapping";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [SourceMapping] ADD GtinMap nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD GtinVal nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand3 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand3.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD WeightMap nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand4 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand4.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [SourceMapping] ADD WeightVal nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand5 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand5.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void AddGtinWeigtToTaskMapping()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select GtinAction, GtinVal, WeightAction, WeightVal from Task";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [Task] ADD GtinVal nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [Task] ADD GtinAction int NULL;";
				using (SqlCeCommand sqlCeCommand3 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand3.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [Task] ADD WeightVal nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand4 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand4.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [Task] ADD WeightAction int NULL;";
				using (SqlCeCommand sqlCeCommand5 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand5.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void AddPicturesSaveToTaskMapping()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select SavePicturesInFile, SavePicturesPath from Task";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [Task] ADD SavePicturesPath nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
				commandText2 = "ALTER TABLE [Task] ADD SavePicturesInFile int NULL;";
				using (SqlCeCommand sqlCeCommand3 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand3.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void ChangeTypeTaskMapping()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "ALTER TABLE [Task] ALTER COLUMN [ProdCostVal] nvarchar(250) NULL;";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
				commandText = "ALTER TABLE [Task] ALTER COLUMN [ProdPriceVal] nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
				commandText = "ALTER TABLE [Task] ALTER COLUMN [ProdStockVal] nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand3 = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand3.ExecuteNonQuery();
				}
				commandText = "ALTER TABLE [Task] ALTER COLUMN [WeightVal] nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand4 = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand4.ExecuteNonQuery();
				}
			}
			catch
			{
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void AddProdSkuValToSourceMapping()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select ProdSkuVal from SourceMapping";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [SourceMapping] ADD ProdSkuVal nvarchar(250) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void AddJsonConfigToSourceMapping()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select ConfigData from SourceMapping";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [SourceMapping] ADD ConfigData nvarchar(4000) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void AddJsonConfigToTaskMapping()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select ConfigData from [Task]";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [Task] ADD ConfigData nvarchar(4000) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void UpdateSourceAddEntityType()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				sqlCeConnection.Open();
				string commandText = "select EntityType from VendorSource";
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [VendorSource] ADD EntityType int NOT NULL DEFAULT(0);";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void UpdateJsonConfigToSourceMapping()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				sqlCeConnection.Open();
				string commandText = "ALTER TABLE [SourceMapping] ALTER COLUMN ConfigData ntext NULL;";
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void UpdateJsonConfigToTaskMapping()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				sqlCeConnection.Open();
				string commandText = "ALTER TABLE [Task] ALTER COLUMN ConfigData ntext NULL;";
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void AddJsonEditorConfigToStore()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select EditorConfigData from [EShop]";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [EShop] ADD EditorConfigData ntext NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void UpdateSourceAddEncoding()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				sqlCeConnection.Open();
				string commandText = "select SourceEncoding from VendorSource";
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [VendorSource] ADD SourceEncoding nvarchar(50) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void UpdateSourceAddContent()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				sqlCeConnection.Open();
				string commandText = "select SourceContent from VendorSource";
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [VendorSource] ADD SourceContent ntext NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void UpdateSource3()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				sqlCeConnection.Open();
				string commandText = "select UserIdInStore from VendorSource";
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [VendorSource] ADD UserIdInStore int NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void UpdateSourceAddAuthHeader()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				sqlCeConnection.Open();
				string commandText = "select AuthHeader from VendorSource";
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [VendorSource] ADD AuthHeader nvarchar(500) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}

		private static void AddDatabaseType()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			try
			{
				string commandText = "select DatabaseType from EShop";
				sqlCeConnection.Open();
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection))
				{
					sqlCeCommand.ExecuteNonQuery();
				}
			}
			catch
			{
				string commandText2 = "ALTER TABLE [EShop] ADD DatabaseType nvarchar(10) NULL;";
				using (SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection))
				{
					sqlCeCommand2.ExecuteNonQuery();
				}
			}
			finally
			{
				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
			}
		}
	}
}
