using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class SourceCustomerOrdersMapping
	{
		public List<SourceItemMapping> Items
		{
			get;
			set;
		}

		public SourceCustomerOrdersMapping()
		{
			Items = new List<SourceItemMapping>();
		}
	}
}
