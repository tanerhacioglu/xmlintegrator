using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class TaskCustomerRoleMapping
	{
		public bool DeleteBeforeImport
		{
			get;
			set;
		}

		public List<TaskItemMapping> Items
		{
			get;
			set;
		}

		public TaskCustomerRoleMapping()
		{
			Items = new List<TaskItemMapping>();
		}
	}
}
