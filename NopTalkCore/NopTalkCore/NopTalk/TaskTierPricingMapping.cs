using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class TaskTierPricingMapping
	{
		public List<TaskItemMapping> TaskTierPricingMappingItems
		{
			get;
			set;
		}

		public TaskTierPricingMapping()
		{
			TaskTierPricingMappingItems = new List<TaskItemMapping>();
		}
	}
}
