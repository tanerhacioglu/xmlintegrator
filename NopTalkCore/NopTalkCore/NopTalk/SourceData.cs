using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NopTalk
{
	public class SourceData
	{
		public int? VendorSourceId
		{
			get;
			set;
		}

		public char? CsvDelimeter
		{
			get;
			set;
		}

		public int SourceType
		{
			get;
			set;
		}

		[Required(ErrorMessage = "The Source address field is required.")]
		public string SourceAddress
		{
			get;
			set;
		}

		public string SourceUser
		{
			get;
			set;
		}

		public string SourceEncoding
		{
			get;
			set;
		}

		public string SourcePassword
		{
			get;
			set;
		}

		public int SourceFormat
		{
			get;
			set;
		}

		public int ScheduleType
		{
			get;
			set;
		}

		public long ScheduleInterval
		{
			get;
			set;
		}

		public int ScheduleHour
		{
			get;
			set;
		}

		public int ScheduleMinute
		{
			get;
			set;
		}

		public int? FirstRowIndex
		{
			get;
			set;
		}

		public string VendorName
		{
			get;
			set;
		}

		public string SoapAction
		{
			get;
			set;
		}

		public string SoapRequest
		{
			get;
			set;
		}

		public int ProductIdInStore
		{
			get;
			set;
		}

		public int UserIdInStore
		{
			get;
			set;
		}

		public EntityType EntityType
		{
			get;
			set;
		}

		public string SourceContent
		{
			get;
			set;
		}

		public string AuthHeader
		{
			get;
			set;
		}

		public Encoding GetEncoding
		{
			get
			{
				try
				{
					return string.IsNullOrEmpty(SourceEncoding) ? Encoding.UTF8 : Encoding.GetEncoding(Convert.ToInt32(SourceEncoding));
				}
				catch
				{
					return Encoding.UTF8;
				}
			}
		}
	}
}
