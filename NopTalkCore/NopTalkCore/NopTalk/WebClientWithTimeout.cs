using System;
using System.Net;

namespace NopTalk
{
	public class WebClientWithTimeout : WebClient
	{
		protected override WebRequest GetWebRequest(Uri address)
		{
			WebRequest webRequest = base.GetWebRequest(address);
			webRequest.Timeout = Convert.ToInt32(TimeSpan.FromMinutes(60.0).TotalMilliseconds);
			return webRequest;
		}
	}
}
