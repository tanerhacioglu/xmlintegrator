namespace NopTalk
{
	public class VendorData
	{
		public int VendorId
		{
			get;
			set;
		}

		public string VendorName
		{
			get;
			set;
		}

		public string VendorDescription
		{
			get;
			set;
		}

		public int? VendorSourceId
		{
			get;
			set;
		}

		public int? VendorSourceMappingId
		{
			get;
			set;
		}
	}
}
