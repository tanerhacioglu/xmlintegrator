using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class SourceCustomerRoleMapping
	{
		public List<SourceItemMapping> Items
		{
			get;
			set;
		}

		public SourceCustomerRoleMapping()
		{
			Items = new List<SourceItemMapping>();
		}
	}
}
