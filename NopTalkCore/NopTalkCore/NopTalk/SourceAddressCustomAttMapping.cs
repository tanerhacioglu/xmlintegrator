using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class SourceAddressCustomAttMapping
	{
		public List<SourceItemMapping> Items
		{
			get;
			set;
		}

		public SourceAddressCustomAttMapping()
		{
			Items = new List<SourceItemMapping>();
		}
	}
}
