					using NopTalkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml.XPath;

namespace NopTalk
{
	public class SourceItem
	{
		public class WishListItem
		{
			public Guid Id
			{
				get;
				set;
			}

			public string ProductSKU
			{
				get;
				set;
			}

			public string CustomerEmail
			{
				get;
				set;
			}

			public string CustomerUsername
			{
				get;
				set;
			}

			public string CustomSKUID
			{
				get;
				set;
			}
		}

		public class TierPricingItem
		{
			public Guid Id
			{
				get;
				set;
			}

			public int Quantity
			{
				get;
				set;
			}

			public decimal Price
			{
				get;
				set;
			}

			public decimal AddPrice
			{
				get;
				set;
			}

			public int AddPercent
			{
				get;
				set;
			}

			public List<string> CustomerRoles
			{
				get;
				set;
			}

			public string StartDateTime
			{
				get;
				set;
			}

			public string EndDateTime
			{
				get;
				set;
			}
		}

		public class CustomerItem
		{
			public string FirstName
			{
				get;
				set;
			}

			public string LastName
			{
				get;
				set;
			}

			public string Email
			{
				get;
				set;
			}

			public string Gender
			{
				get;
				set;
			}

			public string BirthDay
			{
				get;
				set;
			}

			public bool IsTaxExempt
			{
				get;
				set;
			}

			public bool? Newsletter
			{
				get;
				set;
			}

			public string ManagerOfVendor
			{
				get;
				set;
			}

			public bool? Active
			{
				get;
				set;
			}

			public string Password
			{
				get;
				set;
			}

			public string Username
			{
				get;
				set;
			}

			public List<CustomerAddressItem> CustomerAddressItems
			{
				get;
				set;
			}

			public List<CustomerRuleItem> CustomerRoleItems
			{
				get;
				set;
			}

			public CustomerOthersItem CustomerOthersItem
			{
				get;
				set;
			}

			public CustomerAddressItem CustomerMainAddress
			{
				get;
				set;
			}

			public CustomerItem()
			{
				CustomerAddressItems = new List<CustomerAddressItem>();
				CustomerRoleItems = new List<CustomerRuleItem>();
				CustomerOthersItem = new CustomerOthersItem();
				CustomerMainAddress = new CustomerAddressItem();
			}
		}

		public class CustomerAddressItem
		{
			public string Type
			{
				get;
				set;
			}

			public string FirstName
			{
				get;
				set;
			}

			public string LastName
			{
				get;
				set;
			}

			public string Email
			{
				get;
				set;
			}

			public string Company
			{
				get;
				set;
			}

			public string Country
			{
				get;
				set;
			}

			public string County
			{
				get;
				set;
			}

			public string State
			{
				get;
				set;
			}

			public string City
			{
				get;
				set;
			}

			public string Address1
			{
				get;
				set;
			}

			public string Address2
			{
				get;
				set;
			}

			public string ZipPostalCode
			{
				get;
				set;
			}

			public string Phone
			{
				get;
				set;
			}

			public string Fax
			{
				get;
				set;
			}
		}

		public class CustomerOthersItem
		{
			public string Address3
			{
				get;
				set;
			}

			public string AddressName
			{
				get;
				set;
			}

			public string SAPCustomer
			{
				get;
				set;
			}

			public string GPCustomer
			{
				get;
				set;
			}

			public string AddressSAPCustomer
			{
				get;
				set;
			}

			public bool? MainAddress
			{
				get;
				set;
			}
		}

		public class AttributeValues
		{
			public Guid MappingId
			{
				get;
				set;
			}

			public string AttInfo_Id
			{
				get;
				set;
			}

			public string AttInfo_Name
			{
				get;
				set;
			}

			public int AttInfo_OptionType
			{
				get;
				set;
			}

			public string AttInfo_DefaultValue
			{
				get;
				set;
			}

			public int AttInfo_DisplayOrder
			{
				get;
				set;
			}

			public string AttInfo_TextPrompt
			{
				get;
				set;
			}

			public bool AttInfo_IsRequired
			{
				get;
				set;
			}

			public string AttValue_Value
			{
				get;
				set;
			}

			public int AttValue_Stock
			{
				get;
				set;
			}

			public bool AttValue_IsPreselected
			{
				get;
				set;
			}

			public string AttValue_Color
			{
				get;
				set;
			}

			public string AttValue_AssociatedPicture
			{
				get;
				set;
			}

			public string AttValue_SquarePicture
			{
				get;
				set;
			}

			public int AttValue_AssociatedPictureId
			{
				get;
				set;
			}

			public int AttValue_SquarePictureId
			{
				get;
				set;
			}

			public int AttValue_DisplayOrder
			{
				get;
				set;
			}

			public bool AttValue_PriceUsePercent
			{
				get;
				set;
			}

			public decimal AttValue_Price
			{
				get;
				set;
			}

			public decimal AttValue_WeightAdjustment
			{
				get;
				set;
			}

			public decimal AttValue_Cost
			{
				get;
				set;
			}

			public string AttOrgCode
			{
				get;
				set;
			}

			public string Title
			{
				get;
				set;
			}

			public int AttMapId
			{
				get;
				set;
			}

			public int AttValueId
			{
				get;
				set;
			}

			public int AttDbId
			{
				get;
				set;
			}

			public int AttDbMapId
			{
				get;
				set;
			}

			public int AttDbValueId
			{
				get;
				set;
			}

			public bool ShowOnProduct
			{
				get;
				set;
			}

			public bool AllowFiltering
			{
				get;
				set;
			}

			public string KeyGroup
			{
				get;
				set;
			}

			public string Key
			{
				get;
				set;
			}

			public bool Import
			{
				get;
				set;
			}
		}

		public class VariantItem
		{
			public List<AttributeValues> VariantItems
			{
				get;
				set;
			}

			public string Sku
			{
				get;
				set;
			}

			public int? StockQuantity
			{
				get;
				set;
			}

			public decimal? OverriddenPrice
			{
				get;
				set;
			}

			public bool? AllowOutOfStock
			{
				get;
				set;
			}

			public bool IsSelected
			{
				get;
				set;
			}

			public int? AssociatedPictureId
			{
				get;
				set;
			}

			public VariantItem()
			{
				VariantItems = new List<AttributeValues>();
				IsSelected = true;
			}
		}

		public class FilterValues
		{
			public string FilterId
			{
				get;
				set;
			}

			public string FilterName
			{
				get;
				set;
			}

			public string FilterValue
			{
				get;
				set;
			}
		}

		public class ImageItem : ICloneable
		{
			public int ImageId
			{
				get;
				set;
			}

			public int ImageDBId
			{
				get;
				set;
			}

			public string ImageUrl
			{
				get;
				set;
			}

			public string ImageExtension
			{
				get
				{
					try
					{
						return (!IsBinary && !string.IsNullOrEmpty(ImageUrl)) ? Path.GetExtension(ImageUrl).Replace(".", "") : "jpg";
					}
					catch
					{
						return "jpg";
					}
				}
			}

			public bool IsBinary
			{
				get;
				set;
			}

			public string ImageSeo
			{
				get;
				set;
			}

			public bool IsSelected
			{
				get;
				set;
			}

			public bool IsArray
			{
				get;
				set;
			}

			public object Clone()
			{
				return MemberwiseClone();
			}
		}

		public class CategoryItem
		{
			public int CatId
			{
				get;
				set;
			}

			public string Category
			{
				get;
				set;
			}

			public int? ParentCatId
			{
				get;
				set;
			}

			public int CatDbId
			{
				get;
				set;
			}

			public bool Splitted
			{
				get;
				set;
			}
		}

		public class CustomerRuleItem
		{
			public int CustomerRuleId
			{
				get;
				set;
			}

			public string CustomerRule
			{
				get;
				set;
			}
		}

		public class ProductTagItem
		{
			public int ProductTagId
			{
				get;
				set;
			}

			public string ProductTagName
			{
				get;
				set;
			}
		}

		public class CrossSellItem
		{
			public int ProductId
			{
				get;
				set;
			}

			public string ProductSKU
			{
				get;
				set;
			}
		}

		public class RelatedProductItem
		{
			public int ProductId
			{
				get;
				set;
			}

			public string ProductSKU
			{
				get;
				set;
			}
		}

		private SourceData _sourceData = null;

		public string ProdId
		{
			get;
			set;
		}

		public string ProdGroupBy
		{
			get;
			set;
		}

		public string ProdSku
		{
			get;
			set;
		}

		public string ProdName
		{
			get;
			set;
		}

		public string ProdShortDesc
		{
			get;
			set;
		}

		public string ProdFullDesc
		{
			get;
			set;
		}

		public string ProdManPartNum
		{
			get;
			set;
		}

		public decimal? ProdCost
		{
			get;
			set;
		}

		public decimal? ProdPrice
		{
			get;
			set;
		}

		public decimal? ProdOldPrice
		{
			get;
			set;
		}

		public int? ProdStock
		{
			get;
			set;
		}

		public string SeoMetaKey
		{
			get;
			set;
		}

		public string SeoMetaDesc
		{
			get;
			set;
		}

		public string SeoMetaTitle
		{
			get;
			set;
		}

		public string SearchPage
		{
			get;
			set;
		}

		public string Manufacturer
		{
			get;
			set;
		}

		public string Vendor
		{
			get;
			set;
		}

		public string TaxCategory
		{
			get;
			set;
		}

		public bool TaxExempt
		{
			get;
			set;
		}

		public string Gtin
		{
			get;
			set;
		}

		public decimal? Weight
		{
			get;
			set;
		}

		public decimal? Length
		{
			get;
			set;
		}

		public decimal? Width
		{
			get;
			set;
		}

		public decimal? Height
		{
			get;
			set;
		}

		public bool Filtered
		{
			get;
			set;
		}

		public int? RelatedTaskId
		{
			get;
			set;
		}

		public string RelatedSourceAddress
		{
			get;
			set;
		}

		public string TierPricing_ACCID
		{
			get;
			set;
		}

		public bool ProductStock_AllowBackInStockSubscriptions
		{
			get;
			set;
		}

		public bool ProductStock_DisableBuyButton
		{
			get;
			set;
		}

		public bool ProductPrice_BasepriceEnabled
		{
			get;
			set;
		}

		public decimal ProductPrice_BasepriceAmount
		{
			get;
			set;
		}

		public string ProductPrice_BasepriceUnit
		{
			get;
			set;
		}

		public decimal ProductPrice_BasepriceBaseAmount
		{
			get;
			set;
		}

		public string ProductPrice_BasepriceBaseUnit
		{
			get;
			set;
		}

		public List<WishListItem> WishList
		{
			get;
			set;
		}

		public List<AttributeValues> Attributes
		{
			get;
			set;
		}

		public List<AttributeValues> SpecAttributes
		{
			get;
			set;
		}

		public List<AttributeValues> AddressCustomAttributes
		{
			get;
			set;
		}

		public List<AttributeValues> CustomerCustomAttributes
		{
			get;
			set;
		}

		public List<VariantItem> Variants
		{
			get;
			set;
		}

		public List<FilterValues> Filters
		{
			get;
			set;
		}

		public List<ImageItem> Images
		{
			get;
			set;
		}

		public List<CategoryItem> Categories
		{
			get;
			set;
		}

		public List<CustomerRuleItem> CustomerRoles
		{
			get;
			set;
		}

		public List<ProductTagItem> ProductTags
		{
			get;
			set;
		}

		public List<CrossSellItem> CrossSellItems
		{
			get;
			set;
		}

		public List<RelatedProductItem> RelatedProductItems
		{
			get;
			set;
		}

		public CustomerItem User
		{
			get;
			set;
		}

		public List<TierPricingItem> TierPricingItems
		{
			get;
			set;
		}

		public int ProdSettingsMinStockQty
		{
			get;
			set;
		}

		public int ProdSettingsNotifyQty
		{
			get;
			set;
		}

		public int ProdSettingsMinCartQty
		{
			get;
			set;
		}

		public int ProdSettingsMaxCartQty
		{
			get;
			set;
		}

		public int ProdSettingsAllowedQty
		{
			get;
			set;
		}

		public bool ProdSettingsShippingEnabled
		{
			get;
			set;
		}

		public bool ProdSettingsFreeShipping
		{
			get;
			set;
		}

		public bool ProdSettingsVisibleIndividually
		{
			get;
			set;
		}

		public bool ProdSettingsIsDeleted
		{
			get;
			set;
		}

		public bool ProdSettingsShipSeparately
		{
			get;
			set;
		}

		public decimal? ProdSettingsShippingCharge
		{
			get;
			set;
		}

		public string ProdSettingsDeliveryDate
		{
			get;
			set;
		}

		public bool Unavailable
		{
			get;
			set;
		}

		public SourceItem()
		{
			Categories = new List<CategoryItem>();
			Attributes = new List<AttributeValues>();
			WishList = new List<WishListItem>();
		}

		public SourceItem(XElement xElement, SourceMapping sourceMapping, SourceData sourceData, TaskMapping taskMapping)
		{
			_sourceData = sourceData;
			Variants = new List<VariantItem>();
			ProductTags = new List<ProductTagItem>();
			CrossSellItems = new List<CrossSellItem>();
			RelatedProductItems = new List<RelatedProductItem>();
			if (sourceMapping.EntityType == EntityType.Products)
			{
				ProdId = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.ProdIdMap, null));
				ProdGroupBy = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.ProdGroupByMap, null));
				ProdSku = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.ProdSkuMap, sourceMapping.ProdSkuVal));
				ProdName = GlobalClass.ObjectToString2(GetElementValue(xElement, sourceMapping.ProdNameMap, sourceMapping.ProdNameVal), taskMapping?.UseTitleCase ?? false);
				ProdShortDesc = GlobalClass.ObjectToString2(GetElementValue(xElement, sourceMapping.ProdShortDescMap, sourceMapping.ProdShortDescVal), taskMapping?.UseTitleCase ?? false);
				ProdFullDesc = GlobalClass.ObjectToString2(GetElementValue(xElement, sourceMapping.ProdFullDescMap, sourceMapping.ProdFullDescVal), taskMapping?.UseTitleCase ?? false);
				ProdManPartNum = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.ProdManPartNumMap, sourceMapping.ProdManPartNumVal));
				ProdCost = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceMapping.ProdCostMap, sourceMapping.ProdCostVal));
				ProdPrice = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceMapping.ProdPriceMap, sourceMapping.ProdPriceVal));
				ProdOldPrice = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceMapping.ProdOldPriceMap, sourceMapping.ProdOldPriceVal));
				ProdStock = GlobalClass.StringToInteger(GetElementValue(xElement, sourceMapping.ProdStockMap, sourceMapping.ProdStockVal));
				SeoMetaKey = GlobalClass.ObjectToString2(GetElementValue(xElement, sourceMapping.SeoMetaKeyMap, sourceMapping.SeoMetaKeyVal), taskMapping?.UseTitleCase ?? false);
				SeoMetaDesc = GlobalClass.ObjectToString2(GetElementValue(xElement, sourceMapping.SeoMetaDescMap, sourceMapping.SeoMetaDescVal), taskMapping?.UseTitleCase ?? false);
				SeoMetaTitle = GlobalClass.ObjectToString2(GetElementValue(xElement, sourceMapping.SeoMetaTitleMap, sourceMapping.SeoMetaTitleVal), taskMapping?.UseTitleCase ?? false);
				SearchPage = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.SeoPageMap, sourceMapping.SeoPageVal));
				Manufacturer = GlobalClass.ObjectToString2(GetElementValue(xElement, sourceMapping.ManufactMap, sourceMapping.ManufactVal), taskMapping?.UseTitleCase ?? false);
				Vendor = GlobalClass.ObjectToString2(GetElementValue(xElement, sourceMapping.VendorMap, sourceMapping.VendorVal), taskMapping?.UseTitleCase ?? false);
				TaxCategory = GlobalClass.ObjectToString2(GetElementValue(xElement, sourceMapping.TaxCategoryMap, sourceMapping.TaxCategoryVal), taskMapping?.UseTitleCase ?? false);
				Gtin = GlobalClass.ObjectToString2(GetElementValue(xElement, sourceMapping.GtinMap, sourceMapping.GtinVal), taskMapping?.UseTitleCase ?? false);
				Weight = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceMapping.WeightMap, sourceMapping.WeightVal));
				Length = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceMapping.LengthMap, sourceMapping.LengthVal));
				Width = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceMapping.WidthMap, sourceMapping.WidthVal));
				Height = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceMapping.HeightMap, sourceMapping.HeightVal));
				GetProductInfo(sourceMapping, taskMapping, xElement, null);
				GetProductPrice(sourceMapping, taskMapping, xElement, null);
				ProdSettingsAllowedQty = GlobalClass.StringToInteger(GetElementValue(xElement, sourceMapping.ProdSettingsAllowedQtyMap, sourceMapping.ProdSettingsAllowedQtyVal), -1);
				ProdSettingsMinCartQty = GlobalClass.StringToInteger(GetElementValue(xElement, sourceMapping.ProdSettingsMinCartQtyMap, sourceMapping.ProdSettingsMinCartQtyVal));
				ProdSettingsMaxCartQty = GlobalClass.StringToInteger(GetElementValue(xElement, sourceMapping.ProdSettingsMaxCartQtyMap, sourceMapping.ProdSettingsMaxCartQtyVal));
				ProdSettingsMinStockQty = GlobalClass.StringToInteger(GetElementValue(xElement, sourceMapping.ProdSettingsMinStockQtyMap, sourceMapping.ProdSettingsMinStockQtyVal));
				ProdSettingsNotifyQty = GlobalClass.StringToInteger(GetElementValue(xElement, sourceMapping.ProdSettingsNotifyQtyMap, sourceMapping.ProdSettingsNotifyQtyVal));
				ProdSettingsShippingEnabled = GlobalClass.ObjectToBool(GetElementValue(xElement, sourceMapping.ProdSettingsShippingEnabledMap, sourceMapping.ProdSettingsShippingEnabledVal));
				ProdSettingsFreeShipping = GlobalClass.ObjectToBool(GetElementValue(xElement, sourceMapping.ProdSettingsFreeShippingMap, sourceMapping.ProdSettingsFreeShippingMap));
				ProdSettingsShipSeparately = GlobalClass.ObjectToBool(GetElementValue(xElement, sourceMapping.ProdSettingsShipSeparatelyMap, sourceMapping.ProdSettingsShipSeparatelyVal));
				ProdSettingsShippingCharge = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceMapping.ProdSettingsShippingChargeMap, sourceMapping.ProdSettingsShippingChargeVal));
				ProdSettingsDeliveryDate = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.ProdSettingsDeliveryDateMap, sourceMapping.ProdSettingsDeliveryDateVal));
				ProdSettingsVisibleIndividually = GlobalClass.ObjectToBool(GetElementValue(xElement, sourceMapping.ProdSettingsVisibleIndividuallyMap, sourceMapping.ProdSettingsVisibleIndividuallyVal));
				ProdSettingsIsDeleted = GlobalClass.ObjectToBool(GetElementValue(xElement, sourceMapping.ProdSettingsIsDeletedMap, sourceMapping.ProdSettingsIsDeletedVal));
				if (sourceMapping.RelatedTaskId > 0)
				{
					RelatedTaskId = GlobalClass.StringToInteger(GetElementValue(xElement, null, sourceMapping.RelatedTaskId.Value.ToString()));
					RelatedSourceAddress = GlobalClass.ObjectToString(GetElementValue(xElement, null, sourceMapping.RelatedSourceAddress));
				}
				Filtered = true;
				List<FilterValues> list = new List<FilterValues>();
				if (!string.IsNullOrEmpty(sourceMapping.FilterMap))
				{
					list.Add(new FilterValues
					{
						FilterId = "0",
						FilterName = GlobalClass.ObjectToString(sourceMapping.FilterName),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.FilterMap, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter1Map))
				{
					list.Add(new FilterValues
					{
						FilterId = "1",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter1Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.Filter1Map, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter2Map))
				{
					list.Add(new FilterValues
					{
						FilterId = "2",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter2Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.Filter2Map, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter3Map))
				{
					list.Add(new FilterValues
					{
						FilterId = "3",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter3Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.Filter3Map, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter4Map))
				{
					list.Add(new FilterValues
					{
						FilterId = "4",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter4Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.Filter4Map, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter5Map))
				{
					list.Add(new FilterValues
					{
						FilterId = "5",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter5Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.Filter5Map, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter6Map))
				{
					list.Add(new FilterValues
					{
						FilterId = "6",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter6Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.Filter6Map, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter7Map))
				{
					list.Add(new FilterValues
					{
						FilterId = "7",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter7Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.Filter7Map, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter8Map))
				{
					list.Add(new FilterValues
					{
						FilterId = "8",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter8Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.Filter8Map, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter9Map))
				{
					list.Add(new FilterValues
					{
						FilterId = "9",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter9Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.Filter9Map, null))
					});
				}
				Filters = list;
				List<CategoryItem> list2 = new List<CategoryItem>();
				List<object> elements = GetElements(xElement, sourceMapping.CatMap);
				if (elements != null)
				{
					foreach (object item3 in elements)
					{
						if (item3 != null)
						{
							try
							{
								string catValues2 = ((XElement)item3).Value;
								catValues2 = GlobalClass.ObjectToString(GetElementValue(catValues2, sourceMapping.CatVal));
								if (sourceMapping.CatDelimeter.HasValue && catValues2.IndexOf(sourceMapping.CatDelimeter.Value) > -1)
								{
									List<string> list3 = catValues2.Split(sourceMapping.CatDelimeter.Value).ToList();
									foreach (string cat12 in list3)
									{
										if (!string.IsNullOrEmpty(cat12) && !list2.Exists((CategoryItem a) => a.Category.Equals(cat12.Trim())))
										{
											list2.Add(new CategoryItem
											{
												CatId = 0,
												Category = cat12.Trim(),
												Splitted = true
											});
										}
									}
								}
								else if (!string.IsNullOrEmpty(catValues2) && !list2.Exists((CategoryItem a) => a.Category.Equals(catValues2)))
								{
									list2.Add(new CategoryItem
									{
										CatId = 0,
										Category = catValues2
									});
								}
							}
							catch
							{
								string catValues = ((XAttribute)item3).Value;
								catValues = GlobalClass.ObjectToString(GetElementValue(catValues, sourceMapping.CatVal));
								if (sourceMapping.CatDelimeter.HasValue && catValues.IndexOf(sourceMapping.CatDelimeter.Value) > -1)
								{
									List<string> list4 = catValues.Split(sourceMapping.CatDelimeter.Value).ToList();
									foreach (string cat11 in list4)
									{
										if (!string.IsNullOrEmpty(cat11) && !list2.Exists((CategoryItem a) => a.Category.Equals(cat11.Trim())))
										{
											list2.Add(new CategoryItem
											{
												CatId = 0,
												Category = cat11.Trim(),
												Splitted = true
											});
										}
									}
								}
								else if (!string.IsNullOrEmpty(catValues) && !list2.Exists((CategoryItem a) => a.Category.Equals(catValues)))
								{
									list2.Add(new CategoryItem
									{
										CatId = 0,
										Category = catValues
									});
								}
							}
						}
					}
				}
				else if (!string.IsNullOrEmpty(sourceMapping.CatVal))
				{
					string cat10 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.CatMap, sourceMapping.CatVal));
					if (!string.IsNullOrEmpty(cat10) && !list2.Exists((CategoryItem a) => a.Category.Equals(cat10.Trim())))
					{
						list2.Add(new CategoryItem
						{
							CatId = 0,
							Category = cat10.Trim()
						});
					}
				}
				if (!string.IsNullOrEmpty(sourceMapping.Cat1Map) || !string.IsNullOrEmpty(sourceMapping.Cat1Val))
				{
					string cat9 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.Cat1Map, sourceMapping.Cat1Val));
					if (!string.IsNullOrEmpty(cat9) && !list2.Exists((CategoryItem a) => a.Category.Equals(cat9.Trim())))
					{
						list2.Add(new CategoryItem
						{
							CatId = 1,
							Category = cat9.Trim(),
							ParentCatId = sourceMapping.Cat1Parent
						});
					}
				}
				if (!string.IsNullOrEmpty(sourceMapping.Cat2Map) || !string.IsNullOrEmpty(sourceMapping.Cat2Val))
				{
					string cat8 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.Cat2Map, sourceMapping.Cat2Val));
					if (!string.IsNullOrEmpty(cat8) && !list2.Exists((CategoryItem a) => a.Category.Equals(cat8.Trim())))
					{
						list2.Add(new CategoryItem
						{
							CatId = 2,
							Category = cat8.Trim(),
							ParentCatId = sourceMapping.Cat2Parent
						});
					}
				}
				if (!string.IsNullOrEmpty(sourceMapping.Cat3Map) || !string.IsNullOrEmpty(sourceMapping.Cat3Val))
				{
					string cat7 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.Cat3Map, sourceMapping.Cat3Val));
					if (!string.IsNullOrEmpty(cat7) && !list2.Exists((CategoryItem a) => a.Category.Equals(cat7.Trim())))
					{
						list2.Add(new CategoryItem
						{
							CatId = 3,
							Category = cat7.Trim(),
							ParentCatId = sourceMapping.Cat3Parent
						});
					}
				}
				if (!string.IsNullOrEmpty(sourceMapping.Cat4Map) || !string.IsNullOrEmpty(sourceMapping.Cat4Val))
				{
					string cat6 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.Cat4Map, sourceMapping.Cat4Val));
					if (!string.IsNullOrEmpty(cat6) && !list2.Exists((CategoryItem a) => a.Category.Equals(cat6.Trim())))
					{
						list2.Add(new CategoryItem
						{
							CatId = 4,
							Category = cat6.Trim(),
							ParentCatId = sourceMapping.Cat4Parent
						});
					}
				}
				Categories = list2;
				Images = new List<ImageItem>();
				if (sourceMapping.SourceProdPicturesMapping?.Items != null && sourceMapping.SourceProdPicturesMapping.Items.Any())
				{
					SourceItemMapping sourceItemMapping = sourceMapping.SourceProdPicturesMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1);
					if (sourceItemMapping != null)
					{
						List<object> elements2 = GetElements(xElement, sourceItemMapping.FieldMappingForXml);
						if (elements2 != null)
						{
							foreach (XElement item4 in elements2)
							{
								GetProductPictures(sourceMapping, taskMapping, item4, null);
							}
						}
					}
					GetProductPictures(sourceMapping, taskMapping, xElement, null);
				}
				Images = CheckForArrayImage(Images);
			}
			if (sourceMapping.EntityType == EntityType.Users || sourceMapping.EntityType == EntityType.Products)
			{
				List<CustomerRuleItem> list5 = new List<CustomerRuleItem>();
				List<object> elements3 = GetElements(xElement, sourceMapping.CustomerRoleMap);
				if (elements3 != null)
				{
					foreach (object item5 in elements3)
					{
						if (item5 != null)
						{
							try
							{
								string customerRolesValues2 = ((XElement)item5).Value;
								customerRolesValues2 = GlobalClass.ObjectToString(GetElementValue(customerRolesValues2, sourceMapping.CustomerRoleMap));
								if (sourceMapping.CustomerRoleDelimeter.HasValue && customerRolesValues2.IndexOf(sourceMapping.CustomerRoleDelimeter.Value) > -1)
								{
									List<string> list6 = customerRolesValues2.Split(sourceMapping.CustomerRoleDelimeter.Value).ToList();
									foreach (string item2 in list6)
									{
										if (!string.IsNullOrEmpty(item2) && !list5.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(item2.Trim())))
										{
											list5.Add(new CustomerRuleItem
											{
												CustomerRuleId = 0,
												CustomerRule = item2.Trim()
											});
										}
									}
								}
								else if (!string.IsNullOrEmpty(customerRolesValues2) && !list5.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(customerRolesValues2)))
								{
									list5.Add(new CustomerRuleItem
									{
										CustomerRuleId = 0,
										CustomerRule = customerRolesValues2
									});
								}
							}
							catch
							{
								string customerRolesValues = ((XAttribute)item5).Value;
								customerRolesValues = GlobalClass.ObjectToString(GetElementValue(customerRolesValues, sourceMapping.CustomerRoleMap));
								if (sourceMapping.CustomerRoleDelimeter.HasValue && customerRolesValues.IndexOf(sourceMapping.CustomerRoleDelimeter.Value) > -1)
								{
									List<string> list7 = customerRolesValues.Split(sourceMapping.CustomerRoleDelimeter.Value).ToList();
									foreach (string item in list7)
									{
										if (!string.IsNullOrEmpty(item) && !list5.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(item.Trim())))
										{
											list5.Add(new CustomerRuleItem
											{
												CustomerRuleId = 0,
												CustomerRule = item.Trim()
											});
										}
									}
								}
								else if (!string.IsNullOrEmpty(customerRolesValues) && !list5.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(customerRolesValues)))
								{
									list5.Add(new CustomerRuleItem
									{
										CustomerRuleId = 0,
										CustomerRule = customerRolesValues
									});
								}
							}
						}
					}
				}
				else if (!string.IsNullOrEmpty(sourceMapping.CustomerRoleVal))
				{
					string cat5 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.CustomerRoleMap, sourceMapping.CustomerRoleVal));
					if (!string.IsNullOrEmpty(cat5) && !list5.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(cat5.Trim())))
					{
						list5.Add(new CustomerRuleItem
						{
							CustomerRuleId = 0,
							CustomerRule = cat5.Trim()
						});
					}
				}
				if (!string.IsNullOrEmpty(sourceMapping.CustomerRole1Map) || !string.IsNullOrEmpty(sourceMapping.CustomerRole1Val))
				{
					string cat4 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.CustomerRole1Map, sourceMapping.CustomerRole1Val));
					if (!string.IsNullOrEmpty(cat4) && !list5.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(cat4.Trim())))
					{
						list5.Add(new CustomerRuleItem
						{
							CustomerRuleId = 1,
							CustomerRule = cat4.Trim()
						});
					}
				}
				if (!string.IsNullOrEmpty(sourceMapping.CustomerRole2Map) || !string.IsNullOrEmpty(sourceMapping.CustomerRole2Val))
				{
					string cat3 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.CustomerRole2Map, sourceMapping.CustomerRole2Val));
					if (!string.IsNullOrEmpty(cat3) && !list5.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(cat3.Trim())))
					{
						list5.Add(new CustomerRuleItem
						{
							CustomerRuleId = 2,
							CustomerRule = cat3.Trim()
						});
					}
				}
				if (!string.IsNullOrEmpty(sourceMapping.CustomerRole3Map) || !string.IsNullOrEmpty(sourceMapping.CustomerRole3Val))
				{
					string cat2 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.CustomerRole3Map, sourceMapping.CustomerRole3Val));
					if (!string.IsNullOrEmpty(cat2) && !list5.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(cat2.Trim())))
					{
						list5.Add(new CustomerRuleItem
						{
							CustomerRuleId = 3,
							CustomerRule = cat2.Trim()
						});
					}
				}
				if (!string.IsNullOrEmpty(sourceMapping.CustomerRole4Map) || !string.IsNullOrEmpty(sourceMapping.CustomerRole4Val))
				{
					string cat = GlobalClass.ObjectToString(GetElementValue(xElement, sourceMapping.CustomerRole4Map, sourceMapping.CustomerRole4Val));
					if (!string.IsNullOrEmpty(cat) && !list5.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(cat.Trim())))
					{
						list5.Add(new CustomerRuleItem
						{
							CustomerRuleId = 4,
							CustomerRule = cat.Trim()
						});
					}
				}
				CustomerRoles = list5;
				TierPricingItems = new List<TierPricingItem>();
				if (sourceMapping.SourceTierPricingMapping?.SourceTierPricingMappingItems != null)
				{
					List<int> list8 = new List<int>();
					TierPricingItem tierPricingItem = new TierPricingItem();
					foreach (SourceItemMapping sourceTierPricingItem in sourceMapping.SourceTierPricingMapping?.SourceTierPricingMappingItems.Where((SourceItemMapping x) => x.MappingItemType > -1))
					{
						tierPricingItem = TierPricingItems.FirstOrDefault((TierPricingItem x) => x.Id == sourceTierPricingItem.Id);
						TaskItemMapping taskItemMapping = taskMapping?.TaskTierPricingMapping?.TaskTierPricingMappingItems.FirstOrDefault((TaskItemMapping x) => x.Id == sourceTierPricingItem.Id && x.MappingItemType == sourceTierPricingItem.MappingItemType);
						if (taskItemMapping?.FieldImport ?? true)
						{
							if (tierPricingItem == null)
							{
								tierPricingItem = new TierPricingItem
								{
									Id = sourceTierPricingItem.Id
								};
								TierPricingItems.Add(tierPricingItem);
							}
							if (sourceTierPricingItem.MappingItemType == 0)
							{
								tierPricingItem.Quantity = GlobalClass.StringToInteger(GetElementValue(xElement, sourceTierPricingItem.FieldMappingForXml, sourceTierPricingItem.FieldRule));
							}
							if (sourceTierPricingItem.MappingItemType == 1)
							{
								tierPricingItem.Price = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceTierPricingItem.FieldMappingForXml, sourceTierPricingItem.FieldRule));
								tierPricingItem.AddPrice = GlobalClass.StringToDecimal(taskItemMapping?.FieldRule2 ?? 0m);
								tierPricingItem.AddPercent = GlobalClass.StringToInteger(taskItemMapping?.FieldRule1 ?? 0);
							}
							if (sourceTierPricingItem.MappingItemType == 2)
							{
								tierPricingItem.CustomerRoles = GlobalClass.ObjectToString(GetElementValue(xElement, sourceTierPricingItem.FieldMappingForXml, sourceTierPricingItem.FieldRule))?.Split(',').ToList();
							}
							if (sourceTierPricingItem.MappingItemType == 13)
							{
								tierPricingItem.StartDateTime = GlobalClass.ObjectToString(GetElementValue(xElement, sourceTierPricingItem.FieldMappingForXml, sourceTierPricingItem.FieldRule));
							}
							if (sourceTierPricingItem.MappingItemType == 14)
							{
								tierPricingItem.EndDateTime = GlobalClass.ObjectToString(GetElementValue(xElement, sourceTierPricingItem.FieldMappingForXml, sourceTierPricingItem.FieldRule));
							}
						}
					}
				}
				if (taskMapping?.TaskTierPricingMapping.TaskTierPricingMappingItems != null)
				{
					TierPricingItem tierPricingItem2 = new TierPricingItem();
					foreach (TaskItemMapping taskTierPricingItem in taskMapping.TaskTierPricingMapping?.TaskTierPricingMappingItems.Where((TaskItemMapping x) => !x.MappedToSource && x.FieldImport))
					{
						tierPricingItem2 = TierPricingItems.FirstOrDefault((TierPricingItem x) => x.Id == taskTierPricingItem.Id);
						if (tierPricingItem2 == null)
						{
							tierPricingItem2 = new TierPricingItem
							{
								Id = taskTierPricingItem.Id
							};
							TierPricingItems.Add(tierPricingItem2);
						}
						if (taskTierPricingItem.MappingItemType == 0)
						{
							tierPricingItem2.Quantity = GlobalClass.StringToInteger(GetElementValue(xElement, null, taskTierPricingItem.FieldRule));
						}
						if (taskTierPricingItem.MappingItemType == 1)
						{
							tierPricingItem2.Price = GlobalClass.StringToDecimal(GetElementValue(xElement, null, taskTierPricingItem.FieldRule));
							tierPricingItem2.AddPrice = GlobalClass.StringToDecimal(taskTierPricingItem?.FieldRule2 ?? 0m);
							tierPricingItem2.AddPercent = GlobalClass.StringToInteger(taskTierPricingItem?.FieldRule1 ?? 0);
						}
						if (taskTierPricingItem.MappingItemType == 2)
						{
							tierPricingItem2.CustomerRoles = GlobalClass.ObjectToString(GetElementValue(xElement, null, taskTierPricingItem.FieldRule))?.Split(',').ToList();
						}
						if (taskTierPricingItem.MappingItemType == 13)
						{
							tierPricingItem2.StartDateTime = GlobalClass.ObjectToString(GetElementValue(xElement, null, taskTierPricingItem.FieldRule));
						}
						if (taskTierPricingItem.MappingItemType == 14)
						{
							tierPricingItem2.EndDateTime = GlobalClass.ObjectToString(GetElementValue(xElement, null, taskTierPricingItem.FieldRule));
						}
					}
				}
				Attributes = new List<AttributeValues>();
				if (sourceMapping.SourceProdAttMapping?.SourceProdAttMappingItems != null && sourceMapping.SourceProdAttMapping.SourceProdAttMappingItems.Any())
				{
					SourceItemMapping sourceItemMapping2 = sourceMapping.SourceProdAttMapping.SourceProdAttMappingItems.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1);
					if (sourceItemMapping2 != null)
					{
						List<object> elements4 = GetElements(xElement, sourceItemMapping2.FieldMappingForXml);
						if (elements4 != null)
						{
							foreach (XElement item6 in elements4)
							{
								GetAttributes(sourceMapping, taskMapping, item6, null);
							}
						}
					}
					GetAttributes(sourceMapping, taskMapping, xElement, null);
				}
				SpecAttributes = new List<AttributeValues>();
				if (sourceMapping.SourceProdSpecAttMapping?.SourceProdSpecAttMappingItems != null && sourceMapping.SourceProdSpecAttMapping.SourceProdSpecAttMappingItems.Any())
				{
					SourceItemMapping sourceItemMapping3 = sourceMapping.SourceProdSpecAttMapping.SourceProdSpecAttMappingItems.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1);
					if (sourceItemMapping3 != null)
					{
						List<object> elements5 = GetElements(xElement, sourceItemMapping3.FieldMappingForXml);
						if (elements5 != null)
						{
							foreach (XElement item7 in elements5)
							{
								GetSpecAttributes(sourceMapping, taskMapping, item7, null);
							}
						}
					}
					GetSpecAttributes(sourceMapping, taskMapping, xElement, null);
				}
			}
			if (sourceMapping.EntityType == EntityType.Users)
			{
				User = new CustomerItem();
				GetCustomer(sourceMapping, taskMapping, xElement, null);
				if (sourceMapping.SourceCustomerShippingAddressMapping?.Items != null && sourceMapping.SourceCustomerShippingAddressMapping.Items.Any())
				{
					SourceItemMapping sourceItemMapping4 = sourceMapping.SourceCustomerShippingAddressMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1);
					if (sourceItemMapping4 != null)
					{
						List<object> elements6 = GetElements(xElement, sourceItemMapping4.FieldMappingForXml);
						if (elements6 != null)
						{
							foreach (XElement item8 in elements6)
							{
								GetCustomerShippingAddress(sourceMapping, taskMapping, item8, null);
							}
						}
					}
					GetCustomerShippingAddress(sourceMapping, taskMapping, xElement, null);
				}
				if (sourceMapping.SourceCustomerBillingAddressMapping?.Items != null && sourceMapping.SourceCustomerBillingAddressMapping.Items.Any())
				{
					SourceItemMapping sourceItemMapping5 = sourceMapping.SourceCustomerBillingAddressMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1);
					if (sourceItemMapping5 != null)
					{
						List<object> elements7 = GetElements(xElement, sourceItemMapping5.FieldMappingForXml);
						if (elements7 != null)
						{
							foreach (XElement item9 in elements7)
							{
								GetCustomerBillingAddress(sourceMapping, taskMapping, item9, null);
							}
						}
					}
					GetCustomerBillingAddress(sourceMapping, taskMapping, xElement, null);
				}
				if (sourceMapping.SourceCustomerRoleMapping?.Items != null && sourceMapping.SourceCustomerRoleMapping.Items.Any())
				{
					SourceItemMapping sourceItemMapping6 = sourceMapping.SourceCustomerRoleMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1);
					if (sourceItemMapping6 != null)
					{
						List<object> elements8 = GetElements(xElement, sourceItemMapping6.FieldMappingForXml);
						if (elements8 != null)
						{
							foreach (XElement item10 in elements8)
							{
								GetCustomerRole(sourceMapping, taskMapping, item10, null);
							}
						}
					}
					GetCustomerRole(sourceMapping, taskMapping, xElement, null);
				}
				AddressCustomAttributes = new List<AttributeValues>();
				if (sourceMapping.SourceAddressCustomAttMapping?.Items != null && sourceMapping.SourceAddressCustomAttMapping.Items.Any())
				{
					SourceItemMapping sourceItemMapping7 = sourceMapping.SourceAddressCustomAttMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1);
					if (sourceItemMapping7 != null)
					{
						List<object> elements9 = GetElements(xElement, sourceItemMapping7.FieldMappingForXml);
						if (elements9 != null)
						{
							foreach (XElement item11 in elements9)
							{
								GetAddressCustomAttributes(sourceMapping, taskMapping, item11, null);
							}
						}
					}
					GetAddressCustomAttributes(sourceMapping, taskMapping, xElement, null);
				}
				CustomerCustomAttributes = new List<AttributeValues>();
				if (sourceMapping.SourceCustomerCustomAttMapping?.Items != null && sourceMapping.SourceCustomerCustomAttMapping.Items.Any())
				{
					SourceItemMapping sourceItemMapping8 = sourceMapping.SourceCustomerCustomAttMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1);
					if (sourceItemMapping8 != null)
					{
						List<object> elements10 = GetElements(xElement, sourceItemMapping8.FieldMappingForXml);
						if (elements10 != null)
						{
							foreach (XElement item12 in elements10)
							{
								GetCustomerCustomAttributes(sourceMapping, taskMapping, item12, null);
							}
						}
					}
					GetCustomerCustomAttributes(sourceMapping, taskMapping, xElement, null);
				}
				GetCustomerOthers(sourceMapping, taskMapping, xElement, null);
			}
			if (sourceMapping.EntityType != EntityType.WishList)
			{
				return;
			}
			WishList = new List<WishListItem>();
			if (sourceMapping.SourceWishListMapping?.Items == null || !sourceMapping.SourceWishListMapping.Items.Any())
			{
				return;
			}
			SourceItemMapping sourceItemMapping9 = sourceMapping.SourceWishListMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1);
			if (sourceItemMapping9 != null)
			{
				List<object> elements11 = GetElements(xElement, sourceItemMapping9.FieldMappingForXml);
				if (elements11 != null)
				{
					foreach (XElement item13 in elements11)
					{
						GetWishList(sourceMapping, taskMapping, item13, null);
					}
				}
			}
			GetWishList(sourceMapping, taskMapping, xElement, null);
		}

		public SourceItem(DataRow dataRow, SourceMapping sourceMapping, SourceData sourceData, TaskMapping taskMapping)
		{
			_sourceData = sourceData;
			Variants = new List<VariantItem>();
			ProductTags = new List<ProductTagItem>();
			CrossSellItems = new List<CrossSellItem>();
			RelatedProductItems = new List<RelatedProductItem>();
			if (sourceMapping.EntityType == EntityType.Products)
			{
				ProdId = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.ProdIdMap, null));
				ProdGroupBy = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.ProdGroupByMap, null));
				ProdSku = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.ProdSkuMap, sourceMapping.ProdSkuVal));
				ProdName = GlobalClass.ObjectToString2(GetElementValue(dataRow, sourceMapping.ProdNameMap, sourceMapping.ProdNameVal), taskMapping?.UseTitleCase ?? false);
				ProdShortDesc = GlobalClass.ObjectToString2(GetElementValue(dataRow, sourceMapping.ProdShortDescMap, sourceMapping.ProdShortDescVal), taskMapping?.UseTitleCase ?? false);
				ProdFullDesc = GlobalClass.ObjectToString2(GetElementValue(dataRow, sourceMapping.ProdFullDescMap, sourceMapping.ProdFullDescVal), taskMapping?.UseTitleCase ?? false);
				ProdManPartNum = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.ProdManPartNumMap, sourceMapping.ProdManPartNumVal));
				ProdCost = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceMapping.ProdCostMap, sourceMapping.ProdCostVal));
				ProdPrice = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceMapping.ProdPriceMap, sourceMapping.ProdPriceVal));
				ProdOldPrice = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceMapping.ProdOldPriceMap, sourceMapping.ProdOldPriceVal));
				ProdStock = GlobalClass.StringToInteger(GetElementValue(dataRow, sourceMapping.ProdStockMap, sourceMapping.ProdStockVal));
				SeoMetaKey = GlobalClass.ObjectToString2(GetElementValue(dataRow, sourceMapping.SeoMetaKeyMap, sourceMapping.SeoMetaKeyVal), taskMapping?.UseTitleCase ?? false);
				SeoMetaDesc = GlobalClass.ObjectToString2(GetElementValue(dataRow, sourceMapping.SeoMetaDescMap, sourceMapping.SeoMetaDescVal), taskMapping?.UseTitleCase ?? false);
				SeoMetaTitle = GlobalClass.ObjectToString2(GetElementValue(dataRow, sourceMapping.SeoMetaTitleMap, sourceMapping.SeoMetaTitleVal), taskMapping?.UseTitleCase ?? false);
				SearchPage = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.SeoPageMap, sourceMapping.SeoPageVal));
				Manufacturer = GlobalClass.ObjectToString2(GetElementValue(dataRow, sourceMapping.ManufactMap, sourceMapping.ManufactVal), taskMapping?.UseTitleCase ?? false);
				Vendor = GlobalClass.ObjectToString2(GetElementValue(dataRow, sourceMapping.VendorMap, sourceMapping.VendorVal), taskMapping?.UseTitleCase ?? false);
				TaxCategory = GlobalClass.ObjectToString2(GetElementValue(dataRow, sourceMapping.TaxCategoryMap, sourceMapping.TaxCategoryVal), taskMapping?.UseTitleCase ?? false);
				Gtin = GlobalClass.ObjectToString2(GetElementValue(dataRow, sourceMapping.GtinMap, sourceMapping.GtinVal), taskMapping?.UseTitleCase ?? false);
				Weight = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceMapping.WeightMap, sourceMapping.WeightVal));
				Length = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceMapping.LengthMap, sourceMapping.LengthVal));
				Width = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceMapping.WidthMap, sourceMapping.WidthVal));
				Height = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceMapping.HeightMap, sourceMapping.HeightVal));
				GetProductInfo(sourceMapping, taskMapping, null, dataRow);
				GetProductPrice(sourceMapping, taskMapping, null, dataRow);
				ProdSettingsAllowedQty = GlobalClass.StringToInteger(GetElementValue(dataRow, sourceMapping.ProdSettingsAllowedQtyMap, sourceMapping.ProdSettingsAllowedQtyVal), -1);
				ProdSettingsMinCartQty = GlobalClass.StringToInteger(GetElementValue(dataRow, sourceMapping.ProdSettingsMinCartQtyMap, sourceMapping.ProdSettingsMinCartQtyVal));
				ProdSettingsMaxCartQty = GlobalClass.StringToInteger(GetElementValue(dataRow, sourceMapping.ProdSettingsMaxCartQtyMap, sourceMapping.ProdSettingsMaxCartQtyVal));
				ProdSettingsMinStockQty = GlobalClass.StringToInteger(GetElementValue(dataRow, sourceMapping.ProdSettingsMinStockQtyMap, sourceMapping.ProdSettingsMinStockQtyVal));
				ProdSettingsNotifyQty = GlobalClass.StringToInteger(GetElementValue(dataRow, sourceMapping.ProdSettingsNotifyQtyMap, sourceMapping.ProdSettingsNotifyQtyVal));
				ProdSettingsShippingEnabled = GlobalClass.ObjectToBool(GetElementValue(dataRow, sourceMapping.ProdSettingsShippingEnabledMap, sourceMapping.ProdSettingsShippingEnabledVal));
				ProdSettingsFreeShipping = GlobalClass.ObjectToBool(GetElementValue(dataRow, sourceMapping.ProdSettingsFreeShippingMap, sourceMapping.ProdSettingsFreeShippingVal));
				ProdSettingsShipSeparately = GlobalClass.ObjectToBool(GetElementValue(dataRow, sourceMapping.ProdSettingsShipSeparatelyMap, sourceMapping.ProdSettingsShipSeparatelyVal));
				ProdSettingsShippingCharge = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceMapping.ProdSettingsShippingChargeMap, sourceMapping.ProdSettingsShippingChargeVal));
				ProdSettingsDeliveryDate = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.ProdSettingsDeliveryDateMap, sourceMapping.ProdSettingsDeliveryDateVal));
				ProdSettingsVisibleIndividually = GlobalClass.ObjectToBool(GetElementValue(dataRow, sourceMapping.ProdSettingsVisibleIndividuallyMap, sourceMapping.ProdSettingsVisibleIndividuallyVal));
				ProdSettingsIsDeleted = GlobalClass.ObjectToBool(GetElementValue(dataRow, sourceMapping.ProdSettingsIsDeletedMap, sourceMapping.ProdSettingsIsDeletedVal));
				Filtered = false;
				List<FilterValues> list = new List<FilterValues>();
				if (!string.IsNullOrEmpty(sourceMapping.FilterMap) && sourceMapping.FilterMap != "-1")
				{
					list.Add(new FilterValues
					{
						FilterId = "0",
						FilterName = GlobalClass.ObjectToString(sourceMapping.FilterName),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.FilterMap, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter1Map) && sourceMapping.Filter1Map != "-1")
				{
					list.Add(new FilterValues
					{
						FilterId = "1",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter1Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.Filter1Map, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter2Map) && sourceMapping.Filter2Map != "-1")
				{
					list.Add(new FilterValues
					{
						FilterId = "2",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter2Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.Filter2Map, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter3Map) && sourceMapping.Filter3Map != "-1")
				{
					list.Add(new FilterValues
					{
						FilterId = "3",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter3Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.Filter3Map, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter4Map) && sourceMapping.Filter4Map != "-1")
				{
					list.Add(new FilterValues
					{
						FilterId = "4",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter4Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.Filter4Map, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter5Map) && sourceMapping.Filter5Map != "-1")
				{
					list.Add(new FilterValues
					{
						FilterId = "5",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter5Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.Filter5Map, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter6Map) && sourceMapping.Filter6Map != "-1")
				{
					list.Add(new FilterValues
					{
						FilterId = "6",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter6Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.Filter6Map, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter7Map) && sourceMapping.Filter7Map != "-1")
				{
					list.Add(new FilterValues
					{
						FilterId = "7",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter7Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.Filter7Map, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter8Map) && sourceMapping.Filter8Map != "-1")
				{
					list.Add(new FilterValues
					{
						FilterId = "8",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter8Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.Filter8Map, null))
					});
				}
				if (!string.IsNullOrEmpty(sourceMapping.Filter9Map) && sourceMapping.Filter9Map != "-1")
				{
					list.Add(new FilterValues
					{
						FilterId = "9",
						FilterName = GlobalClass.ObjectToString(sourceMapping.Filter9Name),
						FilterValue = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.Filter9Map, null))
					});
				}
				Filters = list;
				List<CategoryItem> list2 = new List<CategoryItem>();
				if (!string.IsNullOrEmpty(sourceMapping.CatMap) || !string.IsNullOrEmpty(sourceMapping.CatVal))
				{
					string cat10 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.CatMap, sourceMapping.CatVal));
					if (sourceMapping.CatDelimeter.HasValue && cat10.IndexOf(sourceMapping.CatDelimeter.Value) > -1)
					{
						List<string> list3 = cat10.Split(sourceMapping.CatDelimeter.Value).ToList();
						foreach (string catItem2 in list3)
						{
							if (!string.IsNullOrEmpty(catItem2) && !list2.Exists((CategoryItem a) => a.Category.Equals(catItem2.Trim())))
							{
								list2.Add(new CategoryItem
								{
									CatId = 0,
									Category = catItem2.Trim(),
									Splitted = true
								});
							}
						}
					}
					else if (!string.IsNullOrEmpty(cat10) && !list2.Exists((CategoryItem a) => a.Category.Equals(cat10.Trim())))
					{
						list2.Add(new CategoryItem
						{
							CatId = 0,
							Category = cat10.Trim()
						});
					}
				}
				if (!string.IsNullOrEmpty(sourceMapping.Cat1Map) || !string.IsNullOrEmpty(sourceMapping.Cat1Val))
				{
					string cat9 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.Cat1Map, sourceMapping.Cat1Val));
					if (!string.IsNullOrEmpty(cat9) && !list2.Exists((CategoryItem a) => a.Category.Equals(cat9.Trim())))
					{
						list2.Add(new CategoryItem
						{
							CatId = 1,
							Category = cat9.Trim(),
							ParentCatId = sourceMapping.Cat1Parent
						});
					}
				}
				if (!string.IsNullOrEmpty(sourceMapping.Cat2Map) || !string.IsNullOrEmpty(sourceMapping.Cat2Val))
				{
					string cat8 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.Cat2Map, sourceMapping.Cat2Val));
					if (!string.IsNullOrEmpty(cat8) && !list2.Exists((CategoryItem a) => a.Category.Equals(cat8.Trim())))
					{
						list2.Add(new CategoryItem
						{
							CatId = 2,
							Category = cat8.Trim(),
							ParentCatId = sourceMapping.Cat2Parent
						});
					}
				}
				if (!string.IsNullOrEmpty(sourceMapping.Cat3Map) || !string.IsNullOrEmpty(sourceMapping.Cat3Val))
				{
					string cat7 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.Cat3Map, sourceMapping.Cat3Val));
					if (!string.IsNullOrEmpty(cat7) && !list2.Exists((CategoryItem a) => a.Category.Equals(cat7.Trim())))
					{
						list2.Add(new CategoryItem
						{
							CatId = 3,
							Category = cat7.Trim(),
							ParentCatId = sourceMapping.Cat3Parent
						});
					}
				}
				if (!string.IsNullOrEmpty(sourceMapping.Cat4Map) || !string.IsNullOrEmpty(sourceMapping.Cat4Val))
				{
					string cat6 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.Cat4Map, sourceMapping.Cat4Val));
					if (!string.IsNullOrEmpty(cat6) && !list2.Exists((CategoryItem a) => a.Category.Equals(cat6.Trim())))
					{
						list2.Add(new CategoryItem
						{
							CatId = 4,
							Category = cat6.Trim(),
							ParentCatId = sourceMapping.Cat4Parent
						});
					}
				}
				Categories = list2;
				Attributes = new List<AttributeValues>();
				GetAttributes(sourceMapping, taskMapping, null, dataRow);
				SpecAttributes = new List<AttributeValues>();
				GetSpecAttributes(sourceMapping, taskMapping, null, dataRow);
				Images = new List<ImageItem>();
				GetProductPictures(sourceMapping, taskMapping, null, dataRow);
				Images = CheckForArrayImage(Images);
			}
			if (sourceMapping.EntityType == EntityType.Users || sourceMapping.EntityType == EntityType.Products)
			{
				List<CustomerRuleItem> list4 = new List<CustomerRuleItem>();
				if (!string.IsNullOrEmpty(sourceMapping.CustomerRoleMap) || !string.IsNullOrEmpty(sourceMapping.CustomerRoleVal))
				{
					string cat5 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.CustomerRoleMap, sourceMapping.CustomerRoleVal));
					if (sourceMapping.CustomerRoleDelimeter.HasValue && cat5.IndexOf(sourceMapping.CustomerRoleDelimeter.Value) > -1)
					{
						List<string> list5 = cat5.Split(sourceMapping.CustomerRoleDelimeter.Value).ToList();
						foreach (string catItem in list5)
						{
							if (!string.IsNullOrEmpty(catItem) && !list4.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(catItem.Trim())))
							{
								list4.Add(new CustomerRuleItem
								{
									CustomerRuleId = 0,
									CustomerRule = catItem.Trim()
								});
							}
						}
					}
					else if (!string.IsNullOrEmpty(cat5) && !list4.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(cat5.Trim())))
					{
						list4.Add(new CustomerRuleItem
						{
							CustomerRuleId = 0,
							CustomerRule = cat5.Trim()
						});
					}
				}
				if (!string.IsNullOrEmpty(sourceMapping.CustomerRole1Map) || !string.IsNullOrEmpty(sourceMapping.CustomerRole1Val))
				{
					string cat4 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.CustomerRole1Map, sourceMapping.CustomerRole1Val));
					if (!string.IsNullOrEmpty(cat4) && !list4.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(cat4.Trim())))
					{
						list4.Add(new CustomerRuleItem
						{
							CustomerRuleId = 1,
							CustomerRule = cat4.Trim()
						});
					}
				}
				if (!string.IsNullOrEmpty(sourceMapping.CustomerRole2Map) || !string.IsNullOrEmpty(sourceMapping.CustomerRole2Val))
				{
					string cat3 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.CustomerRole2Map, sourceMapping.CustomerRole2Val));
					if (!string.IsNullOrEmpty(cat3) && !list4.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(cat3.Trim())))
					{
						list4.Add(new CustomerRuleItem
						{
							CustomerRuleId = 2,
							CustomerRule = cat3.Trim()
						});
					}
				}
				if (!string.IsNullOrEmpty(sourceMapping.CustomerRole3Map) || !string.IsNullOrEmpty(sourceMapping.CustomerRole3Val))
				{
					string cat2 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.CustomerRole3Map, sourceMapping.CustomerRole3Val));
					if (!string.IsNullOrEmpty(cat2) && !list4.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(cat2.Trim())))
					{
						list4.Add(new CustomerRuleItem
						{
							CustomerRuleId = 3,
							CustomerRule = cat2.Trim()
						});
					}
				}
				if (!string.IsNullOrEmpty(sourceMapping.CustomerRole4Map) || !string.IsNullOrEmpty(sourceMapping.CustomerRole4Val))
				{
					string cat = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceMapping.CustomerRole4Map, sourceMapping.CustomerRole4Val));
					if (!string.IsNullOrEmpty(cat) && !list4.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(cat.Trim())))
					{
						list4.Add(new CustomerRuleItem
						{
							CustomerRuleId = 4,
							CustomerRule = cat.Trim()
						});
					}
				}
				CustomerRoles = list4;
				TierPricingItems = new List<TierPricingItem>();
				if (sourceMapping.SourceTierPricingMapping?.SourceTierPricingMappingItems != null)
				{
					List<int> list6 = new List<int>();
					TierPricingItem tierPricingItem = new TierPricingItem();
					foreach (SourceItemMapping sourceTierPricingItem in sourceMapping.SourceTierPricingMapping?.SourceTierPricingMappingItems.Where((SourceItemMapping x) => x.MappingItemType > -1))
					{
						tierPricingItem = TierPricingItems.FirstOrDefault((TierPricingItem x) => x.Id == sourceTierPricingItem.Id);
						TaskItemMapping taskItemMapping = taskMapping?.TaskTierPricingMapping?.TaskTierPricingMappingItems.FirstOrDefault((TaskItemMapping x) => x.Id == sourceTierPricingItem.Id && x.MappingItemType == sourceTierPricingItem.MappingItemType);
						if (taskItemMapping?.FieldImport ?? true)
						{
							if (tierPricingItem == null)
							{
								tierPricingItem = new TierPricingItem
								{
									Id = sourceTierPricingItem.Id
								};
								TierPricingItems.Add(tierPricingItem);
							}
							if (sourceTierPricingItem.MappingItemType == 0)
							{
								tierPricingItem.Quantity = GlobalClass.StringToInteger(GetElementValue(dataRow, sourceTierPricingItem.FieldMappingForIndex.ToString(), sourceTierPricingItem.FieldRule));
							}
							if (sourceTierPricingItem.MappingItemType == 1)
							{
								tierPricingItem.Price = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceTierPricingItem.FieldMappingForIndex.ToString(), sourceTierPricingItem.FieldRule));
								tierPricingItem.AddPrice = GlobalClass.StringToDecimal(taskItemMapping?.FieldRule2 ?? 0m);
								tierPricingItem.AddPercent = GlobalClass.StringToInteger(taskItemMapping?.FieldRule1 ?? 0);
							}
							if (sourceTierPricingItem.MappingItemType == 2)
							{
								tierPricingItem.CustomerRoles = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceTierPricingItem.FieldMappingForIndex.ToString(), sourceTierPricingItem.FieldRule))?.Split(',').ToList();
							}
							if (sourceTierPricingItem.MappingItemType == 13)
							{
								tierPricingItem.StartDateTime = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceTierPricingItem.FieldMappingForIndex.ToString(), sourceTierPricingItem.FieldRule));
							}
							if (sourceTierPricingItem.MappingItemType == 14)
							{
								tierPricingItem.EndDateTime = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceTierPricingItem.FieldMappingForIndex.ToString(), sourceTierPricingItem.FieldRule));
							}
						}
					}
				}
				if (taskMapping?.TaskTierPricingMapping.TaskTierPricingMappingItems != null)
				{
					TierPricingItem tierPricingItem2 = new TierPricingItem();
					foreach (TaskItemMapping taskTierPricingItem in taskMapping.TaskTierPricingMapping?.TaskTierPricingMappingItems.Where((TaskItemMapping x) => !x.MappedToSource && x.FieldImport))
					{
						tierPricingItem2 = TierPricingItems.FirstOrDefault((TierPricingItem x) => x.Id == taskTierPricingItem.Id);
						if (tierPricingItem2 == null)
						{
							tierPricingItem2 = new TierPricingItem
							{
								Id = taskTierPricingItem.Id
							};
							TierPricingItems.Add(tierPricingItem2);
						}
						if (taskTierPricingItem.MappingItemType == 0)
						{
							tierPricingItem2.Quantity = GlobalClass.StringToInteger(GetElementValue(dataRow, null, taskTierPricingItem.FieldRule));
						}
						if (taskTierPricingItem.MappingItemType == 1)
						{
							tierPricingItem2.Price = GlobalClass.StringToDecimal(GetElementValue(dataRow, null, taskTierPricingItem.FieldRule));
							tierPricingItem2.AddPrice = GlobalClass.StringToDecimal(taskTierPricingItem.FieldRule2);
							tierPricingItem2.AddPercent = GlobalClass.StringToInteger(taskTierPricingItem.FieldRule1);
						}
						if (taskTierPricingItem.MappingItemType == 2)
						{
							tierPricingItem2.CustomerRoles = GlobalClass.ObjectToString(GetElementValue(dataRow, null, taskTierPricingItem.FieldRule))?.Split(',').ToList();
						}
						if (taskTierPricingItem.MappingItemType == 13)
						{
							tierPricingItem2.StartDateTime = GlobalClass.ObjectToString(GetElementValue(dataRow, null, taskTierPricingItem.FieldRule));
						}
						if (taskTierPricingItem.MappingItemType == 14)
						{
							tierPricingItem2.EndDateTime = GlobalClass.ObjectToString(GetElementValue(dataRow, null, taskTierPricingItem.FieldRule));
						}
					}
				}
			}
			if (sourceMapping.EntityType == EntityType.Users)
			{
				User = new CustomerItem();
				GetCustomer(sourceMapping, taskMapping, null, dataRow);
				GetCustomerShippingAddress(sourceMapping, taskMapping, null, dataRow);
				GetCustomerBillingAddress(sourceMapping, taskMapping, null, dataRow);
				GetCustomerRole(sourceMapping, taskMapping, null, dataRow);
				AddressCustomAttributes = new List<AttributeValues>();
				GetAddressCustomAttributes(sourceMapping, taskMapping, null, dataRow);
				CustomerCustomAttributes = new List<AttributeValues>();
				GetCustomerCustomAttributes(sourceMapping, taskMapping, null, dataRow);
				GetCustomerOthers(sourceMapping, taskMapping, null, dataRow);
			}
			if (sourceMapping.EntityType == EntityType.WishList)
			{
				WishList = new List<WishListItem>();
				GetWishList(sourceMapping, taskMapping, null, dataRow);
			}
		}

		public SourceItem(FileFormat fileFormat, string productUrl, SourceMapping sourceMapping, SourceData sourceData, TaskMapping taskMapping, BestSecretClient bestSecretClient = null)
		{
			_sourceData = sourceData;
			CustomerRoles = new List<CustomerRuleItem>();
			Categories = new List<CategoryItem>();
			Images = new List<ImageItem>();
			SpecAttributes = new List<AttributeValues>();
			Attributes = new List<AttributeValues>();
			Filters = new List<FilterValues>();
			Variants = new List<VariantItem>();
			ProductTags = new List<ProductTagItem>();
			CrossSellItems = new List<CrossSellItem>();
			RelatedProductItems = new List<RelatedProductItem>();
			if (fileFormat == FileFormat.BestSecret)
			{
				BestSecretProduct result = bestSecretClient.GetProductDetails("https://www.bestsecret.com" + productUrl).Result;
				ProdId = GlobalClass.ObjectToString(GetElementValue(result.ProductId, null));
				ProdSku = GlobalClass.ObjectToString(GetElementValue(result.ProductSku, null));
				ProdName = GlobalClass.ObjectToString2(GetElementValue(result.ProductName, sourceMapping.ProdNameMap, sourceMapping.ProdNameVal), taskMapping?.UseTitleCase ?? false);
				ProdShortDesc = GlobalClass.ObjectToString2(GetElementValue(result.ProductShortDescription, sourceMapping.ProdShortDescMap, sourceMapping.ProdShortDescVal), taskMapping?.UseTitleCase ?? false);
				ProdFullDesc = GlobalClass.ObjectToString(GetElementValue(result.ProductFullDescription, sourceMapping.ProdFullDescMap, sourceMapping.ProdFullDescVal));
				Manufacturer = GlobalClass.ObjectToString(GetElementValue(result.Manufacturer, sourceMapping.ManufactMap, sourceMapping.ManufactVal));
				ProdPrice = GlobalClass.StringToDecimal(GetElementValue(result.ProductPrice.ToString(), sourceMapping.ProdPriceMap, sourceMapping.ProdPriceVal));
				ProdOldPrice = GlobalClass.StringToDecimal(GetElementValue(result.ProductOldPrice.ToString(), sourceMapping.ProdOldPriceMap, sourceMapping.ProdOldPriceVal));
				ProdCost = GlobalClass.StringToDecimal(GetElementValue(result.ProductCostPrice.ToString(), sourceMapping.ProdCostMap, sourceMapping.ProdCostVal));
				ProdStock = GlobalClass.StringToInteger(GetElementValue(result.ProductQuantity.ToString(), sourceMapping.ProdStockMap, sourceMapping.ProdStockVal));
				Unavailable = result.Unavailable;
				List<ImageItem> list = new List<ImageItem>();
				int num = 0;
				foreach (NopTalkCore.ImageItem ımageItem in result.ImageItems)
				{
					if (!string.IsNullOrEmpty(ımageItem.ImageUrl) || !string.IsNullOrEmpty(sourceMapping.ImageVal))
					{
						string text = GlobalClass.ObjectToString(GetElementValue(ımageItem.ImageUrl, sourceMapping.ImageVal));
						if (!string.IsNullOrEmpty(text))
						{
							list.Add(new ImageItem
							{
								ImageId = num,
								ImageUrl = text,
								IsBinary = sourceMapping.ImageIsBinary,
								IsSelected = true
							});
						}
					}
					num++;
				}
				Images = list;
				List<AttributeValues> list2 = new List<AttributeValues>();
				int num2 = 0;
				foreach (AttributeItem attributeItem in result.AttributeItems)
				{
					if (!string.IsNullOrEmpty(attributeItem.AttributeName) && attributeItem.OptionItems.Any())
					{
						foreach (OptionItem optionItem in attributeItem.OptionItems)
						{
							AttributeValues attributeValues = new AttributeValues
							{
								AttInfo_Id = num2.ToString(),
								AttInfo_Name = GlobalClass.ObjectToString(GetElementValue(attributeItem.AttributeName, sourceMapping.AttNameVal)),
								AttValue_Value = GlobalClass.ObjectToString(GetElementValue(optionItem.OptionValue, optionItem.Title)),
								AttOrgCode = optionItem.OptionId,
								Title = optionItem.Title,
								AttValue_Stock = optionItem.Quantity
							};
							if (!string.IsNullOrEmpty(attributeValues.AttInfo_Name) && !string.IsNullOrEmpty(attributeValues.AttValue_Value))
							{
								list2.Add(attributeValues);
							}
						}
						num2++;
					}
					Attributes = list2;
					List<VariantItem> list4 = Variants = new List<VariantItem>();
					int num3 = 0;
					foreach (NopTalkCore.CategoryItem categoriesItem in result.CategoriesItems)
					{
						if (!string.IsNullOrEmpty(categoriesItem.Category))
						{
							Categories.Add(new CategoryItem
							{
								CatId = num3,
								Category = categoriesItem.Category,
								ParentCatId = ((num3 > 0) ? (num3 - 1) : 0)
							});
							num3++;
						}
					}
				}
			}
			if (fileFormat == FileFormat.AliExpress)
			{
				AliExpressProduct productDetails = AliExpressClient.GetProductDetails(productUrl);
				ProdId = GlobalClass.ObjectToString(GetElementValue(productDetails.ProductId, null));
				ProdSku = GlobalClass.ObjectToString(GetElementValue(productDetails.ProductSku, null));
				ProdName = GlobalClass.ObjectToString2(GetElementValue(productDetails.ProductName, sourceMapping.ProdNameMap, sourceMapping.ProdNameVal), taskMapping?.UseTitleCase ?? false);
				ProdShortDesc = GlobalClass.ObjectToString2(GetElementValue(productDetails.ProductShortDescription, sourceMapping.ProdShortDescMap, sourceMapping.ProdShortDescVal), taskMapping?.UseTitleCase ?? false);
				ProdFullDesc = GlobalClass.ObjectToString(GetElementValue(productDetails.ProductFullDescription, sourceMapping.ProdFullDescMap, sourceMapping.ProdFullDescVal));
				ProdPrice = GlobalClass.StringToDecimal(GetElementValue(productDetails.ProductPrice.ToString(), sourceMapping.ProdPriceMap, sourceMapping.ProdPriceVal));
				ProdStock = GlobalClass.StringToInteger(GetElementValue(productDetails.ProductQuantity.ToString(), sourceMapping.ProdStockMap, sourceMapping.ProdStockVal));
				if (sourceMapping.ImageMap == "AliExpress")
				{
					List<ImageItem> list5 = new List<ImageItem>();
					int num4 = 0;
					foreach (NopTalkCore.ImageItem ımageItem2 in productDetails.ImageItems)
					{
						if (!string.IsNullOrEmpty(ımageItem2.ImageUrl) || !string.IsNullOrEmpty(sourceMapping.ImageVal))
						{
							string text2 = GlobalClass.ObjectToString(GetElementValue(ımageItem2.ImageUrl, sourceMapping.ImageVal));
							if (!string.IsNullOrEmpty(text2))
							{
								list5.Add(new ImageItem
								{
									ImageId = num4,
									ImageUrl = text2,
									IsBinary = sourceMapping.ImageIsBinary,
									IsSelected = true
								});
							}
						}
						num4++;
					}
					Images = list5;
				}
				if (sourceMapping.AttNameMap == "AliExpress" || sourceMapping.AttNameMap != "AliExpress")
				{
					List<AttributeValues> list6 = new List<AttributeValues>();
					int num5 = 0;
					foreach (AttributeItem attributeItem2 in productDetails.AttributeItems)
					{
						if (!string.IsNullOrEmpty(attributeItem2.AttributeName) && attributeItem2.OptionItems.Any())
						{
							foreach (OptionItem optionItem2 in attributeItem2.OptionItems)
							{
								AttributeValues attributeValues2 = new AttributeValues
								{
									AttInfo_Id = num5.ToString(),
									AttInfo_Name = GlobalClass.ObjectToString(GetElementValue(attributeItem2.AttributeName, sourceMapping.AttNameVal)),
									AttValue_Value = GlobalClass.ObjectToString(GetElementValue(optionItem2.OptionValue, optionItem2.Title)),
									AttOrgCode = optionItem2.OptionId,
									Title = optionItem2.Title
								};
								if (!string.IsNullOrEmpty(attributeValues2.AttInfo_Name) && !string.IsNullOrEmpty(attributeValues2.AttValue_Value))
								{
									list6.Add(attributeValues2);
								}
							}
							num5++;
						}
					}
					Attributes = list6;
					List<VariantItem> list7 = new List<VariantItem>();
					int num6 = 0;
					foreach (AliExpressProductVariantStruct variantionItem in productDetails.VariantionItems)
					{
						List<string> attIds = variantionItem.skuPropIds.Split(',').ToList();
						VariantItem variantItem = new VariantItem
						{
							Sku = variantionItem.skuAttr,
							OverriddenPrice = GlobalClass.StringToDecimal(variantionItem.skuVal?.actSkuCalPrice)
						};
						decimal? overriddenPrice = variantItem.OverriddenPrice;
						if ((overriddenPrice.GetValueOrDefault() == default(decimal)) & overriddenPrice.HasValue)
						{
							variantItem.OverriddenPrice = GlobalClass.StringToDecimal(variantionItem.skuVal?.skuCalPrice);
						}
						variantItem.StockQuantity = GlobalClass.StringToInteger(variantionItem.skuVal?.availQuantity);
						if (variantItem.StockQuantity == 0)
						{
							variantItem.StockQuantity = GlobalClass.StringToInteger(variantionItem.skuVal?.inventory);
						}
						variantItem.VariantItems.AddRange(Attributes.Where((AttributeValues x) => attIds.Contains(x.AttOrgCode)).ToList());
						list7.Add(variantItem);
						num6++;
					}
					Variants = list7;
				}
			}
			if (fileFormat != FileFormat.AliExpress && fileFormat != FileFormat.BestSecret)
			{
				ProdManPartNum = GlobalClass.ObjectToString(GetElementValue(sourceMapping.ProdManPartNumMap, sourceMapping.ProdManPartNumVal));
				ProdCost = GlobalClass.StringToDecimal(GetElementValue(sourceMapping.ProdCostMap, sourceMapping.ProdCostVal));
				ProdOldPrice = GlobalClass.StringToDecimal(GetElementValue(sourceMapping.ProdOldPriceMap, sourceMapping.ProdOldPriceVal));
				SeoMetaKey = GlobalClass.ObjectToString2(GetElementValue(sourceMapping.SeoMetaKeyMap, sourceMapping.SeoMetaKeyVal), taskMapping?.UseTitleCase ?? false);
				SeoMetaDesc = GlobalClass.ObjectToString2(GetElementValue(sourceMapping.SeoMetaDescMap, sourceMapping.SeoMetaDescVal), taskMapping?.UseTitleCase ?? false);
				SeoMetaTitle = GlobalClass.ObjectToString2(GetElementValue(sourceMapping.SeoMetaTitleMap, sourceMapping.SeoMetaTitleVal), taskMapping?.UseTitleCase ?? false);
				SearchPage = GlobalClass.ObjectToString(GetElementValue(sourceMapping.SeoPageMap, sourceMapping.SeoPageVal));
				Manufacturer = GlobalClass.ObjectToString2(GetElementValue(sourceMapping.ManufactMap, sourceMapping.ManufactVal), taskMapping?.UseTitleCase ?? false);
				Vendor = GlobalClass.ObjectToString2(GetElementValue(sourceMapping.VendorMap, sourceMapping.VendorVal), taskMapping?.UseTitleCase ?? false);
				TaxCategory = GlobalClass.ObjectToString2(GetElementValue(sourceMapping.TaxCategoryMap, sourceMapping.TaxCategoryVal), taskMapping?.UseTitleCase ?? false);
				Gtin = GlobalClass.ObjectToString2(GetElementValue(sourceMapping.GtinMap, sourceMapping.GtinVal), taskMapping?.UseTitleCase ?? false);
				Weight = GlobalClass.StringToDecimal(GetElementValue(sourceMapping.WeightMap, sourceMapping.WeightVal));
				Length = GlobalClass.StringToDecimal(GetElementValue(sourceMapping.LengthMap, sourceMapping.LengthVal));
				Width = GlobalClass.StringToDecimal(GetElementValue(sourceMapping.WidthMap, sourceMapping.WidthVal));
				Height = GlobalClass.StringToDecimal(GetElementValue(sourceMapping.HeightMap, sourceMapping.HeightVal));
			}
			ProdSettingsAllowedQty = GlobalClass.StringToInteger(GetElementValue(sourceMapping.ProdSettingsAllowedQtyMap, sourceMapping.ProdSettingsAllowedQtyVal), -1);
			ProdSettingsMinCartQty = GlobalClass.StringToInteger(GetElementValue(sourceMapping.ProdSettingsMinCartQtyMap, sourceMapping.ProdSettingsMinCartQtyVal));
			ProdSettingsMaxCartQty = GlobalClass.StringToInteger(GetElementValue(sourceMapping.ProdSettingsMaxCartQtyMap, sourceMapping.ProdSettingsMaxCartQtyVal));
			ProdSettingsMinStockQty = GlobalClass.StringToInteger(GetElementValue(sourceMapping.ProdSettingsMinStockQtyMap, sourceMapping.ProdSettingsMinStockQtyVal));
			ProdSettingsNotifyQty = GlobalClass.StringToInteger(GetElementValue(sourceMapping.ProdSettingsNotifyQtyMap, sourceMapping.ProdSettingsNotifyQtyVal));
			ProdSettingsShippingEnabled = GlobalClass.ObjectToBool(GetElementValue(sourceMapping.ProdSettingsShippingEnabledMap, sourceMapping.ProdSettingsShippingEnabledVal));
			ProdSettingsFreeShipping = GlobalClass.ObjectToBool(GetElementValue(sourceMapping.ProdSettingsFreeShippingMap, sourceMapping.ProdSettingsFreeShippingVal));
			ProdSettingsShipSeparately = GlobalClass.ObjectToBool(GetElementValue(sourceMapping.ProdSettingsShipSeparatelyMap, sourceMapping.ProdSettingsShipSeparatelyVal));
			ProdSettingsShippingCharge = GlobalClass.StringToDecimal(GetElementValue(sourceMapping.ProdSettingsShippingChargeMap, sourceMapping.ProdSettingsShippingChargeVal));
			ProdSettingsDeliveryDate = GlobalClass.ObjectToString(GetElementValue(sourceMapping.ProdSettingsDeliveryDateMap, sourceMapping.ProdSettingsDeliveryDateVal));
			ProdSettingsVisibleIndividually = GlobalClass.ObjectToBool(GetElementValue(sourceMapping.ProdSettingsVisibleIndividuallyMap, sourceMapping.ProdSettingsVisibleIndividuallyVal));
			ProdSettingsIsDeleted = GlobalClass.ObjectToBool(GetElementValue(sourceMapping.ProdSettingsIsDeletedMap, sourceMapping.ProdSettingsIsDeletedVal));
			if (!string.IsNullOrEmpty(sourceMapping.CatMap) || !string.IsNullOrEmpty(sourceMapping.CatVal))
			{
				string cat10 = GlobalClass.ObjectToString(GetElementValue(sourceMapping.CatMap, sourceMapping.CatVal));
				if (sourceMapping.CatDelimeter.HasValue && cat10.IndexOf(sourceMapping.CatDelimeter.Value) > -1)
				{
					List<string> list8 = cat10.Split(sourceMapping.CatDelimeter.Value).ToList();
					foreach (string catItem2 in list8)
					{
						if (!string.IsNullOrEmpty(catItem2) && !Categories.Exists((CategoryItem a) => a.Category.Equals(catItem2.Trim())))
						{
							Categories.Add(new CategoryItem
							{
								CatId = 0,
								Category = catItem2.Trim(),
								Splitted = true
							});
						}
					}
				}
				else if (!string.IsNullOrEmpty(cat10) && !Categories.Exists((CategoryItem a) => a.Category.Equals(cat10.Trim())))
				{
					Categories.Add(new CategoryItem
					{
						CatId = 0,
						Category = cat10.Trim()
					});
				}
			}
			if (!string.IsNullOrEmpty(sourceMapping.Cat1Map) || !string.IsNullOrEmpty(sourceMapping.Cat1Val))
			{
				string cat9 = GlobalClass.ObjectToString(GetElementValue(sourceMapping.Cat1Map, sourceMapping.Cat1Val));
				if (!string.IsNullOrEmpty(cat9) && !Categories.Exists((CategoryItem a) => a.Category.Equals(cat9.Trim())))
				{
					Categories.Add(new CategoryItem
					{
						CatId = 1,
						Category = cat9.Trim(),
						ParentCatId = sourceMapping.Cat1Parent
					});
				}
			}
			if (!string.IsNullOrEmpty(sourceMapping.Cat2Map) || !string.IsNullOrEmpty(sourceMapping.Cat2Val))
			{
				string cat8 = GlobalClass.ObjectToString(GetElementValue(sourceMapping.Cat2Map, sourceMapping.Cat2Val));
				if (!string.IsNullOrEmpty(cat8) && !Categories.Exists((CategoryItem a) => a.Category.Equals(cat8.Trim())))
				{
					Categories.Add(new CategoryItem
					{
						CatId = 2,
						Category = cat8.Trim(),
						ParentCatId = sourceMapping.Cat2Parent
					});
				}
			}
			if (!string.IsNullOrEmpty(sourceMapping.Cat3Map) || !string.IsNullOrEmpty(sourceMapping.Cat3Val))
			{
				string cat7 = GlobalClass.ObjectToString(GetElementValue(sourceMapping.Cat3Map, sourceMapping.Cat3Val));
				if (!string.IsNullOrEmpty(cat7) && !Categories.Exists((CategoryItem a) => a.Category.Equals(cat7.Trim())))
				{
					Categories.Add(new CategoryItem
					{
						CatId = 3,
						Category = cat7.Trim(),
						ParentCatId = sourceMapping.Cat3Parent
					});
				}
			}
			if (!string.IsNullOrEmpty(sourceMapping.Cat4Map) || !string.IsNullOrEmpty(sourceMapping.Cat4Val))
			{
				string cat6 = GlobalClass.ObjectToString(GetElementValue(sourceMapping.Cat4Map, sourceMapping.Cat4Val));
				if (!string.IsNullOrEmpty(cat6) && !Categories.Exists((CategoryItem a) => a.Category.Equals(cat6.Trim())))
				{
					Categories.Add(new CategoryItem
					{
						CatId = 4,
						Category = cat6.Trim(),
						ParentCatId = sourceMapping.Cat4Parent
					});
				}
			}
			List<CustomerRuleItem> list9 = new List<CustomerRuleItem>();
			if (!string.IsNullOrEmpty(sourceMapping.CustomerRoleMap) || !string.IsNullOrEmpty(sourceMapping.CustomerRoleVal))
			{
				string cat5 = GlobalClass.ObjectToString(GetElementValue(sourceMapping.CustomerRoleMap, sourceMapping.CustomerRoleVal));
				if (sourceMapping.CustomerRoleDelimeter.HasValue && cat5.IndexOf(sourceMapping.CustomerRoleDelimeter.Value) > -1)
				{
					List<string> list10 = cat5.Split(sourceMapping.CustomerRoleDelimeter.Value).ToList();
					foreach (string catItem in list10)
					{
						if (!string.IsNullOrEmpty(catItem) && !list9.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(catItem.Trim())))
						{
							list9.Add(new CustomerRuleItem
							{
								CustomerRuleId = 0,
								CustomerRule = catItem.Trim()
							});
						}
					}
				}
				else if (!string.IsNullOrEmpty(cat5) && !list9.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(cat5.Trim())))
				{
					list9.Add(new CustomerRuleItem
					{
						CustomerRuleId = 0,
						CustomerRule = cat5.Trim()
					});
				}
			}
			if (!string.IsNullOrEmpty(sourceMapping.CustomerRole1Map) || !string.IsNullOrEmpty(sourceMapping.CustomerRole1Val))
			{
				string cat4 = GlobalClass.ObjectToString(GetElementValue(sourceMapping.CustomerRole1Map, sourceMapping.CustomerRole1Val));
				if (!string.IsNullOrEmpty(cat4) && !list9.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(cat4.Trim())))
				{
					list9.Add(new CustomerRuleItem
					{
						CustomerRuleId = 1,
						CustomerRule = cat4.Trim()
					});
				}
			}
			if (!string.IsNullOrEmpty(sourceMapping.CustomerRole2Map) || !string.IsNullOrEmpty(sourceMapping.CustomerRole2Val))
			{
				string cat3 = GlobalClass.ObjectToString(GetElementValue(sourceMapping.CustomerRole2Map, sourceMapping.CustomerRole2Val));
				if (!string.IsNullOrEmpty(cat3) && !list9.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(cat3.Trim())))
				{
					list9.Add(new CustomerRuleItem
					{
						CustomerRuleId = 2,
						CustomerRule = cat3.Trim()
					});
				}
			}
			if (!string.IsNullOrEmpty(sourceMapping.CustomerRole3Map) || !string.IsNullOrEmpty(sourceMapping.CustomerRole3Val))
			{
				string cat2 = GlobalClass.ObjectToString(GetElementValue(sourceMapping.CustomerRole3Map, sourceMapping.CustomerRole3Val));
				if (!string.IsNullOrEmpty(cat2) && !list9.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(cat2.Trim())))
				{
					list9.Add(new CustomerRuleItem
					{
						CustomerRuleId = 3,
						CustomerRule = cat2.Trim()
					});
				}
			}
			if (!string.IsNullOrEmpty(sourceMapping.CustomerRole4Map) || !string.IsNullOrEmpty(sourceMapping.CustomerRole4Val))
			{
				string cat = GlobalClass.ObjectToString(GetElementValue(sourceMapping.CustomerRole4Map, sourceMapping.CustomerRole4Val));
				if (!string.IsNullOrEmpty(cat) && !list9.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(cat.Trim())))
				{
					list9.Add(new CustomerRuleItem
					{
						CustomerRuleId = 4,
						CustomerRule = cat.Trim()
					});
				}
			}
			CustomerRoles = list9;
		}

		private void GetWishList(SourceMapping sourceMapping, TaskMapping taskMapping, XElement xElement, DataRow dataRow)
		{
			List<SourceItemMapping> list = sourceMapping.SourceWishListMapping?.Items;
			List<TaskItemMapping> list2 = taskMapping?.TaskWishListMapping?.Items;
			if (list != null && list.Any())
			{
				List<IGrouping<Guid, SourceItemMapping>> list3 = (from u in list?.Where((SourceItemMapping x) => x.MappingItemType > -1)
					group u by u.Id).ToList();
				foreach (IGrouping<Guid, SourceItemMapping> item in list3)
				{
					WishListItem wishListItem = new WishListItem();
					bool flag = false;
					foreach (SourceItemMapping sourceItem in item)
					{
						TaskItemMapping taskItemMapping = list2?.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
						if (taskItemMapping?.FieldImport ?? true)
						{
							string defaultValue = (taskItemMapping == null) ? sourceItem.FieldRule : taskItemMapping.FieldRule;
							flag = (taskItemMapping?.FieldImport ?? false);
							if (sourceItem.MappingItemType == 3001)
							{
								if (xElement != null)
								{
									wishListItem.ProductSKU = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									wishListItem.ProductSKU = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 3003)
							{
								if (xElement != null)
								{
									wishListItem.CustomerEmail = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									wishListItem.CustomerEmail = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 3002)
							{
								if (xElement != null)
								{
									wishListItem.CustomerUsername = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									wishListItem.CustomerUsername = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 3004)
							{
								if (xElement != null)
								{
									wishListItem.CustomSKUID = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									wishListItem.CustomSKUID = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
						}
					}
					WishList.Add(wishListItem);
				}
			}
		}

		private void GetProductPrice(SourceMapping sourceMapping, TaskMapping taskMapping, XElement xElement, DataRow dataRow)
		{
			List<SourceItemMapping> list = sourceMapping.SourceProdPriceMapping?.Items;
			List<TaskItemMapping> list2 = taskMapping?.TaskProductPriceMapping?.Items;
			if (list != null && list.Any())
			{
				List<IGrouping<Guid, SourceItemMapping>> list3 = (from u in list?.Where((SourceItemMapping x) => x.MappingItemType > -1)
					group u by u.Id).ToList();
				foreach (IGrouping<Guid, SourceItemMapping> item in list3)
				{
					bool flag = false;
					foreach (SourceItemMapping sourceItem in item)
					{
						TaskItemMapping taskItemMapping = list2?.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
						if (taskItemMapping?.FieldImport ?? true)
						{
							string defaultValue = (taskItemMapping == null) ? sourceItem.FieldRule : taskItemMapping.FieldRule;
							flag = (taskItemMapping?.FieldImport ?? false);
							if (sourceItem.MappingItemType == 1001)
							{
								if (xElement != null)
								{
									ProdCost = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProdCost = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 1002)
							{
								if (xElement != null)
								{
									ProdPrice = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProdPrice = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
								decimal d = GlobalClass.StringToDecimal(taskItemMapping?.FieldRule2);
								int value = GlobalClass.StringToInteger(taskItemMapping?.FieldRule1);
								ProdPrice = ProdPrice.GetValueOrDefault() + ProdPrice.GetValueOrDefault() * (decimal)value / 100m + d;
							}
							if (sourceItem.MappingItemType == 1003)
							{
								if (xElement != null)
								{
									ProdOldPrice = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProdOldPrice = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 1004)
							{
								if (xElement != null)
								{
									ProductPrice_BasepriceEnabled = GlobalClass.ObjectToBool(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProductPrice_BasepriceEnabled = GlobalClass.ObjectToBool(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 1005)
							{
								if (xElement != null)
								{
									ProductPrice_BasepriceAmount = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProductPrice_BasepriceAmount = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 1008)
							{
								if (xElement != null)
								{
									ProductPrice_BasepriceBaseUnit = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProductPrice_BasepriceBaseUnit = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 1006)
							{
								if (xElement != null)
								{
									ProductPrice_BasepriceUnit = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProductPrice_BasepriceUnit = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 1007)
							{
								if (xElement != null)
								{
									ProductPrice_BasepriceBaseAmount = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProductPrice_BasepriceBaseAmount = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 1009)
							{
								if (xElement != null)
								{
									TaxCategory = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									TaxCategory = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 1010)
							{
								if (xElement != null)
								{
									TaxExempt = GlobalClass.ObjectToBool(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									TaxExempt = GlobalClass.ObjectToBool(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
						}
					}
				}
			}
		}

		private void GetProductInfo(SourceMapping sourceMapping, TaskMapping taskMapping, XElement xElement, DataRow dataRow)
		{
			if (sourceMapping.SourceProdInfoMapping?.Items != null && sourceMapping.SourceProdInfoMapping.Items.Any())
			{
				List<IGrouping<Guid, SourceItemMapping>> list = (from x in sourceMapping.SourceProdInfoMapping.Items
					where x.MappingItemType > -1
					select x into u
					group u by u.Id).ToList();
				foreach (IGrouping<Guid, SourceItemMapping> item in list)
				{
					bool flag = false;
					string text = "";
					foreach (SourceItemMapping sourceItem in item)
					{
						TaskItemMapping taskItemMapping = taskMapping?.TaskProductInfoMapping?.Items?.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
						if (taskItemMapping?.FieldImport ?? true)
						{
							string defaultValue = (taskItemMapping == null) ? sourceItem.FieldRule : taskItemMapping.FieldRule;
							flag = (taskItemMapping?.FieldImport ?? false);
							if (sourceItem.MappingItemType == 501)
							{
								if (xElement != null)
								{
									ProdId = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProdId = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 502)
							{
								if (xElement != null)
								{
									ProdSku = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProdSku = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 503)
							{
								if (xElement != null)
								{
									ProdGroupBy = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProdGroupBy = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 504)
							{
								if (xElement != null)
								{
									ProdName = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProdName = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 505)
							{
								if (xElement != null)
								{
									ProdShortDesc = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProdShortDesc = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 518)
							{
								if (xElement != null)
								{
									ProdFullDesc = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProdFullDesc = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 517)
							{
								string text2 = "";
								if (xElement != null)
								{
									text2 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									text2 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
								text = GlobalClass.ObjectToString(sourceItem?.FieldRule1);
								if (!string.IsNullOrEmpty(text2))
								{
									if (!string.IsNullOrEmpty(text) && text2.IndexOf(text) > -1)
									{
										List<string> list2 = text2.Split(new string[1]
										{
											text
										}, StringSplitOptions.None).ToList();
										int num = 0;
										foreach (string item2 in list2)
										{
											string valueItemTrimed3 = item2.Trim();
											if (!string.IsNullOrEmpty(valueItemTrimed3) && !ProductTags.Exists((ProductTagItem a) => a.ProductTagName.Equals(valueItemTrimed3)))
											{
												ProductTagItem productTagItem = new ProductTagItem();
												productTagItem.ProductTagId = num;
												productTagItem.ProductTagName = valueItemTrimed3;
												ProductTags.Add(productTagItem);
												num++;
											}
										}
									}
									else
									{
										ProductTagItem productTagItem2 = new ProductTagItem();
										productTagItem2.ProductTagId = 0;
										productTagItem2.ProductTagName = text2;
										ProductTags.Add(productTagItem2);
									}
								}
							}
							if (sourceItem.MappingItemType == 506)
							{
								if (xElement != null)
								{
									ProdManPartNum = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProdManPartNum = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 510)
							{
								if (xElement != null)
								{
									ProdStock = GlobalClass.StringToInteger(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProdStock = GlobalClass.StringToInteger(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 512)
							{
								if (xElement != null)
								{
									Gtin = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									Gtin = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 513)
							{
								if (xElement != null)
								{
									Weight = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									Weight = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 515)
							{
								if (xElement != null)
								{
									Width = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									Width = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 516)
							{
								if (xElement != null)
								{
									Height = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									Height = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 514)
							{
								if (xElement != null)
								{
									Length = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									Length = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 701)
							{
								string text3 = "";
								if (xElement != null)
								{
									text3 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									text3 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
								string text4 = GlobalClass.ObjectToString(sourceItem?.FieldRule1);
								if (!string.IsNullOrEmpty(text3))
								{
									if (!string.IsNullOrEmpty(text4) && text3.IndexOf(text4) > -1)
									{
										List<string> list3 = text3.Split(new string[1]
										{
											text4
										}, StringSplitOptions.None).ToList();
										int num2 = 0;
										foreach (string item3 in list3)
										{
											string valueItemTrimed2 = item3.Trim();
											if (!string.IsNullOrEmpty(valueItemTrimed2) && !CrossSellItems.Exists((CrossSellItem a) => a.ProductSKU.Equals(valueItemTrimed2)))
											{
												CrossSellItem crossSellItem = new CrossSellItem();
												crossSellItem.ProductId = num2;
												crossSellItem.ProductSKU = valueItemTrimed2;
												CrossSellItems.Add(crossSellItem);
												num2++;
											}
										}
									}
									else
									{
										CrossSellItem crossSellItem2 = new CrossSellItem();
										crossSellItem2.ProductId = 0;
										crossSellItem2.ProductSKU = text3;
										CrossSellItems.Add(crossSellItem2);
									}
								}
							}
							if (sourceItem.MappingItemType == 801)
							{
								string text5 = "";
								if (xElement != null)
								{
									text5 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									text5 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
								string text6 = GlobalClass.ObjectToString(sourceItem?.FieldRule1);
								if (!string.IsNullOrEmpty(text5))
								{
									if (!string.IsNullOrEmpty(text6) && text5.IndexOf(text6) > -1)
									{
										List<string> list4 = text5.Split(new string[1]
										{
											text6
										}, StringSplitOptions.None).ToList();
										int num3 = 0;
										foreach (string item4 in list4)
										{
											string valueItemTrimed = item4.Trim();
											if (!string.IsNullOrEmpty(valueItemTrimed) && !RelatedProductItems.Exists((RelatedProductItem a) => a.ProductSKU.Equals(valueItemTrimed)))
											{
												RelatedProductItem relatedProductItem = new RelatedProductItem();
												relatedProductItem.ProductId = num3;
												relatedProductItem.ProductSKU = valueItemTrimed;
												RelatedProductItems.Add(relatedProductItem);
												num3++;
											}
										}
									}
									else
									{
										RelatedProductItem relatedProductItem2 = new RelatedProductItem();
										relatedProductItem2.ProductId = 0;
										relatedProductItem2.ProductSKU = text5;
										RelatedProductItems.Add(relatedProductItem2);
									}
								}
							}
							if (sourceItem.MappingItemType == 901)
							{
								if (xElement != null)
								{
									TierPricing_ACCID = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									TierPricing_ACCID = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 2001)
							{
								if (xElement != null)
								{
									ProductStock_AllowBackInStockSubscriptions = GlobalClass.ObjectToBool(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProductStock_AllowBackInStockSubscriptions = GlobalClass.ObjectToBool(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 2002)
							{
								if (xElement != null)
								{
									ProductStock_DisableBuyButton = GlobalClass.ObjectToBool(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									ProductStock_DisableBuyButton = GlobalClass.ObjectToBool(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
						}
					}
				}
			}
		}

		private void GetAttributes(SourceMapping sourceMapping, TaskMapping taskMapping, XElement xElement, DataRow dataRow)
		{
			int num = 0;
			AttributeValues attributeValues = new AttributeValues();
			List<IGrouping<Guid, SourceItemMapping>> list = (from x in sourceMapping.SourceProdAttMapping.SourceProdAttMappingItems
				where x.MappingItemType > -1
				select x into u
				group u by u.Id).ToList();
			foreach (IGrouping<Guid, SourceItemMapping> item in list)
			{
				Guid guid = Guid.Empty;
				string info_attName = null;
				int attInfo_OptionType = 0;
				bool flag = false;
				string attInfo_DefaultValue = null;
				int attInfo_DisplayOrder = 0;
				int attValue_DisplayOrder = 0;
				string attInfo_TextPrompt = null;
				string attSku = null;
				string value_attValue = null;
				int attValue_Stock = 0;
				int value = 0;
				decimal num2 = default(decimal);
				decimal num3 = default(decimal);
				decimal attValue_WeightAdjustment = default(decimal);
				decimal value2 = default(decimal);
				bool flag2 = false;
				bool attValue_PriceUsePercent = false;
				bool value3 = false;
				string text = null;
				string attValue_Color = null;
				string attValue_AssociatedPicture = null;
				string attValue_SquarePicture = null;
				attributeValues = new AttributeValues();
				foreach (SourceItemMapping sourceProdAttItem in item)
				{
					TaskItemMapping taskItemMapping = taskMapping?.TaskProductAttributesMapping?.TaskProductAttributesMappingItems?.FirstOrDefault((TaskItemMapping x) => x.Id == sourceProdAttItem.Id && x.MappingItemType == sourceProdAttItem.MappingItemType);
					if (taskItemMapping?.FieldImport ?? true)
					{
						string defaultValue = (taskItemMapping == null) ? sourceProdAttItem.FieldRule : taskItemMapping.FieldRule;
						guid = (taskItemMapping?.Id ?? guid);
						if (sourceProdAttItem.MappingItemType == 4)
						{
							if (xElement != null)
							{
								info_attName = GlobalClass.ObjectToDistinctString(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								info_attName = GlobalClass.ObjectToDistinctString(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 3)
						{
							if (xElement != null)
							{
								attSku = GlobalClass.ObjectToString(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								attSku = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 110)
						{
							if (xElement != null)
							{
								attInfo_TextPrompt = GlobalClass.ObjectToDistinctString(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								attInfo_TextPrompt = GlobalClass.ObjectToDistinctString(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 108)
						{
							if (xElement != null)
							{
								attInfo_DefaultValue = GlobalClass.ObjectToDistinctString(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								attInfo_DefaultValue = GlobalClass.ObjectToDistinctString(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 107)
						{
							if (xElement != null)
							{
								attInfo_OptionType = GlobalClass.StringToInteger(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								attInfo_OptionType = GlobalClass.StringToInteger(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 109)
						{
							if (xElement != null)
							{
								attInfo_DisplayOrder = GlobalClass.StringToInteger(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								attInfo_DisplayOrder = GlobalClass.StringToInteger(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 106)
						{
							if (xElement != null)
							{
								flag = GlobalClass.ObjectToBool(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								flag = GlobalClass.ObjectToBool(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
							flag2 = GlobalClass.ObjectToBool(taskItemMapping?.FieldRule3);
						}
						if (sourceProdAttItem.MappingItemType == 104)
						{
							if (xElement != null)
							{
								flag2 = GlobalClass.ObjectToBool(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								flag2 = GlobalClass.ObjectToBool(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 5)
						{
							if (xElement != null)
							{
								value_attValue = GlobalClass.ObjectToDistinctString(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								value_attValue = GlobalClass.ObjectToDistinctString(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
							text = GlobalClass.ObjectToString(sourceProdAttItem?.FieldRule1);
						}
						if (sourceProdAttItem.MappingItemType == 6)
						{
							if (xElement != null)
							{
								attValue_Stock = GlobalClass.StringToInteger(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								attValue_Stock = GlobalClass.StringToInteger(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 112)
						{
							if (xElement != null)
							{
								value = GlobalClass.StringToInteger(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								value = GlobalClass.StringToInteger(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 111)
						{
							if (xElement != null)
							{
								value2 = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								value2 = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 7)
						{
							if (xElement != null)
							{
								num2 = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								num2 = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 11)
						{
							if (xElement != null)
							{
								attValue_Color = GlobalClass.ObjectToString(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								attValue_Color = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 10)
						{
							if (xElement != null)
							{
								attValue_AssociatedPicture = GlobalClass.ObjectToString(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								attValue_AssociatedPicture = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 12)
						{
							if (xElement != null)
							{
								attValue_SquarePicture = GlobalClass.ObjectToString(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								attValue_SquarePicture = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 101)
						{
							if (xElement != null)
							{
								attValue_PriceUsePercent = GlobalClass.ObjectToBool(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								attValue_PriceUsePercent = GlobalClass.ObjectToBool(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 102)
						{
							if (xElement != null)
							{
								attValue_WeightAdjustment = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								attValue_WeightAdjustment = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 103)
						{
							if (xElement != null)
							{
								num3 = GlobalClass.StringToDecimal(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								num3 = GlobalClass.StringToDecimal(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 105)
						{
							if (xElement != null)
							{
								attValue_DisplayOrder = GlobalClass.StringToInteger(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								attValue_DisplayOrder = GlobalClass.StringToInteger(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
						if (sourceProdAttItem.MappingItemType == 113)
						{
							if (xElement != null)
							{
								value3 = GlobalClass.ObjectToBool(GetElementValue(xElement, sourceProdAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								value3 = GlobalClass.ObjectToBool(GetElementValue(dataRow, sourceProdAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
					}
				}
				if (!string.IsNullOrEmpty(info_attName) && !Attributes.Any((AttributeValues x) => x.AttInfo_Name == info_attName && x.AttValue_Value == value_attValue))
				{
					string attOrgCode = info_attName + value_attValue;
					attributeValues.MappingId = guid;
					attributeValues.AttInfo_Id = num.ToString();
					attributeValues.AttInfo_Name = info_attName;
					attributeValues.AttInfo_TextPrompt = attInfo_TextPrompt;
					attributeValues.AttInfo_DefaultValue = attInfo_DefaultValue;
					attributeValues.AttInfo_DisplayOrder = attInfo_DisplayOrder;
					attributeValues.AttInfo_IsRequired = flag;
					attributeValues.AttInfo_OptionType = attInfo_OptionType;
					attributeValues.AttValue_Value = value_attValue;
					attributeValues.AttValue_Stock = attValue_Stock;
					attributeValues.AttValue_IsPreselected = flag2;
					attributeValues.AttValue_Color = attValue_Color;
					attributeValues.AttValue_AssociatedPicture = attValue_AssociatedPicture;
					attributeValues.AttValue_SquarePicture = attValue_SquarePicture;
					attributeValues.AttValue_PriceUsePercent = attValue_PriceUsePercent;
					attributeValues.AttValue_WeightAdjustment = attValue_WeightAdjustment;
					attributeValues.AttValue_Price = num2;
					attributeValues.AttValue_Cost = num3;
					attributeValues.AttValue_DisplayOrder = attValue_DisplayOrder;
					attributeValues.AttOrgCode = attOrgCode;
					if (!string.IsNullOrEmpty(text) && !string.IsNullOrEmpty(value_attValue) && value_attValue.IndexOf(text) > -1)
					{
						List<string> list2 = value_attValue.Split(new string[1]
						{
							text
						}, StringSplitOptions.None).ToList();
						int num4 = 0;
						foreach (string item2 in list2)
						{
							string valueItemTrimed = item2.Trim();
							if (!string.IsNullOrEmpty(valueItemTrimed) && !Attributes.Exists((AttributeValues a) => a.AttInfo_Name.Equals(info_attName) && a.AttValue_Value.Equals(valueItemTrimed)))
							{
								attOrgCode = info_attName + valueItemTrimed;
								attributeValues = new AttributeValues();
								attributeValues.MappingId = guid;
								attributeValues.AttInfo_Id = num.ToString();
								attributeValues.AttInfo_Name = info_attName;
								attributeValues.AttInfo_TextPrompt = attInfo_TextPrompt;
								attributeValues.AttInfo_DefaultValue = attInfo_DefaultValue;
								attributeValues.AttInfo_DisplayOrder = attInfo_DisplayOrder;
								attributeValues.AttInfo_IsRequired = (num4 == 0 && flag);
								attributeValues.AttInfo_OptionType = attInfo_OptionType;
								attributeValues.AttValue_Value = valueItemTrimed;
								attributeValues.AttValue_Stock = attValue_Stock;
								attributeValues.AttValue_IsPreselected = (num4 == 0 && flag2);
								attributeValues.AttValue_Color = attValue_Color;
								attributeValues.AttValue_AssociatedPicture = attValue_AssociatedPicture;
								attributeValues.AttValue_SquarePicture = attValue_SquarePicture;
								attributeValues.AttValue_PriceUsePercent = attValue_PriceUsePercent;
								attributeValues.AttValue_WeightAdjustment = attValue_WeightAdjustment;
								attributeValues.AttValue_Price = num3;
								attributeValues.AttValue_Cost = num2;
								attributeValues.AttValue_DisplayOrder = attValue_DisplayOrder;
								attributeValues.AttOrgCode = attOrgCode;
								Attributes.Add(attributeValues);
								AddVariant(attSku, value2, value, value3, attributeValues);
								num++;
								num4++;
							}
						}
					}
					else
					{
						Attributes.Add(attributeValues);
						AddVariant(attSku, value2, value, value3, attributeValues);
						num++;
					}
				}
			}
		}

		private void AddVariant(string attSku, decimal? attPrice, int? stock, bool? allowOutOfStock, AttributeValues prodAttItem)
		{
			if (!string.IsNullOrEmpty(attSku))
			{
				VariantItem variantItem = Variants?.FirstOrDefault((VariantItem x) => x.Sku == attSku);
				if (variantItem == null)
				{
					VariantItem variantItem2 = new VariantItem
					{
						Sku = attSku,
						OverriddenPrice = attPrice,
						StockQuantity = stock,
						AllowOutOfStock = allowOutOfStock
					};
					variantItem2.VariantItems.Add(prodAttItem);
					Variants.Add(variantItem2);
				}
                else
                {
                    variantItem.VariantItems.Add(prodAttItem);
                }
            }
		}

		//BURADA VARYANLARI EKLIYOR İC İCE EKLERSE SIKINTI YOK İLK RENK ALIYOR SONRAKİLERİ PATLATIYOR.
		private void AddVariantReplace(string attSku, decimal? attPrice, int? stock, bool? allowOutOfStock, AttributeValues prodAttItem , string colorCode)
		{
			if (!string.IsNullOrEmpty(attSku))
			{
				VariantItem variantItem = Variants?.FirstOrDefault((VariantItem x) => x.Sku == attSku);
				if (variantItem == null)
				{
					VariantItem variantItem2 = new VariantItem
					{
						Sku = attSku,
						OverriddenPrice = attPrice,
						StockQuantity = stock,
						AllowOutOfStock = allowOutOfStock
					};
					variantItem2.VariantItems.Add(prodAttItem);
					Variants.Add(variantItem2);
				}
				else
				{
					variantItem.VariantItems.Add(prodAttItem);
				}
			}
		}


		private void GetSpecAttributes(SourceMapping sourceMapping, TaskMapping taskMapping, XElement xElement, DataRow dataRow)
		{
			int num = 0;
			AttributeValues attributeValues = new AttributeValues();
			List<IGrouping<Guid, SourceItemMapping>> list = (from x in sourceMapping.SourceProdSpecAttMapping.SourceProdSpecAttMappingItems
				where x.MappingItemType > -1
				select x into u
				group u by u.Id).ToList();
			foreach (IGrouping<Guid, SourceItemMapping> item in list)
			{
				string specAttName = null;
				string specAttValue = null;
				int attInfo_OptionType = 0;
				bool showOnProduct = false;
				bool allowFiltering = false;
				bool ımport = false;
				string text = null;
				attributeValues = new AttributeValues();
				foreach (SourceItemMapping sourceProdSpecAttItem in item)
				{
					TaskItemMapping taskItemMapping = taskMapping?.TaskProductSpecAttributesMapping?.TaskProductSpecAttributesMappingItems?.FirstOrDefault((TaskItemMapping x) => x.Id == sourceProdSpecAttItem.Id && x.MappingItemType == sourceProdSpecAttItem.MappingItemType);
					if (taskItemMapping == null || taskItemMapping.FieldImport || (taskMapping?.AddSpecAttributtesToFullDescAsTable ?? false))
					{
						string defaultValue = (taskItemMapping == null) ? sourceProdSpecAttItem.FieldRule : taskItemMapping.FieldRule;
						ımport = (taskItemMapping?.FieldImport ?? false);
						if (sourceProdSpecAttItem.MappingItemType == 8)
						{
							if (xElement != null)
							{
								specAttName = GlobalClass.ObjectToDistinctString(GetElementValue(xElement, sourceProdSpecAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								specAttName = GlobalClass.ObjectToDistinctString(GetElementValue(dataRow, sourceProdSpecAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
							attInfo_OptionType = GlobalClass.StringToInteger(taskItemMapping?.FieldRule5);
							showOnProduct = GlobalClass.ObjectToBool(taskItemMapping?.FieldRule3);
							allowFiltering = GlobalClass.ObjectToBool(taskItemMapping?.FieldRule4);
						}
						if (sourceProdSpecAttItem.MappingItemType == 9)
						{
							if (xElement != null)
							{
								specAttValue = GlobalClass.ObjectToString(GetElementValue(xElement, sourceProdSpecAttItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								specAttValue = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceProdSpecAttItem.FieldMappingForIndex.ToString(), defaultValue));
							}
							text = GlobalClass.ObjectToString(sourceProdSpecAttItem?.FieldRule1);
						}
					}
				}
				if (!string.IsNullOrEmpty(specAttName) && !string.IsNullOrEmpty(specAttValue) && !SpecAttributes.Any((AttributeValues x) => x.AttInfo_Name == specAttName && x.AttValue_Value == specAttValue))
				{
					string attOrgCode = specAttName + specAttValue;
					attributeValues.AttInfo_Id = num.ToString();
					attributeValues.AttInfo_Name = specAttName;
					attributeValues.AttValue_Value = specAttValue;
					attributeValues.AttOrgCode = attOrgCode;
					attributeValues.AttInfo_OptionType = attInfo_OptionType;
					attributeValues.ShowOnProduct = showOnProduct;
					attributeValues.AllowFiltering = allowFiltering;
					attributeValues.Import = ımport;
					if (!string.IsNullOrEmpty(text) && specAttValue.IndexOf(text) > -1)
					{
						List<string> list2 = specAttValue.Split(new string[1]
						{
							text
						}, StringSplitOptions.None).ToList();
						foreach (string item2 in list2)
						{
							string valueItemTrimed = item2.Trim();
							if (!string.IsNullOrEmpty(valueItemTrimed) && !Attributes.Exists((AttributeValues a) => a.AttInfo_Name.Equals(specAttName) && a.AttValue_Value.Equals(valueItemTrimed)))
							{
								attributeValues = new AttributeValues();
								attributeValues.AttInfo_Id = num.ToString();
								attributeValues.AttInfo_Name = specAttName;
								attributeValues.AttValue_Value = valueItemTrimed;
								attributeValues.AttOrgCode = attOrgCode;
								attributeValues.AttInfo_OptionType = attInfo_OptionType;
								attributeValues.ShowOnProduct = showOnProduct;
								attributeValues.AllowFiltering = allowFiltering;
								attributeValues.Import = ımport;
								SpecAttributes.Add(attributeValues);
								num++;
							}
						}
					}
					else
					{
						SpecAttributes.Add(attributeValues);
						num++;
					}
				}
			}
		}

		private void GetProductPictures(SourceMapping sourceMapping, TaskMapping taskMapping, XElement xElement, DataRow dataRow)
		{
			int num = 0;
			ImageItem ımageItem = new ImageItem();
			List<IGrouping<Guid, SourceItemMapping>> list = (from x in sourceMapping.SourceProdPicturesMapping.Items
				where x.MappingItemType > -1
				select x into u
				group u by u.Id).ToList();
			foreach (IGrouping<Guid, SourceItemMapping> item in list)
			{
				bool flag = false;
				string text = "";
				bool flag2 = false;
				foreach (SourceItemMapping sourceItem in item)
				{
					TaskItemMapping taskItemMapping = taskMapping?.TaskProdPicturesMapping?.Items?.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
					if (taskItemMapping?.FieldImport ?? true)
					{
						string defaultValue = (taskItemMapping == null) ? sourceItem.FieldRule : taskItemMapping.FieldRule;
						flag = (taskItemMapping?.FieldImport ?? false);
						if (sourceItem.MappingItemType == 601)
						{
							string text2 = "";
							if (xElement != null)
							{
								text2 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								text2 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
							}
							text = GlobalClass.ObjectToString(sourceItem?.FieldRule1);
							if (!string.IsNullOrEmpty(text2))
							{
								if (!string.IsNullOrEmpty(text) && text2.IndexOf(text) > -1)
								{
									List<string> list2 = text2.Split(new string[1]
									{
										text
									}, StringSplitOptions.None).ToList();
									int num2 = 0;
									foreach (string item2 in list2)
									{
										string valueItemTrimed = item2.Trim();
										if (!string.IsNullOrEmpty(valueItemTrimed) && !Images.Exists((ImageItem a) => a.ImageUrl.Equals(valueItemTrimed)))
										{
											ImageItem ımageItem2 = new ImageItem();
											ımageItem2.ImageId = num2;
											ımageItem2.ImageUrl = valueItemTrimed;
											Images.Add(ımageItem2);
											num2++;
											num++;
											flag2 = true;
										}
									}
								}
								else
								{
									ImageItem ımageItem3 = new ImageItem();
									ımageItem3.ImageId = num;
									ımageItem3.ImageUrl = text2;
									Images.Add(ımageItem3);
									flag2 = false;
								}
							}
						}
						if (sourceItem.MappingItemType == 602 && Images != null && Images.Any())
						{
							if (flag2)
							{
								if (xElement != null)
								{
									Images.ForEach(delegate(ImageItem x)
									{
										x.ImageSeo = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
									});
								}
								if (dataRow != null)
								{
									Images.ForEach(delegate(ImageItem x)
									{
										x.ImageSeo = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
									});
								}
							}
							else
							{
								if (xElement != null)
								{
									Images.LastOrDefault().ImageSeo = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									Images.LastOrDefault().ImageSeo = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
						}
					}
				}
			}
		}

		private void GetCustomer(SourceMapping sourceMapping, TaskMapping taskMapping, XElement xElement, DataRow dataRow)
		{
			if (sourceMapping.SourceCustomerMapping?.Items != null && sourceMapping.SourceCustomerMapping.Items.Any())
			{
				List<IGrouping<Guid, SourceItemMapping>> list = (from x in sourceMapping.SourceCustomerMapping.Items
					where x.MappingItemType > -1
					select x into u
					group u by u.Id).ToList();
				foreach (IGrouping<Guid, SourceItemMapping> item in list)
				{
					bool flag = false;
					foreach (SourceItemMapping sourceItem in item)
					{
						TaskItemMapping taskItemMapping = taskMapping?.TaskCustomerMapping?.Items?.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
						if (taskItemMapping?.FieldImport ?? true)
						{
							string defaultValue = (taskItemMapping == null) ? sourceItem.FieldRule : taskItemMapping.FieldRule;
							flag = (taskItemMapping?.FieldImport ?? false);
							if (sourceItem.MappingItemType == 210)
							{
								if (xElement != null)
								{
									User.Username = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.Username = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 211)
							{
								if (xElement != null)
								{
									User.Email = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.Email = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 212)
							{
								if (xElement != null)
								{
									User.Password = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.Password = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 213)
							{
								if (xElement != null)
								{
									User.FirstName = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.FirstName = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 214)
							{
								if (xElement != null)
								{
									User.LastName = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.LastName = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 217)
							{
								if (xElement != null)
								{
									User.CustomerMainAddress.Company = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.CustomerMainAddress.Company = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 220)
							{
								if (xElement != null)
								{
									User.ManagerOfVendor = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.ManagerOfVendor = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 215)
							{
								if (xElement != null)
								{
									User.Gender = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.Gender = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 216)
							{
								if (xElement != null)
								{
									User.BirthDay = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.BirthDay = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 219)
							{
								if (xElement != null)
								{
									User.Newsletter = GlobalClass.ObjectToBoolNull(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.Newsletter = GlobalClass.ObjectToBoolNull(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 221)
							{
								if (xElement != null)
								{
									User.Active = GlobalClass.ObjectToBoolNull(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.Active = GlobalClass.ObjectToBoolNull(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 218)
							{
								if (xElement != null)
								{
									User.IsTaxExempt = GlobalClass.ObjectToBool(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.IsTaxExempt = GlobalClass.ObjectToBool(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 238)
							{
								if (xElement != null)
								{
									User.CustomerMainAddress.Address1 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.CustomerMainAddress.Address1 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 239)
							{
								if (xElement != null)
								{
									User.CustomerMainAddress.Address2 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.CustomerMainAddress.Address2 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 237)
							{
								if (xElement != null)
								{
									User.CustomerMainAddress.City = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.CustomerMainAddress.City = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 234)
							{
								if (xElement != null)
								{
									User.CustomerMainAddress.Company = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.CustomerMainAddress.Company = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 235)
							{
								if (xElement != null)
								{
									User.CustomerMainAddress.Country = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.CustomerMainAddress.Country = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 244)
							{
								if (xElement != null)
								{
									User.CustomerMainAddress.County = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.CustomerMainAddress.County = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 242)
							{
								if (xElement != null)
								{
									User.CustomerMainAddress.Fax = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.CustomerMainAddress.Fax = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 241)
							{
								if (xElement != null)
								{
									User.CustomerMainAddress.Phone = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.CustomerMainAddress.Phone = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 236)
							{
								if (xElement != null)
								{
									User.CustomerMainAddress.State = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.CustomerMainAddress.State = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 240)
							{
								if (xElement != null)
								{
									User.CustomerMainAddress.ZipPostalCode = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									User.CustomerMainAddress.ZipPostalCode = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
						}
					}
				}
			}
		}

		private void GetCustomerShippingAddress(SourceMapping sourceMapping, TaskMapping taskMapping, XElement xElement, DataRow dataRow)
		{
			CustomerAddressItem customerAddressItem = new CustomerAddressItem();
			List<SourceItemMapping> list = sourceMapping.SourceCustomerShippingAddressMapping?.Items;
			List<TaskItemMapping> list2 = taskMapping?.TaskCustomerShippingAddressMapping?.Items;
			customerAddressItem.Type = "S";
			if (list != null && list.Any())
			{
				List<IGrouping<Guid, SourceItemMapping>> list3 = (from x in list
					where x.MappingItemType > -1
					select x into u
					group u by u.Id).ToList();
				foreach (IGrouping<Guid, SourceItemMapping> item in list3)
				{
					bool flag = false;
					foreach (SourceItemMapping sourceItem in item)
					{
						TaskItemMapping taskItemMapping = list2?.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
						if (taskItemMapping?.FieldImport ?? true)
						{
							string defaultValue = (taskItemMapping == null) ? sourceItem.FieldRule : taskItemMapping.FieldRule;
							flag = (taskItemMapping?.FieldImport ?? false);
							if (sourceItem.MappingItemType == 233)
							{
								if (xElement != null)
								{
									customerAddressItem.Email = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.Email = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 238)
							{
								if (xElement != null)
								{
									customerAddressItem.Address1 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.Address1 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 239)
							{
								if (xElement != null)
								{
									customerAddressItem.Address2 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.Address2 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 237)
							{
								if (xElement != null)
								{
									customerAddressItem.City = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.City = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 234)
							{
								if (xElement != null)
								{
									customerAddressItem.Company = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.Company = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 235)
							{
								if (xElement != null)
								{
									customerAddressItem.Country = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.Country = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 244)
							{
								if (xElement != null)
								{
									customerAddressItem.County = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.County = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 242)
							{
								if (xElement != null)
								{
									customerAddressItem.Fax = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.Fax = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 241)
							{
								if (xElement != null)
								{
									customerAddressItem.Phone = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.Phone = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 236)
							{
								if (xElement != null)
								{
									customerAddressItem.State = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.State = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 240)
							{
								if (xElement != null)
								{
									customerAddressItem.ZipPostalCode = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.ZipPostalCode = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 231)
							{
								if (xElement != null)
								{
									customerAddressItem.FirstName = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.FirstName = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 232)
							{
								if (xElement != null)
								{
									customerAddressItem.LastName = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.LastName = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
						}
					}
				}
				if (!string.IsNullOrEmpty(customerAddressItem.Email) || !string.IsNullOrEmpty(customerAddressItem.FirstName) || !string.IsNullOrEmpty(customerAddressItem.LastName) || !string.IsNullOrEmpty(customerAddressItem.Company) || !string.IsNullOrEmpty(customerAddressItem.Country) || !string.IsNullOrEmpty(customerAddressItem.Address1) || !string.IsNullOrEmpty(customerAddressItem.Address2) || !string.IsNullOrEmpty(customerAddressItem.City))
				{
					User.CustomerAddressItems.Add(customerAddressItem);
				}
			}
		}

		private void GetCustomerBillingAddress(SourceMapping sourceMapping, TaskMapping taskMapping, XElement xElement, DataRow dataRow)
		{
			CustomerAddressItem customerAddressItem = new CustomerAddressItem();
			List<SourceItemMapping> list = sourceMapping.SourceCustomerBillingAddressMapping?.Items;
			List<TaskItemMapping> list2 = taskMapping?.TaskCustomerBillingAddressMapping?.Items;
			customerAddressItem.Type = "B";
			if (list != null && list.Any())
			{
				List<IGrouping<Guid, SourceItemMapping>> list3 = (from x in list
					where x.MappingItemType > -1
					select x into u
					group u by u.Id).ToList();
				foreach (IGrouping<Guid, SourceItemMapping> item in list3)
				{
					bool flag = false;
					foreach (SourceItemMapping sourceItem in item)
					{
						TaskItemMapping taskItemMapping = list2?.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
						if (taskItemMapping?.FieldImport ?? true)
						{
							string defaultValue = (taskItemMapping == null) ? sourceItem.FieldRule : taskItemMapping.FieldRule;
							flag = (taskItemMapping?.FieldImport ?? false);
							if (sourceItem.MappingItemType == 233)
							{
								if (xElement != null)
								{
									customerAddressItem.Email = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.Email = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 238)
							{
								if (xElement != null)
								{
									customerAddressItem.Address1 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.Address1 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 239)
							{
								if (xElement != null)
								{
									customerAddressItem.Address2 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.Address2 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 237)
							{
								if (xElement != null)
								{
									customerAddressItem.City = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.City = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 234)
							{
								if (xElement != null)
								{
									customerAddressItem.Company = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.Company = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 235)
							{
								if (xElement != null)
								{
									customerAddressItem.Country = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.Country = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 244)
							{
								if (xElement != null)
								{
									customerAddressItem.County = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.County = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 242)
							{
								if (xElement != null)
								{
									customerAddressItem.Fax = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.Fax = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 241)
							{
								if (xElement != null)
								{
									customerAddressItem.Phone = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.Phone = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 236)
							{
								if (xElement != null)
								{
									customerAddressItem.State = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.State = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 240)
							{
								if (xElement != null)
								{
									customerAddressItem.ZipPostalCode = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.ZipPostalCode = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 231)
							{
								if (xElement != null)
								{
									customerAddressItem.FirstName = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.FirstName = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 232)
							{
								if (xElement != null)
								{
									customerAddressItem.LastName = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerAddressItem.LastName = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
						}
					}
				}
				if (!string.IsNullOrEmpty(customerAddressItem.Email) || !string.IsNullOrEmpty(customerAddressItem.FirstName) || !string.IsNullOrEmpty(customerAddressItem.LastName) || !string.IsNullOrEmpty(customerAddressItem.Company) || !string.IsNullOrEmpty(customerAddressItem.Country) || !string.IsNullOrEmpty(customerAddressItem.Address1) || !string.IsNullOrEmpty(customerAddressItem.Address2) || !string.IsNullOrEmpty(customerAddressItem.City))
				{
					User.CustomerAddressItems.Add(customerAddressItem);
				}
			}
		}

		private void GetCustomerRole(SourceMapping sourceMapping, TaskMapping taskMapping, XElement xElement, DataRow dataRow)
		{
			if (sourceMapping.SourceCustomerRoleMapping?.Items != null && sourceMapping.SourceCustomerRoleMapping.Items.Any())
			{
				List<IGrouping<Guid, SourceItemMapping>> list = (from x in sourceMapping.SourceCustomerRoleMapping.Items
					where x.MappingItemType > -1
					select x into u
					group u by u.Id).ToList();
				foreach (IGrouping<Guid, SourceItemMapping> item in list)
				{
					bool flag = false;
					foreach (SourceItemMapping sourceItem in item)
					{
						TaskItemMapping taskItemMapping = taskMapping?.TaskCustomerRoleMapping?.Items?.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
						if (taskItemMapping?.FieldImport ?? true)
						{
							string defaultValue = (taskItemMapping == null) ? sourceItem.FieldRule : taskItemMapping.FieldRule;
							flag = (taskItemMapping?.FieldImport ?? false);
							if (sourceItem.MappingItemType == 251)
							{
								string text = "";
								if (xElement != null)
								{
									text = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									text = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
								string text2 = GlobalClass.ObjectToString(sourceItem?.FieldRule1);
								if (!string.IsNullOrEmpty(text))
								{
									if (!string.IsNullOrEmpty(text2) && text.IndexOf(text2) > -1)
									{
										List<string> list2 = text.Split(new string[1]
										{
											text2
										}, StringSplitOptions.None).ToList();
										int num = 0;
										foreach (string item2 in list2)
										{
											string valueItemTrimed = item2.Trim();
											if (!string.IsNullOrEmpty(valueItemTrimed) && !User.CustomerRoleItems.Exists((CustomerRuleItem a) => a.CustomerRule.Equals(valueItemTrimed)))
											{
												CustomerRuleItem customerRuleItem = new CustomerRuleItem();
												customerRuleItem.CustomerRuleId = num;
												customerRuleItem.CustomerRule = valueItemTrimed;
												User.CustomerRoleItems.Add(customerRuleItem);
												num++;
											}
										}
									}
									else
									{
										CustomerRuleItem customerRuleItem2 = new CustomerRuleItem();
										customerRuleItem2.CustomerRuleId = 0;
										customerRuleItem2.CustomerRule = text;
										User.CustomerRoleItems.Add(customerRuleItem2);
									}
								}
							}
						}
					}
				}
			}
		}

		private void GetCustomerOthers(SourceMapping sourceMapping, TaskMapping taskMapping, XElement xElement, DataRow dataRow)
		{
			CustomerOthersItem customerOthersItem = new CustomerOthersItem();
			if (sourceMapping.SourceCustomerOthersMapping?.Items != null && sourceMapping.SourceCustomerOthersMapping.Items.Any())
			{
				List<IGrouping<Guid, SourceItemMapping>> list = (from x in sourceMapping.SourceCustomerOthersMapping.Items
					where x.MappingItemType > -1
					select x into u
					group u by u.Id).ToList();
				foreach (IGrouping<Guid, SourceItemMapping> item in list)
				{
					bool flag = false;
					foreach (SourceItemMapping sourceItem in item)
					{
						TaskItemMapping taskItemMapping = taskMapping?.TaskCustomerOthersMapping?.Items?.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
						if (taskItemMapping?.FieldImport ?? true)
						{
							string defaultValue = (taskItemMapping == null) ? sourceItem.FieldRule : taskItemMapping.FieldRule;
							flag = (taskItemMapping?.FieldImport ?? false);
							if (sourceItem.MappingItemType == 261)
							{
								if (xElement != null)
								{
									customerOthersItem.Address3 = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerOthersItem.Address3 = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 262)
							{
								if (xElement != null)
								{
									customerOthersItem.AddressName = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerOthersItem.AddressName = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 264)
							{
								if (xElement != null)
								{
									customerOthersItem.GPCustomer = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerOthersItem.GPCustomer = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 266)
							{
								if (xElement != null)
								{
									customerOthersItem.MainAddress = GlobalClass.ObjectToBoolNull(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerOthersItem.MainAddress = GlobalClass.ObjectToBoolNull(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 263)
							{
								if (xElement != null)
								{
									customerOthersItem.SAPCustomer = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerOthersItem.SAPCustomer = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
							if (sourceItem.MappingItemType == 265)
							{
								if (xElement != null)
								{
									customerOthersItem.AddressSAPCustomer = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
								}
								if (dataRow != null)
								{
									customerOthersItem.AddressSAPCustomer = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
								}
							}
						}
					}
				}
				User.CustomerOthersItem = customerOthersItem;
			}
		}

		private void GetAddressCustomAttributes(SourceMapping sourceMapping, TaskMapping taskMapping, XElement xElement, DataRow dataRow)
		{
			int num = 0;
			AttributeValues attributeValues = new AttributeValues();
			List<IGrouping<Guid, SourceItemMapping>> list = (from x in sourceMapping.SourceAddressCustomAttMapping.Items
				where x.MappingItemType > -1
				select x into u
				group u by u.Id).ToList();
			foreach (IGrouping<Guid, SourceItemMapping> item in list)
			{
				int attInfo_OptionType = 0;
				string attName = null;
				string attValue = null;
				bool attInfo_IsRequired = false;
				bool ımport = false;
				bool attValue_IsPreselected = false;
				attributeValues = new AttributeValues();
				foreach (SourceItemMapping sourceItem in item)
				{
					TaskItemMapping taskItemMapping = taskMapping?.TaskAddressCustomAttMapping?.Items?.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
					if (taskItemMapping?.FieldImport ?? true)
					{
						string defaultValue = (taskItemMapping == null) ? sourceItem.FieldRule : taskItemMapping.FieldRule;
						ımport = (taskItemMapping?.FieldImport ?? false);
						if (sourceItem.MappingItemType == 201)
						{
							if (xElement != null)
							{
								attName = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								attName = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
							}
							attInfo_OptionType = GlobalClass.StringToInteger(taskItemMapping?.FieldRule5);
							attInfo_IsRequired = GlobalClass.ObjectToBool(taskItemMapping?.FieldRule3);
							attValue_IsPreselected = GlobalClass.ObjectToBool(taskItemMapping?.FieldRule4);
						}
						if (sourceItem.MappingItemType == 202)
						{
							if (xElement != null)
							{
								attValue = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								attValue = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
					}
					if (!string.IsNullOrEmpty(attName) && !string.IsNullOrEmpty(attValue) && !string.IsNullOrEmpty(attValue) && !AddressCustomAttributes.Any((AttributeValues x) => x.AttInfo_Name == attName && x.AttValue_Value == attValue))
					{
						attributeValues.AttInfo_Id = num.ToString();
						attributeValues.AttInfo_Name = attName;
						attributeValues.AttValue_Value = attValue;
						attributeValues.AttInfo_IsRequired = attInfo_IsRequired;
						attributeValues.AttValue_IsPreselected = attValue_IsPreselected;
						attributeValues.AttInfo_OptionType = attInfo_OptionType;
						attributeValues.Import = ımport;
						AddressCustomAttributes.Add(attributeValues);
						num++;
					}
				}
			}
		}

		private void GetCustomerCustomAttributes(SourceMapping sourceMapping, TaskMapping taskMapping, XElement xElement, DataRow dataRow)
		{
			int num = 0;
			AttributeValues attributeValues = new AttributeValues();
			List<IGrouping<Guid, SourceItemMapping>> list = (from x in sourceMapping.SourceCustomerCustomAttMapping.Items
				where x.MappingItemType > -1
				select x into u
				group u by u.Id).ToList();
			foreach (IGrouping<Guid, SourceItemMapping> item in list)
			{
				int attInfo_OptionType = 0;
				string attName = null;
				string attValue = null;
				bool attInfo_IsRequired = false;
				bool ımport = false;
				bool attValue_IsPreselected = false;
				attributeValues = new AttributeValues();
				foreach (SourceItemMapping sourceItem in item)
				{
					TaskItemMapping taskItemMapping = taskMapping?.TaskCustomerCustomAttMapping?.Items?.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
					if (taskItemMapping?.FieldImport ?? true)
					{
						string defaultValue = (taskItemMapping == null) ? sourceItem.FieldRule : taskItemMapping.FieldRule;
						ımport = (taskItemMapping?.FieldImport ?? false);
						if (sourceItem.MappingItemType == 203)
						{
							if (xElement != null)
							{
								attName = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								attName = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
							}
							attInfo_OptionType = GlobalClass.StringToInteger(taskItemMapping?.FieldRule5);
							attInfo_IsRequired = GlobalClass.ObjectToBool(taskItemMapping?.FieldRule3);
							attValue_IsPreselected = GlobalClass.ObjectToBool(taskItemMapping?.FieldRule4);
						}
						if (sourceItem.MappingItemType == 204)
						{
							if (xElement != null)
							{
								attValue = GlobalClass.ObjectToString(GetElementValue(xElement, sourceItem.FieldMappingForXml, defaultValue));
							}
							if (dataRow != null)
							{
								attValue = GlobalClass.ObjectToString(GetElementValue(dataRow, sourceItem.FieldMappingForIndex.ToString(), defaultValue));
							}
						}
					}
					if (!string.IsNullOrEmpty(attName) && !string.IsNullOrEmpty(attValue) && !string.IsNullOrEmpty(attValue) && !CustomerCustomAttributes.Any((AttributeValues x) => x.AttInfo_Name == attName && x.AttValue_Value == attValue))
					{
						attributeValues.AttInfo_Id = num.ToString();
						attributeValues.AttInfo_Name = attName;
						attributeValues.AttValue_Value = attValue;
						attributeValues.AttInfo_IsRequired = attInfo_IsRequired;
						attributeValues.AttValue_IsPreselected = attValue_IsPreselected;
						attributeValues.AttInfo_OptionType = attInfo_OptionType;
						attributeValues.Import = ımport;
						CustomerCustomAttributes.Add(attributeValues);
						num++;
					}
				}
			}
		}

		private object GetElementValue(XElement xElement, string xpath, string defaultValue)
		{
			object obj = null;
			if (string.IsNullOrEmpty(xpath) && string.IsNullOrEmpty(defaultValue))
			{
				return obj;
			}
			if (!string.IsNullOrEmpty(xpath))
			{
				if (xpath.IndexOf('@') > -1)
				{
					if (xElement.XPathEvaluate(xpath) != null && ((IEnumerable<object>)xElement.XPathEvaluate(xpath)).Any())
					{
						obj = ((IEnumerable<object>)xElement.XPathEvaluate(xpath)).OfType<XAttribute>().Single().Value;
					}
				}
				else if (xElement.XPathSelectElement(xpath) != null)
				{
					obj = xElement.XPathSelectElement(xpath).Value;
				}
			}
			if (obj == null || string.IsNullOrEmpty(obj.ToString()))
			{
				if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('+') == 0)
				{
					if (defaultValue.Trim().IndexOf("++") == 0)
					{
						return null;
					}
					defaultValue = defaultValue.Substring(1);
				}
				return GetDefaultValueXml(xElement, defaultValue);
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('+') == 0)
			{
				obj = ((defaultValue.Trim().IndexOf("++") != 0) ? (obj?.ToString() + GetDefaultValueXml(xElement, defaultValue.Substring(1))) : (obj?.ToString() + GetDefaultValueXml(xElement, defaultValue.Substring(2))));
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('-') == 0)
			{
				obj = GetDefaultValueXml(xElement, defaultValue.Substring(1)) + obj;
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('*') == 0)
			{
				if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf("*~") == 0)
				{
					obj = GlobalClass.StringToDecimal(GetDefaultValueXml(xElement, defaultValue.Substring(2))) * GlobalClass.StringToDecimal(obj);
					int decimals = 0;
					obj = Math.Round(GlobalClass.StringToDecimal(obj), decimals, MidpointRounding.AwayFromZero);
				}
				else
				{
					obj = GlobalClass.StringToDecimal(GetDefaultValueXml(xElement, defaultValue.Substring(1))) * GlobalClass.StringToDecimal(obj);
				}
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('/') == 0)
			{
				obj = GlobalClass.StringToDecimal(obj) / GlobalClass.StringToDecimal(GetDefaultValueXml(xElement, defaultValue.Substring(1)));
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('~') == 0)
			{
				int decimals2 = GlobalClass.StringToInteger(GetDefaultValueXml(xElement, defaultValue.Substring(1)));
				obj = Math.Round(GlobalClass.StringToDecimal(obj), decimals2, MidpointRounding.AwayFromZero);
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf("replace(", StringComparison.OrdinalIgnoreCase) == 0)
			{
				int num = defaultValue.Trim().IndexOf("replace(", StringComparison.OrdinalIgnoreCase) + "replace(".Length;
				int num2 = defaultValue.Trim().IndexOf(")", num + 1);
				string[] array = defaultValue.Substring(num, num2 - num)?.Split(',');
				if (array.Count() > 1 && obj != null && obj.ToString().IndexOf(array[0], StringComparison.OrdinalIgnoreCase) >= 0)
				{
					obj = Regex.Replace(obj.ToString(), array[0], array[1]);
				}
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf("discountpercent(", StringComparison.OrdinalIgnoreCase) == 0)
			{
				int num3 = defaultValue.Trim().IndexOf("discountpercent(", StringComparison.OrdinalIgnoreCase) + "discountpercent(".Length;
				int num4 = defaultValue.Trim().IndexOf(")", num3 + 1);
				string text = defaultValue.Substring(num3, num4 - num3);
				if (!string.IsNullOrEmpty(text))
				{
					decimal num5 = GlobalClass.StringToDecimal(GetDefaultValueXml(xElement, text));
					if (num5 != 0m)
					{
						obj = Math.Round((100m - num5) * GlobalClass.StringToDecimal(obj) / 100m, 2, MidpointRounding.AwayFromZero);
					}
				}
			}
			return obj;
		}

		private object GetElementValue(string defaultValue2, string mapValue, string defaultValue)
		{
			if (mapValue != "AliExpress" && mapValue != "BestSecret")
			{
				defaultValue2 = null;
			}
			object obj = defaultValue2;
			if (string.IsNullOrEmpty(defaultValue))
			{
				return obj;
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('+') == 0)
			{
				obj = ((defaultValue.Trim().IndexOf("++") != 0) ? (obj?.ToString() + defaultValue.Substring(1)) : (obj?.ToString() + defaultValue.Substring(2)));
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('-') == 0)
			{
				obj = defaultValue.Substring(1) + obj;
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('*') == 0)
			{
				if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf("*~") == 0)
				{
					int decimals = 0;
					obj = GlobalClass.StringToDecimal(defaultValue.Substring(2)) * GlobalClass.StringToDecimal(obj);
					obj = Math.Round(GlobalClass.StringToDecimal(obj), decimals, MidpointRounding.AwayFromZero);
				}
				else
				{
					obj = GlobalClass.StringToDecimal(defaultValue.Substring(1)) * GlobalClass.StringToDecimal(obj);
				}
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('/') == 0)
			{
				obj = GlobalClass.StringToDecimal(obj) / GlobalClass.StringToDecimal(defaultValue.Substring(1));
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('~') == 0)
			{
				int decimals2 = GlobalClass.StringToInteger(defaultValue.Substring(1));
				obj = Math.Round(GlobalClass.StringToDecimal(obj), decimals2, MidpointRounding.AwayFromZero);
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf("replace(", StringComparison.OrdinalIgnoreCase) == 0)
			{
				int num = defaultValue.Trim().IndexOf("replace(", StringComparison.OrdinalIgnoreCase) + "replace(".Length;
				int num2 = defaultValue.Trim().IndexOf(")", num + 1);
				string[] array = defaultValue.Substring(num, num2 - num)?.Split(',');
				if (array.Count() > 1 && obj != null && obj.ToString().IndexOf(array[0], StringComparison.OrdinalIgnoreCase) >= 0)
				{
					obj = Regex.Replace(obj.ToString(), array[0], array[1]);
				}
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf("discountpercent(", StringComparison.OrdinalIgnoreCase) == 0)
			{
				int num3 = defaultValue.Trim().IndexOf("discountpercent(", StringComparison.OrdinalIgnoreCase) + "discountpercent(".Length;
				int num4 = defaultValue.Trim().IndexOf(")", num3 + 1);
				string value = defaultValue.Substring(num3, num4 - num3);
				if (!string.IsNullOrEmpty(value))
				{
					decimal num5 = GlobalClass.StringToDecimal(value);
					if (num5 != 0m)
					{
						obj = Math.Round((100m - num5) * GlobalClass.StringToDecimal(obj) / 100m, 2, MidpointRounding.AwayFromZero);
					}
				}
			}
			if (string.IsNullOrEmpty(obj?.ToString()))
			{
				return defaultValue;
			}
			return obj;
		}

		private object GetElementValue(string defaultValue2, string defaultValue)
		{
			object obj = defaultValue2;
			if (string.IsNullOrEmpty(defaultValue))
			{
				return obj;
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('+') == 0)
			{
				obj = ((defaultValue.Trim().IndexOf("++") != 0) ? (obj?.ToString() + defaultValue.Substring(1)) : (obj?.ToString() + defaultValue.Substring(2)));
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('-') == 0)
			{
				obj = defaultValue.Substring(1) + obj;
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('*') == 0)
			{
				if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf("*~") == 0)
				{
					int decimals = 0;
					obj = GlobalClass.StringToDecimal(defaultValue.Substring(2)) * GlobalClass.StringToDecimal(obj);
					obj = Math.Round(GlobalClass.StringToDecimal(obj), decimals, MidpointRounding.AwayFromZero);
				}
				else
				{
					obj = GlobalClass.StringToDecimal(defaultValue.Substring(1)) * GlobalClass.StringToDecimal(obj);
				}
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('/') == 0)
			{
				obj = GlobalClass.StringToDecimal(obj) / GlobalClass.StringToDecimal(defaultValue.Substring(1));
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('~') == 0)
			{
				int decimals2 = GlobalClass.StringToInteger(defaultValue.Substring(1));
				obj = Math.Round(GlobalClass.StringToDecimal(obj), decimals2, MidpointRounding.AwayFromZero);
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf("replace(", StringComparison.OrdinalIgnoreCase) == 0)
			{
				int num = defaultValue.Trim().IndexOf("replace(", StringComparison.OrdinalIgnoreCase) + "replace(".Length;
				int num2 = defaultValue.Trim().IndexOf(")", num + 1);
				string[] array = defaultValue.Substring(num, num2 - num)?.Split(',');
				if (array.Count() > 1 && obj != null && obj.ToString().IndexOf(array[0], StringComparison.OrdinalIgnoreCase) >= 0)
				{
					obj = Regex.Replace(obj.ToString(), array[0], array[1]);
				}
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf("discountpercent(", StringComparison.OrdinalIgnoreCase) == 0)
			{
				int num3 = defaultValue.Trim().IndexOf("discountpercent(", StringComparison.OrdinalIgnoreCase) + "discountpercent(".Length;
				int num4 = defaultValue.Trim().IndexOf(")", num3 + 1);
				string value = defaultValue.Substring(num3, num4 - num3);
				if (!string.IsNullOrEmpty(value))
				{
					decimal num5 = GlobalClass.StringToDecimal(value);
					if (num5 != 0m)
					{
						obj = Math.Round((100m - num5) * GlobalClass.StringToDecimal(obj) / 100m, 2, MidpointRounding.AwayFromZero);
					}
				}
			}
			if (string.IsNullOrEmpty(obj?.ToString()))
			{
				return defaultValue;
			}
			return obj;
		}

		private object GetElementValue(DataRow dataRow, string colIndexString, string defaultValue)
		{
			object obj = null;
			int num = -1;
			if (string.IsNullOrEmpty(colIndexString) && string.IsNullOrEmpty(defaultValue))
			{
				return obj;
			}
			num = GlobalClass.StringToInteger(colIndexString ?? "-1", -1);
			if (num > -1 && num <= dataRow.ItemArray.Count() - 1)
			{
				obj = dataRow[num];
			}
			if (obj == null || string.IsNullOrEmpty(obj.ToString()))
			{
				if (!string.IsNullOrEmpty(defaultValue) && (defaultValue.Trim().IndexOf('+') == 0 || defaultValue.Trim().IndexOf('-') == 0 || defaultValue.Trim().IndexOf('*') == 0))
				{
					if (defaultValue.Trim().IndexOf("++") == 0)
					{
						return null;
					}
					defaultValue = defaultValue.Substring(1);
				}
				return GetDefaultValuePos(dataRow, defaultValue);
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('+') == 0)
			{
				obj = ((defaultValue.Trim().IndexOf("++") != 0) ? (obj?.ToString() + GetDefaultValuePos(dataRow, defaultValue.Substring(1))) : (obj?.ToString() + GetDefaultValuePos(dataRow, defaultValue.Substring(2))));
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('-') == 0)
			{
				obj = GetDefaultValuePos(dataRow, defaultValue.Substring(1)) + obj;
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('*') == 0)
			{
				if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf("*~") == 0)
				{
					obj = GlobalClass.StringToDecimal(GetDefaultValuePos(dataRow, defaultValue.Substring(2))) * GlobalClass.StringToDecimal(obj);
					int decimals = 0;
					obj = Math.Round(GlobalClass.StringToDecimal(obj), decimals, MidpointRounding.AwayFromZero);
				}
				else
				{
					obj = GlobalClass.StringToDecimal(GetDefaultValuePos(dataRow, defaultValue.Substring(1))) * GlobalClass.StringToDecimal(obj);
				}
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('/') == 0)
			{
				obj = GlobalClass.StringToDecimal(obj) / GlobalClass.StringToDecimal(GetDefaultValuePos(dataRow, defaultValue.Substring(1)));
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('~') == 0)
			{
				int decimals2 = GlobalClass.StringToInteger(GetDefaultValuePos(dataRow, defaultValue.Substring(1)));
				obj = Math.Round(GlobalClass.StringToDecimal(obj), decimals2, MidpointRounding.AwayFromZero);
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf("replace(", StringComparison.OrdinalIgnoreCase) == 0)
			{
				int num2 = defaultValue.Trim().IndexOf("replace(", StringComparison.OrdinalIgnoreCase) + "replace(".Length;
				int num3 = defaultValue.Trim().IndexOf(")", num2 + 1);
				string[] array = defaultValue.Substring(num2, num3 - num2)?.Split(',');
				if (array.Count() > 1 && obj != null && obj.ToString().IndexOf(array[0], StringComparison.OrdinalIgnoreCase) >= 0)
				{
					try
					{
						obj = obj?.ToString()?.Replace(array[0], array[1]);
					}
					catch
					{
						obj = Regex.Replace(obj.ToString(), array[0], array[1]);
					}
				}
			}
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf("discountpercent(", StringComparison.OrdinalIgnoreCase) == 0)
			{
				int num4 = defaultValue.Trim().IndexOf("discountpercent(", StringComparison.OrdinalIgnoreCase) + "discountpercent(".Length;
				int num5 = defaultValue.Trim().IndexOf(")", num4 + 1);
				string text = defaultValue.Substring(num4, num5 - num4);
				if (!string.IsNullOrEmpty(text))
				{
					decimal num6 = GlobalClass.StringToDecimal(GetDefaultValuePos(dataRow, text));
					if (num6 != 0m)
					{
						obj = Math.Round((100m - num6) * GlobalClass.StringToDecimal(obj) / 100m, 2, MidpointRounding.AwayFromZero);
					}
				}
			}
			return obj;
		}

		private List<object> GetElements(XElement xElement, string xpath)
		{
			List<object> result = null;
			if (!string.IsNullOrEmpty(xpath))
			{
				if (xpath.IndexOf('@') > -1)
				{
					if (xElement.XPathEvaluate(xpath) != null)
					{
						result = ((IEnumerable<object>)xElement.XPathEvaluate(xpath)).OfType<object>().ToList();
					}
				}
				else if (xElement.XPathSelectElement(xpath) != null)
				{
					result = xElement.XPathSelectElements(xpath).OfType<object>().ToList();
				}
			}
			return result;
		}

		private string GetDefaultValuePos(DataRow dataRow, string defaultValue)
		{
			string text = defaultValue;
			int num = -1;
			if (!string.IsNullOrEmpty(defaultValue))
			{
				MatchCollection source = Regex.Matches(defaultValue, "\\[\\d+]");
				List<string> list = (from Match match in source
					select match.Value).ToList();
				foreach (string item in list)
				{
					string newValue = GlobalClass.ObjectToString(GetElementValue(dataRow, GlobalClass.StringToInteger(item.Replace("[", "").Replace("]", "").Trim(), -1).ToString(), ""));
					text = text.Replace(item, newValue);
				}
			}
			return text;
		}

		private string GetDefaultValueXml(XElement xElement, string defaultValue)
		{
			string text = defaultValue;
			string text2 = "";
			if (!string.IsNullOrEmpty(defaultValue))
			{
				MatchCollection source = Regex.Matches(defaultValue, "\\[.*?\\]");
				List<string> list = (from Match match in source
					select match.Value).ToList();
				foreach (string item in list)
				{
					text2 = item.Replace("[", "").Replace("]", "").Trim();
					text2 = text2.Replace("{", "[").Replace("}", "]").Trim();
					string newValue = GlobalClass.ObjectToString(GetElementValue(xElement, text2.ToString(), ""));
					text = text.Replace(item, newValue);
				}
			}
			return text;
		}

		private string GetTranformedValueXml(XElement xElement, string defaultValue)
		{
			string text = defaultValue;
			string text2 = "";
			if (!string.IsNullOrEmpty(defaultValue))
			{
				MatchCollection source = Regex.Matches(defaultValue, "\\T[\\w+,\\w+,]");
				List<string> list = (from Match match in source
					select match.Value).ToList();
				foreach (string item in list)
				{
					text2 = item.Replace("[", "").Replace("]", "").Trim();
					string newValue = GlobalClass.ObjectToString(GetElementValue(xElement, text2.ToString(), ""));
					text = text.Replace(item, newValue);
				}
			}
			return text;
		}

		public Image GetImageFromUrl(string imageUrl)
		{
			Image result = null;
			if (string.IsNullOrEmpty(imageUrl))
			{
				return result;
			}
			try
			{
				result = Image.FromFile(imageUrl);
			}
			catch
			{
				WebClient webClient = new WebClient();
				webClient.Headers.Add("user-agent", "ASP.NET WebClient");
				try
				{
					result = Image.FromStream(webClient.OpenRead(imageUrl));
				}
				catch
				{
					webClient.Credentials = new NetworkCredential(_sourceData.SourceUser, _sourceData.SourcePassword);
					result = Image.FromStream(webClient.OpenRead(imageUrl));
				}
			}
			return result;
		}

		public Image GetImage(ImageItem imageItem)
		{
			Image result = null;
			if (string.IsNullOrEmpty(imageItem.ImageUrl))
			{
				return result;
			}
			if (imageItem.IsBinary)
			{
				byte[] array = Convert.FromBase64String(imageItem.ImageUrl);
				if (array != null)
				{
					MemoryStream stream = new MemoryStream(array);
					result = Image.FromStream(stream);
				}
			}
			else
			{
				WebClient webClient = new WebClient();
				webClient.Headers.Add("user-agent", "ASP.NET WebClient");
				try
				{
					if (_sourceData != null)
					{
						webClient.Credentials = new NetworkCredential(_sourceData.SourceUser, _sourceData.SourcePassword);
					}
					result = Image.FromStream(webClient.OpenRead(imageItem.ImageUrl));
				}
				catch (Exception)
				{
					result = Image.FromStream(webClient.OpenRead(imageItem.ImageUrl));
				}
			}
			return result;
		}

		private List<ImageItem> CheckForArrayImage(List<ImageItem> imageItems)
		{
			try
			{
				List<ImageItem> list = new List<ImageItem>();
				if (imageItems != null)
				{
					foreach (ImageItem imageItem in imageItems)
					{
						MatchCollection source = Regex.Matches(imageItem.ImageUrl, "\\{.*?\\}");
						List<string> list2 = (from Match match in source
							select match.Value).ToList();
						if (list2.Any())
						{
							foreach (string item in list2)
							{
								if (item.IndexOf('-') > 0)
								{
									string text = item.Replace("{", "").Replace("}", "").Replace(" ", "");
									List<int> list3 = text.Split('-').Select(int.Parse).ToList();
									for (int i = list3[0]; i <= list3[1]; i++)
									{
										ImageItem ımageItem = (ImageItem)imageItem.Clone();
										ımageItem.ImageUrl = imageItem.ImageUrl.Replace(item.ToString(), i.ToString());
										ımageItem.IsArray = true;
										list.Add(ımageItem);
									}
								}
							}
						}
						else
						{
							list.Add(imageItem);
						}
					}
				}
				return list;
			}
			catch
			{
				return imageItems;
			}
		}
	}
}
