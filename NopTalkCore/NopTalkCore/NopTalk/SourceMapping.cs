using System.ComponentModel.DataAnnotations;

namespace NopTalk
{
	public class SourceMapping
	{
		public int? SourceMappingId
		{
			get;
			set;
		}

		public char? CsvDelimeter
		{
			get;
			set;
		}

		public EntityType EntityType
		{
			get;
			set;
		}

		public int? FirstRowIndex
		{
			get;
			set;
		}

		public int? VendorSourceId
		{
			get;
			set;
		}

		[Required(ErrorMessage = "The product repeat node map field is required.")]
		public string ProdInfoRepeatNode
		{
			get;
			set;
		}

		[Required(ErrorMessage = "The product id map field is required.")]
		public string ProdIdMap
		{
			get;
			set;
		}

		public string ProdGroupByMap
		{
			get;
			set;
		}

		public string ProdSkuMap
		{
			get;
			set;
		}

		public string ProdSkuVal
		{
			get;
			set;
		}

		[Required(ErrorMessage = "The product name map or expresion field is required.")]
		public string ProdNameMap
		{
			get;
			set;
		}

		[Required(ErrorMessage = "The product name map or expresion field is required.")]
		public string ProdNameVal
		{
			get;
			set;
		}

		public string ProdShortDescMap
		{
			get;
			set;
		}

		public string ProdShortDescVal
		{
			get;
			set;
		}

		public string ProdFullDescMap
		{
			get;
			set;
		}

		public string ProdFullDescVal
		{
			get;
			set;
		}

		public string ProdManPartNumMap
		{
			get;
			set;
		}

		public string ProdManPartNumVal
		{
			get;
			set;
		}

		public string ProdCostMap
		{
			get;
			set;
		}

		public string ProdCostVal
		{
			get;
			set;
		}

		public double ProdCostVal2
		{
			get;
			set;
		}

		public string ProdPriceMap
		{
			get;
			set;
		}

		public string ProdPriceVal
		{
			get;
			set;
		}

		public string ProdOldPriceMap
		{
			get;
			set;
		}

		public string ProdOldPriceVal
		{
			get;
			set;
		}

		public string ProdStockMap
		{
			get;
			set;
		}

		public string ProdStockVal
		{
			get;
			set;
		}

		public string SeoMetaKeyMap
		{
			get;
			set;
		}

		public string SeoMetaKeyVal
		{
			get;
			set;
		}

		public string SeoMetaDescMap
		{
			get;
			set;
		}

		public string SeoMetaDescVal
		{
			get;
			set;
		}

		public string SeoMetaTitleMap
		{
			get;
			set;
		}

		public string SeoMetaTitleVal
		{
			get;
			set;
		}

		public string SeoPageMap
		{
			get;
			set;
		}

		public string SeoPageVal
		{
			get;
			set;
		}

		public string CatMap
		{
			get;
			set;
		}

		public string CatVal
		{
			get;
			set;
		}

		public char? CatDelimeter
		{
			get;
			set;
		}

		public string Cat1Map
		{
			get;
			set;
		}

		public string Cat1Val
		{
			get;
			set;
		}

		public int? Cat1Parent
		{
			get;
			set;
		}

		public string Cat2Map
		{
			get;
			set;
		}

		public string Cat2Val
		{
			get;
			set;
		}

		public int Cat2Parent
		{
			get;
			set;
		}

		public string Cat3Map
		{
			get;
			set;
		}

		public string Cat3Val
		{
			get;
			set;
		}

		public int Cat3Parent
		{
			get;
			set;
		}

		public string Cat4Map
		{
			get;
			set;
		}

		public string Cat4Val
		{
			get;
			set;
		}

		public int Cat4Parent
		{
			get;
			set;
		}

		public string ManufactMap
		{
			get;
			set;
		}

		public string ManufactVal
		{
			get;
			set;
		}

		public string ImageMap
		{
			get;
			set;
		}

		public string Image1Map
		{
			get;
			set;
		}

		public string Image2Map
		{
			get;
			set;
		}

		public string Image3Map
		{
			get;
			set;
		}

		public string Image4Map
		{
			get;
			set;
		}

		public string ImageSeoMap
		{
			get;
			set;
		}

		public string ImageSeo1Map
		{
			get;
			set;
		}

		public string ImageSeo2Map
		{
			get;
			set;
		}

		public string ImageSeo3Map
		{
			get;
			set;
		}

		public string ImageSeo4Map
		{
			get;
			set;
		}

		public string ImageSeoVal
		{
			get;
			set;
		}

		public string ImageSeo1Val
		{
			get;
			set;
		}

		public string ImageSeo2Val
		{
			get;
			set;
		}

		public string ImageSeo3Val
		{
			get;
			set;
		}

		public string ImageSeo4Val
		{
			get;
			set;
		}

		public string AttRepeatNodeMap
		{
			get;
			set;
		}

		public string AttNameMap
		{
			get;
			set;
		}

		public string AttNameVal
		{
			get;
			set;
		}

		public string AttSkuMap
		{
			get;
			set;
		}

		public string AttSkuVal
		{
			get;
			set;
		}

		public string AttValueMap
		{
			get;
			set;
		}

		public string AttValueVal
		{
			get;
			set;
		}

		public string AttStockMap
		{
			get;
			set;
		}

		public string AttStockVal
		{
			get;
			set;
		}

		public string AttPriceMap
		{
			get;
			set;
		}

		public string AttPriceVal
		{
			get;
			set;
		}

		public string Att1SkuMap
		{
			get;
			set;
		}

		public string Att1SkuVal
		{
			get;
			set;
		}

		public string Att1NameMap
		{
			get;
			set;
		}

		public string Att1NameVal
		{
			get;
			set;
		}

		public string Att1ValueMap
		{
			get;
			set;
		}

		public string Att1ValueVal
		{
			get;
			set;
		}

		public string Att1StockMap
		{
			get;
			set;
		}

		public string Att1StockVal
		{
			get;
			set;
		}

		public string Att1PriceMap
		{
			get;
			set;
		}

		public string Att1PriceVal
		{
			get;
			set;
		}

		public string Att2SkuMap
		{
			get;
			set;
		}

		public string Att2SkuVal
		{
			get;
			set;
		}

		public string Att2NameMap
		{
			get;
			set;
		}

		public string Att2NameVal
		{
			get;
			set;
		}

		public string Att2ValueMap
		{
			get;
			set;
		}

		public string Att2ValueVal
		{
			get;
			set;
		}

		public string Att2StockMap
		{
			get;
			set;
		}

		public string Att2StockVal
		{
			get;
			set;
		}

		public string Att2PriceMap
		{
			get;
			set;
		}

		public string Att2PriceVal
		{
			get;
			set;
		}

		public string FilterName
		{
			get;
			set;
		}

		public string FilterMap
		{
			get;
			set;
		}

		public string Filter1Name
		{
			get;
			set;
		}

		public string Filter1Map
		{
			get;
			set;
		}

		public string Filter2Name
		{
			get;
			set;
		}

		public string Filter2Map
		{
			get;
			set;
		}

		public string Filter3Name
		{
			get;
			set;
		}

		public string Filter3Map
		{
			get;
			set;
		}

		public string Filter4Name
		{
			get;
			set;
		}

		public string Filter4Map
		{
			get;
			set;
		}

		public string Filter5Name
		{
			get;
			set;
		}

		public string Filter5Map
		{
			get;
			set;
		}

		public string Filter6Name
		{
			get;
			set;
		}

		public string Filter6Map
		{
			get;
			set;
		}

		public string Filter7Name
		{
			get;
			set;
		}

		public string Filter7Map
		{
			get;
			set;
		}

		public string Filter8Name
		{
			get;
			set;
		}

		public string Filter8Map
		{
			get;
			set;
		}

		public string Filter9Name
		{
			get;
			set;
		}

		public string Filter9Map
		{
			get;
			set;
		}

		public bool CatIsId
		{
			get;
			set;
		}

		public bool Cat1IsId
		{
			get;
			set;
		}

		public bool Cat2IsId
		{
			get;
			set;
		}

		public bool Cat3IsId
		{
			get;
			set;
		}

		public bool Cat4IsId
		{
			get;
			set;
		}

		public bool ManufacturerIsId
		{
			get;
			set;
		}

		public bool VendorIsId
		{
			get;
			set;
		}

		public bool DeliveryDateIsId
		{
			get;
			set;
		}

		public string VendorMap
		{
			get;
			set;
		}

		public string VendorVal
		{
			get;
			set;
		}

		public bool ImageIsBinary
		{
			get;
			set;
		}

		public bool Image1IsBinary
		{
			get;
			set;
		}

		public bool Image2IsBinary
		{
			get;
			set;
		}

		public bool Image3IsBinary
		{
			get;
			set;
		}

		public bool Image4IsBinary
		{
			get;
			set;
		}

		public string ImageVal
		{
			get;
			set;
		}

		public string Image1Val
		{
			get;
			set;
		}

		public string Image2Val
		{
			get;
			set;
		}

		public string Image3Val
		{
			get;
			set;
		}

		public string Image4Val
		{
			get;
			set;
		}

		public int? RelatedTaskId
		{
			get;
			set;
		}

		public string RelatedSourceAddress
		{
			get;
			set;
		}

		public string TaxCategoryMap
		{
			get;
			set;
		}

		public string TaxCategoryVal
		{
			get;
			set;
		}

		public string GtinMap
		{
			get;
			set;
		}

		public string GtinVal
		{
			get;
			set;
		}

		public string WeightMap
		{
			get;
			set;
		}

		public string WeightVal
		{
			get;
			set;
		}

		public string LengthMap
		{
			get;
			set;
		}

		public string LengthVal
		{
			get;
			set;
		}

		public string WidthMap
		{
			get;
			set;
		}

		public string WidthVal
		{
			get;
			set;
		}

		public string HeightMap
		{
			get;
			set;
		}

		public string HeightVal
		{
			get;
			set;
		}

		public string CustomerRoleMap
		{
			get;
			set;
		}

		public string CustomerRoleVal
		{
			get;
			set;
		}

		public char? CustomerRoleDelimeter
		{
			get;
			set;
		}

		public string CustomerRole1Map
		{
			get;
			set;
		}

		public string CustomerRole1Val
		{
			get;
			set;
		}

		public string CustomerRole2Map
		{
			get;
			set;
		}

		public string CustomerRole2Val
		{
			get;
			set;
		}

		public string CustomerRole3Map
		{
			get;
			set;
		}

		public string CustomerRole3Val
		{
			get;
			set;
		}

		public string CustomerRole4Map
		{
			get;
			set;
		}

		public string CustomerRole4Val
		{
			get;
			set;
		}

		public bool CustomerRoleIsId
		{
			get;
			set;
		}

		public bool CustomerRole1IsId
		{
			get;
			set;
		}

		public bool CustomerRole2IsId
		{
			get;
			set;
		}

		public bool CustomerRole3IsId
		{
			get;
			set;
		}

		public bool CustomerRole4IsId
		{
			get;
			set;
		}

		public string SpecAttRepeatNodeMap
		{
			get;
			set;
		}

		public string SpecAttNameMap
		{
			get;
			set;
		}

		public string SpecAttNameVal
		{
			get;
			set;
		}

		public string SpecAttValueMap
		{
			get;
			set;
		}

		public string SpecAttValueVal
		{
			get;
			set;
		}

		public string SpecAtt1NameMap
		{
			get;
			set;
		}

		public string SpecAtt1NameVal
		{
			get;
			set;
		}

		public string SpecAtt1ValueMap
		{
			get;
			set;
		}

		public string SpecAtt1ValueVal
		{
			get;
			set;
		}

		public string SpecAtt2NameMap
		{
			get;
			set;
		}

		public string SpecAtt2NameVal
		{
			get;
			set;
		}

		public string SpecAtt2ValueMap
		{
			get;
			set;
		}

		public string SpecAtt2ValueVal
		{
			get;
			set;
		}

		public string SpecAtt3NameMap
		{
			get;
			set;
		}

		public string SpecAtt3NameVal
		{
			get;
			set;
		}

		public string SpecAtt3ValueMap
		{
			get;
			set;
		}

		public string SpecAtt3ValueVal
		{
			get;
			set;
		}

		public string SpecAtt4NameMap
		{
			get;
			set;
		}

		public string SpecAtt4NameVal
		{
			get;
			set;
		}

		public string SpecAtt4ValueMap
		{
			get;
			set;
		}

		public string SpecAtt4ValueVal
		{
			get;
			set;
		}

		public SourceUserMapping SourceUserMapping
		{
			get;
			set;
		}

		public string ProdSettingsMinStockQtyMap
		{
			get;
			set;
		}

		public string ProdSettingsMinStockQtyVal
		{
			get;
			set;
		}

		public string ProdSettingsNotifyQtyMap
		{
			get;
			set;
		}

		public string ProdSettingsNotifyQtyVal
		{
			get;
			set;
		}

		public string ProdSettingsMinCartQtyMap
		{
			get;
			set;
		}

		public string ProdSettingsMinCartQtyVal
		{
			get;
			set;
		}

		public string ProdSettingsMaxCartQtyMap
		{
			get;
			set;
		}

		public string ProdSettingsMaxCartQtyVal
		{
			get;
			set;
		}

		public string ProdSettingsAllowedQtyMap
		{
			get;
			set;
		}

		public string ProdSettingsAllowedQtyVal
		{
			get;
			set;
		}

		public string ProdSettingsShippingEnabledMap
		{
			get;
			set;
		}

		public string ProdSettingsShippingEnabledVal
		{
			get;
			set;
		}

		public string ProdSettingsFreeShippingMap
		{
			get;
			set;
		}

		public string ProdSettingsFreeShippingVal
		{
			get;
			set;
		}

		public string ProdSettingsShipSeparatelyMap
		{
			get;
			set;
		}

		public string ProdSettingsShipSeparatelyVal
		{
			get;
			set;
		}

		public string ProdSettingsShippingChargeMap
		{
			get;
			set;
		}

		public string ProdSettingsShippingChargeVal
		{
			get;
			set;
		}

		public string ProdSettingsDeliveryDateMap
		{
			get;
			set;
		}

		public string ProdSettingsDeliveryDateVal
		{
			get;
			set;
		}

		public string ProdSettingsVisibleIndividuallyMap
		{
			get;
			set;
		}

		public string ProdSettingsVisibleIndividuallyVal
		{
			get;
			set;
		}

		public string ProdSettingsIsDeletedMap
		{
			get;
			set;
		}

		public string ProdSettingsIsDeletedVal
		{
			get;
			set;
		}

		public SourceTierPricingMapping SourceTierPricingMapping
		{
			get;
			set;
		}

		public SourceProdAttMapping SourceProdAttMapping
		{
			get;
			set;
		}

		public SourceProdSpecAttMapping SourceProdSpecAttMapping
		{
			get;
			set;
		}

		public SourceAddressCustomAttMapping SourceAddressCustomAttMapping
		{
			get;
			set;
		}

		public SourceCustomerCustomAttMapping SourceCustomerCustomAttMapping
		{
			get;
			set;
		}

		public SourceProdInfoMapping SourceProdInfoMapping
		{
			get;
			set;
		}

		public SourceProdPriceMapping SourceProdPriceMapping
		{
			get;
			set;
		}

		public SourceProdPicturesMapping SourceProdPicturesMapping
		{
			get;
			set;
		}

		public SourceCustomerMapping SourceCustomerMapping
		{
			get;
			set;
		}

		public SourceCustomerAddressMapping SourceCustomerShippingAddressMapping
		{
			get;
			set;
		}

		public SourceCustomerAddressMapping SourceCustomerBillingAddressMapping
		{
			get;
			set;
		}

		public SourceCustomerRoleMapping SourceCustomerRoleMapping
		{
			get;
			set;
		}

		public SourceCustomerOrdersMapping SourceCustomerOrdersMapping
		{
			get;
			set;
		}

		public SourceCustomerOthersMapping SourceCustomerOthersMapping
		{
			get;
			set;
		}

		public SourceWishListMapping SourceWishListMapping
		{
			get;
			set;
		}

		public SourceMapping()
		{
			SourceUserMapping = new SourceUserMapping();
			SourceWishListMapping = new SourceWishListMapping();
			SourceTierPricingMapping = new SourceTierPricingMapping();
			SourceProdAttMapping = new SourceProdAttMapping();
			SourceProdInfoMapping = new SourceProdInfoMapping();
			SourceProdPriceMapping = new SourceProdPriceMapping();
			SourceProdSpecAttMapping = new SourceProdSpecAttMapping();
			SourceAddressCustomAttMapping = new SourceAddressCustomAttMapping();
			SourceCustomerCustomAttMapping = new SourceCustomerCustomAttMapping();
			SourceProdPicturesMapping = new SourceProdPicturesMapping();
			SourceCustomerMapping = new SourceCustomerMapping();
			SourceCustomerShippingAddressMapping = new SourceCustomerAddressMapping();
			SourceCustomerBillingAddressMapping = new SourceCustomerAddressMapping();
			SourceCustomerRoleMapping = new SourceCustomerRoleMapping();
			SourceCustomerOrdersMapping = new SourceCustomerOrdersMapping();
			SourceCustomerOthersMapping = new SourceCustomerOthersMapping();
		}
	}
}
