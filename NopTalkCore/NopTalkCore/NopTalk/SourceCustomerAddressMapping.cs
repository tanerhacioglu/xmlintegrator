using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class SourceCustomerAddressMapping
	{
		public List<SourceItemMapping> Items
		{
			get;
			set;
		}

		public SourceCustomerAddressMapping()
		{
			Items = new List<SourceItemMapping>();
		}
	}
}
