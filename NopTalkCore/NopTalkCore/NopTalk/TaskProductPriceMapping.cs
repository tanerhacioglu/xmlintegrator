using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class TaskProductPriceMapping
	{
		public List<TaskItemMapping> Items
		{
			get;
			set;
		}

		public bool ProductPrice_ProductCost_Import => Items.Exists((TaskItemMapping x) => x.MappingItemType == 1001 && x.FieldImport);

		public bool ProductPrice_ProductPrice_Import => Items.Exists((TaskItemMapping x) => x.MappingItemType == 1002 && x.FieldImport);

		public bool ProductPrice_ProductOldPrice_Import => Items.Exists((TaskItemMapping x) => x.MappingItemType == 1003 && x.FieldImport);

		public bool ProductPrice_BasepriceEnabled_Import => Items.Exists((TaskItemMapping x) => x.MappingItemType == 1004 && x.FieldImport);

		public bool ProductPrice_BasepriceAmount_Import => Items.Exists((TaskItemMapping x) => x.MappingItemType == 1005 && x.FieldImport);

		public bool ProductPrice_BasepriceUnitId_Import => Items.Exists((TaskItemMapping x) => x.MappingItemType == 1006 && x.FieldImport);

		public bool ProductPrice_BasepriceBaseAmount_Import => Items.Exists((TaskItemMapping x) => x.MappingItemType == 1007 && x.FieldImport);

		public bool ProductPrice_BasepriceBaseUnitId_Import => Items.Exists((TaskItemMapping x) => x.MappingItemType == 1008 && x.FieldImport);

		public bool ProductPrice_TaxCategory_Import => Items.Exists((TaskItemMapping x) => x.MappingItemType == 1009 && x.FieldImport);

		public bool ProductPrice_TaxExempt_Import => Items.Exists((TaskItemMapping x) => x.MappingItemType == 1010 && x.FieldImport);

		public TaskProductPriceMapping()
		{
			Items = new List<TaskItemMapping>();
		}
	}
}
