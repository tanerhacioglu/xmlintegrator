namespace NopTalk
{
	public class TaskUserMapping
	{
		public string FirstNameVal
		{
			get;
			set;
		}

		public bool FirstNameAction
		{
			get;
			set;
		}

		public string LastNameVal
		{
			get;
			set;
		}

		public bool LastNameAction
		{
			get;
			set;
		}

		public string UserEmailVal
		{
			get;
			set;
		}

		public bool UserEmailAction
		{
			get;
			set;
		}

		public string AddressEmailVal
		{
			get;
			set;
		}

		public bool AddressEmailAction
		{
			get;
			set;
		}

		public string CompanyVal
		{
			get;
			set;
		}

		public bool CompanyAction
		{
			get;
			set;
		}

		public string CountryVal
		{
			get;
			set;
		}

		public bool CountryAction
		{
			get;
			set;
		}

		public string StateVal
		{
			get;
			set;
		}

		public bool StateAction
		{
			get;
			set;
		}

		public string CityVal
		{
			get;
			set;
		}

		public bool CityAction
		{
			get;
			set;
		}

		public string Address1Val
		{
			get;
			set;
		}

		public bool Address1Action
		{
			get;
			set;
		}

		public string Address2Val
		{
			get;
			set;
		}

		public bool Address2Action
		{
			get;
			set;
		}

		public string Address3Val
		{
			get;
			set;
		}

		public bool Address3Action
		{
			get;
			set;
		}

		public string ZipPostalCodeVal
		{
			get;
			set;
		}

		public bool ZipPostalCodeAction
		{
			get;
			set;
		}

		public string PhoneVal
		{
			get;
			set;
		}

		public bool PhoneAction
		{
			get;
			set;
		}

		public string FaxVal
		{
			get;
			set;
		}

		public bool FaxAction
		{
			get;
			set;
		}

		public string PasswordVal
		{
			get;
			set;
		}

		public bool PasswordAction
		{
			get;
			set;
		}

		public string UsernameVal
		{
			get;
			set;
		}

		public bool UsernameAction
		{
			get;
			set;
		}

		public string AddressNameVal
		{
			get;
			set;
		}

		public bool AddressNameAction
		{
			get;
			set;
		}

		public string SAPCustomerVal
		{
			get;
			set;
		}

		public bool SAPCustomerAction
		{
			get;
			set;
		}

		public string GPCustomerVal
		{
			get;
			set;
		}

		public bool GPCustomerAction
		{
			get;
			set;
		}

		public string AddressSAPCustomerVal
		{
			get;
			set;
		}

		public bool AddressSAPCustomerAction
		{
			get;
			set;
		}

		public string MainAddressVal
		{
			get;
			set;
		}

		public bool MainAddressAction
		{
			get;
			set;
		}
	}
}
