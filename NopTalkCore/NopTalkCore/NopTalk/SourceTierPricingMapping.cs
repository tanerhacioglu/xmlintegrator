using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class SourceTierPricingMapping
	{
		public List<SourceItemMapping> SourceTierPricingMappingItems
		{
			get;
			set;
		}

		public SourceTierPricingMapping()
		{
			SourceTierPricingMappingItems = new List<SourceItemMapping>();
		}
	}
}
