using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class SourceProdPriceMapping
	{
		public List<SourceItemMapping> Items
		{
			get;
			set;
		}

		public SourceProdPriceMapping()
		{
			Items = new List<SourceItemMapping>();
		}
	}
}
