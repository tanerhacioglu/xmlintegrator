namespace NopTalk
{
	public enum ManageInventoryMethod
	{
		DontManageStock,
		ManageStock,
		ManageStockByAttributes
	}
}
