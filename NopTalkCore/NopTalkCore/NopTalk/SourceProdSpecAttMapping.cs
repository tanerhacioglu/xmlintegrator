using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class SourceProdSpecAttMapping
	{
		public List<SourceItemMapping> SourceProdSpecAttMappingItems
		{
			get;
			set;
		}

		public SourceProdSpecAttMapping()
		{
			SourceProdSpecAttMappingItems = new List<SourceItemMapping>();
		}
	}
}
