using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class TaskAddressCustomAttMapping
	{
		public bool DeleteBeforeImport
		{
			get;
			set;
		}

		public List<TaskItemMapping> Items
		{
			get;
			set;
		}

		public TaskAddressCustomAttMapping()
		{
			Items = new List<TaskItemMapping>();
		}
	}
}
