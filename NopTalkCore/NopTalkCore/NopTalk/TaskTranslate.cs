namespace NopTalk
{
	public class TaskTranslate
	{
		public int? LanguageId
		{
			get;
			set;
		}

		public bool ProductName
		{
			get;
			set;
		}

		public bool ProductShortDesc
		{
			get;
			set;
		}

		public bool ProductFullDesc
		{
			get;
			set;
		}

		public bool ProductAttributes
		{
			get;
			set;
		}

		public bool CategoryName
		{
			get;
			set;
		}

		public string TranslatorApi
		{
			get;
			set;
		}

		public string SubscribtionKey
		{
			get;
			set;
		}

		public string ReplaceFile
		{
			get;
			set;
		}
	}
}
