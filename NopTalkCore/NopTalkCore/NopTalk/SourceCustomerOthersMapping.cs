using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class SourceCustomerOthersMapping
	{
		public List<SourceItemMapping> Items
		{
			get;
			set;
		}

		public SourceCustomerOthersMapping()
		{
			Items = new List<SourceItemMapping>();
		}
	}
}
