namespace NopTalk
{
	public class TaskEmailSettings
	{
		public string EmailAddressFrom
		{
			get;
			set;
		}

		public string EmailAddressTo
		{
			get;
			set;
		}

		public string EmailDisplayName
		{
			get;
			set;
		}

		public string EmailHost
		{
			get;
			set;
		}

		public string EmailPort
		{
			get;
			set;
		}

		public string EmailUser
		{
			get;
			set;
		}

		public string EmailPassword
		{
			get;
			set;
		}

		public string EmailSubject
		{
			get;
			set;
		}

		public string EmailBody
		{
			get;
			set;
		}

		public bool EmailSSL
		{
			get;
			set;
		}
	}
}
