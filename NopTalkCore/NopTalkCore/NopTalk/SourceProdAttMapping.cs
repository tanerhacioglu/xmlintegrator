using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class SourceProdAttMapping
	{
		public List<SourceItemMapping> SourceProdAttMappingItems
		{
			get;
			set;
		}

		public SourceProdAttMapping()
		{
			SourceProdAttMappingItems = new List<SourceItemMapping>();
		}
	}
}
