using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class SourceCustomerMapping
	{
		public List<SourceItemMapping> Items
		{
			get;
			set;
		}

		public SourceCustomerMapping()
		{
			Items = new List<SourceItemMapping>();
		}
	}
}
