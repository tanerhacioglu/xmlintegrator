using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class TaskProductInfoMapping
	{
		public List<TaskItemMapping> Items
		{
			get;
			set;
		}

		public bool ProductStock_AllowBackInStockSubscriptions_Import => Items.Exists((TaskItemMapping x) => x.MappingItemType == 2001 && x.FieldImport);

		public bool ProductStock_DisableBuyButton_Import => Items.Exists((TaskItemMapping x) => x.MappingItemType == 2002 && x.FieldImport);

		public TaskProductInfoMapping()
		{
			Items = new List<TaskItemMapping>();
		}
	}
}
