using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class TaskCustomerOthersMapping
	{
		public bool DeleteBeforeImport
		{
			get;
			set;
		}

		public List<TaskItemMapping> Items
		{
			get;
			set;
		}

		public TaskCustomerOthersMapping()
		{
			Items = new List<TaskItemMapping>();
		}
	}
}
