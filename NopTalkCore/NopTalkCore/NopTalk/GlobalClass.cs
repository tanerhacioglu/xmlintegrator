using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Xml;

namespace NopTalk
{
	public class GlobalClass
	{
		public static SqlCeDataAdapter adap;

		public static DataTable dt;

		public static string RemoveInvalidXmlChars(string text)
		{
			char[] value = text.Where((char ch) => XmlConvert.IsXmlChar(ch)).ToArray();
			return new string(value);
		}

		public static bool IsInteger(object value)
		{
			bool result = false;
			try
			{
				if (int.TryParse(value.ToString(), out int _))
				{
					result = true;
				}
			}
			catch
			{
			}
			return result;
		}

		public static string ObjectToString(object value)
		{
			try
			{
				if (value != null && !string.IsNullOrEmpty(value.ToString()))
				{
					return value.ToString();
				}
				return string.Empty;
			}
			catch
			{
				return string.Empty;
			}
		}

		public static string ObjectToString2(object value, bool useTitleCase)
		{
			try
			{
				if (value != null && !string.IsNullOrEmpty(value.ToString()))
				{
					if (useTitleCase)
					{
						return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value.ToString().ToLower());
					}
					return value.ToString();
				}
				return string.Empty;
			}
			catch
			{
				return string.Empty;
			}
		}

		public static string ObjectToDistinctString(object value)
		{
			try
			{
				if (value != null && !string.IsNullOrEmpty(value.ToString()))
				{
					value = string.Join(" ", value.ToString().Split(' ').Distinct());
					return value.ToString().Trim();
				}
				return string.Empty;
			}
			catch
			{
				return string.Empty;
			}
		}

		public static bool ObjectToBool(object value)
		{
			try
			{
				if (value != null && !string.IsNullOrEmpty(value.ToString()))
				{
					if (value.ToString() == "1")
					{
						return true;
					}
					if (value.ToString() == "0")
					{
						return false;
					}
					return Convert.ToBoolean(value);
				}
			}
			catch
			{
			}
			return false;
		}

		public static bool? ObjectToBoolNull(object value)
		{
			try
			{
				if (value != null && !string.IsNullOrEmpty(value.ToString()))
				{
					return Convert.ToBoolean(value);
				}
			}
			catch
			{
			}
			return null;
		}

		public static char? ObjectTChar(object value)
		{
			try
			{
				if (value != null && !string.IsNullOrEmpty(value.ToString()))
				{
					return Convert.ToChar(value);
				}
				return null;
			}
			catch
			{
				return null;
			}
		}

		public static string NameWithoutDomain(string username)
		{
			if (!string.IsNullOrEmpty(username) && username.IndexOf('\\') > -1)
			{
				string[] array = username.Split('\\');
				return array[1];
			}
			return username;
		}

		public static string GetDomainFromUsername(string username)
		{
			if (!string.IsNullOrEmpty(username) && username.IndexOf('\\') > -1)
			{
				string[] array = username.Split('\\');
				return array[0];
			}
			return "";
		}

		public static decimal StringToDecimal(object value, int defaultValue = 0)
		{
			decimal num = 0.5m;
			decimal result = defaultValue;
			try
			{
				if (value == null)
				{
					return result;
				}
				if (num.ToString("N2").IndexOf(",") > 0)
				{
					decimal.TryParse(value.ToString().Replace(".", ","), out result);
				}
				else
				{
					decimal.TryParse(value.ToString().Replace(",", "."), out result);
				}
			}
			catch
			{
			}
			return result;
		}

		public static int StringToInteger(object value, int defaultValue = 0)
		{
			int result = defaultValue;
			try
			{
				if (value == null)
				{
					return defaultValue;
				}
				if (int.TryParse(value.ToString(), out result))
				{
					return result;
				}
				return defaultValue;
			}
			catch
			{
			}
			return result;
		}

		public static DateTime? StringToDatetime(object value, string formatParse = "yyyy-MM-dd h:mm tt")
		{
			try
			{
				if (string.IsNullOrEmpty(value.ToString()))
				{
					return null;
				}
				CultureInfo ınvariantCulture = CultureInfo.InvariantCulture;
				return DateTime.ParseExact(value.ToString(), formatParse, ınvariantCulture);
			}
			catch
			{
			}
			return null;
		}

		public static DataTable ToDataTable<T>(List<T> items)
		{
			DataTable dataTable = new DataTable(typeof(T).Name);
			PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public);
			PropertyInfo[] array = properties;
			foreach (PropertyInfo propertyInfo in array)
			{
				dataTable.Columns.Add(propertyInfo.Name);
			}
			foreach (T item in items)
			{
				object[] array2 = new object[properties.Length];
				for (int j = 0; j < properties.Length; j++)
				{
					array2[j] = properties[j].GetValue(item, null);
				}
				dataTable.Rows.Add(array2);
			}
			return dataTable;
		}

		public static bool IsMapped(object obj1, object obj2 = null)
		{
			if (obj1 != null && !string.IsNullOrEmpty(obj1.ToString().Trim()) && obj1.ToString().Trim() != "-1")
			{
				return true;
			}
			if (obj2 != null && !string.IsNullOrEmpty(obj2.ToString().Trim()))
			{
				return true;
			}
			return false;
		}
	}
}
