using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class TaskWishListMapping
	{
		public bool DeleteBeforeImport
		{
			get;
			set;
		}

		public List<TaskItemMapping> Items
		{
			get;
			set;
		}

		public bool WishList_ProductSKU_Import => Items.Exists((TaskItemMapping x) => x.MappingItemType == 3001 && x.FieldImport);

		public bool WishList_UserEmail_Import => Items.Exists((TaskItemMapping x) => x.MappingItemType == 3003 && x.FieldImport);

		public bool WishList_UserName_Import => Items.Exists((TaskItemMapping x) => x.MappingItemType == 3002 && x.FieldImport);

		public bool WishList_CustomSKUID_Import => Items.Exists((TaskItemMapping x) => x.MappingItemType == 3004 && x.FieldImport);

		public TaskWishListMapping()
		{
			Items = new List<TaskItemMapping>();
		}
	}
}
