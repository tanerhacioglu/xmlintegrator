using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class TaskCustomerAddressMapping
	{
		public bool DeleteBeforeImport
		{
			get;
			set;
		}

		public List<TaskItemMapping> Items
		{
			get;
			set;
		}

		public TaskCustomerAddressMapping()
		{
			Items = new List<TaskItemMapping>();
		}
	}
}
