using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class TaskProductSpecAttributesMapping
	{
		public List<TaskItemMapping> TaskProductSpecAttributesMappingItems
		{
			get;
			set;
		}

		public TaskProductSpecAttributesMapping()
		{
			TaskProductSpecAttributesMappingItems = new List<TaskItemMapping>();
		}
	}
}
