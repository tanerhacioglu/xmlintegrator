using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class SourceWishListMapping
	{
		public List<SourceItemMapping> Items
		{
			get;
			set;
		}

		public SourceWishListMapping()
		{
			Items = new List<SourceItemMapping>();
		}
	}
}
