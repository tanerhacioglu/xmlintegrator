using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class TaskProductAttributesMapping
	{
		public List<TaskItemMapping> TaskProductAttributesMappingItems
		{
			get;
			set;
		}

		public bool ProdAttInfo_Name_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 4 && x.FieldImport);

		public bool ProdAttInfo_IsRequired_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 106 && x.FieldImport);

		public bool ProdAttInfo_ControlType_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 107 && x.FieldImport);

		public bool ProdAttInfo_DefaultValue_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 108 && x.FieldImport);

		public bool ProdAttInfo_DisplayOrder_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 109 && x.FieldImport);

		public bool ProdAttInfo_TextPrompt_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 110 && x.FieldImport);

		public bool ProdAttValue_Value_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 5 && x.FieldImport);

		public bool ProdAttValue_Price_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 7 && x.FieldImport);

		public bool ProdAttValue_AssociatedPicture_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 10 && x.FieldImport);

		public bool ProdAttValue_SquarePicture_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 12 && x.FieldImport);

		public bool ProdAttValue_Color_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 11 && x.FieldImport);

		public bool ProdAttValue_PriceUsePercent_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 101 && x.FieldImport);

		public bool ProdAttValue_WeightAdjustment_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 102 && x.FieldImport);

		public bool ProdAttValue_Cost_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 103 && x.FieldImport);

		public bool ProdAttValue_Quantity_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 6 && x.FieldImport);

		public bool ProdAttValue_IsPreselected_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 104 && x.FieldImport);

		public bool ProdAttValue_DisplayOrder_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 105 && x.FieldImport);

		public bool ProdAttCombination_Sku_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 3 && x.FieldImport);

		public bool ProdAttCombination_Quantity_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 112 && x.FieldImport);

		public bool ProdAttCombination_AllowOutOfStock_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 113 && x.FieldImport);

		public bool ProdAttCombination_Price_Import => TaskProductAttributesMappingItems.Exists((TaskItemMapping x) => x.MappingItemType == 111 && x.FieldImport);

		public TaskProductAttributesMapping()
		{
			TaskProductAttributesMappingItems = new List<TaskItemMapping>();
		}
	}
}
