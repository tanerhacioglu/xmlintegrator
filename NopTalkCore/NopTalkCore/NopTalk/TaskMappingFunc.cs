using Newtonsoft.Json;
using NopTalkCore;
using System;
using System.Data;
using System.Data.SqlServerCe;
using System.Linq;
using System.Linq.Expressions;

namespace NopTalk
{
	public class TaskMappingFunc
	{
		private string _settingsFilePath = "";

		private DataSettingsManager dataSettingsManager = new DataSettingsManager();

		public TaskMappingFunc()
		{
		}

		public TaskMappingFunc(string settingsFilePath)
		{
			_settingsFilePath = settingsFilePath;
		}

		public int InsertTaskMapping(TaskMapping taskMapping)
		{
			int num = 0;
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString);
			string commandText = "\r\n            INSERT INTO [Task]\r\n                   (\r\n                    [VendorSourceId],\r\n                    [EShopId],\r\n                    [TaskName],\r\n                    [TaskAction],\r\n                    [TaskStatusId],\r\n                    [ProdNameVal],\r\n                    [ProdNameAction],\r\n                    [ProdManPartNumVal],\r\n                    [ProdManPartNumAction],\r\n                    [ProdCostVal],\r\n                    [ProdCostAction],\r\n                    [ProdPriceVal],\r\n                    [ProdPriceAddPercentVal],\r\n                    [ProdPriceAddUnitVal],\r\n                    [ProdPriceAction],\r\n                    [ProdStockVal],\r\n                    [ProdStockAction],\r\n                    [SeoMetaKeyVal],\r\n                    [SeoMetaKeyAction],\r\n                    [SeoMetaDescVal],\r\n                    [SeoMetaDescAction],\r\n                    [SeoMetaTitleVal],\r\n                    [SeoMetaTitleAction],\r\n                    [SeoPageVal],\r\n                    [SeoPageAction],\r\n                    [CatVal],\r\n                    [CatAction],\r\n                    [Cat1Val],\r\n                    [Cat1Action],\r\n                    [Cat2Val],\r\n                    [Cat2Action],\r\n                    [Cat3Val]       ,\r\n                    [Cat3Action]      ,\r\n                    [Cat4Val]       ,\r\n                    [Cat4Action]      ,\r\n                    [ManufactVal]   ,\r\n                    [ManufactAction]  ,\r\n                    [ImageAction]     ,\r\n                    [Image1Action]    ,\r\n                    [Image2Action]    ,\r\n                    [Image3Action]    ,\r\n                    [Image4Action]    ,\r\n                    [AttNameVal]    ,\r\n                    [AttNameAction]   ,\r\n                    [AttValueVal]   ,\r\n                    [AttValueAction]  ,\r\n                    [AttStockVal]   ,\r\n                    [AttStockAction]  ,\r\n                    [Att1NameVal]   ,\r\n                    [Att1NameAction]  ,\r\n                    [Att1ValueVal]  ,\r\n                    [Att1ValueAction] ,\r\n                    [Att1StockVal],\r\n                    [Att1StockAction] ,\r\n                    [Att2NameVal]   ,\r\n                    [Att2NameAction]  ,\r\n                    [Att2ValueVal]  ,\r\n                    [Att2ValueAction] ,\r\n                    [Att2StockVal],\r\n                    [Att2StockAction] ,\r\n                    [FilterAction]  ,\r\n                    [FilterVal]     ,\r\n                    [Filter1Action] ,\r\n                    [Filter1Val]    ,\r\n                    [Filter2Action] ,\r\n                    [Filter2Val]    ,\r\n                    [Filter3Action] ,\r\n                    [Filter3Val]    ,\r\n                    [Filter4Action] ,\r\n                    [Filter4Val]    ,\r\n                    [Filter5Action] ,\r\n                    [Filter5Val]    ,\r\n                    [Filter6Action] ,\r\n                    [Filter6Val]    ,\r\n                    [Filter7Action] ,\r\n                    [Filter7Val]    ,\r\n                    [Filter8Action] ,\r\n                    [Filter8Val]    ,\r\n                    [Filter9Action] ,\r\n                    [Filter9Val]    ,\r\n                    [LimitedToStores],\r\n                    [DisableShop],\r\n\t                [ScheduleType],\r\n                    [ScheduleInterval],\r\n                    [ScheduleHour],\r\n                    [ScheduleMinute],\r\n                    [UnpublishProducts],\r\n                    [ManageInventoryMethodId],\r\n                    [DeliveryDayId],\r\n                    [AttControlTypeId],\r\n                    [Att1ControlTypeId],\r\n                    [Att2ControlTypeId],\r\n                    [ProductPublish],\r\n                    [Cat1Parent],\r\n                    [Cat2Parent],\r\n                    [Cat3Parent],\r\n                    [Cat4Parent],\r\n                    [CreatedDate],\r\n                    [VendorVal],      \r\n                    [VendorAction],\r\n                    [TaxCategoryVal],      \r\n                    [TaxCategoryAction],\r\n                    [GtinVal],      \r\n                    [GtinAction],\r\n                    [WeightVal],      \r\n                    [WeightAction],\r\n                    [SavePicturesInFile],      \r\n                    [SavePicturesPath],\r\n                    [DeleteSourceFile]     ,\r\n                    [RelatedTaskId],\r\n                    [RelatedSourceAddress],\r\n                    [ConfigData]\r\n                    )  \r\n             VALUES\r\n                   (\r\n                    @VendorSourceId,\r\n                    @EShopId,\r\n                    @TaskName,\r\n                    @TaskAction,\r\n                    @TaskStatusId,\r\n                    @ProdNameVal,\r\n                    @ProdNameAction,\r\n                    @ProdManPartNumVal,\r\n                    @ProdManPartNumAction,\r\n                    @ProdCostVal,\r\n                    @ProdCostAction,\r\n                    @ProdPriceVal,\r\n                    @ProdPriceAddPercentVal,\r\n                    @ProdPriceAddUnitVal,\r\n                    @ProdPriceAction,\r\n                    @ProdStockVal,\r\n                    @ProdStockAction,\r\n                    @SeoMetaKeyVal,\r\n                    @SeoMetaKeyAction,\r\n                    @SeoMetaDescVal,\r\n                    @SeoMetaDescAction,\r\n                    @SeoMetaTitleVal,\r\n                    @SeoMetaTitleAction,\r\n                    @SeoPageVal,\r\n                    @SeoPageAction,\r\n                    @CatVal,\r\n                    @CatAction,\r\n                    @Cat1Val,\r\n                    @Cat1Action,\r\n                    @Cat2Val,\r\n                    @Cat2Action,\r\n                    @Cat3Val       ,\r\n                    @Cat3Action      ,\r\n                    @Cat4Val       ,\r\n                    @Cat4Action      ,\r\n                    @ManufactVal   ,\r\n                    @ManufactAction  ,\r\n                    @ImageAction     ,\r\n                    @Image1Action    ,\r\n                    @Image2Action    ,\r\n                    @Image3Action    ,\r\n                    @Image4Action    ,\r\n                    @AttNameVal    ,\r\n                    @AttNameAction   ,\r\n                    @AttValueVal   ,\r\n                    @AttValueAction  ,\r\n                    @AttStockVal    ,\r\n                    @AttStockAction  ,\r\n                    @Att1NameVal   ,\r\n                    @Att1NameAction  ,\r\n                    @Att1ValueVal  ,\r\n                    @Att1ValueAction ,\r\n                    @Att1StockVal,\r\n                    @Att1StockAction ,\r\n                    @Att2NameVal   ,\r\n                    @Att2NameAction  ,\r\n                    @Att2ValueVal  ,\r\n                    @Att2ValueAction ,\r\n                    @Att2StockVal,\r\n                    @Att2StockAction ,\r\n                    @FilterAction  ,\r\n                    @FilterVal     ,\r\n                    @Filter1Action ,\r\n                    @Filter1Val    ,\r\n                    @Filter2Action ,\r\n                    @Filter2Val    ,\r\n                    @Filter3Action ,\r\n                    @Filter3Val    ,\r\n                    @Filter4Action ,\r\n                    @Filter4Val    ,\r\n                    @Filter5Action ,\r\n                    @Filter5Val    ,\r\n                    @Filter6Action ,\r\n                    @Filter6Val    ,\r\n                    @Filter7Action ,\r\n                    @Filter7Val    ,\r\n                    @Filter8Action ,\r\n                    @Filter8Val    ,\r\n                    @Filter9Action ,\r\n                    @Filter9Val    ,\r\n                    @LimitedToStores,\r\n                    @DisableShop,\r\n                    @ScheduleType,\r\n                    @ScheduleInterval,\r\n                    @ScheduleHour,\r\n                    @ScheduleMinute,\r\n                    @UnpublishProducts,\r\n                    @ManageInventoryMethodId,\r\n                    @DeliveryDayId,\r\n                    @AttControlTypeId,\r\n                    @Att1ControlTypeId,\r\n                    @Att2ControlTypeId,\r\n                    @ProductPublish,\r\n                    @Cat1Parent,\r\n                    @Cat2Parent,\r\n                    @Cat3Parent,\r\n                    @Cat4Parent,\r\n                    @CreatedDate,\r\n                    @VendorVal,      \r\n                    @VendorAction,\r\n                    @TaxCategoryVal,      \r\n                    @TaxCategoryAction,\r\n                    @GtinVal,      \r\n                    @GtinAction,\r\n                    @WeightVal,      \r\n                    @WeightAction,\r\n                    @SavePicturesInFile,      \r\n                    @SavePicturesPath,\r\n                    @DeleteSourceFile,\r\n                    @RelatedTaskId,\r\n                    @RelatedSourceAddress,\r\n                    @ConfigData\r\n                    )";
			SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorSourceId", taskMapping.VendorSourceId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@EShopId", taskMapping.EShopId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaskName", taskMapping.TaskName.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaskAction", taskMapping.TaskAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaskStatusId", taskMapping.TaskStatusId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdNameVal", taskMapping.ProdNameVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdNameAction", taskMapping.ProdNameAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdManPartNumVal", taskMapping.ProdManPartNumVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdManPartNumAction", taskMapping.ProdManPartNumAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdCostVal", taskMapping.ProdCostVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdCostAction", taskMapping.ProdCostAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdPriceVal", taskMapping.ProdPriceVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdPriceAddPercentVal", taskMapping.ProdPriceAddPercentVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdPriceAddUnitVal", taskMapping.ProdPriceAddUnitVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdPriceAction", taskMapping.ProdPriceAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdStockVal", taskMapping.ProdStockVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdStockAction", taskMapping.ProdStockAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SeoMetaKeyVal", taskMapping.SeoMetaKeyVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SeoMetaKeyAction", taskMapping.SeoMetaKeyAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SeoMetaDescVal", taskMapping.SeoMetaDescVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SeoMetaDescAction", taskMapping.SeoMetaDescAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SeoMetaTitleVal", taskMapping.SeoMetaTitleVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SeoMetaTitleAction", taskMapping.SeoMetaTitleAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SeoPageVal", taskMapping.SeoPageVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SeoPageAction", taskMapping.SeoPageAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@CatVal", taskMapping.CatVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@CatAction", taskMapping.CatAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat1Val", taskMapping.Cat1Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat1Action", taskMapping.Cat1Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat2Val", taskMapping.Cat2Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat2Action", taskMapping.Cat2Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat3Val", taskMapping.Cat3Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat3Action", taskMapping.Cat3Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat4Val", taskMapping.Cat4Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat4Action", taskMapping.Cat4Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat1Parent", taskMapping.Cat1Parent.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat2Parent", taskMapping.Cat2Parent.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat3Parent", taskMapping.Cat3Parent.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat4Parent", taskMapping.Cat4Parent.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ManufactVal", taskMapping.ManufactVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ManufactAction", taskMapping.ManufactAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ImageAction", taskMapping.ImageAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Image1Action", taskMapping.Image1Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Image2Action", taskMapping.Image2Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Image3Action", taskMapping.Image3Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Image4Action", taskMapping.Image4Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@AttNameVal", taskMapping.AttNameVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@AttNameAction", taskMapping.AttNameAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@AttValueVal", taskMapping.AttValueVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@AttValueAction", taskMapping.AttValueAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@AttStockVal", taskMapping.AttStockVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@AttStockAction", taskMapping.AttStockAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att1NameVal", taskMapping.Att1NameVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att1NameAction", taskMapping.Att1NameAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att1ValueVal", taskMapping.Att1ValueVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att1ValueAction", taskMapping.Att1ValueAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att1StockVal", taskMapping.Att1StockVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att1StockAction", taskMapping.Att1StockAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att2NameVal", taskMapping.Att2NameVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att2NameAction", taskMapping.Att2NameAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att2ValueVal", taskMapping.Att2ValueVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att2ValueAction", taskMapping.Att2ValueAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att2StockVal", taskMapping.Att2StockVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att2StockAction", taskMapping.Att2StockAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@FilterAction", taskMapping.FilterAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@FilterVal", taskMapping.FilterVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter1Action", taskMapping.Filter1Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter1Val", taskMapping.Filter1Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter2Action", taskMapping.Filter2Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter2Val", taskMapping.Filter2Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter3Action", taskMapping.Filter3Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter3Val", taskMapping.Filter3Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter4Action", taskMapping.Filter4Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter4Val", taskMapping.Filter4Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter5Action", taskMapping.Filter5Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter5Val", taskMapping.Filter5Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter6Action", taskMapping.Filter6Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter6Val", taskMapping.Filter6Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter7Action", taskMapping.Filter7Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter7Val", taskMapping.Filter7Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter8Action", taskMapping.Filter8Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter8Val", taskMapping.Filter8Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter9Action", taskMapping.Filter9Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter9Val", taskMapping.Filter9Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@LimitedToStores", taskMapping.LimitedToStores.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@DisableShop", taskMapping.DisableShop.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ScheduleType", taskMapping.ScheduleType.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ScheduleInterval", taskMapping.ScheduleInterval.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ScheduleHour", taskMapping.ScheduleHour.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ScheduleMinute", taskMapping.ScheduleMinute.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@CreatedDate", DateTime.Now));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorVal", taskMapping.VendorVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorAction", taskMapping.VendorAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@DeleteSourceFile", taskMapping.DeleteSourceFile.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@UnpublishProducts", taskMapping.UnpublishProducts.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProductPublish", taskMapping.ProductPublish.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ManageInventoryMethodId", taskMapping.ManageInventoryMethodId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@DeliveryDayId", taskMapping.DeliveryDayId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@AttControlTypeId", taskMapping.AttControlTypeId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att1ControlTypeId", taskMapping.Att1ControlTypeId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att2ControlTypeId", taskMapping.Att2ControlTypeId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@RelatedTaskId", taskMapping.RelatedTaskId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@RelatedSourceAddress", taskMapping.RelatedSourceAddress.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaxCategoryVal", taskMapping.TaxCategoryVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaxCategoryAction", taskMapping.TaxCategoryAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@GtinVal", taskMapping.GtinVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@GtinAction", taskMapping.GtinAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@WeightVal", taskMapping.WeightVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@WeightAction", taskMapping.WeightAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SavePicturesPath", taskMapping.SavePicturesPath.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SavePicturesInFile", taskMapping.SavePicturesInFile.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ConfigData", JsonConvert.SerializeObject(taskMapping)));
			sqlCeConnection.Open();
			sqlCeCommand.ExecuteNonQuery();
			sqlCeCommand.CommandText = "SELECT @@IDENTITY";
			num = Convert.ToInt32(sqlCeCommand.ExecuteScalar());
			sqlCeConnection.Close();
			if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
			{
				sqlCeConnection.Close();
			}
			sqlCeCommand?.Dispose();
			return num;
		}

		public void UpdateTaskMapping(TaskMapping taskMapping)
		{
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString);
			string commandText = "\r\n                UPDATE  [Task] SET\r\n                        [VendorSourceId] = @VendorSourceId,\r\n                        [EShopId] = @EShopId,\r\n                        [TaskName] = @TaskName,\r\n                        [TaskAction] = @TaskAction,\r\n                        [TaskStatusId] = @TaskStatusId,\r\n                        [ProdNameVal] = @ProdNameVal,\r\n                        [ProdNameAction] = @ProdNameAction,\r\n                        [ProdManPartNumVal] = @ProdManPartNumVal,\r\n                        [ProdManPartNumAction] = @ProdManPartNumAction,\r\n                        [ProdCostVal] = @ProdCostVal,\r\n                        [ProdCostAction] = @ProdCostAction,\r\n                        [ProdPriceVal] = @ProdPriceVal,\r\n                        [ProdPriceAddPercentVal] = @ProdPriceAddPercentVal,\r\n                        [ProdPriceAddUnitVal] = @ProdPriceAddUnitVal,\r\n                        [ProdPriceAction] = @ProdPriceAction,\r\n                        [ProdStockVal] = @ProdStockVal,\r\n                        [ProdStockAction] = @ProdStockAction,\r\n                        [SeoMetaKeyVal] = @SeoMetaKeyVal,\r\n                        [SeoMetaKeyAction] = @SeoMetaKeyAction,\r\n                        [SeoMetaDescVal] = @SeoMetaDescVal,\r\n                        [SeoMetaDescAction] = @SeoMetaDescAction,\r\n                        [SeoMetaTitleVal] = @SeoMetaTitleVal,\r\n                        [SeoMetaTitleAction] = @SeoMetaTitleAction,\r\n                        [SeoPageVal] = @SeoPageVal,\r\n                        [SeoPageAction] = @SeoPageAction,\r\n                        [CatVal] = @CatVal,\r\n                        [CatAction] = @CatAction,\r\n                        [Cat1Val] = @Cat1Val,\r\n                        [Cat1Action] = @Cat1Action,\r\n                        [Cat2Val] = @Cat2Val,\r\n                        [Cat2Action] = @Cat2Action,\r\n                        [Cat3Val] = @Cat3Val,\r\n                        [Cat3Action] = @Cat3Action     ,\r\n                        [Cat4Val]  = @Cat4Val     ,\r\n                        [Cat4Action]   = @Cat4Action   ,\r\n                        [Cat1Parent]   = @Cat1Parent   ,\r\n                        [Cat2Parent]   = @Cat2Parent   ,\r\n                        [Cat3Parent]   = @Cat3Parent   ,\r\n                        [Cat4Parent]   = @Cat4Parent   ,\r\n                        [ManufactVal]  = @ManufactVal ,\r\n                        [ManufactAction] = @ManufactAction ,\r\n                        [ImageAction] = @ImageAction    ,\r\n                        [Image1Action] = @Image1Action   ,\r\n                        [Image2Action]  = @Image2Action  ,\r\n                        [Image3Action] = @Image3Action   ,\r\n                        [Image4Action] = @Image4Action   ,\r\n                        [AttNameVal] = @AttNameVal   ,\r\n                        [AttNameAction] = @AttNameAction  ,\r\n                        [AttValueVal] = @AttValueVal  ,\r\n                        [AttValueAction]  = @AttValueAction,\r\n                        [AttStockVal] = @AttStockVal,\r\n                        [AttStockAction]  = @AttStockAction,\r\n                        [Att1NameVal]  = @Att1NameVal ,\r\n                        [Att1NameAction] = @Att1NameAction ,\r\n                        [Att1ValueVal]  = @Att1ValueVal,\r\n                        [Att1ValueAction] = @Att1ValueAction,\r\n                        [Att1StockVal] = @Att1StockVal,\r\n                        [Att1StockAction] = @Att1StockAction,\r\n                        [Att2NameVal] = @Att2NameVal  ,\r\n                        [Att2NameAction] = @Att2NameAction ,\r\n                        [Att2ValueVal]  = @Att2ValueVal,\r\n                        [Att2ValueAction] = @Att2ValueAction,\r\n                        [Att2StockVal] = @Att2StockVal,\r\n                        [Att2StockAction] = @Att2StockAction ,\r\n                        [FilterAction] = @FilterAction ,\r\n                        [FilterVal] = @FilterVal    ,\r\n                        [Filter1Action] = @Filter1Action,\r\n                        [Filter1Val]  = @Filter1Val  ,\r\n                        [Filter2Action]  = @Filter2Action,\r\n                        [Filter2Val] = @Filter2Val   ,\r\n                        [Filter3Action]  = @Filter3Action,\r\n                        [Filter3Val] = @Filter3Val   ,\r\n                        [Filter4Action] = @Filter4Action,\r\n                        [Filter4Val] = @Filter4Val   ,\r\n                        [Filter5Action] = @Filter5Action ,\r\n                        [Filter5Val]  = @Filter5Val  ,\r\n                        [Filter6Action] =@Filter6Action ,\r\n                        [Filter6Val]  = @Filter6Val  ,\r\n                        [Filter7Action] = @Filter7Action,\r\n                        [Filter7Val]  = @Filter7Val  ,\r\n                        [Filter8Action] = @Filter8Action ,\r\n                        [Filter8Val] = @Filter8Val   ,\r\n                        [Filter9Action] = @Filter9Action,\r\n                        [Filter9Val] = @Filter9Val   ,\r\n                        [LimitedToStores] = @LimitedToStores,\r\n                        [DisableShop] = @DisableShop,\r\n                        [ScheduleType] = @ScheduleType,\r\n                        [ScheduleInterval] = @ScheduleInterval,\r\n                        [ScheduleHour] = @ScheduleHour,\r\n                        [ScheduleMinute] = @ScheduleMinute,\r\n                        [UnpublishProducts] = @UnpublishProducts,\r\n                        [ManageInventoryMethodId] = @ManageInventoryMethodId,\r\n                        [DeliveryDayId] = @DeliveryDayId,\r\n                        [AttControlTypeId] = @AttControlTypeId,\r\n                        [Att1ControlTypeId] = @Att1ControlTypeId,\r\n                        [Att2ControlTypeId] = @Att2ControlTypeId,\r\n                        [ProductPublish] = @ProductPublish,\r\n                        [VendorVal] = @VendorVal,\r\n                        [VendorAction] = @VendorAction,\r\n                        [TaxCategoryVal] = @TaxCategoryVal,\r\n                        [TaxCategoryAction] = @TaxCategoryAction,\r\n                        [GtinVal] = @GtinVal,\r\n                        [GtinAction] = @GtinAction,\r\n                        [WeightVal] = @WeightVal,\r\n                        [WeightAction] = @WeightAction,\r\n                        [SavePicturesPath] = @SavePicturesPath,\r\n                        [SavePicturesInFile] = @SavePicturesInFile,\r\n                        [DeleteSourceFile] = @DeleteSourceFile,\r\n                        [RelatedTaskId] = @RelatedTaskId,\r\n                        [RelatedSourceAddress] = @RelatedSourceAddress,\r\n                        [ConfigData] = @ConfigData\r\n                     WHERE [Id] = @TaskId";
			SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaskId", taskMapping.TaskId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorSourceId", taskMapping.VendorSourceId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@EShopId", taskMapping.EShopId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaskName", taskMapping.TaskName.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaskAction", taskMapping.TaskAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaskStatusId", taskMapping.TaskStatusId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdNameVal", taskMapping.ProdNameVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdNameAction", taskMapping.ProdNameAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdShortDescVal", taskMapping.ProdShortDescVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdShortDescAction", taskMapping.ProdShortDescAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdFullDescVal", taskMapping.ProdFullDescVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdFullDescAction", taskMapping.ProdFullDescAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdManPartNumVal", taskMapping.ProdManPartNumVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdManPartNumAction", taskMapping.ProdManPartNumAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdCostVal", taskMapping.ProdCostVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdCostAction", taskMapping.ProdCostAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdPriceVal", taskMapping.ProdPriceVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdPriceAddPercentVal", taskMapping.ProdPriceAddPercentVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdPriceAddUnitVal", taskMapping.ProdPriceAddUnitVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdPriceAction", taskMapping.ProdPriceAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdStockVal", taskMapping.ProdStockVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProdStockAction", taskMapping.ProdStockAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SeoMetaKeyVal", taskMapping.SeoMetaKeyVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SeoMetaKeyAction", taskMapping.SeoMetaKeyAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SeoMetaDescVal", taskMapping.SeoMetaDescVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SeoMetaDescAction", taskMapping.SeoMetaDescAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SeoMetaTitleVal", taskMapping.SeoMetaTitleVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SeoMetaTitleAction", taskMapping.SeoMetaTitleAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SeoPageVal", taskMapping.SeoPageVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SeoPageAction", taskMapping.SeoPageAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@CatVal", taskMapping.CatVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@CatAction", taskMapping.CatAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat1Val", taskMapping.Cat1Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat1Action", taskMapping.Cat1Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat2Val", taskMapping.Cat2Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat2Action", taskMapping.Cat2Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat3Val", taskMapping.Cat3Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat3Action", taskMapping.Cat3Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat4Val", taskMapping.Cat4Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat4Action", taskMapping.Cat4Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat1Parent", taskMapping.Cat1Parent.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat2Parent", taskMapping.Cat2Parent.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat3Parent", taskMapping.Cat3Parent.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Cat4Parent", taskMapping.Cat4Parent.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ManufactVal", taskMapping.ManufactVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ManufactAction", taskMapping.ManufactAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ImageAction", taskMapping.ImageAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Image1Action", taskMapping.Image1Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Image2Action", taskMapping.Image2Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Image3Action", taskMapping.Image3Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Image4Action", taskMapping.Image4Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@AttNameVal", taskMapping.AttNameVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@AttNameAction", taskMapping.AttNameAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@AttValueVal", taskMapping.AttValueVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@AttValueAction", taskMapping.AttValueAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@AttStockVal", taskMapping.AttStockVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@AttStockAction", taskMapping.AttStockAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att1NameVal", taskMapping.Att1NameVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att1NameAction", taskMapping.Att1NameAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att1ValueVal", taskMapping.Att1ValueVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att1ValueAction", taskMapping.Att1ValueAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att1StockVal", taskMapping.Att1StockVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att1StockAction", taskMapping.Att1StockAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att2NameVal", taskMapping.Att2NameVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att2NameAction", taskMapping.Att2NameAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att2ValueVal", taskMapping.Att2ValueVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att2ValueAction", taskMapping.Att2ValueAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att2StockVal", taskMapping.Att2StockVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att2StockAction", taskMapping.Att2StockAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@FilterAction", taskMapping.FilterAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@FilterVal", taskMapping.FilterVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter1Action", taskMapping.Filter1Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter1Val", taskMapping.Filter1Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter2Action", taskMapping.Filter2Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter2Val", taskMapping.Filter2Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter3Action", taskMapping.Filter3Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter3Val", taskMapping.Filter3Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter4Action", taskMapping.Filter4Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter4Val", taskMapping.Filter4Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter5Action", taskMapping.Filter5Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter5Val", taskMapping.Filter5Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter6Action", taskMapping.Filter6Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter6Val", taskMapping.Filter6Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter7Action", taskMapping.Filter7Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter7Val", taskMapping.Filter7Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter8Action", taskMapping.Filter8Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter8Val", taskMapping.Filter8Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter9Action", taskMapping.Filter9Action.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Filter9Val", taskMapping.Filter9Val.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@LimitedToStores", taskMapping.LimitedToStores.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@DisableShop", taskMapping.DisableShop.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ScheduleType", taskMapping.ScheduleType.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ScheduleInterval", taskMapping.ScheduleInterval.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ScheduleHour", taskMapping.ScheduleHour.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ScheduleMinute", taskMapping.ScheduleMinute.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorVal", taskMapping.VendorVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorAction", taskMapping.VendorAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@UnpublishProducts", taskMapping.UnpublishProducts.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@DeleteSourceFile", taskMapping.DeleteSourceFile.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProductPublish", taskMapping.ProductPublish.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ManageInventoryMethodId", taskMapping.ManageInventoryMethodId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@DeliveryDayId", taskMapping.DeliveryDayId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@AttControlTypeId", taskMapping.AttControlTypeId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att1ControlTypeId", taskMapping.Att1ControlTypeId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@Att2ControlTypeId", taskMapping.Att2ControlTypeId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@RelatedTaskId", taskMapping.RelatedTaskId.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@RelatedSourceAddress", taskMapping.RelatedSourceAddress.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaxCategoryVal", taskMapping.TaxCategoryVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaxCategoryAction", taskMapping.TaxCategoryAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@GtinVal", taskMapping.GtinVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@GtinAction", taskMapping.GtinAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@WeightVal", taskMapping.WeightVal.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@WeightAction", taskMapping.WeightAction.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SavePicturesPath", taskMapping.SavePicturesPath.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SavePicturesInFile", taskMapping.SavePicturesInFile.ReplaceToParam()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ConfigData", JsonConvert.SerializeObject(taskMapping)));
			sqlCeConnection.Open();
			sqlCeCommand.ExecuteNonQuery();
			sqlCeConnection.Close();
			if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
			{
				sqlCeConnection.Close();
			}
			sqlCeCommand?.Dispose();
		}

		public TaskMapping GetTaskMapping(int taskId)
		{
			TaskMapping taskMapping = new TaskMapping();
			string dataConnectionString = dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString;
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataConnectionString);
			string commandText = "select * from Task where Id=@TaskId";
			SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaskId", taskId));
			sqlCeConnection.Open();
			SqlCeDataReader sqlCeDataReader = sqlCeCommand.ExecuteReader();
			if (sqlCeDataReader.Read())
			{
				if (!string.IsNullOrEmpty(GlobalClass.ObjectToString(sqlCeDataReader["ConfigData"])))
				{
					taskMapping = JsonConvert.DeserializeObject<TaskMapping>(GlobalClass.ObjectToString(sqlCeDataReader["ConfigData"]));
					taskMapping.TaskId = taskId;
					taskMapping.VendorSourceId = GlobalClass.StringToInteger(taskMapping?.VendorSourceId);
					taskMapping.EShopId = GlobalClass.StringToInteger(taskMapping?.EShopId);
					taskMapping.TaskName = GlobalClass.ObjectToString(taskMapping?.TaskName);
					taskMapping.TaskAction = GlobalClass.StringToInteger(taskMapping?.TaskAction);
					taskMapping.TaskStatusId = GlobalClass.StringToInteger(taskMapping?.TaskStatusId);
					taskMapping.ProdNameVal = GlobalClass.ObjectToString(taskMapping?.ProdNameVal);
					taskMapping.ProdNameAction = GlobalClass.ObjectToBool(taskMapping?.ProdNameAction);
					taskMapping.ProdShortDescVal = GlobalClass.ObjectToString(taskMapping?.ProdShortDescVal);
					taskMapping.ProdShortDescAction = GlobalClass.ObjectToBool(taskMapping?.ProdShortDescAction);
					taskMapping.ProdFullDescVal = GlobalClass.ObjectToString(taskMapping?.ProdFullDescVal);
					taskMapping.ProdFullDescAction = GlobalClass.ObjectToBool(taskMapping?.ProdFullDescAction);
					taskMapping.ProdManPartNumVal = GlobalClass.ObjectToString(taskMapping?.ProdManPartNumVal);
					taskMapping.ProdManPartNumAction = GlobalClass.ObjectToBool(taskMapping?.ProdManPartNumAction);
					taskMapping.ProdCostVal = GlobalClass.ObjectToString(taskMapping?.ProdCostVal);
					taskMapping.ProdCostAction = GlobalClass.ObjectToBool(taskMapping?.ProdCostAction);
					taskMapping.ProdOldPriceVal = GlobalClass.ObjectToString(taskMapping?.ProdOldPriceVal);
					taskMapping.ProdOldPriceAction = GlobalClass.ObjectToBool(taskMapping?.ProdOldPriceAction);
					taskMapping.ProdPriceVal = GlobalClass.ObjectToString(taskMapping?.ProdPriceVal);
					taskMapping.ProdPriceAddPercentVal = GlobalClass.StringToInteger(taskMapping?.ProdPriceAddPercentVal);
					taskMapping.ProdPriceAddUnitVal = GlobalClass.StringToDecimal(taskMapping?.ProdPriceAddUnitVal);
					taskMapping.ProdPriceAction = GlobalClass.ObjectToBool(taskMapping?.ProdPriceAction);
					taskMapping.ProdStockVal = GlobalClass.ObjectToString(taskMapping?.ProdStockVal);
					taskMapping.ProdStockAction = GlobalClass.ObjectToBool(taskMapping?.ProdStockAction);
					taskMapping.SeoMetaKeyVal = GlobalClass.ObjectToString(taskMapping?.SeoMetaKeyVal);
					taskMapping.SeoMetaKeyAction = GlobalClass.ObjectToBool(taskMapping?.SeoMetaKeyAction);
					taskMapping.SeoMetaDescVal = GlobalClass.ObjectToString(taskMapping?.SeoMetaDescVal);
					taskMapping.SeoMetaDescAction = GlobalClass.ObjectToBool(taskMapping?.SeoMetaDescAction);
					taskMapping.SeoMetaTitleVal = GlobalClass.ObjectToString(taskMapping?.SeoMetaTitleVal);
					taskMapping.SeoMetaTitleAction = GlobalClass.ObjectToBool(taskMapping?.SeoMetaTitleAction);
					taskMapping.SeoPageVal = GlobalClass.ObjectToString(taskMapping?.SeoPageVal);
					taskMapping.SeoPageAction = GlobalClass.ObjectToBool(taskMapping?.SeoPageAction);
					taskMapping.CatVal = GlobalClass.ObjectToString(taskMapping?.CatVal);
					taskMapping.CatAction = GlobalClass.ObjectToBool(taskMapping?.CatAction);
					taskMapping.Cat1Val = GlobalClass.ObjectToString(taskMapping?.Cat1Val);
					taskMapping.Cat1Action = GlobalClass.ObjectToBool(taskMapping?.Cat1Action);
					taskMapping.Cat2Val = GlobalClass.ObjectToString(taskMapping?.Cat2Val);
					taskMapping.Cat2Action = GlobalClass.ObjectToBool(taskMapping?.Cat2Action);
					taskMapping.Cat3Val = GlobalClass.ObjectToString(taskMapping?.Cat3Val);
					taskMapping.Cat3Action = GlobalClass.ObjectToBool(taskMapping?.Cat3Action);
					taskMapping.Cat4Val = GlobalClass.ObjectToString(taskMapping?.Cat4Val);
					taskMapping.Cat4Action = GlobalClass.ObjectToBool(taskMapping?.Cat4Action);
					taskMapping.Cat1Parent = GlobalClass.StringToInteger(taskMapping?.Cat1Parent);
					taskMapping.Cat2Parent = GlobalClass.StringToInteger(taskMapping?.Cat2Parent);
					taskMapping.Cat3Parent = GlobalClass.StringToInteger(taskMapping?.Cat3Parent);
					taskMapping.Cat4Parent = GlobalClass.StringToInteger(taskMapping?.Cat4Parent);
					taskMapping.ManufactVal = GlobalClass.ObjectToString(taskMapping?.ManufactVal);
					taskMapping.ManufactAction = GlobalClass.ObjectToBool(taskMapping?.ManufactAction);
					taskMapping.VendorVal = GlobalClass.ObjectToString(taskMapping?.VendorVal);
					taskMapping.VendorAction = GlobalClass.ObjectToBool(taskMapping?.VendorAction);
					taskMapping.TaxCategoryVal = GlobalClass.ObjectToString(taskMapping?.TaxCategoryVal);
					taskMapping.TaxCategoryAction = GlobalClass.ObjectToBool(taskMapping?.TaxCategoryAction);
					taskMapping.GtinVal = GlobalClass.ObjectToString(taskMapping?.GtinVal);
					taskMapping.GtinAction = GlobalClass.ObjectToBool(taskMapping?.GtinAction);
					taskMapping.WeightVal = GlobalClass.ObjectToString(taskMapping?.WeightVal);
					taskMapping.WeightAction = GlobalClass.ObjectToBool(taskMapping?.WeightAction);
					taskMapping.LengthVal = GlobalClass.ObjectToString(taskMapping?.LengthVal);
					taskMapping.LengthAction = GlobalClass.ObjectToBool(taskMapping?.LengthAction);
					taskMapping.WidthVal = GlobalClass.ObjectToString(taskMapping?.WidthVal);
					taskMapping.WidthAction = GlobalClass.ObjectToBool(taskMapping?.WidthAction);
					taskMapping.HeightVal = GlobalClass.ObjectToString(taskMapping?.HeightVal);
					taskMapping.HeightAction = GlobalClass.ObjectToBool(taskMapping?.HeightAction);
					taskMapping.ImageAction = GlobalClass.ObjectToBool(taskMapping?.ImageAction);
					taskMapping.Image1Action = GlobalClass.ObjectToBool(taskMapping?.Image1Action);
					taskMapping.Image2Action = GlobalClass.ObjectToBool(taskMapping?.Image2Action);
					taskMapping.Image3Action = GlobalClass.ObjectToBool(taskMapping?.Image3Action);
					taskMapping.Image4Action = GlobalClass.ObjectToBool(taskMapping?.Image4Action);
					taskMapping.AttNameVal = GlobalClass.ObjectToString(taskMapping?.AttNameVal);
					taskMapping.AttNameAction = GlobalClass.ObjectToBool(taskMapping?.AttNameAction);
					taskMapping.AttValueVal = GlobalClass.ObjectToString(taskMapping?.AttValueVal);
					taskMapping.AttValueAction = GlobalClass.ObjectToBool(taskMapping?.AttValueAction);
					taskMapping.AttStockVal = GlobalClass.StringToInteger(taskMapping?.AttStockVal);
					taskMapping.AttStockAction = GlobalClass.ObjectToBool(taskMapping?.AttStockAction);
					taskMapping.Att1NameVal = GlobalClass.ObjectToString(taskMapping?.Att1NameVal);
					taskMapping.Att1NameAction = GlobalClass.ObjectToBool(taskMapping?.Att1NameAction);
					taskMapping.Att1ValueVal = GlobalClass.ObjectToString(taskMapping?.Att1ValueVal);
					taskMapping.Att1ValueAction = GlobalClass.ObjectToBool(taskMapping?.Att1ValueAction);
					taskMapping.Att1StockVal = GlobalClass.StringToInteger(taskMapping?.Att1StockVal);
					taskMapping.Att1StockAction = GlobalClass.ObjectToBool(taskMapping?.Att1StockAction);
					taskMapping.Att2NameVal = GlobalClass.ObjectToString(taskMapping?.Att2NameVal);
					taskMapping.Att2NameAction = GlobalClass.ObjectToBool(taskMapping?.Att2NameAction);
					taskMapping.Att2ValueVal = GlobalClass.ObjectToString(taskMapping?.Att2ValueVal);
					taskMapping.Att2ValueAction = GlobalClass.ObjectToBool(taskMapping?.Att2ValueAction);
					taskMapping.Att2StockVal = GlobalClass.StringToInteger(taskMapping?.Att2StockVal);
					taskMapping.Att2StockAction = GlobalClass.ObjectToBool(taskMapping?.Att2StockAction);
					taskMapping.FilterAction = GlobalClass.StringToInteger(taskMapping?.FilterAction);
					taskMapping.FilterVal = GlobalClass.ObjectToString(taskMapping?.FilterVal);
					taskMapping.Filter1Action = GlobalClass.StringToInteger(taskMapping?.Filter1Action);
					taskMapping.Filter1Val = GlobalClass.ObjectToString(taskMapping?.Filter1Val);
					taskMapping.Filter2Action = GlobalClass.StringToInteger(taskMapping?.Filter2Action);
					taskMapping.Filter2Val = GlobalClass.ObjectToString(taskMapping?.Filter2Val);
					taskMapping.Filter3Action = GlobalClass.StringToInteger(taskMapping?.Filter3Action);
					taskMapping.Filter3Val = GlobalClass.ObjectToString(taskMapping?.Filter3Val);
					taskMapping.Filter4Action = GlobalClass.StringToInteger(taskMapping?.Filter4Action);
					taskMapping.Filter4Val = GlobalClass.ObjectToString(taskMapping?.Filter4Val);
					taskMapping.Filter5Action = GlobalClass.StringToInteger(taskMapping?.Filter5Action);
					taskMapping.Filter5Val = GlobalClass.ObjectToString(taskMapping?.Filter5Val);
					taskMapping.Filter6Action = GlobalClass.StringToInteger(taskMapping?.Filter6Action);
					taskMapping.Filter6Val = GlobalClass.ObjectToString(taskMapping?.Filter6Val);
					taskMapping.Filter7Action = GlobalClass.StringToInteger(taskMapping?.Filter7Action);
					taskMapping.Filter7Val = GlobalClass.ObjectToString(taskMapping?.Filter7Val);
					taskMapping.Filter8Action = GlobalClass.StringToInteger(taskMapping?.Filter8Action);
					taskMapping.Filter8Val = GlobalClass.ObjectToString(taskMapping?.Filter8Val);
					taskMapping.Filter9Action = GlobalClass.StringToInteger(taskMapping?.Filter9Action);
					taskMapping.Filter9Val = GlobalClass.ObjectToString(taskMapping?.Filter9Val);
					taskMapping.LimitedToStores = GlobalClass.ObjectToString(taskMapping?.LimitedToStores);
					taskMapping.DisableShop = GlobalClass.ObjectToBool(taskMapping?.DisableShop);
					taskMapping.ScheduleType = GlobalClass.StringToInteger(taskMapping?.ScheduleType);
					taskMapping.ScheduleInterval = GlobalClass.StringToInteger(taskMapping?.ScheduleInterval);
					taskMapping.ScheduleHour = GlobalClass.StringToInteger(taskMapping?.ScheduleHour);
					taskMapping.ScheduleMinute = GlobalClass.StringToInteger(taskMapping?.ScheduleMinute);
					taskMapping.UnpublishProducts = GlobalClass.ObjectToBool(taskMapping?.UnpublishProducts);
					taskMapping.ProductPublish = GlobalClass.ObjectToBoolNull(taskMapping?.ProductPublish);
					taskMapping.ManageInventoryMethodId = GlobalClass.StringToInteger(taskMapping?.ManageInventoryMethodId, -1);
					taskMapping.BackorderModeId = GlobalClass.StringToInteger(taskMapping?.BackorderModeId, -1);
					taskMapping.LowStockActivityId = GlobalClass.StringToInteger(taskMapping?.LowStockActivityId, -1);
					taskMapping.DeliveryDayId = GlobalClass.StringToInteger(taskMapping?.DeliveryDayId, -1);
					taskMapping.AttControlTypeId = GlobalClass.StringToInteger(taskMapping?.AttControlTypeId, -1);
					taskMapping.Att1ControlTypeId = GlobalClass.StringToInteger(taskMapping?.Att1ControlTypeId, -1);
					taskMapping.Att2ControlTypeId = GlobalClass.StringToInteger(taskMapping?.Att2ControlTypeId, -1);
					taskMapping.DeleteSourceFile = GlobalClass.ObjectToBool(taskMapping?.DeleteSourceFile);
					taskMapping.RelatedTaskId = GlobalClass.StringToInteger(taskMapping?.RelatedTaskId);
					taskMapping.RelatedSourceAddress = GlobalClass.ObjectToString(taskMapping?.RelatedSourceAddress);
					taskMapping.SavePicturesInFile = GlobalClass.ObjectToBool(taskMapping?.SavePicturesInFile);
					taskMapping.UseTitleCase = GlobalClass.ObjectToBool(taskMapping?.UseTitleCase);
					taskMapping.SavePicturesPath = GlobalClass.ObjectToString(taskMapping?.SavePicturesPath);
					taskMapping.CustomerRoleVal = GlobalClass.ObjectToString(taskMapping?.CustomerRoleVal);
					taskMapping.CustomerRoleAction = GlobalClass.ObjectToBool(taskMapping?.CustomerRoleAction);
					taskMapping.CustomerRole1Val = GlobalClass.ObjectToString(taskMapping?.CustomerRole1Val);
					taskMapping.CustomerRole1Action = GlobalClass.ObjectToBool(taskMapping?.CustomerRole1Action);
					taskMapping.CustomerRole2Val = GlobalClass.ObjectToString(taskMapping?.CustomerRole2Val);
					taskMapping.CustomerRole2Action = GlobalClass.ObjectToBool(taskMapping?.CustomerRole2Action);
					taskMapping.CustomerRole3Val = GlobalClass.ObjectToString(taskMapping?.CustomerRole3Val);
					taskMapping.CustomerRole3Action = GlobalClass.ObjectToBool(taskMapping?.CustomerRole3Action);
					taskMapping.CustomerRole4Val = GlobalClass.ObjectToString(taskMapping?.CustomerRole4Val);
					taskMapping.CustomerRole4Action = GlobalClass.ObjectToBool(taskMapping?.CustomerRole4Action);
					taskMapping.AllowCustomerReviews = GlobalClass.ObjectToBoolNull(taskMapping?.AllowCustomerReviews);
					taskMapping.DisplayAvailability = GlobalClass.ObjectToBoolNull(taskMapping?.DisplayAvailability);
					taskMapping.DisplayStockQuantity = GlobalClass.ObjectToBoolNull(taskMapping?.DisplayStockQuantity);
					taskMapping.ProdSettingsAllowedQtyVal = GlobalClass.ObjectToString(taskMapping?.ProdSettingsAllowedQtyVal);
					taskMapping.ProdSettingsAllowedQtyAction = GlobalClass.ObjectToBool(taskMapping?.ProdSettingsAllowedQtyAction);
					taskMapping.ProdSettingsMinStockQtyVal = GlobalClass.ObjectToString(taskMapping?.ProdSettingsMinStockQtyVal);
					taskMapping.ProdSettingsMinStockQtyAction = GlobalClass.ObjectToBool(taskMapping?.ProdSettingsMinStockQtyAction);
					taskMapping.ProdSettingsMinCartQtyVal = GlobalClass.ObjectToString(taskMapping?.ProdSettingsMinCartQtyVal);
					taskMapping.ProdSettingsMinCartQtyAction = GlobalClass.ObjectToBool(taskMapping?.ProdSettingsMinCartQtyAction);
					taskMapping.ProdSettingsMaxCartQtyVal = GlobalClass.ObjectToString(taskMapping?.ProdSettingsMaxCartQtyVal);
					taskMapping.ProdSettingsMaxCartQtyAction = GlobalClass.ObjectToBool(taskMapping?.ProdSettingsMaxCartQtyAction);
					taskMapping.ProdSettingsNotifyQtyVal = GlobalClass.ObjectToString(taskMapping?.ProdSettingsNotifyQtyVal);
					taskMapping.ProdSettingsNotifyQtyAction = GlobalClass.ObjectToBool(taskMapping?.ProdSettingsNotifyQtyAction);
					taskMapping.ProdSettingsShippingEnabledVal = GlobalClass.ObjectToBool(taskMapping?.ProdSettingsShippingEnabledVal);
					taskMapping.ProdSettingsShippingEnabledAction = GlobalClass.ObjectToBool(taskMapping?.ProdSettingsShippingEnabledAction);
					taskMapping.ProdSettingsFreeShippingVal = GlobalClass.ObjectToBool(taskMapping?.ProdSettingsFreeShippingVal);
					taskMapping.ProdSettingsFreeShippingAction = GlobalClass.ObjectToBool(taskMapping?.ProdSettingsFreeShippingAction);
					taskMapping.ProdSettingsShipSeparatelyVal = GlobalClass.ObjectToBool(taskMapping?.ProdSettingsShipSeparatelyVal);
					taskMapping.ProdSettingsShipSeparatelyAction = GlobalClass.ObjectToBool(taskMapping?.ProdSettingsShipSeparatelyAction);
					taskMapping.ProdSettingsShippingChargeVal = GlobalClass.StringToDecimal(taskMapping?.ProdSettingsShippingChargeVal);
					taskMapping.ProdSettingsShippingChargeAction = GlobalClass.ObjectToBool(taskMapping?.ProdSettingsShippingChargeAction);
				}
				else
				{
					taskMapping.TaskId = taskId;
					taskMapping.VendorSourceId = GlobalClass.StringToInteger(sqlCeDataReader["VendorSourceId"]);
					taskMapping.EShopId = GlobalClass.StringToInteger(sqlCeDataReader["EShopId"]);
					taskMapping.TaskName = GlobalClass.ObjectToString(sqlCeDataReader["TaskName"]);
					taskMapping.TaskAction = GlobalClass.StringToInteger(sqlCeDataReader["TaskAction"]);
					taskMapping.TaskStatusId = GlobalClass.StringToInteger(sqlCeDataReader["TaskStatusId"]);
					taskMapping.ProdNameVal = GlobalClass.ObjectToString(sqlCeDataReader["ProdNameVal"]);
					taskMapping.ProdNameAction = GlobalClass.ObjectToBool(sqlCeDataReader["ProdNameAction"]);
					taskMapping.ProdShortDescVal = GlobalClass.ObjectToString(sqlCeDataReader["ProdShortDescVal"]);
					taskMapping.ProdShortDescAction = GlobalClass.ObjectToBool(sqlCeDataReader["ProdShortDescAction"]);
					taskMapping.ProdFullDescVal = GlobalClass.ObjectToString(sqlCeDataReader["ProdFullDescVal"]);
					taskMapping.ProdFullDescAction = GlobalClass.ObjectToBool(sqlCeDataReader["ProdFullDescAction"]);
					taskMapping.ProdManPartNumVal = GlobalClass.ObjectToString(sqlCeDataReader["ProdManPartNumVal"]);
					taskMapping.ProdManPartNumAction = GlobalClass.ObjectToBool(sqlCeDataReader["ProdManPartNumAction"]);
					taskMapping.ProdCostVal = GlobalClass.ObjectToString(sqlCeDataReader["ProdCostVal"]);
					taskMapping.ProdCostAction = GlobalClass.ObjectToBool(sqlCeDataReader["ProdCostAction"]);
					taskMapping.ProdPriceVal = GlobalClass.ObjectToString(sqlCeDataReader["ProdPriceVal"]);
					taskMapping.ProdPriceAddPercentVal = GlobalClass.StringToInteger(sqlCeDataReader["ProdPriceAddPercentVal"]);
					taskMapping.ProdPriceAddUnitVal = GlobalClass.StringToDecimal(sqlCeDataReader["ProdPriceAddUnitVal"]);
					taskMapping.ProdPriceAction = GlobalClass.ObjectToBool(sqlCeDataReader["ProdPriceAction"]);
					taskMapping.ProdStockVal = GlobalClass.ObjectToString(sqlCeDataReader["ProdStockVal"]);
					taskMapping.ProdStockAction = GlobalClass.ObjectToBool(sqlCeDataReader["ProdStockAction"]);
					taskMapping.SeoMetaKeyVal = GlobalClass.ObjectToString(sqlCeDataReader["SeoMetaKeyVal"]);
					taskMapping.SeoMetaKeyAction = GlobalClass.ObjectToBool(sqlCeDataReader["SeoMetaKeyAction"]);
					taskMapping.SeoMetaDescVal = GlobalClass.ObjectToString(sqlCeDataReader["SeoMetaDescVal"]);
					taskMapping.SeoMetaDescAction = GlobalClass.ObjectToBool(sqlCeDataReader["SeoMetaDescAction"]);
					taskMapping.SeoMetaTitleVal = GlobalClass.ObjectToString(sqlCeDataReader["SeoMetaTitleVal"]);
					taskMapping.SeoMetaTitleAction = GlobalClass.ObjectToBool(sqlCeDataReader["SeoMetaTitleAction"]);
					taskMapping.SeoPageVal = GlobalClass.ObjectToString(sqlCeDataReader["SeoPageVal"]);
					taskMapping.SeoPageAction = GlobalClass.ObjectToBool(sqlCeDataReader["SeoPageAction"]);
					taskMapping.CatVal = GlobalClass.ObjectToString(sqlCeDataReader["CatVal"]);
					taskMapping.CatAction = GlobalClass.ObjectToBool(sqlCeDataReader["CatAction"]);
					taskMapping.Cat1Val = GlobalClass.ObjectToString(sqlCeDataReader["Cat1Val"]);
					taskMapping.Cat1Action = GlobalClass.ObjectToBool(sqlCeDataReader["Cat1Action"]);
					taskMapping.Cat2Val = GlobalClass.ObjectToString(sqlCeDataReader["Cat2Val"]);
					taskMapping.Cat2Action = GlobalClass.ObjectToBool(sqlCeDataReader["Cat2Action"]);
					taskMapping.Cat3Val = GlobalClass.ObjectToString(sqlCeDataReader["Cat3Val"]);
					taskMapping.Cat3Action = GlobalClass.ObjectToBool(sqlCeDataReader["Cat3Action"]);
					taskMapping.Cat4Val = GlobalClass.ObjectToString(sqlCeDataReader["Cat4Val"]);
					taskMapping.Cat4Action = GlobalClass.ObjectToBool(sqlCeDataReader["Cat4Action"]);
					taskMapping.Cat1Parent = GlobalClass.StringToInteger(sqlCeDataReader["Cat1Parent"]);
					taskMapping.Cat2Parent = GlobalClass.StringToInteger(sqlCeDataReader["Cat2Parent"]);
					taskMapping.Cat3Parent = GlobalClass.StringToInteger(sqlCeDataReader["Cat3Parent"]);
					taskMapping.Cat4Parent = GlobalClass.StringToInteger(sqlCeDataReader["Cat4Parent"]);
					taskMapping.ManufactVal = GlobalClass.ObjectToString(sqlCeDataReader["ManufactVal"]);
					taskMapping.ManufactAction = GlobalClass.ObjectToBool(sqlCeDataReader["ManufactAction"]);
					taskMapping.VendorVal = GlobalClass.ObjectToString(sqlCeDataReader["VendorVal"]);
					taskMapping.VendorAction = GlobalClass.ObjectToBool(sqlCeDataReader["VendorAction"]);
					taskMapping.TaxCategoryVal = GlobalClass.ObjectToString(sqlCeDataReader["TaxCategoryVal"]);
					taskMapping.TaxCategoryAction = GlobalClass.ObjectToBool(sqlCeDataReader["TaxCategoryAction"]);
					taskMapping.GtinVal = GlobalClass.ObjectToString(sqlCeDataReader["GtinVal"]);
					taskMapping.GtinAction = GlobalClass.ObjectToBool(sqlCeDataReader["GtinAction"]);
					taskMapping.WeightVal = GlobalClass.ObjectToString(sqlCeDataReader["WeightVal"]);
					taskMapping.WeightAction = GlobalClass.ObjectToBool(sqlCeDataReader["WeightAction"]);
					taskMapping.ImageAction = GlobalClass.ObjectToBool(sqlCeDataReader["ImageAction"]);
					taskMapping.Image1Action = GlobalClass.ObjectToBool(sqlCeDataReader["Image1Action"]);
					taskMapping.Image2Action = GlobalClass.ObjectToBool(sqlCeDataReader["Image2Action"]);
					taskMapping.Image3Action = GlobalClass.ObjectToBool(sqlCeDataReader["Image3Action"]);
					taskMapping.Image4Action = GlobalClass.ObjectToBool(sqlCeDataReader["Image4Action"]);
					taskMapping.AttNameVal = GlobalClass.ObjectToString(sqlCeDataReader["AttNameVal"]);
					taskMapping.AttNameAction = GlobalClass.ObjectToBool(sqlCeDataReader["AttNameAction"]);
					taskMapping.AttValueVal = GlobalClass.ObjectToString(sqlCeDataReader["AttValueVal"]);
					taskMapping.AttValueAction = GlobalClass.ObjectToBool(sqlCeDataReader["AttValueAction"]);
					taskMapping.AttStockVal = GlobalClass.StringToInteger(sqlCeDataReader["AttStockVal"]);
					taskMapping.AttStockAction = GlobalClass.ObjectToBool(sqlCeDataReader["AttStockAction"]);
					taskMapping.Att1NameVal = GlobalClass.ObjectToString(sqlCeDataReader["Att1NameVal"]);
					taskMapping.Att1NameAction = GlobalClass.ObjectToBool(sqlCeDataReader["Att1NameAction"]);
					taskMapping.Att1ValueVal = GlobalClass.ObjectToString(sqlCeDataReader["Att1ValueVal"]);
					taskMapping.Att1ValueAction = GlobalClass.ObjectToBool(sqlCeDataReader["Att1ValueAction"]);
					taskMapping.Att1StockVal = GlobalClass.StringToInteger(sqlCeDataReader["Att1StockVal"]);
					taskMapping.Att1StockAction = GlobalClass.ObjectToBool(sqlCeDataReader["Att1StockAction"]);
					taskMapping.Att2NameVal = GlobalClass.ObjectToString(sqlCeDataReader["Att2NameVal"]);
					taskMapping.Att2NameAction = GlobalClass.ObjectToBool(sqlCeDataReader["Att2NameAction"]);
					taskMapping.Att2ValueVal = GlobalClass.ObjectToString(sqlCeDataReader["Att2ValueVal"]);
					taskMapping.Att2ValueAction = GlobalClass.ObjectToBool(sqlCeDataReader["Att2ValueAction"]);
					taskMapping.Att2StockVal = GlobalClass.StringToInteger(sqlCeDataReader["Att2StockVal"]);
					taskMapping.Att2StockAction = GlobalClass.ObjectToBool(sqlCeDataReader["Att2StockAction"]);
					taskMapping.FilterAction = GlobalClass.StringToInteger(sqlCeDataReader["FilterAction"]);
					taskMapping.FilterVal = GlobalClass.ObjectToString(sqlCeDataReader["FilterVal"]);
					taskMapping.Filter1Action = GlobalClass.StringToInteger(sqlCeDataReader["Filter1Action"]);
					taskMapping.Filter1Val = GlobalClass.ObjectToString(sqlCeDataReader["Filter1Val"]);
					taskMapping.Filter2Action = GlobalClass.StringToInteger(sqlCeDataReader["Filter2Action"]);
					taskMapping.Filter2Val = GlobalClass.ObjectToString(sqlCeDataReader["Filter2Val"]);
					taskMapping.Filter3Action = GlobalClass.StringToInteger(sqlCeDataReader["Filter3Action"]);
					taskMapping.Filter3Val = GlobalClass.ObjectToString(sqlCeDataReader["Filter3Val"]);
					taskMapping.Filter4Action = GlobalClass.StringToInteger(sqlCeDataReader["Filter4Action"]);
					taskMapping.Filter4Val = GlobalClass.ObjectToString(sqlCeDataReader["Filter4Val"]);
					taskMapping.Filter5Action = GlobalClass.StringToInteger(sqlCeDataReader["Filter5Action"]);
					taskMapping.Filter5Val = GlobalClass.ObjectToString(sqlCeDataReader["Filter5Val"]);
					taskMapping.Filter6Action = GlobalClass.StringToInteger(sqlCeDataReader["Filter6Action"]);
					taskMapping.Filter6Val = GlobalClass.ObjectToString(sqlCeDataReader["Filter6Val"]);
					taskMapping.Filter7Action = GlobalClass.StringToInteger(sqlCeDataReader["Filter7Action"]);
					taskMapping.Filter7Val = GlobalClass.ObjectToString(sqlCeDataReader["Filter7Val"]);
					taskMapping.Filter8Action = GlobalClass.StringToInteger(sqlCeDataReader["Filter8Action"]);
					taskMapping.Filter8Val = GlobalClass.ObjectToString(sqlCeDataReader["Filter8Val"]);
					taskMapping.Filter9Action = GlobalClass.StringToInteger(sqlCeDataReader["Filter9Action"]);
					taskMapping.Filter9Val = GlobalClass.ObjectToString(sqlCeDataReader["Filter9Val"]);
					taskMapping.LimitedToStores = GlobalClass.ObjectToString(sqlCeDataReader["LimitedToStores"]);
					taskMapping.DisableShop = GlobalClass.ObjectToBool(sqlCeDataReader["DisableShop"]);
					taskMapping.ScheduleType = GlobalClass.StringToInteger(sqlCeDataReader["ScheduleType"]);
					taskMapping.ScheduleInterval = GlobalClass.StringToInteger(sqlCeDataReader["ScheduleInterval"]);
					taskMapping.ScheduleHour = GlobalClass.StringToInteger(sqlCeDataReader["ScheduleHour"]);
					taskMapping.ScheduleMinute = GlobalClass.StringToInteger(sqlCeDataReader["ScheduleMinute"]);
					taskMapping.UnpublishProducts = GlobalClass.ObjectToBool(sqlCeDataReader["UnpublishProducts"]);
					taskMapping.ProductPublish = GlobalClass.ObjectToBoolNull(sqlCeDataReader["ProductPublish"]);
					taskMapping.ManageInventoryMethodId = GlobalClass.StringToInteger(sqlCeDataReader["ManageInventoryMethodId"], -1);
					taskMapping.DeliveryDayId = GlobalClass.StringToInteger(sqlCeDataReader["DeliveryDayId"], -1);
					taskMapping.AttControlTypeId = GlobalClass.StringToInteger(sqlCeDataReader["AttControlTypeId"], -1);
					taskMapping.Att1ControlTypeId = GlobalClass.StringToInteger(sqlCeDataReader["Att1ControlTypeId"], -1);
					taskMapping.Att2ControlTypeId = GlobalClass.StringToInteger(sqlCeDataReader["Att2ControlTypeId"], -1);
					taskMapping.DeleteSourceFile = GlobalClass.ObjectToBool(sqlCeDataReader["DeleteSourceFile"]);
					taskMapping.RelatedTaskId = GlobalClass.StringToInteger(sqlCeDataReader["RelatedTaskId"]);
					taskMapping.RelatedSourceAddress = GlobalClass.ObjectToString(sqlCeDataReader["RelatedSourceAddress"]);
					taskMapping.SavePicturesInFile = GlobalClass.ObjectToBool(sqlCeDataReader["SavePicturesInFile"]);
					taskMapping.SavePicturesPath = GlobalClass.ObjectToString(sqlCeDataReader["SavePicturesPath"]);
				}
			}
			if (taskMapping.ProductPublish.HasValue)
			{
				taskMapping.ProductPublishAction = true;
			}
			if (taskMapping.ManageInventoryMethodId.HasValue && taskMapping.ManageInventoryMethodId > -1)
			{
				taskMapping.ManageInventoryMethodAction = true;
			}
			if (taskMapping.BackorderModeId.HasValue && taskMapping.BackorderModeId > -1)
			{
				taskMapping.BackorderModeAction = true;
			}
			if (taskMapping.LowStockActivityId.HasValue && taskMapping.LowStockActivityId > -1)
			{
				taskMapping.LowStockActivityAction = true;
			}
			if (taskMapping.DeliveryDayId.HasValue && taskMapping.DeliveryDayId > -1)
			{
				taskMapping.ProdSettingsDeliveryDateAction = true;
			}
			sqlCeDataReader.Close();
			if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
			{
				sqlCeConnection.Close();
			}
			sqlCeCommand?.Dispose();
			return taskMapping;
		}

		public SourceDocument GetSourceDoucumentForTasks(SourceDocument sourceDoucument, TaskMapping taskMapping)
		{
			SourceDocument sourceDocument = new SourceDocument();
			Expression<Func<SourceItem, bool>> filterAction = (SourceItem b) => ((taskMapping.FilterAction ?? 0) <= 0 || taskMapping.FilterAction == (int?)1 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "0" && f.FilterValue.Trim().ToLower() == taskMapping.FilterVal.Trim().ToLower()) != null || taskMapping.FilterAction == (int?)2 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "0" && GlobalClass.StringToDecimal(f.FilterValue, 0) <= GlobalClass.StringToDecimal(taskMapping.FilterVal, -1)) != null || taskMapping.FilterAction == (int?)3 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "0" && GlobalClass.StringToDecimal(f.FilterValue, -1) >= GlobalClass.StringToDecimal(taskMapping.FilterVal, 0)) != null || taskMapping.FilterAction == (int?)4 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "0" && f.FilterValue.Trim().ToLower() != taskMapping.FilterVal.Trim().ToLower()) != null || taskMapping.FilterAction == (int?)5 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "0" && f.FilterValue.Trim().ToLower().Contains(taskMapping.FilterVal.Trim().ToLower())) != null || taskMapping.FilterAction == (int?)6 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "0" && f.FilterValue.Trim().ToLower().StartsWith(taskMapping.FilterVal.Trim().ToLower())) != null || taskMapping.FilterAction == (int?)7 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "0" && f.FilterValue.Trim().ToLower().EndsWith(taskMapping.FilterVal.Trim().ToLower())) != null || taskMapping.FilterAction == (int?)8 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "0" && !f.FilterValue.Trim().ToLower().Contains(taskMapping.FilterVal.Trim().ToLower())) != null) && ((taskMapping.Filter1Action ?? 0) <= 0 || taskMapping.Filter1Action == (int?)1 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "1" && f.FilterValue.Trim().ToLower() == taskMapping.Filter1Val.Trim().ToLower()) != null || taskMapping.Filter1Action == (int?)2 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "1" && GlobalClass.StringToDecimal(f.FilterValue, 0) <= GlobalClass.StringToDecimal(taskMapping.Filter1Val, -1)) != null || taskMapping.Filter1Action == (int?)3 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "1" && GlobalClass.StringToDecimal(f.FilterValue, -1) >= GlobalClass.StringToDecimal(taskMapping.Filter1Val, 0)) != null || taskMapping.Filter1Action == (int?)4 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "1" && f.FilterValue.Trim().ToLower() != taskMapping.Filter1Val.Trim().ToLower()) != null || taskMapping.Filter1Action == (int?)5 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "1" && f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter1Val.Trim().ToLower())) != null || taskMapping.Filter1Action == (int?)6 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "1" && f.FilterValue.Trim().ToLower().StartsWith(taskMapping.Filter1Val.Trim().ToLower())) != null || taskMapping.Filter1Action == (int?)7 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "1" && f.FilterValue.Trim().ToLower().EndsWith(taskMapping.Filter1Val.Trim().ToLower())) != null || taskMapping.Filter1Action == (int?)8 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "1" && !f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter1Val.Trim().ToLower())) != null) && ((taskMapping.Filter2Action ?? 0) <= 0 || taskMapping.Filter2Action == (int?)1 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "2" && f.FilterValue.Trim().ToLower() == taskMapping.Filter2Val.Trim().ToLower()) != null || taskMapping.Filter2Action == (int?)2 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "2" && GlobalClass.StringToDecimal(f.FilterValue, 0) <= GlobalClass.StringToDecimal(taskMapping.Filter2Val, -1)) != null || taskMapping.Filter2Action == (int?)3 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "2" && GlobalClass.StringToDecimal(f.FilterValue, -1) >= GlobalClass.StringToDecimal(taskMapping.Filter2Val, 0)) != null || taskMapping.Filter2Action == (int?)4 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "2" && f.FilterValue.Trim().ToLower() != taskMapping.Filter2Val.Trim().ToLower()) != null || taskMapping.Filter2Action == (int?)5 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "2" && f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter2Val.Trim().ToLower())) != null || taskMapping.Filter2Action == (int?)6 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "2" && f.FilterValue.Trim().ToLower().StartsWith(taskMapping.Filter2Val.Trim().ToLower())) != null || taskMapping.Filter2Action == (int?)7 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "2" && f.FilterValue.Trim().ToLower().EndsWith(taskMapping.Filter2Val.Trim().ToLower())) != null || taskMapping.Filter2Action == (int?)8 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "2" && !f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter2Val.Trim().ToLower())) != null) && ((taskMapping.Filter3Action ?? 0) <= 0 || taskMapping.Filter3Action == (int?)1 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "3" && f.FilterValue.Trim().ToLower() == taskMapping.Filter3Val.Trim().ToLower()) != null || taskMapping.Filter3Action == (int?)2 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "3" && GlobalClass.StringToDecimal(f.FilterValue, 0) <= GlobalClass.StringToDecimal(taskMapping.Filter3Val, -1)) != null || taskMapping.Filter3Action == (int?)3 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "3" && GlobalClass.StringToDecimal(f.FilterValue, -1) >= GlobalClass.StringToDecimal(taskMapping.Filter3Val, 0)) != null || taskMapping.Filter3Action == (int?)4 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "3" && f.FilterValue.Trim().ToLower() != taskMapping.Filter3Val.Trim().ToLower()) != null || taskMapping.Filter3Action == (int?)5 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "3" && f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter3Val.Trim().ToLower())) != null || taskMapping.Filter3Action == (int?)6 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "3" && f.FilterValue.Trim().ToLower().StartsWith(taskMapping.Filter3Val.Trim().ToLower())) != null || taskMapping.Filter3Action == (int?)7 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "3" && f.FilterValue.Trim().ToLower().EndsWith(taskMapping.Filter3Val.Trim().ToLower())) != null || taskMapping.Filter3Action == (int?)8 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "3" && !f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter3Val.Trim().ToLower())) != null) && ((taskMapping.Filter4Action ?? 0) <= 0 || taskMapping.Filter4Action == (int?)1 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "4" && f.FilterValue.Trim().ToLower() == taskMapping.Filter4Val.Trim().ToLower()) != null || taskMapping.Filter4Action == (int?)2 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "4" && GlobalClass.StringToDecimal(f.FilterValue, 0) <= GlobalClass.StringToDecimal(taskMapping.Filter4Val, -1)) != null || taskMapping.Filter4Action == (int?)3 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "4" && GlobalClass.StringToDecimal(f.FilterValue, -1) >= GlobalClass.StringToDecimal(taskMapping.Filter4Val, 0)) != null || taskMapping.Filter4Action == (int?)4 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "4" && f.FilterValue.Trim().ToLower() != taskMapping.Filter4Val.Trim().ToLower()) != null || taskMapping.Filter4Action == (int?)5 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "4" && f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter4Val.Trim().ToLower())) != null || taskMapping.Filter4Action == (int?)6 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "4" && f.FilterValue.Trim().ToLower().StartsWith(taskMapping.Filter4Val.Trim().ToLower())) != null || taskMapping.Filter4Action == (int?)7 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "4" && f.FilterValue.Trim().ToLower().EndsWith(taskMapping.Filter4Val.Trim().ToLower())) != null || taskMapping.Filter4Action == (int?)8 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "4" && !f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter4Val.Trim().ToLower())) != null) && ((taskMapping.Filter5Action ?? 0) <= 0 || taskMapping.Filter5Action == (int?)1 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "5" && f.FilterValue.Trim().ToLower() == taskMapping.Filter5Val.Trim().ToLower()) != null || taskMapping.Filter5Action == (int?)2 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "5" && GlobalClass.StringToDecimal(f.FilterValue, 0) <= GlobalClass.StringToDecimal(taskMapping.Filter5Val, -1)) != null || taskMapping.Filter5Action == (int?)3 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "5" && GlobalClass.StringToDecimal(f.FilterValue, -1) >= GlobalClass.StringToDecimal(taskMapping.Filter5Val, 0)) != null || taskMapping.Filter5Action == (int?)4 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "5" && f.FilterValue.Trim().ToLower() != taskMapping.Filter5Val.Trim().ToLower()) != null || taskMapping.Filter5Action == (int?)5 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "5" && f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter5Val.Trim().ToLower())) != null || taskMapping.Filter5Action == (int?)6 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "5" && f.FilterValue.Trim().ToLower().StartsWith(taskMapping.Filter5Val.Trim().ToLower())) != null || taskMapping.Filter5Action == (int?)7 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "5" && f.FilterValue.Trim().ToLower().EndsWith(taskMapping.Filter5Val.Trim().ToLower())) != null || taskMapping.Filter5Action == (int?)8 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "5" && !f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter5Val.Trim().ToLower())) != null) && ((taskMapping.Filter6Action ?? 0) <= 0 || taskMapping.Filter6Action == (int?)1 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "6" && f.FilterValue.Trim().ToLower() == taskMapping.Filter6Val.Trim().ToLower()) != null || taskMapping.Filter6Action == (int?)2 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "6" && GlobalClass.StringToDecimal(f.FilterValue, 0) <= GlobalClass.StringToDecimal(taskMapping.Filter6Val, -1)) != null || taskMapping.Filter6Action == (int?)3 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "6" && GlobalClass.StringToDecimal(f.FilterValue, -1) >= GlobalClass.StringToDecimal(taskMapping.Filter6Val, 0)) != null || taskMapping.Filter6Action == (int?)4 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "6" && f.FilterValue.Trim().ToLower() != taskMapping.Filter6Val.Trim().ToLower()) != null || taskMapping.Filter6Action == (int?)5 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "6" && f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter6Val.Trim().ToLower())) != null || taskMapping.Filter6Action == (int?)6 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "6" && f.FilterValue.Trim().ToLower().StartsWith(taskMapping.Filter6Val.Trim().ToLower())) != null || taskMapping.Filter6Action == (int?)7 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "6" && f.FilterValue.Trim().ToLower().EndsWith(taskMapping.Filter6Val.Trim().ToLower())) != null || taskMapping.Filter6Action == (int?)8 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "6" && !f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter6Val.Trim().ToLower())) != null) && ((taskMapping.Filter7Action ?? 0) <= 0 || taskMapping.Filter7Action == (int?)1 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "7" && f.FilterValue.Trim().ToLower() == taskMapping.Filter7Val.Trim().ToLower()) != null || taskMapping.Filter7Action == (int?)2 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "7" && GlobalClass.StringToDecimal(f.FilterValue, 0) <= GlobalClass.StringToDecimal(taskMapping.Filter7Val, -1)) != null || taskMapping.Filter7Action == (int?)3 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "7" && GlobalClass.StringToDecimal(f.FilterValue, -1) >= GlobalClass.StringToDecimal(taskMapping.Filter7Val, 0)) != null || taskMapping.Filter7Action == (int?)4 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "7" && f.FilterValue.Trim().ToLower() != taskMapping.Filter7Val.Trim().ToLower()) != null || taskMapping.Filter7Action == (int?)5 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "7" && f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter7Val.Trim().ToLower())) != null || taskMapping.Filter7Action == (int?)6 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "7" && f.FilterValue.Trim().ToLower().StartsWith(taskMapping.Filter7Val.Trim().ToLower())) != null || taskMapping.Filter7Action == (int?)7 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "7" && f.FilterValue.Trim().ToLower().EndsWith(taskMapping.Filter7Val.Trim().ToLower())) != null || taskMapping.Filter7Action == (int?)8 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "7" && !f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter7Val.Trim().ToLower())) != null) && ((taskMapping.Filter8Action ?? 0) <= 0 || taskMapping.Filter8Action == (int?)1 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "8" && f.FilterValue.Trim().ToLower() == taskMapping.Filter8Val.Trim().ToLower()) != null || taskMapping.Filter8Action == (int?)2 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "8" && GlobalClass.StringToDecimal(f.FilterValue, 0) <= GlobalClass.StringToDecimal(taskMapping.Filter8Val, -1)) != null || taskMapping.Filter8Action == (int?)3 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "8" && GlobalClass.StringToDecimal(f.FilterValue, -1) >= GlobalClass.StringToDecimal(taskMapping.Filter8Val, 0)) != null || taskMapping.Filter8Action == (int?)4 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "8" && f.FilterValue.Trim().ToLower() != taskMapping.Filter8Val.Trim().ToLower()) != null || taskMapping.Filter8Action == (int?)5 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "8" && f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter8Val.Trim().ToLower())) != null || taskMapping.Filter8Action == (int?)6 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "8" && f.FilterValue.Trim().ToLower().StartsWith(taskMapping.Filter8Val.Trim().ToLower())) != null || taskMapping.Filter8Action == (int?)7 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "8" && f.FilterValue.Trim().ToLower().EndsWith(taskMapping.Filter8Val.Trim().ToLower())) != null || taskMapping.Filter8Action == (int?)8 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "8" && !f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter8Val.Trim().ToLower())) != null) && ((taskMapping.Filter9Action ?? 0) <= 0 || taskMapping.Filter9Action == (int?)1 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "9" && f.FilterValue.Trim().ToLower() == taskMapping.Filter9Val.Trim().ToLower()) != null || taskMapping.Filter9Action == (int?)2 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "9" && GlobalClass.StringToDecimal(f.FilterValue, 0) <= GlobalClass.StringToDecimal(taskMapping.Filter9Val, -1)) != null || taskMapping.Filter9Action == (int?)3 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "9" && GlobalClass.StringToDecimal(f.FilterValue, -1) >= GlobalClass.StringToDecimal(taskMapping.Filter9Val, 0)) != null || taskMapping.Filter9Action == (int?)4 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "9" && f.FilterValue.Trim().ToLower() != taskMapping.Filter9Val.Trim().ToLower()) != null || taskMapping.Filter9Action == (int?)5 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "9" && f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter9Val.Trim().ToLower())) != null || taskMapping.Filter9Action == (int?)6 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "9" && f.FilterValue.Trim().ToLower().StartsWith(taskMapping.Filter9Val.Trim().ToLower())) != null || taskMapping.Filter9Action == (int?)7 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "9" && f.FilterValue.Trim().ToLower().EndsWith(taskMapping.Filter9Val.Trim().ToLower())) != null || taskMapping.Filter9Action == (int?)8 && b.Filters.FirstOrDefault<SourceItem.FilterValues>((SourceItem.FilterValues f) => f.FilterId == "9" && !f.FilterValue.Trim().ToLower().Contains(taskMapping.Filter9Val.Trim().ToLower())) != null);
			foreach (SourceItem sourceItem in sourceDoucument.SourceItems.AsQueryable<SourceItem>().Where<SourceItem>(filterAction))
			{
				sourceDocument.SourceItems.Add(sourceItem);
			}
			sourceDocument.Total = sourceDoucument.Total;
			sourceDocument.LoadByOne = sourceDoucument.LoadByOne;
			sourceDocument.SourceLines = sourceDoucument.SourceLines;
			sourceDocument.SourceMapping = sourceDoucument.SourceMapping;
			sourceDocument.TaskMapping = sourceDoucument.TaskMapping;
			sourceDocument.SourceData = sourceDoucument.SourceData;
			return sourceDocument;
		}

		public void UpdateTaskData(int taskId, long foundItems, long exportedItems, DateTime? lastRunDate)
		{
			SqlCeConnection sqlCeConnection = null;
			SqlCeCommand sqlCeCommand = null;
			try
			{
				sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString);
				string commandText = "\r\n                UPDATE  [Task] SET\r\n                        [TotalItems] = @TotalItems,\r\n                        [ExportedItems]          = @ExportedItems,\r\n                        [TaskLastRun]        = @TaskLastRun,\r\n                        [ModifiedDate]        = @ModifiedDate\r\n                WHERE Id=@taskId";
				sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@taskId", taskId));
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@TotalItems", foundItems));
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@ExportedItems", exportedItems));
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@ModifiedDate", DateTime.Now));
				if (lastRunDate.HasValue)
				{
					sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaskLastRun", lastRunDate));
				}
				else
				{
					sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaskLastRun", DBNull.Value));
				}
				sqlCeConnection.Open();
				sqlCeCommand.ExecuteNonQuery();
				sqlCeConnection.Close();
			}
			catch
			{
			}
			finally
			{
				if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
				sqlCeCommand?.Dispose();
			}
		}

		public void UpdateTaskNextRun(int taskId, DateTime? nextRunDate)
		{
			SqlCeConnection sqlCeConnection = null;
			SqlCeCommand sqlCeCommand = null;
			try
			{
				sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString);
				string commandText = "\r\n                UPDATE  [Task] SET\r\n                        [TaskNextRun]        = @TaskNextRun,\r\n                        [ModifiedDate]        = @ModifiedDate\r\n                WHERE Id=@taskId";
				sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@taskId", taskId));
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@ModifiedDate", DateTime.Now));
				if (nextRunDate.HasValue)
				{
					sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaskNextRun", nextRunDate));
				}
				else
				{
					sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaskNextRun", DBNull.Value));
				}
				sqlCeConnection.Open();
				sqlCeCommand.ExecuteNonQuery();
				sqlCeConnection.Close();
			}
			catch
			{
			}
			finally
			{
				if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
				sqlCeCommand?.Dispose();
			}
		}

		public void UpdateTaskRunning(int taskId, bool taskRunning)
		{
			SqlCeConnection sqlCeConnection = null;
			SqlCeCommand sqlCeCommand = null;
			try
			{
				sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString);
				string commandText = "\r\n                UPDATE  [Task] SET\r\n                        [TaskRunning]        = @TaskRunning,\r\n                        [ModifiedDate]        = @ModifiedDate\r\n                WHERE Id=@taskId";
				sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@taskId", taskId));
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaskRunning", taskRunning));
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@ModifiedDate", DateTime.Now));
				sqlCeConnection.Open();
				sqlCeCommand.ExecuteNonQuery();
				sqlCeConnection.Close();
			}
			catch
			{
			}
			finally
			{
				if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
				sqlCeCommand?.Dispose();
			}
		}

		public void DeleteTask(int taskId)
		{
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString);
			string commandText = "\r\n                DELETE FROM  [Task]\r\n                WHERE Id=@taskId";
			SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@taskId", taskId));
			sqlCeConnection.Open();
			sqlCeCommand.ExecuteNonQuery();
			sqlCeConnection.Close();
			if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
			{
				sqlCeConnection.Close();
			}
			sqlCeCommand?.Dispose();
		}

		public DataTable GetAnyDataAsTable(string sqlString)
		{
			DataTable dataTable = new DataTable();
			using (SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString))
			{
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(sqlString, sqlCeConnection))
				{
					sqlCeConnection.Open();
					dataTable.Load(sqlCeCommand.ExecuteReader());
				}
			}
			return dataTable;
		}

		public void UpdateTaskStatus(int taskId, int taskStatusId)
		{
			SqlCeConnection sqlCeConnection = null;
			SqlCeCommand sqlCeCommand = null;
			try
			{
				sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString);
				string commandText = "\r\n                UPDATE  [Task] SET\r\n                        [TaskStatusId]        = @taskStatusId\r\n                WHERE Id=@taskId";
				sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@taskId", taskId));
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@taskStatusId", taskStatusId));
				sqlCeConnection.Open();
				sqlCeCommand.ExecuteNonQuery();
				sqlCeConnection.Close();
			}
			catch
			{
			}
			finally
			{
				if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
				sqlCeCommand?.Dispose();
			}
		}

		public bool IsTaskRunning(int taskId)
		{
			SqlCeConnection sqlCeConnection = null;
			SqlCeCommand sqlCeCommand = null;
			try
			{
				bool result = false;
				string dataConnectionString = dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString;
				sqlCeConnection = new SqlCeConnection(dataConnectionString);
				string commandText = "select * from Task where Id=@TaskId";
				sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaskId", taskId));
				sqlCeConnection.Open();
				SqlCeDataReader sqlCeDataReader = sqlCeCommand.ExecuteReader();
				if (sqlCeDataReader.Read())
				{
					result = GlobalClass.ObjectToBool(sqlCeDataReader["TaskRunning"]);
				}
				sqlCeDataReader.Close();
				return result;
			}
			catch
			{
			}
			finally
			{
				if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
				sqlCeCommand?.Dispose();
			}
			return true;
		}

		public void UpdateTaskProgress(int taskId, long foundItems, long exportedItems)
		{
			SqlCeConnection sqlCeConnection = null;
			SqlCeCommand sqlCeCommand = null;
			try
			{
				sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString);
				string commandText = "\r\n                UPDATE  [Task] SET\r\n                        [TotalItems] = @TotalItems,\r\n                        [ExportedItems] = @ExportedItems,\r\n                        [ModifiedDate]        = @ModifiedDate\r\n                WHERE Id=@taskId";
				sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@taskId", taskId));
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@TotalItems", foundItems));
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@ExportedItems", exportedItems));
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@ModifiedDate", DateTime.Now));
				sqlCeConnection.Open();
				sqlCeCommand.ExecuteNonQuery();
				sqlCeConnection.Close();
			}
			catch
			{
			}
			finally
			{
				if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
				sqlCeCommand?.Dispose();
			}
		}
	}
}
