using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class SourceProdPicturesMapping
	{
		public List<SourceItemMapping> Items
		{
			get;
			set;
		}

		public SourceProdPicturesMapping()
		{
			Items = new List<SourceItemMapping>();
		}
	}
}
