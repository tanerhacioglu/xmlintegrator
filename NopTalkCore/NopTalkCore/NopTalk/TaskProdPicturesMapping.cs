using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class TaskProdPicturesMapping
	{
		public List<TaskItemMapping> Items
		{
			get;
			set;
		}

		public TaskProdPicturesMapping()
		{
			Items = new List<TaskItemMapping>();
		}
	}
}
