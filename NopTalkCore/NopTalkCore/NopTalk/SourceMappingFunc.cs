using Newtonsoft.Json;
using NopTalkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.Text;
using System.Threading.Tasks;

namespace NopTalk
{
	public class SourceMappingFunc
	{
		private string _settingsFilePath = "";

		private DataSettingsManager dataSettingsManager = new DataSettingsManager();

		public SourceMappingFunc()
		{
		}

		public SourceMappingFunc(string settingsFilePath)
		{
			_settingsFilePath = settingsFilePath;
		}

		public int InsertVendor(string name, string description)
		{
			int num = 0;
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString);
			string commandText = "INSERT INTO [Vendor]\r\n           ([Name]\r\n           ,[Description]) \r\n            VALUES\r\n           (@name\r\n           ,@description)";
			SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@name", name));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@description", description ?? string.Empty));
			sqlCeConnection.Open();
			sqlCeCommand.ExecuteNonQuery();
			sqlCeCommand.CommandText = "SELECT @@IDENTITY";
			num = Convert.ToInt32(sqlCeCommand.ExecuteScalar());
			sqlCeConnection.Close();
			sqlCeCommand?.Dispose();
			return num;
		}

		public void UpdateVendor(int vendorId, string name, string description)
		{
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString);
			string commandText = "\r\n            UPDATE [Vendor]\r\n               SET [Name] = @name\r\n                  ,[Description] = @description\r\n             WHERE Id=@vendorId";
			SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@vendorId", vendorId));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@name", name));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@description", description ?? string.Empty));
			sqlCeConnection.Open();
			sqlCeCommand.ExecuteNonQuery();
			sqlCeConnection.Close();
			sqlCeCommand?.Dispose();
		}

		public void DeleteVendor(int vendorId)
		{
			VendorData vendorData = GetVendorData(vendorId);
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString);
			string commandText = "\r\n            DELETE FROM [Task]\r\n             WHERE VendorSourceId=@vendorSourceId";
			SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@vendorSourceId", vendorData.VendorSourceId));
			sqlCeConnection.Open();
			sqlCeCommand.ExecuteNonQuery();
			sqlCeConnection.Close();
			sqlCeCommand?.Dispose();
			while (GetVendorData(vendorId).VendorSourceMappingId > 0)
			{
				commandText = "\r\n            DELETE FROM [SourceMapping]\r\n             WHERE Id=@VendorSourceMappingId";
				sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorSourceMappingId", vendorData.VendorSourceMappingId));
				sqlCeConnection.Open();
				sqlCeCommand.ExecuteNonQuery();
				sqlCeConnection.Close();
				sqlCeCommand?.Dispose();
			}
			while (GetVendorData(vendorId).VendorSourceId > 0)
			{
				commandText = "\r\n            DELETE FROM [VendorSource]\r\n             WHERE Id=@VendorSourceId";
				sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorSourceId", GetVendorData(vendorId).VendorSourceId));
				sqlCeConnection.Open();
				sqlCeCommand.ExecuteNonQuery();
				sqlCeConnection.Close();
				sqlCeCommand?.Dispose();
			}
			commandText = "\r\n            DELETE FROM [VendorSource]\r\n             WHERE VendorId=@vendorId";
			sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@vendorId", vendorId));
			sqlCeConnection.Open();
			sqlCeCommand.ExecuteNonQuery();
			sqlCeConnection.Close();
			sqlCeCommand?.Dispose();
			commandText = "\r\n            DELETE FROM [Vendor]\r\n             WHERE Id=@vendorId";
			sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@vendorId", vendorId));
			sqlCeConnection.Open();
			sqlCeCommand.ExecuteNonQuery();
			sqlCeConnection.Close();
			sqlCeCommand?.Dispose();
		}

		public async Task<int> InsertSourceMapping(SourceMapping sourceMapping)
		{
			SqlCeConnection conn = new SqlCeConnection(dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString);
			string sqlString = "\r\n            INSERT INTO [SourceMapping]\r\n                   (\r\n                    [VendorSourceId]    ,\r\n                    [ProdInfoRepeatNode],\r\n                    [ProdIdMap]         ,\r\n                    [ProdSkuMap]        ,\r\n                    [ProdSkuVal]        ,\r\n                    [ProdNameMap]       ,\r\n                    [ProdNameVal]       ,\r\n                    [ProdManPartNumMap] ,\r\n                    [ProdManPartNumVal] ,\r\n                    [ProdCostMap]       ,\r\n                    [ProdCostVal]       ,\r\n                    [ProdPriceMap]      ,\r\n                    [ProdPriceVal]      ,\r\n                    [ProdStockMap]      ,\r\n                    [ProdStockVal]      ,\r\n                    [SeoMetaKeyMap]     ,\r\n                    [SeoMetaKeyVal]     ,\r\n                    [SeoMetaDescMap]    ,\r\n                    [SeoMetaDescVal]    ,\r\n                    [SeoMetaTitleMap]   ,\r\n                    [SeoMetaTitleVal]   ,\r\n                    [SeoPageMap]        ,\r\n                    [SeoPageVal]        ,\r\n                    [CatMap]            ,\r\n                    [CatVal]            ,\r\n                    [CatDelimeter]      ,\r\n                    [Cat1Map]           ,\r\n                    [Cat1Val]           ,\r\n                    [Cat2Map]           ,\r\n                    [Cat2Val]           ,\r\n                    [Cat3Map]           ,\r\n                    [Cat3Val]           ,\r\n                    [Cat4Map]           ,\r\n                    [Cat4Val]           ,\r\n                    [ManufactMap]       ,\r\n                    [ManufactVal]       ,\r\n                    [ImageMap]          ,\r\n                    [Image1Map]         ,\r\n                    [Image2Map]         ,\r\n                    [Image3Map]         ,\r\n                    [Image4Map]         ,\r\n                    [ImageVal]          ,\r\n                    [Image1Val]         ,\r\n                    [Image2Val]         ,\r\n                    [Image3Val]         ,\r\n                    [Image4Val]         ,\r\n\t                [AttRepeatNodeMap]  ,\r\n                    [AttNameMap]        ,\r\n                    [AttNameVal]        ,\r\n                    [AttValueMap]       ,\r\n                    [AttValueVal]       ,\r\n\t                [AttStockMap]       ,\r\n                    [AttStockVal]       ,\r\n                    [Att1NameMap]       ,\r\n                    [Att1NameVal]       ,\r\n                    [Att1ValueMap]      ,\r\n                    [Att1ValueVal]      ,\r\n\t                [Att1StockMap]       ,\r\n                    [Att1StockVal]       ,\r\n                    [Att2NameMap]       ,\r\n                    [Att2NameVal]       ,\r\n                    [Att2ValueMap]      ,\r\n                    [Att2ValueVal]      ,\r\n\t                [Att2StockMap]       ,\r\n                    [Att2StockVal]       ,\r\n                    [FilterName]        ,\r\n                    [FilterMap]         ,\r\n                    [Filter1Name]       ,\r\n                    [Filter1Map]        ,\r\n                    [Filter2Name]       ,\r\n                    [Filter2Map]        ,\r\n                    [Filter3Name]       ,\r\n                    [Filter3Map]        ,\r\n                    [Filter4Name]       ,\r\n                    [Filter4Map]        ,\r\n                    [Filter5Name]       ,\r\n                    [Filter5Map]        ,\r\n                    [Filter6Name]       ,\r\n                    [Filter6Map]        ,\r\n                    [Filter7Name]       ,\r\n                    [Filter7Map]        ,\r\n                    [Filter8Name]       ,\r\n                    [Filter8Map]        ,\r\n                    [Filter9Name]       ,\r\n                    [Filter9Map]        ,\r\n                    [CatIsId]        ,\r\n                    [Cat1IsId]        ,\r\n                    [Cat2IsId]        ,\r\n                    [Cat3IsId]        ,\r\n                    [Cat4IsId]        ,\r\n                    [ManufacturerIsId]        ,\r\n                    [VendorIsId]        ,\r\n                    [VendorMap]        ,\r\n                    [VendorVal]        ,\r\n                    [TaxCategoryMap]        ,\r\n                    [TaxCategoryVal]        ,\r\n                    [GtinMap]        ,\r\n                    [GtinVal]        ,\r\n                    [WeightMap]        ,\r\n                    [WeightVal]        ,\r\n                    [ImageIsBinary]        ,\r\n                    [Image1IsBinary]        ,\r\n                    [Image2IsBinary]        ,\r\n                    [Image3IsBinary]        ,\r\n                    [Image4IsBinary]     ,\r\n                    [ConfigData]\r\n                    )  \r\n             VALUES\r\n                   (\r\n                    @VendorSourceId    ,\r\n                    @ProdInfoRepeatNode,\r\n                    @ProdIdMap         ,\r\n                    @ProdSkuMap        ,\r\n                    @ProdSkuVal        ,\r\n                    @ProdNameMap       ,\r\n                    @ProdNameVal       ,\r\n                    @ProdManPartNumMap ,\r\n                    @ProdManPartNumVal ,\r\n                    @ProdCostMap       ,\r\n                    @ProdCostVal       ,\r\n                    @ProdPriceMap      ,\r\n                    @ProdPriceVal      ,\r\n                    @ProdStockMap      ,\r\n                    @ProdStockVal      ,\r\n                    @SeoMetaKeyMap     ,\r\n                    @SeoMetaKeyVal     ,\r\n                    @SeoMetaDescMap    ,\r\n                    @SeoMetaDescVal    ,\r\n                    @SeoMetaTitleMap   ,\r\n                    @SeoMetaTitleVal   ,\r\n                    @SeoPageMap        ,\r\n                    @SeoPageVal        ,\r\n                    @CatMap            ,\r\n                    @CatVal            ,\r\n                    @CatDelimeter      ,\r\n                    @Cat1Map           ,\r\n                    @Cat1Val           ,\r\n                    @Cat2Map           ,\r\n                    @Cat2Val           ,\r\n                    @Cat3Map           ,\r\n                    @Cat3Val           ,\r\n                    @Cat4Map           ,\r\n                    @Cat4Val           ,\r\n                    @ManufactMap       ,\r\n                    @ManufactVal       ,\r\n                    @ImageMap        ,\r\n                    @Image1Map        ,\r\n                    @Image2Map        ,\r\n                    @Image3Map        ,\r\n                    @Image4Map     ,\r\n                    @ImageVal        ,\r\n                    @Image1Val        ,\r\n                    @Image2Val        ,\r\n                    @Image3Val        ,\r\n                    @Image4Val     ,\r\n\t                @AttRepeatNodeMap ,\r\n                    @AttNameMap      ,\r\n                    @AttNameVal        ,\r\n                    @AttValueMap      ,\r\n                    @AttValueVal      ,\r\n\t                @AttStockMap      ,\r\n                    @AttStockVal     ,\r\n                    @Att1NameMap     ,\r\n                    @Att1NameVal      ,\r\n                    @Att1ValueMap   ,\r\n                    @Att1ValueVal     ,\r\n\t                @Att1StockMap    ,\r\n                    @Att1StockVal     ,\r\n                    @Att2NameMap     ,\r\n                    @Att2NameVal    ,\r\n                    @Att2ValueMap    ,\r\n                    @Att2ValueVal    ,\r\n\t                @Att2StockMap     ,\r\n                    @Att2StockVal     ,\r\n                    @FilterName    ,\r\n                    @FilterMap     ,\r\n                    @Filter1Name  ,\r\n                    @Filter1Map     ,\r\n                    @Filter2Name    ,\r\n                    @Filter2Map   ,\r\n                    @Filter3Name    ,\r\n                    @Filter3Map    ,\r\n                    @Filter4Name    ,\r\n                    @Filter4Map   ,\r\n                    @Filter5Name   ,\r\n                    @Filter5Map    ,\r\n                    @Filter6Name   ,\r\n                    @Filter6Map  ,\r\n                    @Filter7Name   ,\r\n                    @Filter7Map    ,\r\n                    @Filter8Name     ,\r\n                    @Filter8Map    ,\r\n                    @Filter9Name    ,\r\n                    @Filter9Map     ,\r\n                    @CatIsId        ,\r\n                    @Cat1IsId        ,\r\n                    @Cat2IsId        ,\r\n                    @Cat3IsId       ,\r\n                    @Cat4IsId        ,\r\n                    @ManufacturerIsId       ,\r\n                    @VendorIsId      ,\r\n                    @VendorMap       ,\r\n                    @VendorVal  ,\r\n                    @TaxCategoryMap       ,\r\n                    @TaxCategoryVal  ,\r\n                    @GtinMap        ,\r\n                    @GtinVal        ,\r\n                    @WeightMap        ,\r\n                    @WeightVal        ,\r\n                    @ImageIsBinary        ,\r\n                    @Image1IsBinary        ,\r\n                    @Image2IsBinary        ,\r\n                    @Image3IsBinary        ,\r\n                    @Image4IsBinary       , \r\n                    @ConfigData\r\n                    )";
			SqlCeCommand comm = new SqlCeCommand(sqlString, conn)
			{
				Parameters = 
				{
					new SqlCeParameter("@VendorSourceId", GlobalClass.StringToInteger(sourceMapping.VendorSourceId)),
					new SqlCeParameter("@ProdInfoRepeatNode", GlobalClass.ObjectToString(sourceMapping.ProdInfoRepeatNode)),
					new SqlCeParameter("@ProdIdMap", GlobalClass.ObjectToString(sourceMapping.ProdIdMap)),
					new SqlCeParameter("@ProdSkuMap", GlobalClass.ObjectToString(sourceMapping.ProdSkuMap)),
					new SqlCeParameter("@ProdSkuVal", GlobalClass.ObjectToString(sourceMapping.ProdSkuVal)),
					new SqlCeParameter("@ProdNameMap", GlobalClass.ObjectToString(sourceMapping.ProdNameMap)),
					new SqlCeParameter("@ProdNameVal", GlobalClass.ObjectToString(sourceMapping.ProdNameVal)),
					new SqlCeParameter("@ProdManPartNumMap", GlobalClass.ObjectToString(sourceMapping.ProdManPartNumMap)),
					new SqlCeParameter("@ProdManPartNumVal", GlobalClass.ObjectToString(sourceMapping.ProdManPartNumVal)),
					new SqlCeParameter("@ProdCostMap", GlobalClass.ObjectToString(sourceMapping.ProdCostMap)),
					new SqlCeParameter("@ProdCostVal", GlobalClass.ObjectToString(sourceMapping.ProdCostVal)),
					new SqlCeParameter("@ProdPriceMap", GlobalClass.ObjectToString(sourceMapping.ProdPriceMap)),
					new SqlCeParameter("@ProdPriceVal", GlobalClass.ObjectToString(sourceMapping.ProdPriceVal)),
					new SqlCeParameter("@ProdStockMap", GlobalClass.ObjectToString(sourceMapping.ProdStockMap)),
					new SqlCeParameter("@ProdStockVal", GlobalClass.ObjectToString(sourceMapping.ProdStockVal)),
					new SqlCeParameter("@SeoMetaKeyMap", GlobalClass.ObjectToString(sourceMapping.SeoMetaKeyMap)),
					new SqlCeParameter("@SeoMetaKeyVal", GlobalClass.ObjectToString(sourceMapping.SeoMetaKeyVal)),
					new SqlCeParameter("@SeoMetaDescMap", GlobalClass.ObjectToString(sourceMapping.SeoMetaDescMap)),
					new SqlCeParameter("@SeoMetaDescVal", GlobalClass.ObjectToString(sourceMapping.SeoMetaDescVal)),
					new SqlCeParameter("@SeoMetaTitleMap", GlobalClass.ObjectToString(sourceMapping.SeoMetaTitleMap)),
					new SqlCeParameter("@SeoMetaTitleVal", GlobalClass.ObjectToString(sourceMapping.SeoMetaTitleVal)),
					new SqlCeParameter("@SeoPageMap", GlobalClass.ObjectToString(sourceMapping.SeoPageMap)),
					new SqlCeParameter("@SeoPageVal", GlobalClass.ObjectToString(sourceMapping.SeoPageVal)),
					new SqlCeParameter("@CatMap", GlobalClass.ObjectToString(sourceMapping.CatMap)),
					new SqlCeParameter("@CatVal", GlobalClass.ObjectToString(sourceMapping.CatVal))
				}
			};
			if (sourceMapping.CatDelimeter.HasValue)
			{
				comm.Parameters.Add(new SqlCeParameter("@CatDelimeter", GlobalClass.ObjectToString(sourceMapping.CatDelimeter)));
			}
			else
			{
				comm.Parameters.Add(new SqlCeParameter("@CatDelimeter", DBNull.Value));
			}
			comm.Parameters.Add(new SqlCeParameter("@Cat1Map", GlobalClass.ObjectToString(sourceMapping.Cat1Map)));
			comm.Parameters.Add(new SqlCeParameter("@Cat1Val", GlobalClass.ObjectToString(sourceMapping.Cat1Val)));
			comm.Parameters.Add(new SqlCeParameter("@Cat2Map", GlobalClass.ObjectToString(sourceMapping.Cat2Map)));
			comm.Parameters.Add(new SqlCeParameter("@Cat2Val", GlobalClass.ObjectToString(sourceMapping.Cat2Val)));
			comm.Parameters.Add(new SqlCeParameter("@Cat3Map", GlobalClass.ObjectToString(sourceMapping.Cat3Map)));
			comm.Parameters.Add(new SqlCeParameter("@Cat3Val", GlobalClass.ObjectToString(sourceMapping.Cat3Val)));
			comm.Parameters.Add(new SqlCeParameter("@Cat4Map", GlobalClass.ObjectToString(sourceMapping.Cat4Map)));
			comm.Parameters.Add(new SqlCeParameter("@Cat4Val", GlobalClass.ObjectToString(sourceMapping.Cat4Val)));
			comm.Parameters.Add(new SqlCeParameter("@ManufactMap", GlobalClass.ObjectToString(sourceMapping.ManufactMap)));
			comm.Parameters.Add(new SqlCeParameter("@ManufactVal", GlobalClass.ObjectToString(sourceMapping.ManufactVal)));
			comm.Parameters.Add(new SqlCeParameter("@ImageMap", GlobalClass.ObjectToString(sourceMapping.ImageMap)));
			comm.Parameters.Add(new SqlCeParameter("@Image1Map", GlobalClass.ObjectToString(sourceMapping.Image1Map)));
			comm.Parameters.Add(new SqlCeParameter("@Image2Map", GlobalClass.ObjectToString(sourceMapping.Image2Map)));
			comm.Parameters.Add(new SqlCeParameter("@Image3Map", GlobalClass.ObjectToString(sourceMapping.Image3Map)));
			comm.Parameters.Add(new SqlCeParameter("@Image4Map", GlobalClass.ObjectToString(sourceMapping.Image4Map)));
			comm.Parameters.Add(new SqlCeParameter("@AttRepeatNodeMap", GlobalClass.ObjectToString(sourceMapping.AttRepeatNodeMap)));
			comm.Parameters.Add(new SqlCeParameter("@AttNameMap", GlobalClass.ObjectToString(sourceMapping.AttNameMap)));
			comm.Parameters.Add(new SqlCeParameter("@AttNameVal", GlobalClass.ObjectToString(sourceMapping.AttNameVal)));
			comm.Parameters.Add(new SqlCeParameter("@AttValueMap", GlobalClass.ObjectToString(sourceMapping.AttValueMap)));
			comm.Parameters.Add(new SqlCeParameter("@AttValueVal", GlobalClass.ObjectToString(sourceMapping.AttValueVal)));
			comm.Parameters.Add(new SqlCeParameter("@AttStockMap", GlobalClass.ObjectToString(sourceMapping.AttStockMap)));
			comm.Parameters.Add(new SqlCeParameter("@AttStockVal", GlobalClass.ObjectToString(sourceMapping.AttStockVal)));
			comm.Parameters.Add(new SqlCeParameter("@Att1NameMap", GlobalClass.ObjectToString(sourceMapping.Att1NameMap)));
			comm.Parameters.Add(new SqlCeParameter("@Att1NameVal", GlobalClass.ObjectToString(sourceMapping.Att1NameVal)));
			comm.Parameters.Add(new SqlCeParameter("@Att1ValueMap", GlobalClass.ObjectToString(sourceMapping.Att1ValueMap)));
			comm.Parameters.Add(new SqlCeParameter("@Att1ValueVal", GlobalClass.ObjectToString(sourceMapping.Att1ValueVal)));
			comm.Parameters.Add(new SqlCeParameter("@Att1StockMap", GlobalClass.ObjectToString(sourceMapping.Att1StockMap)));
			comm.Parameters.Add(new SqlCeParameter("@Att1StockVal", GlobalClass.ObjectToString(sourceMapping.Att1StockVal)));
			comm.Parameters.Add(new SqlCeParameter("@Att2NameMap", GlobalClass.ObjectToString(sourceMapping.Att2NameMap)));
			comm.Parameters.Add(new SqlCeParameter("@Att2NameVal", GlobalClass.ObjectToString(sourceMapping.Att2NameVal)));
			comm.Parameters.Add(new SqlCeParameter("@Att2ValueMap", GlobalClass.ObjectToString(sourceMapping.Att2ValueMap)));
			comm.Parameters.Add(new SqlCeParameter("@Att2ValueVal", GlobalClass.ObjectToString(sourceMapping.Att2ValueVal)));
			comm.Parameters.Add(new SqlCeParameter("@Att2StockMap", GlobalClass.ObjectToString(sourceMapping.Att2StockMap)));
			comm.Parameters.Add(new SqlCeParameter("@Att2StockVal", GlobalClass.ObjectToString(sourceMapping.Att2StockVal)));
			comm.Parameters.Add(new SqlCeParameter("@FilterName", GlobalClass.ObjectToString(sourceMapping.FilterName)));
			comm.Parameters.Add(new SqlCeParameter("@FilterMap", GlobalClass.ObjectToString(sourceMapping.FilterMap)));
			comm.Parameters.Add(new SqlCeParameter("@Filter1Name", GlobalClass.ObjectToString(sourceMapping.Filter1Name)));
			comm.Parameters.Add(new SqlCeParameter("@Filter1Map", GlobalClass.ObjectToString(sourceMapping.Filter1Map)));
			comm.Parameters.Add(new SqlCeParameter("@Filter2Name", GlobalClass.ObjectToString(sourceMapping.Filter2Name)));
			comm.Parameters.Add(new SqlCeParameter("@Filter2Map", GlobalClass.ObjectToString(sourceMapping.Filter2Map)));
			comm.Parameters.Add(new SqlCeParameter("@Filter3Name", GlobalClass.ObjectToString(sourceMapping.Filter3Name)));
			comm.Parameters.Add(new SqlCeParameter("@Filter3Map", GlobalClass.ObjectToString(sourceMapping.Filter3Map)));
			comm.Parameters.Add(new SqlCeParameter("@Filter4Name", GlobalClass.ObjectToString(sourceMapping.Filter4Name)));
			comm.Parameters.Add(new SqlCeParameter("@Filter4Map", GlobalClass.ObjectToString(sourceMapping.Filter4Map)));
			comm.Parameters.Add(new SqlCeParameter("@Filter5Name", GlobalClass.ObjectToString(sourceMapping.Filter5Name)));
			comm.Parameters.Add(new SqlCeParameter("@Filter5Map", GlobalClass.ObjectToString(sourceMapping.Filter5Map)));
			comm.Parameters.Add(new SqlCeParameter("@Filter6Name", GlobalClass.ObjectToString(sourceMapping.Filter6Name)));
			comm.Parameters.Add(new SqlCeParameter("@Filter6Map", GlobalClass.ObjectToString(sourceMapping.Filter6Map)));
			comm.Parameters.Add(new SqlCeParameter("@Filter7Name", GlobalClass.ObjectToString(sourceMapping.Filter7Name)));
			comm.Parameters.Add(new SqlCeParameter("@Filter7Map", GlobalClass.ObjectToString(sourceMapping.Filter7Map)));
			comm.Parameters.Add(new SqlCeParameter("@Filter8Name", GlobalClass.ObjectToString(sourceMapping.Filter8Name)));
			comm.Parameters.Add(new SqlCeParameter("@Filter8Map", GlobalClass.ObjectToString(sourceMapping.Filter8Map)));
			comm.Parameters.Add(new SqlCeParameter("@Filter9Name", GlobalClass.ObjectToString(sourceMapping.Filter9Name)));
			comm.Parameters.Add(new SqlCeParameter("@Filter9Map", GlobalClass.ObjectToString(sourceMapping.Filter9Map)));
			comm.Parameters.Add(new SqlCeParameter("@CatIsId", GlobalClass.ObjectToBool(sourceMapping.CatIsId)));
			comm.Parameters.Add(new SqlCeParameter("@Cat1IsId", GlobalClass.ObjectToBool(sourceMapping.Cat1IsId)));
			comm.Parameters.Add(new SqlCeParameter("@Cat2IsId", GlobalClass.ObjectToBool(sourceMapping.Cat2IsId)));
			comm.Parameters.Add(new SqlCeParameter("@Cat3IsId", GlobalClass.ObjectToBool(sourceMapping.Cat3IsId)));
			comm.Parameters.Add(new SqlCeParameter("@Cat4IsId", GlobalClass.ObjectToBool(sourceMapping.Cat4IsId)));
			comm.Parameters.Add(new SqlCeParameter("@ManufacturerIsId", GlobalClass.ObjectToBool(sourceMapping.ManufacturerIsId)));
			comm.Parameters.Add(new SqlCeParameter("@VendorIsId", GlobalClass.ObjectToBool(sourceMapping.VendorIsId)));
			comm.Parameters.Add(new SqlCeParameter("@VendorMap", GlobalClass.ObjectToString(sourceMapping.VendorMap)));
			comm.Parameters.Add(new SqlCeParameter("@VendorVal", GlobalClass.ObjectToString(sourceMapping.VendorVal)));
			comm.Parameters.Add(new SqlCeParameter("@ImageIsBinary", GlobalClass.ObjectToBool(sourceMapping.ImageIsBinary)));
			comm.Parameters.Add(new SqlCeParameter("@Image1IsBinary", GlobalClass.ObjectToBool(sourceMapping.Image1IsBinary)));
			comm.Parameters.Add(new SqlCeParameter("@Image2IsBinary", GlobalClass.ObjectToBool(sourceMapping.Image2IsBinary)));
			comm.Parameters.Add(new SqlCeParameter("@Image3IsBinary", GlobalClass.ObjectToBool(sourceMapping.Image3IsBinary)));
			comm.Parameters.Add(new SqlCeParameter("@Image4IsBinary", GlobalClass.ObjectToBool(sourceMapping.Image4IsBinary)));
			comm.Parameters.Add(new SqlCeParameter("@ImageVal", GlobalClass.ObjectToString(sourceMapping.ImageVal)));
			comm.Parameters.Add(new SqlCeParameter("@Image1Val", GlobalClass.ObjectToString(sourceMapping.Image1Val)));
			comm.Parameters.Add(new SqlCeParameter("@Image2Val", GlobalClass.ObjectToString(sourceMapping.Image2Val)));
			comm.Parameters.Add(new SqlCeParameter("@Image3Val", GlobalClass.ObjectToString(sourceMapping.Image3Val)));
			comm.Parameters.Add(new SqlCeParameter("@Image4Val", GlobalClass.ObjectToString(sourceMapping.Image4Val)));
			comm.Parameters.Add(new SqlCeParameter("@TaxCategoryMap", GlobalClass.ObjectToString(sourceMapping.TaxCategoryMap)));
			comm.Parameters.Add(new SqlCeParameter("@TaxCategoryVal", GlobalClass.ObjectToString(sourceMapping.TaxCategoryVal)));
			comm.Parameters.Add(new SqlCeParameter("@GtinMap", GlobalClass.ObjectToString(sourceMapping.GtinMap)));
			comm.Parameters.Add(new SqlCeParameter("@GtinVal", GlobalClass.ObjectToString(sourceMapping.GtinVal)));
			comm.Parameters.Add(new SqlCeParameter("@WeightMap", GlobalClass.ObjectToString(sourceMapping.WeightMap)));
			comm.Parameters.Add(new SqlCeParameter("@WeightVal", GlobalClass.ObjectToString(sourceMapping.WeightVal)));
			comm.Parameters.Add(new SqlCeParameter("@ConfigData", JsonConvert.SerializeObject(sourceMapping)));
			conn.Open();
			comm.ExecuteNonQuery();
			comm.CommandText = "SELECT @@IDENTITY";
			int retVal = Convert.ToInt32(comm.ExecuteScalar());
			conn.Close();
			comm?.Dispose();
			return retVal;
		}

		public async void UpdateSourceMapping(SourceMapping sourceMapping)
		{
			if (sourceMapping.VendorSourceId.HasValue)
			{
				SqlCeConnection conn = new SqlCeConnection(dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString);
				string sqlString = "\r\n                UPDATE  [SourceMapping] SET\r\n                        [ProdInfoRepeatNode] = @ProdInfoRepeatNode,\r\n                        [ProdIdMap]          = @ProdIdMap,\r\n                        [ProdSkuMap]         = @ProdSkuMap,\r\n                        [ProdSkuVal]        = @ProdSkuVal,\r\n                        [ProdNameMap]        = @ProdNameMap,\r\n                        [ProdNameVal]        = @ProdNameVal,\r\n                        [ProdManPartNumMap]  = @ProdManPartNumMap,\r\n                        [ProdManPartNumVal]  = @ProdManPartNumVal,\r\n                        [ProdCostMap]        = @ProdCostMap,\r\n                        [ProdCostVal]        = @ProdCostVal,\r\n                        [ProdPriceMap]       = @ProdPriceMap,\r\n                        [ProdPriceVal]       = @ProdPriceVal,\r\n                        [ProdStockMap]       = @ProdStockMap,\r\n                        [ProdStockVal]       = @ProdStockVal,\r\n                        [SeoMetaKeyMap]      = @SeoMetaKeyMap,\r\n                        [SeoMetaKeyVal]      = @SeoMetaKeyVal,\r\n                        [SeoMetaDescMap]     = @SeoMetaDescMap,\r\n                        [SeoMetaDescVal]     = @SeoMetaDescVal,\r\n                        [SeoMetaTitleMap]    = @SeoMetaTitleMap,\r\n                        [SeoMetaTitleVal]    = @SeoMetaTitleVal,\r\n                        [SeoPageMap]         = @SeoPageMap,\r\n                        [SeoPageVal]         = @SeoPageVal,\r\n                        [CatMap]             = @CatMap,\r\n                        [CatVal]             = @CatVal,\r\n                        [CatDelimeter]       = @CatDelimeter,\r\n                        [Cat1Map]            = @Cat1Map,\r\n                        [Cat1Val]            = @Cat1Val,\r\n                        [Cat2Map]            = @Cat2Map,\r\n                        [Cat2Val]            = @Cat2Val,\r\n                        [Cat3Map]            = @Cat3Map,\r\n                        [Cat3Val]            = @Cat3Val,\r\n                        [Cat4Map]            = @Cat4Map,\r\n                        [Cat4Val]            = @Cat4Val,\r\n                        [ManufactMap]        = @ManufactMap,\r\n                        [ManufactVal]        = @ManufactVal,\r\n                        [ImageMap]           = @ImageMap,\r\n                        [Image1Map]          = @Image1Map,\r\n                        [Image2Map]          = @Image2Map,\r\n                        [Image3Map]          = @Image3Map,\r\n                        [Image4Map]          = @Image4Map,\r\n                        [ImageVal]           = @ImageVal,\r\n                        [Image1Val]          = @Image1Val,\r\n                        [Image2Val]          = @Image2Val,\r\n                        [Image3Val]          = @Image3Val,\r\n                        [Image4Val]          = @Image4Val,\r\n\t                    [AttRepeatNodeMap]   = @AttRepeatNodeMap,\r\n                        [AttNameMap]         = @AttNameMap,\r\n                        [AttNameVal]         = @AttNameVal,\r\n                        [AttValueMap]        = @AttValueMap,\r\n                        [AttValueVal]        = @AttValueVal,\r\n\t                    [AttStockMap]        = @AttStockMap,\r\n                        [AttStockVal]        = @AttStockVal,\r\n                        [Att1NameMap]        = @Att1NameMap,\r\n                        [Att1NameVal]        = @Att1NameVal,\r\n                        [Att1ValueMap]       = @Att1ValueMap,\r\n                        [Att1ValueVal]       = @Att1ValueVal,\r\n\t                    [Att1StockMap]       = @Att1StockMap,\r\n                        [Att1StockVal]       = @Att1StockVal,\r\n                        [Att2NameMap]        = @Att2NameMap,\r\n                        [Att2NameVal]        = @Att2NameVal,\r\n                        [Att2ValueMap]       = @Att2ValueMap,\r\n                        [Att2ValueVal]       = @Att2ValueVal,\r\n\t                    [Att2StockMap]       = @Att2StockMap,\r\n                        [Att2StockVal]       = @Att2StockVal,\r\n                        [FilterName]         = @FilterName,\r\n                        [FilterMap]          = @FilterMap,\r\n                        [Filter1Name]        = @Filter1Name,\r\n                        [Filter1Map]         = @Filter1Map,\r\n                        [Filter2Name]        = @Filter2Name,\r\n                        [Filter2Map]         = @Filter2Map,\r\n                        [Filter3Name]        = @Filter3Name,\r\n                        [Filter3Map]         = @Filter3Map,\r\n                        [Filter4Name]        = @Filter4Name,\r\n                        [Filter4Map]         = @Filter4Map,\r\n                        [Filter5Name]        = @Filter5Name,\r\n                        [Filter5Map]         = @Filter5Map,\r\n                        [Filter6Name]        = @Filter6Name,\r\n                        [Filter6Map]         = @Filter6Map,\r\n                        [Filter7Name]        = @Filter7Name,\r\n                        [Filter7Map]         = @Filter7Map,\r\n                        [Filter8Name]        = @Filter8Name,\r\n                        [Filter8Map]         = @Filter8Map,\r\n                        [Filter9Name]        = @Filter9Name,\r\n                        [Filter9Map]         = @Filter9Map,\r\n                        [CatIsId]         = @CatIsId,\r\n                        [Cat1IsId]         = @Cat1IsId,\r\n                        [Cat2IsId]         = @Cat2IsId,\r\n                        [Cat3IsId]         = @Cat3IsId,\r\n                        [Cat4IsId]         = @Cat4IsId,\r\n                        [ManufacturerIsId]         = @ManufacturerIsId,\r\n                        [VendorIsId]         = @VendorIsId,\r\n                        [VendorMap]         = @VendorMap,\r\n                        [VendorVal]         = @VendorVal,\r\n                        [TaxCategoryMap]         = @TaxCategoryMap,\r\n                        [TaxCategoryVal]         = @TaxCategoryVal,\r\n                        [GtinMap]         = @GtinMap,\r\n                        [GtinVal]         = @GtinVal,\r\n                        [WeightMap]         = @WeightMap,\r\n                        [WeightVal]         = @WeightVal,\r\n                        [ImageIsBinary]         = @ImageIsBinary,\r\n                        [Image1IsBinary]         = @Image1IsBinary,\r\n                        [Image2IsBinary]         = @Image2IsBinary,\r\n                        [Image3IsBinary]         = @Image3IsBinary,\r\n                        [Image4IsBinary]         = @Image4IsBinary,\r\n                        [ConfigData] = @ConfigData\r\n                WHERE [VendorSourceId]=@VendorSourceId";
				SqlCeCommand comm = new SqlCeCommand(sqlString, conn)
				{
					Parameters = 
					{
						new SqlCeParameter("@VendorSourceId", sourceMapping.VendorSourceId),
						new SqlCeParameter("@ProdInfoRepeatNode", GlobalClass.ObjectToString(sourceMapping.ProdInfoRepeatNode)),
						new SqlCeParameter("@ProdIdMap", GlobalClass.ObjectToString(sourceMapping.ProdIdMap)),
						new SqlCeParameter("@ProdSkuMap", GlobalClass.ObjectToString(sourceMapping.ProdSkuMap)),
						new SqlCeParameter("@ProdSkuVal", GlobalClass.ObjectToString(sourceMapping.ProdSkuVal)),
						new SqlCeParameter("@ProdNameMap", GlobalClass.ObjectToString(sourceMapping.ProdNameMap)),
						new SqlCeParameter("@ProdNameVal", GlobalClass.ObjectToString(sourceMapping.ProdNameVal)),
						new SqlCeParameter("@ProdManPartNumMap", GlobalClass.ObjectToString(sourceMapping.ProdManPartNumMap)),
						new SqlCeParameter("@ProdManPartNumVal", GlobalClass.ObjectToString(sourceMapping.ProdManPartNumVal)),
						new SqlCeParameter("@ProdCostMap", GlobalClass.ObjectToString(sourceMapping.ProdCostMap)),
						new SqlCeParameter("@ProdCostVal", GlobalClass.ObjectToString(sourceMapping.ProdCostVal)),
						new SqlCeParameter("@ProdPriceMap", GlobalClass.ObjectToString(sourceMapping.ProdPriceMap)),
						new SqlCeParameter("@ProdPriceVal", GlobalClass.ObjectToString(sourceMapping.ProdPriceVal)),
						new SqlCeParameter("@ProdStockMap", GlobalClass.ObjectToString(sourceMapping.ProdStockMap)),
						new SqlCeParameter("@ProdStockVal", GlobalClass.ObjectToString(sourceMapping.ProdStockVal)),
						new SqlCeParameter("@SeoMetaKeyMap", GlobalClass.ObjectToString(sourceMapping.SeoMetaKeyMap)),
						new SqlCeParameter("@SeoMetaKeyVal", GlobalClass.ObjectToString(sourceMapping.SeoMetaKeyVal)),
						new SqlCeParameter("@SeoMetaDescMap", GlobalClass.ObjectToString(sourceMapping.SeoMetaDescMap)),
						new SqlCeParameter("@SeoMetaDescVal", GlobalClass.ObjectToString(sourceMapping.SeoMetaDescVal)),
						new SqlCeParameter("@SeoMetaTitleMap", GlobalClass.ObjectToString(sourceMapping.SeoMetaTitleMap)),
						new SqlCeParameter("@SeoMetaTitleVal", GlobalClass.ObjectToString(sourceMapping.SeoMetaTitleVal)),
						new SqlCeParameter("@SeoPageMap", GlobalClass.ObjectToString(sourceMapping.SeoPageMap)),
						new SqlCeParameter("@SeoPageVal", GlobalClass.ObjectToString(sourceMapping.SeoPageVal)),
						new SqlCeParameter("@CatMap", GlobalClass.ObjectToString(sourceMapping.CatMap)),
						new SqlCeParameter("@CatVal", GlobalClass.ObjectToString(sourceMapping.CatVal))
					}
				};
				if (sourceMapping.CatDelimeter.HasValue)
				{
					comm.Parameters.Add(new SqlCeParameter("@CatDelimeter", GlobalClass.ObjectToString(sourceMapping.CatDelimeter)));
				}
				else
				{
					comm.Parameters.Add(new SqlCeParameter("@CatDelimeter", DBNull.Value));
				}
				comm.Parameters.Add(new SqlCeParameter("@Cat1Map", GlobalClass.ObjectToString(sourceMapping.Cat1Map)));
				comm.Parameters.Add(new SqlCeParameter("@Cat1Val", GlobalClass.ObjectToString(sourceMapping.Cat1Val)));
				comm.Parameters.Add(new SqlCeParameter("@Cat2Map", GlobalClass.ObjectToString(sourceMapping.Cat2Map)));
				comm.Parameters.Add(new SqlCeParameter("@Cat2Val", GlobalClass.ObjectToString(sourceMapping.Cat2Val)));
				comm.Parameters.Add(new SqlCeParameter("@Cat3Map", GlobalClass.ObjectToString(sourceMapping.Cat3Map)));
				comm.Parameters.Add(new SqlCeParameter("@Cat3Val", GlobalClass.ObjectToString(sourceMapping.Cat3Val)));
				comm.Parameters.Add(new SqlCeParameter("@Cat4Map", GlobalClass.ObjectToString(sourceMapping.Cat4Map)));
				comm.Parameters.Add(new SqlCeParameter("@Cat4Val", GlobalClass.ObjectToString(sourceMapping.Cat4Val)));
				comm.Parameters.Add(new SqlCeParameter("@ManufactMap", GlobalClass.ObjectToString(sourceMapping.ManufactMap)));
				comm.Parameters.Add(new SqlCeParameter("@ManufactVal", GlobalClass.ObjectToString(sourceMapping.ManufactVal)));
				comm.Parameters.Add(new SqlCeParameter("@ImageMap", GlobalClass.ObjectToString(sourceMapping.ImageMap)));
				comm.Parameters.Add(new SqlCeParameter("@Image1Map", GlobalClass.ObjectToString(sourceMapping.Image1Map)));
				comm.Parameters.Add(new SqlCeParameter("@Image2Map", GlobalClass.ObjectToString(sourceMapping.Image2Map)));
				comm.Parameters.Add(new SqlCeParameter("@Image3Map", GlobalClass.ObjectToString(sourceMapping.Image3Map)));
				comm.Parameters.Add(new SqlCeParameter("@Image4Map", GlobalClass.ObjectToString(sourceMapping.Image4Map)));
				comm.Parameters.Add(new SqlCeParameter("@AttRepeatNodeMap", GlobalClass.ObjectToString(sourceMapping.AttRepeatNodeMap)));
				comm.Parameters.Add(new SqlCeParameter("@AttNameMap", GlobalClass.ObjectToString(sourceMapping.AttNameMap)));
				comm.Parameters.Add(new SqlCeParameter("@AttNameVal", GlobalClass.ObjectToString(sourceMapping.AttNameVal)));
				comm.Parameters.Add(new SqlCeParameter("@AttValueMap", GlobalClass.ObjectToString(sourceMapping.AttValueMap)));
				comm.Parameters.Add(new SqlCeParameter("@AttValueVal", GlobalClass.ObjectToString(sourceMapping.AttValueVal)));
				comm.Parameters.Add(new SqlCeParameter("@AttStockMap", GlobalClass.ObjectToString(sourceMapping.AttStockMap)));
				comm.Parameters.Add(new SqlCeParameter("@AttStockVal", GlobalClass.ObjectToString(sourceMapping.AttStockVal)));
				comm.Parameters.Add(new SqlCeParameter("@Att1NameMap", GlobalClass.ObjectToString(sourceMapping.Att1NameMap)));
				comm.Parameters.Add(new SqlCeParameter("@Att1NameVal", GlobalClass.ObjectToString(sourceMapping.Att1NameVal)));
				comm.Parameters.Add(new SqlCeParameter("@Att1ValueMap", GlobalClass.ObjectToString(sourceMapping.Att1ValueMap)));
				comm.Parameters.Add(new SqlCeParameter("@Att1ValueVal", GlobalClass.ObjectToString(sourceMapping.Att1ValueVal)));
				comm.Parameters.Add(new SqlCeParameter("@Att1StockMap", GlobalClass.ObjectToString(sourceMapping.Att1StockMap)));
				comm.Parameters.Add(new SqlCeParameter("@Att1StockVal", GlobalClass.ObjectToString(sourceMapping.Att1StockVal)));
				comm.Parameters.Add(new SqlCeParameter("@Att2NameMap", GlobalClass.ObjectToString(sourceMapping.Att2NameMap)));
				comm.Parameters.Add(new SqlCeParameter("@Att2NameVal", GlobalClass.ObjectToString(sourceMapping.Att2NameVal)));
				comm.Parameters.Add(new SqlCeParameter("@Att2ValueMap", GlobalClass.ObjectToString(sourceMapping.Att2ValueMap)));
				comm.Parameters.Add(new SqlCeParameter("@Att2ValueVal", GlobalClass.ObjectToString(sourceMapping.Att2ValueVal)));
				comm.Parameters.Add(new SqlCeParameter("@Att2StockMap", GlobalClass.ObjectToString(sourceMapping.Att2StockMap)));
				comm.Parameters.Add(new SqlCeParameter("@Att2StockVal", GlobalClass.ObjectToString(sourceMapping.Att2StockVal)));
				comm.Parameters.Add(new SqlCeParameter("@FilterName", GlobalClass.ObjectToString(sourceMapping.FilterName)));
				comm.Parameters.Add(new SqlCeParameter("@FilterMap", GlobalClass.ObjectToString(sourceMapping.FilterMap)));
				comm.Parameters.Add(new SqlCeParameter("@Filter1Name", GlobalClass.ObjectToString(sourceMapping.Filter1Name)));
				comm.Parameters.Add(new SqlCeParameter("@Filter1Map", GlobalClass.ObjectToString(sourceMapping.Filter1Map)));
				comm.Parameters.Add(new SqlCeParameter("@Filter2Name", GlobalClass.ObjectToString(sourceMapping.Filter2Name)));
				comm.Parameters.Add(new SqlCeParameter("@Filter2Map", GlobalClass.ObjectToString(sourceMapping.Filter2Map)));
				comm.Parameters.Add(new SqlCeParameter("@Filter3Name", GlobalClass.ObjectToString(sourceMapping.Filter3Name)));
				comm.Parameters.Add(new SqlCeParameter("@Filter3Map", GlobalClass.ObjectToString(sourceMapping.Filter3Map)));
				comm.Parameters.Add(new SqlCeParameter("@Filter4Name", GlobalClass.ObjectToString(sourceMapping.Filter4Name)));
				comm.Parameters.Add(new SqlCeParameter("@Filter4Map", GlobalClass.ObjectToString(sourceMapping.Filter4Map)));
				comm.Parameters.Add(new SqlCeParameter("@Filter5Name", GlobalClass.ObjectToString(sourceMapping.Filter5Name)));
				comm.Parameters.Add(new SqlCeParameter("@Filter5Map", GlobalClass.ObjectToString(sourceMapping.Filter5Map)));
				comm.Parameters.Add(new SqlCeParameter("@Filter6Name", GlobalClass.ObjectToString(sourceMapping.Filter6Name)));
				comm.Parameters.Add(new SqlCeParameter("@Filter6Map", GlobalClass.ObjectToString(sourceMapping.Filter6Map)));
				comm.Parameters.Add(new SqlCeParameter("@Filter7Name", GlobalClass.ObjectToString(sourceMapping.Filter7Name)));
				comm.Parameters.Add(new SqlCeParameter("@Filter7Map", GlobalClass.ObjectToString(sourceMapping.Filter7Map)));
				comm.Parameters.Add(new SqlCeParameter("@Filter8Name", GlobalClass.ObjectToString(sourceMapping.Filter8Name)));
				comm.Parameters.Add(new SqlCeParameter("@Filter8Map", GlobalClass.ObjectToString(sourceMapping.Filter8Map)));
				comm.Parameters.Add(new SqlCeParameter("@Filter9Name", GlobalClass.ObjectToString(sourceMapping.Filter9Name)));
				comm.Parameters.Add(new SqlCeParameter("@Filter9Map", GlobalClass.ObjectToString(sourceMapping.Filter9Map)));
				comm.Parameters.Add(new SqlCeParameter("@CatIsId", GlobalClass.ObjectToBool(sourceMapping.CatIsId)));
				comm.Parameters.Add(new SqlCeParameter("@Cat1IsId", GlobalClass.ObjectToBool(sourceMapping.Cat1IsId)));
				comm.Parameters.Add(new SqlCeParameter("@Cat2IsId", GlobalClass.ObjectToBool(sourceMapping.Cat2IsId)));
				comm.Parameters.Add(new SqlCeParameter("@Cat3IsId", GlobalClass.ObjectToBool(sourceMapping.Cat3IsId)));
				comm.Parameters.Add(new SqlCeParameter("@Cat4IsId", GlobalClass.ObjectToBool(sourceMapping.Cat4IsId)));
				comm.Parameters.Add(new SqlCeParameter("@ManufacturerIsId", GlobalClass.ObjectToBool(sourceMapping.ManufacturerIsId)));
				comm.Parameters.Add(new SqlCeParameter("@VendorIsId", GlobalClass.ObjectToBool(sourceMapping.VendorIsId)));
				comm.Parameters.Add(new SqlCeParameter("@VendorMap", GlobalClass.ObjectToString(sourceMapping.VendorMap)));
				comm.Parameters.Add(new SqlCeParameter("@VendorVal", GlobalClass.ObjectToString(sourceMapping.VendorVal)));
				comm.Parameters.Add(new SqlCeParameter("@ImageIsBinary", GlobalClass.ObjectToBool(sourceMapping.ImageIsBinary)));
				comm.Parameters.Add(new SqlCeParameter("@Image1IsBinary", GlobalClass.ObjectToBool(sourceMapping.Image1IsBinary)));
				comm.Parameters.Add(new SqlCeParameter("@Image2IsBinary", GlobalClass.ObjectToBool(sourceMapping.Image2IsBinary)));
				comm.Parameters.Add(new SqlCeParameter("@Image3IsBinary", GlobalClass.ObjectToBool(sourceMapping.Image3IsBinary)));
				comm.Parameters.Add(new SqlCeParameter("@Image4IsBinary", GlobalClass.ObjectToBool(sourceMapping.Image4IsBinary)));
				comm.Parameters.Add(new SqlCeParameter("@ImageVal", GlobalClass.ObjectToString(sourceMapping.ImageVal)));
				comm.Parameters.Add(new SqlCeParameter("@Image1Val", GlobalClass.ObjectToString(sourceMapping.Image1Val)));
				comm.Parameters.Add(new SqlCeParameter("@Image2Val", GlobalClass.ObjectToString(sourceMapping.Image2Val)));
				comm.Parameters.Add(new SqlCeParameter("@Image3Val", GlobalClass.ObjectToString(sourceMapping.Image3Val)));
				comm.Parameters.Add(new SqlCeParameter("@Image4Val", GlobalClass.ObjectToString(sourceMapping.Image4Val)));
				comm.Parameters.Add(new SqlCeParameter("@TaxCategoryMap", GlobalClass.ObjectToString(sourceMapping.TaxCategoryMap)));
				comm.Parameters.Add(new SqlCeParameter("@TaxCategoryVal", GlobalClass.ObjectToString(sourceMapping.TaxCategoryVal)));
				comm.Parameters.Add(new SqlCeParameter("@GtinMap", GlobalClass.ObjectToString(sourceMapping.GtinMap)));
				comm.Parameters.Add(new SqlCeParameter("@GtinVal", GlobalClass.ObjectToString(sourceMapping.GtinVal)));
				comm.Parameters.Add(new SqlCeParameter("@WeightMap", GlobalClass.ObjectToString(sourceMapping.WeightMap)));
				comm.Parameters.Add(new SqlCeParameter("@WeightVal", GlobalClass.ObjectToString(sourceMapping.WeightVal)));
				comm.Parameters.Add(new SqlCeParameter("@ConfigData", JsonConvert.SerializeObject(sourceMapping)));
				conn.Open();
				comm.ExecuteNonQuery();
				conn.Close();
				comm?.Dispose();
			}
		}

		public SourceData GetSourceData(int vendorSourceId)
		{
			SourceData sourceData = new SourceData();
			string dataConnectionString = dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString;
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataConnectionString);
			string commandText = "select VendorSource.*, Vendor.Name As VendorName FROM  Vendor INNER JOIN VendorSource ON Vendor.Id = VendorSource.VendorId where VendorSource.Id=@VendorSourceId";
			SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorSourceId", vendorSourceId));
			sqlCeConnection.Open();
			SqlCeDataReader sqlCeDataReader = sqlCeCommand.ExecuteReader();
			if (sqlCeDataReader.Read())
			{
				sourceData.VendorSourceId = vendorSourceId;
				sourceData.CsvDelimeter = GlobalClass.ObjectTChar(GlobalClass.ObjectTChar(sqlCeDataReader["CsvDelimeter"]));
				sourceData.SourceType = GlobalClass.StringToInteger(sqlCeDataReader["SourceType"], -1);
				sourceData.SourceAddress = GlobalClass.ObjectToString(sqlCeDataReader["SourceAddress"]);
				sourceData.SourceUser = GlobalClass.ObjectToString(sqlCeDataReader["SourceUser"]);
				sourceData.SourcePassword = GlobalClass.ObjectToString(sqlCeDataReader["SourcePassword"]);
				sourceData.SourceFormat = GlobalClass.StringToInteger(sqlCeDataReader["SourceFormat"], -1);
				sourceData.ScheduleType = GlobalClass.StringToInteger(sqlCeDataReader["ScheduleType"], -1);
				sourceData.ScheduleInterval = GlobalClass.StringToInteger(sqlCeDataReader["ScheduleInterval"]);
				sourceData.ScheduleHour = GlobalClass.StringToInteger(sqlCeDataReader["ScheduleHour"]);
				sourceData.ScheduleMinute = GlobalClass.StringToInteger(sqlCeDataReader["ScheduleMinute"]);
				sourceData.FirstRowIndex = GlobalClass.StringToInteger(sqlCeDataReader["FirstRowIndex"]);
				sourceData.VendorName = GlobalClass.ObjectToString(sqlCeDataReader["VendorName"]);
				sourceData.SoapAction = GlobalClass.ObjectToString(sqlCeDataReader["SoapAction"]);
				sourceData.SoapRequest = GlobalClass.ObjectToString(sqlCeDataReader["SoapRequest"]);
				sourceData.ProductIdInStore = GlobalClass.StringToInteger(sqlCeDataReader["ProductIdInStore"]);
				sourceData.UserIdInStore = GlobalClass.StringToInteger(sqlCeDataReader["UserIdInStore"]);
				sourceData.EntityType = (EntityType)GlobalClass.StringToInteger(sqlCeDataReader["EntityType"]);
				sourceData.SourceEncoding = GlobalClass.ObjectToString(sqlCeDataReader["SourceEncoding"]);
				sourceData.SourceContent = GlobalClass.ObjectToString(sqlCeDataReader["SourceContent"]);
				sourceData.AuthHeader = GlobalClass.ObjectToString(sqlCeDataReader["AuthHeader"]);
			}
			sqlCeDataReader.Close();
			if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
			{
				sqlCeConnection.Close();
			}
			sqlCeCommand?.Dispose();
			return sourceData;
		}

		public SourceMapping GetSourceMapping(int vendorSourceId)
		{
			SourceMapping sourceMapping = new SourceMapping();
			string dataConnectionString = dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString;
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataConnectionString);
			string commandText = "select * from SourceMapping where VendorSourceId=@VendorSourceId";
			SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorSourceId", vendorSourceId));
			sqlCeConnection.Open();
			SqlCeDataReader sqlCeDataReader = sqlCeCommand.ExecuteReader();
			if (sqlCeDataReader.Read())
			{
				if (!string.IsNullOrEmpty(GlobalClass.ObjectToString(sqlCeDataReader["ConfigData"])))
				{
					sourceMapping = JsonConvert.DeserializeObject<SourceMapping>(GlobalClass.ObjectToString(sqlCeDataReader["ConfigData"]));
					sourceMapping.VendorSourceId = vendorSourceId;
					sourceMapping.SourceMappingId = GlobalClass.StringToInteger(sqlCeDataReader["Id"]);
					sourceMapping.EntityType = (sourceMapping?.EntityType ?? EntityType.Products);
					sourceMapping.ProdInfoRepeatNode = GlobalClass.ObjectToString(sourceMapping?.ProdInfoRepeatNode);
					sourceMapping.ProdIdMap = GlobalClass.ObjectToString(sourceMapping?.ProdIdMap);
					sourceMapping.ProdSkuMap = GlobalClass.ObjectToString(sourceMapping?.ProdSkuMap);
					sourceMapping.ProdSkuVal = GlobalClass.ObjectToString(sourceMapping?.ProdSkuVal);
					sourceMapping.ProdNameMap = GlobalClass.ObjectToString(sourceMapping?.ProdNameMap);
					sourceMapping.ProdNameVal = GlobalClass.ObjectToString(sourceMapping?.ProdNameVal);
					sourceMapping.ProdShortDescMap = GlobalClass.ObjectToString(sourceMapping?.ProdShortDescMap);
					sourceMapping.ProdShortDescVal = GlobalClass.ObjectToString(sourceMapping?.ProdShortDescVal);
					sourceMapping.ProdFullDescMap = GlobalClass.ObjectToString(sourceMapping?.ProdFullDescMap);
					sourceMapping.ProdFullDescVal = GlobalClass.ObjectToString(sourceMapping?.ProdFullDescVal);
					sourceMapping.ProdManPartNumMap = GlobalClass.ObjectToString(sourceMapping?.ProdManPartNumMap);
					sourceMapping.ProdManPartNumVal = GlobalClass.ObjectToString(sourceMapping?.ProdManPartNumVal);
					sourceMapping.ProdCostMap = GlobalClass.ObjectToString(sourceMapping?.ProdCostMap);
					sourceMapping.ProdCostVal = GlobalClass.ObjectToString(sourceMapping?.ProdCostVal);
					sourceMapping.ProdOldPriceMap = GlobalClass.ObjectToString(sourceMapping?.ProdOldPriceMap);
					sourceMapping.ProdOldPriceVal = GlobalClass.ObjectToString(sourceMapping?.ProdOldPriceVal);
					sourceMapping.ProdPriceMap = GlobalClass.ObjectToString(sourceMapping?.ProdPriceMap);
					sourceMapping.ProdPriceVal = GlobalClass.ObjectToString(sourceMapping?.ProdPriceVal);
					sourceMapping.ProdStockMap = GlobalClass.ObjectToString(sourceMapping?.ProdStockMap);
					sourceMapping.ProdStockVal = GlobalClass.ObjectToString(sourceMapping?.ProdStockVal);
					sourceMapping.SeoMetaKeyMap = GlobalClass.ObjectToString(sourceMapping?.SeoMetaKeyMap);
					sourceMapping.SeoMetaKeyVal = GlobalClass.ObjectToString(sourceMapping?.SeoMetaKeyVal);
					sourceMapping.SeoMetaDescMap = GlobalClass.ObjectToString(sourceMapping?.SeoMetaDescMap);
					sourceMapping.SeoMetaDescVal = GlobalClass.ObjectToString(sourceMapping?.SeoMetaDescVal);
					sourceMapping.SeoMetaTitleMap = GlobalClass.ObjectToString(sourceMapping?.SeoMetaTitleMap);
					sourceMapping.SeoMetaTitleVal = GlobalClass.ObjectToString(sourceMapping?.SeoMetaTitleVal);
					sourceMapping.SeoPageMap = GlobalClass.ObjectToString(sourceMapping?.SeoPageMap);
					sourceMapping.SeoPageVal = GlobalClass.ObjectToString(sourceMapping?.SeoPageVal);
					sourceMapping.CatMap = GlobalClass.ObjectToString(sourceMapping?.CatMap);
					sourceMapping.CatVal = GlobalClass.ObjectToString(sourceMapping?.CatVal);
					sourceMapping.CatDelimeter = GlobalClass.ObjectTChar(sourceMapping?.CatDelimeter);
					sourceMapping.Cat1Map = GlobalClass.ObjectToString(sourceMapping?.Cat1Map);
					sourceMapping.Cat1Val = GlobalClass.ObjectToString(sourceMapping?.Cat1Val);
					sourceMapping.Cat2Map = GlobalClass.ObjectToString(sourceMapping?.Cat2Map);
					sourceMapping.Cat2Val = GlobalClass.ObjectToString(sourceMapping?.Cat2Val);
					sourceMapping.Cat3Map = GlobalClass.ObjectToString(sourceMapping?.Cat3Map);
					sourceMapping.Cat3Val = GlobalClass.ObjectToString(sourceMapping?.Cat3Val);
					sourceMapping.Cat4Map = GlobalClass.ObjectToString(sourceMapping?.Cat4Map);
					sourceMapping.Cat4Val = GlobalClass.ObjectToString(sourceMapping?.Cat4Val);
					sourceMapping.ManufactMap = GlobalClass.ObjectToString(sourceMapping?.ManufactMap);
					sourceMapping.ManufactVal = GlobalClass.ObjectToString(sourceMapping?.ManufactVal);
					sourceMapping.ImageMap = GlobalClass.ObjectToString(sourceMapping?.ImageMap);
					sourceMapping.Image1Map = GlobalClass.ObjectToString(sourceMapping?.Image1Map);
					sourceMapping.Image2Map = GlobalClass.ObjectToString(sourceMapping?.Image2Map);
					sourceMapping.Image3Map = GlobalClass.ObjectToString(sourceMapping?.Image3Map);
					sourceMapping.Image4Map = GlobalClass.ObjectToString(sourceMapping?.Image4Map);
					sourceMapping.ImageSeoMap = GlobalClass.ObjectToString(sourceMapping?.ImageSeoMap);
					sourceMapping.ImageSeo1Map = GlobalClass.ObjectToString(sourceMapping?.ImageSeo1Map);
					sourceMapping.ImageSeo2Map = GlobalClass.ObjectToString(sourceMapping?.ImageSeo2Map);
					sourceMapping.ImageSeo3Map = GlobalClass.ObjectToString(sourceMapping?.ImageSeo3Map);
					sourceMapping.ImageSeo4Map = GlobalClass.ObjectToString(sourceMapping?.ImageSeo4Map);
					sourceMapping.ImageSeoVal = GlobalClass.ObjectToString(sourceMapping?.ImageSeoVal);
					sourceMapping.ImageSeo1Val = GlobalClass.ObjectToString(sourceMapping?.ImageSeo1Val);
					sourceMapping.ImageSeo2Val = GlobalClass.ObjectToString(sourceMapping?.ImageSeo2Val);
					sourceMapping.ImageSeo3Val = GlobalClass.ObjectToString(sourceMapping?.ImageSeo3Val);
					sourceMapping.ImageSeo4Val = GlobalClass.ObjectToString(sourceMapping?.ImageSeo4Val);
					sourceMapping.AttRepeatNodeMap = GlobalClass.ObjectToString(sourceMapping?.AttRepeatNodeMap);
					sourceMapping.AttNameMap = GlobalClass.ObjectToString(sourceMapping?.AttNameMap);
					sourceMapping.AttNameVal = GlobalClass.ObjectToString(sourceMapping?.AttNameVal);
					sourceMapping.AttValueMap = GlobalClass.ObjectToString(sourceMapping?.AttValueMap);
					sourceMapping.AttValueVal = GlobalClass.ObjectToString(sourceMapping?.AttValueVal);
					sourceMapping.AttStockMap = GlobalClass.ObjectToString(sourceMapping?.AttStockMap);
					sourceMapping.AttStockVal = GlobalClass.ObjectToString(sourceMapping?.AttStockVal);
					sourceMapping.Att1NameMap = GlobalClass.ObjectToString(sourceMapping?.Att1NameMap);
					sourceMapping.Att1NameVal = GlobalClass.ObjectToString(sourceMapping?.Att1NameVal);
					sourceMapping.Att1ValueMap = GlobalClass.ObjectToString(sourceMapping?.Att1ValueMap);
					sourceMapping.Att1ValueVal = GlobalClass.ObjectToString(sourceMapping?.Att1ValueVal);
					sourceMapping.Att1StockMap = GlobalClass.ObjectToString(sourceMapping?.Att1StockMap);
					sourceMapping.Att1StockVal = GlobalClass.ObjectToString(sourceMapping?.Att1StockVal);
					sourceMapping.Att2NameMap = GlobalClass.ObjectToString(sourceMapping?.Att2NameMap);
					sourceMapping.Att2NameVal = GlobalClass.ObjectToString(sourceMapping?.Att2NameVal);
					sourceMapping.Att2ValueMap = GlobalClass.ObjectToString(sourceMapping?.Att2ValueMap);
					sourceMapping.Att2ValueVal = GlobalClass.ObjectToString(sourceMapping?.Att2ValueVal);
					sourceMapping.Att2StockMap = GlobalClass.ObjectToString(sourceMapping?.Att2StockMap);
					sourceMapping.Att2StockVal = GlobalClass.ObjectToString(sourceMapping?.Att2StockVal);
					sourceMapping.FilterName = GlobalClass.ObjectToString(sourceMapping?.FilterName);
					sourceMapping.FilterMap = GlobalClass.ObjectToString(sourceMapping?.FilterMap);
					sourceMapping.Filter1Name = GlobalClass.ObjectToString(sourceMapping?.Filter1Name);
					sourceMapping.Filter1Map = GlobalClass.ObjectToString(sourceMapping?.Filter1Map);
					sourceMapping.Filter2Name = GlobalClass.ObjectToString(sourceMapping?.Filter2Name);
					sourceMapping.Filter2Map = GlobalClass.ObjectToString(sourceMapping?.Filter2Map);
					sourceMapping.Filter3Name = GlobalClass.ObjectToString(sourceMapping?.Filter3Name);
					sourceMapping.Filter3Map = GlobalClass.ObjectToString(sourceMapping?.Filter3Map);
					sourceMapping.Filter4Name = GlobalClass.ObjectToString(sourceMapping?.Filter4Name);
					sourceMapping.Filter4Map = GlobalClass.ObjectToString(sourceMapping?.Filter4Map);
					sourceMapping.Filter5Name = GlobalClass.ObjectToString(sourceMapping?.Filter5Name);
					sourceMapping.Filter5Map = GlobalClass.ObjectToString(sourceMapping?.Filter5Map);
					sourceMapping.Filter6Name = GlobalClass.ObjectToString(sourceMapping?.Filter6Name);
					sourceMapping.Filter6Map = GlobalClass.ObjectToString(sourceMapping?.Filter6Map);
					sourceMapping.Filter7Name = GlobalClass.ObjectToString(sourceMapping?.Filter7Name);
					sourceMapping.Filter7Map = GlobalClass.ObjectToString(sourceMapping?.Filter7Map);
					sourceMapping.Filter8Name = GlobalClass.ObjectToString(sourceMapping?.Filter8Name);
					sourceMapping.Filter8Map = GlobalClass.ObjectToString(sourceMapping?.Filter8Map);
					sourceMapping.Filter9Name = GlobalClass.ObjectToString(sourceMapping?.Filter9Name);
					sourceMapping.Filter9Map = GlobalClass.ObjectToString(sourceMapping?.Filter9Map);
					sourceMapping.CatIsId = GlobalClass.ObjectToBool(sourceMapping?.CatIsId);
					sourceMapping.Cat1IsId = GlobalClass.ObjectToBool(sourceMapping?.Cat1IsId);
					sourceMapping.Cat2IsId = GlobalClass.ObjectToBool(sourceMapping?.Cat2IsId);
					sourceMapping.Cat3IsId = GlobalClass.ObjectToBool(sourceMapping?.Cat3IsId);
					sourceMapping.Cat4IsId = GlobalClass.ObjectToBool(sourceMapping?.Cat4IsId);
					sourceMapping.ManufacturerIsId = GlobalClass.ObjectToBool(sourceMapping?.ManufacturerIsId);
					sourceMapping.VendorIsId = GlobalClass.ObjectToBool(sourceMapping?.VendorIsId);
					sourceMapping.DeliveryDateIsId = GlobalClass.ObjectToBool(sourceMapping?.DeliveryDateIsId);
					sourceMapping.VendorMap = GlobalClass.ObjectToString(sourceMapping?.VendorMap);
					sourceMapping.VendorVal = GlobalClass.ObjectToString(sourceMapping?.VendorVal);
					sourceMapping.ImageIsBinary = GlobalClass.ObjectToBool(sourceMapping?.ImageIsBinary);
					sourceMapping.Image1IsBinary = GlobalClass.ObjectToBool(sourceMapping?.Image1IsBinary);
					sourceMapping.Image2IsBinary = GlobalClass.ObjectToBool(sourceMapping?.Image2IsBinary);
					sourceMapping.Image3IsBinary = GlobalClass.ObjectToBool(sourceMapping?.Image3IsBinary);
					sourceMapping.Image4IsBinary = GlobalClass.ObjectToBool(sourceMapping?.Image4IsBinary);
					sourceMapping.ImageVal = GlobalClass.ObjectToString(sourceMapping?.ImageVal);
					sourceMapping.Image1Val = GlobalClass.ObjectToString(sourceMapping?.Image1Val);
					sourceMapping.Image2Val = GlobalClass.ObjectToString(sourceMapping?.Image2Val);
					sourceMapping.Image3Val = GlobalClass.ObjectToString(sourceMapping?.Image3Val);
					sourceMapping.Image4Val = GlobalClass.ObjectToString(sourceMapping?.Image4Val);
					sourceMapping.TaxCategoryMap = GlobalClass.ObjectToString(sourceMapping?.TaxCategoryMap);
					sourceMapping.TaxCategoryVal = GlobalClass.ObjectToString(sourceMapping?.TaxCategoryVal);
					sourceMapping.GtinMap = GlobalClass.ObjectToString(sourceMapping?.GtinMap);
					sourceMapping.GtinVal = GlobalClass.ObjectToString(sourceMapping?.GtinVal);
					sourceMapping.WeightMap = GlobalClass.ObjectToString(sourceMapping?.WeightMap);
					sourceMapping.WeightVal = GlobalClass.ObjectToString(sourceMapping?.WeightVal);
					sourceMapping.LengthMap = GlobalClass.ObjectToString(sourceMapping?.LengthMap);
					sourceMapping.LengthVal = GlobalClass.ObjectToString(sourceMapping?.LengthVal);
					sourceMapping.WidthMap = GlobalClass.ObjectToString(sourceMapping?.WidthMap);
					sourceMapping.WidthVal = GlobalClass.ObjectToString(sourceMapping?.WidthVal);
					sourceMapping.HeightMap = GlobalClass.ObjectToString(sourceMapping?.HeightMap);
					sourceMapping.HeightVal = GlobalClass.ObjectToString(sourceMapping?.HeightVal);
				}
				else
				{
					sourceMapping.VendorSourceId = vendorSourceId;
					sourceMapping.SourceMappingId = GlobalClass.StringToInteger(sqlCeDataReader["Id"]);
					sourceMapping.EntityType = EntityType.Products;
					sourceMapping.ProdInfoRepeatNode = GlobalClass.ObjectToString(sqlCeDataReader["ProdInfoRepeatNode"]);
					sourceMapping.ProdIdMap = GlobalClass.ObjectToString(sqlCeDataReader["ProdIdMap"]);
					sourceMapping.ProdSkuMap = GlobalClass.ObjectToString(sqlCeDataReader["ProdSkuMap"]);
					sourceMapping.ProdSkuVal = GlobalClass.ObjectToString(sqlCeDataReader["ProdSkuVal"]);
					sourceMapping.ProdNameMap = GlobalClass.ObjectToString(sqlCeDataReader["ProdNameMap"]);
					sourceMapping.ProdNameVal = GlobalClass.ObjectToString(sqlCeDataReader["ProdNameVal"]);
					sourceMapping.ProdShortDescMap = GlobalClass.ObjectToString(sqlCeDataReader["ProdShortDescMap"]);
					sourceMapping.ProdShortDescVal = GlobalClass.ObjectToString(sqlCeDataReader["ProdShortDescVal"]);
					sourceMapping.ProdFullDescMap = GlobalClass.ObjectToString(sqlCeDataReader["ProdFullDescMap"]);
					sourceMapping.ProdFullDescVal = GlobalClass.ObjectToString(sqlCeDataReader["ProdFullDescVal"]);
					sourceMapping.ProdManPartNumMap = GlobalClass.ObjectToString(sqlCeDataReader["ProdManPartNumMap"]);
					sourceMapping.ProdManPartNumVal = GlobalClass.ObjectToString(sqlCeDataReader["ProdManPartNumVal"]);
					sourceMapping.ProdCostMap = GlobalClass.ObjectToString(sqlCeDataReader["ProdCostMap"]);
					sourceMapping.ProdCostVal = GlobalClass.ObjectToString(sqlCeDataReader["ProdCostVal"]);
					sourceMapping.ProdPriceMap = GlobalClass.ObjectToString(sqlCeDataReader["ProdPriceMap"]);
					sourceMapping.ProdPriceVal = GlobalClass.ObjectToString(sqlCeDataReader["ProdPriceVal"]);
					sourceMapping.ProdStockMap = GlobalClass.ObjectToString(sqlCeDataReader["ProdStockMap"]);
					sourceMapping.ProdStockVal = GlobalClass.ObjectToString(sqlCeDataReader["ProdStockVal"]);
					sourceMapping.SeoMetaKeyMap = GlobalClass.ObjectToString(sqlCeDataReader["SeoMetaKeyMap"]);
					sourceMapping.SeoMetaKeyVal = GlobalClass.ObjectToString(sqlCeDataReader["SeoMetaKeyVal"]);
					sourceMapping.SeoMetaDescMap = GlobalClass.ObjectToString(sqlCeDataReader["SeoMetaDescMap"]);
					sourceMapping.SeoMetaDescVal = GlobalClass.ObjectToString(sqlCeDataReader["SeoMetaDescVal"]);
					sourceMapping.SeoMetaTitleMap = GlobalClass.ObjectToString(sqlCeDataReader["SeoMetaTitleMap"]);
					sourceMapping.SeoMetaTitleVal = GlobalClass.ObjectToString(sqlCeDataReader["SeoMetaTitleVal"]);
					sourceMapping.SeoPageMap = GlobalClass.ObjectToString(sqlCeDataReader["SeoPageMap"]);
					sourceMapping.SeoPageVal = GlobalClass.ObjectToString(sqlCeDataReader["SeoPageVal"]);
					sourceMapping.CatMap = GlobalClass.ObjectToString(sqlCeDataReader["CatMap"]);
					sourceMapping.CatVal = GlobalClass.ObjectToString(sqlCeDataReader["CatVal"]);
					sourceMapping.CatDelimeter = GlobalClass.ObjectTChar(sqlCeDataReader["CatDelimeter"]);
					sourceMapping.Cat1Map = GlobalClass.ObjectToString(sqlCeDataReader["Cat1Map"]);
					sourceMapping.Cat1Val = GlobalClass.ObjectToString(sqlCeDataReader["Cat1Val"]);
					sourceMapping.Cat2Map = GlobalClass.ObjectToString(sqlCeDataReader["Cat2Map"]);
					sourceMapping.Cat2Val = GlobalClass.ObjectToString(sqlCeDataReader["Cat2Val"]);
					sourceMapping.Cat3Map = GlobalClass.ObjectToString(sqlCeDataReader["Cat3Map"]);
					sourceMapping.Cat3Val = GlobalClass.ObjectToString(sqlCeDataReader["Cat3Val"]);
					sourceMapping.Cat4Map = GlobalClass.ObjectToString(sqlCeDataReader["Cat4Map"]);
					sourceMapping.Cat4Val = GlobalClass.ObjectToString(sqlCeDataReader["Cat4Val"]);
					sourceMapping.ManufactMap = GlobalClass.ObjectToString(sqlCeDataReader["ManufactMap"]);
					sourceMapping.ManufactVal = GlobalClass.ObjectToString(sqlCeDataReader["ManufactVal"]);
					sourceMapping.ImageMap = GlobalClass.ObjectToString(sqlCeDataReader["ImageMap"]);
					sourceMapping.Image1Map = GlobalClass.ObjectToString(sqlCeDataReader["Image1Map"]);
					sourceMapping.Image2Map = GlobalClass.ObjectToString(sqlCeDataReader["Image2Map"]);
					sourceMapping.Image3Map = GlobalClass.ObjectToString(sqlCeDataReader["Image3Map"]);
					sourceMapping.Image4Map = GlobalClass.ObjectToString(sqlCeDataReader["Image4Map"]);
					sourceMapping.AttRepeatNodeMap = GlobalClass.ObjectToString(sqlCeDataReader["AttRepeatNodeMap"]);
					sourceMapping.AttNameMap = GlobalClass.ObjectToString(sqlCeDataReader["AttNameMap"]);
					sourceMapping.AttNameVal = GlobalClass.ObjectToString(sqlCeDataReader["AttNameVal"]);
					sourceMapping.AttValueMap = GlobalClass.ObjectToString(sqlCeDataReader["AttValueMap"]);
					sourceMapping.AttValueVal = GlobalClass.ObjectToString(sqlCeDataReader["AttValueVal"]);
					sourceMapping.AttStockMap = GlobalClass.ObjectToString(sqlCeDataReader["AttStockMap"]);
					sourceMapping.AttStockVal = GlobalClass.ObjectToString(sqlCeDataReader["AttStockVal"]);
					sourceMapping.Att1NameMap = GlobalClass.ObjectToString(sqlCeDataReader["Att1NameMap"]);
					sourceMapping.Att1NameVal = GlobalClass.ObjectToString(sqlCeDataReader["Att1NameVal"]);
					sourceMapping.Att1ValueMap = GlobalClass.ObjectToString(sqlCeDataReader["Att1ValueMap"]);
					sourceMapping.Att1ValueVal = GlobalClass.ObjectToString(sqlCeDataReader["Att1ValueVal"]);
					sourceMapping.Att1StockMap = GlobalClass.ObjectToString(sqlCeDataReader["Att1StockMap"]);
					sourceMapping.Att1StockVal = GlobalClass.ObjectToString(sqlCeDataReader["Att1StockVal"]);
					sourceMapping.Att2NameMap = GlobalClass.ObjectToString(sqlCeDataReader["Att2NameMap"]);
					sourceMapping.Att2NameVal = GlobalClass.ObjectToString(sqlCeDataReader["Att2NameVal"]);
					sourceMapping.Att2ValueMap = GlobalClass.ObjectToString(sqlCeDataReader["Att2ValueMap"]);
					sourceMapping.Att2ValueVal = GlobalClass.ObjectToString(sqlCeDataReader["Att2ValueVal"]);
					sourceMapping.Att2StockMap = GlobalClass.ObjectToString(sqlCeDataReader["Att2StockMap"]);
					sourceMapping.Att2StockVal = GlobalClass.ObjectToString(sqlCeDataReader["Att2StockVal"]);
					sourceMapping.FilterName = GlobalClass.ObjectToString(sqlCeDataReader["FilterName"]);
					sourceMapping.FilterMap = GlobalClass.ObjectToString(sqlCeDataReader["FilterMap"]);
					sourceMapping.Filter1Name = GlobalClass.ObjectToString(sqlCeDataReader["Filter1Name"]);
					sourceMapping.Filter1Map = GlobalClass.ObjectToString(sqlCeDataReader["Filter1Map"]);
					sourceMapping.Filter2Name = GlobalClass.ObjectToString(sqlCeDataReader["Filter2Name"]);
					sourceMapping.Filter2Map = GlobalClass.ObjectToString(sqlCeDataReader["Filter2Map"]);
					sourceMapping.Filter3Name = GlobalClass.ObjectToString(sqlCeDataReader["Filter3Name"]);
					sourceMapping.Filter3Map = GlobalClass.ObjectToString(sqlCeDataReader["Filter3Map"]);
					sourceMapping.Filter4Name = GlobalClass.ObjectToString(sqlCeDataReader["Filter4Name"]);
					sourceMapping.Filter4Map = GlobalClass.ObjectToString(sqlCeDataReader["Filter4Map"]);
					sourceMapping.Filter5Name = GlobalClass.ObjectToString(sqlCeDataReader["Filter5Name"]);
					sourceMapping.Filter5Map = GlobalClass.ObjectToString(sqlCeDataReader["Filter5Map"]);
					sourceMapping.Filter6Name = GlobalClass.ObjectToString(sqlCeDataReader["Filter6Name"]);
					sourceMapping.Filter6Map = GlobalClass.ObjectToString(sqlCeDataReader["Filter6Map"]);
					sourceMapping.Filter7Name = GlobalClass.ObjectToString(sqlCeDataReader["Filter7Name"]);
					sourceMapping.Filter7Map = GlobalClass.ObjectToString(sqlCeDataReader["Filter7Map"]);
					sourceMapping.Filter8Name = GlobalClass.ObjectToString(sqlCeDataReader["Filter8Name"]);
					sourceMapping.Filter8Map = GlobalClass.ObjectToString(sqlCeDataReader["Filter8Map"]);
					sourceMapping.Filter9Name = GlobalClass.ObjectToString(sqlCeDataReader["Filter9Name"]);
					sourceMapping.Filter9Map = GlobalClass.ObjectToString(sqlCeDataReader["Filter9Map"]);
					sourceMapping.CatIsId = GlobalClass.ObjectToBool(sqlCeDataReader["CatIsId"]);
					sourceMapping.Cat1IsId = GlobalClass.ObjectToBool(sqlCeDataReader["Cat1IsId"]);
					sourceMapping.Cat2IsId = GlobalClass.ObjectToBool(sqlCeDataReader["Cat2IsId"]);
					sourceMapping.Cat3IsId = GlobalClass.ObjectToBool(sqlCeDataReader["Cat3IsId"]);
					sourceMapping.Cat4IsId = GlobalClass.ObjectToBool(sqlCeDataReader["Cat4IsId"]);
					sourceMapping.ManufacturerIsId = GlobalClass.ObjectToBool(sqlCeDataReader["ManufacturerIsId"]);
					sourceMapping.VendorIsId = GlobalClass.ObjectToBool(sqlCeDataReader["VendorIsId"]);
					sourceMapping.VendorMap = GlobalClass.ObjectToString(sqlCeDataReader["VendorMap"]);
					sourceMapping.VendorVal = GlobalClass.ObjectToString(sqlCeDataReader["VendorVal"]);
					sourceMapping.ImageIsBinary = GlobalClass.ObjectToBool(sqlCeDataReader["ImageIsBinary"]);
					sourceMapping.Image1IsBinary = GlobalClass.ObjectToBool(sqlCeDataReader["Image1IsBinary"]);
					sourceMapping.Image2IsBinary = GlobalClass.ObjectToBool(sqlCeDataReader["Image2IsBinary"]);
					sourceMapping.Image3IsBinary = GlobalClass.ObjectToBool(sqlCeDataReader["Image3IsBinary"]);
					sourceMapping.Image4IsBinary = GlobalClass.ObjectToBool(sqlCeDataReader["Image4IsBinary"]);
					sourceMapping.ImageVal = GlobalClass.ObjectToString(sqlCeDataReader["ImageVal"]);
					sourceMapping.Image1Val = GlobalClass.ObjectToString(sqlCeDataReader["Image1Val"]);
					sourceMapping.Image2Val = GlobalClass.ObjectToString(sqlCeDataReader["Image2Val"]);
					sourceMapping.Image3Val = GlobalClass.ObjectToString(sqlCeDataReader["Image3Val"]);
					sourceMapping.Image4Val = GlobalClass.ObjectToString(sqlCeDataReader["Image4Val"]);
					sourceMapping.TaxCategoryMap = GlobalClass.ObjectToString(sqlCeDataReader["TaxCategoryMap"]);
					sourceMapping.TaxCategoryVal = GlobalClass.ObjectToString(sqlCeDataReader["TaxCategoryVal"]);
					sourceMapping.GtinMap = GlobalClass.ObjectToString(sqlCeDataReader["GtinMap"]);
					sourceMapping.GtinVal = GlobalClass.ObjectToString(sqlCeDataReader["GtinVal"]);
					sourceMapping.WeightMap = GlobalClass.ObjectToString(sqlCeDataReader["WeightMap"]);
					sourceMapping.WeightVal = GlobalClass.ObjectToString(sqlCeDataReader["WeightVal"]);
				}
			}
			sqlCeDataReader.Close();
			if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
			{
				sqlCeConnection.Close();
			}
			sqlCeCommand?.Dispose();
			return sourceMapping;
		}

		public async Task<int> InsertVendorSource(int vendorId, SourceData sourceData)
		{
			string cnString = dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString;
			SqlCeConnection conn = new SqlCeConnection(cnString);
			string sqlString = "\r\n            INSERT INTO [VendorSource]\r\n                   ([VendorId]\r\n                   ,[SourceType]\r\n                   ,[SourceAddress]\r\n                   ,[SourceUser]\r\n                   ,[SourcePassword]\r\n                   ,[SourceFormat]\r\n                   ,[FirstRowIndex]\r\n                   ,[CsvDelimeter]\r\n                   ,[SoapAction]\r\n                   ,[SoapRequest]\r\n                   ,[ProductIdInStore]\r\n                   ,[UserIdInStore]\r\n                   ,[EntityType]\r\n                   ,[SourceEncoding]\r\n                    )  \r\n             VALUES\r\n                   (@VendorId\r\n                   ,@SourceType\r\n                   ,@SourceAddress\r\n                   ,@SourceUser\r\n                   ,@SourcePassword\r\n                   ,@SourceFormat\r\n                   ,@FirstRowIndex\r\n                   ,@CsvDelimeter\r\n                   ,@SoapAction\r\n                   ,@SoapRequest\r\n                   ,@ProductIdInStore\r\n                   ,@UserIdInStore\r\n                   ,@EntityType\r\n                   ,@SourceEncoding\r\n                    )";
			SqlCeCommand comm = new SqlCeCommand(sqlString, conn)
			{
				Parameters = 
				{
					new SqlCeParameter("@VendorId", vendorId),
					new SqlCeParameter("@SourceType", sourceData.SourceType),
					new SqlCeParameter("@SourceAddress", GlobalClass.ObjectToString(sourceData.SourceAddress)),
					new SqlCeParameter("@SourceUser", GlobalClass.ObjectToString(sourceData.SourceUser)),
					new SqlCeParameter("@SourcePassword", GlobalClass.ObjectToString(sourceData.SourcePassword)),
					new SqlCeParameter("@SourceFormat", sourceData.SourceFormat),
					new SqlCeParameter("@FirstRowIndex", sourceData.FirstRowIndex.GetValueOrDefault()),
					new SqlCeParameter("@CsvDelimeter", GlobalClass.ObjectToString(sourceData.CsvDelimeter)),
					new SqlCeParameter("@SoapAction", GlobalClass.ObjectToString(sourceData.SoapAction)),
					new SqlCeParameter("@SoapRequest", GlobalClass.ObjectToString(sourceData.SoapRequest)),
					new SqlCeParameter("@ProductIdInStore", sourceData.ProductIdInStore),
					new SqlCeParameter("@UserIdInStore", sourceData.UserIdInStore),
					new SqlCeParameter("@EntityType", sourceData.EntityType),
					new SqlCeParameter("@SourceEncoding", sourceData.SourceEncoding)
				}
			};
			conn.Open();
			comm.ExecuteNonQuery();
			comm.CommandText = "SELECT @@IDENTITY";
			int retValue = Convert.ToInt32(comm.ExecuteScalar());
			conn.Close();
			if (conn != null && conn.State == ConnectionState.Open)
			{
				conn.Close();
			}
			comm?.Dispose();
			return retValue;
		}

		public void UpdateVendorSource(SourceData sourceData)
		{
			string dataConnectionString = dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString;
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataConnectionString);
			string commandText = "\r\n                UPDATE  [VendorSource] SET\r\n                        [SourceType] = @SourceType,\r\n                        [SourceAddress] = @SourceAddress,\r\n                        [SourceUser] = @SourceUser,\r\n                        [SourcePassword] = @SourcePassword,\r\n                        [SourceFormat] = @SourceFormat,\r\n                        [FirstRowIndex] = @FirstRowIndex,\r\n                        [CsvDelimeter] = @CsvDelimeter,\r\n                        [SoapAction] = @SoapAction,\r\n                        [SoapRequest] = @SoapRequest,\r\n                        [ProductIdInStore] = @ProductIdInStore,\r\n                        [UserIdInStore] = @UserIdInStore,\r\n                        [EntityType] = @EntityType,\r\n                        [SourceEncoding] = @SourceEncoding\r\n                WHERE [Id]=@VendorSourceId";
			SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorSourceId", sourceData.VendorSourceId));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceType", sourceData.SourceType));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceAddress", GlobalClass.ObjectToString(sourceData.SourceAddress)));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceUser", GlobalClass.ObjectToString(sourceData.SourceUser)));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourcePassword", GlobalClass.ObjectToString(sourceData.SourcePassword)));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceFormat", sourceData.SourceFormat));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@FirstRowIndex", sourceData.FirstRowIndex.GetValueOrDefault()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@CsvDelimeter", GlobalClass.ObjectToString(sourceData.CsvDelimeter)));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SoapAction", GlobalClass.ObjectToString(sourceData.SoapAction)));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SoapRequest", GlobalClass.ObjectToString(sourceData.SoapRequest)));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProductIdInStore", sourceData.ProductIdInStore));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@UserIdInStore", sourceData.UserIdInStore));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@EntityType", sourceData.EntityType));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceEncoding", sourceData.SourceEncoding));
			sqlCeConnection.Open();
			sqlCeCommand.ExecuteNonQuery();
			sqlCeConnection.Close();
			if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
			{
				sqlCeConnection.Close();
			}
			sqlCeCommand?.Dispose();
		}

		public static byte[] GetFileContentStatic(SourceType sourceType, string userName, string pass, string addressPath, string soapAction, string soapRequest, Encoding encoding, string authHeader)
		{
			byte[] result = null;
			SourceClient sourceClient = new SourceClient();
			switch (sourceType)
			{
			case SourceType.FileSytem:
				result = sourceClient.GetFileFromFileSystem(GlobalClass.NameWithoutDomain(userName), GlobalClass.GetDomainFromUsername(userName), pass, addressPath.Trim(), encoding, xls: false);
				break;
			case SourceType.WebPage:
				try
				{
					result = sourceClient.GetFileFromWeb(userName, "", pass, addressPath.Trim(), authHeader);
				}
				catch
				{
					result = sourceClient.GetFileFromWeb(GlobalClass.NameWithoutDomain(userName), GlobalClass.GetDomainFromUsername(userName), pass, addressPath.Trim(), authHeader);
				}
				break;
			case SourceType.Ftp:
				try
				{
					result = sourceClient.GetFileFromFtp(GlobalClass.NameWithoutDomain(userName), GlobalClass.GetDomainFromUsername(userName), pass, addressPath.Trim());
				}
				catch
				{
					result = sourceClient.GetFileFromFtp(userName, "", pass, addressPath.Trim());
				}
				break;
			case SourceType.WebService:
				try
				{
					result = sourceClient.GetFileFromWebService(GlobalClass.NameWithoutDomain(userName), GlobalClass.GetDomainFromUsername(userName), pass, addressPath.Trim(), soapAction, soapRequest, encoding);
				}
				catch
				{
					result = sourceClient.GetFileFromWebService(userName, "", pass, addressPath.Trim(), soapAction, soapRequest, encoding);
				}
				break;
			}
			return result;
		}

		public byte[] GetFileContent(SourceType sourceType, string userName, string pass, string addressPath, string soapAction, string soapRequest, string encodingCode, bool xls, string content, string authHeader)
		{
			byte[] result = null;
			Encoding encoding = GetEncoding(encodingCode);
			SourceClient sourceClient = new SourceClient();
			switch (sourceType)
			{
			case SourceType.FileSytem:
				result = sourceClient.GetFileFromFileSystem(GlobalClass.NameWithoutDomain(userName), GlobalClass.GetDomainFromUsername(userName), pass, addressPath.Trim(), encoding, xls);
				break;
			case SourceType.WebPage:
				try
				{
					result = sourceClient.GetFileFromWeb(userName, "", pass, addressPath.Trim(), authHeader);
				}
				catch
				{
					result = sourceClient.GetFileFromWeb(GlobalClass.NameWithoutDomain(userName), GlobalClass.GetDomainFromUsername(userName), pass, addressPath.Trim(), authHeader);
				}
				break;
			case SourceType.Ftp:
				try
				{
					result = sourceClient.GetFileFromFtp(GlobalClass.NameWithoutDomain(userName), GlobalClass.GetDomainFromUsername(userName), pass, addressPath.Trim());
				}
				catch
				{
					result = sourceClient.GetFileFromFtp(userName, "", pass, addressPath.Trim());
				}
				break;
			case SourceType.WebService:
				try
				{
					result = sourceClient.GetFileFromWebService(GlobalClass.NameWithoutDomain(userName), GlobalClass.GetDomainFromUsername(userName), pass, addressPath.Trim(), soapAction, soapRequest, encoding);
				}
				catch
				{
					result = sourceClient.GetFileFromWebService(userName, "", pass, addressPath.Trim(), soapAction, soapRequest, encoding);
				}
				break;
			case SourceType.AliExpress:
				if (ImportManager.productsLimits > 0)
				{
					content = "https://www.aliexpress.com/item/Reloje-2018-LIGE-Men-Watch-Male-Leather-Automatic-date-Quartz-Watches-Mens-Luxury-Brand-Waterproof-Sport/32858662595.html?spm=a2g01.12279004.rand13tcz4.7.7b2279cdvJuHdw&gps-id=5858046&scm=1007.17919.117252.0&scm_id=1007.17919.117252.0&scm-url=1007.17919.117252.0&pvid=655ed753-4dc2-443a-9882-9fcc869d9ccf\r\n                                    https://www.aliexpress.com/item/7-HD-Car-Backup-Monitor-MP5-Touch-Screen-Digital-Display-Bluetooth-Multimedia-With-USB-Port-Support/32766171698.html?spm=a2g01.11715694.fdpcl001.2.7f8e25c4Sb2InO&gps-id=5547572&scm=1007.19201.111800.0&scm_id=1007.19201.111800.0&scm-url=1007.19201.111800.0&pvid=9ba099b8-0258-480b-8825-c83ddae9c677\r\n                                    https://www.aliexpress.com/item/new-TECHKEY-usb-flash-drive-64GB-32GB-16GB-8GB-4GB-pen-drive-pendrive-waterproof-metal-silver/32795437819.html?spm=a2g01.12109665.layer-g9jhq.4.2f643b3dmD9Dh6&gps-id=5793304&scm=1007.17919.117252.0&scm_id=1007.17919.117252.0&scm-url=1007.17919.117252.0&pvid=f19fa962-99ca-413e-9aef-2d62db3bb6e0\r\n                                    https://www.aliexpress.com/item/2016-New-Brand-ALIKE-Casual-Watch-Men-G-Style-Waterproof-Sports-Military-Watches-Shock-Men-s/32641996554.html?spm=a2g01.12279004.layer-siq74y.1.7b2279cdvJuHdw&gps-id=5862610&scm=1007.19881.118560.0&scm_id=1007.19881.118560.0&scm-url=1007.19881.118560.0&pvid=998061a2-2627-403f-84fc-d27c1dd6a13d\r\n                                    https://www.aliexpress.com/item/ZAKOL-Trendy-Bride-Jewelry-Clear-CZ-Crystal-Leaf-Charm-Cubic-Zirconia-Adjustable-Chain-Bracelets-For-Women/32877531185.html?spm=a2g01.12279004.randbn3vwi.10.7b2279cdvJuHdw&gps-id=5870154&scm=1007.19881.118560.0&scm_id=1007.19881.118560.0&scm-url=1007.19881.118560.0&pvid=7172d06a-4775-4faa-9a82-b0714ad375e7";
				}
				result = encoding.GetBytes(content);
				break;
			case SourceType.BestSecret:
				if (ImportManager.productsLimits > 0)
				{
					content = "https://www.bestsecret.com/category.htm?back_url=%2fcategory.htm%3fgender%3dFEMALE%26category%3dwomen_accessoires&area=WOMEN_ACCESSORIES&gender=FEMALE&sort_by=topseller&category=women_accessoires&filterParam_priceRanges=0-40~2";
				}
				result = encoding.GetBytes(content);
				break;
			}
			return result;
		}

		public VendorData GetVendorData(int vendorId)
		{
			VendorData vendorData = new VendorData();
			string dataConnectionString = dataSettingsManager.LoadSettings(_settingsFilePath).DataConnectionString;
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataConnectionString);
			string commandText = "SELECT TOP 1 Vendor.Id, Vendor.Name, Vendor.Description, SourceMapping.Id AS SourceMappingId, SourceMapping.VendorSourceId as VendorSourceId FROM \r\n                SourceMapping RIGHT OUTER JOIN\r\n                         VendorSource ON SourceMapping.VendorSourceId = VendorSource.Id RIGHT OUTER JOIN\r\n                         Vendor ON VendorSource.VendorId = Vendor.Id where Vendor.Id=@VendorId";
			SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorId", vendorId));
			sqlCeConnection.Open();
			SqlCeDataReader sqlCeDataReader = sqlCeCommand.ExecuteReader();
			if (sqlCeDataReader.Read())
			{
				vendorData.VendorId = vendorId;
				vendorData.VendorName = GlobalClass.ObjectToString(sqlCeDataReader["Name"]);
				vendorData.VendorDescription = GlobalClass.ObjectToString(sqlCeDataReader["Description"]);
				vendorData.VendorSourceId = GlobalClass.StringToInteger(sqlCeDataReader["VendorSourceId"]);
				vendorData.VendorSourceMappingId = GlobalClass.StringToInteger(sqlCeDataReader["SourceMappingId"]);
			}
			sqlCeDataReader.Close();
			if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
			{
				sqlCeConnection.Close();
			}
			sqlCeCommand?.Dispose();
			return vendorData;
		}

		public Encoding GetEncoding(string encoding)
		{
			try
			{
				return string.IsNullOrEmpty(encoding) ? Encoding.UTF8 : Encoding.GetEncoding(Convert.ToInt32(encoding));
			}
			catch
			{
				return Encoding.UTF8;
			}
		}

		public Dictionary<string, string> GetAllSourceFieldsAsync(byte[] fileContent, FileFormat dataFormatType, int rowIndex = 1, string delimiter = null)
		{
			Dictionary<string, string> result = new Dictionary<string, string>();
			if (fileContent != null)
			{
				switch (dataFormatType)
				{
				case FileFormat.XML:
				{
					string @string = Encoding.UTF8.GetString(fileContent);
					result = XmlHelper.GetXmlElements(@string);
					break;
				}
				case FileFormat.CSV:
					result = SourceHelper.GetDataTableColumns(SourceHelper.GetDataAsDataTable(fileContent, FileFormat.CSV, rowIndex, delimiter), columnsInRow: false);
					break;
				case FileFormat.XLSX:
					result = SourceHelper.GetDataTableColumns(SourceHelper.GetDataAsDataTable(fileContent, FileFormat.XLSX, rowIndex), columnsInRow: true);
					break;
				}
			}
			return result;
		}


		public Dictionary<string, string> GetAllSourceFieldsXmlModifyAsync(byte[] fileContent, FileFormat dataFormatType, int rowIndex = 1, string delimiter = null)
		{
			Dictionary<string, string> result = new Dictionary<string, string>();
			if (fileContent != null)
			{
				switch (dataFormatType)
				{
					case FileFormat.XML:
						{
							string @string = Encoding.UTF8.GetString(fileContent);
							result = XmlHelper.GetXmlElementsModiftyTicimax(@string);
							break;
						}
					case FileFormat.CSV:
						result = SourceHelper.GetDataTableColumns(SourceHelper.GetDataAsDataTable(fileContent, FileFormat.CSV, rowIndex, delimiter), columnsInRow: false);
						break;
					case FileFormat.XLSX:
						result = SourceHelper.GetDataTableColumns(SourceHelper.GetDataAsDataTable(fileContent, FileFormat.XLSX, rowIndex), columnsInRow: true);
						break;
				}
			}
			return result;
		}

	}
}
