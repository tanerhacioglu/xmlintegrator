using System;
using System.ComponentModel.DataAnnotations;

namespace NopTalk
{
	public class TaskMapping
	{
		public int? TaskId
		{
			get;
			set;
		}

		public long? EShopId
		{
			get;
			set;
		}

		public int VendorId
		{
			get;
			set;
		}

		[Required(ErrorMessage = "The vendor source field is required.")]
		public int VendorSourceId
		{
			get;
			set;
		}

		public int SourceMappingId
		{
			get;
			set;
		}

		[Required(ErrorMessage = "The task name field is required.")]
		public string TaskName
		{
			get;
			set;
		}

		[Required(ErrorMessage = "The task action field is required.")]
		public int TaskAction
		{
			get;
			set;
		}

		public DateTime? TaskLastRun
		{
			get;
			set;
		}

		public DateTime? TaskNextRun
		{
			get;
			set;
		}

		public long TotalItems
		{
			get;
			set;
		}

		public long ExportedItems
		{
			get;
			set;
		}

		[Required(ErrorMessage = "The task status field is required.")]
		public int TaskStatusId
		{
			get;
			set;
		}

		public EntityType EntityType
		{
			get;
			set;
		}

		public string ProdNameVal
		{
			get;
			set;
		}

		public bool ProdNameAction
		{
			get;
			set;
		}

		public string ProdShortDescVal
		{
			get;
			set;
		}

		public bool ProdShortDescAction
		{
			get;
			set;
		}

		public string ProdFullDescVal
		{
			get;
			set;
		}

		public bool ProdFullDescAction
		{
			get;
			set;
		}

		public string ProdManPartNumVal
		{
			get;
			set;
		}

		public bool ProdManPartNumAction
		{
			get;
			set;
		}

		public string ProdCostVal
		{
			get;
			set;
		}

		public bool ProdCostAction
		{
			get;
			set;
		}

		public string ProdPriceVal
		{
			get;
			set;
		}

		public bool ProdOldPriceAction
		{
			get;
			set;
		}

		public string ProdOldPriceVal
		{
			get;
			set;
		}

		public int? ProdPriceAddPercentVal
		{
			get;
			set;
		}

		public decimal? ProdPriceAddUnitVal
		{
			get;
			set;
		}

		public bool ProdPriceAction
		{
			get;
			set;
		}

		public string ProdStockVal
		{
			get;
			set;
		}

		public bool ProdStockAction
		{
			get;
			set;
		}

		public string SeoMetaKeyVal
		{
			get;
			set;
		}

		public bool SeoMetaKeyAction
		{
			get;
			set;
		}

		public string SeoMetaDescVal
		{
			get;
			set;
		}

		public bool SeoMetaDescAction
		{
			get;
			set;
		}

		public string SeoMetaTitleVal
		{
			get;
			set;
		}

		public bool SeoMetaTitleAction
		{
			get;
			set;
		}

		public string SeoPageVal
		{
			get;
			set;
		}

		public bool SeoPageAction
		{
			get;
			set;
		}

		public string CatVal
		{
			get;
			set;
		}

		public bool CatAction
		{
			get;
			set;
		}

		public string Cat1Val
		{
			get;
			set;
		}

		public bool Cat1Action
		{
			get;
			set;
		}

		public string Cat2Val
		{
			get;
			set;
		}

		public bool Cat2Action
		{
			get;
			set;
		}

		public string Cat3Val
		{
			get;
			set;
		}

		public bool Cat3Action
		{
			get;
			set;
		}

		public string Cat4Val
		{
			get;
			set;
		}

		public bool Cat4Action
		{
			get;
			set;
		}

		public int? CatParent
		{
			get;
			set;
		}

		public int? Cat1Parent
		{
			get;
			set;
		}

		public int? Cat2Parent
		{
			get;
			set;
		}

		public int? Cat3Parent
		{
			get;
			set;
		}

		public int? Cat4Parent
		{
			get;
			set;
		}

		public string ManufactVal
		{
			get;
			set;
		}

		public bool ManufactAction
		{
			get;
			set;
		}

		public string AttNameVal
		{
			get;
			set;
		}

		public bool AttNameAction
		{
			get;
			set;
		}

		public string AttValueVal
		{
			get;
			set;
		}

		public bool AttValueAction
		{
			get;
			set;
		}

		public int? AttStockVal
		{
			get;
			set;
		}

		public bool AttStockAction
		{
			get;
			set;
		}

		public string Att1NameVal
		{
			get;
			set;
		}

		public bool Att1NameAction
		{
			get;
			set;
		}

		public string Att1ValueVal
		{
			get;
			set;
		}

		public bool Att1ValueAction
		{
			get;
			set;
		}

		public int? Att1StockVal
		{
			get;
			set;
		}

		public bool Att1StockAction
		{
			get;
			set;
		}

		public string Att2NameVal
		{
			get;
			set;
		}

		public bool Att2NameAction
		{
			get;
			set;
		}

		public string Att2ValueVal
		{
			get;
			set;
		}

		public bool Att2ValueAction
		{
			get;
			set;
		}

		public int? Att2StockVal
		{
			get;
			set;
		}

		public bool Att2StockAction
		{
			get;
			set;
		}

		public bool ImageAction
		{
			get;
			set;
		}

		public bool Image1Action
		{
			get;
			set;
		}

		public bool Image2Action
		{
			get;
			set;
		}

		public bool Image3Action
		{
			get;
			set;
		}

		public bool Image4Action
		{
			get;
			set;
		}

		public bool ImageSeoAction
		{
			get;
			set;
		}

		public bool ImageSeo1Action
		{
			get;
			set;
		}

		public bool ImageSeo2Action
		{
			get;
			set;
		}

		public bool ImageSeo3Action
		{
			get;
			set;
		}

		public bool ImageSeo4Action
		{
			get;
			set;
		}

		public string ImageSeoVal
		{
			get;
			set;
		}

		public string ImageSeo1Val
		{
			get;
			set;
		}

		public string ImageSeo2Val
		{
			get;
			set;
		}

		public string ImageSeo3Val
		{
			get;
			set;
		}

		public string ImageSeo4Val
		{
			get;
			set;
		}

		public int? FilterAction
		{
			get;
			set;
		}

		public string FilterVal
		{
			get;
			set;
		}

		public int? Filter1Action
		{
			get;
			set;
		}

		public string Filter1Val
		{
			get;
			set;
		}

		public int? Filter2Action
		{
			get;
			set;
		}

		public string Filter2Val
		{
			get;
			set;
		}

		public int? Filter3Action
		{
			get;
			set;
		}

		public string Filter3Val
		{
			get;
			set;
		}

		public int? Filter4Action
		{
			get;
			set;
		}

		public string Filter4Val
		{
			get;
			set;
		}

		public int? Filter5Action
		{
			get;
			set;
		}

		public string Filter5Val
		{
			get;
			set;
		}

		public int? Filter6Action
		{
			get;
			set;
		}

		public string Filter6Val
		{
			get;
			set;
		}

		public int? Filter7Action
		{
			get;
			set;
		}

		public string Filter7Val
		{
			get;
			set;
		}

		public int? Filter8Action
		{
			get;
			set;
		}

		public string Filter8Val
		{
			get;
			set;
		}

		public int? Filter9Action
		{
			get;
			set;
		}

		public string Filter9Val
		{
			get;
			set;
		}

		public bool DisableShop
		{
			get;
			set;
		}

		public string LimitedToStores
		{
			get;
			set;
		}

		public string LimitedToStoresEntities
		{
			get;
			set;
		}

		public int? ScheduleType
		{
			get;
			set;
		}

		public long? ScheduleInterval
		{
			get;
			set;
		}

		public int? ScheduleHour
		{
			get;
			set;
		}

		public int? ScheduleMinute
		{
			get;
			set;
		}

		public bool UnpublishProducts
		{
			get;
			set;
		}

		public bool? ProductPublish
		{
			get;
			set;
		}

		public bool ProductPublishAction
		{
			get;
			set;
		}

		public int? ManageInventoryMethodId
		{
			get;
			set;
		}

		public int? BackorderModeId
		{
			get;
			set;
		}

		public int? LowStockActivityId
		{
			get;
			set;
		}

		public bool ManageInventoryMethodAction
		{
			get;
			set;
		}

		public bool BackorderModeAction
		{
			get;
			set;
		}

		public bool LowStockActivityAction
		{
			get;
			set;
		}

		public int? DeliveryDayId
		{
			get;
			set;
		}

		public int? WarehouseId
		{
			get;
			set;
		}

		public int? ProductTemplateId
		{
			get;
			set;
		}

		public int? AttControlTypeId
		{
			get;
			set;
		}

		public int? Att1ControlTypeId
		{
			get;
			set;
		}

		public int? Att2ControlTypeId
		{
			get;
			set;
		}

		public bool? AttPreselected
		{
			get;
			set;
		}

		public bool? Att1Preselected
		{
			get;
			set;
		}

		public bool? Att2Preselected
		{
			get;
			set;
		}

		public bool DeleteSourceFile
		{
			get;
			set;
		}

		public string VendorVal
		{
			get;
			set;
		}

		public bool VendorAction
		{
			get;
			set;
		}

		public int? RelatedTaskId
		{
			get;
			set;
		}

		public string RelatedSourceAddress
		{
			get;
			set;
		}

		public string TaxCategoryVal
		{
			get;
			set;
		}

		public bool TaxCategoryAction
		{
			get;
			set;
		}

		public bool GtinAction
		{
			get;
			set;
		}

		public string GtinVal
		{
			get;
			set;
		}

		public bool WeightAction
		{
			get;
			set;
		}

		public string WeightVal
		{
			get;
			set;
		}

		public bool SavePicturesInFile
		{
			get;
			set;
		}

		public bool UseTitleCase
		{
			get;
			set;
		}

		public string SavePicturesPath
		{
			get;
			set;
		}

		public string SaveSourceCopyPath
		{
			get;
			set;
		}

		public bool HeightAction
		{
			get;
			set;
		}

		public string HeightVal
		{
			get;
			set;
		}

		public bool WidthAction
		{
			get;
			set;
		}

		public string WidthVal
		{
			get;
			set;
		}

		public bool LengthAction
		{
			get;
			set;
		}

		public string LengthVal
		{
			get;
			set;
		}

		public string CustomerRoleVal
		{
			get;
			set;
		}

		public bool CustomerRoleAction
		{
			get;
			set;
		}

		public string CustomerRole1Val
		{
			get;
			set;
		}

		public bool CustomerRole1Action
		{
			get;
			set;
		}

		public string CustomerRole2Val
		{
			get;
			set;
		}

		public bool CustomerRole2Action
		{
			get;
			set;
		}

		public string CustomerRole3Val
		{
			get;
			set;
		}

		public bool CustomerRole3Action
		{
			get;
			set;
		}

		public string CustomerRole4Val
		{
			get;
			set;
		}

		public bool CustomerRole4Action
		{
			get;
			set;
		}

		public bool DeleteOldCatMapping
		{
			get;
			set;
		}

		public bool DeleteOldPictures
		{
			get;
			set;
		}

		public bool DeleteOldAttributtes
		{
			get;
			set;
		}

		public bool DeleteOldSpecAttributtes
		{
			get;
			set;
		}

		public bool DeleteOldCustomerAddresses
		{
			get;
			set;
		}

		public bool ForceShippingAddressOnUpdate
		{
			get;
			set;
		}

		public bool ForceBillingAddressOnUpdate
		{
			get;
			set;
		}

		public bool DeleteOldCustomerRoles
		{
			get;
			set;
		}

		public bool DeleteExistingTierPricingOnUpdate
		{
			get;
			set;
		}

		public bool AddSpecAttributtesToFullDescAsTable
		{
			get;
			set;
		}

		public bool ProductsInTheLastSubCategory
		{
			get;
			set;
		}

		public bool InsertCategoryPictureOnInsert
		{
			get;
			set;
		}

		public bool InsertCategoryPictureOnUpdate
		{
			get;
			set;
		}

		public bool DisableNestedCategories
		{
			get;
			set;
		}

		public string SpecAttNameVal
		{
			get;
			set;
		}

		public bool SpecAttNameAction
		{
			get;
			set;
		}

		public string SpecAttValueVal
		{
			get;
			set;
		}

		public bool SpecAttValueAction
		{
			get;
			set;
		}

		public int? SpecAttControlTypeId
		{
			get;
			set;
		}

		public bool SpecAttShowInProduct
		{
			get;
			set;
		}

		public bool SpecAttAllowFiltering
		{
			get;
			set;
		}

		public string SpecAtt1NameVal
		{
			get;
			set;
		}

		public bool SpecAtt1NameAction
		{
			get;
			set;
		}

		public string SpecAtt1ValueVal
		{
			get;
			set;
		}

		public bool SpecAtt1ValueAction
		{
			get;
			set;
		}

		public int? SpecAtt1ControlTypeId
		{
			get;
			set;
		}

		public bool SpecAtt1ShowInProduct
		{
			get;
			set;
		}

		public bool SpecAtt1AllowFiltering
		{
			get;
			set;
		}

		public string SpecAtt2NameVal
		{
			get;
			set;
		}

		public bool SpecAtt2NameAction
		{
			get;
			set;
		}

		public string SpecAtt2ValueVal
		{
			get;
			set;
		}

		public bool SpecAtt2ValueAction
		{
			get;
			set;
		}

		public int? SpecAtt2ControlTypeId
		{
			get;
			set;
		}

		public bool SpecAtt2ShowInProduct
		{
			get;
			set;
		}

		public bool SpecAtt2AllowFiltering
		{
			get;
			set;
		}

		public string SpecAtt3NameVal
		{
			get;
			set;
		}

		public bool SpecAtt3NameAction
		{
			get;
			set;
		}

		public string SpecAtt3ValueVal
		{
			get;
			set;
		}

		public bool SpecAtt3ValueAction
		{
			get;
			set;
		}

		public int? SpecAtt3ControlTypeId
		{
			get;
			set;
		}

		public bool SpecAtt3ShowInProduct
		{
			get;
			set;
		}

		public bool SpecAtt3AllowFiltering
		{
			get;
			set;
		}

		public string SpecAtt4NameVal
		{
			get;
			set;
		}

		public bool SpecAtt4NameAction
		{
			get;
			set;
		}

		public string SpecAtt4ValueVal
		{
			get;
			set;
		}

		public bool SpecAtt4ValueAction
		{
			get;
			set;
		}

		public int? SpecAtt4ControlTypeId
		{
			get;
			set;
		}

		public bool SpecAtt4ShowInProduct
		{
			get;
			set;
		}

		public bool SpecAtt4AllowFiltering
		{
			get;
			set;
		}

		public bool? AllowCustomerReviews
		{
			get;
			set;
		}

		public bool? DisplayAvailability
		{
			get;
			set;
		}

		public bool? DisplayStockQuantity
		{
			get;
			set;
		}

		public TaskUserMapping TaskUserMapping
		{
			get;
			set;
		}

		public bool ProdSettingsMinStockQtyAction
		{
			get;
			set;
		}

		public string ProdSettingsMinStockQtyVal
		{
			get;
			set;
		}

		public bool ProdSettingsNotifyQtyAction
		{
			get;
			set;
		}

		public string ProdSettingsNotifyQtyVal
		{
			get;
			set;
		}

		public bool ProdSettingsMinCartQtyAction
		{
			get;
			set;
		}

		public string ProdSettingsMinCartQtyVal
		{
			get;
			set;
		}

		public bool ProdSettingsMaxCartQtyAction
		{
			get;
			set;
		}

		public string ProdSettingsMaxCartQtyVal
		{
			get;
			set;
		}

		public bool ProdSettingsAllowedQtyAction
		{
			get;
			set;
		}

		public string ProdSettingsAllowedQtyVal
		{
			get;
			set;
		}

		public bool ProdSettingsShippingEnabledAction
		{
			get;
			set;
		}

		public bool ProdSettingsShippingEnabledVal
		{
			get;
			set;
		}

		public bool ProdSettingsFreeShippingAction
		{
			get;
			set;
		}

		public bool ProdSettingsFreeShippingVal
		{
			get;
			set;
		}

		public bool ProdSettingsShipSeparatelyAction
		{
			get;
			set;
		}

		public bool ProdSettingsShipSeparatelyVal
		{
			get;
			set;
		}

		public bool ProdSettingsShippingChargeAction
		{
			get;
			set;
		}

		public decimal? ProdSettingsShippingChargeVal
		{
			get;
			set;
		}

		public bool ProdSettingsDeliveryDateAction
		{
			get;
			set;
		}

		public string ProdSettingsDeliveryDateVal
		{
			get;
			set;
		}

		public bool ProdSettingsVisibleIndividuallyAction
		{
			get;
			set;
		}

		public bool ProdSettingsVisibleIndividuallyVal
		{
			get;
			set;
		}

		public bool ProdSettingsIsDeletedAction
		{
			get;
			set;
		}

		public bool ProdSettingsIsDeletedVal
		{
			get;
			set;
		}

		public TaskTierPricingMapping TaskTierPricingMapping
		{
			get;
			set;
		}

		public TaskTranslate TaskTranslate
		{
			get;
			set;
		}

		public bool ResizePicture
		{
			get;
			set;
		}

		public int ResizePictureHeight
		{
			get;
			set;
		}

		public int ResizePictureWidth
		{
			get;
			set;
		}

		public int ResizePictureQuality
		{
			get;
			set;
		}

		public TaskWishListMapping TaskWishListMapping
		{
			get;
			set;
		}

		public TaskProductInfoMapping TaskProductInfoMapping
		{
			get;
			set;
		}

		public TaskProductPriceMapping TaskProductPriceMapping
		{
			get;
			set;
		}

		public TaskProdPicturesMapping TaskProdPicturesMapping
		{
			get;
			set;
		}

		public TaskProductAttributesMapping TaskProductAttributesMapping
		{
			get;
			set;
		}

		public TaskProductSpecAttributesMapping TaskProductSpecAttributesMapping
		{
			get;
			set;
		}

		public TaskAddressCustomAttMapping TaskAddressCustomAttMapping
		{
			get;
			set;
		}

		public TaskCustomerCustomAttMapping TaskCustomerCustomAttMapping
		{
			get;
			set;
		}

		public TaskEmailSettings TaskEmailSettings
		{
			get;
			set;
		}

		public TaskCustomerMapping TaskCustomerMapping
		{
			get;
			set;
		}

		public TaskCustomerOthersMapping TaskCustomerOthersMapping
		{
			get;
			set;
		}

		public TaskCustomerRoleMapping TaskCustomerRoleMapping
		{
			get;
			set;
		}

		public TaskCustomerAddressMapping TaskCustomerShippingAddressMapping
		{
			get;
			set;
		}

		public TaskCustomerAddressMapping TaskCustomerBillingAddressMapping
		{
			get;
			set;
		}

		public int? CustomSqlAction
		{
			get;
			set;
		}

		public string CustomSqlQueries
		{
			get;
			set;
		}

		public bool? MediaSaveInDatabase
		{
			get;
			set;
		}

		public TaskMapping()
		{
			TaskUserMapping = new TaskUserMapping();
			TaskCustomerMapping = new TaskCustomerMapping();
			TaskCustomerRoleMapping = new TaskCustomerRoleMapping();
			TaskCustomerShippingAddressMapping = new TaskCustomerAddressMapping();
			TaskCustomerBillingAddressMapping = new TaskCustomerAddressMapping();
			TaskCustomerOthersMapping = new TaskCustomerOthersMapping();
			TaskProductInfoMapping = new TaskProductInfoMapping();
			TaskProductPriceMapping = new TaskProductPriceMapping();
			TaskProdPicturesMapping = new TaskProdPicturesMapping();
			TaskTierPricingMapping = new TaskTierPricingMapping();
			TaskProductAttributesMapping = new TaskProductAttributesMapping();
			TaskProductSpecAttributesMapping = new TaskProductSpecAttributesMapping();
			TaskTranslate = new TaskTranslate();
			TaskAddressCustomAttMapping = new TaskAddressCustomAttMapping();
			TaskCustomerCustomAttMapping = new TaskCustomerCustomAttMapping();
			TaskEmailSettings = new TaskEmailSettings();
			TaskWishListMapping = new TaskWishListMapping();
			DeleteOldCatMapping = true;
			DeleteOldPictures = true;
			DeleteOldAttributtes = true;
			DeleteOldSpecAttributtes = true;
			UseTitleCase = true;
			ResizePictureHeight = 800;
			ResizePictureWidth = 800;
			ResizePictureQuality = 80;
		}
	}
}
