using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class TaskCustomerCustomAttMapping
	{
		public bool DeleteBeforeImport
		{
			get;
			set;
		}

		public List<TaskItemMapping> Items
		{
			get;
			set;
		}

		public TaskCustomerCustomAttMapping()
		{
			Items = new List<TaskItemMapping>();
		}
	}
}
