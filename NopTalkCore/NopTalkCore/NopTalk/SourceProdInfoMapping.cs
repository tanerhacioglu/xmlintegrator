using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class SourceProdInfoMapping
	{
		public List<SourceItemMapping> Items
		{
			get;
			set;
		}

		public SourceProdInfoMapping()
		{
			Items = new List<SourceItemMapping>();
		}
	}
}
