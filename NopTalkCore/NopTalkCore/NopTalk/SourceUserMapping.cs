namespace NopTalk
{
	public class SourceUserMapping
	{
		public string RepeatNode
		{
			get;
			set;
		}

		public string FirstNameMap
		{
			get;
			set;
		}

		public string FirstNameVal
		{
			get;
			set;
		}

		public string LastNameMap
		{
			get;
			set;
		}

		public string LastNameVal
		{
			get;
			set;
		}

		public string UserEmailMap
		{
			get;
			set;
		}

		public string UserEmailVal
		{
			get;
			set;
		}

		public string AddressEmailMap
		{
			get;
			set;
		}

		public string AddressEmailVal
		{
			get;
			set;
		}

		public string CompanyMap
		{
			get;
			set;
		}

		public string CompanyVal
		{
			get;
			set;
		}

		public string CountryMap
		{
			get;
			set;
		}

		public string CountryVal
		{
			get;
			set;
		}

		public string StateMap
		{
			get;
			set;
		}

		public string StateVal
		{
			get;
			set;
		}

		public string CityMap
		{
			get;
			set;
		}

		public string CityVal
		{
			get;
			set;
		}

		public string Address1Map
		{
			get;
			set;
		}

		public string Address1Val
		{
			get;
			set;
		}

		public string Address2Map
		{
			get;
			set;
		}

		public string Address2Val
		{
			get;
			set;
		}

		public string Address3Map
		{
			get;
			set;
		}

		public string Address3Val
		{
			get;
			set;
		}

		public string ZipPostalCodeMap
		{
			get;
			set;
		}

		public string ZipPostalCodeVal
		{
			get;
			set;
		}

		public string PhoneMap
		{
			get;
			set;
		}

		public string PhoneVal
		{
			get;
			set;
		}

		public string FaxMap
		{
			get;
			set;
		}

		public string FaxVal
		{
			get;
			set;
		}

		public string PasswordMap
		{
			get;
			set;
		}

		public string PasswordVal
		{
			get;
			set;
		}

		public string UsernameMap
		{
			get;
			set;
		}

		public string UsernameVal
		{
			get;
			set;
		}

		public string AddressNameMap
		{
			get;
			set;
		}

		public string AddressNameVal
		{
			get;
			set;
		}

		public string SAPCustomerMap
		{
			get;
			set;
		}

		public string SAPCustomerVal
		{
			get;
			set;
		}

		public string GPCustomerMap
		{
			get;
			set;
		}

		public string GPCustomerVal
		{
			get;
			set;
		}

		public string AddressSAPCustomerMap
		{
			get;
			set;
		}

		public string AddressSAPCustomerVal
		{
			get;
			set;
		}

		public string MainAddressMap
		{
			get;
			set;
		}

		public string MainAddressVal
		{
			get;
			set;
		}
	}
}
