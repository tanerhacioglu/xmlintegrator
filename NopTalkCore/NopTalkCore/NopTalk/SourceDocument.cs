using Excel;
using Microsoft.VisualBasic.FileIO;
using NopTalkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;

namespace NopTalk
{
	public class SourceDocument
	{
		public List<SourceItem> SourceItems = new List<SourceItem>();

		public bool LoadByOne = false;

		public int Total = 0;

		public string[] SourceLines;

		public SourceMapping SourceMapping;

		public TaskMapping TaskMapping;

		public SourceData SourceData;

		public BestSecretClient bestSecretClient;

		public int CurrentItem
		{
			get;
			set;
		}

		public int VendorId
		{
			get;
			set;
		}

		public int VendorSourceId
		{
			get;
			set;
		}

		public int TaskId
		{
			get;
			set;
		}

		public SourceDocument()
		{
			SourceItems = new List<SourceItem>();
		}

		public SourceDocument(FileFormat fileFormat, byte[] fileContent, SourceMapping sourceMapping, TaskMapping taskMapping, SourceData sourceData, bool loadByOne = false)
		{
			LoadByOne = loadByOne;
			SourceMapping = sourceMapping;
			TaskMapping = taskMapping;
			SourceData = sourceData;
			VendorSourceId = (sourceMapping.VendorSourceId ?? sourceData.VendorSourceId ?? taskMapping.VendorSourceId);
			if (fileContent == null)
			{
				throw new Exception("Source file is empty.");
			}
			if (taskMapping != null)
			{
				sourceMapping = UpdateSourceMappingByTaskMapping(sourceMapping, taskMapping);
			}
			if (fileFormat == FileFormat.XML)
			{
				XElement xElement = null;
				bool flag = fileContent[0] == 239 && fileContent[1] == 187 && fileContent[2] == 191;
				int num = flag ? 3 : 0;
				string @string = sourceData.GetEncoding.GetString(fileContent, flag ? num : 0, fileContent.Length - num);
				try
				{
					xElement = XElement.Parse(@string);
				}
				catch
				{
					try
					{
						xElement = XElement.Parse(GlobalClass.RemoveInvalidXmlChars(@string));
					}
					catch (Exception ex)
					{
						throw new Exception("Wrong XML format. Error: " + ex.ToString());
					}
				}
				XElement xElement2 = stripNS(xElement);
				IEnumerable<XElement> enumerable = (sourceMapping.EntityType == EntityType.WishList) ? xElement2.XPathSelectElements(sourceMapping.SourceWishListMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1).FieldMappingForXml) : ((sourceMapping.EntityType == EntityType.Users) ? xElement2.XPathSelectElements(sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1).FieldMappingForXml) : xElement2.XPathSelectElements(sourceMapping.ProdInfoRepeatNode));
				int num2 = 0;
				List<SourceItem> list = new List<SourceItem>();
				foreach (XElement item in enumerable)
				{
					if (num2 >= sourceMapping.FirstRowIndex.GetValueOrDefault())
					{
						list.Add(new SourceItem(item, sourceMapping, sourceData, taskMapping));
					}
					num2++;
				}
				if (enumerable.Count() == 0)
				{
					list.Add(new SourceItem(xElement2, sourceMapping, sourceData, taskMapping));
				}
				SourceItems = list;
			}
			if (fileFormat == FileFormat.XLSX)
			{
				MemoryStream fileStream = new MemoryStream(fileContent);
				IExcelDataReader excelDataReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
				excelDataReader.IsFirstRowAsColumnNames = false;
				DataSet dataSet = excelDataReader.AsDataSet();
				DataTable dataTable = dataSet.Tables[0];
				int num3 = 0;
				List<SourceItem> list2 = new List<SourceItem>();
				foreach (DataRow row in dataTable.Rows)
				{
					if (num3 >= sourceMapping.FirstRowIndex.GetValueOrDefault())
					{
						bool flag2 = false;
						foreach (DataColumn column in dataTable.Columns)
						{
							if (!string.IsNullOrEmpty(row[column].ToString()))
							{
								flag2 = true;
								break;
							}
						}
						if (flag2)
						{
							list2.Add(new SourceItem(row, sourceMapping, sourceData, taskMapping));
						}
					}
					num3++;
				}
				SourceItems = list2;
			}
			if (fileFormat == FileFormat.CSV)
			{
				if (!sourceMapping.CsvDelimeter.HasValue)
				{
					throw new Exception("Not set CSV file delimeter.");
				}
				DataTable dataTable2 = ConvertCSVtoDataTable(fileContent, sourceMapping.CsvDelimeter.Value, sourceData?.GetEncoding ?? Encoding.UTF8);
				int num4 = 0;
				List<SourceItem> list3 = new List<SourceItem>();
				foreach (DataRow row2 in dataTable2.Rows)
				{
					if (num4 >= sourceMapping.FirstRowIndex.GetValueOrDefault())
					{
						list3.Add(new SourceItem(row2, sourceMapping, sourceData, taskMapping));
					}
					num4++;
				}
				SourceItems = list3;
			}
			if (fileFormat == FileFormat.AliExpress)
			{
				string string2 = (sourceData?.GetEncoding ?? Encoding.UTF8).GetString(fileContent);
				string[] array = string2.Split(new string[1]
				{
					Environment.NewLine
				}, StringSplitOptions.None);
				List<SourceItem> list4 = new List<SourceItem>();
				Total = array.Count();
				SourceLines = array;
				if (!LoadByOne)
				{
					string[] array2 = array;
					foreach (string text in array2)
					{
						if (!string.IsNullOrEmpty(text))
						{
							list4.Add(new SourceItem(fileFormat, text, sourceMapping, sourceData, taskMapping));
						}
					}
				}
				SourceItems = list4;
			}
			if (fileFormat == FileFormat.BestSecret)
			{
				string string3 = (sourceData?.GetEncoding ?? Encoding.UTF8).GetString(fileContent);
				string[] array3 = string3.Split(new string[1]
				{
					Environment.NewLine
				}, StringSplitOptions.None);
				List<SourceItem> list5 = new List<SourceItem>();
				Total = array3.Count();
				SourceLines = array3;
				if (!LoadByOne)
				{
					bestSecretClient = new BestSecretClient(sourceData.SourceAddress, sourceData.SourceUser, sourceData.SourcePassword);
					string[] array4 = array3;
					foreach (string text2 in array4)
					{
						if (!string.IsNullOrEmpty(text2))
						{
							List<string> result = bestSecretClient.GetProductsListUrls(text2).Result;
							foreach (string item2 in result)
							{
								List<string> result2 = bestSecretClient.GetProductsUrls("https://www.bestsecret.com" + item2).Result;
								foreach (string item3 in result2)
								{
									if (!string.IsNullOrEmpty(item3))
									{
										list5.Add(new SourceItem(fileFormat, item3, sourceMapping, sourceData, taskMapping, bestSecretClient));
									}
								}
							}
						}
					}
				}
				else
				{
					List<string> list6 = new List<string>();
					bestSecretClient = new BestSecretClient(sourceData.SourceAddress, sourceData.SourceUser, sourceData.SourcePassword);
					string[] array5 = array3;
					foreach (string text3 in array5)
					{
						if (!string.IsNullOrEmpty(text3))
						{
							List<string> result3 = bestSecretClient.GetProductsListUrls(text3).Result;
							if (result3?.Any() ?? false)
							{
								foreach (string item4 in result3)
								{
									List<string> result4 = bestSecretClient.GetProductsUrls("https://www.bestsecret.com" + item4).Result;
									list6.AddRange(result4.Distinct());
								}
							}
						}
					}
					SourceLines = list6.Distinct().ToArray();
					Total = list6.Count();
				}
				SourceItems = list5;
			}
			if (SourceItems.Count > 0)
			{
				Total = SourceItems.Count;
			}
		}

		public SourceItem GetItemByIndex(FileFormat fileFormat, int index)
		{
			if (index <= SourceItems.Count - 1)
			{
				return SourceItems[index];
			}
			SourceItem sourceItem = new SourceItem(fileFormat, SourceLines[index], SourceMapping, SourceData, TaskMapping, bestSecretClient);
			SourceItems.Add(sourceItem);
			return sourceItem;
		}

		public void SaveItemByIndex(int index, SourceItem newSourceItem)
		{
			if (index <= SourceItems.Count - 1)
			{
				SourceItems[index] = newSourceItem;
			}
		}

		private XElement stripNS(XElement root)
		{
			XElement xElement = new XElement(root.Name.LocalName, root.HasElements ? ((IEnumerable)(from el in root.Elements()
				select stripNS(el))) : ((IEnumerable)root.Value));
			xElement.ReplaceAttributes(from attr in root.Attributes()
				where !attr.IsNamespaceDeclaration
				select attr);
			return xElement;
		}

		private static DataTable ConvertCSVtoDataTable(byte[] fileContent, char delimeter, Encoding encoding, bool hasFieldsEnclosedInQuotes = true)
		{
			DataTable dataTable = new DataTable();
			Stream stream = new MemoryStream(fileContent);
			using (TextFieldParser textFieldParser = new TextFieldParser(stream, encoding))
			{
				textFieldParser.TrimWhiteSpace = true;
				textFieldParser.TextFieldType = FieldType.Delimited;
				textFieldParser.SetDelimiters(delimeter.ToString());
				textFieldParser.HasFieldsEnclosedInQuotes = hasFieldsEnclosedInQuotes;
				string[] array = textFieldParser.ReadFields();
				string[] array2 = array;
				foreach (string columnName in array2)
				{
					DataColumn dataColumn = new DataColumn(columnName);
					dataColumn.AllowDBNull = true;
					dataTable.Columns.Add(dataColumn);
				}
				DataRowCollection rows = dataTable.Rows;
				object[] values = array;
				rows.Add(values);
				while (!textFieldParser.EndOfData)
				{
					string[] array3 = null;
					try
					{
						array3 = textFieldParser.ReadFields();
					}
					catch (MalformedLineException)
					{
						if (!textFieldParser.ErrorLine.StartsWith("\""))
						{
							throw;
						}
						string text = textFieldParser.ErrorLine.Substring(1, textFieldParser.ErrorLine.Length - 2);
						array3 = text.Split(new string[1]
						{
							$"\"{delimeter}\""
						}, StringSplitOptions.None);
					}
					DataRowCollection rows2 = dataTable.Rows;
					values = array3;
					rows2.Add(values);
				}
			}
			stream.Dispose();
			return dataTable;
		}

		public DataTable ConvertCSVtoDataTable2(byte[] fileContent, char delimeter)
		{
			DataTable dataTable = new DataTable();
			using (StreamReader streamReader = new StreamReader(new MemoryStream(fileContent)))
			{
				string[] array = streamReader.ReadLine().Split(delimeter);
				string[] array2 = array;
				foreach (string columnName in array2)
				{
					dataTable.Columns.Add(columnName);
				}
				while (!streamReader.EndOfStream)
				{
					string[] array3 = streamReader.ReadLine().Split(',');
					DataRow dataRow = dataTable.NewRow();
					for (int j = 0; j < array.Length; j++)
					{
						dataRow[j] = array3[j];
					}
					dataTable.Rows.Add(dataRow);
				}
			}
			return dataTable;
		}

		public SourceMapping UpdateSourceMappingByTaskMapping(SourceMapping sourceMapping, TaskMapping taskMapping)
		{
			if (sourceMapping.EntityType == EntityType.Products)
			{
				sourceMapping.ProdNameMap = (taskMapping.ProdNameAction ? sourceMapping.ProdNameMap : null);
				sourceMapping.ProdNameVal = (taskMapping.ProdNameAction ? taskMapping.ProdNameVal : null);
				sourceMapping.ProdShortDescMap = (taskMapping.ProdShortDescAction ? sourceMapping.ProdShortDescMap : null);
				sourceMapping.ProdShortDescVal = (taskMapping.ProdShortDescAction ? taskMapping.ProdShortDescVal : null);
				sourceMapping.ProdFullDescMap = (taskMapping.ProdFullDescAction ? sourceMapping.ProdFullDescMap : null);
				sourceMapping.ProdFullDescVal = (taskMapping.ProdFullDescAction ? taskMapping.ProdFullDescVal : null);
				sourceMapping.ProdManPartNumMap = (taskMapping.ProdManPartNumAction ? sourceMapping.ProdManPartNumMap : null);
				sourceMapping.ProdManPartNumVal = (taskMapping.ProdManPartNumAction ? taskMapping.ProdManPartNumVal : null);
				sourceMapping.ProdCostMap = (taskMapping.ProdCostAction ? sourceMapping.ProdCostMap : null);
				sourceMapping.ProdCostVal = (taskMapping.ProdCostAction ? taskMapping.ProdCostVal.ToString() : null);
				sourceMapping.ProdPriceMap = (taskMapping.ProdPriceAction ? sourceMapping.ProdPriceMap : null);
				sourceMapping.ProdPriceVal = (taskMapping.ProdPriceAction ? taskMapping.ProdPriceVal.ToString() : null);
				sourceMapping.ProdOldPriceMap = (taskMapping.ProdOldPriceAction ? sourceMapping.ProdOldPriceMap : null);
				sourceMapping.ProdOldPriceVal = (taskMapping.ProdOldPriceAction ? taskMapping.ProdOldPriceVal.ToString() : null);
				sourceMapping.ProdStockMap = (taskMapping.ProdStockAction ? sourceMapping.ProdStockMap : null);
				sourceMapping.ProdStockVal = (taskMapping.ProdStockAction ? taskMapping.ProdStockVal.ToString() : null);
				sourceMapping.SeoMetaKeyMap = (taskMapping.SeoMetaKeyAction ? sourceMapping.SeoMetaKeyMap : null);
				sourceMapping.SeoMetaKeyVal = (taskMapping.SeoMetaKeyAction ? taskMapping.SeoMetaKeyVal : null);
				sourceMapping.SeoMetaDescMap = (taskMapping.SeoMetaDescAction ? sourceMapping.SeoMetaDescMap : null);
				sourceMapping.SeoMetaDescVal = (taskMapping.SeoMetaDescAction ? taskMapping.SeoMetaDescVal : null);
				sourceMapping.SeoMetaTitleMap = (taskMapping.SeoMetaTitleAction ? sourceMapping.SeoMetaTitleMap : null);
				sourceMapping.SeoMetaTitleVal = (taskMapping.SeoMetaTitleAction ? taskMapping.SeoMetaTitleVal : null);
				sourceMapping.SeoPageMap = (taskMapping.SeoPageAction ? sourceMapping.SeoPageMap : null);
				sourceMapping.SeoPageVal = (taskMapping.SeoPageAction ? taskMapping.SeoPageVal : null);
				sourceMapping.ManufactMap = (taskMapping.ManufactAction ? sourceMapping.ManufactMap : null);
				sourceMapping.ManufactVal = (taskMapping.ManufactAction ? taskMapping.ManufactVal : null);
				sourceMapping.VendorMap = (taskMapping.VendorAction ? sourceMapping.VendorMap : null);
				sourceMapping.VendorVal = (taskMapping.VendorAction ? taskMapping.VendorVal : null);
				sourceMapping.CatMap = ((taskMapping.CatAction || taskMapping.Cat1Parent == 1 || taskMapping.Cat2Parent == 1 || taskMapping.Cat3Parent == 1 || taskMapping.Cat4Parent == 1) ? sourceMapping.CatMap : null);
				sourceMapping.CatVal = ((taskMapping.CatAction || taskMapping.Cat1Parent == 1 || taskMapping.Cat2Parent == 1 || taskMapping.Cat3Parent == 1 || taskMapping.Cat4Parent == 1) ? taskMapping.CatVal : null);
				sourceMapping.Cat1Map = ((taskMapping.Cat1Action || taskMapping.Cat2Parent == 2 || taskMapping.Cat3Parent == 2 || taskMapping.Cat4Parent == 2) ? sourceMapping.Cat1Map : null);
				sourceMapping.Cat1Val = ((taskMapping.Cat1Action || taskMapping.Cat2Parent == 2 || taskMapping.Cat3Parent == 2 || taskMapping.Cat4Parent == 2) ? taskMapping.Cat1Val : null);
				sourceMapping.Cat1Parent = (taskMapping.Cat1Parent ?? 1) - 1;
				sourceMapping.Cat2Map = ((taskMapping.Cat2Action || taskMapping.Cat3Parent == 3 || taskMapping.Cat4Parent == 3) ? sourceMapping.Cat2Map : null);
				sourceMapping.Cat2Val = ((taskMapping.Cat2Action || taskMapping.Cat3Parent == 3 || taskMapping.Cat4Parent == 3) ? taskMapping.Cat2Val : null);
				sourceMapping.Cat2Parent = (taskMapping.Cat2Parent ?? 1) - 1;
				sourceMapping.Cat3Map = ((taskMapping.Cat3Action || taskMapping.Cat4Parent == 4) ? sourceMapping.Cat3Map : null);
				sourceMapping.Cat3Val = ((taskMapping.Cat3Action || taskMapping.Cat4Parent == 4) ? taskMapping.Cat3Val : null);
				sourceMapping.Cat3Parent = (taskMapping.Cat3Parent ?? 1) - 1;
				sourceMapping.Cat4Map = (taskMapping.Cat4Action ? sourceMapping.Cat4Map : null);
				sourceMapping.Cat4Val = (taskMapping.Cat4Action ? taskMapping.Cat4Val : null);
				sourceMapping.Cat4Parent = (taskMapping.Cat4Parent ?? 1) - 1;
				sourceMapping.AttNameMap = (taskMapping.AttNameAction ? sourceMapping.AttNameMap : null);
				sourceMapping.AttNameVal = (taskMapping.AttNameAction ? taskMapping.AttNameVal : null);
				sourceMapping.Att1NameMap = (taskMapping.Att1NameAction ? sourceMapping.Att1NameMap : null);
				sourceMapping.Att1NameVal = (taskMapping.Att1NameAction ? taskMapping.Att1NameVal : null);
				sourceMapping.Att2NameMap = (taskMapping.Att2NameAction ? sourceMapping.Att2NameMap : null);
				sourceMapping.Att2NameVal = (taskMapping.Att2NameAction ? taskMapping.Att2NameVal : null);
				sourceMapping.AttValueMap = (taskMapping.AttValueAction ? sourceMapping.AttValueMap : null);
				sourceMapping.AttValueVal = (taskMapping.AttValueAction ? taskMapping.AttValueVal : null);
				sourceMapping.Att1ValueMap = (taskMapping.Att1ValueAction ? sourceMapping.Att1ValueMap : null);
				sourceMapping.Att1ValueVal = (taskMapping.Att1ValueAction ? taskMapping.Att1ValueVal : null);
				sourceMapping.Att2ValueMap = (taskMapping.Att2ValueAction ? sourceMapping.Att2ValueMap : null);
				sourceMapping.Att2ValueVal = (taskMapping.Att2ValueAction ? taskMapping.Att2ValueVal : null);
				sourceMapping.AttStockMap = (taskMapping.AttStockAction ? sourceMapping.AttStockMap : null);
				sourceMapping.AttStockVal = (taskMapping.AttStockAction ? taskMapping.AttStockVal.ToString() : "0");
				sourceMapping.Att1StockMap = (taskMapping.Att1StockAction ? sourceMapping.Att1StockMap : null);
				sourceMapping.Att1StockVal = (taskMapping.Att1StockAction ? taskMapping.Att1StockVal.ToString() : "0");
				sourceMapping.Att2StockMap = (taskMapping.Att2StockAction ? sourceMapping.Att2StockMap : null);
				sourceMapping.Att2StockVal = (taskMapping.Att2StockAction ? taskMapping.Att2StockVal.ToString() : "0");
				sourceMapping.ImageMap = (taskMapping.ImageAction ? sourceMapping.ImageMap : null);
				sourceMapping.Image1Map = (taskMapping.Image1Action ? sourceMapping.Image1Map : null);
				sourceMapping.Image2Map = (taskMapping.Image2Action ? sourceMapping.Image2Map : null);
				sourceMapping.Image3Map = (taskMapping.Image3Action ? sourceMapping.Image3Map : null);
				sourceMapping.Image4Map = (taskMapping.Image4Action ? sourceMapping.Image4Map : null);
				sourceMapping.ImageVal = (taskMapping.ImageAction ? sourceMapping.ImageVal : null);
				sourceMapping.Image1Val = (taskMapping.Image1Action ? sourceMapping.Image1Val : null);
				sourceMapping.Image2Val = (taskMapping.Image2Action ? sourceMapping.Image2Val : null);
				sourceMapping.Image3Val = (taskMapping.Image3Action ? sourceMapping.Image3Val : null);
				sourceMapping.Image4Val = (taskMapping.Image4Action ? sourceMapping.Image4Val : null);
				sourceMapping.ImageSeoMap = (taskMapping.ImageSeoAction ? sourceMapping.ImageSeoMap : null);
				sourceMapping.ImageSeo1Map = (taskMapping.ImageSeo1Action ? sourceMapping.ImageSeo1Map : null);
				sourceMapping.ImageSeo2Map = (taskMapping.ImageSeo2Action ? sourceMapping.ImageSeo2Map : null);
				sourceMapping.ImageSeo3Map = (taskMapping.ImageSeo3Action ? sourceMapping.ImageSeo3Map : null);
				sourceMapping.ImageSeo4Map = (taskMapping.ImageSeo4Action ? sourceMapping.ImageSeo4Map : null);
				sourceMapping.ImageSeoVal = (taskMapping.ImageSeoAction ? taskMapping.ImageSeoVal : null);
				sourceMapping.ImageSeo1Val = (taskMapping.ImageSeo1Action ? taskMapping.ImageSeo1Val : null);
				sourceMapping.ImageSeo2Val = (taskMapping.ImageSeo2Action ? taskMapping.ImageSeo2Val : null);
				sourceMapping.ImageSeo3Val = (taskMapping.ImageSeo3Action ? taskMapping.ImageSeo3Val : null);
				sourceMapping.ImageSeo4Val = (taskMapping.ImageSeo4Action ? taskMapping.ImageSeo4Val : null);
				sourceMapping.RelatedTaskId = taskMapping.RelatedTaskId;
				sourceMapping.RelatedSourceAddress = taskMapping.RelatedSourceAddress;
				sourceMapping.TaxCategoryMap = (taskMapping.TaxCategoryAction ? sourceMapping.TaxCategoryMap : null);
				sourceMapping.TaxCategoryVal = (taskMapping.TaxCategoryAction ? taskMapping.TaxCategoryVal : null);
				sourceMapping.GtinMap = (taskMapping.GtinAction ? sourceMapping.GtinMap : null);
				sourceMapping.GtinVal = (taskMapping.GtinAction ? taskMapping.GtinVal : null);
				sourceMapping.WeightMap = (taskMapping.WeightAction ? sourceMapping.WeightMap : null);
				sourceMapping.WeightVal = (taskMapping.WeightAction ? taskMapping.WeightVal.ToString() : null);
				sourceMapping.LengthMap = (taskMapping.LengthAction ? sourceMapping.LengthMap : null);
				sourceMapping.LengthVal = (taskMapping.LengthAction ? taskMapping.LengthVal.ToString() : null);
				sourceMapping.WidthMap = (taskMapping.WidthAction ? sourceMapping.WidthMap : null);
				sourceMapping.WidthVal = (taskMapping.WidthAction ? taskMapping.WidthVal.ToString() : null);
				sourceMapping.HeightMap = (taskMapping.HeightAction ? sourceMapping.HeightMap : null);
				sourceMapping.HeightVal = (taskMapping.HeightAction ? taskMapping.HeightVal.ToString() : null);
				sourceMapping.SpecAttNameMap = (taskMapping.SpecAttNameAction ? sourceMapping.SpecAttNameMap : null);
				sourceMapping.SpecAttNameVal = (taskMapping.SpecAttNameAction ? taskMapping.SpecAttNameVal : null);
				sourceMapping.SpecAtt1NameMap = (taskMapping.SpecAtt1NameAction ? sourceMapping.SpecAtt1NameMap : null);
				sourceMapping.SpecAtt1NameVal = (taskMapping.SpecAtt1NameAction ? taskMapping.SpecAtt1NameVal : null);
				sourceMapping.SpecAtt2NameMap = (taskMapping.SpecAtt2NameAction ? sourceMapping.SpecAtt2NameMap : null);
				sourceMapping.SpecAtt2NameVal = (taskMapping.SpecAtt2NameAction ? taskMapping.SpecAtt2NameVal : null);
				sourceMapping.SpecAtt3NameMap = (taskMapping.SpecAtt3NameAction ? sourceMapping.SpecAtt3NameMap : null);
				sourceMapping.SpecAtt3NameVal = (taskMapping.SpecAtt3NameAction ? taskMapping.SpecAtt3NameVal : null);
				sourceMapping.SpecAtt4NameMap = (taskMapping.SpecAtt4NameAction ? sourceMapping.SpecAtt4NameMap : null);
				sourceMapping.SpecAtt4NameVal = (taskMapping.SpecAtt4NameAction ? taskMapping.SpecAtt4NameVal : null);
				sourceMapping.SpecAttValueMap = (taskMapping.SpecAttValueAction ? sourceMapping.SpecAttValueMap : null);
				sourceMapping.SpecAttValueVal = (taskMapping.SpecAttValueAction ? taskMapping.SpecAttValueVal : null);
				sourceMapping.SpecAtt1ValueMap = (taskMapping.SpecAtt1ValueAction ? sourceMapping.SpecAtt1ValueMap : null);
				sourceMapping.SpecAtt1ValueVal = (taskMapping.SpecAtt1ValueAction ? taskMapping.SpecAtt1ValueVal : null);
				sourceMapping.SpecAtt2ValueMap = (taskMapping.SpecAtt2ValueAction ? sourceMapping.SpecAtt2ValueMap : null);
				sourceMapping.SpecAtt2ValueVal = (taskMapping.SpecAtt2ValueAction ? taskMapping.SpecAtt2ValueVal : null);
				sourceMapping.SpecAtt3ValueMap = (taskMapping.SpecAtt3ValueAction ? sourceMapping.SpecAtt3ValueMap : null);
				sourceMapping.SpecAtt3ValueVal = (taskMapping.SpecAtt3ValueAction ? taskMapping.SpecAtt3ValueVal : null);
				sourceMapping.SpecAtt4ValueMap = (taskMapping.SpecAtt4ValueAction ? sourceMapping.SpecAtt4ValueMap : null);
				sourceMapping.SpecAtt4ValueVal = (taskMapping.SpecAtt4ValueAction ? taskMapping.SpecAtt4ValueVal : null);
				sourceMapping.ProdSettingsAllowedQtyMap = (taskMapping.ProdSettingsAllowedQtyAction ? sourceMapping.ProdSettingsAllowedQtyMap : null);
				sourceMapping.ProdSettingsAllowedQtyVal = ((!taskMapping.ProdSettingsAllowedQtyAction) ? null : taskMapping.ProdSettingsAllowedQtyVal?.ToString());
				sourceMapping.ProdSettingsMinCartQtyMap = (taskMapping.ProdSettingsMinCartQtyAction ? sourceMapping.ProdSettingsMinCartQtyMap : null);
				sourceMapping.ProdSettingsMinCartQtyVal = ((!taskMapping.ProdSettingsMinCartQtyAction) ? null : taskMapping.ProdSettingsMinCartQtyVal?.ToString());
				sourceMapping.ProdSettingsMaxCartQtyMap = (taskMapping.ProdSettingsMaxCartQtyAction ? sourceMapping.ProdSettingsMaxCartQtyMap : null);
				sourceMapping.ProdSettingsMaxCartQtyVal = ((!taskMapping.ProdSettingsMaxCartQtyAction) ? null : taskMapping.ProdSettingsMaxCartQtyVal?.ToString());
				sourceMapping.ProdSettingsMinStockQtyMap = (taskMapping.ProdSettingsMinStockQtyAction ? sourceMapping.ProdSettingsMinStockQtyMap : null);
				sourceMapping.ProdSettingsMinStockQtyVal = ((!taskMapping.ProdSettingsMinStockQtyAction) ? null : taskMapping.ProdSettingsMinStockQtyVal?.ToString());
				sourceMapping.ProdSettingsNotifyQtyMap = (taskMapping.ProdSettingsNotifyQtyAction ? sourceMapping.ProdSettingsNotifyQtyMap : null);
				sourceMapping.ProdSettingsNotifyQtyVal = ((!taskMapping.ProdSettingsNotifyQtyAction) ? null : taskMapping.ProdSettingsNotifyQtyVal?.ToString());
				sourceMapping.ProdSettingsShippingEnabledMap = (taskMapping.ProdSettingsShippingEnabledAction ? sourceMapping.ProdSettingsShippingEnabledMap : null);
				sourceMapping.ProdSettingsShippingEnabledVal = (taskMapping.ProdSettingsShippingEnabledAction ? taskMapping.ProdSettingsShippingEnabledVal.ToString() : null);
				sourceMapping.ProdSettingsFreeShippingMap = (taskMapping.ProdSettingsFreeShippingAction ? sourceMapping.ProdSettingsFreeShippingMap : null);
				sourceMapping.ProdSettingsFreeShippingVal = (taskMapping.ProdSettingsFreeShippingAction ? taskMapping.ProdSettingsFreeShippingVal.ToString() : null);
				sourceMapping.ProdSettingsShipSeparatelyMap = (taskMapping.ProdSettingsShipSeparatelyAction ? sourceMapping.ProdSettingsShipSeparatelyMap : null);
				sourceMapping.ProdSettingsShipSeparatelyVal = (taskMapping.ProdSettingsShipSeparatelyAction ? taskMapping.ProdSettingsShipSeparatelyVal.ToString() : null);
				sourceMapping.ProdSettingsShippingChargeMap = (taskMapping.ProdSettingsShippingChargeAction ? sourceMapping.ProdSettingsShippingChargeMap : null);
				sourceMapping.ProdSettingsShippingChargeVal = ((!taskMapping.ProdSettingsShippingChargeAction) ? null : taskMapping.ProdSettingsShippingChargeVal?.ToString());
				sourceMapping.ProdSettingsDeliveryDateMap = (taskMapping.ProdSettingsDeliveryDateAction ? sourceMapping.ProdSettingsDeliveryDateMap : null);
				sourceMapping.ProdSettingsDeliveryDateVal = ((!taskMapping.ProdSettingsDeliveryDateAction) ? null : taskMapping.ProdSettingsDeliveryDateVal?.ToString());
				sourceMapping.ProdSettingsVisibleIndividuallyMap = (taskMapping.ProdSettingsVisibleIndividuallyAction ? sourceMapping.ProdSettingsVisibleIndividuallyMap : null);
				sourceMapping.ProdSettingsVisibleIndividuallyVal = (taskMapping.ProdSettingsVisibleIndividuallyAction ? taskMapping.ProdSettingsVisibleIndividuallyVal.ToString() : null);
				sourceMapping.ProdSettingsIsDeletedMap = (taskMapping.ProdSettingsIsDeletedAction ? sourceMapping.ProdSettingsIsDeletedMap : null);
				sourceMapping.ProdSettingsIsDeletedVal = (taskMapping.ProdSettingsIsDeletedAction ? taskMapping.ProdSettingsIsDeletedVal.ToString() : null);
				if (taskMapping.TaskTierPricingMapping?.TaskTierPricingMappingItems != null)
				{
					foreach (TaskItemMapping tierPricingItem in taskMapping.TaskTierPricingMapping.TaskTierPricingMappingItems.Where((TaskItemMapping x) => x.FieldImport))
					{
						sourceMapping.SourceTierPricingMapping?.SourceTierPricingMappingItems?.Where((SourceItemMapping x) => x.Id == tierPricingItem.Id && x.MappingItemType == tierPricingItem.MappingItemType).ToList().ForEach(delegate(SourceItemMapping x)
						{
							x.MappedToTask = true;
						});
						sourceMapping.SourceTierPricingMapping?.SourceTierPricingMappingItems?.Where((SourceItemMapping x) => x.Id == tierPricingItem.Id && x.MappingItemType == tierPricingItem.MappingItemType).ToList().ForEach(delegate(SourceItemMapping x)
						{
							x.FieldRule = tierPricingItem.FieldRule;
						});
					}
				}
			}
			if (sourceMapping.EntityType == EntityType.Products || sourceMapping.EntityType == EntityType.Users)
			{
				sourceMapping.CustomerRoleMap = (taskMapping.CustomerRoleAction ? sourceMapping.CustomerRoleMap : null);
				sourceMapping.CustomerRoleVal = (taskMapping.CustomerRoleAction ? taskMapping.CustomerRoleVal : null);
				sourceMapping.CustomerRole1Map = (taskMapping.CustomerRole1Action ? sourceMapping.CustomerRole1Map : null);
				sourceMapping.CustomerRole1Val = (taskMapping.CustomerRole1Action ? taskMapping.CustomerRole1Val : null);
				sourceMapping.CustomerRole2Map = (taskMapping.CustomerRole2Action ? sourceMapping.CustomerRole2Map : null);
				sourceMapping.CustomerRole2Val = (taskMapping.CustomerRole2Action ? taskMapping.CustomerRole2Val : null);
				sourceMapping.CustomerRole3Map = (taskMapping.CustomerRole3Action ? sourceMapping.CustomerRole3Map : null);
				sourceMapping.CustomerRole3Val = (taskMapping.CustomerRole3Action ? taskMapping.CustomerRole3Val : null);
				sourceMapping.CustomerRole4Map = (taskMapping.CustomerRole4Action ? sourceMapping.CustomerRole4Map : null);
				sourceMapping.CustomerRole4Val = (taskMapping.CustomerRole4Action ? taskMapping.CustomerRole4Val : null);
			}
			if (sourceMapping.EntityType == EntityType.Users)
			{
			}
			return sourceMapping;
		}

		public void UpdateTotal()
		{
			Total = SourceItems.Count();
		}
	}
}
