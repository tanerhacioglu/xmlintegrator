using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace NopTalk
{
	public static class Extensions
	{
		public static string Replace(this string source, string oldString, string newString, StringComparison comp)
		{
			int num = source.IndexOf(oldString, comp);
			if (num >= 0)
			{
				source = source.Remove(num, oldString.Length);
				source = source.Insert(num, newString);
			}
			return source;
		}

		public static object ReplaceToParam<T>(this T obj)
		{
			if (obj == null)
			{
				return DBNull.Value;
			}
			if (string.IsNullOrEmpty(obj?.ToString()))
			{
				return string.Empty;
			}
			return obj;
		}

		public static string SqlQueryToMySql<T>(this T obj)
		{
			if (string.IsNullOrEmpty(obj?.ToString()))
			{
				return string.Empty;
			}
			string text = obj.ToString();
			if (text.ToLower().IndexOf("top 1") > 0)
			{
				text += " LIMIT 1";
			}
			return text.Replace("TOP 1", "").Replace("[Key]", "`Key`").Replace("; SELECT SCOPE_IDENTITY() AS insertId;", "; select last_insert_id();")
				.Replace(";SELECT SCOPE_IDENTITY() AS insertId;", "; select last_insert_id();")
				.Replace("WITH (NOLOCK)", "")
				.Replace("(NOLOCK)", "")
				.Replace("[", "")
				.Replace("]", "")
				.Replace("dbo.", "");
		}

		public static string NormalizeText<T>(this T obj)
		{
			if (string.IsNullOrEmpty(obj?.ToString()))
			{
				return string.Empty;
			}
			string text = obj.ToString();
			return string.Join("", from c in text.Normalize(NormalizationForm.FormD)
				where char.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark
				select c);
		}

		public static object ReplaceToParam2<T>(this T obj)
		{
			if (string.IsNullOrEmpty(obj?.ToString()))
			{
				return DBNull.Value;
			}
			return obj;
		}

		public static IEnumerable<IEnumerable<T>> Batch<T>(this IEnumerable<T> items, int maxItems)
		{
			return from x in items.Select((T item, int inx) => new
				{
					item,
					inx
				})
				group x by x.inx / maxItems into g
				select from x in g
					select x.item;
		}
	}
}
