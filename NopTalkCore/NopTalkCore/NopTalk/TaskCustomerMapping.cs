using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class TaskCustomerMapping
	{
		public bool DeleteIfNotFound
		{
			get;
			set;
		}

		public bool DeactivateIfNotFound
		{
			get;
			set;
		}

		public List<TaskItemMapping> Items
		{
			get;
			set;
		}

		public TaskCustomerMapping()
		{
			Items = new List<TaskItemMapping>();
		}
	}
}
