using NopTalkCore;
using System.Collections.Generic;

namespace NopTalk
{
	public class SourceCustomerCustomAttMapping
	{
		public List<SourceItemMapping> Items
		{
			get;
			set;
		}

		public SourceCustomerCustomAttMapping()
		{
			Items = new List<SourceItemMapping>();
		}
	}
}
