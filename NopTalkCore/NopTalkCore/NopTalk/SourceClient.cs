using NopTalkCore;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace NopTalk
{
	public class SourceClient
	{
		public byte[] GetFileFromFileSystem(string user, string domain, string password, string filePath, Encoding encoding, bool xls)
		{
			byte[] array = null;
			try
			{
				if (!xls)
				{
					string s = File.ReadAllText(filePath, encoding);
					return encoding.GetBytes(s);
				}
				return File.ReadAllBytes(filePath);
			}
			catch (Exception ex)
			{
				if (!string.IsNullOrEmpty(user) && !string.IsNullOrEmpty(password))
				{
					using (new Impersonator(GlobalClass.NameWithoutDomain(user), GlobalClass.GetDomainFromUsername(user), password))
					{
						string s2 = File.ReadAllText(filePath, encoding);
						return encoding.GetBytes(s2);
					}
				}
				LogManager.LogWrite(null, "File could not open. Error=" + ex.ToString(), LogType.Warning);
				throw new Exception("File could not open. Error=" + ex.Message);
			}
		}

		public byte[] GetFileFromWeb(string user, string domain, string password, string fileUrl, string authHeader)
		{
			WebClientWithTimeout webClientWithTimeout = new WebClientWithTimeout();
			try
			{
				if (!string.IsNullOrEmpty(user))
				{
					webClientWithTimeout.Credentials = new NetworkCredential(user, password, domain);
				}
				else
				{
					webClientWithTimeout.UseDefaultCredentials = true;
				}
				if (!string.IsNullOrEmpty(authHeader))
				{
					webClientWithTimeout.Headers.Add("Authorization", authHeader);
				}
				return webClientWithTimeout.DownloadData(fileUrl);
			}
			catch
			{
				webClientWithTimeout.UseDefaultCredentials = true;
				return webClientWithTimeout.DownloadData(fileUrl);
			}
		}

		public byte[] GetFileFromFtp(string user, string domain, string password, string fileUrl)
		{
			WebClientWithTimeout webClientWithTimeout = new WebClientWithTimeout();
			if (string.IsNullOrEmpty(user))
			{
				webClientWithTimeout.UseDefaultCredentials = true;
			}
			else
			{
				webClientWithTimeout.Credentials = new NetworkCredential(user, password, domain);
			}
			return webClientWithTimeout.DownloadData(fileUrl);
		}

		public static void UploadFileToFtp(string user, string password, string fileUrl, byte[] fileBytes)
		{
			using (WebClient webClient = new WebClient())
			{
				if (string.IsNullOrEmpty(user))
				{
					webClient.UseDefaultCredentials = true;
				}
				else
				{
					webClient.Credentials = new NetworkCredential(user, password);
				}
				webClient.UploadData(fileUrl, "STOR", fileBytes);
			}
		}

		public static bool DeleteFileOnFtpServer(string fileUrl, string ftpUsername, string ftpPassword)
		{
			try
			{
				Uri uri = new Uri(fileUrl);
				if (uri.Scheme != Uri.UriSchemeFtp)
				{
					return false;
				}
				FtpWebRequest ftpWebRequest = (FtpWebRequest)WebRequest.Create(uri);
				ftpWebRequest.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
				ftpWebRequest.Method = "DELE";
				FtpWebResponse ftpWebResponse = (FtpWebResponse)ftpWebRequest.GetResponse();
				ftpWebResponse.Close();
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public byte[] GetFileFromWebService(string user, string domain, string password, string serviceUrl, string soapAction, string soapRequest, Encoding encoding)
		{
			HttpWebRequest httpWebRequest = null;
			HttpWebResponse httpWebResponse = null;
			httpWebRequest = (HttpWebRequest)WebRequest.CreateDefault(new Uri(serviceUrl));
			httpWebRequest.Method = "POST";
			httpWebRequest.ContentType = "text/xml; charset=utf-8";
			httpWebRequest.Accept = "text/xml";
			if (!string.IsNullOrEmpty(soapAction))
			{
				httpWebRequest.Headers["SOAPAction"] = soapAction;
			}
			if (!string.IsNullOrEmpty(user))
			{
				httpWebRequest.Credentials = new NetworkCredential(user, password, domain);
			}
			else
			{
				httpWebRequest.UseDefaultCredentials = true;
			}
			httpWebRequest.ContentLength = soapRequest.Length + encoding.GetPreamble().Length;
			StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream(), encoding);
			streamWriter.Write(soapRequest);
			streamWriter.Close();
			httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
			Stream responseStream = httpWebResponse.GetResponseStream();
			StreamReader streamReader = new StreamReader(responseStream);
			byte[] bytes = encoding.GetBytes(streamReader.ReadToEnd());
			streamReader.Close();
			streamReader.Dispose();
			responseStream.Close();
			responseStream.Dispose();
			return bytes;
		}
	}
}
