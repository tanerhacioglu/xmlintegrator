using NopTalkCore;

namespace NopTalk
{
	public class StoreData
	{
		public int StoreId
		{
			get;
			set;
		}

		public string StoreName
		{
			get;
			set;
		}

		public string StoreDescription
		{
			get;
			set;
		}

		public string DatabaseConnString
		{
			get;
			set;
		}

		public NopCommerceVer Version
		{
			get;
			set;
		}

		public DatabaseType DatabaseType
		{
			get;
			set;
		}

		public EditorConfigData EditorConfigData
		{
			get;
			set;
		}
	}
}
