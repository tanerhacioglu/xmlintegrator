using Newtonsoft.Json;
using NopTalkCore;
using System.Data;
using System.Data.SqlServerCe;

namespace NopTalk
{
	public class StoreFunc
	{
		public static StoreData GetStoreData(long storeId)
		{
			StoreData storeData = new StoreData();
			if (storeId > 0)
			{
				DataSettingsManager dataSettingsManager = new DataSettingsManager();
				SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
				string commandText = "select * from EShop where Id=@StoreId";
				SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@StoreId", storeId));
				sqlCeConnection.Open();
				SqlCeDataReader sqlCeDataReader = sqlCeCommand.ExecuteReader();
				if (sqlCeDataReader.Read())
				{
					storeData.StoreId = GlobalClass.StringToInteger(sqlCeDataReader["Id"]);
					storeData.StoreName = GlobalClass.ObjectToString(sqlCeDataReader["Name"]);
					storeData.StoreDescription = GlobalClass.ObjectToString(sqlCeDataReader["Description"]);
					storeData.DatabaseConnString = GlobalClass.ObjectToString(sqlCeDataReader["DatabaseConnString"]);
					storeData.Version = ((GlobalClass.ObjectToString(sqlCeDataReader["Version"]) == "4.3") ? NopCommerceVer.Ver43 : ((GlobalClass.ObjectToString(sqlCeDataReader["Version"]) == "4.2") ? NopCommerceVer.Ver42 : ((GlobalClass.ObjectToString(sqlCeDataReader["Version"]) == "4.1") ? NopCommerceVer.Ver41 : ((GlobalClass.ObjectToString(sqlCeDataReader["Version"]) == "4.0") ? NopCommerceVer.Ver40 : ((GlobalClass.ObjectToString(sqlCeDataReader["Version"]) == "3.9") ? NopCommerceVer.Ver39 : ((GlobalClass.ObjectToString(sqlCeDataReader["Version"]) == "3.8") ? NopCommerceVer.Ver38 : ((GlobalClass.ObjectToString(sqlCeDataReader["Version"]) == "3.7") ? NopCommerceVer.Ver37 : ((GlobalClass.ObjectToString(sqlCeDataReader["Version"]) == "3.6") ? NopCommerceVer.Ver36 : NopCommerceVer.Ver35))))))));
					storeData.EditorConfigData = JsonConvert.DeserializeObject<EditorConfigData>(GlobalClass.ObjectToString(sqlCeDataReader["EditorConfigData"]));
					storeData.DatabaseType = ((GlobalClass.ObjectToString(sqlCeDataReader["DatabaseType"]) == "MySQL") ? DatabaseType.MySQL : DatabaseType.MSSQL);
				}
				if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
				sqlCeCommand?.Dispose();
			}
			else
			{
				storeData.DatabaseConnString = "Data Source=localhost;Initial Catalog=VaskSoftDb35;Integrated Security=True;Persist Security Info=False";
			}
			return storeData;
		}
	}
}
