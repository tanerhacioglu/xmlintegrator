﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;

namespace NopTalk.Helpers
{
	public static class DataTableHelper
	{
		public static List<T> ConvertToList<T>(this DataTable dt)
		{
			List<string> columnNames = (from DataColumn c in dt.Columns
										select c.ColumnName).ToList();
			PropertyInfo[] properties = typeof(T).GetProperties();
			return dt.AsEnumerable().Select(delegate (DataRow row)
			{
				T val = Activator.CreateInstance<T>();
				PropertyInfo[] array = properties;
				foreach (PropertyInfo propertyInfo in array)
				{
					if (columnNames.Contains(propertyInfo.Name))
					{
						PropertyInfo property = val.GetType().GetProperty(propertyInfo.Name);
						propertyInfo.SetValue(val, (row[propertyInfo.Name] == DBNull.Value) ? null : Convert.ChangeType(row[propertyInfo.Name], property.PropertyType));
					}
				}
				return val;
			}).ToList();
		}

		public static DataTable ConvertToDataTable<T>(this IList<T> data)
		{
			PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
			DataTable dataTable = new DataTable();
			foreach (PropertyDescriptor item in properties)
			{
				dataTable.Columns.Add(item.Name, Nullable.GetUnderlyingType(item.PropertyType) ?? item.PropertyType);
			}
			foreach (T datum in data)
			{
				DataRow dataRow = dataTable.NewRow();
				foreach (PropertyDescriptor item2 in properties)
				{
					dataRow[item2.Name] = (item2.GetValue(datum) ?? DBNull.Value);
				}
				dataTable.Rows.Add(dataRow);
			}
			dataTable.AcceptChanges();
			return dataTable;
		}

		public static DataRow ConvertToDataRow<T>(this T item, DataTable table)
		{
			PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
			DataRow dataRow = table.NewRow();
			foreach (PropertyDescriptor item2 in properties)
			{
				dataRow[item2.Name] = (item2.GetValue(item) ?? DBNull.Value);
			}
			return dataRow;
		}

		public static T ConvertToEntity<T>(this DataRow tableRow) where T : new()
		{
			Type typeFromHandle = typeof(T);
			T val = new T();
			foreach (DataColumn column in tableRow.Table.Columns)
			{
				string columnName = column.ColumnName;
				PropertyInfo property = typeFromHandle.GetProperty(columnName.ToLower(), BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
				if (property != null)
				{
					object val2 = tableRow[columnName];
					if (Nullable.GetUnderlyingType(property.PropertyType) != null)
					{
						val2 = ((!(val2 is DBNull)) ? Convert.ChangeType(val2, Nullable.GetUnderlyingType(property.PropertyType)) : null);
					}
					else
					{
						SetDefaultValue(ref val2, property.PropertyType);
						val2 = ((!property.PropertyType.IsEnum || property.PropertyType.IsGenericType) ? Convert.ChangeType(val2, property.PropertyType) : Enum.ToObject(property.PropertyType, val2));
					}
					if (property.CanWrite)
					{
						property.SetValue(val, val2, null);
					}
				}
			}
			return val;
		}

		private static void SetDefaultValue(ref object val, Type propertyType)
		{
			if (val is DBNull)
			{
				val = GetDefault(propertyType);
			}
		}

		public static object GetDefault(Type type)
		{
			if (type.IsValueType)
			{
				return Activator.CreateInstance(type);
			}
			return null;
		}
	}
}