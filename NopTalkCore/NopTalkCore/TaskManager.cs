using NopTalk;
using Schedule;
using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.Linq;
using System.Threading.Tasks;

namespace NopTalkCore
{
	public class TaskManager
	{
		private delegate string dOutput(StoreData storeData, int taskId, bool scheduler, int taskStatusId, TaskMapping taskMapping = null);

		public static ScheduleTimer timer = null;

		private SourceMappingFunc sourceMappingFunc = new SourceMappingFunc();

		private TaskMappingFunc taskMappingFunc = new TaskMappingFunc();

		private ImportManager importManager = new ImportManager();

		private DataSettingsManager dataSettingsManager = new DataSettingsManager();

		private int tasksLimits = 0;

		private static Dictionary<int, int> tasksList = new Dictionary<int, int>();

		public int TasksListCount => (tasksList != null) ? tasksList.Count() : 0;

		public static bool IsScheduled => timer != null;

		public void StartAllTasks(StoreData storeData, bool runNow, int taskIdCurrent = 0, TaskMapping taskMappingForm = null)
		{
			if (tasksList == null)
			{
				tasksList = new Dictionary<int, int>();
			}
			if (taskIdCurrent > 0)
			{
				Task task = Task.Run(delegate
				{
					RunTask(storeData, taskIdCurrent, scheduler: false, 1, taskMappingForm);
				});
				return;
			}
			TaskMapping taskMapping = new TaskMapping();
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			string commandText = "select * from Task";
			if (tasksLimits > 0 && ImportManager.productsLimits > 0)
			{
				LogManager.LogWrite(num, "NopTalk Pro version has tasks running limit=" + tasksLimits, LogType.Warning);
				commandText = "select TOP " + tasksLimits + " * from Task";
			}
			DateTime now = DateTime.Now;
			SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
			sqlCeConnection.Open();
			SqlCeDataReader sqlCeDataReader = sqlCeCommand.ExecuteReader();
			long num4 = 0L;
			List<Task> list = new List<Task>();
			bool flag = true;
			if (!runNow)
			{
				StopTasks();
				timer = new ScheduleTimer();
			}
			while (sqlCeDataReader.Read())
			{
				num4++;
				num = GlobalClass.StringToInteger(sqlCeDataReader["Id"]);
				num2 = GlobalClass.StringToInteger(sqlCeDataReader["TaskStatusId"]);
				num3 = GlobalClass.StringToInteger(sqlCeDataReader["VendorSourceId"]);
				now = DateTime.Now;
				if (runNow)
				{
					list.Add(Task.Factory.StartNew(delegate(object obj)
					{
						this.RunTask(storeData, ((dynamic)obj).taskId, false, 1, ((dynamic)obj).taskMappingForm);
					}, new
					{
						taskId = num,
						taskMappingForm = taskMappingForm
					}));
					continue;
				}
				taskMapping = taskMappingFunc.GetTaskMapping(num);
				if (taskMapping.ScheduleType == 0)
				{
					if (taskMapping.ScheduleInterval == 0)
					{
						LogManager.LogWrite(num, "Need set scheduler interval for task Id=" + num, LogType.Warning);
						taskMappingFunc.UpdateTaskNextRun(num, null);
						continue;
					}
					IScheduledItem schedule = new SimpleInterval(DateTime.Now.AddMinutes(taskMapping.ScheduleInterval.GetValueOrDefault()), TimeSpan.FromMinutes(taskMapping.ScheduleInterval.GetValueOrDefault()));
					now = now.AddMinutes(taskMapping.ScheduleInterval.GetValueOrDefault());
					if (num2 != 2)
					{
						taskMappingFunc.UpdateTaskNextRun(num, null);
						continue;
					}
					taskMappingFunc.UpdateTaskNextRun(num, now);
					Delegate f = new dOutput(RunTask);
					timer.AddAsyncJob(schedule, f, storeData, num, true, num2, taskMapping);
				}
				else if (taskMapping.ScheduleType == 1)
				{
					if (taskMapping.ScheduleInterval == 0)
					{
						LogManager.LogWrite(num, "Need set scheduler interval for task Id=" + num, LogType.Warning);
						taskMappingFunc.UpdateTaskNextRun(num, null);
						continue;
					}
					IScheduledItem schedule2 = new SimpleInterval(DateTime.Now.AddHours(taskMapping.ScheduleInterval.GetValueOrDefault()), TimeSpan.FromHours(taskMapping.ScheduleInterval.GetValueOrDefault()));
					now = now.AddHours(taskMapping.ScheduleInterval.GetValueOrDefault());
					if (num2 != 2)
					{
						taskMappingFunc.UpdateTaskNextRun(num, null);
						continue;
					}
					taskMappingFunc.UpdateTaskNextRun(num, now);
					Delegate f2 = new dOutput(RunTask);
					timer.AddAsyncJob(schedule2, f2, storeData, num, true, num2, taskMapping);
				}
				else if (taskMapping.ScheduleType == 2)
				{
					string strOffset = $"{taskMapping.ScheduleHour}:{taskMapping.ScheduleMinute}:00";
					IScheduledItem scheduledItem = new ScheduledTime("Daily", strOffset);
					now = scheduledItem.NextRunTime(now, IncludeStartTime: true);
					if (num2 != 2)
					{
						taskMappingFunc.UpdateTaskNextRun(num, null);
						continue;
					}
					taskMappingFunc.UpdateTaskNextRun(num, now);
					Delegate f3 = new dOutput(RunTask);
					timer.AddAsyncJob(scheduledItem, f3, storeData, num, true, num2, taskMapping);
				}
				else
				{
					LogManager.LogWrite(num, "Need set scheduler for task Id=" + num, LogType.Warning);
					taskMappingFunc.UpdateTaskNextRun(num, null);
				}
			}
			if (list.Count > 0)
			{
				Task.WhenAll(list.ToArray());
			}
			if (timer != null)
			{
				timer.Start();
			}
			sqlCeConnection.Close();
		}

		private string RunTask(StoreData storeData, int taskId, bool scheduler, int taskStatusId, TaskMapping taskMapping = null)
		{
			if (tasksList == null)
			{
				tasksList = new Dictionary<int, int>();
			}
			string resultMessage = "";
			long insertedItems = 0L;
			long updatedItems = 0L;
			DateTime value = DateTime.Now;
			if (scheduler && taskMapping != null)
			{
				if (taskMapping.ScheduleType == 0)
				{
					value = value.AddMinutes(taskMapping.ScheduleInterval.GetValueOrDefault());
				}
				if (taskMapping.ScheduleType == 1)
				{
					value = value.AddHours(taskMapping.ScheduleInterval.GetValueOrDefault());
				}
				if (taskMapping.ScheduleType == 2)
				{
					value = value.AddDays(1.0);
				}
				if (taskStatusId != 2)
				{
					taskMappingFunc.UpdateTaskNextRun(taskId, null);
				}
				else
				{
					taskMappingFunc.UpdateTaskNextRun(taskId, value);
					if (tasksList != null && !tasksList.ContainsKey(taskId))
					{
						tasksList.Add(taskId, taskId);
					}
					taskMappingFunc.UpdateTaskRunning(taskId, taskRunning: true);
					if (taskMapping == null)
					{
						taskMapping = taskMappingFunc.GetTaskMapping(taskId);
					}
					if (taskMapping.EntityType == EntityType.Products)
					{
						importManager.StartTaskImport(null, null, storeData, taskId, scheduler, out resultMessage, out insertedItems, out updatedItems, taskMapping);
					}
					if (taskMapping.EntityType == EntityType.Users)
					{
						importManager.StartTaskUserImport(null, storeData, taskId, scheduler, out resultMessage, out insertedItems, out updatedItems, taskMapping);
					}
					if (taskMapping.EntityType == EntityType.WishList)
					{
						importManager.StartTaskWishListImport(null, storeData, taskId, scheduler, out resultMessage, out insertedItems, out updatedItems, taskMapping);
					}
					taskMappingFunc.UpdateTaskRunning(taskId, taskRunning: false);
					if (tasksList != null && tasksList.ContainsKey(taskId))
					{
						tasksList.Remove(taskId);
					}
				}
			}
			else
			{
				if (tasksList != null && !tasksList.ContainsKey(taskId))
				{
					tasksList.Add(taskId, taskId);
				}
				taskMappingFunc.UpdateTaskRunning(taskId, taskRunning: true);
				if (taskMapping == null)
				{
					taskMapping = taskMappingFunc.GetTaskMapping(taskId);
				}
				if (taskMapping.EntityType == EntityType.Products)
				{
					importManager.StartTaskImport(null, null, storeData, taskId, scheduler, out resultMessage, out insertedItems, out updatedItems, taskMapping);
				}
				if (taskMapping.EntityType == EntityType.Users)
				{
					importManager.StartTaskUserImport(null, storeData, taskId, scheduler, out resultMessage, out insertedItems, out updatedItems, taskMapping);
				}
				if (taskMapping.EntityType == EntityType.WishList)
				{
					importManager.StartTaskWishListImport(null, storeData, taskId, scheduler, out resultMessage, out insertedItems, out updatedItems, taskMapping);
				}
				taskMappingFunc.UpdateTaskRunning(taskId, taskRunning: false);
				if (tasksList != null && tasksList.ContainsKey(taskId))
				{
					tasksList.Remove(taskId);
				}
			}
			return resultMessage;
		}

		public void StopTasks(int? taskId = null)
		{
			if (!taskId.HasValue && timer != null)
			{
				timer.Stop();
				timer.Dispose();
				timer = null;
			}
			if (taskId.HasValue && taskId > 0)
			{
				taskMappingFunc.UpdateTaskRunning(taskId.Value, taskRunning: false);
				LogManager.LogWrite(taskId, "Task canceled.", LogType.Info);
				if (tasksList != null && tasksList.ContainsKey(taskId.Value))
				{
					tasksList.Remove(taskId.Value);
				}
			}
			if (!taskId.HasValue && tasksList != null && tasksList.Count > 0)
			{
				foreach (KeyValuePair<int, int> tasks in tasksList)
				{
					taskMappingFunc.UpdateTaskRunning(tasks.Key, taskRunning: false);
					LogManager.LogWrite(tasks.Key, "Task canceled.", LogType.Info);
				}
				tasksList = null;
			}
		}
	}
}
