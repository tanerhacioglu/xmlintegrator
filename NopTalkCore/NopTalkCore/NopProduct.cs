using System;

namespace NopTalkCore
{
	public class NopProduct
	{
		public int ProductId
		{
			get;
			set;
		}

		public int ProductTypeId
		{
			get;
			set;
		}

		public int ParentGroupedProductId
		{
			get;
			set;
		}

		public byte VisibleIndividually
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public object ShortDescription
		{
			get;
			set;
		}

		public object FullDescription
		{
			get;
			set;
		}

		public object AdminComment
		{
			get;
			set;
		}

		public int ProductTemplateId
		{
			get;
			set;
		}

		public int VendorId
		{
			get;
			set;
		}

		public byte ShowOnHomePage
		{
			get;
			set;
		}

		public object MetaKeywords
		{
			get;
			set;
		}

		public object MetaDescription
		{
			get;
			set;
		}

		public object MetaTitle
		{
			get;
			set;
		}

		public byte AllowCustomerReviews
		{
			get;
			set;
		}

		public int ApprovedRatingSum
		{
			get;
			set;
		}

		public int NotApprovedRatingSum
		{
			get;
			set;
		}

		public int ApprovedTotalReviews
		{
			get;
			set;
		}

		public int NotApprovedTotalReviews
		{
			get;
			set;
		}

		public bool? SubjectToAcl
		{
			get;
			set;
		}

		public bool LimitedToStores
		{
			get;
			set;
		}

		public object Sku
		{
			get;
			set;
		}

		public object ManufacturerPartNumber
		{
			get;
			set;
		}

		public object Gtin
		{
			get;
			set;
		}

		public byte IsGiftCard
		{
			get;
			set;
		}

		public int GiftCardTypeId
		{
			get;
			set;
		}

		public byte RequireOtherProducts
		{
			get;
			set;
		}

		public object RequiredProductIds
		{
			get;
			set;
		}

		public byte AutomaticallyAddRequiredProducts
		{
			get;
			set;
		}

		public byte IsDownload
		{
			get;
			set;
		}

		public int DownloadId
		{
			get;
			set;
		}

		public byte UnlimitedDownloads
		{
			get;
			set;
		}

		public int MaxNumberOfDownloads
		{
			get;
			set;
		}

		public int DownloadExpirationDays
		{
			get;
			set;
		}

		public int DownloadActivationTypeId
		{
			get;
			set;
		}

		public byte HasSampleDownload
		{
			get;
			set;
		}

		public int SampleDownloadId
		{
			get;
			set;
		}

		public byte HasUserAgreement
		{
			get;
			set;
		}

		public object UserAgreementText
		{
			get;
			set;
		}

		public byte IsRecurring
		{
			get;
			set;
		}

		public int RecurringCycleLength
		{
			get;
			set;
		}

		public int RecurringCyclePeriodId
		{
			get;
			set;
		}

		public int RecurringTotalCycles
		{
			get;
			set;
		}

		public byte IsRental
		{
			get;
			set;
		}

		public int RentalPriceLength
		{
			get;
			set;
		}

		public int RentalPricePeriodId
		{
			get;
			set;
		}

		public byte IsShipEnabled
		{
			get;
			set;
		}

		public byte IsFreeShipping
		{
			get;
			set;
		}

		public byte ShipSeparately
		{
			get;
			set;
		}

		public decimal AdditionalShippingCharge
		{
			get;
			set;
		}

		public int DeliveryDateId
		{
			get;
			set;
		}

		public byte IsTaxExempt
		{
			get;
			set;
		}

		public int TaxCategoryId
		{
			get;
			set;
		}

		public byte IsTelecommunicationsOrBroadcastingOrElectronicServices
		{
			get;
			set;
		}

		public int ManageInventoryMethodId
		{
			get;
			set;
		}

		public byte UseMultipleWarehouses
		{
			get;
			set;
		}

		public int WarehouseId
		{
			get;
			set;
		}

		public int StockQuantity
		{
			get;
			set;
		}

		public byte DisplayStockAvailability
		{
			get;
			set;
		}

		public byte DisplayStockQuantity
		{
			get;
			set;
		}

		public int MinStockQuantity
		{
			get;
			set;
		}

		public int LowStockActivityId
		{
			get;
			set;
		}

		public int NotifyAdminForQuantityBelow
		{
			get;
			set;
		}

		public int BackorderModeId
		{
			get;
			set;
		}

		public byte AllowBackInStockSubscriptions
		{
			get;
			set;
		}

		public int OrderMinimumQuantity
		{
			get;
			set;
		}

		public int OrderMaximumQuantity
		{
			get;
			set;
		}

		public object AllowedQuantities
		{
			get;
			set;
		}

		public byte AllowAddingOnlyExistingAttributeCombinations
		{
			get;
			set;
		}

		public byte DisableBuyButton
		{
			get;
			set;
		}

		public byte DisableWishlistButton
		{
			get;
			set;
		}

		public byte AvailableForPreOrder
		{
			get;
			set;
		}

		public object PreOrderAvailabilityStartDateTimeUtc
		{
			get;
			set;
		}

		public byte CallForPrice
		{
			get;
			set;
		}

		public decimal Price
		{
			get;
			set;
		}

		public decimal OldPrice
		{
			get;
			set;
		}

		public decimal ProductCost
		{
			get;
			set;
		}

		public object SpecialPrice
		{
			get;
			set;
		}

		public object SpecialPriceStartDateTimeUtc
		{
			get;
			set;
		}

		public object SpecialPriceEndDateTimeUtc
		{
			get;
			set;
		}

		public byte CustomerEntersPrice
		{
			get;
			set;
		}

		public decimal MinimumCustomerEnteredPrice
		{
			get;
			set;
		}

		public decimal MaximumCustomerEnteredPrice
		{
			get;
			set;
		}

		public byte HasTierPrices
		{
			get;
			set;
		}

		public byte HasDiscountsApplied
		{
			get;
			set;
		}

		public decimal Weight
		{
			get;
			set;
		}

		public decimal Length
		{
			get;
			set;
		}

		public decimal Width
		{
			get;
			set;
		}

		public decimal Height
		{
			get;
			set;
		}

		public object AvailableStartDateTimeUtc
		{
			get;
			set;
		}

		public object AvailableEndDateTimeUtc
		{
			get;
			set;
		}

		public int DisplayOrder
		{
			get;
			set;
		}

		public byte Published
		{
			get;
			set;
		}

		public byte Deleted
		{
			get;
			set;
		}

		public object CreatedOnUtc
		{
			get;
			set;
		}

		public object UpdatedOnUtc
		{
			get;
			set;
		}

		public byte BasepriceEnabled
		{
			get;
			set;
		}

		public decimal BasepriceAmount
		{
			get;
			set;
		}

		public int BasepriceUnitId
		{
			get;
			set;
		}

		public decimal BasepriceBaseAmount
		{
			get;
			set;
		}

		public int BasepriceBaseUnitId
		{
			get;
			set;
		}

		public byte NotReturnable
		{
			get;
			set;
		}

		public bool LeaveProductType
		{
			get;
			set;
		}

		public NopProduct()
		{
			ProductTypeId = 5;
			ParentGroupedProductId = 0;
			VisibleIndividually = 1;
			ProductTemplateId = 1;
			AllowCustomerReviews = 1;
			ApprovedRatingSum = 0;
			NotApprovedRatingSum = 0;
			SubjectToAcl = false;
			LimitedToStores = false;
			IsGiftCard = 0;
			GiftCardTypeId = 0;
			RequireOtherProducts = 0;
			AutomaticallyAddRequiredProducts = 0;
			IsDownload = 0;
			DownloadId = 0;
			UnlimitedDownloads = 1;
			MaxNumberOfDownloads = 10;
			DownloadActivationTypeId = 1;
			HasSampleDownload = 0;
			SampleDownloadId = 0;
			HasUserAgreement = 0;
			IsRecurring = 0;
			RecurringCycleLength = 100;
			RecurringCyclePeriodId = 0;
			RecurringTotalCycles = 10;
			IsRental = 0;
			RentalPriceLength = 1;
			RentalPricePeriodId = 0;
			IsShipEnabled = 1;
			IsFreeShipping = 0;
			ShipSeparately = 0;
			AdditionalShippingCharge = 0m;
			DeliveryDateId = 0;
			IsTaxExempt = 0;
			TaxCategoryId = 0;
			IsTelecommunicationsOrBroadcastingOrElectronicServices = 0;
			ManageInventoryMethodId = 0;
			UseMultipleWarehouses = 0;
			WarehouseId = 0;
			StockQuantity = 0;
			DisplayStockAvailability = 0;
			DisplayStockQuantity = 0;
			MinStockQuantity = 0;
			LowStockActivityId = 1;
			NotifyAdminForQuantityBelow = 1;
			BackorderModeId = 0;
			AllowBackInStockSubscriptions = 0;
			OrderMinimumQuantity = 1;
			OrderMaximumQuantity = 10;
			AllowAddingOnlyExistingAttributeCombinations = 0;
			DisableBuyButton = 0;
			DisableWishlistButton = 0;
			AvailableForPreOrder = 0;
			CallForPrice = 0;
			CustomerEntersPrice = 0;
			MinimumCustomerEnteredPrice = 0m;
			MaximumCustomerEnteredPrice = 0m;
			HasTierPrices = 0;
			HasDiscountsApplied = 0;
			Weight = 0m;
			Length = 0m;
			Width = 0m;
			Height = 0m;
			DisplayOrder = 0;
			Published = 0;
			Deleted = 0;
			CreatedOnUtc = DateTime.Now;
			UpdatedOnUtc = DateTime.Now;
			SpecialPrice = DBNull.Value;
			ShowOnHomePage = 0;
			Published = 0;
			BasepriceEnabled = 0;
			BasepriceUnitId = 0;
			BasepriceBaseAmount = 0m;
			BasepriceBaseUnitId = 0;
			NotReturnable = 0;
		}
	}
}
