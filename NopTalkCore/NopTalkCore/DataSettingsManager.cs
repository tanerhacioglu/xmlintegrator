using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Web.Hosting;

namespace NopTalkCore
{
	public class DataSettingsManager
	{
		protected const char separator = ':';

		protected const string filename = "Settings.txt";

		private bool IsWeb = false;

		protected virtual string MapPath()
		{
			string text = (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["dirPath"])) ? ConfigurationManager.AppSettings["dirPath"] : HostingEnvironment.ApplicationPhysicalPath;
			string path = string.IsNullOrEmpty(text) ? Process.GetCurrentProcess().MainModule.FileName : text;
			return Path.GetDirectoryName(path);
		}

		protected virtual DataSettings ParseSettings(string text)
		{
			DataSettings dataSettings = new DataSettings();
			if (string.IsNullOrEmpty(text))
			{
				return dataSettings;
			}
			List<string> list = new List<string>();
			using (StringReader stringReader = new StringReader(text))
			{
				string item;
				while ((item = stringReader.ReadLine()) != null)
				{
					list.Add(item);
				}
			}
			foreach (string item2 in list)
			{
				int num = item2.IndexOf(':');
				if (num != -1)
				{
					string text2 = item2.Substring(0, num).Trim();
					string text3 = item2.Substring(num + 1).Trim();
					switch (text2)
					{
					case "DataProvider":
						dataSettings.DataProvider = text3;
						break;
					case "DataConnectionString":
						dataSettings.DataConnectionString = text3;
						break;
					default:
						dataSettings.RawDataSettings.Add(text2, text3);
						break;
					}
				}
			}
			return dataSettings;
		}

		protected virtual string ComposeSettings(DataSettings settings)
		{
			if (settings == null)
			{
				return "";
			}
			return string.Format("DataProvider: {0}{2}DataConnectionString: {1}{2}", settings.DataProvider, settings.DataConnectionString, Environment.NewLine);
		}

		public virtual DataSettings LoadSettings(string filePath = null)
		{
			if (string.IsNullOrEmpty(filePath))
			{
				filePath = Path.Combine(MapPath(), "Settings.txt");
			}
			if (File.Exists(filePath))
			{
				string text = File.ReadAllText(filePath);
				return ParseSettings(text);
			}
			return new DataSettings();
		}

		public virtual void SaveSettings(DataSettings settings)
		{
			if (settings == null)
			{
				throw new ArgumentNullException("settings");
			}
			string path = Path.Combine(MapPath(), "Settings.txt");
			if (!File.Exists(path))
			{
				using (File.Create(path))
				{
				}
			}
			string contents = ComposeSettings(settings);
			File.WriteAllText(path, contents);
		}
	}
}
