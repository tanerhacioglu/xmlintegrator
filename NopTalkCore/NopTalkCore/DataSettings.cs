using System.Collections.Generic;

namespace NopTalkCore
{
	public class DataSettings
	{
		public string DataProvider
		{
			get;
			set;
		}

		public string DataConnectionString
		{
			get;
			set;
		}

		public IDictionary<string, string> RawDataSettings
		{
			get;
			private set;
		}

		public DataSettings()
		{
			RawDataSettings = new Dictionary<string, string>();
		}

		public bool IsValid()
		{
			return !string.IsNullOrEmpty(DataProvider) && !string.IsNullOrEmpty(DataConnectionString);
		}
	}
}
