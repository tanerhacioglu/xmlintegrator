using System.Collections.Generic;

namespace NopTalkCore
{
	public class AttributeItem
	{
		public string AttributeName
		{
			get;
			set;
		}

		public List<OptionItem> OptionItems
		{
			get;
			set;
		}

		public AttributeItem()
		{
			OptionItems = new List<OptionItem>();
		}
	}
}
