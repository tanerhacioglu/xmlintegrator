using System;

namespace NopTalkCore
{
	public class SourceItemMapping
	{
		public Guid Id
		{
			get;
			set;
		}

		public int FieldIndex
		{
			get;
			set;
		}

		public int MappingItemType
		{
			get;
			set;
		}

		public string FieldName
		{
			get;
			set;
		}

		public string FieldNameForDisplay
		{
			get;
			set;
		}

		public int FieldMappingForIndex
		{
			get;
			set;
		}

		public string FieldMappingForXml
		{
			get;
			set;
		}

		public string FieldRule
		{
			get;
			set;
		}

		public string FieldRule1
		{
			get;
			set;
		}

		public bool MappedToTask
		{
			get;
			set;
		}

		public int Order
		{
			get;
			set;
		}

		public int Group
		{
			get;
			set;
		}
	}
}
