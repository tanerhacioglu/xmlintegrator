using System;

namespace NopTalkCore
{
	public class NopCustomer
	{
		public int Id
		{
			get;
			set;
		}

		public Guid CustomerGuid
		{
			get;
			set;
		}

		public string Username
		{
			get;
			set;
		}

		public string Email
		{
			get;
			set;
		}

		public string DayOfBirth
		{
			get;
			set;
		}

		public string EmailToRevalidate
		{
			get;
			set;
		}

		public string AdminComment
		{
			get;
			set;
		}

		public bool? IsTaxExempt
		{
			get;
			set;
		}

		public bool? Newsletter
		{
			get;
			set;
		}

		public int AffiliateId
		{
			get;
			set;
		}

		public int? VendorId
		{
			get;
			set;
		}

		public bool HasShoppingCartItems
		{
			get;
			set;
		}

		public bool RequireReLogin
		{
			get;
			set;
		}

		public int FailedLoginAttempts
		{
			get;
			set;
		}

		public DateTime? CannotLoginUntilDateUtc
		{
			get;
			set;
		}

		public bool? Active
		{
			get;
			set;
		}

		public bool Deleted
		{
			get;
			set;
		}

		public bool IsSystemAccount
		{
			get;
			set;
		}

		public string SystemName
		{
			get;
			set;
		}

		public string LastIpAddress
		{
			get;
			set;
		}

		public DateTime CreatedOnUtc
		{
			get;
			set;
		}

		public DateTime? LastLoginDateUtc
		{
			get;
			set;
		}

		public DateTime LastActivityDateUtc
		{
			get;
			set;
		}

		public int RegisteredInStoreId
		{
			get;
			set;
		}

		public int? BillingAddress_Id
		{
			get;
			set;
		}

		public int? ShippingAddress_Id
		{
			get;
			set;
		}

		public bool LimitedToStores
		{
			get;
			set;
		}

		public string SAPCustomer
		{
			get;
			set;
		}

		public string GPCustomer
		{
			get;
			set;
		}

		public string FirstName
		{
			get;
			set;
		}

		public string LastName
		{
			get;
			set;
		}

		public string Gender
		{
			get;
			set;
		}

		public NopCustomer()
		{
			CustomerGuid = Guid.NewGuid();
			IsTaxExempt = false;
			AffiliateId = 0;
			VendorId = 0;
			HasShoppingCartItems = false;
			RequireReLogin = false;
			FailedLoginAttempts = 0;
			CannotLoginUntilDateUtc = null;
			Active = true;
			Deleted = false;
			IsSystemAccount = false;
			CreatedOnUtc = DateTime.Now;
			LastActivityDateUtc = DateTime.Now;
			RegisteredInStoreId = 0;
			BillingAddress_Id = 0;
			ShippingAddress_Id = 0;
		}
	}
}
