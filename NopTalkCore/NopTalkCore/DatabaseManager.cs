using NopTalk;
using NopTalkCore.Struct.nopCommerce;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Z.BulkOperations;

namespace NopTalkCore
{
	public class DatabaseManager
	{
		private string _dbConnString;

		private NopCommerceVer _nopCommerceVersion = NopCommerceVer.Ver35;

		public List<NopTierPrice> NopTierPrices
		{
			get;
			set;
		}

		public List<NopShoppingCartItem> NopShoppingCartItems
		{
			get;
			set;
		}

		public DatabaseManager(string connString, NopCommerceVer nopCommerceVersion)
		{
			_dbConnString = connString;
			_nopCommerceVersion = nopCommerceVersion;
			NopTierPrices = new List<NopTierPrice>();
			NopShoppingCartItems = new List<NopShoppingCartItem>();
		}

		private void TierPricingsInsertUpdate()
		{
			if (NopTierPrices != null && NopTierPrices.Where((NopTierPrice x) => !x.Updated).Count() != 0)
			{
				IEnumerable<NopTierPrice> enumerable = NopTierPrices.Where((NopTierPrice x) => !x.Updated && !string.IsNullOrEmpty(x.ACCID));
				IEnumerable<NopTierPrice> enumerable2 = NopTierPrices.Where((NopTierPrice x) => !x.Updated && string.IsNullOrEmpty(x.ACCID));
				if (enumerable.Count() != 0 || enumerable2.Count() != 0)
				{
					using (SqlConnection sqlConnection = new SqlConnection(_dbConnString))
					{
						sqlConnection.Open();
						if (enumerable2?.Any() ?? false)
						{
							using (BulkOperation<NopTierPrice> bulkOperation = new BulkOperation<NopTierPrice>(sqlConnection))
							{
								bulkOperation.DestinationTableName = "TierPrice";
								bulkOperation.ColumnInputExpression = ((NopTierPrice c) => new
								{
									c.ProductId,
									c.StoreId,
									c.CustomerRoleId,
									c.Price,
									c.Quantity,
									c.StartDateTimeUtc,
									c.EndDateTimeUtc
								});
								bulkOperation.ColumnPrimaryKeyExpression = ((NopTierPrice c) => new
								{
									c.ProductId,
									c.StoreId,
									c.CustomerRoleId,
									c.Price,
									c.Quantity,
									c.StartDateTimeUtc
								});
								bulkOperation.ColumnOutputExpression = ((NopTierPrice c) => c.Id);
								bulkOperation.BulkMerge(enumerable2);
							}
						}
						if (enumerable?.Any() ?? false)
						{
							using (BulkOperation<NopTierPrice> bulkOperation2 = new BulkOperation<NopTierPrice>(sqlConnection))
							{
								bulkOperation2.DestinationTableName = "TierPrice";
								bulkOperation2.ColumnInputExpression = ((NopTierPrice c) => new
								{
									c.ProductId,
									c.StoreId,
									c.CustomerRoleId,
									c.Price,
									c.Quantity,
									c.StartDateTimeUtc,
									c.EndDateTimeUtc,
									c.ACCID
								});
								bulkOperation2.ColumnPrimaryKeyExpression = ((NopTierPrice c) => c.ACCID);
								bulkOperation2.ColumnOutputExpression = ((NopTierPrice c) => c.Id);
								bulkOperation2.BulkMerge(enumerable);
							}
						}
					}
					NopTierPrices.ForEach(delegate(NopTierPrice x)
					{
						x.Updated = true;
					});
				}
			}
		}

		private void ShoppingCartItemsInsertUpdate()
		{
			if (NopShoppingCartItems != null && NopShoppingCartItems.Where((NopShoppingCartItem x) => !x.Updated).Count() != 0)
			{
				NopShoppingCartItems = (from item in NopShoppingCartItems
					group item by new
					{
						item.StoreId,
						item.ShoppingCartTypeId,
						item.CustomerId,
						item.ProductId
					} into @group
					select @group.First()).ToList();
				IEnumerable<NopShoppingCartItem> enumerable = NopShoppingCartItems.Where((NopShoppingCartItem x) => !x.Updated && !string.IsNullOrEmpty(x.SKUID));
				IEnumerable<NopShoppingCartItem> enumerable2 = NopShoppingCartItems.Where((NopShoppingCartItem x) => !x.Updated && string.IsNullOrEmpty(x.SKUID));
				if (enumerable.Count() != 0 || enumerable2.Count() != 0)
				{
					using (SqlConnection sqlConnection = new SqlConnection(_dbConnString))
					{
						sqlConnection.Open();
						if (enumerable2?.Any() ?? false)
						{
							using (BulkOperation<NopShoppingCartItem> bulkOperation = new BulkOperation<NopShoppingCartItem>(sqlConnection))
							{
								bulkOperation.DestinationTableName = "ShoppingCartItem";
								bulkOperation.ColumnInputExpression = ((NopShoppingCartItem c) => new
								{
									c.ShoppingCartTypeId,
									c.ProductId,
									c.StoreId,
									c.CustomerId,
									c.CreatedOnUtc,
									c.UpdatedOnUtc,
									c.CustomerEnteredPrice,
									c.Quantity
								});
								bulkOperation.ColumnPrimaryKeyExpression = ((NopShoppingCartItem c) => new
								{
									c.ShoppingCartTypeId,
									c.ProductId,
									c.StoreId,
									c.CustomerId
								});
								bulkOperation.ColumnOutputExpression = ((NopShoppingCartItem c) => c.Id);
								bulkOperation.AllowDuplicateKeys = false;
								bulkOperation.BulkMerge(enumerable2);
							}
						}
						if (enumerable?.Any() ?? false)
						{
							using (BulkOperation<NopShoppingCartItem> bulkOperation2 = new BulkOperation<NopShoppingCartItem>(sqlConnection))
							{
								bulkOperation2.DestinationTableName = "ShoppingCartItem";
								bulkOperation2.ColumnInputExpression = ((NopShoppingCartItem c) => new
								{
									c.ShoppingCartTypeId,
									c.ProductId,
									c.StoreId,
									c.CustomerId,
									c.CreatedOnUtc,
									c.UpdatedOnUtc,
									c.SKUID,
									c.CustomerEnteredPrice,
									c.Quantity
								});
								bulkOperation2.ColumnPrimaryKeyExpression = ((NopShoppingCartItem c) => c.SKUID);
								bulkOperation2.ColumnOutputExpression = ((NopShoppingCartItem c) => c.Id);
								bulkOperation2.AllowDuplicateKeys = false;
								bulkOperation2.BulkMerge(enumerable);
							}
						}
					}
					NopShoppingCartItems.ForEach(delegate(NopShoppingCartItem x)
					{
						x.Updated = true;
					});
				}
			}
		}

		public void DatabaseSynchro()
		{
			if (NopTierPrices != null && NopTierPrices.Any())
			{
				TierPricingsInsertUpdate();
			}
			if (NopShoppingCartItems != null && NopShoppingCartItems.Any())
			{
				ShoppingCartItemsInsertUpdate();
			}
		}

		public void TierPricingsSynchro()
		{
			if (NopTierPrices != null && NopTierPrices.Where((NopTierPrice x) => !x.Updated).Count() > 500)
			{
				TierPricingsInsertUpdate();
			}
		}

		public void WishListSynchro()
		{
			if (NopShoppingCartItems != null && NopShoppingCartItems.Where((NopShoppingCartItem x) => !x.Updated).Count() > 500)
			{
				ShoppingCartItemsInsertUpdate();
			}
		}
	}
}
