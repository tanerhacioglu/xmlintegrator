using NopTalk;
using NopTalkCore.Struct.nopCommerce;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace NopTalkCore
{
	public class ImportManager
	{
		private ReplaceTextHelper replaceTextHelper;

		public static int productsLimits = 0;

		public static bool multipleStore = true;

		public static string expiredDate = null;

		public static bool mssql = true;

		public static bool mysql = true;

		public ImportManager()
		{
			if (!string.IsNullOrEmpty(expiredDate) && GlobalClass.StringToDatetime(expiredDate, "yyyy-MM-dd") < DateTime.Now)
			{
				throw new Exception("Your License is expired. Please contact info@artiglobal.net");
			}
		}

		public void StartTaskImport(int? relatedProductId, string sourceAddress, StoreData storeData, int taskId, bool scheduler, out string resultMessage, out long insertedItems, out long updatedItems, TaskMapping taskMapping = null)
		{
			long num = 0L;
			long num2 = 0L;
			long num3 = 0L;
			long num4 = 0L;
			long num5 = 0L;
			long num6 = 0L;
			long num7 = 0L;
			long num8 = 0L;
			long num9 = 0L;
			DateTime now = DateTime.Now;
			bool flag = false;
			resultMessage = "";
			long num10 = 0L;
			long num11 = 0L;
			insertedItems = 0L;
			updatedItems = 0L;
			decimal num12 = 0m;
			decimal num13 = 0m;
			int num14 = 0;
			long num15 = 0L;
			long num16 = 0L;
			long num17 = 0L;
			string productSku = (relatedProductId != null) ? relatedProductId.Value.ToString() : "";
			SourceMappingFunc sourceMappingFunc = new SourceMappingFunc();
			TaskMappingFunc taskMappingFunc = new TaskMappingFunc();
			NopManager nopManager = null;
			DatabaseManager databaseManager = null;
			List<string> list = new List<string>();
			Dictionary<string, int> dictionary = new Dictionary<string, int>();
			Dictionary<string, int> picturesInStore = new Dictionary<string, int>();
			Dictionary<string, int> picturesSeoInStore = new Dictionary<string, int>();
			Dictionary<int, string> dictionary2 = new Dictionary<int, string>();
			Dictionary<string, int> specAttributeList = new Dictionary<string, int>();
			Dictionary<string, int> specAttributeOptionList = new Dictionary<string, int>();
			Dictionary<string, int> specAttributeProductList = new Dictionary<string, int>();
			List<string> list2 = new List<string>();
			Dictionary<SourceItem, NopProduct> dictionary3 = new Dictionary<SourceItem, NopProduct>();
			StringBuilder stringBuilder = new StringBuilder();
			StringBuilder stringBuilder2 = new StringBuilder();
			bool flag2 = false;
			try
			{
				bool flag3 = !string.IsNullOrEmpty(ImportManager.expiredDate) && GlobalClass.StringToDatetime(ImportManager.expiredDate, "yyyy-MM-dd") < DateTime.Now;
				if (flag3)
				{
					throw new Exception("Your License is expired. Please contact info@vasksoft.com");
				}
				bool flag4 = relatedProductId == null;
				if (flag4)
				{
					LogManager.LogWrite(new long?((long)taskId), "Task started.", LogType.Info, null);
					stringBuilder.AppendLine("Task started: " + DateTime.Now.ToString());
				}
				bool flag5 = taskMapping == null;
				if (flag5)
				{
					taskMapping = taskMappingFunc.GetTaskMapping(taskId);
				}
				SourceData sourceData = sourceMappingFunc.GetSourceData(taskMapping.VendorSourceId);
				this.replaceTextHelper = new ReplaceTextHelper(taskMapping.VendorSourceId);
				byte[] fileContent = sourceMappingFunc.GetFileContent((SourceType)sourceData.SourceType, sourceData.SourceUser, sourceData.SourcePassword, string.IsNullOrEmpty(sourceAddress) ? sourceData.SourceAddress : sourceAddress, sourceData.SoapAction, sourceData.SoapRequest, sourceData.SourceEncoding, sourceData.SourceFormat == 2, sourceData.SourceContent, sourceData.AuthHeader);
				bool flag6 = storeData == null;
				if (flag6)
				{
					storeData = StoreFunc.GetStoreData(taskMapping.EShopId.Value);
				}
				bool flag7 = !string.IsNullOrEmpty(taskMapping.SaveSourceCopyPath);
				if (flag7)
				{
					string filename = string.Format("SourceCopy_TaskId_{0}.{1}", taskId, ((FileFormat)sourceData.SourceFormat).ToString()).ToLower();
					this.SaveSourceCopy(new long?((long)taskId), taskMapping.SaveSourceCopyPath, filename, fileContent, stringBuilder2);
				}
				SourceMapping sourceMapping = sourceMappingFunc.GetSourceMapping(taskMapping.VendorSourceId);
				sourceMapping.CsvDelimeter = sourceData.CsvDelimeter;
				sourceMapping.FirstRowIndex = sourceData.FirstRowIndex;
				nopManager = new NopManager(storeData.DatabaseConnString, storeData.Version, storeData.DatabaseType);
				bool flag8 = storeData.DatabaseType == DatabaseType.MSSQL && !ImportManager.mssql;
				if (flag8)
				{
					LogManager.LogWrite(new long?((long)taskId), "Task import stopped. NopTalk Pro version does not have MSSQL licence. Please contact info@vasksoft.com", LogType.Warning, productSku);
				}
				else
				{
					bool flag9 = storeData.DatabaseType == DatabaseType.MySQL && !ImportManager.mysql;
					if (flag9)
					{
						LogManager.LogWrite(new long?((long)taskId), "Task import stopped. NopTalk Pro version does not have MySQL licence. Please contact info@vasksoft.com", LogType.Warning, productSku);
					}
					else
					{
						databaseManager = new DatabaseManager(storeData.DatabaseConnString, storeData.Version);
						bool disableShop = taskMapping.DisableShop;
						if (disableShop)
						{
							nopManager.CloseStore(true);
						}
						this.RunCustomSql(new long?((long)taskId), taskMapping, nopManager, 1, stringBuilder2);
						taskMapping.MediaSaveInDatabase = nopManager.IsMediaStorageDatabase();
						bool flag10 = taskMapping.SavePicturesInFile && taskMapping.MediaSaveInDatabase != null && taskMapping.MediaSaveInDatabase.Value;
						if (flag10)
						{
							LogManager.LogWrite(new long?((long)taskId), "Pictures will not imported! Store Media Settings is set to store in database, NopTalk Media Settings is set to store in file system.", LogType.Warning, null);
						}
						else
						{
							bool flag11 = !taskMapping.SavePicturesInFile && taskMapping.MediaSaveInDatabase != null && !taskMapping.MediaSaveInDatabase.Value;
							if (flag11)
							{
								LogManager.LogWrite(new long?((long)taskId), "Pictures will not imported! Store Media Settings is set to store in file system, NopTalk Media Settings is set to store in database.", LogType.Warning, null);
							}
						}
						bool flag12 = fileContent != null;
						if (flag12)
						{
							bool flag13 = sourceData.SourceType == 4;
							SourceDocument sourceDoucument;
							if (flag13)
							{
								sourceDoucument = new SourceDocument(FileFormat.AliExpress, fileContent, sourceMapping, taskMapping, sourceData, false);
							}
							else
							{
								bool flag14 = sourceData.SourceType == 5;
								if (flag14)
								{
									sourceDoucument = new SourceDocument(FileFormat.BestSecret, fileContent, sourceMapping, taskMapping, sourceData, false);
								}
								else
								{
									sourceDoucument = new SourceDocument((FileFormat)sourceData.SourceFormat, fileContent, sourceMapping, taskMapping, sourceData, false);
								}
							}
							SourceDocument sourceDoucumentForTasks = taskMappingFunc.GetSourceDoucumentForTasks(sourceDoucument, taskMapping);
							num10 = (long)sourceDoucumentForTasks.SourceItems.Count<SourceItem>();
							bool flag15 = taskMapping.TaskAction == 0 || taskMapping.TaskAction == 1 || taskMapping.TaskAction == 2 || taskMapping.TaskAction == 3;
							if (flag15)
							{
								bool flag16 = num10 > 0L;
								if (flag16)
								{
									try
									{
										nopManager.ExistProducts(out dictionary2);
										bool flag17 = dictionary2.Count > 0;
										if (flag17)
										{
											flag2 = true;
										}
									}
									catch (Exception ex)
									{
									}
								}
								taskMappingFunc.UpdateTaskProgress(taskId, num10, num11);
								List<string> list3 = new List<string>();
								List<string> list4 = new List<string>();
								string text = "";
								bool flag18 = true;
								foreach (SourceItem sourceItem in sourceDoucumentForTasks.SourceItems)
								{
									flag = false;
									num16 += 1L;
									num17 += 1L;
									num14 = 0;
									try
									{
										bool flag19 = relatedProductId == null;
										if (flag19)
										{
											try
											{
												bool flag20 = num16 >= 5L;
												if (flag20)
												{
													bool flag21 = !taskMappingFunc.IsTaskRunning(taskId);
													if (flag21)
													{
														break;
													}
													taskMappingFunc.UpdateTaskProgress(taskId, num10, num11);
													num16 = 0L;
												}
											}
											catch
											{
											}
										}
										NopProduct nopProduct = new NopProduct();
										bool flag22 = text == "" || text != sourceItem.ProdId;
										flag18 = flag22;
										text = sourceItem.ProdId;
										nopProduct.Sku = (string.IsNullOrEmpty(sourceItem.ProdSku) ? sourceItem.ProdId : sourceItem.ProdSku);
										nopProduct.Name = sourceItem.ProdName;
										nopProduct.ShortDescription = sourceItem.ProdShortDesc;
										nopProduct.FullDescription = sourceItem.ProdFullDesc;
										nopProduct.ManufacturerPartNumber = sourceItem.ProdManPartNum;
										nopProduct.ProductCost = sourceItem.ProdCost.GetValueOrDefault();
										nopProduct.OldPrice = sourceItem.ProdOldPrice.GetValueOrDefault();
										nopProduct.Price = sourceItem.ProdPrice.GetValueOrDefault();
										nopProduct.Price = this.RoundDecimal(taskMapping.ProdPriceVal, nopProduct.Price);
										nopProduct.StockQuantity = sourceItem.ProdStock.GetValueOrDefault();
										nopProduct.AdminComment = "Artı Global auto import VendorSourceId=" + taskMapping.VendorSourceId.ToString();
										nopProduct.MetaKeywords = sourceItem.SeoMetaKey;
										nopProduct.MetaDescription = sourceItem.SeoMetaDesc;
										nopProduct.MetaTitle = sourceItem.SeoMetaTitle;
										nopProduct.TaxCategoryId = nopManager.GetTaxCategory(sourceItem.TaxCategory);
										nopProduct.Gtin = sourceItem.Gtin;
										nopProduct.Weight = sourceItem.Weight.GetValueOrDefault();
										nopProduct.Length = sourceItem.Length.GetValueOrDefault();
										nopProduct.Width = sourceItem.Width.GetValueOrDefault();
										nopProduct.Height = sourceItem.Height.GetValueOrDefault();
										nopProduct.HasTierPrices = (byte)((sourceItem.TierPricingItems != null && sourceItem.TierPricingItems.Any<SourceItem.TierPricingItem>()) ? 1 : 0);
										NopProduct nopProduct2 = nopProduct;
										int? num18 = taskMapping.ManageInventoryMethodId;
										int num19 = 2;
										nopProduct2.AllowAddingOnlyExistingAttributeCombinations = (byte)((num18.GetValueOrDefault() == num19 & num18 != null) ? 1 : 0);
										nopProduct.IsTaxExempt = Convert.ToByte(sourceItem.TaxExempt);
										nopProduct.BasepriceEnabled = Convert.ToByte(sourceItem.ProductPrice_BasepriceEnabled);
										nopProduct.BasepriceAmount = sourceItem.ProductPrice_BasepriceAmount;
										nopProduct.BasepriceBaseAmount = sourceItem.ProductPrice_BasepriceBaseAmount;
										nopProduct.BasepriceUnitId = nopManager.GetMeasureWeight(sourceItem.ProductPrice_BasepriceUnit);
										nopProduct.BasepriceBaseUnitId = nopManager.GetMeasureWeight(sourceItem.ProductPrice_BasepriceBaseUnit);
										nopProduct.AllowBackInStockSubscriptions = Convert.ToByte(sourceItem.ProductStock_AllowBackInStockSubscriptions);
										nopProduct.DisableBuyButton = Convert.ToByte(sourceItem.ProductStock_DisableBuyButton);
										bool prodSettingsMinStockQtyAction = taskMapping.ProdSettingsMinStockQtyAction;
										if (prodSettingsMinStockQtyAction)
										{
											nopProduct.MinStockQuantity = sourceItem.ProdSettingsMinStockQty;
										}
										bool prodSettingsNotifyQtyAction = taskMapping.ProdSettingsNotifyQtyAction;
										if (prodSettingsNotifyQtyAction)
										{
											nopProduct.NotifyAdminForQuantityBelow = sourceItem.ProdSettingsNotifyQty;
										}
										bool prodSettingsMinCartQtyAction = taskMapping.ProdSettingsMinCartQtyAction;
										if (prodSettingsMinCartQtyAction)
										{
											nopProduct.OrderMinimumQuantity = sourceItem.ProdSettingsMinCartQty;
										}
										bool prodSettingsMaxCartQtyAction = taskMapping.ProdSettingsMaxCartQtyAction;
										if (prodSettingsMaxCartQtyAction)
										{
											nopProduct.OrderMaximumQuantity = sourceItem.ProdSettingsMaxCartQty;
										}
										bool prodSettingsAllowedQtyAction = taskMapping.ProdSettingsAllowedQtyAction;
										if (prodSettingsAllowedQtyAction)
										{
											nopProduct.AllowedQuantities = ((sourceItem.ProdSettingsAllowedQty > -1) ? new int?(sourceItem.ProdSettingsAllowedQty) : null);
										}
										bool prodSettingsShippingEnabledAction = taskMapping.ProdSettingsShippingEnabledAction;
										if (prodSettingsShippingEnabledAction)
										{
											nopProduct.IsShipEnabled = Convert.ToByte(sourceItem.ProdSettingsShippingEnabled);
										}
										bool prodSettingsFreeShippingAction = taskMapping.ProdSettingsFreeShippingAction;
										if (prodSettingsFreeShippingAction)
										{
											nopProduct.IsFreeShipping = Convert.ToByte(sourceItem.ProdSettingsFreeShipping);
										}
										bool prodSettingsShipSeparatelyAction = taskMapping.ProdSettingsShipSeparatelyAction;
										if (prodSettingsShipSeparatelyAction)
										{
											nopProduct.ShipSeparately = Convert.ToByte(sourceItem.ProdSettingsShipSeparately);
										}
										bool prodSettingsShippingChargeAction = taskMapping.ProdSettingsShippingChargeAction;
										if (prodSettingsShippingChargeAction)
										{
											nopProduct.AdditionalShippingCharge = sourceItem.ProdSettingsShippingCharge.GetValueOrDefault();
										}
										bool prodSettingsVisibleIndividuallyAction = taskMapping.ProdSettingsVisibleIndividuallyAction;
										if (prodSettingsVisibleIndividuallyAction)
										{
											nopProduct.VisibleIndividually = Convert.ToByte(GlobalClass.ObjectToBool(sourceItem.ProdSettingsVisibleIndividually));
										}
										bool prodSettingsIsDeletedAction = taskMapping.ProdSettingsIsDeletedAction;
										if (prodSettingsIsDeletedAction)
										{
											nopProduct.Deleted = Convert.ToByte(GlobalClass.ObjectToBool(sourceItem.ProdSettingsIsDeleted));
										}
										bool flag23 = false;
										bool flag24 = !string.IsNullOrEmpty(sourceItem.ProdGroupBy) && sourceItem.ProdGroupBy != sourceItem.ProdSku;
										if (flag24)
										{
											bool flag25 = true;
											bool flag26 = !dictionary.ContainsKey(sourceItem.ProdGroupBy);
											if (flag26)
											{
												int num20 = this.InsertGroupParentProduct(sourceItem, taskMapping, taskId, nopManager, sourceMapping, storeData, flag2, dictionary2, flag25, stringBuilder2, picturesInStore, num, num2, num3, num4, num5, num6, num7, num8, num9, flag18, specAttributeList, specAttributeOptionList, specAttributeProductList, picturesSeoInStore);
												bool flag27 = flag25 && num20 > 0;
												if (flag27)
												{
													nopProduct.ParentGroupedProductId = num20;
													dictionary.Add(sourceItem.ProdGroupBy, nopProduct.ParentGroupedProductId);
												}
											}
											else
											{
												bool flag28 = flag25;
												if (flag28)
												{
													nopProduct.ParentGroupedProductId = dictionary[sourceItem.ProdGroupBy];
												}
											}
											flag23 = true;
											bool flag29 = !taskMapping.ProdSettingsVisibleIndividuallyAction;
											if (flag29)
											{
												nopProduct.VisibleIndividually = 0;
											}
										}
										nopProduct.SubjectToAcl = ((sourceItem.CustomerRoles != null && sourceItem.CustomerRoles.Any<SourceItem.CustomerRuleItem>() && (taskMapping.CustomerRoleAction || taskMapping.CustomerRole1Action || taskMapping.CustomerRole2Action || taskMapping.CustomerRole3Action || taskMapping.CustomerRole4Action)) ? new bool?(true) : null);
										productSku = nopProduct.Sku.ToString();
										bool flag30 = taskMapping.ProductPublish != null;
										if (flag30)
										{
											nopProduct.Published = Convert.ToByte(taskMapping.ProductPublish.Value);
										}
										bool flag31 = taskMapping.AllowCustomerReviews != null;
										if (flag31)
										{
											nopProduct.AllowCustomerReviews = Convert.ToByte(taskMapping.AllowCustomerReviews.Value);
										}
										bool flag32 = taskMapping.DisplayAvailability != null;
										if (flag32)
										{
											nopProduct.DisplayStockAvailability = Convert.ToByte(taskMapping.DisplayAvailability.Value);
										}
										bool flag33 = taskMapping.DisplayStockQuantity != null;
										if (flag33)
										{
											nopProduct.DisplayStockQuantity = Convert.ToByte(taskMapping.DisplayStockQuantity.Value);
										}
										bool flag34 = taskMapping.DeliveryDayId != null;
										if (flag34)
										{
											nopProduct.DeliveryDateId = taskMapping.DeliveryDayId.Value;
										}
										bool flag35 = taskMapping.ManageInventoryMethodId != null;
										if (flag35)
										{
											nopProduct.ManageInventoryMethodId = taskMapping.ManageInventoryMethodId.Value;
										}
										bool flag36 = taskMapping.BackorderModeId != null;
										if (flag36)
										{
											nopProduct.BackorderModeId = taskMapping.BackorderModeId.Value;
										}
										bool flag37 = taskMapping.LowStockActivityId != null;
										if (flag37)
										{
											nopProduct.LowStockActivityId = taskMapping.LowStockActivityId.Value;
										}
										bool flag38 = !string.IsNullOrEmpty(taskMapping.LimitedToStores);
										if (flag38)
										{
											list3 = taskMapping.LimitedToStores.Split(new char[]
											{
												';'
											}, StringSplitOptions.RemoveEmptyEntries).ToList<string>();
											list4 = taskMapping.LimitedToStoresEntities.Split(new char[]
											{
												';'
											}, StringSplitOptions.RemoveEmptyEntries).ToList<string>();
											nopProduct.LimitedToStores = true;
										}
										bool unavailable = sourceItem.Unavailable;
										if (unavailable)
										{
											nopProduct.DisableBuyButton = 1;
										}
										bool vendorIsId = sourceMapping.VendorIsId;
										if (vendorIsId)
										{
											nopProduct.VendorId = GlobalClass.StringToInteger(taskMapping.VendorVal, 0);
										}
										else
										{
											string searchValue = taskMapping.VendorAction ? sourceItem.Vendor : string.Empty;
											nopProduct.VendorId = nopManager.GetVendor(searchValue);
										}
										bool prodSettingsDeliveryDateAction = taskMapping.ProdSettingsDeliveryDateAction;
										if (prodSettingsDeliveryDateAction)
										{
											bool deliveryDateIsId = sourceMapping.DeliveryDateIsId;
											if (deliveryDateIsId)
											{
												nopProduct.DeliveryDateId = GlobalClass.StringToInteger(sourceItem.ProdSettingsDeliveryDate, taskMapping.DeliveryDayId.GetValueOrDefault());
											}
											else
											{
												int deliveryDate = nopManager.GetDeliveryDate(sourceItem.ProdSettingsDeliveryDate);
												nopProduct.DeliveryDateId = ((deliveryDate > 0) ? deliveryDate : taskMapping.DeliveryDayId.GetValueOrDefault());
											}
										}
										bool flag39 = taskMapping.WarehouseId != null;
										if (flag39)
										{
											nopProduct.WarehouseId = taskMapping.WarehouseId.Value;
										}
										num18 = taskMapping.ProductTemplateId;
										num19 = 9989;
										bool flag40 = !(num18.GetValueOrDefault() == num19 & num18 != null) || flag23;
										if (flag40)
										{
											bool flag41 = taskMapping.ProductTemplateId != null;
											if (flag41)
											{
												NopProduct nopProduct3 = nopProduct;
												num18 = taskMapping.ProductTemplateId;
												num19 = 9989;
												nopProduct3.ProductTemplateId = ((num18.GetValueOrDefault() == num19 & num18 != null) ? 1 : taskMapping.ProductTemplateId.Value);
												num18 = taskMapping.ProductTemplateId;
												num19 = 2;
												bool flag42 = num18.GetValueOrDefault() == num19 & num18 != null;
												if (flag42)
												{
													nopProduct.ProductTypeId = 10;
												}
											}
										}
										else
										{
											nopProduct.LeaveProductType = true;
										}
										bool flag43;
										if (relatedProductId != null)
										{
											num18 = relatedProductId;
											num19 = 0;
											flag43 = (num18.GetValueOrDefault() > num19 & num18 != null);
										}
										else
										{
											flag43 = false;
										}
										bool flag44 = flag43;
										if (flag44)
										{
											num14 = relatedProductId.Value;
										}
										else
										{
											bool flag45 = flag2 && sourceData.ProductIdInStore != 1;
											if (flag45)
											{
												bool flag46 = sourceData.ProductIdInStore == 2;
												if (flag46)
												{
													bool flag47 = dictionary2.ContainsKey(GlobalClass.StringToInteger(text, 0));
													if (flag47)
													{
														num14 = GlobalClass.StringToInteger(text, 0);
													}
												}
												else
												{
													string tempSku = nopProduct.Sku.ToString().ToLower().Trim();
													bool flag48 = dictionary2.ContainsValue(tempSku);
													if (flag48)
													{
														num14 = GlobalClass.StringToInteger((from x in dictionary2
																							 where x.Value == tempSku
																							 select x).FirstOrDefault<KeyValuePair<int, string>>().Key, 0);
													}
												}
											}
											else
											{
												nopManager.SelectProduct((sourceData.ProductIdInStore == 2) ? text : nopProduct.Sku.ToString(), nopProduct.VendorId, sourceData.ProductIdInStore, out num14, out num12, out num13);
											}
										}
										bool flag49 = num14 == 0 && (taskMapping.TaskAction == 0 || taskMapping.TaskAction == 2);
										if (flag49)
										{
											flag = true;
											this.AddSpecAttributtesToFullDecription(taskMapping, sourceItem, nopProduct);
											num14 = nopManager.InsertProduct(nopProduct);
											nopProduct.ProductId = num14;
											dictionary3.Add(sourceItem, nopProduct);
											bool flag50 = flag2 && !dictionary2.ContainsKey(num14);
											if (flag50)
											{
												dictionary2.Add(num14, nopProduct.Sku.ToString().ToLower().Trim());
											}
											list.Add(nopProduct.Sku.ToString());
											string value = string.IsNullOrEmpty(sourceItem.SearchPage) ? (nopProduct.Name + "-" + num14.ToString()) : sourceItem.SearchPage;
											nopManager.InsertUpdateUrlRecord("Product", num14, this.ReplaceKeyParams(num14, value));
											num += this.InsertUpdateImages(new long?((long)taskId), sourceMapping, taskMapping, sourceItem, nopManager, num14, flag18, nopProduct, stringBuilder2, picturesInStore, picturesSeoInStore);
											num4 += this.InsertUpdateProductAttributes(new long?((long)taskId), sourceMapping, taskMapping, sourceItem, nopManager, nopProduct, storeData, flag18, stringBuilder2, picturesInStore, picturesSeoInStore);
											num5 += this.InsertUpdateSpecAttributes(new long?((long)taskId), sourceMapping, taskMapping, sourceItem, nopManager, nopProduct, flag18, stringBuilder2, specAttributeList, specAttributeOptionList, specAttributeProductList);
											num2 += this.InsertUpdateCategories(new long?((long)taskId), sourceMapping, taskMapping, sourceItem, nopManager, nopProduct, list3, list4, flag18, stringBuilder2);
											num3 += this.InsertUpdateManufacturer(new long?((long)taskId), sourceMapping, taskMapping, sourceItem, nopManager, nopProduct, list3, list4, flag18, stringBuilder2);
											num7 += this.InsertUpdateTirPricing((long)taskId, sourceMapping, taskMapping, sourceItem, nopManager, nopProduct, list3, list4, stringBuilder2, flag18, databaseManager);
											num6 += this.InsertUpdateProductTags(new long?((long)taskId), sourceMapping, taskMapping, sourceItem, nopManager, nopProduct, flag18, stringBuilder2);
											num8 += this.InsertUpdateProductCrossSells(new long?((long)taskId), sourceMapping, taskMapping, sourceItem, nopManager, nopProduct, flag18, stringBuilder2, dictionary2);
											num9 += this.InsertUpdateProductRelated(new long?((long)taskId), sourceMapping, taskMapping, sourceItem, nopManager, nopProduct, flag18, stringBuilder2, dictionary2);
											bool flag51 = taskMapping.CustomerRoleAction || taskMapping.CustomerRole1Action || taskMapping.CustomerRole2Action || taskMapping.CustomerRole3Action || taskMapping.CustomerRole4Action;
											if (flag51)
											{
												nopManager.DeleteAclMappings("product", num14);
												foreach (SourceItem.CustomerRuleItem customerRuleItem in sourceItem.CustomerRoles)
												{
													bool flag52 = !string.IsNullOrEmpty(customerRuleItem.CustomerRule);
													if (flag52)
													{
														int customerRole = nopManager.GetCustomerRole(customerRuleItem.CustomerRule);
														nopManager.InsertAclMappingRecord("product", num14, customerRole);
													}
												}
											}
											bool flag53 = nopProduct.LimitedToStores && list4.Contains("1");
											if (flag53)
											{
												foreach (string value2 in list3)
												{
													bool flag54 = GlobalClass.StringToInteger(value2, 0) > 0;
													if (flag54)
													{
														nopManager.GetStoreMapping(num14, "Product", GlobalClass.StringToInteger(value2, 0));
													}
												}
											}
											num11 += 1L;
											insertedItems += 1L;
										}
										else
										{
											bool flag55 = num14 > 0 && ((taskMapping.TaskAction == 0 && !flag18) || taskMapping.TaskAction == 1 || taskMapping.TaskAction == 2);
											if (flag55)
											{
												nopProduct.ProductId = num14;
												bool flag56 = !dictionary3.ContainsKey(sourceItem);
												if (flag56)
												{
													dictionary3.Add(sourceItem, nopProduct);
												}
												bool flag57 = taskMapping.TaskAction == 3;
												if (flag57)
												{
													updatedItems += 1L;
													continue;
												}
												bool flag58 = num14 == nopProduct.ParentGroupedProductId;
												if (flag58)
												{
													updatedItems += 1L;
													continue;
												}
												bool flag59 = list.Contains(nopProduct.Sku.ToString());
												if (flag59)
												{
													flag18 = false;
												}
												else
												{
													list.Add(nopProduct.Sku.ToString());
												}
												bool flag60 = flag2 && !dictionary2.ContainsKey(num14);
												if (flag60)
												{
													dictionary2.Add(num14, nopProduct.Sku.ToString().ToLower().Trim());
												}
												flag = true;
												bool flag61 = flag18;
												if (flag61)
												{
													this.AddSpecAttributtesToFullDecription(taskMapping, sourceItem, nopProduct);
													nopManager.UpdateProduct(nopProduct, taskMapping);
													bool seoPageAction = taskMapping.SeoPageAction;
													if (seoPageAction)
													{
														string value3 = string.IsNullOrEmpty(sourceItem.SearchPage) ? (nopProduct.Name + "-" + num14.ToString()) : sourceItem.SearchPage;
														nopManager.InsertUpdateUrlRecord("Product", num14, this.ReplaceKeyParams(num14, value3));
													}
												}
												num += this.InsertUpdateImages(new long?((long)taskId), sourceMapping, taskMapping, sourceItem, nopManager, num14, flag18, nopProduct, stringBuilder2, picturesInStore, picturesSeoInStore);
												num4 += this.InsertUpdateProductAttributes(new long?((long)taskId), sourceMapping, taskMapping, sourceItem, nopManager, nopProduct, storeData, flag18, stringBuilder2, picturesInStore, picturesSeoInStore);
												num5 += this.InsertUpdateSpecAttributes(new long?((long)taskId), sourceMapping, taskMapping, sourceItem, nopManager, nopProduct, flag18, stringBuilder2, specAttributeList, specAttributeOptionList, specAttributeProductList);
												bool flag62 = flag18;
												if (flag62)
												{
													num2 += this.InsertUpdateCategories(new long?((long)taskId), sourceMapping, taskMapping, sourceItem, nopManager, nopProduct, list3, list4, flag18, stringBuilder2);
												}
												bool flag63 = flag18;
												if (flag63)
												{
													num3 += this.InsertUpdateManufacturer(new long?((long)taskId), sourceMapping, taskMapping, sourceItem, nopManager, nopProduct, list3, list4, flag18, stringBuilder2);
												}
												num7 += this.InsertUpdateTirPricing((long)taskId, sourceMapping, taskMapping, sourceItem, nopManager, nopProduct, list3, list4, stringBuilder2, flag18, databaseManager);
												bool flag64 = flag18;
												if (flag64)
												{
													num6 += this.InsertUpdateProductTags(new long?((long)taskId), sourceMapping, taskMapping, sourceItem, nopManager, nopProduct, flag18, stringBuilder2);
												}
												bool flag65 = flag18;
												if (flag65)
												{
													num8 += this.InsertUpdateProductCrossSells(new long?((long)taskId), sourceMapping, taskMapping, sourceItem, nopManager, nopProduct, flag18, stringBuilder2, dictionary2);
												}
												bool flag66 = flag18;
												if (flag66)
												{
													num9 += this.InsertUpdateProductRelated(new long?((long)taskId), sourceMapping, taskMapping, sourceItem, nopManager, nopProduct, flag18, stringBuilder2, dictionary2);
												}
												bool flag67 = flag18;
												if (flag67)
												{
													bool flag68 = taskMapping.CustomerRoleAction || taskMapping.CustomerRole1Action || taskMapping.CustomerRole2Action || taskMapping.CustomerRole3Action || taskMapping.CustomerRole4Action;
													if (flag68)
													{
														bool flag69 = flag18;
														if (flag69)
														{
															nopManager.DeleteAclMappings("product", num14);
														}
														foreach (SourceItem.CustomerRuleItem customerRuleItem2 in sourceItem.CustomerRoles)
														{
															bool flag70 = !string.IsNullOrEmpty(customerRuleItem2.CustomerRule);
															if (flag70)
															{
																int customerRole2 = nopManager.GetCustomerRole(customerRuleItem2.CustomerRule);
																nopManager.InsertAclMappingRecord("product", num14, customerRole2);
															}
														}
													}
													bool flag71 = list4.Contains("1");
													if (flag71)
													{
														bool flag72 = flag18;
														if (flag72)
														{
															nopManager.DeleteStoreMapping(num14, "Product");
														}
														bool limitedToStores = nopProduct.LimitedToStores;
														if (limitedToStores)
														{
															foreach (string value4 in list3)
															{
																bool flag73 = GlobalClass.StringToInteger(value4, 0) > 0;
																if (flag73)
																{
																	nopManager.GetStoreMapping(num14, "Product", GlobalClass.StringToInteger(value4, 0));
																}
															}
														}
													}
												}
												num11 += 1L;
												updatedItems += 1L;
											}
										}
										bool flag74 = flag && flag18 && sourceItem.RelatedTaskId.GetValueOrDefault() > 0 && !string.IsNullOrEmpty(sourceItem.RelatedSourceAddress);
										if (flag74)
										{
											long num21 = 0L;
											long num22 = 0L;
											taskMappingFunc.UpdateTaskRunning(sourceItem.RelatedTaskId.Value, true);
											this.StartTaskImport(new int?(num14), sourceItem.RelatedSourceAddress, storeData, sourceItem.RelatedTaskId.Value, scheduler, out resultMessage, out num21, out num22, null);
											taskMappingFunc.UpdateTaskRunning(sourceItem.RelatedTaskId.Value, false);
											bool flag75 = !flag;
											if (flag75)
											{
												insertedItems += num21;
												updatedItems += num22;
												bool flag76 = num21 > 0L || num22 > 0L;
												if (flag76)
												{
													num11 += 1L;
												}
											}
										}
										num15 += 1L;
										bool flag77 = ((taskMapping.TaskAction == 1) ? num11 : num15) >= (long)ImportManager.productsLimits && ImportManager.productsLimits > 0;
										if (flag77)
										{
											LogManager.LogWrite(new long?((long)taskId), "Task import stopped. NopTalk Pro version has products import limit=" + ImportManager.productsLimits.ToString(), LogType.Warning, productSku);
											break;
										}
									}
									catch (Exception ex2)
									{
										LogManager.LogWrite(new long?((long)taskId), "Item import unexpected error: " + ex2.ToString(), LogType.Error, productSku);
										stringBuilder2.AppendLine("Item import unexpected error: " + ex2.ToString());
										bool flag78 = num17 >= (long)ImportManager.productsLimits && ImportManager.productsLimits > 0;
										if (flag78)
										{
											LogManager.LogWrite(new long?((long)taskId), "Task import stopped. NopTalk Pro version has products import limit=" + ImportManager.productsLimits.ToString(), LogType.Warning, productSku);
											break;
										}
									}
									bool flag79;
									if (dictionary3.Count >= 50 || num17 == num10)
									{
										TaskTranslate taskTranslate = taskMapping.TaskTranslate;
										flag79 = (taskTranslate != null && taskTranslate.LanguageId != null);
									}
									else
									{
										flag79 = false;
									}
									bool flag80 = flag79;
									if (flag80)
									{
										this.TranslateText(new long?((long)taskId), taskMapping, dictionary3, stringBuilder2);
										this.AddLocalizedProperties(new long?((long)taskId), sourceMapping, taskMapping, nopManager, dictionary3, stringBuilder2);
										this.replaceTextHelper.SaveReplacedFile();
									}
									bool flag81 = dictionary3.Count >= 50 || num17 == num10;
									if (flag81)
									{
										resultMessage = string.Format("Task performance statistic. Items imported:{7}, Pictures duration: {0}s, Categories duration: {1}s, Manufactures duration: {2}s, Product Attributtes duration: {3}s, Spec Attributtes duration: {4}s, Product Tags duration: {5}s, Tier pricing duration: {6}s, CrossSell duration: {8}s, Related duration: {9}s", new object[]
										{
											num / 1000L,
											num2 / 1000L,
											num3 / 1000L,
											num4 / 1000L,
											num5 / 1000L,
											num6 / 1000L,
											num7 / 1000L,
											num11,
											num8 / 1000L,
											num9 / 1000L
										});
										dictionary3 = new Dictionary<SourceItem, NopProduct>();
									}
								}
								databaseManager.DatabaseSynchro();
								bool flag82 = taskMapping.UnpublishProducts && (taskMapping.TaskAction == 1 || taskMapping.TaskAction == 2);
								if (flag82)
								{
									nopManager.UnpublishNotUpdatedProducts("NopTalk auto import VendorSourceId=" + taskMapping.VendorSourceId.ToString(), now);
								}
							}
							else
							{
								stringBuilder2.AppendLine("Task action (insert/update) not found.");
								LogManager.LogWrite(new long?((long)taskId), "Task action (insert/update) not found.", LogType.Warning, productSku);
							}
						}
						else
						{
							stringBuilder2.AppendLine("The source file is empty.");
							LogManager.LogWrite(new long?((long)taskId), "The source file is empty.", LogType.Warning, productSku);
						}
						try
						{
							bool flag83 = taskMapping.DeleteSourceFile && sourceData.SourceType == 0;
							if (flag83)
							{
								File.Delete(sourceData.SourceAddress);
							}
						}
						catch (Exception ex3)
						{
							stringBuilder2.AppendLine("Error deleting file:" + ex3.ToString());
							LogManager.LogWrite(new long?((long)taskId), "Error deleting file:" + ex3.ToString(), LogType.Error, productSku);
						}
						bool disableShop2 = taskMapping.DisableShop;
						if (disableShop2)
						{
							nopManager.CloseStore(false);
						}
						this.RunCustomSql(new long?((long)taskId), taskMapping, nopManager, 2, stringBuilder2);
					}
				}
			}
			catch (Exception ex4)
			{
				stringBuilder2.AppendLine("Task import unexpected error: " + ex4.ToString());
				LogManager.LogWrite(new long?((long)taskId), "Task import unexpected error: " + ex4.ToString(), LogType.Error, productSku);
			}
			finally
			{
				try
				{
					taskMappingFunc.UpdateTaskData(taskId, num10, num11, new DateTime?(DateTime.Now));
				}
				catch
				{
				}
				bool flag84 = relatedProductId == null;
				if (flag84)
				{
					resultMessage = string.Format("Task finished. Result of import. Items found: " + num10.ToString() + ". Total items imported: {0}. Inserted:{1}, Updated:{2}", num11, insertedItems, updatedItems);
					LogManager.LogWrite(new long?((long)taskId), resultMessage, LogType.Info, productSku);
					stringBuilder.AppendLine("Task Finished: " + DateTime.Now.ToString());
					stringBuilder.AppendLine("Result: " + string.Format("Items found: " + num10.ToString() + ". Total items imported: {0}. Inserted:{1}, Updated:{2}", num11, insertedItems, updatedItems));
					this.SendLogNotification(new long?((long)taskId), taskMapping, stringBuilder, stringBuilder2);
				}
				bool flag85 = taskMappingFunc.IsTaskRunning(taskId) && taskMapping.RelatedTaskId.GetValueOrDefault() > 0 && string.IsNullOrEmpty(taskMapping.RelatedSourceAddress);
				if (flag85)
				{
					long num23 = 0L;
					long num24 = 0L;
					taskMappingFunc.UpdateTaskRunning(taskMapping.RelatedTaskId.Value, true);
					this.StartTaskImport(null, null, storeData, taskMapping.RelatedTaskId.Value, scheduler, out resultMessage, out num23, out num24, null);
					taskMappingFunc.UpdateTaskRunning(taskMapping.RelatedTaskId.Value, false);
				}
			}
		}

		private int InsertGroupParentProduct(SourceItem item, TaskMapping taskMapping, int taskId, NopManager nopManager, SourceMapping sourceMapping, StoreData storeData, bool initialData, Dictionary<int, string> productsInStore, bool childProducts, StringBuilder errorList, Dictionary<string, int> picturesInStore, long duration_pictures_import, long duration_categories_import, long duration_manufactures_import, long duration_product_attributtes_import, long duration_spec_attributtes_import, long duration_product_tags_import, long duration_tier_pricing_import, long duration_crossell_import, long duration_related_import, bool newItem, Dictionary<string, int> specAttributeList, Dictionary<string, int> specAttributeOptionList, Dictionary<string, int> specAttributeProductList, Dictionary<string, int> picturesSeoInStore)
		{
			NopProduct nopProduct = new NopProduct();
			int productId = 0;
			List<string> list = new List<string>();
			List<string> list2 = new List<string>();
			nopProduct.Sku = item.ProdGroupBy;
			nopProduct.Name = item.ProdName;
			nopProduct.ShortDescription = item.ProdShortDesc;
			nopProduct.FullDescription = item.ProdFullDesc;
			nopProduct.ManufacturerPartNumber = item.ProdManPartNum;
			nopProduct.StockQuantity = item.ProdStock.GetValueOrDefault();
			nopProduct.AdminComment = "NopTalk auto import VendorSourceId=" + taskMapping.VendorSourceId;
			nopProduct.MetaKeywords = item.SeoMetaKey;
			nopProduct.MetaDescription = item.SeoMetaDesc;
			nopProduct.MetaTitle = item.SeoMetaTitle;
			nopProduct.TaxCategoryId = nopManager.GetTaxCategory(item.TaxCategory);
			nopProduct.Gtin = item.Gtin;
			nopProduct.Weight = item.Weight.GetValueOrDefault();
			nopProduct.Length = item.Length.GetValueOrDefault();
			nopProduct.Width = item.Width.GetValueOrDefault();
			nopProduct.Height = item.Height.GetValueOrDefault();
			nopProduct.HasTierPrices = (byte)((item.TierPricingItems != null && item.TierPricingItems.Any()) ? 1 : 0);
			nopProduct.AllowAddingOnlyExistingAttributeCombinations = (byte)((taskMapping.ManageInventoryMethodId == 2) ? 1 : 0);
			nopProduct.IsTaxExempt = Convert.ToByte(item.TaxExempt);
			nopProduct.BasepriceEnabled = Convert.ToByte(item.ProductPrice_BasepriceEnabled);
			nopProduct.BasepriceAmount = item.ProductPrice_BasepriceAmount;
			nopProduct.BasepriceBaseAmount = item.ProductPrice_BasepriceBaseAmount;
			nopProduct.BasepriceUnitId = nopManager.GetMeasureWeight(item.ProductPrice_BasepriceUnit);
			nopProduct.BasepriceBaseUnitId = nopManager.GetMeasureWeight(item.ProductPrice_BasepriceBaseUnit);
			nopProduct.AllowBackInStockSubscriptions = Convert.ToByte(item.ProductStock_AllowBackInStockSubscriptions);
			nopProduct.DisableBuyButton = Convert.ToByte(item.ProductStock_DisableBuyButton);
			if (taskMapping.ProdSettingsMinStockQtyAction)
			{
				nopProduct.MinStockQuantity = item.ProdSettingsMinStockQty;
			}
			if (taskMapping.ProdSettingsNotifyQtyAction)
			{
				nopProduct.NotifyAdminForQuantityBelow = item.ProdSettingsNotifyQty;
			}
			if (taskMapping.ProdSettingsMinCartQtyAction)
			{
				nopProduct.OrderMinimumQuantity = item.ProdSettingsMinCartQty;
			}
			if (taskMapping.ProdSettingsMaxCartQtyAction)
			{
				nopProduct.OrderMaximumQuantity = item.ProdSettingsMaxCartQty;
			}
			if (taskMapping.ProdSettingsAllowedQtyAction)
			{
				nopProduct.AllowedQuantities = ((item.ProdSettingsAllowedQty > -1) ? new int?(item.ProdSettingsAllowedQty) : null);
			}
			if (taskMapping.ProdSettingsShippingEnabledAction)
			{
				nopProduct.IsShipEnabled = Convert.ToByte(item.ProdSettingsShippingEnabled);
			}
			if (taskMapping.ProdSettingsFreeShippingAction)
			{
				nopProduct.IsFreeShipping = Convert.ToByte(item.ProdSettingsFreeShipping);
			}
			if (taskMapping.ProdSettingsShipSeparatelyAction)
			{
				nopProduct.ShipSeparately = Convert.ToByte(item.ProdSettingsShipSeparately);
			}
			if (taskMapping.ProdSettingsShippingChargeAction)
			{
				nopProduct.AdditionalShippingCharge = item.ProdSettingsShippingCharge.GetValueOrDefault();
			}
			if (taskMapping.ProdSettingsVisibleIndividuallyAction)
			{
				nopProduct.VisibleIndividually = Convert.ToByte(GlobalClass.ObjectToBool(item.ProdSettingsVisibleIndividually));
			}
			if (taskMapping.ProdSettingsIsDeletedAction)
			{
				nopProduct.Deleted = Convert.ToByte(GlobalClass.ObjectToBool(item.ProdSettingsIsDeleted));
			}
			if (childProducts)
			{
				nopProduct.ProductTypeId = 10;
				nopProduct.ProductTemplateId = 2;
			}
			nopProduct.SubjectToAcl = ((item.CustomerRoles.Any() && (taskMapping.CustomerRoleAction || taskMapping.CustomerRole1Action || taskMapping.CustomerRole2Action || taskMapping.CustomerRole3Action || taskMapping.CustomerRole4Action)) ? new bool?(true) : null);
			if (taskMapping.ProductPublish.HasValue)
			{
				nopProduct.Published = Convert.ToByte(taskMapping.ProductPublish.Value);
			}
			if (taskMapping.AllowCustomerReviews.HasValue)
			{
				nopProduct.AllowCustomerReviews = Convert.ToByte(taskMapping.AllowCustomerReviews.Value);
			}
			if (taskMapping.DisplayAvailability.HasValue)
			{
				nopProduct.DisplayStockAvailability = Convert.ToByte(taskMapping.DisplayAvailability.Value);
			}
			if (taskMapping.DisplayStockQuantity.HasValue)
			{
				nopProduct.DisplayStockQuantity = Convert.ToByte(taskMapping.DisplayStockQuantity.Value);
			}
			if (taskMapping.DeliveryDayId.HasValue)
			{
				nopProduct.DeliveryDateId = taskMapping.DeliveryDayId.Value;
			}
			if (taskMapping.ManageInventoryMethodId.HasValue)
			{
				nopProduct.ManageInventoryMethodId = taskMapping.ManageInventoryMethodId.Value;
			}
			if (taskMapping.BackorderModeId.HasValue)
			{
				nopProduct.BackorderModeId = taskMapping.BackorderModeId.Value;
			}
			if (taskMapping.LowStockActivityId.HasValue)
			{
				nopProduct.LowStockActivityId = taskMapping.LowStockActivityId.Value;
			}
			if (taskMapping.WarehouseId.HasValue)
			{
				nopProduct.WarehouseId = taskMapping.WarehouseId.Value;
			}
			if (taskMapping.ProductTemplateId != 9989)
			{
				if (taskMapping.ProductTemplateId.HasValue)
				{
					nopProduct.ProductTemplateId = taskMapping.ProductTemplateId.Value;
					if (taskMapping.ProductTemplateId == 2)
					{
						nopProduct.ProductTypeId = 10;
					}
				}
			}
			else
			{
				nopProduct.LeaveProductType = true;
			}
			if (!string.IsNullOrEmpty(taskMapping.LimitedToStores))
			{
				nopProduct.LimitedToStores = true;
				list = taskMapping.LimitedToStores.Split(new char[1]
				{
					';'
				}, StringSplitOptions.RemoveEmptyEntries).ToList();
				list2 = taskMapping.LimitedToStoresEntities.Split(new char[1]
				{
					';'
				}, StringSplitOptions.RemoveEmptyEntries).ToList();
			}
			if (sourceMapping.VendorIsId)
			{
				nopProduct.VendorId = GlobalClass.StringToInteger(taskMapping.VendorVal);
			}
			else
			{
				string searchValue = taskMapping.VendorAction ? item.Vendor : string.Empty;
				nopProduct.VendorId = nopManager.GetVendor(searchValue);
			}
			if (taskMapping.ProdSettingsDeliveryDateAction)
			{
				if (sourceMapping.DeliveryDateIsId)
				{
					nopProduct.DeliveryDateId = GlobalClass.StringToInteger(item.ProdSettingsDeliveryDate, taskMapping.DeliveryDayId.GetValueOrDefault());
				}
				else
				{
					int deliveryDate = nopManager.GetDeliveryDate(item.ProdSettingsDeliveryDate);
					nopProduct.DeliveryDateId = ((deliveryDate > 0) ? deliveryDate : taskMapping.DeliveryDayId.GetValueOrDefault());
				}
			}
			if (initialData && productsInStore != null && productsInStore.Count > 0)
			{
				string tempSku = nopProduct.Sku.ToString().ToLower().Trim();
				if (productsInStore.ContainsValue(tempSku))
				{
					productId = GlobalClass.StringToInteger(productsInStore.Where((KeyValuePair<int, string> x) => x.Value == tempSku).FirstOrDefault().Key);
				}
			}
			else
			{
				nopManager.SelectProduct(nopProduct.Sku.ToString(), nopProduct.VendorId, 0, out productId, out decimal _, out decimal _);
			}
			if (childProducts && productId == 0 && (taskMapping.TaskAction == 0 || taskMapping.TaskAction == 2))
			{
				AddSpecAttributtesToFullDecription(taskMapping, item, nopProduct);
				productId = (nopProduct.ProductId = nopManager.InsertProduct(nopProduct));
				if (initialData && !productsInStore.ContainsKey(productId))
				{
					productsInStore.Add(productId, nopProduct.Sku.ToString().ToLower().Trim());
				}
				string value = string.IsNullOrEmpty(item.SearchPage) ? (nopProduct.Name + "-" + productId) : item.SearchPage;
				nopManager.InsertUpdateUrlRecord("Product", productId, ReplaceKeyParams(productId, value));
				duration_pictures_import += InsertUpdateImages(taskId, sourceMapping, taskMapping, item, nopManager, productId, newItem, nopProduct, errorList, picturesInStore, picturesSeoInStore);
				duration_product_attributtes_import += InsertUpdateProductAttributes(taskId, sourceMapping, taskMapping, item, nopManager, nopProduct, storeData, newItem, errorList, picturesInStore, picturesSeoInStore);
				duration_spec_attributtes_import += InsertUpdateSpecAttributes(taskId, sourceMapping, taskMapping, item, nopManager, nopProduct, newItem, errorList, specAttributeList, specAttributeOptionList, specAttributeProductList);
				duration_categories_import += InsertUpdateCategories(taskId, sourceMapping, taskMapping, item, nopManager, nopProduct, list, list2, newItem, errorList);
				duration_manufactures_import += InsertUpdateManufacturer(taskId, sourceMapping, taskMapping, item, nopManager, nopProduct, list, list2, newItem, errorList);
				duration_tier_pricing_import += InsertUpdateTirPricing(taskId, sourceMapping, taskMapping, item, nopManager, nopProduct, list, list2, errorList, newItem, null);
				duration_product_tags_import += InsertUpdateProductTags(taskId, sourceMapping, taskMapping, item, nopManager, nopProduct, newItem, errorList);
				duration_crossell_import += InsertUpdateProductCrossSells(taskId, sourceMapping, taskMapping, item, nopManager, nopProduct, newItem, errorList, productsInStore);
				duration_related_import += InsertUpdateProductRelated(taskId, sourceMapping, taskMapping, item, nopManager, nopProduct, newItem, errorList, productsInStore);
				if (taskMapping.CustomerRoleAction || taskMapping.CustomerRole1Action || taskMapping.CustomerRole2Action || taskMapping.CustomerRole3Action || taskMapping.CustomerRole4Action)
				{
					nopManager.DeleteAclMappings("product", productId);
					foreach (SourceItem.CustomerRuleItem customerRole3 in item.CustomerRoles)
					{
						if (!string.IsNullOrEmpty(customerRole3.CustomerRule))
						{
							int customerRole = nopManager.GetCustomerRole(customerRole3.CustomerRule);
							nopManager.InsertAclMappingRecord("product", productId, customerRole);
						}
					}
				}
				if (list2.Contains("1") && nopProduct.LimitedToStores)
				{
					foreach (string item2 in list)
					{
						if (GlobalClass.StringToInteger(item2) > 0)
						{
							nopManager.GetStoreMapping(productId, "Product", GlobalClass.StringToInteger(item2));
						}
					}
				}
			}
			else if (productId > 0 && ((taskMapping.TaskAction == 0 && !newItem) || taskMapping.TaskAction == 1 || taskMapping.TaskAction == 2))
			{
				nopProduct.ProductId = productId;
				AddSpecAttributtesToFullDecription(taskMapping, item, nopProduct);
				nopManager.UpdateProduct(nopProduct, taskMapping);
				if (initialData && !productsInStore.ContainsKey(productId))
				{
					productsInStore.Add(productId, nopProduct.Sku.ToString().ToLower().Trim());
				}
				if (taskMapping.SeoPageAction)
				{
					string value2 = string.IsNullOrEmpty(item.SearchPage) ? (nopProduct.Name + "-" + productId) : item.SearchPage;
					nopManager.InsertUpdateUrlRecord("Product", productId, ReplaceKeyParams(productId, value2));
				}
				duration_pictures_import += InsertUpdateImages(taskId, sourceMapping, taskMapping, item, nopManager, productId, newItem, nopProduct, errorList, picturesInStore, picturesSeoInStore);
				duration_product_attributtes_import += InsertUpdateProductAttributes(taskId, sourceMapping, taskMapping, item, nopManager, nopProduct, storeData, newItem, errorList, picturesInStore, picturesSeoInStore);
				duration_spec_attributtes_import += InsertUpdateSpecAttributes(taskId, sourceMapping, taskMapping, item, nopManager, nopProduct, newItem, errorList, specAttributeList, specAttributeOptionList, specAttributeProductList);
				duration_categories_import += InsertUpdateCategories(taskId, sourceMapping, taskMapping, item, nopManager, nopProduct, list, list2, newItem, errorList);
				duration_manufactures_import += InsertUpdateManufacturer(taskId, sourceMapping, taskMapping, item, nopManager, nopProduct, list, list2, newItem, errorList);
				duration_tier_pricing_import += InsertUpdateTirPricing(taskId, sourceMapping, taskMapping, item, nopManager, nopProduct, list, list2, errorList, newItem, null);
				duration_product_tags_import += InsertUpdateProductTags(taskId, sourceMapping, taskMapping, item, nopManager, nopProduct, newItem, errorList);
				duration_crossell_import += InsertUpdateProductCrossSells(taskId, sourceMapping, taskMapping, item, nopManager, nopProduct, newItem, errorList, productsInStore);
				duration_related_import += InsertUpdateProductRelated(taskId, sourceMapping, taskMapping, item, nopManager, nopProduct, newItem, errorList, productsInStore);
				if (taskMapping.CustomerRoleAction || taskMapping.CustomerRole1Action || taskMapping.CustomerRole2Action || taskMapping.CustomerRole3Action || taskMapping.CustomerRole4Action)
				{
					nopManager.DeleteAclMappings("product", productId);
					foreach (SourceItem.CustomerRuleItem customerRole4 in item.CustomerRoles)
					{
						if (!string.IsNullOrEmpty(customerRole4.CustomerRule))
						{
							int customerRole2 = nopManager.GetCustomerRole(customerRole4.CustomerRule);
							nopManager.InsertAclMappingRecord("product", productId, customerRole2);
						}
					}
				}
				if (list2.Contains("1"))
				{
					nopManager.DeleteStoreMapping(productId, "Product");
					if (nopProduct.LimitedToStores)
					{
						foreach (string item3 in list)
						{
							if (GlobalClass.StringToInteger(item3) > 0)
							{
								nopManager.GetStoreMapping(productId, "Product", GlobalClass.StringToInteger(item3));
							}
						}
					}
				}
			}
			return productId;
		}

		public void StartTaskUserImport(string sourceAddress, StoreData storeData, int taskId, bool scheduler, out string resultMessage, out long insertedItems, out long updatedItems, TaskMapping taskMapping = null)
		{
			bool flag = false;
			resultMessage = "";
			long foundItems = 0L;
			long num = 0L;
			insertedItems = 0L;
			updatedItems = 0L;
			int num2 = 0;
			long num3 = 0L;
			long num4 = 0L;
			long num5 = 0L;
			SourceMappingFunc sourceMappingFunc = new SourceMappingFunc();
			TaskMappingFunc taskMappingFunc = new TaskMappingFunc();
			NopManager nopManager = null;
			StringBuilder stringBuilder = new StringBuilder();
			StringBuilder stringBuilder2 = new StringBuilder();
			List<string> list = new List<string>();
			try
			{
				LogManager.LogWrite(taskId, "Task started.", LogType.Info);
				stringBuilder2.AppendLine("Task started: " + DateTime.Now.ToString());
				if (taskMapping == null)
				{
					taskMapping = taskMappingFunc.GetTaskMapping(taskId);
				}
				SourceDocument sourceDocument = null;
				SourceData sourceData = sourceMappingFunc.GetSourceData(taskMapping.VendorSourceId);
				byte[] fileContent = sourceMappingFunc.GetFileContent((SourceType)sourceData.SourceType, sourceData.SourceUser, sourceData.SourcePassword, string.IsNullOrEmpty(sourceAddress) ? sourceData.SourceAddress : sourceAddress, sourceData.SoapAction, sourceData.SoapRequest, sourceData.SourceEncoding, sourceData.SourceFormat == 2, sourceData.SourceContent, sourceData.AuthHeader);
				if (storeData == null)
				{
					storeData = StoreFunc.GetStoreData(taskMapping.EShopId.Value);
				}
				SourceMapping sourceMapping = sourceMappingFunc.GetSourceMapping(taskMapping.VendorSourceId);
				sourceMapping.CsvDelimeter = sourceData.CsvDelimeter;
				sourceMapping.FirstRowIndex = sourceData.FirstRowIndex;
				nopManager = new NopManager(storeData.DatabaseConnString, storeData.Version, storeData.DatabaseType);
				if (storeData.DatabaseType == DatabaseType.MSSQL && !mssql)
				{
					LogManager.LogWrite(taskId, "Task import stopped. NopTalk Pro version does not have MSSQL licence. Please contact info@vasksoft.com", LogType.Warning);
				}
				else
				{
					if (storeData.DatabaseType != DatabaseType.MySQL || mysql)
					{
						if (taskMapping.DisableShop)
						{
							nopManager.CloseStore(disableStore: true);
						}
						if (fileContent != null)
						{
							sourceDocument = ((sourceData.SourceType != 4) ? new SourceDocument((FileFormat)sourceData.SourceFormat, fileContent, sourceMapping, taskMapping, sourceData) : new SourceDocument(FileFormat.AliExpress, fileContent, sourceMapping, taskMapping, sourceData));
							SourceDocument sourceDoucumentForTasks = taskMappingFunc.GetSourceDoucumentForTasks(sourceDocument, taskMapping);
							foundItems = sourceDoucumentForTasks.SourceItems.Count();
							if (taskMapping.TaskAction == 0 || taskMapping.TaskAction == 1 || taskMapping.TaskAction == 2)
							{
								RunCustomSql(taskId, taskMapping, nopManager, 1, stringBuilder);
								taskMappingFunc.UpdateTaskProgress(taskId, foundItems, num);
								List<string> list2 = new List<string>();
								string text = "";
								bool flag2 = true;
								foreach (SourceItem sourceItem in sourceDoucumentForTasks.SourceItems)
								{
									SourceItem.CustomerItem user = sourceItem.User;
									flag = false;
									num4++;
									num5++;
									num2 = 0;
									try
									{
										try
										{
											if (num4 >= 5)
											{
												if (!taskMappingFunc.IsTaskRunning(taskId))
												{
													goto IL_1421;
												}
												taskMappingFunc.UpdateTaskProgress(taskId, foundItems, num);
												num4 = 0L;
											}
										}
										catch
										{
										}
										NopCustomer nopCustomer = new NopCustomer();
										flag2 = true;
										nopCustomer.Username = user.Username;
										nopCustomer.Email = user.Email;
										nopCustomer.Active = user.Active;
										nopCustomer.DayOfBirth = user.BirthDay;
										nopCustomer.FirstName = user.FirstName;
										nopCustomer.LastName = user.LastName;
										nopCustomer.Gender = user.Gender;
										nopCustomer.IsTaxExempt = user.IsTaxExempt;
										nopCustomer.Newsletter = user.Newsletter;
										nopCustomer.SAPCustomer = user.CustomerOthersItem.SAPCustomer;
										nopCustomer.GPCustomer = user.CustomerOthersItem.GPCustomer;
										nopCustomer.VendorId = ((user.ManagerOfVendor == null) ? null : new int?(nopManager.GetVendor(user.ManagerOfVendor)));
										NopAddress nopAddress = new NopAddress();
										nopAddress.Company = user.CustomerMainAddress.Company;
										nopAddress.City = user.CustomerMainAddress.City;
										nopAddress.County = user.CustomerMainAddress.County;
										nopAddress.Address1 = user.CustomerMainAddress.Address1;
										nopAddress.Address2 = user.CustomerMainAddress.Address2;
										nopAddress.Address3 = user.CustomerOthersItem.Address3;
										nopAddress.ZipPostalCode = user.CustomerMainAddress.ZipPostalCode;
										nopAddress.PhoneNumber = user.CustomerMainAddress.Phone;
										nopAddress.FaxNumber = user.CustomerMainAddress.Fax;
										nopAddress.CountryId = nopManager.SelectCountry(user.CustomerMainAddress.Country);
										nopAddress.StateProvinceId = nopManager.SelectState(user.CustomerMainAddress.State);
										if (!string.IsNullOrEmpty(taskMapping.LimitedToStores))
										{
											list2 = taskMapping.LimitedToStores.Split(new char[1]
											{
												';'
											}, StringSplitOptions.RemoveEmptyEntries).ToList();
											nopCustomer.LimitedToStores = true;
											nopCustomer.RegisteredInStoreId = GlobalClass.StringToInteger(list2.FirstOrDefault());
										}
										if (sourceData.UserIdInStore == 0 && string.IsNullOrEmpty(nopCustomer.Email))
										{
											LogManager.LogWrite(taskId, "'User Identifier In Store' is UserEmail, but UserEmail is empty or not mapped", LogType.Warning);
										}
										else if (sourceData.UserIdInStore == 1 && string.IsNullOrEmpty(nopCustomer.Username))
										{
											LogManager.LogWrite(taskId, "'User Identifier In Store' is UserName, but UserName is empty or not mapped", LogType.Warning);
										}
										else if (sourceData.UserIdInStore == 2 && string.IsNullOrEmpty(nopCustomer.Email) && string.IsNullOrEmpty(nopCustomer.Username))
										{
											LogManager.LogWrite(taskId, "'User Identifier In Store' is UserEmail&&UserName, but UserName&&UserEmail is empty or not mapped", LogType.Warning);
										}
										else if (sourceData.UserIdInStore == 3 && string.IsNullOrEmpty(nopCustomer.SAPCustomer))
										{
											LogManager.LogWrite(taskId, "'User Identifier In Store' is SAPCustomer, but SAPCustomer is empty or not mapped", LogType.Warning);
										}
										else if (sourceData.UserIdInStore < 0)
										{
											LogManager.LogWrite(taskId, "'User Identifier In Store' is not selected", LogType.Warning);
										}
										else
										{
											if (sourceData.UserIdInStore == 3)
											{
												taskMapping.TaskUserMapping.SAPCustomerAction = true;
											}
											int billingAddressId = 0;
											int shippingAddressId = 0;
											nopManager.SelectCustomer(sourceData.UserIdInStore, nopCustomer.Username, nopCustomer.Email, nopCustomer.SAPCustomer, out num2, out billingAddressId, out shippingAddressId);
											if (num2 == 0 && (taskMapping.TaskAction == 0 || taskMapping.TaskAction == 2))
											{
												if (sourceData.UserIdInStore == 1)
												{
													list.Add(nopCustomer.Username.ToString());
												}
												else if (sourceData.UserIdInStore == 2)
												{
													list.Add(nopCustomer.Email.ToString() + "_" + nopCustomer.Username.ToString());
												}
												else if (sourceData.UserIdInStore == 3)
												{
													list.Add(nopCustomer.SAPCustomer.ToString());
												}
												else
												{
													list.Add(nopCustomer.Email.ToString());
												}
												flag = true;
												int? billingAddress_Id = null;
												int? shippingAddress_Id = null;
												List<int> list3 = new List<int>();
												foreach (SourceItem.CustomerAddressItem customerAddressItem in user.CustomerAddressItems)
												{
													NopAddress nopAddress2 = new NopAddress();
													nopAddress2.AddressName = user.CustomerOthersItem.AddressName;
													nopAddress2.SAPCustomer = user.CustomerOthersItem.AddressSAPCustomer;
													nopAddress2.MainAddress = user.CustomerOthersItem.MainAddress;
													nopAddress2.FirstName = customerAddressItem.FirstName;
													nopAddress2.LastName = customerAddressItem.LastName;
													nopAddress2.Email = customerAddressItem.Email;
													nopAddress2.Company = customerAddressItem.Company;
													nopAddress2.City = customerAddressItem.City;
													nopAddress2.County = customerAddressItem.County;
													nopAddress2.Address1 = customerAddressItem.Address1;
													nopAddress2.Address2 = customerAddressItem.Address2;
													nopAddress2.Address3 = user.CustomerOthersItem.Address3;
													nopAddress2.ZipPostalCode = customerAddressItem.ZipPostalCode;
													nopAddress2.PhoneNumber = customerAddressItem.Phone;
													nopAddress2.FaxNumber = customerAddressItem.Fax;
													nopAddress2.CountryId = nopManager.SelectCountry(customerAddressItem.Country);
													nopAddress2.StateProvinceId = nopManager.SelectState(customerAddressItem.State);
													int num6 = nopManager.InsertAddress(nopAddress2);
													list3.Add(num6);
													if (customerAddressItem.Type?.ToString().ToUpper() == "S")
													{
														shippingAddress_Id = num6;
													}
													else if (customerAddressItem.Type?.ToString().ToUpper() == "B")
													{
														billingAddress_Id = num6;
													}
													else
													{
														if (shippingAddress_Id.GetValueOrDefault() == 0)
														{
															shippingAddress_Id = num6;
														}
														if (billingAddress_Id.GetValueOrDefault() == 0)
														{
															billingAddress_Id = num6;
														}
													}
												}
												nopCustomer.ShippingAddress_Id = shippingAddress_Id;
												nopCustomer.BillingAddress_Id = billingAddress_Id;
												num2 = nopManager.InsertCustomer(nopCustomer);
												foreach (int item in list3)
												{
													nopManager.DeleteCustomerAddressMapping(num2, item);
													nopManager.InsertCustomerAddressMapping(num2, item);
												}
												nopCustomer.Id = num2;
												InsertUpdateCustomerGenericAttributes(taskId, sourceMapping, sourceItem, nopManager, nopCustomer, nopAddress, flag2, stringBuilder);
												InsertUpdateCustomerCustomAttributes(taskId, sourceMapping, taskMapping, sourceItem, nopManager, nopCustomer, nopAddress, flag2, stringBuilder);
												InsertUpdateAddressCustomAttributes(taskId, sourceMapping, taskMapping, sourceItem, nopManager, nopCustomer, nopAddress, flag2, stringBuilder);
												if (sourceItem.User.CustomerRoleItems != null && sourceItem.User.CustomerRoleItems.Any())
												{
													nopManager.DeleteCustomerRoleMapping(num2);
													foreach (SourceItem.CustomerRuleItem customerRoleItem in sourceItem.User.CustomerRoleItems)
													{
														if (!string.IsNullOrEmpty(customerRoleItem.CustomerRule))
														{
															int customerRole = nopManager.GetCustomerRole(customerRoleItem.CustomerRule);
															nopManager.DeleteCustomerRoleMapping(num2, customerRole);
															nopManager.InsertCustomerRoleMapping(num2, customerRole);
														}
													}
												}
												nopManager.InsertCustomerPassword(num2, user.Password);
												if (nopCustomer.LimitedToStores && nopCustomer.Newsletter == true)
												{
													foreach (string item2 in list2)
													{
														if (GlobalClass.StringToInteger(item2) > 0)
														{
															nopManager.GetNewsLetterSubscription(nopCustomer.Email, item2);
														}
													}
												}
												num++;
												insertedItems++;
											}
											else if (num2 > 0 && (taskMapping.TaskAction == 1 || taskMapping.TaskAction == 2))
											{
												nopCustomer.Id = num2;
												if (sourceData.UserIdInStore == 1 && list.Contains(nopCustomer.Username.ToString()))
												{
													flag2 = false;
												}
												else if (sourceData.UserIdInStore == 2 && list.Contains(nopCustomer.Email.ToString() + "_" + nopCustomer.Username.ToString()))
												{
													flag2 = false;
												}
												else if (sourceData.UserIdInStore == 3 && list.Contains(nopCustomer.SAPCustomer.ToString()))
												{
													flag2 = false;
												}
												else if (sourceData.UserIdInStore == 0 && list.Contains(nopCustomer.Email.ToString()))
												{
													flag2 = false;
												}
												else
												{
													flag2 = true;
													if (sourceData.UserIdInStore == 1)
													{
														list.Add(nopCustomer.Username.ToString());
													}
													else if (sourceData.UserIdInStore == 2)
													{
														list.Add(nopCustomer.Email.ToString() + "_" + nopCustomer.Username.ToString());
													}
													else if (sourceData.UserIdInStore == 3)
													{
														list.Add(nopCustomer.SAPCustomer.ToString());
													}
													else
													{
														list.Add(nopCustomer.Email.ToString());
													}
												}
												flag = true;
												int? billingAddress_Id2 = null;
												int? shippingAddress_Id2 = null;
												List<int> list4 = new List<int>();
												foreach (SourceItem.CustomerAddressItem customerAddressItem2 in user.CustomerAddressItems)
												{
													int num7 = 0;
													NopAddress nopAddress3 = new NopAddress();
													nopAddress3.AddressName = user.CustomerOthersItem.AddressName;
													nopAddress3.SAPCustomer = user.CustomerOthersItem.AddressSAPCustomer;
													nopAddress3.MainAddress = user.CustomerOthersItem.MainAddress;
													nopAddress3.FirstName = customerAddressItem2.FirstName;
													nopAddress3.LastName = customerAddressItem2.LastName;
													nopAddress3.Email = customerAddressItem2.Email;
													nopAddress3.Company = customerAddressItem2.Company;
													nopAddress3.City = customerAddressItem2.City;
													nopAddress3.County = customerAddressItem2.County;
													nopAddress3.Address1 = customerAddressItem2.Address1;
													nopAddress3.Address2 = customerAddressItem2.Address2;
													nopAddress3.Address3 = user.CustomerOthersItem.Address3;
													nopAddress3.ZipPostalCode = customerAddressItem2.ZipPostalCode;
													nopAddress3.PhoneNumber = customerAddressItem2.Phone;
													nopAddress3.FaxNumber = customerAddressItem2.Fax;
													nopAddress3.CountryId = nopManager.SelectCountry(customerAddressItem2.Country);
													nopAddress3.StateProvinceId = nopManager.SelectState(customerAddressItem2.State);
													num7 = ((customerAddressItem2.Type?.ToString().ToUpper() == "S" && shippingAddressId > 0 && taskMapping.ForceShippingAddressOnUpdate) ? shippingAddressId : ((!(customerAddressItem2.Type?.ToString().ToUpper() == "B") || billingAddressId <= 0 || !taskMapping.ForceBillingAddressOnUpdate) ? nopManager.SelectAddress(customerAddressItem2.Email) : billingAddressId));
													if (num7 == 0)
													{
														num7 = nopManager.InsertAddress(nopAddress3);
														nopAddress3.Id = num7;
														if (!flag2)
														{
															insertedItems++;
															num++;
														}
													}
													else
													{
														nopAddress3.Id = num7;
														nopManager.UpdateAddress(nopAddress3);
														if (!flag2)
														{
															updatedItems++;
															num++;
														}
													}
													list4.Add(num7);
													if (customerAddressItem2.Type?.ToString().ToUpper() == "S")
													{
														shippingAddress_Id2 = num7;
													}
													else if (customerAddressItem2.Type?.ToString().ToUpper() == "B")
													{
														billingAddress_Id2 = num7;
													}
													else
													{
														if (shippingAddress_Id2.GetValueOrDefault() == 0)
														{
															shippingAddress_Id2 = num7;
														}
														if (billingAddress_Id2.GetValueOrDefault() == 0)
														{
															billingAddress_Id2 = num7;
														}
													}
												}
												nopCustomer.ShippingAddress_Id = shippingAddress_Id2;
												nopCustomer.BillingAddress_Id = billingAddress_Id2;
												nopManager.UpdateCustomer(nopCustomer, taskMapping);
												if (flag2 && taskMapping.DeleteOldCustomerAddresses)
												{
													nopManager.DeleteCustomerAddressMapping(num2);
												}
												foreach (int item3 in list4)
												{
													nopManager.DeleteCustomerAddressMapping(num2, item3);
													nopManager.InsertCustomerAddressMapping(num2, item3);
												}
												nopCustomer.Id = num2;
												if (flag2)
												{
													InsertUpdateCustomerGenericAttributes(taskId, sourceMapping, sourceItem, nopManager, nopCustomer, nopAddress, flag2, stringBuilder);
													InsertUpdateCustomerCustomAttributes(taskId, sourceMapping, taskMapping, sourceItem, nopManager, nopCustomer, nopAddress, flag2, stringBuilder);
												}
												InsertUpdateAddressCustomAttributes(taskId, sourceMapping, taskMapping, sourceItem, nopManager, nopCustomer, nopAddress, flag2, stringBuilder);
												if (sourceItem.User.CustomerRoleItems != null && sourceItem.User.CustomerRoleItems.Any())
												{
													if (taskMapping.DeleteOldCustomerRoles)
													{
														nopManager.DeleteCustomerRoleMapping(num2);
													}
													foreach (SourceItem.CustomerRuleItem customerRoleItem2 in sourceItem.User.CustomerRoleItems)
													{
														if (!string.IsNullOrEmpty(customerRoleItem2.CustomerRule))
														{
															int customerRole2 = nopManager.GetCustomerRole(customerRoleItem2.CustomerRule);
															nopManager.DeleteCustomerRoleMapping(num2, customerRole2);
															nopManager.InsertCustomerRoleMapping(num2, customerRole2);
														}
													}
												}
												if (taskMapping.TaskUserMapping.PasswordAction)
												{
													nopManager.DeleteCustomerPassword(num2);
													nopManager.InsertCustomerPassword(num2, user.Password);
												}
												if (nopCustomer.LimitedToStores && nopCustomer.Newsletter == true)
												{
													foreach (string item4 in list2)
													{
														if (GlobalClass.StringToInteger(item4) > 0)
														{
															nopManager.GetNewsLetterSubscription(nopAddress.Email, item4);
														}
													}
												}
												if (flag2)
												{
													num++;
													updatedItems++;
												}
											}
											num3++;
											if (((taskMapping.TaskAction == 1) ? num : num3) >= productsLimits && productsLimits > 0)
											{
												LogManager.LogWrite(taskId, "Task import stopped. NopTalk Pro version has customer import limit=" + productsLimits, LogType.Warning);
												goto IL_1421;
											}
										}
									}
									catch (Exception ex)
									{
										LogManager.LogWrite(taskId, "Item import unexpected error: " + ex.ToString(), LogType.Error);
										stringBuilder.AppendLine("Item import unexpected error: " + ex.ToString());
										if (num5 >= productsLimits && productsLimits > 0)
										{
											LogManager.LogWrite(taskId, "Task import stopped. NopTalk Pro version has customer import limit=" + productsLimits, LogType.Warning);
											goto IL_1421;
										}
									}
								}
								goto IL_1421;
							}
							LogManager.LogWrite(taskId, "Task action (insert/update) not found.", LogType.Warning);
						}
						else
						{
							LogManager.LogWrite(taskId, "The source file is empty.", LogType.Warning);
						}
						goto IL_1468;
					}
					LogManager.LogWrite(taskId, "Task import stopped. NopTalk Pro version does not have MySQL licence. Please contact info@vasksoft.com", LogType.Warning);
				}
				goto end_IL_004f;
				IL_1468:
				try
				{
					if (taskMapping.DeleteSourceFile && sourceData.SourceType == 0)
					{
						File.Delete(sourceData.SourceAddress);
					}
				}
				catch (Exception ex2)
				{
					LogManager.LogWrite(taskId, "Error deleting file:" + ex2.ToString(), LogType.Error);
				}
				if (taskMapping.DisableShop)
				{
					nopManager.CloseStore(disableStore: false);
				}
				goto end_IL_004f;
				IL_1421:
				RunCustomSql(taskId, taskMapping, nopManager, 2, stringBuilder);
				goto IL_1468;
				end_IL_004f:;
			}
			catch (Exception ex3)
			{
				LogManager.LogWrite(taskId, "Task import unexpected error: " + ex3.ToString(), LogType.Error);
				stringBuilder.AppendLine("Task import unexpected error: " + ex3.ToString());
			}
			finally
			{
				try
				{
					taskMappingFunc.UpdateTaskData(taskId, foundItems, num, DateTime.Now);
				}
				catch
				{
				}
				resultMessage = string.Format("Task finished. Result of import. Items found: " + foundItems + ". Total items imported: {0}. Inserted:{1}, Updated:{2}", num, insertedItems, updatedItems);
				LogManager.LogWrite(taskId, resultMessage, LogType.Info);
				stringBuilder2.AppendLine("Task finished: " + DateTime.Now.ToString());
				stringBuilder2.AppendLine(string.Format("Result of import: Items found: " + foundItems + ". Total items imported: {0}. Inserted:{1}, Updated:{2}", num, insertedItems, updatedItems));
				try
				{
					SendLogNotification(taskId, taskMapping, stringBuilder2, stringBuilder);
				}
				catch
				{
				}
			}
		}

		public int ImportItem(SourceItem item, StoreData storeData, TaskMapping taskMapping, SourceMapping sourceMapping)
		{
			StringBuilder errorList = new StringBuilder();
			Dictionary<string, int> picturesInStore = new Dictionary<string, int>();
			Dictionary<string, int> picturesSeoInStore = new Dictionary<string, int>();
			NopManager nopManager = new NopManager(storeData.DatabaseConnString, storeData.Version, storeData.DatabaseType);
			if (storeData.DatabaseType == DatabaseType.MSSQL && !mssql)
			{
				LogManager.LogWrite(null, "Task import stopped. NopTalk Pro version does not have MSSQL licence. Please contact info@vasksoft.com", LogType.Warning);
				return 0;
			}
			if (storeData.DatabaseType == DatabaseType.MySQL && !mysql)
			{
				LogManager.LogWrite(null, "Task import stopped. NopTalk Pro version does not have MySQL licence. Please contact info@vasksoft.com", LogType.Warning);
				return 0;
			}
			NopProduct nopProduct = new NopProduct();
			int productId = 0;
			nopProduct.Sku = item.ProdSku;
			nopProduct.Name = item.ProdName;
			nopProduct.ShortDescription = item.ProdShortDesc;
			nopProduct.FullDescription = item.ProdFullDesc;
			nopProduct.ManufacturerPartNumber = item.ProdManPartNum;
			nopProduct.ProductCost = item.ProdCost.GetValueOrDefault();
			nopProduct.OldPrice = item.ProdOldPrice.GetValueOrDefault();
			nopProduct.Price = item.ProdPrice.GetValueOrDefault();
			nopProduct.Price = RoundDecimal(taskMapping.ProdPriceVal, nopProduct.Price);
			nopProduct.StockQuantity = item.ProdStock.GetValueOrDefault();
			nopProduct.AdminComment = "NopTalk manuall import";
			nopProduct.MetaKeywords = item.SeoMetaKey;
			nopProduct.MetaDescription = item.SeoMetaDesc;
			nopProduct.MetaTitle = item.SeoMetaTitle;
			nopProduct.TaxCategoryId = nopManager.GetTaxCategory(item.TaxCategory);
			nopProduct.Gtin = item.Gtin;
			nopProduct.Weight = item.Weight.GetValueOrDefault();
			nopProduct.Length = item.Length.GetValueOrDefault();
			nopProduct.Width = item.Width.GetValueOrDefault();
			nopProduct.Height = item.Height.GetValueOrDefault();
			nopProduct.HasTierPrices = (byte)((item.TierPricingItems != null && item.TierPricingItems.Any()) ? 1 : 0);
			nopProduct.AllowAddingOnlyExistingAttributeCombinations = (byte)((taskMapping.ManageInventoryMethodId == 2) ? 1 : 0);
			nopProduct.IsTaxExempt = Convert.ToByte(item.TaxExempt);
			nopProduct.BasepriceEnabled = Convert.ToByte(item.ProductPrice_BasepriceEnabled);
			nopProduct.BasepriceAmount = item.ProductPrice_BasepriceAmount;
			nopProduct.BasepriceBaseAmount = item.ProductPrice_BasepriceBaseAmount;
			nopProduct.BasepriceUnitId = nopManager.GetMeasureWeight(item.ProductPrice_BasepriceUnit);
			nopProduct.BasepriceBaseUnitId = nopManager.GetMeasureWeight(item.ProductPrice_BasepriceBaseUnit);
			nopProduct.AllowBackInStockSubscriptions = Convert.ToByte(item.ProductStock_AllowBackInStockSubscriptions);
			nopProduct.DisableBuyButton = Convert.ToByte(item.ProductStock_DisableBuyButton);
			Dictionary<string, int> specAttributeList = new Dictionary<string, int>();
			Dictionary<string, int> specAttributeOptionList = new Dictionary<string, int>();
			Dictionary<string, int> specAttributeProductList = new Dictionary<string, int>();
			if (taskMapping.ProdSettingsMinStockQtyAction)
			{
				nopProduct.MinStockQuantity = GlobalClass.StringToInteger(taskMapping.ProdSettingsMinStockQtyVal);
			}
			if (taskMapping.ProdSettingsNotifyQtyAction)
			{
				nopProduct.NotifyAdminForQuantityBelow = GlobalClass.StringToInteger(taskMapping.ProdSettingsNotifyQtyVal);
			}
			if (taskMapping.ProdSettingsMinCartQtyAction)
			{
				nopProduct.OrderMinimumQuantity = GlobalClass.StringToInteger(taskMapping.ProdSettingsMinCartQtyVal);
			}
			if (taskMapping.ProdSettingsMaxCartQtyAction)
			{
				nopProduct.OrderMaximumQuantity = GlobalClass.StringToInteger(taskMapping.ProdSettingsMaxCartQtyVal);
			}
			if (taskMapping.ProdSettingsAllowedQtyAction)
			{
				nopProduct.AllowedQuantities = ((GlobalClass.StringToInteger(taskMapping.ProdSettingsAllowedQtyVal) > -1) ? new int?(GlobalClass.StringToInteger(taskMapping.ProdSettingsAllowedQtyVal)) : null);
			}
			if (taskMapping.ProdSettingsShippingEnabledAction)
			{
				nopProduct.IsShipEnabled = Convert.ToByte(GlobalClass.ObjectToBool(taskMapping.ProdSettingsShippingEnabledVal));
			}
			if (taskMapping.ProdSettingsFreeShippingAction)
			{
				nopProduct.IsFreeShipping = Convert.ToByte(GlobalClass.ObjectToBool(taskMapping.ProdSettingsFreeShippingVal));
			}
			if (taskMapping.ProdSettingsShipSeparatelyAction)
			{
				nopProduct.ShipSeparately = Convert.ToByte(GlobalClass.ObjectToBool(taskMapping.ProdSettingsShipSeparatelyVal));
			}
			if (taskMapping.ProdSettingsShippingChargeAction)
			{
				nopProduct.AdditionalShippingCharge = GlobalClass.StringToDecimal(taskMapping.ProdSettingsShippingChargeVal);
			}
			if (taskMapping.ProdSettingsVisibleIndividuallyAction)
			{
				nopProduct.VisibleIndividually = Convert.ToByte(GlobalClass.ObjectToBool(taskMapping.ProdSettingsVisibleIndividuallyVal));
			}
			if (taskMapping.ProdSettingsIsDeletedAction)
			{
				nopProduct.Deleted = Convert.ToByte(GlobalClass.ObjectToBool(item.ProdSettingsIsDeleted));
			}
			nopProduct.SubjectToAcl = ((item.CustomerRoles.Any() && (taskMapping.CustomerRoleAction || taskMapping.CustomerRole1Action || taskMapping.CustomerRole2Action || taskMapping.CustomerRole3Action || taskMapping.CustomerRole4Action)) ? new bool?(true) : null);
			if (taskMapping.ProductPublish.HasValue)
			{
				nopProduct.Published = Convert.ToByte(taskMapping.ProductPublish.Value);
			}
			if (taskMapping.AllowCustomerReviews.HasValue)
			{
				nopProduct.AllowCustomerReviews = Convert.ToByte(taskMapping.AllowCustomerReviews.Value);
			}
			if (taskMapping.DisplayAvailability.HasValue)
			{
				nopProduct.DisplayStockAvailability = Convert.ToByte(taskMapping.DisplayAvailability.Value);
			}
			if (taskMapping.DisplayStockQuantity.HasValue)
			{
				nopProduct.DisplayStockQuantity = Convert.ToByte(taskMapping.DisplayStockQuantity.Value);
			}
			if (taskMapping.DeliveryDayId.HasValue)
			{
				nopProduct.DeliveryDateId = taskMapping.DeliveryDayId.Value;
			}
			if (taskMapping.ManageInventoryMethodId.HasValue)
			{
				nopProduct.ManageInventoryMethodId = taskMapping.ManageInventoryMethodId.Value;
			}
			if (taskMapping.BackorderModeId.HasValue)
			{
				nopProduct.BackorderModeId = taskMapping.BackorderModeId.Value;
			}
			if (taskMapping.LowStockActivityId.HasValue)
			{
				nopProduct.LowStockActivityId = taskMapping.LowStockActivityId.Value;
			}
			if (taskMapping.WarehouseId.HasValue)
			{
				nopProduct.WarehouseId = taskMapping.WarehouseId.Value;
			}
			if (taskMapping.ProductTemplateId != 9989)
			{
				if (taskMapping.ProductTemplateId.HasValue)
				{
					nopProduct.ProductTemplateId = taskMapping.ProductTemplateId.Value;
					if (taskMapping.ProductTemplateId == 2)
					{
						nopProduct.ProductTypeId = 10;
					}
				}
			}
			else
			{
				nopProduct.LeaveProductType = true;
			}
			if (!string.IsNullOrEmpty(taskMapping.LimitedToStores))
			{
				nopProduct.LimitedToStores = true;
			}
			if (sourceMapping.VendorIsId)
			{
				nopProduct.VendorId = GlobalClass.StringToInteger(taskMapping.VendorVal);
			}
			else
			{
				string searchValue = taskMapping.VendorAction ? item.Vendor : string.Empty;
				nopProduct.VendorId = nopManager.GetVendor(searchValue);
			}
			if (taskMapping.ProdSettingsDeliveryDateAction)
			{
				if (sourceMapping.DeliveryDateIsId)
				{
					nopProduct.DeliveryDateId = GlobalClass.StringToInteger(item.ProdSettingsDeliveryDate, taskMapping.DeliveryDayId.GetValueOrDefault());
				}
				else
				{
					int deliveryDate = nopManager.GetDeliveryDate(item.ProdSettingsDeliveryDate);
					nopProduct.DeliveryDateId = ((deliveryDate > 0) ? deliveryDate : taskMapping.DeliveryDayId.GetValueOrDefault());
				}
			}
			nopManager.SelectProduct(nopProduct.Sku.ToString(), nopProduct.VendorId, 0, out productId, out decimal _, out decimal _);
			if (productId == 0 && (taskMapping.TaskAction == 0 || taskMapping.TaskAction == 2))
			{
				AddSpecAttributtesToFullDecription(taskMapping, item, nopProduct);
				productId = (nopProduct.ProductId = nopManager.InsertProduct(nopProduct));
				string slug = string.IsNullOrEmpty(item.SearchPage) ? (nopProduct.Name + "-" + productId) : item.SearchPage;
				nopManager.InsertUpdateUrlRecord("Product", productId, slug);
				InsertUpdateProductTags(null, sourceMapping, taskMapping, item, nopManager, nopProduct, newItem: true, errorList);
				InsertUpdateImages(null, sourceMapping, taskMapping, item, nopManager, productId, newItem: true, nopProduct, errorList, picturesInStore, picturesSeoInStore);
				InsertUpdateCategories(null, sourceMapping, taskMapping, item, nopManager, nopProduct, new List<string>(), new List<string>(), newItem: true, errorList);
				InsertUpdateManufacturer(null, sourceMapping, taskMapping, item, nopManager, nopProduct, new List<string>(), new List<string>(), newItem: true, errorList);
				InsertUpdateProductAttributes(null, sourceMapping, taskMapping, item, nopManager, nopProduct, storeData, newItem: true, errorList, picturesInStore, picturesSeoInStore);
				InsertUpdateSpecAttributes(null, sourceMapping, taskMapping, item, nopManager, nopProduct, newItem: true, errorList, specAttributeList, specAttributeOptionList, specAttributeProductList);
				if (taskMapping.CustomerRoleAction || taskMapping.CustomerRole1Action || taskMapping.CustomerRole2Action || taskMapping.CustomerRole3Action || taskMapping.CustomerRole4Action)
				{
					nopManager.DeleteAclMappings("product", productId);
					foreach (SourceItem.CustomerRuleItem customerRole3 in item.CustomerRoles)
					{
						if (!string.IsNullOrEmpty(customerRole3.CustomerRule))
						{
							int customerRole = nopManager.GetCustomerRole(customerRole3.CustomerRule);
							nopManager.InsertAclMappingRecord("product", productId, customerRole);
						}
					}
				}
				if (nopProduct.LimitedToStores)
				{
					List<string> list = taskMapping.LimitedToStores.Split(new char[1]
					{
						';'
					}, StringSplitOptions.RemoveEmptyEntries).ToList();
					foreach (string item2 in list)
					{
						if (GlobalClass.StringToInteger(item2) > 0)
						{
							nopManager.GetStoreMapping(productId, "Product", GlobalClass.StringToInteger(item2));
						}
					}
				}
			}
			else if (productId > 0 && (taskMapping.TaskAction == 1 || taskMapping.TaskAction == 2))
			{
				nopProduct.ProductId = productId;
				AddSpecAttributtesToFullDecription(taskMapping, item, nopProduct);
				nopManager.UpdateProduct(nopProduct, taskMapping);
				if (taskMapping.SeoPageAction)
				{
					string value = string.IsNullOrEmpty(item.SearchPage) ? (nopProduct.Name + "-" + productId) : item.SearchPage;
					nopManager.InsertUpdateUrlRecord("Product", productId, ReplaceKeyParams(productId, value));
				}
				InsertUpdateProductTags(null, sourceMapping, taskMapping, item, nopManager, nopProduct, newItem: true, errorList);
				InsertUpdateImages(null, sourceMapping, taskMapping, item, nopManager, productId, newItem: true, nopProduct, errorList, picturesInStore, picturesSeoInStore);
				InsertUpdateProductAttributes(null, sourceMapping, taskMapping, item, nopManager, nopProduct, storeData, newItem: true, errorList, picturesInStore, picturesSeoInStore);
				InsertUpdateSpecAttributes(null, sourceMapping, taskMapping, item, nopManager, nopProduct, newItem: true, errorList, specAttributeList, specAttributeOptionList, specAttributeProductList);
				InsertUpdateCategories(null, sourceMapping, taskMapping, item, nopManager, nopProduct, new List<string>(), new List<string>(), newItem: true, errorList);
				InsertUpdateManufacturer(null, sourceMapping, taskMapping, item, nopManager, nopProduct, new List<string>(), new List<string>(), newItem: true, errorList);
				if (taskMapping.CustomerRoleAction || taskMapping.CustomerRole1Action || taskMapping.CustomerRole2Action || taskMapping.CustomerRole3Action || taskMapping.CustomerRole4Action)
				{
					nopManager.DeleteAclMappings("product", productId);
					foreach (SourceItem.CustomerRuleItem customerRole4 in item.CustomerRoles)
					{
						if (!string.IsNullOrEmpty(customerRole4.CustomerRule))
						{
							int customerRole2 = nopManager.GetCustomerRole(customerRole4.CustomerRule);
							nopManager.InsertAclMappingRecord("product", productId, customerRole2);
						}
					}
				}
				nopManager.DeleteStoreMapping(productId, "Product");
				if (nopProduct.LimitedToStores)
				{
					List<string> list2 = taskMapping.LimitedToStores.Split(new char[1]
					{
						';'
					}, StringSplitOptions.RemoveEmptyEntries).ToList();
					foreach (string item3 in list2)
					{
						if (GlobalClass.StringToInteger(item3) > 0)
						{
							nopManager.GetStoreMapping(productId, "Product", GlobalClass.StringToInteger(item3));
						}
					}
				}
			}
			return productId;
		}

		private string ReplaceKeyParams(int productId, string value)
		{
			if (!string.IsNullOrEmpty(value))
			{
				string input = value?.ToString().Replace("{ProductId}", productId.ToString());
				return Regex.Replace(input, "[^\\w\\d_-]", "-", RegexOptions.None);
			}
			return value;
		}

		private decimal RoundDecimal(string defaultValue, decimal retVal)
		{
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('~') == 0)
			{
				int decimals = GlobalClass.StringToInteger(defaultValue.Substring(1));
				retVal = Math.Round(GlobalClass.StringToDecimal(retVal), decimals, MidpointRounding.AwayFromZero);
			}
			return retVal;
		}

		private long InsertUpdateProductAttributes(long? taskId, SourceMapping sourceMapping, TaskMapping taskMapping, SourceItem item, NopManager nopManager, NopProduct nopProduct, StoreData storeData, bool newItem, StringBuilder errorList, Dictionary<string, int> picturesInStore, Dictionary<string, int> picturesSeoInStore)
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			try
			{
				int num = 0;
				int num2 = 0;
				foreach (SourceItem.AttributeValues attribute in item.Attributes)
				{
					num2 = (taskMapping.TaskProductAttributesMapping.ProdAttValue_DisplayOrder_Import ? attribute.AttValue_DisplayOrder : ((!newItem) ? 1 : 0));
					if (num == 0 && newItem && taskMapping.DeleteOldAttributtes)
					{
						nopManager.DeleteProductAttributes(nopProduct.ProductId);
					}
					num++;
					if (!string.IsNullOrEmpty(attribute.AttInfo_Name))
					{
						try
						{
							if (!string.IsNullOrEmpty(attribute.AttValue_AssociatedPicture?.ToLower()))
							{
								if (picturesInStore.ContainsKey(nopProduct.ProductId + attribute.AttValue_AssociatedPicture?.ToLower()))
								{
									attribute.AttValue_AssociatedPictureId = picturesInStore[nopProduct.ProductId + attribute.AttValue_AssociatedPicture?.ToLower()];
								}
								else if (picturesSeoInStore.ContainsKey(nopProduct.ProductId + attribute.AttValue_AssociatedPicture?.ToLower()))
								{
									attribute.AttValue_AssociatedPictureId = picturesSeoInStore[nopProduct.ProductId + attribute.AttValue_AssociatedPicture?.ToLower()];
								}
							}
						}
						catch (Exception ex)
						{
							LogManager.LogWrite(taskId, $"Associated Picture of attribute is not imported. Error: {ex.Message}", LogType.Error, nopProduct.Sku.ToString());
							attribute.AttValue_AssociatedPicture = null;
						}
						try
						{
							attribute.AttValue_SquarePictureId = attribute.AttValue_AssociatedPictureId;
							if (!string.IsNullOrEmpty(attribute.AttValue_SquarePicture?.ToLower()))
							{
								if (picturesInStore.ContainsKey(nopProduct.ProductId + attribute.AttValue_SquarePicture?.ToLower()))
								{
									attribute.AttValue_SquarePictureId = picturesInStore[nopProduct.ProductId + attribute.AttValue_SquarePicture?.ToLower()];
								}
								else if (picturesSeoInStore.ContainsKey(nopProduct.ProductId + attribute.AttValue_SquarePicture?.ToLower()))
								{
									attribute.AttValue_SquarePictureId = picturesSeoInStore[nopProduct.ProductId + attribute.AttValue_SquarePicture?.ToLower()];
								}
								else
								{
									if (string.IsNullOrEmpty(attribute.AttValue_SquarePicture))
									{
										continue;
									}
									string fileName = Path.GetFileName(attribute.AttValue_SquarePicture);
									if (string.IsNullOrEmpty(fileName) && item.Images.Count() == 1)
									{
										continue;
									}
									SourceItem.ImageItem ımageItem = new SourceItem.ImageItem
									{
										ImageUrl = attribute.AttValue_SquarePicture
									};
									Image image = item.GetImage(ımageItem);
									if (taskMapping.ResizePicture)
									{
										image = ImageHelper.ResizeImage(image, taskMapping.ResizePictureWidth, taskMapping.ResizePictureHeight, taskMapping.ResizePictureQuality, enforceRatio: true, addPadding: true);
									}
									if (!taskMapping.SavePicturesInFile && (!taskMapping.MediaSaveInDatabase.HasValue || taskMapping.MediaSaveInDatabase.Value))
									{
										attribute.AttValue_SquarePictureId = nopManager.InsertPicture(image, 0, ReplaceKeyParams(nopProduct.ProductId, ımageItem.ImageSeo), ımageItem.ImageExtension, 0, 0);
									}
									else if (taskMapping.SavePicturesInFile && (!taskMapping.MediaSaveInDatabase.HasValue || !taskMapping.MediaSaveInDatabase.Value))
									{
										attribute.AttValue_SquarePictureId = nopManager.InsertPictureFile(image, 0, taskMapping.SavePicturesPath, ReplaceKeyParams(nopProduct.ProductId, ımageItem.ImageSeo), ımageItem.ImageExtension, 0, 0);
									}
									if (!picturesInStore.ContainsKey(nopProduct.ProductId + ımageItem.ImageUrl?.ToLower()))
									{
										picturesInStore.Add(nopProduct.ProductId + ımageItem.ImageUrl?.ToLower(), attribute.AttValue_SquarePictureId);
									}
									if (!string.IsNullOrEmpty(ımageItem.ImageSeo) && !picturesSeoInStore.ContainsKey(nopProduct.ProductId + ımageItem.ImageSeo?.ToLower()))
									{
										picturesSeoInStore.Add(nopProduct.ProductId + ımageItem.ImageSeo?.ToLower(), attribute.AttValue_SquarePictureId);
									}
								}
							}
						}
						catch (Exception ex2)
						{
							LogManager.LogWrite(taskId, $"Square Picture of attribute is not imported. Error: {ex2.Message}", LogType.Error, nopProduct.Sku.ToString());
							attribute.AttValue_AssociatedPicture = null;
						}
					}
					TaskProductAttributesMapping taskProductAttributesMapping = new TaskProductAttributesMapping();
					taskProductAttributesMapping.TaskProductAttributesMappingItems = taskMapping.TaskProductAttributesMapping.TaskProductAttributesMappingItems.Where((TaskItemMapping x) => x.Id == attribute.MappingId).ToList();
					int productAttribute = nopManager.GetProductAttribute(attribute.AttInfo_Name);
					int productAttributeMapping = nopManager.GetProductAttributeMapping(taskProductAttributesMapping, nopProduct.ProductId, productAttribute, attribute.AttInfo_OptionType, attribute.AttInfo_DefaultValue, attribute.AttInfo_IsRequired, attribute.AttInfo_TextPrompt, attribute.AttInfo_DisplayOrder);
					int num3 = nopManager.InsertUpdateProductAttributeValue(taskProductAttributesMapping, nopProduct.ProductId, taskMapping.SavePicturesInFile, taskMapping.SavePicturesPath, productAttributeMapping, attribute.AttValue_Value, attribute.AttValue_Stock, Convert.ToByte(attribute.AttValue_IsPreselected), attribute.AttValue_Color, attribute.AttValue_AssociatedPictureId, attribute.AttValue_SquarePictureId, num2, attribute.AttValue_Price, attribute.AttValue_Cost, attribute.AttValue_PriceUsePercent, attribute.AttValue_WeightAdjustment);
					attribute.AttDbId = productAttribute;
					attribute.AttDbMapId = productAttributeMapping;
					attribute.AttDbValueId = num3;
					if (item.Variants != null && item.Variants.Any((SourceItem.VariantItem x) => x.IsSelected && x.VariantItems.Any((SourceItem.AttributeValues p) => p.AttOrgCode == attribute.AttOrgCode)))
					{
						foreach (SourceItem.VariantItem variant in item.Variants)
						{
							foreach (SourceItem.AttributeValues variantItem in variant.VariantItems)
							{
								if (attribute.AttOrgCode == variantItem.AttOrgCode)
								{
									variantItem.AttMapId = productAttributeMapping;
									variantItem.AttValueId = num3;
									variantItem.AttValue_AssociatedPictureId = attribute.AttValue_AssociatedPictureId;
									variantItem.AttValue_SquarePictureId = attribute.AttValue_SquarePictureId;
								}
							}
						}
					}
					else if (item.Variants != null && item.Variants.Any((SourceItem.VariantItem x) => !x.IsSelected && x.VariantItems.Any((SourceItem.AttributeValues p) => p.AttOrgCode == attribute.AttOrgCode)))
					{
						nopManager.DeleteProductAttributeValue(productAttributeMapping, attribute.AttValue_Value);
					}
					else
					{
						nopManager.InsertProductAttCombination(nopProduct.ProductId, productAttributeMapping, num3, attribute.AttValue_Stock, storeData.Version, attribute.AttValue_AssociatedPictureId);
					}
				}
				if (item.Variants != null)
				{
					int num4 = 0;
					foreach (SourceItem.VariantItem item2 in item.Variants.Where((SourceItem.VariantItem x) => x.IsSelected))
					{
						decimal valueOrDefault = item2.OverriddenPrice.GetValueOrDefault();
						decimal valueOrDefault2 = item2.OverriddenPrice.GetValueOrDefault();
						decimal? d = taskMapping.ProdPriceAddPercentVal;
						decimal? d2 = (decimal?)valueOrDefault2 * d / (decimal?)100m;
						item2.OverriddenPrice = ((decimal?)valueOrDefault + d2 + taskMapping.ProdPriceAddUnitVal).GetValueOrDefault();
						item2.OverriddenPrice = RoundDecimal(taskMapping.ProdPriceVal, item2.OverriddenPrice.GetValueOrDefault());
						item2.AssociatedPictureId = item2.VariantItems.FirstOrDefault((SourceItem.AttributeValues x) => x.AttValue_AssociatedPictureId > 0)?.AttValue_AssociatedPictureId;
						nopManager.InsertUpdateProductAttCombinations(nopProduct.ProductId, item2, storeData.Version, item2.AssociatedPictureId);
						num4++;
					}
				}
			}
			catch (Exception ex3)
			{
				LogManager.LogWrite(taskId, $"ProductAttributes not imported. Error: {ex3.ToString()}", LogType.Error, nopProduct.Sku.ToString());
				errorList.AppendLine($"ProductAttributes not imported. Error: {ex3.Message}");
			}
			finally
			{
				stopwatch.Stop();
			}
			return stopwatch.ElapsedMilliseconds;
		}

		private long InsertUpdateImages(long? taskId, SourceMapping sourceMapping, TaskMapping taskMapping, SourceItem item, NopManager nopManager, int productId, bool newItem, NopProduct nopProduct, StringBuilder errorList, Dictionary<string, int> picturesInStore, Dictionary<string, int> picturesSeoInStore)
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			try
			{
				if (item.Images != null && item.Images.Any())
				{
					bool flag = false;
					bool flag2 = false;
					bool flag3 = false;
					int num = 0;
					List<int> list = new List<int>();
					List<int> list2 = new List<int>();
					foreach (SourceItem.ImageItem ımage in item.Images)
					{
						if (!ımage.IsBinary)
						{
							if (string.IsNullOrEmpty(ımage.ImageUrl))
							{
								continue;
							}
							if (picturesInStore.ContainsKey(nopProduct.ProductId + ımage.ImageUrl.ToLower()))
							{
								flag = true;
								continue;
							}
							if (picturesSeoInStore.ContainsKey(nopProduct.ProductId + ımage.ImageSeo?.ToLower()))
							{
								flag = true;
								continue;
							}
							string fileName = Path.GetFileName(ımage.ImageUrl);
							if (string.IsNullOrEmpty(fileName) && item.Images.Count() == 1)
							{
								flag2 = true;
								continue;
							}
						}
						if (num == 0 && newItem && taskMapping.DeleteOldPictures)
						{
							nopManager.DeleteProductImages(productId, taskMapping.DeleteOldPictures, taskMapping.SavePicturesPath, 0, deleteIfNotMapped: false);
						}
						else if (num == 0)
						{
							list = nopManager.ExistProductImages(productId);
						}
						if (!string.IsNullOrEmpty(ımage.ImageUrl.Trim()))
						{
							try
							{
								int num2 = 0;
								int num3 = (newItem && num < list.Count) ? list[num] : 0;
								Image image = item.GetImage(ımage);
								if (taskMapping.ResizePicture)
								{
									image = ImageHelper.ResizeImage(image, taskMapping.ResizePictureWidth, taskMapping.ResizePictureHeight, taskMapping.ResizePictureQuality, enforceRatio: true, addPadding: true);
								}
								if (!taskMapping.SavePicturesInFile && (!taskMapping.MediaSaveInDatabase.HasValue || taskMapping.MediaSaveInDatabase.Value))
								{
									num2 = nopManager.InsertPicture(image, productId, ReplaceKeyParams(productId, ımage.ImageSeo), ımage.ImageExtension, num3, 0);
								}
								else if (taskMapping.SavePicturesInFile && (!taskMapping.MediaSaveInDatabase.HasValue || !taskMapping.MediaSaveInDatabase.Value))
								{
									num2 = nopManager.InsertPictureFile(image, productId, taskMapping.SavePicturesPath, ReplaceKeyParams(productId, ımage.ImageSeo), ımage.ImageExtension, num3, 0);
								}
								flag = true;
								ımage.ImageDBId = num2;
								list2.Add(num3);
								if (!picturesInStore.ContainsKey(nopProduct.ProductId + ımage.ImageUrl?.ToLower()))
								{
									picturesInStore.Add(nopProduct.ProductId + ımage.ImageUrl?.ToLower(), num2);
								}
								if (!string.IsNullOrEmpty(ımage.ImageSeo) && !picturesSeoInStore.ContainsKey(nopProduct.ProductId + ımage.ImageSeo?.ToLower()))
								{
									picturesSeoInStore.Add(nopProduct.ProductId + ımage.ImageSeo?.ToLower(), num2);
								}
							}
							catch (Exception ex)
							{
								if (!ımage.IsArray)
								{
									errorList.AppendLine($"Picture {ımage.ImageUrl} not imported for product {item.ProdSku}. Error: {ex.Message}");
									flag3 = true;
								}
							}
						}
						num++;
					}
					if (list.Count > 0 && list2.Count > 0 && newItem)
					{
						IEnumerable<int> enumerable = list.Except(list2);
						foreach (int item2 in enumerable)
						{
							nopManager.DeleteProductImages(productId, deleteFromFile: true, taskMapping.SavePicturesPath, item2, deleteIfNotMapped: false);
						}
					}
					try
					{
						if (!flag && !string.IsNullOrEmpty(sourceMapping.ImageVal) && !picturesSeoInStore.ContainsKey(nopProduct.ProductId + sourceMapping.ImageSeoVal?.ToLower()) && !picturesInStore.ContainsKey(nopProduct.ProductId + sourceMapping.ImageVal?.ToLower()))
						{
							string imageExtension = "jpg";
							try
							{
								imageExtension = Path.GetExtension(sourceMapping.ImageVal).Replace(".", "");
							}
							catch
							{
							}
							Image image2 = item.GetImageFromUrl(sourceMapping.ImageVal);
							int value = 0;
							if (taskMapping.ResizePicture)
							{
								image2 = ImageHelper.ResizeImage(image2, taskMapping.ResizePictureWidth, taskMapping.ResizePictureHeight, taskMapping.ResizePictureQuality, enforceRatio: true, addPadding: true);
							}
							if (!taskMapping.SavePicturesInFile && (!taskMapping.MediaSaveInDatabase.HasValue || taskMapping.MediaSaveInDatabase.Value))
							{
								value = nopManager.InsertPicture(image2, productId, ReplaceKeyParams(productId, sourceMapping.ImageSeoVal), imageExtension, 0, 0);
							}
							else if (taskMapping.SavePicturesInFile && (!taskMapping.MediaSaveInDatabase.HasValue || !taskMapping.MediaSaveInDatabase.Value))
							{
								value = nopManager.InsertPictureFile(image2, productId, taskMapping.SavePicturesPath, ReplaceKeyParams(productId, sourceMapping.ImageSeoVal), imageExtension, 0, 0);
							}
							flag = true;
							if (!picturesInStore.ContainsKey(nopProduct.ProductId + sourceMapping.ImageVal?.ToLower()))
							{
								picturesInStore.Add(nopProduct.ProductId + sourceMapping.ImageVal?.ToLower(), value);
							}
							if (!string.IsNullOrEmpty(sourceMapping.ImageSeoVal) && !picturesSeoInStore.ContainsKey(nopProduct.ProductId + sourceMapping.ImageSeoVal?.ToLower()))
							{
								picturesSeoInStore.Add(nopProduct.ProductId + sourceMapping.ImageSeoVal?.ToLower(), value);
							}
						}
					}
					catch (Exception)
					{
					}
					if (!flag && !flag2 && !flag3)
					{
						string text = $"No pictures found for product {item.ProdSku}";
						LogManager.LogWrite(taskId, text, LogType.Error, nopProduct.Sku.ToString());
						errorList.AppendLine(text);
					}
				}
			}
			catch (Exception ex3)
			{
				LogManager.LogWrite(taskId, $"Pictures not imported for product {item.ProdSku}. Error: {ex3.Message}", LogType.Error, nopProduct.Sku.ToString());
				errorList.AppendLine($"Pictures not imported for product {item.ProdSku}. Error: {ex3.Message}");
			}
			finally
			{
				stopwatch.Stop();
			}
			return stopwatch.ElapsedMilliseconds;
		}

		private long InsertUpdateTirPricing(long taskId, SourceMapping sourceMapping, TaskMapping taskMapping, SourceItem item, NopManager nopManager, NopProduct nopProduct, List<string> limitedStores, List<string> limitedStoresEntities, StringBuilder errorList, bool newItem, DatabaseManager databaseManager)
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			try
			{
				if (item.TierPricingItems != null && item.TierPricingItems.Any())
				{
					if (newItem && taskMapping.DeleteExistingTierPricingOnUpdate)
					{
						nopManager.DeleteTierPricing(nopProduct.ProductId);
					}
					foreach (SourceItem.TierPricingItem tierPricingItem in item.TierPricingItems)
					{
						decimal num = tierPricingItem.Price + tierPricingItem.AddPrice + tierPricingItem.Price * (decimal)tierPricingItem.AddPercent / 100m;
						if (num > 0m && tierPricingItem.Quantity > 0)
						{
							if (nopProduct.LimitedToStores && limitedStoresEntities.Contains("4"))
							{
								foreach (string limitedStore in limitedStores)
								{
									int? customerRoleId = null;
									if (tierPricingItem.CustomerRoles != null && tierPricingItem.CustomerRoles.Any())
									{
										foreach (string customerRole in tierPricingItem.CustomerRoles)
										{
											customerRoleId = null;
											if (!string.IsNullOrEmpty(customerRole))
											{
												customerRoleId = nopManager.GetCustomerRole(customerRole);
											}
										}
									}
									databaseManager.NopTierPrices.Add(new NopTierPrice
									{
										ProductId = nopProduct.ProductId,
										StoreId = GlobalClass.StringToInteger(limitedStore),
										CustomerRoleId = customerRoleId,
										Price = num,
										Quantity = tierPricingItem.Quantity,
										StartDateTimeUtc = tierPricingItem.StartDateTime,
										EndDateTimeUtc = tierPricingItem.EndDateTime,
										ACCID = item.TierPricing_ACCID
									});
								}
							}
							else
							{
								int num2 = 0;
								int? customerRoleId2 = null;
								if (tierPricingItem.CustomerRoles != null && tierPricingItem.CustomerRoles.Any())
								{
									foreach (string customerRole2 in tierPricingItem.CustomerRoles)
									{
										customerRoleId2 = null;
										if (!string.IsNullOrEmpty(customerRole2))
										{
											customerRoleId2 = nopManager.GetCustomerRole(customerRole2);
										}
									}
								}
								databaseManager.NopTierPrices.Add(new NopTierPrice
								{
									ProductId = nopProduct.ProductId,
									StoreId = GlobalClass.StringToInteger(num2),
									CustomerRoleId = customerRoleId2,
									Price = num,
									Quantity = tierPricingItem.Quantity,
									StartDateTimeUtc = tierPricingItem.StartDateTime,
									EndDateTimeUtc = tierPricingItem.EndDateTime,
									ACCID = item.TierPricing_ACCID
								});
							}
						}
					}
					databaseManager.TierPricingsSynchro();
				}
			}
			catch (Exception ex)
			{
				LogManager.LogWrite(taskId, $"Tier Pricing not imported. Error: {ex.Message}", LogType.Error, nopProduct.Sku.ToString());
				errorList.AppendLine($"Tier Pricing not imported. Error: {ex.Message}");
			}
			finally
			{
				stopwatch.Stop();
			}
			return stopwatch.ElapsedMilliseconds;
		}

		private long InsertUpdateManufacturer(long? taskId, SourceMapping sourceMapping, TaskMapping taskMapping, SourceItem item, NopManager nopManager, NopProduct nopProduct, List<string> limitedStores, List<string> limitedStoresEntities, bool newItem, StringBuilder errorList)
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			try
			{
				if (!string.IsNullOrEmpty(item.Manufacturer))
				{
					int num = 0;
					num = ((!sourceMapping.ManufacturerIsId) ? nopManager.GetManufacturer(item.Manufacturer, limitedStoresEntities.Contains("3") && nopProduct.LimitedToStores) : GlobalClass.StringToInteger(item.Manufacturer));
					nopManager.InsertProductAndManufacter(nopProduct.ProductId, num);
					string manufacturer = item.Manufacturer;
					nopManager.InsertUpdateUrlRecord("Manufacturer", num, manufacturer);
					if (nopProduct.LimitedToStores && limitedStoresEntities.Contains("3"))
					{
						foreach (string limitedStore in limitedStores)
						{
							if (GlobalClass.StringToInteger(limitedStore) > 0)
							{
								nopManager.GetStoreMapping(num, "Manufacturer", GlobalClass.StringToInteger(limitedStore));
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogManager.LogWrite(taskId, $"Manufacturer not imported. Error: {ex.Message}", LogType.Error, nopProduct.Sku.ToString());
				errorList.AppendLine($"Manufacturer not imported. Error: {ex.Message}");
			}
			finally
			{
				stopwatch.Stop();
			}
			return stopwatch.ElapsedMilliseconds;
		}

		private long InsertUpdateCategories(long? taskId, SourceMapping sourceMapping, TaskMapping taskMapping, SourceItem item, NopManager nopManager, NopProduct nopProduct, List<string> limitedStores, List<string> limitedStoresEntities, bool newItem, StringBuilder errorList)
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			try
			{
				int num = 0;
				int num2 = 0;
				int num3 = 0;
				int num4 = 0;
				int num5 = 0;
				int num6 = 0;
				int num7 = (from w in item.Categories.Distinct()
					where w.CatId == 0
					select w).Count();
				foreach (SourceItem.CategoryItem cat in item.Categories.Distinct())
				{
					bool flag = false;
					bool isNew = false;
					if (num == 0 && newItem && taskMapping.DeleteOldCatMapping && taskMapping.TaskAction == 0 && taskMapping.TaskAction == 2)
					{
						nopManager.DeleteProductAndCategoriesMapping(nopProduct.ProductId);
					}
					int num8 = 0;
					if (!string.IsNullOrEmpty(cat.Category))
					{
						int num9 = 0;
						num9 = ((cat.CatId == 0 && taskMapping.CatParent == 1) ? num2 : ((cat.CatId != 0) ? ((cat.CatId == 1 && taskMapping.Cat1Parent == 1) ? num3 : ((cat.CatId == 2 && taskMapping.Cat2Parent == 1) ? num3 : ((cat.CatId == 2 && taskMapping.Cat2Parent == 2) ? num4 : ((cat.CatId == 3 && taskMapping.Cat3Parent == 1) ? num3 : ((cat.CatId == 3 && taskMapping.Cat3Parent == 2) ? num4 : ((cat.CatId == 3 && taskMapping.Cat3Parent == 3) ? num5 : 0)))))) : 0));
						if (num < num7 && cat.CatId == 0)
						{
							if (taskMapping.CatParent == 1)
							{
								num9 = num2;
								flag = (num == num7 - 1);
							}
							else
							{
								num9 = num3;
								flag = (num == num7 - 1);
							}
						}
						if (taskMapping.DisableNestedCategories)
						{
							num9 = 0;
							flag = true;
						}
						num8 = (((!sourceMapping.CatIsId || cat.CatId != 0) && (!sourceMapping.Cat1IsId || cat.CatId != 1) && (!sourceMapping.Cat2IsId || cat.CatId != 2) && (!sourceMapping.Cat3IsId || cat.CatId != 3) && (!sourceMapping.Cat4IsId || cat.CatId != 4)) ? nopManager.GetCategory(cat.Category, num9, limitedStoresEntities.Contains("2") && nopProduct.LimitedToStores, out isNew) : GlobalClass.StringToInteger(cat.Category));
						cat.CatDbId = num8;
						if (cat.CatId == 0)
						{
							if (taskMapping.CatParent == 1 && num < num7 - 1)
							{
								num2 = num8;
							}
							else
							{
								num3 = num8;
							}
						}
						if (cat.CatId == 1)
						{
							num4 = num8;
						}
						if (cat.CatId == 2)
						{
							num5 = num8;
						}
						if (cat.CatId == 3)
						{
							num6 = num8;
						}
						string category = cat.Category;
						nopManager.InsertUpdateUrlRecord("Category", num8, category);
						nopManager.DeleteStoreMapping(num8, "Category");
						if (nopProduct.LimitedToStores && limitedStoresEntities.Contains("2"))
						{
							foreach (string limitedStore in limitedStores)
							{
								if (GlobalClass.StringToInteger(limitedStore) > 0)
								{
									nopManager.GetStoreMapping(num8, "Category", GlobalClass.StringToInteger(limitedStore));
								}
							}
						}
						if (taskMapping.ProductsInTheLastSubCategory && !taskMapping.DisableNestedCategories && item.Categories.Distinct().Any((SourceItem.CategoryItem x) => x.ParentCatId.HasValue))
						{
							if (!item.Categories.Distinct().Any((SourceItem.CategoryItem x) => x.ParentCatId == cat.CatId && x.CatId > cat.CatId) && ((taskMapping.CatAction && cat.CatId == 0 && flag) || (taskMapping.Cat1Action && cat.CatId == 1) || (taskMapping.Cat2Action && cat.CatId == 2) || (taskMapping.Cat3Action && cat.CatId == 3) || (taskMapping.Cat4Action && cat.CatId == 4)))
							{
								nopManager.InsertProductAndCatecoryMapping(nopProduct.ProductId, num8);
							}
						}
						else if ((taskMapping.CatAction && cat.CatId == 0 && flag) || (taskMapping.Cat1Action && cat.CatId == 1) || (taskMapping.Cat2Action && cat.CatId == 2) || (taskMapping.Cat3Action && cat.CatId == 3) || (taskMapping.Cat4Action && cat.CatId == 4))
						{
							nopManager.InsertProductAndCatecoryMapping(nopProduct.ProductId, num8);
						}
						if (taskMapping.InsertCategoryPictureOnInsert && isNew)
						{
							try
							{
								SourceItem.ImageItem ımageItem = item.Images.FirstOrDefault((SourceItem.ImageItem x) => x.ImageUrl != null);
								if (ımageItem != null)
								{
									Image image = item.GetImage(ımageItem);
									if (taskMapping.ResizePicture)
									{
										image = ImageHelper.ResizeImage(image, taskMapping.ResizePictureWidth, taskMapping.ResizePictureHeight, taskMapping.ResizePictureQuality, enforceRatio: true, addPadding: true);
									}
									if (!taskMapping.SavePicturesInFile && (!taskMapping.MediaSaveInDatabase.HasValue || taskMapping.MediaSaveInDatabase.Value))
									{
										nopManager.InsertPicture(image, nopProduct.ProductId, ReplaceKeyParams(nopProduct.ProductId, ımageItem.ImageSeo), ımageItem.ImageExtension, 0, num8);
									}
									else if (taskMapping.SavePicturesInFile && (!taskMapping.MediaSaveInDatabase.HasValue || !taskMapping.MediaSaveInDatabase.Value))
									{
										nopManager.InsertPictureFile(image, nopProduct.ProductId, taskMapping.SavePicturesPath, ReplaceKeyParams(nopProduct.ProductId, ımageItem.ImageSeo), ımageItem.ImageExtension, 0, num8);
									}
								}
							}
							catch (Exception ex)
							{
								LogManager.LogWrite(taskId, $"Picture of category is not imported. Error: {ex.Message}", LogType.Error, nopProduct.Sku.ToString());
							}
						}
					}
					num++;
				}
			}
			catch (Exception ex2)
			{
				LogManager.LogWrite(taskId, $"Categories not imported. Error: {ex2.Message}", LogType.Error, nopProduct.Sku.ToString());
				errorList.AppendLine($"Categories not imported. Error: {ex2.Message}");
			}
			finally
			{
				stopwatch.Stop();
			}
			return stopwatch.ElapsedMilliseconds;
		}

		private long InsertUpdateSpecAttributes(long? taskId, SourceMapping sourceMapping, TaskMapping taskMapping, SourceItem item, NopManager nopManager, NopProduct nopProduct, bool newItem, StringBuilder errorList, Dictionary<string, int> specAttributeList, Dictionary<string, int> specAttributeOptionList, Dictionary<string, int> specAttributeProductList)
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			try
			{
				int num = 0;
				if (item.SpecAttributes != null && item.SpecAttributes.Any())
				{
					foreach (SourceItem.AttributeValues item2 in item.SpecAttributes.Where((SourceItem.AttributeValues x) => x.Import))
					{
						if (num == 0 && newItem && taskMapping.DeleteOldSpecAttributtes)
						{
							nopManager.DeleteSpecAttributesMapping(nopProduct.ProductId);
						}
						if (!string.IsNullOrEmpty(item2.AttInfo_Name) && !string.IsNullOrEmpty(item2.AttValue_Value))
						{
							int num2 = 0;
							int num3 = 0;
							if (specAttributeList.ContainsKey(item2.AttInfo_Name))
							{
								num2 = specAttributeList[item2.AttInfo_Name];
							}
							else
							{
								num2 = nopManager.GetSpecAttribute(item2.AttInfo_Name);
								specAttributeList.Add(item2.AttInfo_Name, num2);
							}
							if (specAttributeOptionList.ContainsKey(item2.AttValue_Value))
							{
								num3 = specAttributeOptionList[item2.AttValue_Value];
							}
							else
							{
								num3 = nopManager.GetSpecAttributeOption(num2, item2.AttValue_Value);
								specAttributeOptionList.Add(item2.AttValue_Value, num3);
							}
							num++;
							bool showOnProduct = item2.ShowOnProduct;
							bool allowFiltering = item2.AllowFiltering;
							int attInfo_OptionType = item2.AttInfo_OptionType;
							if (!specAttributeProductList.ContainsKey(nopProduct.ProductId + "_" + num3))
							{
								int specAttributeMapping = nopManager.GetSpecAttributeMapping(nopProduct.ProductId, num3, attInfo_OptionType, item2.AttValue_Value, showOnProduct ? 1 : 0, allowFiltering ? 1 : 0);
								specAttributeProductList.Add(nopProduct.ProductId + "_" + num3, specAttributeMapping);
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogManager.LogWrite(taskId, $"Spec attributes are not imported. Error: {ex.Message}", LogType.Error, nopProduct.Sku.ToString());
				errorList.AppendLine($"Spec attributes are not imported. Error: {ex.Message}");
			}
			finally
			{
				stopwatch.Stop();
			}
			return stopwatch.ElapsedMilliseconds;
		}

		private long InsertUpdateProductTags(long? taskId, SourceMapping sourceMapping, TaskMapping taskMapping, SourceItem item, NopManager nopManager, NopProduct nopProduct, bool newItem, StringBuilder errorList)
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			try
			{
				int num = 0;
				if (item.ProductTags != null && item.ProductTags.Any())
				{
					foreach (SourceItem.ProductTagItem productTag2 in item.ProductTags)
					{
						if (num == 0)
						{
							nopManager.DeleteProductTagMapping(nopProduct.ProductId);
						}
						if (!string.IsNullOrEmpty(productTag2.ProductTagName))
						{
							int productTag = nopManager.GetProductTag(productTag2.ProductTagName);
							nopManager.InsertProductTagMapping(nopProduct.ProductId, productTag);
							num++;
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogManager.LogWrite(taskId, $"Product Tags are not imported. Error: {ex.Message}", LogType.Error, nopProduct.Sku.ToString());
				errorList.AppendLine($"Product Tags are not imported. Error: {ex.Message}");
			}
			finally
			{
				stopwatch.Stop();
			}
			return stopwatch.ElapsedMilliseconds;
		}

		private void AddSpecAttributtesToFullDecription(TaskMapping taskMapping, SourceItem item, NopProduct nopProduct)
		{
			if (taskMapping.AddSpecAttributtesToFullDescAsTable && item.SpecAttributes != null && item.SpecAttributes.Any())
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.AppendLine("<ul>");
				foreach (SourceItem.AttributeValues specAttribute in item.SpecAttributes)
				{
					stringBuilder.AppendLine($"<li><b>{specAttribute.AttInfo_Name}</b> {specAttribute.AttValue_Value}</li>");
				}
				stringBuilder.AppendLine("</ul>");
				nopProduct.FullDescription = nopProduct.FullDescription?.ToString() + stringBuilder.ToString();
			}
		}

		private void SaveSourceCopy(long? taskId, string path, string filename, byte[] fileContent, StringBuilder errorList)
		{
			try
			{
				if (!string.IsNullOrEmpty(path) && fileContent != null)
				{
					if (!Directory.Exists(path))
					{
						Directory.CreateDirectory(path);
					}
					File.WriteAllBytes(Path.Combine(path, filename), fileContent);
				}
			}
			catch (Exception ex)
			{
				LogManager.LogWrite(taskId, $"Source copy not saved. Error: {ex.Message}", LogType.Warning);
				errorList.AppendLine($"Source copy not saved. Error: {ex.Message}");
			}
		}

		private void TranslateText(long? taskId, TaskMapping taskMapping, Dictionary<SourceItem, NopProduct> itemsList, StringBuilder errorList)
		{
			try
			{
				TaskTranslate taskTranslate = taskMapping.TaskTranslate;
				if (taskTranslate != null && taskTranslate.LanguageId.HasValue && taskMapping.TaskTranslate.LanguageId.Value != 0 && !string.IsNullOrEmpty(taskMapping.TaskTranslate.TranslatorApi) && !string.IsNullOrEmpty(taskMapping.TaskTranslate.SubscribtionKey))
				{
					List<string> list = new List<string>();
					string text = "";
					foreach (KeyValuePair<SourceItem, NopProduct> items in itemsList)
					{
						SourceItem key = items.Key;
						NopProduct value = items.Value;
						if (taskMapping.TaskTranslate.ProductName)
						{
							text = value.Name;
							if (!string.IsNullOrEmpty(text) && !replaceTextHelper.TranslateTextItems.ContainsKey(text) && !list.Contains(text))
							{
								list.Add(text);
							}
						}
						if (taskMapping.TaskTranslate.ProductShortDesc)
						{
							text = value.ShortDescription.ToString();
							if (!string.IsNullOrEmpty(text) && !replaceTextHelper.TranslateTextItems.ContainsKey(text) && !list.Contains(text))
							{
								list.Add(text);
							}
						}
						if (taskMapping.TaskTranslate.ProductFullDesc)
						{
							text = value.FullDescription.ToString();
							if (!string.IsNullOrEmpty(text) && !replaceTextHelper.TranslateTextItems.ContainsKey(text) && !list.Contains(text))
							{
								list.Add(text);
							}
						}
						if (taskMapping.TaskTranslate.CategoryName && key.Categories != null)
						{
							foreach (SourceItem.CategoryItem category in key.Categories)
							{
								text = category.Category;
								if (!string.IsNullOrEmpty(text) && !replaceTextHelper.TranslateTextItems.ContainsKey(text) && !list.Contains(text))
								{
									list.Add(text);
								}
							}
						}
						if (taskMapping.TaskTranslate.ProductAttributes && key.Attributes != null)
						{
							foreach (SourceItem.AttributeValues attribute in key.Attributes)
							{
								text = attribute.AttInfo_Name;
								if (!string.IsNullOrEmpty(text) && !replaceTextHelper.TranslateTextItems.ContainsKey(text) && !list.Contains(text))
								{
									list.Add(text);
								}
								text = attribute.AttValue_Value;
								if (!string.IsNullOrEmpty(text) && !replaceTextHelper.TranslateTextItems.ContainsKey(text) && !list.Contains(text))
								{
									list.Add(text);
								}
							}
							foreach (SourceItem.AttributeValues specAttribute in key.SpecAttributes)
							{
								text = specAttribute.AttInfo_Name;
								if (!string.IsNullOrEmpty(text) && !replaceTextHelper.TranslateTextItems.ContainsKey(text) && !list.Contains(text))
								{
									list.Add(text);
								}
								text = specAttribute.AttValue_Value;
								if (!string.IsNullOrEmpty(text) && !replaceTextHelper.TranslateTextItems.ContainsKey(text) && !list.Contains(text))
								{
									list.Add(text);
								}
							}
						}
					}
					if (list.Any())
					{
						list = list.Where((string x) => x.Any((char y) => char.IsLetter(y))).ToList().Distinct()
							.ToList();
						TranslateHelper translateHelper = new TranslateHelper();
						foreach (IEnumerable<string> item in list.Batch(5))
						{
							Dictionary<string, string> dictionary = translateHelper.TranslateText(taskMapping.TaskTranslate.TranslatorApi, taskMapping.TaskTranslate.SubscribtionKey, item.ToList());
							foreach (KeyValuePair<string, string> item2 in dictionary)
							{
								replaceTextHelper.SetReplacedText(item2.Key, item2.Value);
							}
							replaceTextHelper.SaveReplacedFile();
							Thread.Sleep(1000);
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogManager.LogWrite(taskId, $"TranslateText. Error: {ex.Message}", LogType.Warning);
				errorList.AppendLine($"TranslateText. Error: {ex.Message}");
			}
		}

		private void RunCustomSql(long? taskId, TaskMapping taskMapping, NopManager nopManager, int customSqlAction, StringBuilder errorList)
		{
			try
			{
				if (taskMapping.CustomSqlAction.HasValue && taskMapping.CustomSqlAction.Value > 0 && taskMapping.CustomSqlAction.Value == customSqlAction)
				{
					nopManager.RunCustomSQL(taskMapping.CustomSqlQueries);
				}
			}
			catch (Exception ex)
			{
				LogManager.LogWrite(taskId, $"RunCustomSql. Error: {ex.Message}", LogType.Warning);
				errorList.AppendLine($"RunCustomSql. Error: {ex.Message}");
			}
		}

		private void AddLocalizedProperties(long? taskId, SourceMapping sourceMapping, TaskMapping taskMapping, NopManager nopManager, Dictionary<SourceItem, NopProduct> itemsList, StringBuilder errorList)
		{
			TaskTranslate taskTranslate = taskMapping.TaskTranslate;
			if (taskTranslate != null && taskTranslate.LanguageId.HasValue)
			{
				List<string> list = new List<string>();
				int value = taskMapping.TaskTranslate.LanguageId.Value;
				if (value != 0)
				{
					try
					{
						if (taskMapping.TaskTranslate.LanguageId > 0)
						{
							foreach (KeyValuePair<SourceItem, NopProduct> items in itemsList)
							{
								SourceItem key = items.Key;
								NopProduct value2 = items.Value;
								int productId = value2.ProductId;
								if (taskMapping.TaskTranslate.ProductName)
								{
									string text = "Product";
									string text2 = "Name";
									string item = $"{value},{productId},{text},{text2}";
									if (!list.Contains(item))
									{
										list.Add(item);
										string name = value2.Name;
										if (!string.IsNullOrEmpty(name))
										{
											string replacedText = replaceTextHelper.GetReplacedText(name);
											replacedText = GlobalClass.ObjectToString2(replacedText, taskMapping.UseTitleCase);
											nopManager.InsertUpdateLocalizedProperty(productId, value, text, text2, replacedText);
										}
									}
								}
								if (taskMapping.TaskTranslate.ProductShortDesc)
								{
									string text = "Product";
									string text2 = "ShortDescription";
									string item = $"{value},{productId},{text},{text2}";
									if (!list.Contains(item))
									{
										list.Add(item);
										string name = value2.ShortDescription.ToString();
										if (!string.IsNullOrEmpty(name))
										{
											string replacedText = replaceTextHelper.GetReplacedText(name);
											replacedText = GlobalClass.ObjectToString2(replacedText, taskMapping.UseTitleCase);
											nopManager.InsertUpdateLocalizedProperty(productId, value, text, text2, replacedText);
										}
									}
								}
								if (taskMapping.TaskTranslate.ProductFullDesc)
								{
									string text = "Product";
									string text2 = "FullDescription";
									string item = $"{value},{productId},{text},{text2}";
									if (!list.Contains(item))
									{
										list.Add(item);
										string name = value2.FullDescription.ToString();
										if (!string.IsNullOrEmpty(name))
										{
											string replacedText = replaceTextHelper.GetReplacedText(name);
											replacedText = GlobalClass.ObjectToString2(replacedText, taskMapping.UseTitleCase);
											nopManager.InsertUpdateLocalizedProperty(productId, value, text, text2, replacedText);
										}
									}
								}
								if (taskMapping.TaskTranslate.CategoryName && key.Categories != null)
								{
									string text = "Category";
									string text2 = "Name";
									foreach (SourceItem.CategoryItem category in key.Categories)
									{
										productId = category.CatDbId;
										string item = $"{value},{productId},{text},{text2}";
										if (!list.Contains(item))
										{
											list.Add(item);
											string name = category.Category.ToString();
											if (!string.IsNullOrEmpty(name))
											{
												string replacedText = replaceTextHelper.GetReplacedText(name);
												replacedText = GlobalClass.ObjectToString2(replacedText, taskMapping.UseTitleCase);
												nopManager.InsertUpdateLocalizedProperty(productId, value, text, text2, replacedText);
											}
										}
									}
								}
								if (taskMapping.TaskTranslate.ProductAttributes)
								{
									if (key.Attributes != null)
									{
										foreach (SourceItem.AttributeValues attribute in key.Attributes)
										{
											productId = attribute.AttDbId;
											string text = "ProductAttribute";
											string text2 = "Name";
											string item = $"{value},{productId},{text},{text2}";
											if (!list.Contains(item))
											{
												list.Add(item);
												string name = attribute.AttInfo_Name.ToString();
												if (!string.IsNullOrEmpty(name))
												{
													string replacedText = replaceTextHelper.GetReplacedText(name);
													replacedText = GlobalClass.ObjectToString2(replacedText, taskMapping.UseTitleCase);
													nopManager.InsertUpdateLocalizedProperty(productId, value, text, text2, replacedText);
												}
											}
											productId = attribute.AttDbValueId;
											text = "ProductAttributeValue";
											text2 = "Name";
											item = $"{value},{productId},{text},{text2}";
											if (!list.Contains(item))
											{
												list.Add(item);
												string name = attribute.AttValue_Value.ToString();
												if (!string.IsNullOrEmpty(name))
												{
													string replacedText = replaceTextHelper.GetReplacedText(name);
													replacedText = GlobalClass.ObjectToString2(replacedText, taskMapping.UseTitleCase);
													nopManager.InsertUpdateLocalizedProperty(productId, value, text, text2, replacedText);
												}
											}
										}
									}
									if (key.SpecAttributes != null)
									{
										foreach (SourceItem.AttributeValues specAttribute in key.SpecAttributes)
										{
											productId = specAttribute.AttDbId;
											string text = "SpecificationAttribute";
											string text2 = "Name";
											string item = $"{value},{productId},{text},{text2}";
											if (!list.Contains(item))
											{
												list.Add(item);
												string name = specAttribute.AttInfo_Name.ToString();
												if (!string.IsNullOrEmpty(name))
												{
													string replacedText = replaceTextHelper.GetReplacedText(name);
													replacedText = GlobalClass.ObjectToString2(replacedText, taskMapping.UseTitleCase);
													nopManager.InsertUpdateLocalizedProperty(productId, value, text, text2, replacedText);
												}
											}
											productId = specAttribute.AttDbValueId;
											text = "SpecificationAttributeOption";
											text2 = "Name";
											item = $"{value},{productId},{text},{text2}";
											if (!list.Contains(item))
											{
												list.Add(item);
												string name = specAttribute.AttValue_Value.ToString();
												if (!string.IsNullOrEmpty(name))
												{
													string replacedText = replaceTextHelper.GetReplacedText(name);
													replacedText = GlobalClass.ObjectToString2(replacedText, taskMapping.UseTitleCase);
													nopManager.InsertUpdateLocalizedProperty(productId, value, text, text2, replacedText);
												}
											}
										}
									}
								}
							}
						}
					}
					catch (Exception ex)
					{
						LogManager.LogWrite(taskId, $"AddLocalizedProperties. Error: {ex.Message}", LogType.Warning);
						errorList.AppendLine($"AddLocalizedProperties. Error: {ex.Message}");
					}
				}
			}
		}

		private void InsertUpdateCustomerGenericAttributes(long? taskId, SourceMapping sourceMapping, SourceItem item, NopManager nopManager, NopCustomer nopCustomer, NopAddress nopAddress, bool newItem, StringBuilder errorList)
		{
			try
			{
				if (nopAddress.AddressName != null)
				{
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "AddressName", nopAddress.AddressName, 0);
				}
				if (nopCustomer.FirstName != null)
				{
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "FirstName", nopCustomer.FirstName, 0);
				}
				if (nopCustomer.LastName != null)
				{
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "LastName", nopCustomer.LastName, 0);
				}
				if (nopAddress.Company != null)
				{
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "Company", nopAddress.Company, 0);
				}
				if (nopAddress.Address1 != null)
				{
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "Address", nopAddress.Address1, 0);
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "StreetAddress", nopAddress.Address1, 0);
				}
				if (nopAddress.Address2 != null)
				{
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "Address2", nopAddress.Address2, 0);
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "StreetAddress2", nopAddress.Address2, 0);
				}
				if (nopAddress.Address3 != null)
				{
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "Address3", nopAddress.Address2, 0);
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "StreetAddress3", nopAddress.Address2, 0);
				}
				if (nopAddress.ZipPostalCode != null)
				{
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "ZipPostalCode", nopAddress.ZipPostalCode, 0);
				}
				if (nopAddress.City != null)
				{
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "City", nopAddress.City, 0);
				}
				if (nopAddress.County != null)
				{
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "County", nopAddress.County, 0);
				}
				if (nopCustomer.DayOfBirth != null)
				{
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "DateOfBirth", nopCustomer.DayOfBirth, 0);
				}
				if (nopCustomer.Gender != null)
				{
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "Gender", nopCustomer.Gender, 0);
				}
				if (nopAddress.CountryId.HasValue)
				{
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "CountryId", nopAddress.CountryId.ToString(), 0);
				}
				if (nopAddress.StateProvinceId.HasValue)
				{
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "StateProvinceId", nopAddress.StateProvinceId.ToString(), 0);
				}
				if (nopAddress.PhoneNumber != null)
				{
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "PhoneNumber", nopAddress.PhoneNumber.ToString(), 0);
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "Phone", nopAddress.PhoneNumber.ToString(), 0);
				}
				if (nopAddress.FaxNumber != null)
				{
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "FaxNumber", nopAddress.FaxNumber.ToString(), 0);
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "Fax", nopAddress.FaxNumber.ToString(), 0);
				}
			}
			catch (Exception ex)
			{
				LogManager.LogWrite(taskId, $"Customer generic attributes are not imported. Error: {ex.Message}", LogType.Error, nopCustomer.Email.ToString());
				errorList.AppendLine($"Customer generic attributes are not imported. Error: {ex.Message}");
			}
		}

		private void InsertUpdateCustomerCustomAttributes(long? taskId, SourceMapping sourceMapping, TaskMapping taskMapping, SourceItem item, NopManager nopManager, NopCustomer nopCustomer, NopAddress nopAddress, bool newItem, StringBuilder errorList)
		{
			try
			{
				string text = taskMapping.TaskCustomerCustomAttMapping.DeleteBeforeImport ? null : nopManager.SelectGenericAttributeValue(nopCustomer.Id, "Customer", "CustomCustomerAttributes");
				if (item.CustomerCustomAttributes != null && item.CustomerCustomAttributes.Any())
				{
					string str = "<Attributes>";
					foreach (SourceItem.AttributeValues item2 in item.CustomerCustomAttributes.Where((SourceItem.AttributeValues x) => x.Import))
					{
						if (!string.IsNullOrEmpty(item2.AttInfo_Name) && !string.IsNullOrEmpty(item2.AttValue_Value))
						{
							item2.AttMapId = nopManager.InsertUpdateCustomerAttribute(item2.AttInfo_Name, item2.AttInfo_OptionType, item2.AttInfo_IsRequired);
							if (item2.AttInfo_OptionType < 4)
							{
								item2.AttValueId = nopManager.InsertUpdateCustomerAttributeValue(item2.AttMapId, item2.AttValue_Value, item2.AttValue_IsPreselected);
							}
							string text2 = $"<CustomerAttribute ID=\"{item2.AttMapId}\"><CustomerAttributeValue><Value>{(string.IsNullOrEmpty(item2.AttValue_Value) ? item2.AttValueId.ToString() : item2.AttValue_Value)}</Value></CustomerAttributeValue></CustomerAttribute>";
							if (!string.IsNullOrEmpty(text) && text.IndexOf(text2) == -1)
							{
								text = text.Replace("<Attributes>", "<Attributes>" + text2);
							}
							str += text2;
						}
					}
					str += "</Attributes>";
					nopManager.InsertUpdateGenericAttribute(nopCustomer.Id, "Customer", "CustomCustomerAttributes", string.IsNullOrEmpty(text) ? str : text, 0);
				}
			}
			catch (Exception ex)
			{
				LogManager.LogWrite(taskId, $"Customer custom attributes are not imported. Error: {ex.Message}", LogType.Error, nopCustomer.Email.ToString());
				errorList.AppendLine($"Customer custom attributes are not imported. Error: {ex.Message}");
			}
		}

		private void InsertUpdateAddressCustomAttributes(long? taskId, SourceMapping sourceMapping, TaskMapping taskMapping, SourceItem item, NopManager nopManager, NopCustomer nopCustomer, NopAddress nopAddress, bool newItem, StringBuilder errorList)
		{
			try
			{
				string text = taskMapping.TaskAddressCustomAttMapping.DeleteBeforeImport ? null : nopManager.SelectAddressAttributeValue(nopAddress.Id.Value);
				if (item.AddressCustomAttributes != null && item.AddressCustomAttributes.Any())
				{
					string str = "<Attributes>";
					foreach (SourceItem.AttributeValues item2 in item.AddressCustomAttributes.Where((SourceItem.AttributeValues x) => x.Import))
					{
						if (!string.IsNullOrEmpty(item2.AttInfo_Name) && !string.IsNullOrEmpty(item2.AttValue_Value))
						{
							item2.AttMapId = nopManager.InsertUpdateAddressAttribute(item2.AttInfo_Name, item2.AttInfo_OptionType, item2.AttInfo_IsRequired);
							if (item2.AttInfo_OptionType < 4)
							{
								item2.AttValueId = nopManager.InsertUpdateCustomerAttributeValue(item2.AttMapId, item2.AttValue_Value, item2.AttValue_IsPreselected);
							}
							string text2 = $"<AddressAttribute ID=\"{item2.AttMapId}\"><AddressAttributeValue><Value>{(string.IsNullOrEmpty(item2.AttValue_Value) ? item2.AttValueId.ToString() : item2.AttValue_Value)}</Value></AddressAttributeValue></AddressAttribute>";
							if (!string.IsNullOrEmpty(text) && text.IndexOf(text2) == -1)
							{
								text = text.Replace("<Attributes>", "<Attributes>" + text2);
							}
							str += text2;
						}
					}
					str += "</Attributes>";
					nopManager.UpdateAddressCustomAttribute(nopAddress.Id.Value, string.IsNullOrEmpty(text) ? str : text);
				}
			}
			catch (Exception ex)
			{
				LogManager.LogWrite(taskId, $"Address custom attributes are not imported. Error: {ex.Message}", LogType.Error, nopCustomer.Email.ToString());
				errorList.AppendLine($"Address custom attributes are not imported. Error: {ex.Message}");
			}
		}

		private void SendLogNotification(long? taskId, TaskMapping taskMapping, StringBuilder infoList, StringBuilder errorList)
		{
			try
			{
				string[] array = RemoveDuplicates(errorList?.ToString().Split(Environment.NewLine.ToCharArray()));
				if (!string.IsNullOrEmpty(taskMapping.TaskEmailSettings.EmailHost) && !string.IsNullOrEmpty(taskMapping.TaskEmailSettings.EmailAddressFrom) && !string.IsNullOrEmpty(taskMapping.TaskEmailSettings.EmailAddressTo))
				{
					using (SmtpClient smtpClient = new SmtpClient(taskMapping.TaskEmailSettings.EmailHost))
					{
						MailMessage mailMessage = new MailMessage
						{
							From = new MailAddress(taskMapping.TaskEmailSettings.EmailAddressFrom, taskMapping.TaskEmailSettings.EmailDisplayName)
						};
						mailMessage.To.Add(new MailAddress(taskMapping.TaskEmailSettings.EmailAddressTo));
						mailMessage.Subject = taskMapping.TaskEmailSettings.EmailSubject;
						taskMapping.TaskEmailSettings.EmailBody = taskMapping.TaskEmailSettings.EmailBody.Replace("[[result]]", infoList.ToString());
						taskMapping.TaskEmailSettings.EmailBody = taskMapping.TaskEmailSettings.EmailBody.Replace("[[errors]]", ConvertStringArrayToString(array));
						mailMessage.Body = taskMapping.TaskEmailSettings.EmailBody;
						mailMessage.IsBodyHtml = false;
						smtpClient.UseDefaultCredentials = (string.IsNullOrEmpty(taskMapping.TaskEmailSettings.EmailUser) ? true : false);
						smtpClient.Port = GlobalClass.StringToInteger(taskMapping.TaskEmailSettings.EmailPort);
						smtpClient.Credentials = new NetworkCredential(taskMapping.TaskEmailSettings.EmailUser, taskMapping.TaskEmailSettings.EmailPassword);
						smtpClient.EnableSsl = taskMapping.TaskEmailSettings.EmailSSL;
						smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
						smtpClient.Send(mailMessage);
					}

				}
			}
			catch (Exception ex)
			{
				LogManager.LogWrite(taskId, $"Error sending email notification. Error: {ex.Message}", LogType.Error);
			}
		}

		private long InsertUpdateProductCrossSells(long? taskId, SourceMapping sourceMapping, TaskMapping taskMapping, SourceItem item, NopManager nopManager, NopProduct nopProduct, bool newItem, StringBuilder errorList, Dictionary<int, string> productsInStore)
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			try
			{
				int num = 0;
				if (item.CrossSellItems != null && item.CrossSellItems.Any())
				{
					foreach (SourceItem.CrossSellItem crossSellItem in item.CrossSellItems)
					{
						int num2 = 0;
						if (num == 0)
						{
							nopManager.DeleteProductCrossSellMapping(nopProduct.ProductId);
						}
						if (!string.IsNullOrEmpty(crossSellItem.ProductSKU))
						{
							string tempSku = crossSellItem.ProductSKU.ToString().ToLower().Trim();
							num2 = ((!productsInStore.ContainsValue(tempSku)) ? nopManager.SelectProductID(tempSku) : GlobalClass.StringToInteger(productsInStore.Where((KeyValuePair<int, string> x) => x.Value == tempSku).FirstOrDefault().Key));
							if (num2 > 0)
							{
								nopManager.InsertProductCrossSellMapping(nopProduct.ProductId, num2);
							}
							else
							{
								LogManager.LogWrite(taskId, $"Product CrossSell is not imported. Product not found by SKU: {tempSku}", LogType.Warning, nopProduct.Sku.ToString());
							}
							num++;
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogManager.LogWrite(taskId, $"Product CrossSells are not imported. Error: {ex.Message}", LogType.Error, nopProduct.Sku.ToString());
				errorList.AppendLine($"Product CrossSells are not imported. Error: {ex.Message}");
			}
			finally
			{
				stopwatch.Stop();
			}
			return stopwatch.ElapsedMilliseconds;
		}

		private long InsertUpdateProductRelated(long? taskId, SourceMapping sourceMapping, TaskMapping taskMapping, SourceItem item, NopManager nopManager, NopProduct nopProduct, bool newItem, StringBuilder errorList, Dictionary<int, string> productsInStore)
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			try
			{
				int num = 0;
				if (item.RelatedProductItems != null && item.RelatedProductItems.Any())
				{
					foreach (SourceItem.RelatedProductItem relatedProductItem in item.RelatedProductItems)
					{
						int num2 = 0;
						if (num == 0)
						{
							nopManager.DeleteProductCrossSellMapping(nopProduct.ProductId);
						}
						if (!string.IsNullOrEmpty(relatedProductItem.ProductSKU))
						{
							string tempSku = relatedProductItem.ProductSKU.ToString().ToLower().Trim();
							num2 = ((!productsInStore.ContainsValue(tempSku)) ? nopManager.SelectProductID(tempSku) : GlobalClass.StringToInteger(productsInStore.Where((KeyValuePair<int, string> x) => x.Value == tempSku).FirstOrDefault().Key));
							if (num2 > 0)
							{
								nopManager.InsertProductCrossSellMapping(nopProduct.ProductId, num2);
							}
							else
							{
								LogManager.LogWrite(taskId, $"Related product is not imported. Related product not found by SKU: {tempSku}", LogType.Warning, nopProduct.Sku.ToString());
							}
							num++;
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogManager.LogWrite(taskId, $"Related products are not imported. Error: {ex.Message}", LogType.Error, nopProduct.Sku.ToString());
				errorList.AppendLine($"Related products are not imported. Error: {ex.Message}");
			}
			finally
			{
				stopwatch.Stop();
			}
			return stopwatch.ElapsedMilliseconds;
		}

		public void StartTaskWishListImport(string sourceAddress, StoreData storeData, int taskId, bool scheduler, out string resultMessage, out long insertedItems, out long updatedItems, TaskMapping taskMapping = null)
		{
			bool flag = false;
			resultMessage = "";
			long foundItems = 0L;
			long num = 0L;
			insertedItems = 0L;
			updatedItems = 0L;
			int num2 = 0;
			long num3 = 0L;
			long num4 = 0L;
			long num5 = 0L;
			SourceMappingFunc sourceMappingFunc = new SourceMappingFunc();
			TaskMappingFunc taskMappingFunc = new TaskMappingFunc();
			NopManager nopManager = null;
			StringBuilder stringBuilder = new StringBuilder();
			StringBuilder stringBuilder2 = new StringBuilder();
			List<int> list = new List<int>();
			Dictionary<int, string> idSkuList = new Dictionary<int, string>();
			Dictionary<int, string> dictionary = new Dictionary<int, string>();
			bool flag2 = false;
			try
			{
				LogManager.LogWrite(taskId, "Task started.", LogType.Info);
				stringBuilder2.AppendLine("Task started: " + DateTime.Now.ToString());
				if (taskMapping == null)
				{
					taskMapping = taskMappingFunc.GetTaskMapping(taskId);
				}
				SourceDocument sourceDocument = null;
				SourceData sourceData = sourceMappingFunc.GetSourceData(taskMapping.VendorSourceId);
				byte[] fileContent = sourceMappingFunc.GetFileContent((SourceType)sourceData.SourceType, sourceData.SourceUser, sourceData.SourcePassword, string.IsNullOrEmpty(sourceAddress) ? sourceData.SourceAddress : sourceAddress, sourceData.SoapAction, sourceData.SoapRequest, sourceData.SourceEncoding, sourceData.SourceFormat == 2, sourceData.SourceContent, sourceData.AuthHeader);
				if (storeData == null)
				{
					storeData = StoreFunc.GetStoreData(taskMapping.EShopId.Value);
				}
				DatabaseManager databaseManager = new DatabaseManager(storeData.DatabaseConnString, storeData.Version);
				SourceMapping sourceMapping = sourceMappingFunc.GetSourceMapping(taskMapping.VendorSourceId);
				sourceMapping.CsvDelimeter = sourceData.CsvDelimeter;
				sourceMapping.FirstRowIndex = sourceData.FirstRowIndex;
				nopManager = new NopManager(storeData.DatabaseConnString, storeData.Version, storeData.DatabaseType);
				if (storeData.DatabaseType == DatabaseType.MSSQL && !mssql)
				{
					LogManager.LogWrite(taskId, "Task import stopped. NopTalk Pro version does not have MSSQL licence. Please contact info@vasksoft.com", LogType.Warning);
				}
				else if (storeData.DatabaseType == DatabaseType.MySQL && !mysql)
				{
					LogManager.LogWrite(taskId, "Task import stopped. NopTalk Pro version does not have MySQL licence. Please contact info@vasksoft.com", LogType.Warning);
				}
				else
				{
					if (taskMapping.DisableShop)
					{
						nopManager.CloseStore(disableStore: true);
					}
					if (fileContent != null)
					{
						sourceDocument = ((sourceData.SourceType != 4) ? new SourceDocument((FileFormat)sourceData.SourceFormat, fileContent, sourceMapping, taskMapping, sourceData) : new SourceDocument(FileFormat.AliExpress, fileContent, sourceMapping, taskMapping, sourceData));
						SourceDocument sourceDoucumentForTasks = taskMappingFunc.GetSourceDoucumentForTasks(sourceDocument, taskMapping);
						foundItems = sourceDoucumentForTasks.SourceItems.Count();
						if (taskMapping.TaskAction == 0 || taskMapping.TaskAction == 1 || taskMapping.TaskAction == 2 || taskMapping.TaskAction == 3)
						{
							try
							{
								nopManager.ExistProducts(out idSkuList);
								if (idSkuList.Count > 0)
								{
									flag2 = true;
								}
							}
							catch (Exception)
							{
							}
							RunCustomSql(taskId, taskMapping, nopManager, 1, stringBuilder);
							taskMappingFunc.UpdateTaskProgress(taskId, foundItems, num);
							List<string> list2 = new List<string>();
							string text = "";
							bool flag3 = true;
							using (List<SourceItem>.Enumerator enumerator = sourceDoucumentForTasks.SourceItems.GetEnumerator())
							{
								string productSku;
								for (; enumerator.MoveNext(); databaseManager.WishListSynchro())
								{
									SourceItem current = enumerator.Current;
									foreach (SourceItem.WishListItem wish in current.WishList)
									{
										int productId = 0;
										decimal productPrice = default(decimal);
										decimal productCost = default(decimal);
										flag = false;
										num4++;
										num5++;
										num2 = 0;
										try
										{
											try
											{
												if (num4 >= 5)
												{
													if (!taskMappingFunc.IsTaskRunning(taskId))
													{
														goto IL_06da;
													}
													taskMappingFunc.UpdateTaskProgress(taskId, foundItems, num);
													num4 = 0L;
												}
											}
											catch
											{
											}
											productSku = wish.ProductSKU.ToString().ToLower().Trim();
											if (idSkuList.ContainsValue(productSku))
											{
												productId = GlobalClass.StringToInteger(idSkuList.Where((KeyValuePair<int, string> x) => x.Value == productSku).FirstOrDefault().Key);
											}
											else
											{
												nopManager.SelectProduct(productSku, 0, 0, out productId, out productPrice, out productCost);
												if (productId > 0)
												{
													idSkuList.Add(productId, productSku);
												}
											}
											nopManager.SelectCustomer(string.IsNullOrEmpty(wish.CustomerEmail) ? 1 : 0, wish.CustomerUsername, wish.CustomerEmail, null, out num2, out int _, out int _);
											NopShoppingCartItem nopShoppingCartItem = new NopShoppingCartItem(ShoppingCartType.Wishlist);
											nopShoppingCartItem.ProductId = productId;
											nopShoppingCartItem.CustomerId = num2;
											nopShoppingCartItem.SKUID = wish.CustomSKUID;
											if (!string.IsNullOrEmpty(taskMapping.LimitedToStores))
											{
												list2 = taskMapping.LimitedToStores.Split(new char[1]
												{
													';'
												}, StringSplitOptions.RemoveEmptyEntries).ToList();
												nopShoppingCartItem.StoreId = GlobalClass.StringToInteger(list2.FirstOrDefault());
											}
											if (nopShoppingCartItem.ProductId == 0)
											{
												LogManager.LogWrite(taskId, "Product not found in the store by Product SKU=" + wish.ProductSKU, LogType.Warning);
											}
											else if (nopShoppingCartItem.CustomerId == 0)
											{
												LogManager.LogWrite(taskId, "Customer not found in the store by Email=" + wish.CustomerEmail + " or Username=" + wish.CustomerUsername, LogType.Warning);
											}
											else
											{
												if (list.Contains(nopShoppingCartItem.CustomerId))
												{
													flag3 = false;
												}
												else
												{
													flag3 = true;
													list.Add(nopShoppingCartItem.CustomerId);
												}
												if (flag3 && taskMapping.TaskWishListMapping.DeleteBeforeImport)
												{
													nopManager.DeleteWishList(nopShoppingCartItem.CustomerId);
												}
												databaseManager.NopShoppingCartItems.Add(nopShoppingCartItem);
												if (flag3)
												{
													updatedItems++;
												}
												num3++;
												if (((taskMapping.TaskAction == 1) ? num : num3) >= productsLimits && productsLimits > 0)
												{
													LogManager.LogWrite(taskId, "Task import stopped. NopTalk Pro version has customer import limit=" + productsLimits, LogType.Warning);
													goto IL_06da;
												}
											}
										}
										catch (Exception ex2)
										{
											LogManager.LogWrite(taskId, "Item import unexpected error: " + ex2.ToString(), LogType.Error);
											stringBuilder.AppendLine("Item import unexpected error: " + ex2.ToString());
											if (num5 >= productsLimits && productsLimits > 0)
											{
												LogManager.LogWrite(taskId, "Task import stopped. NopTalk Pro version has customer import limit=" + productsLimits, LogType.Warning);
												goto IL_06da;
											}
										}
									}
									IL_06da:;
								}
							}
							databaseManager.DatabaseSynchro();
							num = databaseManager.NopShoppingCartItems.Count((NopShoppingCartItem x) => x.Updated);
							RunCustomSql(taskId, taskMapping, nopManager, 2, stringBuilder);
						}
						else
						{
							LogManager.LogWrite(taskId, "Task action (insert/update) not found.", LogType.Warning);
						}
					}
					else
					{
						LogManager.LogWrite(taskId, "The source file is empty.", LogType.Warning);
					}
					try
					{
						if (taskMapping.DeleteSourceFile && sourceData.SourceType == 0)
						{
							File.Delete(sourceData.SourceAddress);
						}
					}
					catch (Exception ex3)
					{
						LogManager.LogWrite(taskId, "Error deleting file:" + ex3.ToString(), LogType.Error);
					}
					if (taskMapping.DisableShop)
					{
						nopManager.CloseStore(disableStore: false);
					}
				}
			}
			catch (Exception ex4)
			{
				LogManager.LogWrite(taskId, "Task import unexpected error: " + ex4.ToString(), LogType.Error);
				stringBuilder.AppendLine("Task import unexpected error: " + ex4.ToString());
			}
			finally
			{
				try
				{
					taskMappingFunc.UpdateTaskData(taskId, foundItems, num, DateTime.Now);
				}
				catch
				{
				}
				resultMessage = string.Format("Task finished. Result of import. Items found: " + foundItems + ". Total items imported: {0}.", num);
				LogManager.LogWrite(taskId, resultMessage, LogType.Info);
				stringBuilder2.AppendLine("Task finished: " + DateTime.Now.ToString());
				stringBuilder2.AppendLine(string.Format("Result of import: Items found: " + foundItems + ". Total items imported: {0}.", num));
				try
				{
					SendLogNotification(taskId, taskMapping, stringBuilder2, stringBuilder);
				}
				catch
				{
				}
			}
		}

		private string[] RemoveDuplicates(string[] s)
		{
			HashSet<string> hashSet = new HashSet<string>(s);
			string[] array = new string[hashSet.Count];
			hashSet.CopyTo(array);
			return array;
		}

		private string ConvertStringArrayToString(string[] array)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (array != null)
			{
				foreach (string value in array)
				{
					if (!string.IsNullOrEmpty(value))
					{
						stringBuilder.AppendLine(value);
					}
				}
			}
			return stringBuilder.ToString();
		}
	}
}
