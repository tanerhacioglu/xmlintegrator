using System;
using System.Collections.Generic;
using System.Linq;

namespace NopTalkCore
{
	public class SourceMappingHelper
	{
		public static List<SourceItemMapping> GetRepeatNode()
		{
			Guid ıd = Guid.NewGuid();
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = -1,
				FieldName = "Repeat Node"
			});
			return list;
		}

		public static List<SourceItemMapping> GetTierPricingNewItem(bool update, List<SourceItemMapping> existList)
		{
			Guid ıd = (update && existList != null && existList.Any()) ? existList.FirstOrDefault().Id : Guid.NewGuid();
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 0)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 0,
					FieldName = "Quantity For Tier Price"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 1)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 1,
					FieldName = "Price For Tier Price"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 2)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 2,
					FieldName = "Customer Role For Tier Price"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 13)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 13,
					FieldName = "Start Date Time (YYYY-MM-DD HH:MI:SS)"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 14)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 14,
					FieldName = "End Date Time (YYYY-MM-DD HH:MI:SS)"
				});
			}
			return list;
		}

		public static List<SourceItemMapping> GetProdAttNewItem(bool update, List<SourceItemMapping> existList)
		{
			Guid ıd = (update && existList != null && existList.Any()) ? existList.FirstOrDefault().Id : Guid.NewGuid();
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 4)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 4,
					FieldName = "Attribute Info - Name"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 110)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 110,
					FieldName = "Attribute Info- Text Prompt"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 106)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 106,
					FieldName = "Attribute Info- Is Required"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 108)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 108,
					FieldName = "Attribute Info- Default Value"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 107)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 107,
					FieldName = "Attribute Info- Control type"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 109)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 109,
					FieldName = "Attribute Info- Display Order"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 5)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 5,
					FieldName = "Attribute Value - Name"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 6)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 6,
					FieldName = "Attribute Value - Quantity"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 7)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 7,
					FieldName = "Attribute Value - Price Adjustment"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 101)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 101,
					FieldName = "Attribute Value - Price adjustment. Use percentage"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 102)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 102,
					FieldName = "Attribute Value - Weight adjustment"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 103)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 103,
					FieldName = "Attribute Value - Cost"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 104)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 104,
					FieldName = "Attribute Value - Is pre-selected"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 11)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 11,
					FieldName = "Attribute Value - Color"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 10)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 10,
					FieldName = "Attribute Value - Associated Picture"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 12)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 12,
					FieldName = "Attribute Value - SquarePicture"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 105)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 105,
					FieldName = "Attribute Value - Display Order"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 3)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 3,
					FieldName = "Attribute Combination - SKU"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 112)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 112,
					FieldName = "Attribute Combination - Stock Quantity"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 111)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 111,
					FieldName = "Attribute Combination - Overridden Price"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 113)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 113,
					FieldName = "Attribute Combination - Allow Out Of Stock"
				});
			}
			return list;
		}

		public static List<SourceItemMapping> ProdAttNewItemNamingOrder(List<SourceItemMapping> existList)
		{
			SourceItemMapping sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 4);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Info - Name";
				sourceItemMapping.Order = 0;
				sourceItemMapping.Group = 0;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 110);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Info- Text Prompt";
				sourceItemMapping.Order = 1;
				sourceItemMapping.Group = 0;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 106);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Info- Is Required";
				sourceItemMapping.Order = 2;
				sourceItemMapping.Group = 0;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 108);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Info- Default Value";
				sourceItemMapping.Order = 3;
				sourceItemMapping.Group = 0;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 107);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Info- Control type";
				sourceItemMapping.Order = 4;
				sourceItemMapping.Group = 0;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 109);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Info- Display Order";
				sourceItemMapping.Order = 5;
				sourceItemMapping.Group = 0;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 5);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Value - Name";
				sourceItemMapping.Order = 6;
				sourceItemMapping.Group = 1;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 6);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Value - Quantity";
				sourceItemMapping.Order = 7;
				sourceItemMapping.Group = 1;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 7);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Value - Price Adjustment";
				sourceItemMapping.Order = 8;
				sourceItemMapping.Group = 1;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 101);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Value - Price adjustment. Use percentage";
				sourceItemMapping.Order = 9;
				sourceItemMapping.Group = 1;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 102);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Value - Weight adjustment";
				sourceItemMapping.Order = 10;
				sourceItemMapping.Group = 1;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 103);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Value - Cost";
				sourceItemMapping.Order = 11;
				sourceItemMapping.Group = 1;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 104);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Value - Is pre-selected";
				sourceItemMapping.Order = 12;
				sourceItemMapping.Group = 1;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 11);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Value - Color";
				sourceItemMapping.Order = 13;
				sourceItemMapping.Group = 1;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 10);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Value - Associated Picture";
				sourceItemMapping.Order = 14;
				sourceItemMapping.Group = 1;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 12);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Value - SquarePicture";
				sourceItemMapping.Order = 15;
				sourceItemMapping.Group = 1;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 105);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Value - Display Order";
				sourceItemMapping.Order = 16;
				sourceItemMapping.Group = 1;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 3);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Combination - SKU";
				sourceItemMapping.Order = 17;
				sourceItemMapping.Group = 2;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 112);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Combination - Stock Quantity";
				sourceItemMapping.Order = 18;
				sourceItemMapping.Group = 2;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 111);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Combination - Overridden Price";
				sourceItemMapping.Order = 19;
				sourceItemMapping.Group = 2;
			}
			sourceItemMapping = existList.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 113);
			if (sourceItemMapping != null)
			{
				sourceItemMapping.FieldName = "Attribute Combination - Allow Out Of Stock";
				sourceItemMapping.Order = 20;
				sourceItemMapping.Group = 2;
			}
			return existList;
		}

		public static List<SourceItemMapping> GetProdPriceNewItem(bool update = false, List<SourceItemMapping> existList = null)
		{
			Guid ıd = (update && existList != null && existList.Any()) ? existList.FirstOrDefault().Id : Guid.NewGuid();
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 1001)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 1001,
					FieldName = "Product Cost"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 1002)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 1002,
					FieldName = "Product Price"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 1003)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 1003,
					FieldName = "Product Old Price"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 1004)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 1004,
					FieldName = "(PAngV) Base Price Enabled"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 1005)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 1005,
					FieldName = "Amount In Product"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 1006)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 1006,
					FieldName = "Unit Of Product"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 1007)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 1007,
					FieldName = "Reference Amount"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 1008)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 1008,
					FieldName = "Reference Unit"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 1010)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 1010,
					FieldName = "Tax Exempt"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 1009)))
			{
				list.Add(new SourceItemMapping
				{
					Id = ıd,
					FieldMappingForIndex = -1,
					MappingItemType = 1009,
					FieldName = "Tax Category"
				});
			}
			return list;
		}

		public static List<SourceItemMapping> GetProdSpecAttNewItem()
		{
			Guid ıd = Guid.NewGuid();
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 8,
				FieldName = "Attribute Name"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 9,
				FieldName = "Attribute Value"
			});
			return list;
		}

		public static List<SourceItemMapping> GetAddressCustomAttNewItem()
		{
			Guid ıd = Guid.NewGuid();
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 201,
				FieldName = "Attribute Name"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 202,
				FieldName = "Attribute Value"
			});
			return list;
		}

		public static List<SourceItemMapping> GetCustomerCustomAttNewItem()
		{
			Guid ıd = Guid.NewGuid();
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 203,
				FieldName = "Attribute Name"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 204,
				FieldName = "Attribute Value"
			});
			return list;
		}

		public static List<SourceItemMapping> GetProdPictureNewItem()
		{
			Guid ıd = Guid.NewGuid();
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 601,
				FieldName = "Picture Path"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 602,
				FieldName = "Picture Seo"
			});
			return list;
		}

		public static List<SourceItemMapping> GetProdInfoNewItem(bool update, List<SourceItemMapping> existList)
		{
			Guid guid = (update && existList != null && existList.Any()) ? existList.FirstOrDefault().Id : Guid.NewGuid();
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 517)))
			{
				list.Add(new SourceItemMapping
				{
					Id = Guid.NewGuid(),
					FieldMappingForIndex = -1,
					MappingItemType = 517,
					FieldName = "Product Tag"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 701)))
			{
				list.Add(new SourceItemMapping
				{
					Id = Guid.NewGuid(),
					FieldMappingForIndex = -1,
					MappingItemType = 701,
					FieldName = "Cross Sell Product SKU"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 801)))
			{
				list.Add(new SourceItemMapping
				{
					Id = Guid.NewGuid(),
					FieldMappingForIndex = -1,
					MappingItemType = 801,
					FieldName = "Related Product SKU"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 901)))
			{
				list.Add(new SourceItemMapping
				{
					Id = Guid.NewGuid(),
					FieldMappingForIndex = -1,
					MappingItemType = 901,
					FieldName = "Tier Pricing Additional Identifier (ACCID)"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 2001)))
			{
				list.Add(new SourceItemMapping
				{
					Id = Guid.NewGuid(),
					FieldMappingForIndex = -1,
					MappingItemType = 2001,
					FieldName = "Allow Back In Stock Subscriptions"
				});
			}
			if (!update || (update && !existList.Exists((SourceItemMapping x) => x.MappingItemType == 2002)))
			{
				list.Add(new SourceItemMapping
				{
					Id = Guid.NewGuid(),
					FieldMappingForIndex = -1,
					MappingItemType = 2002,
					FieldName = "Disable Buy Button"
				});
			}
			return list;
		}

		public static List<SourceItemMapping> GetCustomerNewItem()
		{
			Guid ıd = Guid.NewGuid();
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 210,
				FieldName = "User name"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 211,
				FieldName = "Email"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 212,
				FieldName = "Password"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 213,
				FieldName = "First name"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 214,
				FieldName = "Last name"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 215,
				FieldName = "Gender\n (M|F: M-Male, F-Female)"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 216,
				FieldName = "Date of birth (yyyy-MM-dd)"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 217,
				FieldName = "Company name"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 235,
				FieldName = "Country"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 236,
				FieldName = "State / province"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 237,
				FieldName = "City"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 244,
				FieldName = "County"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 238,
				FieldName = "Address 1"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 239,
				FieldName = "Address 2"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 240,
				FieldName = "Zip / postal code"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 241,
				FieldName = "Phone number"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 242,
				FieldName = "Fax number"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 218,
				FieldName = "Is tax exempt"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 219,
				FieldName = "Newsletter"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 220,
				FieldName = "Manager of vendor"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 221,
				FieldName = "Active"
			});
			return list;
		}

		public static List<SourceItemMapping> GetCustomerRoleNewItem()
		{
			Guid ıd = Guid.NewGuid();
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 251,
				FieldName = "Customer role"
			});
			return list;
		}

		public static List<SourceItemMapping> GetCustomerAddressNewItem()
		{
			Guid ıd = Guid.NewGuid();
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 231,
				FieldName = "First name"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 232,
				FieldName = "Last name"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 233,
				FieldName = "Email"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 234,
				FieldName = "Company name"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 235,
				FieldName = "Country"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 236,
				FieldName = "State / province"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 237,
				FieldName = "City"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 244,
				FieldName = "County"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 238,
				FieldName = "Address 1"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 239,
				FieldName = "Address 2"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 240,
				FieldName = "Zip / postal code"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 241,
				FieldName = "Phone number"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 242,
				FieldName = "Fax number"
			});
			return list;
		}

		public static List<SourceItemMapping> GetCustomerOtherNewItem()
		{
			Guid ıd = Guid.NewGuid();
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 261,
				FieldName = "Custom Customer.Address3"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 263,
				FieldName = "Custom Customer.SAPCustomer"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 264,
				FieldName = "Custom Customer.GPCustomer"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 265,
				FieldName = "Custom Address.SAPCustomer"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 266,
				FieldName = "Custom Address.MainAddress"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 262,
				FieldName = "Custom Address.AddressName"
			});
			return list;
		}

		public static List<SourceItemMapping> GetWishListNewItem()
		{
			Guid ıd = Guid.NewGuid();
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 3001,
				FieldName = "Product SKU"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 3003,
				FieldName = "Customer Email"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 3002,
				FieldName = "Customer UserName"
			});
			list.Add(new SourceItemMapping
			{
				Id = ıd,
				FieldMappingForIndex = -1,
				MappingItemType = 3004,
				FieldName = "Custom field SKUID"
			});
			return list;
		}
	}
}
