using NopTalk;
using System;
using System.Data;
using System.Data.SqlServerCe;

namespace NopTalkCore
{
	public class LogManager
	{
		public static async void LogWrite(long? taskId, string messageText, LogType logType, string productSku = null)
		{
			SqlCeConnection sqlConn = null;
			SqlCeCommand comm = null;
			try
			{
				if (!string.IsNullOrEmpty(productSku) && logType == LogType.Error)
				{
					messageText = "Product SKU: " + productSku + ". " + messageText;
				}
				DataSettingsManager dataSettingsManager = new DataSettingsManager();
				sqlConn = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
				string sqlString = "\r\n            INSERT INTO [Log]\r\n               ([TaskId]\r\n               ,[MessageText]\r\n               ,[LogTypeId]\r\n               ,[CreateDate]\r\n               )  \r\n            VALUES\r\n               (@TaskId\r\n               ,@MessageText\r\n               ,@LogTypeId\r\n               ,@CreateDate\r\n               )";
				comm = new SqlCeCommand(sqlString, sqlConn)
				{
					Parameters = 
					{
						new SqlCeParameter("@TaskId", taskId.ReplaceToParam()),
						new SqlCeParameter("@MessageText", messageText),
						new SqlCeParameter("@LogTypeId", (int)logType),
						new SqlCeParameter("@CreateDate", DateTime.Now)
					}
				};
				sqlConn.Open();
				comm.ExecuteNonQuery();
				sqlConn.Close();
			}
			catch
			{
			}
			finally
			{
				if (sqlConn != null && sqlConn.State == ConnectionState.Open)
				{
					sqlConn.Close();
				}
				comm?.Dispose();
			}
		}

		public static void LogClear(int daysKeep)
		{
			SqlCeConnection sqlCeConnection = null;
			SqlCeCommand sqlCeCommand = null;
			try
			{
				DataSettingsManager dataSettingsManager = new DataSettingsManager();
				sqlCeConnection = new SqlCeConnection(dataSettingsManager.LoadSettings().DataConnectionString);
				while (true)
				{
					string commandText = "\r\n                            DELETE FROM [Log]\r\n                            WHERE Log.LogTypeId >-1 AND (datediff(d, [CreateDate] , getdate()) >= @days OR [CreateDate] IS NULL)";
					if (daysKeep < 1)
					{
						commandText = "\r\n                        DELETE FROM [Log] Where Id in ( select Top(1500) Id from [Log])";
					}
					sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
					if (daysKeep > 0)
					{
						sqlCeCommand.Parameters.Add(new SqlCeParameter("@days", daysKeep));
					}
					sqlCeConnection.Open();
					sqlCeCommand.ExecuteNonQuery();
					sqlCeConnection.Close();
					string commandText2 = "\r\n                            SELECT COUNT(*) FROM [Log]\r\n                            WHERE Log.LogTypeId >-1 AND (datediff(d, [CreateDate] , getdate()) >= @days OR [CreateDate] IS NULL)";
					if (daysKeep < 1)
					{
						commandText2 = "\r\n                        SELECT COUNT(*) FROM [Log] Where Id in ( select Top(1500) Id from [Log])";
					}
					SqlCeCommand sqlCeCommand2 = new SqlCeCommand(commandText2, sqlCeConnection);
					if (daysKeep > 0)
					{
						sqlCeCommand2.Parameters.Add(new SqlCeParameter("@days", daysKeep));
					}
					int num = 0;
					sqlCeConnection.Open();
					SqlCeDataReader sqlCeDataReader = sqlCeCommand2.ExecuteReader();
					if (!sqlCeDataReader.Read())
					{
						break;
					}
					if (sqlCeDataReader.GetInt32(0) == 0)
					{
						sqlCeConnection.Close();
						return;
					}
					sqlCeConnection.Close();
				}
				sqlCeConnection.Close();
			}
			catch (Exception)
			{
			}
			finally
			{
				if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
				sqlCeCommand?.Dispose();
			}
		}
	}
}
