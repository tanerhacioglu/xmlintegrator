using MySql.Data.MySqlClient;
using NopTalk;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace NopTalkCore
{
	public class NopManager
	{
		private string _connString = null;

		private NopCommerceVer _nopCommerceVersion = NopCommerceVer.Ver35;

		private DatabaseType _databaseType = DatabaseType.MSSQL;

		private DatabaseManager _databaseManager;

		public NopManager(string connString, NopCommerceVer nopCommerceVersion, DatabaseType databaseType = DatabaseType.MSSQL)
		{
			_connString = connString;
			_nopCommerceVersion = nopCommerceVersion;
			_databaseType = databaseType;
			_databaseManager = new DatabaseManager(connString, nopCommerceVersion);
		}

		public void UpdateProduct(NopProduct pProduct, TaskMapping taskMapping)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			if (taskMapping.ProdNameAction)
			{
				dictionary.Add("Name", pProduct.Name);
			}
			if (taskMapping.ProdShortDescAction)
			{
				dictionary.Add("ShortDescription", pProduct.ShortDescription);
			}
			if (taskMapping.ProdFullDescAction)
			{
				dictionary.Add("FullDescription", pProduct.FullDescription);
			}
			if (taskMapping.ProdManPartNumAction)
			{
				dictionary.Add("ManufacturerPartNumber", pProduct.ManufacturerPartNumber);
			}
			if (taskMapping.ProdStockAction)
			{
				dictionary.Add("StockQuantity", pProduct.StockQuantity);
			}
			if (taskMapping.SeoMetaKeyAction)
			{
				dictionary.Add("MetaKeywords", pProduct.MetaKeywords);
			}
			if (taskMapping.SeoMetaDescAction)
			{
				dictionary.Add("MetaDescription", pProduct.MetaDescription);
			}
			if (taskMapping.SeoMetaTitleAction)
			{
				dictionary.Add("MetaTitle", pProduct.MetaTitle);
			}
			if (taskMapping.VendorAction)
			{
				dictionary.Add("VendorId", pProduct.VendorId);
			}
			if (taskMapping.WeightAction)
			{
				dictionary.Add("Weight", pProduct.Weight);
			}
			if (taskMapping.LengthAction)
			{
				dictionary.Add("Length", pProduct.Length);
			}
			if (taskMapping.WidthAction)
			{
				dictionary.Add("Width", pProduct.Width);
			}
			if (taskMapping.HeightAction)
			{
				dictionary.Add("Height", pProduct.Height);
			}
			if (taskMapping.GtinAction)
			{
				dictionary.Add("Gtin", pProduct.Gtin);
			}
			if (taskMapping.UnpublishProducts)
			{
				dictionary.Add("AdminComment", pProduct.AdminComment);
			}
			if (taskMapping.ProductPublish.HasValue)
			{
				dictionary.Add("Published", pProduct.Published);
			}
			if (taskMapping.ProdSettingsDeliveryDateAction)
			{
				dictionary.Add("DeliveryDateId", pProduct.DeliveryDateId);
			}
			if (taskMapping.ManageInventoryMethodId.HasValue)
			{
				dictionary.Add("ManageInventoryMethodId", pProduct.ManageInventoryMethodId);
			}
			if (taskMapping.BackorderModeId.HasValue && taskMapping.BackorderModeAction)
			{
				dictionary.Add("BackorderModeId", pProduct.BackorderModeId);
			}
			if (taskMapping.LowStockActivityId.HasValue && taskMapping.LowStockActivityAction)
			{
				dictionary.Add("LowStockActivityId", pProduct.LowStockActivityId);
			}
			if (taskMapping.AllowCustomerReviews.HasValue)
			{
				dictionary.Add("AllowCustomerReviews", pProduct.AllowCustomerReviews);
			}
			if (taskMapping.DisplayAvailability.HasValue)
			{
				dictionary.Add("DisplayStockAvailability", pProduct.DisplayStockAvailability);
			}
			if (taskMapping.DisplayStockQuantity.HasValue)
			{
				dictionary.Add("DisplayStockQuantity", pProduct.DisplayStockQuantity);
			}
			if (taskMapping.ProdSettingsMinStockQtyAction)
			{
				dictionary.Add("MinStockQuantity", pProduct.MinStockQuantity);
			}
			if (taskMapping.ProdSettingsNotifyQtyAction)
			{
				dictionary.Add("NotifyAdminForQuantityBelow", pProduct.NotifyAdminForQuantityBelow);
			}
			if (taskMapping.ProdSettingsMinStockQtyAction)
			{
				dictionary.Add("OrderMinimumQuantity", pProduct.OrderMinimumQuantity);
			}
			if (taskMapping.ProdSettingsMaxCartQtyAction)
			{
				dictionary.Add("OrderMaximumQuantity", pProduct.OrderMaximumQuantity);
			}
			if (taskMapping.ProdSettingsAllowedQtyAction)
			{
				dictionary.Add("AllowedQuantities", pProduct.AllowedQuantities ?? DBNull.Value);
			}
			if (taskMapping.ProdSettingsShippingEnabledAction)
			{
				dictionary.Add("IsShipEnabled", pProduct.IsShipEnabled);
			}
			if (taskMapping.ProdSettingsFreeShippingAction)
			{
				dictionary.Add("IsFreeShipping", pProduct.IsFreeShipping);
			}
			if (taskMapping.ProdSettingsShipSeparatelyAction)
			{
				dictionary.Add("ShipSeparately", pProduct.ShipSeparately);
			}
			if (taskMapping.ProdSettingsShippingChargeAction)
			{
				dictionary.Add("AdditionalShippingCharge", pProduct.AdditionalShippingCharge);
			}
			if (!pProduct.LeaveProductType)
			{
				if (pProduct.ParentGroupedProductId > 0 && pProduct.ParentGroupedProductId == pProduct.ProductId)
				{
					pProduct.ParentGroupedProductId = 0;
				}
				if (!taskMapping.ProductTemplateId.HasValue || pProduct.ParentGroupedProductId > 0)
				{
					dictionary.Add("ParentGroupedProductId", pProduct.ParentGroupedProductId);
				}
				dictionary.Add("ProductTemplateId", pProduct.ProductTemplateId);
				dictionary.Add("ProductTypeId", pProduct.ProductTypeId);
			}
			if (taskMapping.ProdSettingsVisibleIndividuallyAction)
			{
				dictionary.Add("VisibleIndividually", pProduct.VisibleIndividually);
			}
			if (taskMapping.WarehouseId.HasValue)
			{
				dictionary.Add("WarehouseId", pProduct.WarehouseId);
			}
			if (taskMapping.TaskTierPricingMapping?.TaskTierPricingMappingItems?.FirstOrDefault((TaskItemMapping x) => x.FieldImport) != null)
			{
				dictionary.Add("HasTierPrices", pProduct.HasTierPrices);
			}
			if (taskMapping.TaskProductPriceMapping.ProductPrice_TaxCategory_Import)
			{
				dictionary.Add("TaxCategoryId", pProduct.TaxCategoryId);
			}
			if (taskMapping.TaskProductPriceMapping.ProductPrice_TaxExempt_Import)
			{
				dictionary.Add("IsTaxExempt", pProduct.IsTaxExempt);
			}
			if (taskMapping.TaskProductPriceMapping.ProductPrice_ProductPrice_Import)
			{
				dictionary.Add("Price", pProduct.Price);
			}
			if (taskMapping.TaskProductPriceMapping.ProductPrice_ProductCost_Import)
			{
				dictionary.Add("ProductCost", pProduct.ProductCost);
			}
			if (taskMapping.TaskProductPriceMapping.ProductPrice_ProductOldPrice_Import)
			{
				dictionary.Add("OldPrice", pProduct.OldPrice);
			}
			if (taskMapping.TaskProductPriceMapping.ProductPrice_BasepriceEnabled_Import)
			{
				dictionary.Add("BasepriceEnabled", pProduct.BasepriceEnabled);
			}
			if (taskMapping.TaskProductPriceMapping.ProductPrice_BasepriceAmount_Import)
			{
				dictionary.Add("BasepriceAmount", pProduct.BasepriceAmount);
			}
			if (taskMapping.TaskProductPriceMapping.ProductPrice_BasepriceBaseAmount_Import)
			{
				dictionary.Add("BasepriceBaseAmount", pProduct.BasepriceBaseAmount);
			}
			if (taskMapping.TaskProductPriceMapping.ProductPrice_BasepriceUnitId_Import)
			{
				dictionary.Add("BasepriceUnitId", pProduct.BasepriceUnitId);
			}
			if (taskMapping.TaskProductPriceMapping.ProductPrice_BasepriceBaseUnitId_Import)
			{
				dictionary.Add("BasepriceBaseUnitId", pProduct.BasepriceBaseUnitId);
			}
			if (taskMapping.TaskProductInfoMapping.ProductStock_AllowBackInStockSubscriptions_Import)
			{
				dictionary.Add("AllowBackInStockSubscriptions", pProduct.AllowBackInStockSubscriptions);
			}
			if (taskMapping.TaskProductInfoMapping.ProductStock_DisableBuyButton_Import)
			{
				dictionary.Add("DisableBuyButton", pProduct.DisableBuyButton);
			}
			if (!string.IsNullOrEmpty(taskMapping.LimitedToStores))
			{
				dictionary.Add("LimitedToStores", pProduct.LimitedToStores);
			}
			if (pProduct.SubjectToAcl.HasValue)
			{
				dictionary.Add("SubjectToAcl", pProduct.SubjectToAcl);
			}
			dictionary.Add("AllowAddingOnlyExistingAttributeCombinations", pProduct.AllowAddingOnlyExistingAttributeCombinations);
			dictionary.Add("Deleted", pProduct.Deleted);
			dictionary.Add("UpdatedOnUtc", DateTime.Now);
			if (dictionary.Count > 0)
			{
				SqlUpdate("Product", dictionary, "Id=" + pProduct.ProductId);
			}
		}

		public void UpdateCustomer(NopCustomer pCustomer, TaskMapping taskMapping)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			if (pCustomer.Email != null)
			{
				dictionary.Add("Email", pCustomer.Email);
			}
			if (pCustomer.Username != null)
			{
				dictionary.Add("Username", pCustomer.Username);
			}
			if (!string.IsNullOrEmpty(taskMapping.LimitedToStores))
			{
				dictionary.Add("RegisteredInStoreId", pCustomer.RegisteredInStoreId);
			}
			if (pCustomer.SAPCustomer != null)
			{
				dictionary.Add("SAPCustomer", pCustomer.SAPCustomer.ReplaceToParam());
			}
			if (pCustomer.GPCustomer != null)
			{
				dictionary.Add("GPCustomer", pCustomer.GPCustomer.ReplaceToParam());
			}
			if (pCustomer.IsTaxExempt.HasValue)
			{
				dictionary.Add("IsTaxExempt", pCustomer.IsTaxExempt.ReplaceToParam());
			}
			if (pCustomer.Active.HasValue)
			{
				dictionary.Add("Active", pCustomer.Active.ReplaceToParam());
			}
			if (pCustomer.BillingAddress_Id.HasValue)
			{
				dictionary.Add("BillingAddress_Id", pCustomer.BillingAddress_Id.ReplaceToParam());
			}
			if (pCustomer.ShippingAddress_Id.HasValue)
			{
				dictionary.Add("ShippingAddress_Id", pCustomer.ShippingAddress_Id.ReplaceToParam());
			}
			if (pCustomer.VendorId.HasValue)
			{
				dictionary.Add("VendorId", pCustomer.VendorId.ReplaceToParam());
			}
			if (dictionary.Count > 0)
			{
				SqlUpdate("Customer", dictionary, "Id=" + pCustomer.Id);
			}
		}

		public void UnpublishNotUpdatedProducts(string AdminComment, DateTime taskStartTime)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				UnpublishNotUpdatedProductsMySQL(AdminComment, taskStartTime);
			}
			else
			{
				UnpublishNotUpdatedProductsMSSQL(AdminComment, taskStartTime);
			}
		}

		public void SqlUpdate(string table, Dictionary<string, object> values, string where)
		{
			if (values.Count > 0)
			{
				if (_databaseType == DatabaseType.MySQL)
				{
					SqlUpdateMySQL(table, values, where);
				}
				else
				{
					SqlUpdateMSSQL(table, values, where);
				}
			}
		}

		public int InsertProduct(NopProduct pProduct)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertProductMySQL(pProduct);
			}
			return InsertProductMSSQL(pProduct);
		}

		public int InsertUpdateUrlRecord(string entityName, int entityId, string slug)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertUpdateUrlRecordMySQL(entityName, entityId, slug);
			}
			return InsertUpdateUrlRecordMSSQL(entityName, entityId, slug);
		}

		public int InsertUpdateLocalizedProperty(int EntityId, int LanguageId, string LocaleKeyGroup, string LocaleKey, string LocaleValue)
		{
			if (string.IsNullOrEmpty(LocaleValue))
			{
				return 0;
			}
			int num = SelectLocalizedProperty(EntityId, LanguageId, LocaleKeyGroup, LocaleKey);
			if (num > 0)
			{
				UpdateLocalizedProperty(num, LocaleValue);
			}
			else
			{
				num = InsertLocalizedProperty(EntityId, LanguageId, LocaleKeyGroup, LocaleKey, LocaleValue);
			}
			return num;
		}

		public int SelectLocalizedProperty(int EntityId, int LanguageId, string LocaleKeyGroup, string LocaleKey)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return SelectLocalizedPropertyMySQL(EntityId, LanguageId, LocaleKeyGroup, LocaleKey);
			}
			return SelectLocalizedPropertyMSSQL(EntityId, LanguageId, LocaleKeyGroup, LocaleKey);
		}

		private int InsertLocalizedProperty(int EntityId, int LanguageId, string LocaleKeyGroup, string LocaleKey, string LocaleValue)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertLocalizedPropertyMySQL(EntityId, LanguageId, LocaleKeyGroup, LocaleKey, LocaleValue);
			}
			return InsertLocalizedPropertyMSSQL(EntityId, LanguageId, LocaleKeyGroup, LocaleKey, LocaleValue);
		}

		public void UpdateLocalizedProperty(int ItemId, string LocaleValue)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				UpdateLocalizedPropertyMySQL(ItemId, LocaleValue);
			}
			else
			{
				UpdateLocalizedPropertyMSSQL(ItemId, LocaleValue);
			}
		}

		public void DeleteUrlRecord(string entityName, int entityId, string slug)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteUrlRecordMySQL(entityName, entityId, slug);
			}
			else
			{
				DeleteUrlRecordMSSQL(entityName, entityId, slug);
			}
		}

		public int SelectUrlRecordById(string entityName, int entityId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return SelectUrlRecordByIdMySQL(entityName, entityId);
			}
			return SelectUrlRecordByIdMSSQL(entityName, entityId);
		}

		public int SelectUrlRecordBySlug(string searchValue)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return SelectUrlRecordBySlugMySQL(searchValue);
			}
			return SelectUrlRecordBySlugMSSQL(searchValue);
		}

		public int SelectProductID(string productSKU)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return SelectProductIDMySQL(productSKU);
			}
			return SelectProductIDMSSQL(productSKU);
		}

		public void SelectProduct(string searchValue, int vendorId, int productIdInStore, out int productId, out decimal productPrice, out decimal productCost)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				SelectProductMySQL(searchValue, vendorId, productIdInStore, out productId, out productPrice, out productCost);
			}
			else
			{
				SelectProductMSSQL(searchValue, vendorId, productIdInStore, out productId, out productPrice, out productCost);
			}
		}

		public int GetTaxCategory(string searchValue)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetTaxCategoryMySQL(searchValue);
			}
			return GetTaxCategoryMSSQL(searchValue);
		}

		private int InsertTaxCategory(string categoryName)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertTaxCategoryMySQL(categoryName);
			}
			return InsertTaxCategoryMSSQL(categoryName);
		}

		public int GetManufacturer(string searchValue, bool limitedToStores)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetManufacturerMySQL(searchValue, limitedToStores);
			}
			return GetManufacturerMSSQL(searchValue, limitedToStores);
		}

		public int GetVendor(string searchValue)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetVendorMySQL(searchValue);
			}
			return GetVendorMSSQL(searchValue);
		}

		private int InsertVendor(string name)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertVendorMySQL(name);
			}
			return InsertVendorMSSQL(name);
		}

		public int GetDeliveryDate(string searchValue)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetDeliveryDateMySQL(searchValue);
			}
			return GetDeliveryDateMSSQL(searchValue);
		}

		private int InsertDeliveryDate(string name)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertDeliveryDateMySQL(name);
			}
			return InsertDeliveryDateMSSQL(name);
		}

		private int InsertManufacter(string manufacterName, bool limitedToStores)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertManufacterMySQL(manufacterName, limitedToStores);
			}
			return InsertManufacterMSSQL(manufacterName, limitedToStores);
		}

		public int GetCategory(string searchValue, int paretnCategoryId, bool limitedToStores, out bool isNew)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetCategoryMySQL(searchValue, paretnCategoryId, limitedToStores, out isNew);
			}
			return GetCategoryMSSQL(searchValue, paretnCategoryId, limitedToStores, out isNew);
		}

		private int InsertCategory(string categoryName, int paretnCategoryId, bool limitedToStores)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertCategoryMySQL(categoryName, paretnCategoryId, limitedToStores);
			}
			return InsertCategoryMSSQL(categoryName, paretnCategoryId, limitedToStores);
		}

		public int GetProductAttribute(string searchValue)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetProductAttributeMySQL(searchValue);
			}
			return GetProductAttributeMSSQL(searchValue);
		}

		public int GetSpecAttribute(string searchValue)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetSpecAttributeMySQL(searchValue);
			}
			return GetSpecAttributeMSSQL(searchValue);
		}

		public int GetSpecAttributeOption(int specificationAttributeId, string searchValue)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetSpecAttributeOptionMySQL(specificationAttributeId, searchValue);
			}
			return GetSpecAttributeOptionMSSQL(specificationAttributeId, searchValue);
		}

		private int InsertSpecAttributeOption(int specificationAttributeId, string productAttributeName)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertSpecAttributeOptionMySQL(specificationAttributeId, productAttributeName);
			}
			return InsertSpecAttributeOptionMSSQL(specificationAttributeId, productAttributeName);
		}

		private int InsertSpecAttribute(string productAttributeName)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertSpecAttributeMySQL(productAttributeName);
			}
			return InsertSpecAttributeMSSQL(productAttributeName);
		}

		private int InsertProductAttribute(string productAttributeName)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertProductAttributeMySQL(productAttributeName);
			}
			return InsertProductAttributeMSSQL(productAttributeName);
		}

		public List<int> ExistProductImages(int productId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return ExistProductImagesMySQL(productId);
			}
			return ExistProductImagesMSSQL(productId);
		}

		public List<int> ExistProductImage(int productId, int pictureId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return ExistProductImageMySQL(productId, pictureId);
			}
			return ExistProductImageMSSQL(productId, pictureId);
		}

		public void ExistProducts(out Dictionary<int, string> idSkuList)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				ExistProductsMySQL(out idSkuList);
			}
			else
			{
				ExistProductsMSSQL(out idSkuList);
			}
		}

		public void DeleteProduct(int productId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteProductMySQL(productId);
			}
			else
			{
				DeleteProductMSSQL(productId);
			}
		}

		public void DeleteProductImages(int productId, bool deleteFromFile, string pictureSavePath, int pictureId, bool deleteIfNotMapped)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteProductImagesMySQL(productId, deleteFromFile, pictureSavePath, pictureId, deleteIfNotMapped);
			}
			else
			{
				DeleteProductImagesMSSQL(productId, deleteFromFile, pictureSavePath, pictureId, deleteIfNotMapped);
			}
		}

		public void DeleteProductImagesFromFileSystem(int productId, string pictureSavePath, int existingPictureId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteProductImagesFromFileSystemMySQL(productId, pictureSavePath, existingPictureId);
			}
			else
			{
				DeleteProductImagesFromFileSystemMSSQL(productId, pictureSavePath, existingPictureId);
			}
		}

		public void DeleteProductManufacturerMapping(int productId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteProductManufacturerMappingMySQL(productId);
			}
			else
			{
				DeleteProductManufacturerMappingMSSQL(productId);
			}
		}

		public void DeleteProductAttributes(int productId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteProductAttributesMySQL(productId);
			}
			else
			{
				DeleteProductAttributesMSSQL(productId);
			}
		}

		public void DeleteProductAttributesMapping(int productId, int productAttributeId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteProductAttributesMappingMySQL(productId, productAttributeId);
			}
			else
			{
				DeleteProductAttributesMappingMSSQL(productId, productAttributeId);
			}
		}

		public void DeleteSpecAttributesMapping(int productId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteSpecAttributesMappingMySQL(productId);
			}
			else
			{
				DeleteSpecAttributesMappingMSSQL(productId);
			}
		}

		public int GetSpecAttributeMapping(int productId, int productOptionId, int? attributeTypeId, string customValue, int showOnProductPage, int allowFiltering)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetSpecAttributeMappingMySQL(productId, productOptionId, attributeTypeId, customValue, showOnProductPage, allowFiltering);
			}
			return GetSpecAttributeMappingMSSQL(productId, productOptionId, attributeTypeId, customValue, showOnProductPage, allowFiltering);
		}

		public int InsertSpecAttributeMapping(int productId, int productOptionId, int? attributeTypeId, string customValue, int showOnProductPage, int allowFiltering)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertSpecAttributeMappingMySQL(productId, productOptionId, attributeTypeId, customValue, showOnProductPage, allowFiltering);
			}
			return InsertSpecAttributeMappingMSSQL(productId, productOptionId, attributeTypeId, customValue, showOnProductPage, allowFiltering);
		}

		public int GetProductAttributeMapping(TaskProductAttributesMapping taskProductAttributesMapping, int productId, int productAttributeId, int attributeControlType, string defaultValue, bool isRequired, string textPrompt, int displayOrder)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetProductAttributeMappingMySQL(taskProductAttributesMapping, productId, productAttributeId, attributeControlType, defaultValue, isRequired, textPrompt, displayOrder);
			}
			return GetProductAttributeMappingMSSQL(taskProductAttributesMapping, productId, productAttributeId, attributeControlType, defaultValue, isRequired, textPrompt, displayOrder);
		}

		public int InsertProductAttributeMapping(int productId, int productAttributeId, int attributeControlType, string defaultValue, bool isRequired, string textPrompt, int displayOrder)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertProductAttributeMappingMySQL(productId, productAttributeId, attributeControlType, defaultValue, isRequired, textPrompt, displayOrder);
			}
			return InsertProductAttributeMappingMSSQL(productId, productAttributeId, attributeControlType, defaultValue, isRequired, textPrompt, displayOrder);
		}

		public void UpdateProductAttributeMapping(TaskProductAttributesMapping taskProductAttributesMapping, int productAttributeMappingId, int attributeControlType, string defaultValue, bool isRequired, string textPrompt, int displayOrder)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			if (taskProductAttributesMapping.ProdAttInfo_ControlType_Import)
			{
				dictionary.Add("AttributeControlTypeId", attributeControlType);
			}
			if (taskProductAttributesMapping.ProdAttInfo_DefaultValue_Import)
			{
				if (taskProductAttributesMapping.ProdAttInfo_ControlType_Import)
				{
					if (attributeControlType == 30)
					{
						dictionary.Add("DefaultValue", Guid.NewGuid());
					}
					else if (!string.IsNullOrEmpty(defaultValue) && attributeControlType == 4)
					{
						dictionary.Add("DefaultValue", defaultValue);
					}
					else
					{
						dictionary.Add("DefaultValue", DBNull.Value);
					}
				}
				else
				{
					dictionary.Add("DefaultValue", defaultValue);
				}
			}
			if (taskProductAttributesMapping.ProdAttInfo_TextPrompt_Import)
			{
				dictionary.Add("TextPrompt", textPrompt);
			}
			if (taskProductAttributesMapping.ProdAttInfo_DisplayOrder_Import)
			{
				dictionary.Add("DisplayOrder", displayOrder);
			}
			if (taskProductAttributesMapping.ProdAttInfo_IsRequired_Import)
			{
				dictionary.Add("IsRequired", isRequired);
			}
			if (dictionary.Count > 0)
			{
				SqlUpdate("Product_ProductAttribute_Mapping", dictionary, "Id=" + productAttributeMappingId);
			}
		}

		public void DeleteProductAttributeValues(int productAttributeMappingId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteProductAttributeValuesMySQL(productAttributeMappingId);
			}
			else
			{
				DeleteProductAttributeValuesMSSQL(productAttributeMappingId);
			}
		}

		public int InsertUpdateProductAttributeValue(TaskProductAttributesMapping taskProductAttributesMapping, int productId, bool deleteFromFile, string filePath, int productAttributeMappingId, string productAttributeName, int quantity, byte isPreselected, string color, int pictureId, int pictureSquareId, int displayOrder, decimal priceAdjustment, decimal cost, bool priceAdjustmentUsePercentage, decimal weightAdjustment)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertUpdateProductAttributeValueMySQL(taskProductAttributesMapping, productId, deleteFromFile, filePath, productAttributeMappingId, productAttributeName, quantity, isPreselected, color, pictureId, pictureSquareId, displayOrder, priceAdjustment, cost, priceAdjustmentUsePercentage, weightAdjustment);
			}
			return InsertUpdateProductAttributeValueMSSQL(taskProductAttributesMapping, productId, deleteFromFile, filePath, productAttributeMappingId, productAttributeName, quantity, isPreselected, color, pictureId, pictureSquareId, displayOrder, priceAdjustment, cost, priceAdjustmentUsePercentage, weightAdjustment);
		}

		public void UpdateProductAttributeValue(TaskProductAttributesMapping taskProductAttributesMapping, int productAttributeValueId, string productAttributeName, int quantity, byte isPreselected, string color, int pictureId, int pictureSquareId, int displayOrder, decimal priceAdjustment, decimal cost, bool priceAdjustmentUsePercentage, decimal weightAdjustment)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			if (taskProductAttributesMapping.ProdAttValue_AssociatedPicture_Import)
			{
				dictionary.Add("PictureId", pictureId);
			}
			if (taskProductAttributesMapping.ProdAttValue_Color_Import)
			{
				dictionary.Add("ColorSquaresRgb", color);
			}
			if (taskProductAttributesMapping.ProdAttValue_Cost_Import)
			{
				dictionary.Add("Cost", cost);
			}
			if (taskProductAttributesMapping.ProdAttValue_DisplayOrder_Import)
			{
				dictionary.Add("DisplayOrder", displayOrder);
			}
			if (taskProductAttributesMapping.ProdAttValue_IsPreselected_Import)
			{
				dictionary.Add("IsPreSelected", isPreselected);
			}
			if (taskProductAttributesMapping.ProdAttValue_PriceUsePercent_Import)
			{
				dictionary.Add("PriceAdjustmentUsePercentage", priceAdjustmentUsePercentage);
			}
			if (taskProductAttributesMapping.ProdAttValue_Price_Import)
			{
				dictionary.Add("PriceAdjustment", priceAdjustment);
			}
			if (taskProductAttributesMapping.ProdAttValue_Quantity_Import)
			{
				dictionary.Add("Quantity", quantity);
			}
			if (taskProductAttributesMapping.ProdAttValue_SquarePicture_Import)
			{
				dictionary.Add("ImageSquaresPictureId", pictureSquareId);
			}
			if (taskProductAttributesMapping.ProdAttValue_Value_Import)
			{
				dictionary.Add("Name", productAttributeName);
			}
			if (taskProductAttributesMapping.ProdAttValue_WeightAdjustment_Import)
			{
				dictionary.Add("WeightAdjustment", weightAdjustment);
			}
			SqlUpdate("ProductAttributeValue", dictionary, "Id=" + productAttributeValueId);
		}

		public void DeleteProductAttributeValue(int productAttributeMappingId, string productAttributeName)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteProductAttributeValueMySQL(productAttributeMappingId, productAttributeName);
			}
			else
			{
				DeleteProductAttributeValueMSSQL(productAttributeMappingId, productAttributeName);
			}
		}

		public int InsertProductAttributeValue(int ProductAttributeId, string ProductAttributeName, int Quantity, byte IsPreselected, string color, int pictureId, int pictureSquareId, int displayOrder, decimal priceAdjustment, decimal cost, bool priceAdjustmentUsePercentage, decimal weightAdjustment)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertProductAttributeValueMySQL(ProductAttributeId, ProductAttributeName, Quantity, IsPreselected, color, pictureId, pictureSquareId, displayOrder, priceAdjustment, cost, priceAdjustmentUsePercentage, weightAdjustment);
			}
			return InsertProductAttributeValueMSSQL(ProductAttributeId, ProductAttributeName, Quantity, IsPreselected, color, pictureId, pictureSquareId, displayOrder, priceAdjustment, cost, priceAdjustmentUsePercentage, weightAdjustment);
		}

		public void DeleteProductAndCategoriesMapping(int productId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteProductAndCategoriesMappingMySQL(productId);
			}
			else
			{
				DeleteProductAndCategoriesMappingMSSQL(productId);
			}
		}

		public void DeleteProductAndCategoryMapping(int productId, int categoryId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteProductAndCategoryMappingMySQL(productId, categoryId);
			}
			else
			{
				DeleteProductAndCategoryMappingMSSQL(productId, categoryId);
			}
		}

		public int InsertProductAndCatecoryMapping(int productId, int categoryId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertProductAndCatecoryMappingMySQL(productId, categoryId);
			}
			return InsertProductAndCatecoryMappingMSSQL(productId, categoryId);
		}

		public int InsertProductAndManufacter(int productId, int manufacterId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertProductAndManufacterMySQL(productId, manufacterId);
			}
			return InsertProductAndManufacterMSSQL(productId, manufacterId);
		}

		public void DeleteProductAndManufacterMapping(int productId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteProductAndManufacterMappingMySQL(productId);
			}
			else
			{
				DeleteProductAndManufacterMappingMSSQL(productId);
			}
		}

		public string GetProductProductAttCombination(int productId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetProductProductAttCombinationMySQL(productId);
			}
			return GetProductProductAttCombinationMSSQL(productId);
		}

		public int GetProductProductAttCombination(int productId, string productSku)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetProductProductAttCombinationMySQL(productId, productSku);
			}
			return GetProductProductAttCombinationMSSQL(productId, productSku);
		}

		public void DeleteProductAttCombination(int productId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteProductAttCombinationMySQL(productId);
			}
			else
			{
				DeleteProductAttCombinationMSSQL(productId);
			}
		}

		public int InsertUpdateProductAttCombinations(int productId, SourceItem.VariantItem variant, NopCommerceVer version, int? pictureId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertUpdateProductAttCombinationsMySQL(productId, variant, version, pictureId);
			}
			return InsertUpdateProductAttCombinationsMSSQL(productId, variant, version, pictureId);
		}

		public int InsertProductAttCombination(int productId, int ProductVariantAttribute, int ProductVariantAttributeValue, int Quantity, NopCommerceVer version, int? pictureId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertProductAttCombinationMySQL(productId, ProductVariantAttribute, ProductVariantAttributeValue, Quantity, version, pictureId);
			}
			return InsertProductAttCombinationMSSQL(productId, ProductVariantAttribute, ProductVariantAttributeValue, Quantity, version, pictureId);
		}

		public void DeletePictures(int productId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeletePicturesMySQL(productId);
			}
			else
			{
				DeletePicturesMSSQL(productId);
			}
		}

		public int InsertPicture(Image image, int productId, string pictureSeo, string imageExtension, int existPictureId, int categoryId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertPictureMySQL(image, productId, pictureSeo, imageExtension, existPictureId, categoryId);
			}
			return InsertPictureMSSQL(image, productId, pictureSeo, imageExtension, existPictureId, categoryId);
		}

		public int InsertPictureBinary(Image image, int pictureId, string imageExtension)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertPictureBinaryMySQL(image, pictureId, imageExtension);
			}
			return InsertPictureBinaryMSSQL(image, pictureId, imageExtension);
		}

		public void UpdatePictureBinary(Image image, int pictureId, string imageExtension)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				UpdatePictureBinaryMySQL(image, pictureId, imageExtension);
			}
			else
			{
				UpdatePictureBinaryMSSQL(image, pictureId, imageExtension);
			}
		}

		public int InsertPictureFile(Image image, int productId, string pictureSavePath, string pictureSeo, string imageExtension, int existPictureId, int categoryId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertPictureFileMySQL(image, productId, pictureSavePath, pictureSeo, imageExtension, existPictureId, categoryId);
			}
			return InsertPictureFileMSSQL(image, productId, pictureSavePath, pictureSeo, imageExtension, existPictureId, categoryId);
		}

		public void DeleteStoreMapping(int entityId, string entityName)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteStoreMappingMySQL(entityId, entityName);
			}
			else
			{
				DeleteStoreMappingMSSQL(entityId, entityName);
			}
		}

		public int GetStoreMapping(int entityId, string entityName, int storeId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetStoreMappingMySQL(entityId, entityName, storeId);
			}
			return GetStoreMappingMSSQL(entityId, entityName, storeId);
		}

		public int InsertStoreMapping(int entityId, string entityName, int storeId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertStoreMappingMySQL(entityId, entityName, storeId);
			}
			return InsertStoreMappingMSSQL(entityId, entityName, storeId);
		}

		public void CloseStore(bool disableStore)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				CloseStoreMySQL(disableStore);
			}
			else
			{
				CloseStoreMSSQL(disableStore);
			}
		}

		public int GetCustomerRole(string searchValue)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetCustomerRoleMySQL(searchValue);
			}
			return GetCustomerRoleMSSQL(searchValue);
		}

		private int InsertCustomerRole(string name)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertCustomerRoleMySQL(name);
			}
			return InsertCustomerRoleMSSQL(name);
		}

		public int InsertAclMappingRecord(string entityName, int entityId, int roleId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertAclMappingRecordMySQL(entityName, entityId, roleId);
			}
			return InsertAclMappingRecordMSSQL(entityName, entityId, roleId);
		}

		public void DeleteAclMappings(string entityName, int productId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteAclMappingsMySQL(entityName, productId);
			}
			else
			{
				DeleteAclMappingsMSSQL(entityName, productId);
			}
		}

		public void DeleteTierPricing(int productId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteTierPricingMySQL(productId);
			}
			else
			{
				DeleteTierPricingMSSQL(productId);
			}
		}

		public void RunCustomSQL(string customSQL)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				RunCustomSQLMySQL(customSQL);
			}
			else
			{
				RunCustomSQLMSSQL(customSQL);
			}
		}

		public int GetProductTag(string searchValue)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetProductTagMySQL(searchValue);
			}
			return GetProductTagMSSQL(searchValue);
		}

		private int InsertProductTag(string tagName)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertProductTagMySQL(tagName);
			}
			return InsertProductTagMSSQL(tagName);
		}

		public int InsertProductTagMapping(int productId, int tagId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertProductTagMappingMySQL(productId, tagId);
			}
			return InsertProductTagMappingMSSQL(productId, tagId);
		}

		public void DeleteProductTagMapping(int productId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteProductTagMappingMySQL(productId);
			}
			else
			{
				DeleteProductTagMappingMSSQL(productId);
			}
		}

		public void DeleteProductCrossSellMapping(int productId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteProductCrossSellMappingMySQL(productId);
			}
			else
			{
				DeleteProductCrossSellMappingMSSQL(productId);
			}
		}

		public int InsertProductCrossSellMapping(int productId1, int productId2)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertProductCrossSellMappingMySQL(productId1, productId2);
			}
			return InsertProductCrossSellMappingMSSQL(productId1, productId2);
		}

		public void DeleteProductRelatedMapping(int productId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteProductRelatedMappingMySQL(productId);
			}
			else
			{
				DeleteProductRelatedMappingMSSQL(productId);
			}
		}

		public int InsertProductRelatedMapping(int productId1, int productId2)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertProductRelatedMappingMySQL(productId1, productId2);
			}
			return InsertProductRelatedMappingMSSQL(productId1, productId2);
		}

		public bool? IsMediaStorageDatabase()
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return IsMediaStorageDatabaseMySQL();
			}
			return IsMediaStorageDatabaseMSSQL();
		}

		public void DeleteWishList(int customerid)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteWishListMySQL(customerid);
			}
			else
			{
				DeleteWishListMSSQL(customerid);
			}
		}

		public void SelectProductMSSQL(string searchValue, int vendorId, int productIdInStore, out int productId, out decimal productPrice, out decimal productCost)
		{
			productId = 0;
			productPrice = default(decimal);
			productCost = default(decimal);
			bool flag = false;
			string cmdText = "Select Id, Price, ProductCost, Deleted From Product WITH (NOLOCK) Where Sku = @searchValue";
			if (productIdInStore == 1)
			{
				cmdText = "Select Id, Price, ProductCost, Deleted From Product WITH (NOLOCK) Where Sku = @searchValue AND vendorId=@vendorId";
			}
			if (productIdInStore == 2)
			{
				cmdText = "Select Id, Price, ProductCost, Deleted From Product WITH (NOLOCK) Where Id = @searchValue";
			}
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@searchValue", SqlDbType.NVarChar)).Value = searchValue;
					if (productIdInStore == 1)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@vendorId", vendorId));
					}
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							productId = sqlDataReader.GetInt32(0);
							productPrice = sqlDataReader.GetDecimal(1);
							productCost = sqlDataReader.GetDecimal(2);
							flag = sqlDataReader.GetBoolean(3);
						}
					}
				}
			}
			if (!flag || productId <= 0)
			{
			}
		}

		public int? SelectCountryMSSQL(string country)
		{
			if (string.IsNullOrEmpty(country))
			{
				return null;
			}
			int? result = null;
			string cmdText = "Select TOP 1 Id From [Country] WITH (NOLOCK) Where [Name] = @name OR [TwoLetterIsoCode] = @code OR [ThreeLetterIsoCode] = @code2";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@name", country.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@code", country.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@code2", country.ReplaceToParam()));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							result = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		public int? SelectStateMSSQL(string state)
		{
			if (string.IsNullOrEmpty(state))
			{
				return null;
			}
			int? result = null;
			string cmdText = "Select TOP 1 Id From [StateProvince] WITH (NOLOCK) Where [Name] = @name OR [Abbreviation] = @code";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@name", state.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@code", state.ReplaceToParam()));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							result = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		public void SqlUpdateMSSQL(string table, Dictionary<string, object> values, string where)
		{
			List<string> list = new List<string>();
			List<SqlParameter> list2 = new List<SqlParameter>();
			int num = 0;
			foreach (KeyValuePair<string, object> value in values)
			{
				string text = "@sp" + num;
				list.Add($"{value.Key}={text}");
				list2.Add(new SqlParameter(text, (value.Value == null) ? DBNull.Value : value.Value));
				num++;
			}
			string cmdText = string.Format("update {0} set {1} where {2}", table, string.Join(", ", list.ToArray()), where);
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlCommand.Parameters.AddRange(list2.ToArray());
					sqlConnection.Open();
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int GetCustomerRoleMSSQL(string searchValue)
		{
			int num = 0;
			if (string.IsNullOrEmpty(searchValue))
			{
				return num;
			}
			string cmdText = "Select Id From [CustomerRole] WITH (NOLOCK) Where Name = @searchValue";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@searchValue", searchValue));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							num = sqlDataReader.GetInt32(0);
						}
					}
					if (num == 0)
					{
						num = InsertCustomerRole(searchValue);
					}
				}
			}
			return num;
		}

		private int InsertCustomerRoleMSSQL(string name)
		{
			int result = 0;
			string format = "\r\n            INSERT INTO [CustomerRole]\r\n           ([Name]\r\n           ,[FreeShipping]\r\n           ,[TaxExempt]\r\n           ,[Active]\r\n           ,[IsSystemRole]\r\n           ,[SystemName]\r\n           ,[PurchasedWithProductId]\r\n           {0}) \r\n                VALUES\r\n           (@Name\r\n           ,@FreeShipping\r\n           ,@TaxExempt\r\n           ,@Active\r\n           ,@IsSystemRole\r\n           ,@SystemName\r\n           ,@PurchasedWithProductId\r\n               {1})";
			if (_nopCommerceVersion >= NopCommerceVer.Ver39)
			{
				string text = ",[EnablePasswordLifetime]";
				string text2 = ",@EnablePasswordLifetime";
				if (_nopCommerceVersion >= NopCommerceVer.Ver40)
				{
					text += ",[OverrideTaxDisplayType]";
					text2 += ",@OverrideTaxDisplayType";
					text += ",[DefaultTaxDisplayTypeId]";
					text2 += ",@DefaultTaxDisplayTypeId";
				}
				format = string.Format(format, text, text2);
			}
			else
			{
				format = string.Format(format, "", "");
			}
			format += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(format, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@Name", name));
					sqlCommand.Parameters.Add(new SqlParameter("@FreeShipping", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@TaxExempt", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@Active", "1"));
					sqlCommand.Parameters.Add(new SqlParameter("@IsSystemRole", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@SystemName", name));
					sqlCommand.Parameters.Add(new SqlParameter("@PurchasedWithProductId", "0"));
					if (_nopCommerceVersion >= NopCommerceVer.Ver39)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@EnablePasswordLifetime", "0"));
						if (_nopCommerceVersion >= NopCommerceVer.Ver40)
						{
							sqlCommand.Parameters.Add(new SqlParameter("@OverrideTaxDisplayType", "0"));
							sqlCommand.Parameters.Add(new SqlParameter("@DefaultTaxDisplayTypeId", "0"));
						}
					}
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void UnpublishNotUpdatedProductsMSSQL(string AdminComment, DateTime taskStartTime)
		{
			string cmdText = "\r\n             UPDATE [Product]\r\n               SET  [Published] = @Published\r\n                   ,[UpdatedOnUtc] = @date\r\n             WHERE AdminComment LIKE '%' + @AdminComment+ '%' AND [UpdatedOnUtc] < @TaskStartTime\r\n             ";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@AdminComment", AdminComment.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@date", DateTime.Now));
					sqlCommand.Parameters.Add(new SqlParameter("@TaskStartTime", taskStartTime.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@Published", "0"));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertProductMSSQL(NopProduct pProduct)
		{
			if (pProduct.ParentGroupedProductId > 0 && pProduct.ParentGroupedProductId == pProduct.ProductId)
			{
				pProduct.ParentGroupedProductId = 0;
			}
			int result = 0;
			string str = "INSERT INTO [Product]\r\n           ([ProductTypeId]\r\n           ,[ParentGroupedProductId]\r\n           ,[VisibleIndividually]\r\n           ,[Name]\r\n           ,[ShortDescription]\r\n           ,[FullDescription]\r\n           ,[AdminComment]\r\n           ,[ProductTemplateId]\r\n           ,[VendorId]\r\n           ,[ShowOnHomePage]\r\n           ,[MetaKeywords]\r\n           ,[MetaDescription]\r\n           ,[MetaTitle]\r\n           ,[AllowCustomerReviews]\r\n           ,[ApprovedRatingSum]\r\n           ,[NotApprovedRatingSum]\r\n           ,[ApprovedTotalReviews]\r\n           ,[NotApprovedTotalReviews]\r\n           ,[SubjectToAcl]\r\n           ,[LimitedToStores]\r\n           ,[Sku]\r\n           ,[ManufacturerPartNumber]\r\n           ,[Gtin]\r\n           ,[IsGiftCard]\r\n           ,[GiftCardTypeId]\r\n           ,[RequireOtherProducts]\r\n           ,[RequiredProductIds]\r\n           ,[AutomaticallyAddRequiredProducts]\r\n           ,[IsDownload]\r\n           ,[DownloadId]\r\n           ,[UnlimitedDownloads]\r\n           ,[MaxNumberOfDownloads]\r\n           ,[DownloadExpirationDays]\r\n           ,[DownloadActivationTypeId]\r\n           ,[HasSampleDownload]\r\n           ,[SampleDownloadId]\r\n           ,[HasUserAgreement]\r\n           ,[UserAgreementText]\r\n           ,[IsRecurring]\r\n           ,[RecurringCycleLength]\r\n           ,[RecurringCyclePeriodId]\r\n           ,[RecurringTotalCycles]\r\n           ,[IsRental]\r\n           ,[RentalPriceLength]\r\n           ,[RentalPricePeriodId]\r\n           ,[IsShipEnabled]\r\n           ,[IsFreeShipping]\r\n           ,[ShipSeparately]\r\n           ,[AdditionalShippingCharge]\r\n           ,[DeliveryDateId]\r\n           ,[IsTaxExempt]\r\n           ,[TaxCategoryId]\r\n           ,[IsTelecommunicationsOrBroadcastingOrElectronicServices]\r\n           ,[ManageInventoryMethodId]\r\n           ,[UseMultipleWarehouses]\r\n           ,[WarehouseId]\r\n           ,[StockQuantity]\r\n           ,[DisplayStockAvailability]\r\n           ,[DisplayStockQuantity]\r\n           ,[MinStockQuantity]\r\n           ,[LowStockActivityId]\r\n           ,[NotifyAdminForQuantityBelow]\r\n           ,[BackorderModeId]\r\n           ,[AllowBackInStockSubscriptions]\r\n           ,[OrderMinimumQuantity]\r\n           ,[OrderMaximumQuantity]\r\n           ,[AllowedQuantities]\r\n           ,[AllowAddingOnlyExistingAttributeCombinations]\r\n           ,[DisableBuyButton]\r\n           ,[DisableWishlistButton]\r\n           ,[AvailableForPreOrder]\r\n           ,[PreOrderAvailabilityStartDateTimeUtc]\r\n           ,[CallForPrice]\r\n           ,[Price]\r\n           ,[OldPrice]\r\n           ,[ProductCost]\r\n           ,[SpecialPrice]\r\n           ,[SpecialPriceStartDateTimeUtc]\r\n           ,[SpecialPriceEndDateTimeUtc]\r\n           ,[CustomerEntersPrice]\r\n           ,[MinimumCustomerEnteredPrice]\r\n           ,[MaximumCustomerEnteredPrice]\r\n           ,[HasTierPrices]\r\n           ,[HasDiscountsApplied]\r\n           ,[Weight]\r\n           ,[Length]\r\n           ,[Width]\r\n           ,[Height]\r\n           ,[AvailableStartDateTimeUtc]\r\n           ,[AvailableEndDateTimeUtc]\r\n           ,[DisplayOrder]\r\n           ,[Published]\r\n           ,[Deleted]\r\n           ,[CreatedOnUtc]\r\n           ,[UpdatedOnUtc]\r\n           {0}      \r\n            ) \r\n     VALUES\r\n           (@ProductTypeId\r\n           ,@ParentGroupedProductId\r\n           ,@VisibleIndividually\r\n           ,@Name\r\n           ,@ShortDescription\r\n           ,@FullDescription\r\n           ,@AdminComment\r\n           ,@ProductTemplateId\r\n           ,@VendorId\r\n           ,@ShowOnHomePage\r\n           ,@MetaKeywords\r\n           ,@MetaDescription\r\n           ,@MetaTitle\r\n           ,@AllowCustomerReviews\r\n           ,@ApprovedRatingSum\r\n           ,@NotApprovedRatingSum\r\n           ,@ApprovedTotalReviews\r\n           ,@NotApprovedTotalReviews\r\n           ,@SubjectToAcl\r\n           ,@LimitedToStores\r\n           ,@Sku\r\n           ,@ManufacturerPartNumber\r\n           ,@Gtin\r\n           ,@IsGiftCard\r\n           ,@GiftCardTypeId\r\n           ,@RequireOtherProducts\r\n           ,@RequiredProductIds\r\n           ,@AutomaticallyAddRequiredProducts\r\n           ,@IsDownload\r\n           ,@DownloadId\r\n           ,@UnlimitedDownloads\r\n           ,@MaxNumberOfDownloads\r\n           ,@DownloadExpirationDays\r\n           ,@DownloadActivationTypeId\r\n           ,@HasSampleDownload\r\n           ,@SampleDownloadId\r\n           ,@HasUserAgreement\r\n           ,@UserAgreementText\r\n           ,@IsRecurring\r\n           ,@RecurringCycleLength\r\n           ,@RecurringCyclePeriodId\r\n           ,@RecurringTotalCycles\r\n           ,@IsRental\r\n           ,@RentalPriceLength\r\n           ,@RentalPricePeriodId\r\n           ,@IsShipEnabled\r\n           ,@IsFreeShipping\r\n           ,@ShipSeparately\r\n           ,@AdditionalShippingCharge\r\n           ,@DeliveryDateId\r\n           ,@IsTaxExempt\r\n           ,@TaxCategoryId\r\n           ,@IsTelecommunicationsOrBroadcastingOrElectronicServices\r\n           ,@ManageInventoryMethodId\r\n           ,@UseMultipleWarehouses\r\n           ,@WarehouseId\r\n           ,@StockQuantity\r\n           ,@DisplayStockAvailability\r\n           ,@DisplayStockQuantity\r\n           ,@MinStockQuantity\r\n           ,@LowStockActivityId\r\n           ,@NotifyAdminForQuantityBelow\r\n           ,@BackorderModeId\r\n           ,@AllowBackInStockSubscriptions\r\n           ,@OrderMinimumQuantity\r\n           ,@OrderMaximumQuantity\r\n           ,@AllowedQuantities\r\n           ,@AllowAddingOnlyExistingAttributeCombinations\r\n           ,@DisableBuyButton\r\n           ,@DisableWishlistButton\r\n           ,@AvailableForPreOrder\r\n           ,@PreOrderAvailabilityStartDateTimeUtc\r\n           ,@CallForPrice\r\n           ,@Price\r\n           ,@OldPrice\r\n           ,@ProductCost\r\n           ,@SpecialPrice\r\n           ,@SpecialPriceStartDateTimeUtc\r\n           ,@SpecialPriceEndDateTimeUtc\r\n           ,@CustomerEntersPrice\r\n           ,@MinimumCustomerEnteredPrice\r\n           ,@MaximumCustomerEnteredPrice\r\n           ,@HasTierPrices\r\n           ,@HasDiscountsApplied\r\n           ,@Weight\r\n           ,@Length\r\n           ,@Width\r\n           ,@Height\r\n           ,@AvailableStartDateTimeUtc\r\n           ,@AvailableEndDateTimeUtc\r\n           ,@DisplayOrder\r\n           ,@Published\r\n           ,@Deleted\r\n           ,GETDATE()\r\n           ,GETDATE()\r\n           {1}          \r\n            )";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			if (_nopCommerceVersion == NopCommerceVer.Ver35)
			{
				str = string.Format(str, "", "");
			}
			else
			{
				string text = ",[BasepriceEnabled]\r\n                                  ,[BasepriceAmount]\r\n                                  ,[BasepriceUnitId]\r\n                                  ,[BasepriceBaseAmount]\r\n                                  ,[BasepriceBaseUnitId]";
				string text2 = ",@BasepriceEnabled\r\n                                  ,@BasepriceAmount\r\n                                  ,@BasepriceUnitId\r\n                                  ,@BasepriceBaseAmount\r\n                                  ,@BasepriceBaseUnitId";
				if (_nopCommerceVersion >= NopCommerceVer.Ver37)
				{
					text += ",[MarkAsNew]";
					text2 += ",@MarkAsNew";
				}
				if (_nopCommerceVersion >= NopCommerceVer.Ver38)
				{
					text += ",[NotReturnable]";
					text2 += ",@NotReturnable";
					if (_nopCommerceVersion >= NopCommerceVer.Ver39)
					{
						text += ",[ProductAvailabilityRangeId]";
						text2 += ",@ProductAvailabilityRangeId";
						str = str.Replace(",[SpecialPriceStartDateTimeUtc]", "");
						str = str.Replace(",@SpecialPriceStartDateTimeUtc", "");
						str = str.Replace(",[SpecialPriceEndDateTimeUtc]", "");
						str = str.Replace(",@SpecialPriceEndDateTimeUtc", "");
						str = str.Replace(",[SpecialPrice]", "");
						str = str.Replace(",@SpecialPrice", "");
					}
				}
				str = string.Format(str, text, text2);
			}
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@ProductTypeId", pProduct.ProductTypeId));
					sqlCommand.Parameters.Add(new SqlParameter("@ParentGroupedProductId", pProduct.ParentGroupedProductId));
					sqlCommand.Parameters.Add(new SqlParameter("@VisibleIndividually", pProduct.VisibleIndividually));
					sqlCommand.Parameters.Add(new SqlParameter("@Name", pProduct.Name));
					sqlCommand.Parameters.Add(new SqlParameter("@ShortDescription", pProduct.ShortDescription ?? DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@FullDescription", pProduct.FullDescription ?? DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@AdminComment", pProduct.AdminComment ?? DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@ProductTemplateId", pProduct.ProductTemplateId));
					sqlCommand.Parameters.Add(new SqlParameter("@VendorId", pProduct.VendorId));
					sqlCommand.Parameters.Add(new SqlParameter("@ShowOnHomePage", pProduct.ShowOnHomePage));
					sqlCommand.Parameters.Add(new SqlParameter("@MetaKeywords", pProduct.MetaKeywords ?? DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@MetaDescription", pProduct.MetaDescription ?? DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@MetaTitle", pProduct.MetaTitle ?? DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@AllowCustomerReviews", pProduct.AllowCustomerReviews));
					sqlCommand.Parameters.Add(new SqlParameter("@ApprovedRatingSum", pProduct.ApprovedRatingSum));
					sqlCommand.Parameters.Add(new SqlParameter("@NotApprovedRatingSum", pProduct.NotApprovedRatingSum));
					sqlCommand.Parameters.Add(new SqlParameter("@ApprovedTotalReviews", pProduct.ApprovedTotalReviews));
					sqlCommand.Parameters.Add(new SqlParameter("@NotApprovedTotalReviews", pProduct.NotApprovedTotalReviews));
					sqlCommand.Parameters.Add(new SqlParameter("@SubjectToAcl", pProduct.SubjectToAcl.GetValueOrDefault()));
					sqlCommand.Parameters.Add(new SqlParameter("@LimitedToStores", pProduct.LimitedToStores));
					sqlCommand.Parameters.Add(new SqlParameter("@Sku", pProduct.Sku ?? DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@ManufacturerPartNumber", pProduct.ManufacturerPartNumber ?? DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@Gtin", pProduct.Gtin ?? DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@IsGiftCard", pProduct.IsGiftCard));
					sqlCommand.Parameters.Add(new SqlParameter("@GiftCardTypeId", pProduct.GiftCardTypeId));
					sqlCommand.Parameters.Add(new SqlParameter("@RequiredProductIds", pProduct.RequiredProductIds ?? DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@RequireOtherProducts", pProduct.RequireOtherProducts));
					sqlCommand.Parameters.Add(new SqlParameter("@AutomaticallyAddRequiredProducts", pProduct.AutomaticallyAddRequiredProducts));
					sqlCommand.Parameters.Add(new SqlParameter("@IsDownload", pProduct.IsDownload));
					sqlCommand.Parameters.Add(new SqlParameter("@DownloadId", pProduct.DownloadId));
					sqlCommand.Parameters.Add(new SqlParameter("@UnlimitedDownloads", pProduct.UnlimitedDownloads));
					sqlCommand.Parameters.Add(new SqlParameter("@MaxNumberOfDownloads", pProduct.MaxNumberOfDownloads));
					sqlCommand.Parameters.Add(new SqlParameter("@DownloadExpirationDays", pProduct.DownloadExpirationDays));
					sqlCommand.Parameters.Add(new SqlParameter("@DownloadActivationTypeId", pProduct.DownloadActivationTypeId));
					sqlCommand.Parameters.Add(new SqlParameter("@HasSampleDownload", pProduct.HasSampleDownload));
					sqlCommand.Parameters.Add(new SqlParameter("@SampleDownloadId", pProduct.SampleDownloadId));
					sqlCommand.Parameters.Add(new SqlParameter("@HasUserAgreement", pProduct.HasUserAgreement));
					sqlCommand.Parameters.Add(new SqlParameter("@UserAgreementText", pProduct.UserAgreementText ?? DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@IsRecurring", pProduct.IsRecurring));
					sqlCommand.Parameters.Add(new SqlParameter("@RecurringCycleLength", pProduct.RecurringCycleLength));
					sqlCommand.Parameters.Add(new SqlParameter("@RecurringCyclePeriodId", pProduct.RecurringCyclePeriodId));
					sqlCommand.Parameters.Add(new SqlParameter("@RecurringTotalCycles", pProduct.RecurringTotalCycles));
					sqlCommand.Parameters.Add(new SqlParameter("@RentalPricePeriodId", pProduct.RentalPricePeriodId));
					sqlCommand.Parameters.Add(new SqlParameter("@IsRental", pProduct.IsRental));
					sqlCommand.Parameters.Add(new SqlParameter("@RentalPriceLength", pProduct.RentalPriceLength));
					sqlCommand.Parameters.Add(new SqlParameter("@IsShipEnabled", pProduct.IsShipEnabled));
					sqlCommand.Parameters.Add(new SqlParameter("@IsFreeShipping", pProduct.IsFreeShipping));
					sqlCommand.Parameters.Add(new SqlParameter("@ShipSeparately", pProduct.ShipSeparately));
					sqlCommand.Parameters.Add(new SqlParameter("@AdditionalShippingCharge", pProduct.AdditionalShippingCharge));
					sqlCommand.Parameters.Add(new SqlParameter("@DeliveryDateId", pProduct.DeliveryDateId));
					sqlCommand.Parameters.Add(new SqlParameter("@IsTaxExempt", pProduct.IsTaxExempt));
					sqlCommand.Parameters.Add(new SqlParameter("@IsTelecommunicationsOrBroadcastingOrElectronicServices", pProduct.IsTelecommunicationsOrBroadcastingOrElectronicServices));
					sqlCommand.Parameters.Add(new SqlParameter("@TaxCategoryId", pProduct.TaxCategoryId));
					sqlCommand.Parameters.Add(new SqlParameter("@ManageInventoryMethodId", pProduct.ManageInventoryMethodId));
					sqlCommand.Parameters.Add(new SqlParameter("@UseMultipleWarehouses", pProduct.UseMultipleWarehouses));
					sqlCommand.Parameters.Add(new SqlParameter("@WarehouseId", pProduct.WarehouseId));
					sqlCommand.Parameters.Add(new SqlParameter("@StockQuantity", pProduct.StockQuantity));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayStockAvailability", pProduct.DisplayStockAvailability));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayStockQuantity", pProduct.DisplayStockQuantity));
					sqlCommand.Parameters.Add(new SqlParameter("@MinStockQuantity", pProduct.MinStockQuantity));
					sqlCommand.Parameters.Add(new SqlParameter("@LowStockActivityId", pProduct.LowStockActivityId));
					sqlCommand.Parameters.Add(new SqlParameter("@NotifyAdminForQuantityBelow", pProduct.NotifyAdminForQuantityBelow));
					sqlCommand.Parameters.Add(new SqlParameter("@BackorderModeId", pProduct.BackorderModeId));
					sqlCommand.Parameters.Add(new SqlParameter("@AllowBackInStockSubscriptions", pProduct.AllowBackInStockSubscriptions));
					sqlCommand.Parameters.Add(new SqlParameter("@OrderMinimumQuantity", pProduct.OrderMinimumQuantity));
					sqlCommand.Parameters.Add(new SqlParameter("@OrderMaximumQuantity", pProduct.OrderMaximumQuantity));
					sqlCommand.Parameters.Add(new SqlParameter("@AllowedQuantities", pProduct.AllowedQuantities ?? DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@AllowAddingOnlyExistingAttributeCombinations", pProduct.AllowAddingOnlyExistingAttributeCombinations));
					sqlCommand.Parameters.Add(new SqlParameter("@DisableBuyButton", pProduct.DisableBuyButton));
					sqlCommand.Parameters.Add(new SqlParameter("@DisableWishlistButton", pProduct.DisableWishlistButton));
					sqlCommand.Parameters.Add(new SqlParameter("@AvailableForPreOrder", pProduct.AvailableForPreOrder));
					sqlCommand.Parameters.Add(new SqlParameter("@PreOrderAvailabilityStartDateTimeUtc", pProduct.PreOrderAvailabilityStartDateTimeUtc ?? DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@CallForPrice", pProduct.CallForPrice));
					sqlCommand.Parameters.Add(new SqlParameter("@Price", pProduct.Price));
					sqlCommand.Parameters.Add(new SqlParameter("@OldPrice", pProduct.OldPrice));
					sqlCommand.Parameters.Add(new SqlParameter("@ProductCost", pProduct.ProductCost));
					if (_nopCommerceVersion < NopCommerceVer.Ver39)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@SpecialPrice", pProduct.SpecialPrice));
						sqlCommand.Parameters.Add(new SqlParameter("@SpecialPriceStartDateTimeUtc", pProduct.SpecialPriceStartDateTimeUtc ?? DBNull.Value));
						sqlCommand.Parameters.Add(new SqlParameter("@SpecialPriceEndDateTimeUtc", pProduct.SpecialPriceEndDateTimeUtc ?? DBNull.Value));
					}
					sqlCommand.Parameters.Add(new SqlParameter("@CustomerEntersPrice", pProduct.CustomerEntersPrice));
					sqlCommand.Parameters.Add(new SqlParameter("@MinimumCustomerEnteredPrice", pProduct.MinimumCustomerEnteredPrice));
					sqlCommand.Parameters.Add(new SqlParameter("@MaximumCustomerEnteredPrice", pProduct.MaximumCustomerEnteredPrice));
					sqlCommand.Parameters.Add(new SqlParameter("@HasTierPrices", pProduct.HasTierPrices));
					sqlCommand.Parameters.Add(new SqlParameter("@HasDiscountsApplied", pProduct.HasDiscountsApplied));
					sqlCommand.Parameters.Add(new SqlParameter("@Weight", pProduct.Weight));
					sqlCommand.Parameters.Add(new SqlParameter("@Length", pProduct.Length));
					sqlCommand.Parameters.Add(new SqlParameter("@Width", pProduct.Width));
					sqlCommand.Parameters.Add(new SqlParameter("@Height", pProduct.Height));
					sqlCommand.Parameters.Add(new SqlParameter("@AvailableStartDateTimeUtc", pProduct.AvailableStartDateTimeUtc ?? DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@AvailableEndDateTimeUtc", pProduct.AvailableEndDateTimeUtc ?? DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", pProduct.DisplayOrder));
					sqlCommand.Parameters.Add(new SqlParameter("@Published", pProduct.Published));
					sqlCommand.Parameters.Add(new SqlParameter("@Deleted", pProduct.Deleted));
					if (_nopCommerceVersion != 0)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@BasepriceEnabled", pProduct.BasepriceEnabled));
						sqlCommand.Parameters.Add(new SqlParameter("@BasepriceAmount", pProduct.BasepriceAmount));
						sqlCommand.Parameters.Add(new SqlParameter("@BasepriceUnitId", pProduct.BasepriceUnitId));
						sqlCommand.Parameters.Add(new SqlParameter("@BasepriceBaseAmount", pProduct.BasepriceBaseAmount));
						sqlCommand.Parameters.Add(new SqlParameter("@BasepriceBaseUnitId", pProduct.BasepriceBaseUnitId));
						if (_nopCommerceVersion >= NopCommerceVer.Ver37)
						{
							sqlCommand.Parameters.Add(new SqlParameter("@MarkAsNew", "1"));
						}
						if (_nopCommerceVersion >= NopCommerceVer.Ver38)
						{
							sqlCommand.Parameters.Add(new SqlParameter("@NotReturnable", pProduct.NotReturnable));
							if (_nopCommerceVersion >= NopCommerceVer.Ver39)
							{
								sqlCommand.Parameters.Add(new SqlParameter("@ProductAvailabilityRangeId", "0"));
							}
						}
					}
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int InsertUpdateUrlRecordMSSQL(string entityName, int entityId, string slug)
		{
			int num = 0;
			slug = slug.NormalizeText();
			slug = Regex.Replace(slug, "[^\\w\\d_-]", "-", RegexOptions.None);
			if (entityName == "Product")
			{
				DeleteUrlRecord(entityName, entityId, slug);
			}
			num = SelectUrlRecordById(entityName, entityId);
			if (num == 0)
			{
				num = 1;
				while (num > 0)
				{
					num = SelectUrlRecordBySlug(slug);
					if (num > 0)
					{
						slug = slug + "-" + entityId;
					}
				}
				string cmdText = "insert into UrlRecord(EntityId,EntityName,Slug,IsActive,LanguageId) values (@entityId,@entityName,@slug,@isActive,@languageId)";
				using (SqlConnection sqlConnection = new SqlConnection(_connString))
				{
					using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
					{
						sqlConnection.Open();
						sqlCommand.Parameters.Add(new SqlParameter("@entityId", entityId));
						sqlCommand.Parameters.Add(new SqlParameter("@entityName", entityName));
						sqlCommand.Parameters.Add(new SqlParameter("@slug", slug));
						sqlCommand.Parameters.Add(new SqlParameter("@isActive", 1));
						sqlCommand.Parameters.Add(new SqlParameter("@languageId", "0"));
						sqlCommand.ExecuteNonQuery();
					}
				}
			}
			return num;
		}

		public int SelectLocalizedPropertyMSSQL(int EntityId, int LanguageId, string LocaleKeyGroup, string LocaleKey)
		{
			int result = 0;
			string cmdText = "Select Id From [LocalizedProperty] WITH (NOLOCK) Where EntityId = @EntityId And LanguageId = @LanguageId and LocaleKeyGroup = @LocaleKeyGroup and LocaleKey = @LocaleKey";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@EntityId", EntityId));
					sqlCommand.Parameters.Add(new SqlParameter("@LanguageId", LanguageId));
					sqlCommand.Parameters.Add(new SqlParameter("@LocaleKeyGroup", LocaleKeyGroup));
					sqlCommand.Parameters.Add(new SqlParameter("@LocaleKey", LocaleKey));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							result = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		private int InsertLocalizedPropertyMSSQL(int EntityId, int LanguageId, string LocaleKeyGroup, string LocaleKey, string LocaleValue)
		{
			int result = 0;
			string str = "\r\n                                INSERT INTO [LocalizedProperty]\r\n                               ([EntityId]\r\n                               ,[LanguageId]\r\n                               ,[LocaleKeyGroup]\r\n                               ,[LocaleKey]\r\n                               ,[LocaleValue])\r\n                                VALUES\r\n                               (@EntityId\r\n                               ,@LanguageId\r\n                               ,@LocaleKeyGroup\r\n                               ,@LocaleKey\r\n                               ,@LocaleValue)";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@EntityId", EntityId));
					sqlCommand.Parameters.Add(new SqlParameter("@LanguageId", LanguageId));
					sqlCommand.Parameters.Add(new SqlParameter("@LocaleKeyGroup", LocaleKeyGroup));
					sqlCommand.Parameters.Add(new SqlParameter("@LocaleKey", LocaleKey));
					sqlCommand.Parameters.Add(new SqlParameter("@LocaleValue", LocaleValue));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void UpdateLocalizedPropertyMSSQL(int ItemId, string LocaleValue)
		{
			string cmdText = "Update LocalizedProperty Set LocaleValue = @LocaleValue Where Id = @ItemId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@LocaleValue", LocaleValue));
					sqlCommand.Parameters.Add(new SqlParameter("@ItemId", ItemId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void DeleteUrlRecordMSSQL(string entityName, int entityId, string slug)
		{
			string cmdText = "Update UrlRecord Set IsActive = @isActive Where EntityId = @entityId And EntityName = @entityName And Slug != @slug";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@entityName", entityName));
					sqlCommand.Parameters.Add(new SqlParameter("@entityId", entityId));
					sqlCommand.Parameters.Add(new SqlParameter("@isActive", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@slug", slug));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int SelectUrlRecordByIdMSSQL(string entityName, int entityId)
		{
			int result = 0;
			string cmdText = "Select Id From UrlRecord WITH (NOLOCK) Where EntityId = @entityId And EntityName = @entityName and IsActive = @isActive";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@entityName", entityName));
					sqlCommand.Parameters.Add(new SqlParameter("@entityId", entityId));
					sqlCommand.Parameters.Add(new SqlParameter("@isActive", "1"));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							result = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		public int SelectUrlRecordBySlugMSSQL(string searchValue)
		{
			int result = 0;
			string cmdText = "Select Id From UrlRecord WITH (NOLOCK) Where Slug = @searchValue and IsActive = @isActive";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@searchValue", searchValue.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@isActive", "1"));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							result = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		public int SelectProductIDMSSQL(string productSKU)
		{
			int result = 0;
			string cmdText = "Select Id From Product WITH (NOLOCK) Where Sku = @productSKU";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productSKU", SqlDbType.NVarChar)).Value = productSKU;
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							result = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		public int GetTaxCategoryMSSQL(string searchValue)
		{
			int num = 0;
			if (!string.IsNullOrEmpty(searchValue))
			{
				string cmdText = "Select Id From TaxCategory WITH (NOLOCK) Where Name = @searchValue";
				using (SqlConnection sqlConnection = new SqlConnection(_connString))
				{
					using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
					{
						sqlConnection.Open();
						sqlCommand.Parameters.Add(new SqlParameter("@searchValue", searchValue));
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							if (sqlDataReader.Read())
							{
								num = sqlDataReader.GetInt32(0);
							}
						}
					}
				}
				if (num == 0)
				{
					num = InsertTaxCategory(searchValue);
				}
			}
			return num;
		}

		private int InsertTaxCategoryMSSQL(string categoryName)
		{
			int result = 0;
			string str = "\r\n            INSERT INTO [TaxCategory]\r\n                       ([Name]\r\n                       ,[DisplayOrder]\r\n                        ) \r\n                 VALUES\r\n                       (@Name\r\n                       ,@DisplayOrder\r\n                        )";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@Name", categoryName));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", "0"));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int GetManufacturerMSSQL(string searchValue, bool limitedToStores)
		{
			int num = 0;
			string cmdText = "Select Id From Manufacturer WITH (NOLOCK) Where Name = @searchValue";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@searchValue", searchValue));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							num = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertManufacter(searchValue, limitedToStores);
			}
			return num;
		}

		public int GetVendorMSSQL(string searchValue)
		{
			int num = 0;
			if (string.IsNullOrEmpty(searchValue))
			{
				return num;
			}
			string cmdText = "Select Id From [Vendor] WITH (NOLOCK) Where Name = @searchValue";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@searchValue", searchValue));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							num = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertVendor(searchValue);
			}
			return num;
		}

		private int InsertVendorMSSQL(string name)
		{
			int result = 0;
			string str = "\r\n                                INSERT INTO [Vendor]\r\n                               ([Name]\r\n                               ,[Email]\r\n                               ,[Description]\r\n                               ,[AdminComment]\r\n                               ,[Active]\r\n                               ,[Deleted]\r\n                               ,[DisplayOrder]\r\n                               ,[MetaKeywords]\r\n                               ,[MetaDescription]\r\n                               ,[MetaTitle]\r\n                               ,[PageSize]\r\n                               ,[AllowCustomersToSelectPageSize]\r\n                               ,[PageSizeOptions]\r\n                               {0}) \r\n                         VALUES\r\n                               (@Name\r\n                               ,@Email\r\n                               ,@Description\r\n                               ,@AdminComment\r\n                               ,@Active\r\n                               ,@Deleted\r\n                               ,@DisplayOrder\r\n                               ,@MetaKeywords\r\n                               ,@MetaDescription\r\n                               ,@MetaTitle\r\n                               ,@PageSize\r\n                               ,@AllowCustomersToSelectPageSize\r\n                               ,@PageSizeOptions\r\n                                {1})";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			if (_nopCommerceVersion > NopCommerceVer.Ver36)
			{
				string arg = ",[PictureId],[AddressId]";
				string arg2 = ",@PictureId,@AddressId";
				str = string.Format(str, arg, arg2);
			}
			else
			{
				str = string.Format(str, "", "");
			}
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@Name", name));
					sqlCommand.Parameters.Add(new SqlParameter("@Email", ""));
					sqlCommand.Parameters.Add(new SqlParameter("@Description", ""));
					sqlCommand.Parameters.Add(new SqlParameter("@AdminComment", "Vendor from NopTalk"));
					sqlCommand.Parameters.Add(new SqlParameter("@Active", "1"));
					sqlCommand.Parameters.Add(new SqlParameter("@Deleted", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@AllowCustomersToSelectPageSize", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@MetaKeywords", ""));
					sqlCommand.Parameters.Add(new SqlParameter("@MetaDescription", ""));
					sqlCommand.Parameters.Add(new SqlParameter("@MetaTitle", ""));
					sqlCommand.Parameters.Add(new SqlParameter("@PageSize", "4"));
					sqlCommand.Parameters.Add(new SqlParameter("@PageSizeOptions", ""));
					if (_nopCommerceVersion > NopCommerceVer.Ver36)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@PictureId", "0"));
						sqlCommand.Parameters.Add(new SqlParameter("@AddressId", "0"));
					}
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int GetDeliveryDateMSSQL(string searchValue)
		{
			int num = 0;
			if (string.IsNullOrEmpty(searchValue))
			{
				return num;
			}
			string cmdText = "Select Id From [DeliveryDate] WITH (NOLOCK) Where Name = @searchValue";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@searchValue", searchValue));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							num = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertDeliveryDate(searchValue);
			}
			return num;
		}

		private int InsertDeliveryDateMSSQL(string name)
		{
			int result = 0;
			string str = "\r\n                                INSERT INTO [DeliveryDate]\r\n                               ([Name]\r\n                               ,[DisplayOrder]) \r\n                                    VALUES\r\n                               (@Name\r\n                               ,@DisplayOrder\r\n                                )";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@Name", name));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", "0"));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		private int InsertManufacterMSSQL(string manufacterName, bool limitedToStores)
		{
			int result = 0;
			string format = "\r\n\r\n            INSERT INTO [Manufacturer]\r\n                       ([Name]\r\n                       ,[ManufacturerTemplateId]\r\n                       ,[PictureId]\r\n                       ,[PageSize]\r\n                       ,[AllowCustomersToSelectPageSize]\r\n                       ,[SubjectToAcl]\r\n                       ,[LimitedToStores]\r\n                       ,[Published]\r\n                       ,[Deleted]\r\n                       ,[DisplayOrder]\r\n                       ,[CreatedOnUtc]\r\n                       ,[UpdatedOnUtc]\r\n                       {0}\r\n                        ) \r\n                 VALUES\r\n                       (@Name\r\n                       ,@ManufacturerTemplateId\r\n                       ,@PictureId\r\n                       ,@PageSize\r\n                       ,@AllowCustomersToSelectPageSize\r\n                       ,@SubjectToAcl\r\n                       ,@LimitedToStores\r\n                       ,@Published\r\n                       ,@Deleted\r\n                       ,@DisplayOrder\r\n                       ,GETDATE()\r\n                       ,GETDATE()\r\n                       {1}\r\n                        )";
			if (_nopCommerceVersion == NopCommerceVer.Ver35 || _nopCommerceVersion >= NopCommerceVer.Ver37)
			{
				format = string.Format(format, "", "");
			}
			else
			{
				string arg = ",[HasDiscountsApplied]";
				string arg2 = ",@HasDiscountsApplied";
				format = string.Format(format, arg, arg2);
			}
			format += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(format, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@Name", manufacterName.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@ManufacturerTemplateId", 1));
					sqlCommand.Parameters.Add(new SqlParameter("@PictureId", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@PageSize", "4"));
					sqlCommand.Parameters.Add(new SqlParameter("@AllowCustomersToSelectPageSize", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@SubjectToAcl", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@LimitedToStores", limitedToStores));
					sqlCommand.Parameters.Add(new SqlParameter("@Published", "1"));
					sqlCommand.Parameters.Add(new SqlParameter("@Deleted", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", "0"));
					if (_nopCommerceVersion == NopCommerceVer.Ver36)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@HasDiscountsApplied", "0"));
					}
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int GetCategoryMSSQL(string searchValue, int paretnCategoryId, bool limitedToStores, out bool isNew)
		{
			isNew = false;
			int num = 0;
			if (string.IsNullOrEmpty(searchValue))
			{
				return num;
			}
			string cmdText = "Select Id From [Category] WITH (NOLOCK) Where Name = @searchValue and [ParentCategoryId] = @paretnCategoryId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@searchValue", searchValue));
					sqlCommand.Parameters.Add(new SqlParameter("@paretnCategoryId", paretnCategoryId));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							num = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertCategory(searchValue, paretnCategoryId, limitedToStores);
				isNew = true;
			}
			return num;
		}

		private int InsertCategoryMSSQL(string categoryName, int paretnCategoryId, bool limitedToStores)
		{
			int result = 0;
			string str = "\r\n                    INSERT INTO [Category]\r\n                               ([Name]\r\n                               ,[CategoryTemplateId]\r\n                               ,[ParentCategoryId]\r\n                               ,[PictureId]\r\n                               ,[PageSize]\r\n                               ,[AllowCustomersToSelectPageSize]\r\n                               ,[ShowOnHomePage]\r\n                               ,[IncludeInTopMenu]\r\n                               ,[SubjectToAcl]\r\n                               ,[LimitedToStores]\r\n                               ,[Published]\r\n                               ,[Deleted]\r\n                               ,[DisplayOrder]\r\n                               ,[CreatedOnUtc]\r\n                               ,[UpdatedOnUtc]\r\n                                {0}) \r\n                         VALUES\r\n                               (@Name\r\n                               ,@CategoryTemplateId\r\n                               ,@ParentCategoryId\r\n                               ,@PictureId\r\n                               ,@PageSize\r\n                               ,@AllowCustomersToSelectPageSize\r\n                               ,@ShowOnHomePage\r\n                               ,@IncludeInTopMenu\r\n                               ,@SubjectToAcl\r\n                               ,@LimitedToStores\r\n                               ,@Published\r\n                               ,@Deleted\r\n                               ,@DisplayOrder\r\n                               ,GETDATE()\r\n                               ,GETDATE()\r\n                                {1})";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			if (_nopCommerceVersion >= NopCommerceVer.Ver37)
			{
				str = string.Format(str, "", "");
			}
			else
			{
				string arg = ",[HasDiscountsApplied]";
				string arg2 = ",@HasDiscountsApplied";
				str = string.Format(str, arg, arg2);
			}
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@Name", categoryName));
					sqlCommand.Parameters.Add(new SqlParameter("@CategoryTemplateId", "1"));
					sqlCommand.Parameters.Add(new SqlParameter("@ParentCategoryId", paretnCategoryId));
					sqlCommand.Parameters.Add(new SqlParameter("@PictureId", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@PageSize", "4"));
					sqlCommand.Parameters.Add(new SqlParameter("@AllowCustomersToSelectPageSize", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@ShowOnHomePage", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@IncludeInTopMenu", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@SubjectToAcl", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@LimitedToStores", limitedToStores));
					sqlCommand.Parameters.Add(new SqlParameter("@Published", "1"));
					sqlCommand.Parameters.Add(new SqlParameter("@Deleted", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", "0"));
					if (_nopCommerceVersion < NopCommerceVer.Ver37)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@HasDiscountsApplied", "0"));
					}
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int GetProductAttributeMSSQL(string searchValue)
		{
			int num = 0;
			string cmdText = "Select Id From [ProductAttribute] WITH (NOLOCK) Where Name = @searchValue";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@searchValue", searchValue));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							num = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertProductAttribute(searchValue);
			}
			return num;
		}

		public int GetSpecAttributeMSSQL(string searchValue)
		{
			int num = 0;
			string cmdText = "Select Id From [SpecificationAttribute] WITH (NOLOCK) Where Name = @searchValue";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@searchValue", searchValue));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							num = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertSpecAttribute(searchValue);
			}
			return num;
		}

		public int GetSpecAttributeOptionMSSQL(int specificationAttributeId, string searchValue)
		{
			int num = 0;
			string cmdText = "Select Id From [SpecificationAttributeOption] WITH (NOLOCK) Where SpecificationAttributeId = @specificationAttributeId AND Name = @searchValue";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@specificationAttributeId", specificationAttributeId));
					sqlCommand.Parameters.Add(new SqlParameter("@searchValue", searchValue));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							num = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertSpecAttributeOption(specificationAttributeId, searchValue);
			}
			return num;
		}

		private int InsertSpecAttributeOptionMSSQL(int specificationAttributeId, string productAttributeName)
		{
			int result = 0;
			string str = "\r\n                    INSERT INTO [SpecificationAttributeOption]\r\n                           ([SpecificationAttributeId]\r\n                           ,[Name]\r\n                           ,[ColorSquaresRgb]\r\n                           ,[DisplayOrder])\r\n                     VALUES\r\n                           (@SpecificationAttributeId\r\n                           ,@Name\r\n                           ,@ColorSquaresRgb\r\n                           ,@DisplayOrder)";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@SpecificationAttributeId", specificationAttributeId));
					sqlCommand.Parameters.Add(new SqlParameter("@Name", productAttributeName));
					sqlCommand.Parameters.Add(new SqlParameter("@ColorSquaresRgb", DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", "0"));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		private int InsertSpecAttributeMSSQL(string productAttributeName)
		{
			int result = 0;
			string str = "\r\n                    INSERT INTO [SpecificationAttribute]\r\n                           ([Name]\r\n                          , [DisplayOrder]\r\n                           ) \r\n                     VALUES\r\n                           (@Name\r\n                            ,@DisplayOrder\r\n                            )";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@Name", productAttributeName));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", "0"));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		private int InsertProductAttributeMSSQL(string productAttributeName)
		{
			int result = 0;
			string str = "\r\n                    INSERT INTO [ProductAttribute]\r\n                           ([Name]\r\n                           ) \r\n                     VALUES\r\n                           (@Name)";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@Name", productAttributeName));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public List<int> ExistProductImagesMSSQL(int productId)
		{
			List<int> list = new List<int>();
			string cmdText = "Select PictureId From Product_Picture_Mapping  WITH (NOLOCK) Where [ProductId] = @productID";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						while (sqlDataReader.Read())
						{
							list.Add(sqlDataReader.GetInt32(0));
						}
					}
				}
			}
			return list;
		}

		public List<int> ExistProductImageMSSQL(int productId, int pictureId)
		{
			List<int> list = new List<int>();
			string cmdText = "Select PictureId From Product_Picture_Mapping  WITH (NOLOCK) Where [ProductId] = @productID And [PictureId] = @pictureId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					sqlCommand.Parameters.Add(new SqlParameter("@pictureId", pictureId));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						while (sqlDataReader.Read())
						{
							list.Add(sqlDataReader.GetInt32(0));
						}
					}
				}
			}
			return list;
		}

		public void ExistProductsMSSQL(out Dictionary<int, string> idSkuList)
		{
			idSkuList = new Dictionary<int, string>();
			string cmdText = "Select Id, Sku From Product WITH (NOLOCK) Where [Deleted] = @deleted";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@deleted", "0"));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						while (sqlDataReader.Read())
						{
							int num = GlobalClass.StringToInteger(sqlDataReader[0]);
							string value = GlobalClass.ObjectToString(sqlDataReader[1])?.ToLower().Replace("null", "");
							if (num > 0 && !idSkuList.ContainsKey(num))
							{
								idSkuList.Add(num, value);
							}
						}
					}
				}
			}
		}

		public void DeleteProductMSSQL(int productId)
		{
			string cmdText = "Delete From Product Where Id = @productID";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void DeleteProductImagesMSSQL(int productId, bool deleteFromFile, string pictureSavePath, int pictureId, bool deleteIfNotMapped)
		{
			if (!deleteIfNotMapped && deleteFromFile && !string.IsNullOrEmpty(pictureSavePath))
			{
				try
				{
					DeleteProductImagesFromFileSystem(productId, pictureSavePath, pictureId);
				}
				catch
				{
				}
			}
			string cmdText = "Delete From Picture Where Id IN (select [PictureId] FROM [Product_Picture_Mapping] where [ProductId] = @productID)";
			if (pictureId > 0)
			{
				cmdText = "Delete From Picture Where Id = @pictureId";
			}
			if (pictureId > 0 && deleteIfNotMapped)
			{
				List<int> list = ExistProductImage(productId, pictureId);
				if (list != null && list.Any())
				{
					return;
				}
				try
				{
					DeleteProductImagesFromFileSystem(productId, pictureSavePath, pictureId);
				}
				catch
				{
				}
				cmdText = "Delete From Picture Where Id = @pictureId";
			}
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					if (pictureId > 0)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@pictureId", pictureId));
					}
					else
					{
						sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					}
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void DeleteProductImagesFromFileSystemMSSQL(int productId, string pictureSavePath, int existingPictureId)
		{
			int num = existingPictureId;
			if (num > 0)
			{
				if (pictureSavePath.StartsWith("ftp") || !Directory.Exists(pictureSavePath))
				{
					return;
				}
				string searchPattern = num.ToString("0000000") + "_0.*";
				string[] files = Directory.GetFiles(pictureSavePath, searchPattern);
				string[] array = files;
				foreach (string path in array)
				{
					if (File.Exists(path))
					{
						File.Delete(path);
					}
				}
			}
			else
			{
				string cmdText = "Select Id From Picture WITH (NOLOCK) Where Id IN (select [PictureId] FROM [Product_Picture_Mapping] where [ProductId] = @productID)";
				using (SqlConnection sqlConnection = new SqlConnection(_connString))
				{
					using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
					{
						sqlConnection.Open();
						sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							if (sqlDataReader.HasRows)
							{
								while (sqlDataReader.Read())
								{
									try
									{
										string searchPattern2 = sqlDataReader.GetInt32(0).ToString("0000000") + "_0.*";
										if (Directory.Exists(pictureSavePath))
										{
											string[] files2 = Directory.GetFiles(pictureSavePath, searchPattern2);
											string[] array2 = files2;
											foreach (string path2 in array2)
											{
												if (File.Exists(path2))
												{
													File.Delete(path2);
												}
											}
										}
									}
									catch
									{
									}
								}
							}
						}
					}
				}
			}
		}

		public void DeleteProductManufacturerMappingMSSQL(int productId)
		{
			string cmdText = "Delete From Product_Manufacturer_Mapping Where ProductID = @productID";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void DeleteProductAttributesMSSQL(int productId)
		{
			string cmdText = "Delete From Product_ProductAttribute_Mapping Where ProductID = @productID";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					sqlCommand.ExecuteNonQuery();
				}
			}
			DeleteProductAttCombination(productId);
		}

		public void DeleteProductAttributesMappingMSSQL(int productId, int productAttributeId)
		{
			string cmdText = "Delete From Product_ProductAttribute_Mapping Where ProductID = @productID And ProductAttributeId = @productAttributeId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					sqlCommand.Parameters.Add(new SqlParameter("@productAttributeId", productAttributeId));
					sqlCommand.ExecuteNonQuery();
				}
			}
			DeleteProductAttCombination(productId);
		}

		public void DeleteSpecAttributesMappingMSSQL(int productId)
		{
			string cmdText = "Delete From Product_SpecificationAttribute_Mapping Where ProductId = @productID";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int GetSpecAttributeMappingMSSQL(int productId, int productOptionId, int? attributeTypeId, string customValue, int showOnProductPage, int allowFiltering)
		{
			int num = 0;
			string cmdText = "Select Id From [Product_SpecificationAttribute_Mapping] WITH (NOLOCK) Where ProductId = @productId AND SpecificationAttributeOptionId = @productOptionId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productId", productId));
					sqlCommand.Parameters.Add(new SqlParameter("@productOptionId", productOptionId));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							num = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertSpecAttributeMapping(productId, productOptionId, attributeTypeId, customValue, showOnProductPage, allowFiltering);
			}
			return num;
		}

		public int InsertSpecAttributeMappingMSSQL(int productId, int productOptionId, int? attributeTypeId, string customValue, int showOnProductPage, int allowFiltering)
		{
			int result = 0;
			string str = "INSERT INTO [Product_SpecificationAttribute_Mapping]\r\n                                   ([ProductId]\r\n                                   ,[AttributeTypeId]\r\n                                   ,[SpecificationAttributeOptionId]\r\n                                   ,[CustomValue]\r\n                                   ,[AllowFiltering]\r\n                                   ,[ShowOnProductPage]\r\n                                   ,[DisplayOrder])\r\n                             VALUES\r\n                                   (@ProductId \r\n                                   ,@AttributeTypeId\r\n                                   ,@SpecificationAttributeOptionId\r\n                                   ,@CustomValue\r\n                                   ,@AllowFiltering\r\n                                   ,@ShowOnProductPage\r\n                                   ,@DisplayOrder)";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@ProductId", productId));
					sqlCommand.Parameters.Add(new SqlParameter("@AttributeTypeId", attributeTypeId));
					sqlCommand.Parameters.Add(new SqlParameter("@SpecificationAttributeOptionId", productOptionId));
					sqlCommand.Parameters.Add(new SqlParameter("@AllowFiltering", allowFiltering));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@ShowOnProductPage", showOnProductPage));
					if (!string.IsNullOrEmpty(customValue))
					{
						sqlCommand.Parameters.Add(new SqlParameter("@CustomValue", customValue));
					}
					else
					{
						sqlCommand.Parameters.Add(new SqlParameter("@CustomValue", DBNull.Value));
					}
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int GetProductAttributeMappingMSSQL(TaskProductAttributesMapping taskProductAttributesMapping, int productId, int productAttributeId, int attributeControlType, string defaultValue, bool isRequired, string textPrompt, int displayOrder)
		{
			int num = 0;
			string cmdText = "Select Id From [Product_ProductAttribute_Mapping] WITH (NOLOCK) Where ProductId = @productId AND ProductAttributeId = @productAttributeId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productId", productId));
					sqlCommand.Parameters.Add(new SqlParameter("@productAttributeId", productAttributeId));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							num = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertProductAttributeMapping(productId, productAttributeId, attributeControlType, defaultValue, isRequired, textPrompt, displayOrder);
			}
			else
			{
				UpdateProductAttributeMapping(taskProductAttributesMapping, num, attributeControlType, defaultValue, isRequired, textPrompt, displayOrder);
			}
			return num;
		}

		public int InsertProductAttributeMappingMSSQL(int productId, int productAttributeId, int attributeControlType, string defaultValue, bool isRequired, string textPrompt, int displayOrder)
		{
			int result = 0;
			string str = "INSERT INTO [Product_ProductAttribute_Mapping]\r\n           ([ProductId]\r\n           ,[ProductAttributeId]\r\n           ,[TextPrompt]\r\n           ,[IsRequired]\r\n           ,[AttributeControlTypeId]\r\n           ,[DisplayOrder]\r\n           ,[ValidationMinLength]\r\n           ,[ValidationMaxLength]\r\n           ,[ValidationFileAllowedExtensions]\r\n           ,[ValidationFileMaximumSize]\r\n           ,[DefaultValue]) \r\n     VALUES\r\n           (@ProductId\r\n           ,@ProductAttributeId\r\n           ,@TextPrompt\r\n           ,@IsRequired\r\n           ,@AttributeControlTypeId\r\n           ,@DisplayOrder\r\n           ,@ValidationMinLength\r\n           ,@ValidationMaxLength\r\n           ,@ValidationFileAllowedExtensions\r\n           ,@ValidationFileMaximumSize\r\n           ,@DefaultValue)";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@ProductId", productId));
					sqlCommand.Parameters.Add(new SqlParameter("@ProductAttributeId", productAttributeId));
					sqlCommand.Parameters.Add(new SqlParameter("@TextPrompt", textPrompt.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@IsRequired", isRequired.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@AttributeControlTypeId", attributeControlType));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", displayOrder.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@ValidationMinLength", DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@ValidationMaxLength", DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@ValidationFileAllowedExtensions", DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@ValidationFileMaximumSize", DBNull.Value));
					if (attributeControlType == 30)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@DefaultValue", Guid.NewGuid()));
					}
					else if (!string.IsNullOrEmpty(defaultValue) && attributeControlType == 4)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@DefaultValue", defaultValue));
					}
					else
					{
						sqlCommand.Parameters.Add(new SqlParameter("@DefaultValue", DBNull.Value));
					}
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteProductAttributeValuesMSSQL(int productAttributeMappingId)
		{
			string cmdText = "Delete From ProductAttributeValue Where ProductAttributeMappingId = @ProductAttributeMappingId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@ProductAttributeMappingId", productAttributeMappingId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertUpdateProductAttributeValueMSSQL(TaskProductAttributesMapping taskProductAttributesMapping, int productId, bool deleteFromFile, string filePath, int productAttributeMappingId, string productAttributeName, int quantity, byte isPreselected, string color, int pictureId, int pictureSquareId, int displayOrder, decimal priceAdjustment, decimal cost, bool priceAdjustmentUsePercentage, decimal weightAdjustment)
		{
			int num = 0;
			int num2 = 0;
			string text = "Select Id {0} From ProductAttributeValue Where ProductAttributeMappingId = @ProductAttributeMappingId AND Name = @Name";
			if (_nopCommerceVersion > NopCommerceVer.Ver37)
			{
				string arg = ", ImageSquaresPictureId";
				text = string.Format(text, arg);
			}
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(text, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@ProductAttributeMappingId", productAttributeMappingId));
					sqlCommand.Parameters.Add(new SqlParameter("@Name", productAttributeName));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							num = sqlDataReader.GetInt32(0);
							if (_nopCommerceVersion > NopCommerceVer.Ver37)
							{
								num2 = sqlDataReader.GetInt32(1);
							}
						}
					}
				}
			}
			if (num2 > 0 && pictureSquareId != num2)
			{
				DeleteProductImages(productId, deleteFromFile, filePath, num2, deleteIfNotMapped: true);
			}
			if (num == 0)
			{
				num = InsertProductAttributeValue(productAttributeMappingId, productAttributeName, quantity, isPreselected, color, pictureId, pictureSquareId, displayOrder, priceAdjustment, cost, priceAdjustmentUsePercentage, weightAdjustment);
			}
			else
			{
				UpdateProductAttributeValue(taskProductAttributesMapping, num, productAttributeName, quantity, isPreselected, color, pictureId, pictureSquareId, displayOrder, priceAdjustment, cost, priceAdjustmentUsePercentage, weightAdjustment);
			}
			return num;
		}

		public void DeleteProductAttributeValueMSSQL(int productAttributeMappingId, string productAttributeName)
		{
			string cmdText = "Delete From ProductAttributeValue Where ProductAttributeMappingId = @ProductAttributeMappingId AND Name = @Name";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@ProductAttributeMappingId", productAttributeMappingId));
					sqlCommand.Parameters.Add(new SqlParameter("@Name", productAttributeName));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertProductAttributeValueMSSQL(int ProductAttributeId, string ProductAttributeName, int Quantity, byte IsPreselected, string color, int? pictureId, int? pictureSquareId, int displayOrder, decimal priceAdjustment, decimal cost, bool priceAdjustmentUsePercentage, decimal weightAdjustment)
		{
			int result = 0;
			string format = "\r\n          INSERT INTO [ProductAttributeValue]\r\n               ([ProductAttributeMappingId]\r\n               ,[AttributeValueTypeId]\r\n               ,[AssociatedProductId]\r\n               ,[Name]\r\n               ,[ColorSquaresRgb]\r\n               ,[PriceAdjustment]\r\n               ,[WeightAdjustment]\r\n               ,[Cost]\r\n               ,[Quantity]\r\n               ,[IsPreSelected]\r\n               ,[DisplayOrder]\r\n               ,[PictureId]\r\n                {0}) \r\n         VALUES\r\n               (@ProductAttributeMappingId\r\n               ,@AttributeValueTypeId\r\n               ,@AssociatedProductId\r\n               ,@Name\r\n               ,@ColorSquaresRgb\r\n               ,@PriceAdjustment\r\n               ,@WeightAdjustment\r\n               ,@Cost\r\n               ,@Quantity\r\n               ,@IsPreSelected\r\n               ,@DisplayOrder\r\n               ,@PictureId\r\n               {1})";
			if (_nopCommerceVersion > NopCommerceVer.Ver37)
			{
				string text = ",[ImageSquaresPictureId]";
				string text2 = ",@ImageSquaresPictureId";
				if (_nopCommerceVersion > NopCommerceVer.Ver38)
				{
					text += ",[CustomerEntersQty]";
					text2 += ",@CustomerEntersQty";
				}
				if (_nopCommerceVersion > NopCommerceVer.Ver40)
				{
					text += ",[PriceAdjustmentUsePercentage]";
					text2 += ",@PriceAdjustmentUsePercentage";
				}
				format = string.Format(format, text, text2);
			}
			else
			{
				format = string.Format(format, "", "");
			}
			format += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(format, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@ProductAttributeMappingId", ProductAttributeId));
					sqlCommand.Parameters.Add(new SqlParameter("@AttributeValueTypeId", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@AssociatedProductId", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@Name", ProductAttributeName.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@PriceAdjustment", priceAdjustment.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@WeightAdjustment", weightAdjustment.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@Cost", cost.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@Quantity", Quantity.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@IsPreSelected", IsPreselected));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", displayOrder.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@ColorSquaresRgb", color.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@PictureId", pictureId.GetValueOrDefault().ReplaceToParam()));
					if (_nopCommerceVersion > NopCommerceVer.Ver37)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@ImageSquaresPictureId", pictureSquareId.GetValueOrDefault().ReplaceToParam()));
						if (_nopCommerceVersion > NopCommerceVer.Ver38)
						{
							sqlCommand.Parameters.Add(new SqlParameter("@CustomerEntersQty", "0"));
						}
						if (_nopCommerceVersion > NopCommerceVer.Ver40)
						{
							sqlCommand.Parameters.Add(new SqlParameter("@PriceAdjustmentUsePercentage", priceAdjustmentUsePercentage.ReplaceToParam()));
						}
					}
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteProductAndCategoriesMappingMSSQL(int productId)
		{
			string cmdText = "Delete From Product_Category_Mapping Where ProductID = @productID";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void DeleteProductAndCategoryMappingMSSQL(int productId, int categoryId)
		{
			string cmdText = "Delete From Product_Category_Mapping Where ProductID = @productID AND CategoryId = @categoryId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					sqlCommand.Parameters.Add(new SqlParameter("@categoryId", categoryId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertProductAndCatecoryMappingMSSQL(int productId, int categoryId)
		{
			DeleteProductAndCategoryMapping(productId, categoryId);
			int result = 0;
			if (categoryId == 0)
			{
				return result;
			}
			string str = "\r\n            INSERT INTO [Product_Category_Mapping]\r\n               ([ProductId]\r\n               ,[CategoryId]\r\n               ,[IsFeaturedProduct]\r\n               ,[DisplayOrder])  \r\n            VALUES\r\n               (@ProductId\r\n               ,@CategoryId\r\n               ,@IsFeaturedProduct\r\n               ,@DisplayOrder)";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@ProductId", productId));
					sqlCommand.Parameters.Add(new SqlParameter("@CategoryId", categoryId));
					sqlCommand.Parameters.Add(new SqlParameter("@IsFeaturedProduct", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", "0"));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int InsertProductAndManufacterMSSQL(int productId, int manufacterId)
		{
			DeleteProductAndManufacterMapping(productId);
			int result = 0;
			if (manufacterId == 0)
			{
				return result;
			}
			string str = "\r\n            INSERT INTO [Product_Manufacturer_Mapping]\r\n                   ([ProductId]\r\n                   ,[ManufacturerId]\r\n                   ,[IsFeaturedProduct]\r\n                   ,[DisplayOrder])  \r\n             VALUES\r\n                   (@ProductId\r\n                   ,@ManufacturerId\r\n                   ,@IsFeaturedProduct\r\n                   ,@DisplayOrder)";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@ProductId", productId));
					sqlCommand.Parameters.Add(new SqlParameter("@ManufacturerId", manufacterId));
					sqlCommand.Parameters.Add(new SqlParameter("@IsFeaturedProduct", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", "0"));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteProductAndManufacterMappingMSSQL(int productId)
		{
			string cmdText = "Delete From Product_Manufacturer_Mapping Where ProductID = @productID";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public string GetProductProductAttCombinationMSSQL(int productId)
		{
			string result = "";
			string cmdText = "Select [AttributesXml] From [ProductAttributeCombination] WITH (NOLOCK) Where ProductId = @productId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productId", productId));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							result = sqlDataReader.GetString(0);
						}
					}
				}
			}
			return result;
		}

		public int GetProductProductAttCombinationMSSQL(int productId, string productSku)
		{
			int result = 0;
			string cmdText = "Select [Id] From [ProductAttributeCombination] WITH (NOLOCK) Where ProductId = @productId AND SKU = @productSku";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productId", productId));
					sqlCommand.Parameters.Add(new SqlParameter("@productSku", productSku));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							result = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		public void DeleteProductAttCombinationMSSQL(int productId)
		{
			string cmdText = "Delete From ProductAttributeCombination Where ProductID = @productID";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertUpdateProductAttCombinationsMSSQL(int productId, SourceItem.VariantItem variant, NopCommerceVer version, int? pictureId)
		{
			int result = 0;
			string text = "";
			text = "<Attributes>";
			foreach (SourceItem.AttributeValues variantItem in variant.VariantItems)
			{
				text = ((version != 0) ? (text + $"<ProductAttribute ID=\"{variantItem.AttMapId}\"><ProductAttributeValue><Value>{variantItem.AttValueId}</Value></ProductAttributeValue></ProductAttribute>") : (text + $"<ProductVariantAttribute ID=\"{variantItem.AttMapId}\"><ProductVariantAttributeValue><Value>{variantItem.AttValueId}</Value></ProductVariantAttributeValue></ProductVariantAttribute>"));
			}
			text += "</Attributes>";
			int productProductAttCombination = GetProductProductAttCombination(productId, variant.Sku);
			if (productProductAttCombination > 0)
			{
				string format = "UPDATE [ProductAttributeCombination]\r\n                                       SET \r\n                                           [AttributesXml] = @AttributesXml\r\n                                          ,[StockQuantity] = @StockQuantity\r\n                                          ,[OverriddenPrice] = @OverriddenPrice\r\n                                          {0}\r\n                                     WHERE Id=@ProductAttributeCombinationId";
				if (_nopCommerceVersion > NopCommerceVer.Ver40 && pictureId.HasValue)
				{
					string arg = ",[PictureId] = @PictureId";
					format = string.Format(format, arg);
				}
				else
				{
					format = string.Format(format, "", "");
				}
				using (SqlConnection sqlConnection = new SqlConnection(_connString))
				{
					using (SqlCommand sqlCommand = new SqlCommand(format, sqlConnection))
					{
						sqlCommand.Parameters.Add(new SqlParameter("@ProductAttributeCombinationId", productProductAttCombination));
						sqlCommand.Parameters.Add(new SqlParameter("@AttributesXml", text));
						sqlCommand.Parameters.Add(new SqlParameter("@StockQuantity", variant.StockQuantity.ReplaceToParam()));
						sqlCommand.Parameters.Add(new SqlParameter("@OverriddenPrice", variant.OverriddenPrice.ReplaceToParam()));
						if (_nopCommerceVersion > NopCommerceVer.Ver40 && pictureId.HasValue)
						{
							sqlCommand.Parameters.Add(new SqlParameter("@PictureId", pictureId.ReplaceToParam()));
						}
						sqlConnection.Open();
						sqlCommand.ExecuteNonQuery();
					}
				}
			}
			else
			{
				string format2 = "\r\n            INSERT INTO [ProductAttributeCombination]\r\n                   ([ProductId]\r\n                   ,[AttributesXml]\r\n                   ,[StockQuantity]\r\n                   ,[AllowOutOfStockOrders]\r\n                   ,[Sku]\r\n                   ,[ManufacturerPartNumber]\r\n                   ,[Gtin]\r\n                   ,[OverriddenPrice]\r\n                   ,[NotifyAdminForQuantityBelow]\r\n                   {0}) \r\n             VALUES\r\n                   (@ProductId\r\n                   ,@AttributesXml\r\n                   ,@StockQuantity\r\n                   ,@AllowOutOfStockOrders\r\n                   ,@Sku\r\n                   ,@ManufacturerPartNumber\r\n                   ,@Gtin\r\n                   ,@OverriddenPrice\r\n                   ,@NotifyAdminForQuantityBelow\r\n                   {1})";
				if (_nopCommerceVersion > NopCommerceVer.Ver40)
				{
					string arg2 = ",[PictureId]";
					string arg3 = ",@PictureId";
					format2 = string.Format(format2, arg2, arg3);
				}
				else
				{
					format2 = string.Format(format2, "", "");
				}
				format2 += ";SELECT SCOPE_IDENTITY() AS insertId;";
				using (SqlConnection sqlConnection2 = new SqlConnection(_connString))
				{
					using (SqlCommand sqlCommand2 = new SqlCommand(format2, sqlConnection2))
					{
						sqlCommand2.Parameters.Add(new SqlParameter("@ProductId", productId));
						sqlCommand2.Parameters.Add(new SqlParameter("@AttributesXml", text));
						sqlCommand2.Parameters.Add(new SqlParameter("@StockQuantity", variant.StockQuantity.ReplaceToParam()));
						sqlCommand2.Parameters.Add(new SqlParameter("@AllowOutOfStockOrders", false));
						sqlCommand2.Parameters.Add(new SqlParameter("@Sku", variant.Sku.ReplaceToParam()));
						sqlCommand2.Parameters.Add(new SqlParameter("@ManufacturerPartNumber", DBNull.Value));
						sqlCommand2.Parameters.Add(new SqlParameter("@Gtin", DBNull.Value));
						sqlCommand2.Parameters.Add(new SqlParameter("@OverriddenPrice", variant.OverriddenPrice.ReplaceToParam()));
						sqlCommand2.Parameters.Add(new SqlParameter("@NotifyAdminForQuantityBelow", 1));
						if (_nopCommerceVersion > NopCommerceVer.Ver40)
						{
							sqlCommand2.Parameters.Add(new SqlParameter("@PictureId", pictureId.GetValueOrDefault().ReplaceToParam()));
						}
						sqlConnection2.Open();
						result = GlobalClass.StringToInteger(sqlCommand2.ExecuteScalar());
					}
				}
			}
			return result;
		}

		public int InsertProductAttCombinationMSSQL(int productId, int ProductVariantAttribute, int ProductVariantAttributeValue, int Quantity, NopCommerceVer version, int? pictureId)
		{
			int result = 0;
			string text = "";
			text = ((version != 0) ? $"<Attributes><ProductAttribute ID=\"{ProductVariantAttribute}\"><ProductAttributeValue><Value>{ProductVariantAttributeValue}</Value></ProductAttributeValue></ProductAttribute></Attributes>" : $"<Attributes><ProductVariantAttribute ID=\"{ProductVariantAttribute}\"><ProductVariantAttributeValue><Value>{ProductVariantAttributeValue}</Value></ProductVariantAttributeValue></ProductVariantAttribute></Attributes>");
			string format = "\r\n            INSERT INTO [ProductAttributeCombination]\r\n                   ([ProductId]\r\n                   ,[AttributesXml]\r\n                   ,[StockQuantity]\r\n                   ,[AllowOutOfStockOrders]\r\n                   ,[Sku]\r\n                   ,[ManufacturerPartNumber]\r\n                   ,[Gtin]\r\n                   ,[OverriddenPrice]\r\n                   ,[NotifyAdminForQuantityBelow]\r\n                   {0}) \r\n             VALUES\r\n                   (@ProductId\r\n                   ,@AttributesXml\r\n                   ,@StockQuantity\r\n                   ,@AllowOutOfStockOrders\r\n                   ,@Sku\r\n                   ,@ManufacturerPartNumber\r\n                   ,@Gtin\r\n                   ,@OverriddenPrice\r\n                   ,@NotifyAdminForQuantityBelow\r\n                   {1})";
			if (_nopCommerceVersion > NopCommerceVer.Ver40)
			{
				string arg = ",[PictureId]";
				string arg2 = ",@PictureId";
				format = string.Format(format, arg, arg2);
			}
			else
			{
				format = string.Format(format, "", "");
			}
			format += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(format, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@ProductId", productId));
					sqlCommand.Parameters.Add(new SqlParameter("@AttributesXml", text));
					sqlCommand.Parameters.Add(new SqlParameter("@StockQuantity", Quantity));
					sqlCommand.Parameters.Add(new SqlParameter("@AllowOutOfStockOrders", false));
					sqlCommand.Parameters.Add(new SqlParameter("@Sku", DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@ManufacturerPartNumber", DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@Gtin", DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@OverriddenPrice", DBNull.Value));
					sqlCommand.Parameters.Add(new SqlParameter("@NotifyAdminForQuantityBelow", 1));
					if (_nopCommerceVersion > NopCommerceVer.Ver40)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@PictureId", pictureId.GetValueOrDefault()));
					}
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeletePicturesMSSQL(int productId)
		{
			string cmdText = "Delete From Picture Where ProductID = @productID";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertPictureMSSQL(Image image, int productId, string pictureSeo, string imageExtension, int existPictureId, int categoryId)
		{
			int num = 0;
			ImageFormat formatOfImage = ImageHelper.ParseImageFormat(imageExtension, image);
			byte[] value = ImageHelper.ConvertImageToByteArray(image, formatOfImage);
			if (existPictureId == 0)
			{
				string str = "insert into Picture(PictureBinary,MimeType,IsNew,SeoFilename) values (@byteData,@extension,@isnew,@seoFilename)";
				if (_nopCommerceVersion > NopCommerceVer.Ver40)
				{
					str = "insert into Picture(MimeType,IsNew,SeoFilename) values (@extension,@isnew,@seoFilename)";
				}
				str += ";SELECT SCOPE_IDENTITY() AS insertId;";
				string cmdText = "insert into Product_Picture_Mapping(ProductID,PictureID,DisplayOrder) values (@productID,@pictureID,@displayOrder)";
				if (categoryId > 0)
				{
					cmdText = "UPDATE [Category] SET [PictureId]=@pictureID WHERE Id=@categoryID";
				}
				using (SqlConnection sqlConnection = new SqlConnection(_connString))
				{
					using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
					{
						if (_nopCommerceVersion < NopCommerceVer.Ver41)
						{
							sqlCommand.Parameters.Add(new SqlParameter("@byteData", value));
						}
						sqlCommand.Parameters.Add(new SqlParameter("@extension", image.GetMimeType()));
						sqlCommand.Parameters.Add(new SqlParameter("@isnew", true));
						if (string.IsNullOrEmpty(pictureSeo))
						{
							sqlCommand.Parameters.Add(new SqlParameter("@seoFilename", DBNull.Value));
						}
						else
						{
							sqlCommand.Parameters.Add(new SqlParameter("@seoFilename", pictureSeo));
						}
						sqlConnection.Open();
						num = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
						if (_nopCommerceVersion > NopCommerceVer.Ver40)
						{
							InsertPictureBinary(image, num, imageExtension);
						}
					}
					using (SqlCommand sqlCommand2 = new SqlCommand(cmdText, sqlConnection))
					{
						sqlCommand2.Parameters.Add(new SqlParameter("@pictureID", num));
						if (categoryId > 0)
						{
							sqlCommand2.Parameters.Add(new SqlParameter("@categoryID", categoryId));
							sqlCommand2.ExecuteNonQuery();
						}
						else if (productId > 0)
						{
							sqlCommand2.Parameters.Add(new SqlParameter("@productID", productId));
							sqlCommand2.Parameters.Add(new SqlParameter("@displayOrder", 1));
							sqlCommand2.ExecuteNonQuery();
						}
					}
				}
			}
			else
			{
				num = existPictureId;
				string cmdText2 = "update Picture SET PictureBinary=@byteData, MimeType=@extension, IsNew=@isnew,SeoFilename=@seoFilename WHERE Id=@pictureID";
				if (_nopCommerceVersion > NopCommerceVer.Ver40)
				{
					cmdText2 = "update Picture SET MimeType=@extension, IsNew=@isnew,SeoFilename=@seoFilename WHERE Id=@pictureID";
				}
				using (SqlConnection sqlConnection2 = new SqlConnection(_connString))
				{
					using (SqlCommand sqlCommand3 = new SqlCommand(cmdText2, sqlConnection2))
					{
						if (_nopCommerceVersion < NopCommerceVer.Ver41)
						{
							sqlCommand3.Parameters.Add(new SqlParameter("@byteData", new byte[0]));
						}
						sqlCommand3.Parameters.Add(new SqlParameter("@extension", image.GetMimeType()));
						sqlCommand3.Parameters.Add(new SqlParameter("@isnew", true));
						sqlCommand3.Parameters.Add(new SqlParameter("@pictureID", num));
						if (string.IsNullOrEmpty(pictureSeo))
						{
							sqlCommand3.Parameters.Add(new SqlParameter("@seoFilename", DBNull.Value));
						}
						else
						{
							sqlCommand3.Parameters.Add(new SqlParameter("@seoFilename", pictureSeo));
						}
						sqlConnection2.Open();
						sqlCommand3.ExecuteNonQuery();
					}
				}
				if (_nopCommerceVersion > NopCommerceVer.Ver40)
				{
					UpdatePictureBinary(image, num, imageExtension);
				}
			}
			return num;
		}

		public int InsertPictureBinaryMSSQL(Image image, int pictureId, string imageExtension)
		{
			int result = 0;
			ImageFormat formatOfImage = ImageHelper.ParseImageFormat(imageExtension, image);
			byte[] value = ImageHelper.ConvertImageToByteArray(image, formatOfImage);
			string str = "insert into PictureBinary(BinaryData,PictureId) values (@BinaryData,@PictureId)";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@BinaryData", value));
					sqlCommand.Parameters.Add(new SqlParameter("@PictureId", pictureId));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void UpdatePictureBinaryMSSQL(Image image, int pictureId, string imageExtension)
		{
			ImageFormat formatOfImage = ImageHelper.ParseImageFormat(imageExtension, image);
			byte[] value = ImageHelper.ConvertImageToByteArray(image, formatOfImage);
			string cmdText = "Update PictureBinary Set BinaryData=@BinaryData WHERE PictureId=@PictureId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@BinaryData", value));
					sqlCommand.Parameters.Add(new SqlParameter("@PictureId", pictureId));
					sqlConnection.Open();
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertPictureFileMSSQL(Image image, int productId, string pictureSavePath, string pictureSeo, string imageExtension, int existPictureId, int categoryId)
		{
			if (string.IsNullOrEmpty(pictureSavePath) || (!pictureSavePath.StartsWith("ftp") && !Directory.Exists(pictureSavePath)))
			{
				throw new Exception("Parameter 'Save pictures directory location' in Task mapping is empty or is not correct.");
			}
			int num = 0;
			ImageFormat ımageFormat = ImageHelper.ParseImageFormat(imageExtension, image);
			byte[] array = ImageHelper.ConvertImageToByteArray(image, ımageFormat);
			if (existPictureId == 0)
			{
				string str = "insert into Picture(PictureBinary,MimeType,IsNew,SeoFilename) values (@byteData,@extension,@isnew,@seoFilename)";
				if (_nopCommerceVersion > NopCommerceVer.Ver40)
				{
					str = "insert into Picture(MimeType,IsNew,SeoFilename) values (@extension,@isnew,@seoFilename)";
				}
				str += ";SELECT SCOPE_IDENTITY() AS insertId;";
				string cmdText = "insert into Product_Picture_Mapping(ProductID,PictureID,DisplayOrder) values (@productID,@pictureID,@displayOrder)";
				if (categoryId > 0)
				{
					cmdText = "UPDATE [Category] SET [PictureId]=@pictureID WHERE Id=@categoryID";
				}
				using (SqlConnection sqlConnection = new SqlConnection(_connString))
				{
					using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
					{
						if (_nopCommerceVersion < NopCommerceVer.Ver41)
						{
							sqlCommand.Parameters.Add(new SqlParameter("@byteData", new byte[0]));
						}
						sqlCommand.Parameters.Add(new SqlParameter("@extension", imageExtension.Contains("png") ? "image/png" : image.GetMimeType()));
						sqlCommand.Parameters.Add(new SqlParameter("@isnew", true));
						if (string.IsNullOrEmpty(pictureSeo))
						{
							sqlCommand.Parameters.Add(new SqlParameter("@seoFilename", DBNull.Value));
						}
						else
						{
							sqlCommand.Parameters.Add(new SqlParameter("@seoFilename", pictureSeo));
						}
						sqlConnection.Open();
						num = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
					}
					using (SqlCommand sqlCommand2 = new SqlCommand(cmdText, sqlConnection))
					{
						sqlCommand2.Parameters.Add(new SqlParameter("@pictureID", num));
						if (categoryId > 0)
						{
							sqlCommand2.Parameters.Add(new SqlParameter("@categoryID", categoryId));
							sqlCommand2.ExecuteNonQuery();
						}
						else if (productId > 0)
						{
							sqlCommand2.Parameters.Add(new SqlParameter("@productID", productId));
							sqlCommand2.Parameters.Add(new SqlParameter("@displayOrder", 1));
							sqlCommand2.ExecuteNonQuery();
						}
					}
				}
			}
			else
			{
				num = existPictureId;
				string cmdText2 = "update Picture SET PictureBinary=@byteData, MimeType=@extension, IsNew=@isnew,SeoFilename=@seoFilename WHERE Id=@pictureID";
				if (_nopCommerceVersion > NopCommerceVer.Ver40)
				{
					cmdText2 = "update Picture SET MimeType=@extension, IsNew=@isnew,SeoFilename=@seoFilename WHERE Id=@pictureID";
				}
				using (SqlConnection sqlConnection2 = new SqlConnection(_connString))
				{
					using (SqlCommand sqlCommand3 = new SqlCommand(cmdText2, sqlConnection2))
					{
						if (_nopCommerceVersion < NopCommerceVer.Ver41)
						{
							sqlCommand3.Parameters.Add(new SqlParameter("@byteData", new byte[0]));
						}
						sqlCommand3.Parameters.Add(new SqlParameter("@extension", imageExtension.Contains("png") ? "image/png" : image.GetMimeType()));
						sqlCommand3.Parameters.Add(new SqlParameter("@isnew", true));
						sqlCommand3.Parameters.Add(new SqlParameter("@pictureID", num));
						if (string.IsNullOrEmpty(pictureSeo))
						{
							sqlCommand3.Parameters.Add(new SqlParameter("@seoFilename", DBNull.Value));
						}
						else
						{
							sqlCommand3.Parameters.Add(new SqlParameter("@seoFilename", pictureSeo));
						}
						sqlConnection2.Open();
						sqlCommand3.ExecuteNonQuery();
					}
				}
			}
			string path = string.Format("{0}_0{1}", num.ToString("0000000"), imageExtension.Contains("png") ? ".png" : ımageFormat.FileExtensionFromEncoder());
			if (pictureSavePath.StartsWith("ftp"))
			{
				string[] array2 = pictureSavePath.Split(';');
				string path2 = array2[0];
				string user = (array2.Length > 1) ? array2[1] : null;
				string password = (array2.Length > 2) ? array2[2] : null;
				pictureSavePath = Path.Combine(path2, path);
				SourceClient.UploadFileToFtp(user, password, pictureSavePath, array);
			}
			else
			{
				pictureSavePath = Path.Combine(pictureSavePath, path);
				File.WriteAllBytes(pictureSavePath, array);
			}
			return num;
		}

		public void DeleteStoreMappingMSSQL(int entityId, string entityName)
		{
			string cmdText = "Delete From StoreMapping Where EntityId = @entityId AND EntityName = @entityName";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@entityId", entityId));
					sqlCommand.Parameters.Add(new SqlParameter("@entityName", entityName));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int GetStoreMappingMSSQL(int entityId, string entityName, int storeId)
		{
			int num = 0;
			string cmdText = "Select Id From [StoreMapping] WITH (NOLOCK) Where EntityId = @entityId AND EntityName = @entityName AND StoreId = @storeId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@entityId", entityId));
					sqlCommand.Parameters.Add(new SqlParameter("@entityName", entityName));
					sqlCommand.Parameters.Add(new SqlParameter("@storeId", storeId));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							num = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertStoreMapping(entityId, entityName, storeId);
			}
			return num;
		}

		public int InsertStoreMappingMSSQL(int entityId, string entityName, int storeId)
		{
			int result = 0;
			string str = "\r\n              INSERT INTO [StoreMapping]\r\n                       ([EntityId]\r\n                       ,[EntityName]\r\n                       ,[StoreId]) \r\n                 VALUES\r\n                       (@entityId\r\n                       ,@entityName\r\n                       ,@storeId)";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@entityId", entityId));
					sqlCommand.Parameters.Add(new SqlParameter("@entityName", entityName));
					sqlCommand.Parameters.Add(new SqlParameter("@storeId", storeId));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void CloseStoreMSSQL(bool disableStore)
		{
			string cmdText = "Update Setting Set value = @disableStore Where name= 'storeinformationsettings.storeclosed'";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@disableStore", disableStore ? "True" : "False"));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertAclMappingRecordMSSQL(string entityName, int entityId, int roleId)
		{
			int result = 0;
			string str = "INSERT INTO [AclRecord]\r\n                                   ([EntityId]\r\n                                   ,[EntityName]\r\n                                   ,[CustomerRoleId])\r\n                                  VALUES\r\n                                   (@EntityId\r\n                                   ,@EntityName\r\n                                   ,@CustomerRoleId)";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@EntityName", entityName));
					sqlCommand.Parameters.Add(new SqlParameter("@EntityId", entityId));
					sqlCommand.Parameters.Add(new SqlParameter("@CustomerRoleId", roleId));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteAclMappingsMSSQL(string entityName, int productId)
		{
			string cmdText = "Delete From AclRecord Where [EntityId] = @productID AND [EntityName] = @entityName";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					sqlCommand.Parameters.Add(new SqlParameter("@entityName", entityName));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void DeleteTierPricingMSSQL(int productId)
		{
			string cmdText = "Delete From TierPrice Where [ProductId] = @productID";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void RunCustomSQLMSSQL(string customSQL)
		{
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand())
				{
					sqlCommand.Connection = sqlConnection;
					string[] array = customSQL.Split(new string[3]
					{
						"GO\r\n",
						"GO ",
						"GO\t"
					}, StringSplitOptions.RemoveEmptyEntries);
					string[] array2 = array;
					foreach (string commandText in array2)
					{
						sqlConnection.Open();
						sqlCommand.CommandText = commandText;
						sqlCommand.ExecuteNonQuery();
						sqlConnection.Close();
					}
				}
			}
		}

		public int GetProductTagMSSQL(string searchValue)
		{
			int num = 0;
			if (!string.IsNullOrEmpty(searchValue))
			{
				string cmdText = "Select Id From [ProductTag] WITH (NOLOCK) Where Name = @searchValue";
				using (SqlConnection sqlConnection = new SqlConnection(_connString))
				{
					using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
					{
						sqlConnection.Open();
						sqlCommand.Parameters.Add(new SqlParameter("@searchValue", searchValue));
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							if (sqlDataReader.Read())
							{
								num = sqlDataReader.GetInt32(0);
							}
						}
					}
				}
				if (num == 0)
				{
					num = InsertProductTag(searchValue);
				}
			}
			return num;
		}

		private int InsertProductTagMSSQL(string tagName)
		{
			int result = 0;
			string str = "\r\n            INSERT INTO [ProductTag]\r\n                       ([Name]\r\n                        ) \r\n                 VALUES\r\n                       (@Name\r\n                        )";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@Name", tagName));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int InsertProductTagMappingMSSQL(int productId, int tagId)
		{
			int result = 0;
			string str = "\r\n              INSERT INTO [Product_ProductTag_Mapping]\r\n                       ([Product_Id]\r\n                       ,[ProductTag_Id]\r\n                       ) \r\n                 VALUES\r\n                       (@productId\r\n                       ,@tagId\r\n                       )";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@productId", productId));
					sqlCommand.Parameters.Add(new SqlParameter("@tagId", tagId));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteProductTagMappingMSSQL(int productId)
		{
			string cmdText = "Delete From Product_ProductTag_Mapping Where Product_Id = @productID";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void DeleteProductCrossSellMappingMSSQL(int productId)
		{
			string cmdText = "Delete From [CrossSellProduct] Where [ProductId1] = @productID";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertProductCrossSellMappingMSSQL(int productId1, int productId2)
		{
			int result = 0;
			string str = "\r\n              INSERT INTO [CrossSellProduct]\r\n                       ([ProductId1]\r\n                       ,[ProductId2]\r\n                       ) \r\n                 VALUES\r\n                       (@productId1\r\n                       ,@productId2\r\n                       )";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@productId1", productId1));
					sqlCommand.Parameters.Add(new SqlParameter("@productId2", productId2));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteProductRelatedMappingMSSQL(int productId)
		{
			string cmdText = "Delete From [RelatedProduct] Where [ProductId1] = @productID";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@productID", productId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertProductRelatedMappingMSSQL(int productId1, int productId2)
		{
			int result = 0;
			string str = "\r\n              INSERT INTO [RelatedProduct]\r\n                       ([ProductId1]\r\n                       ,[ProductId2]\r\n                       ) \r\n                 VALUES\r\n                       (@productId1\r\n                       ,@productId2\r\n                       )";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@productId1", productId1));
					sqlCommand.Parameters.Add(new SqlParameter("@productId2", productId2));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public bool? IsMediaStorageDatabaseMSSQL()
		{
			bool? result = null;
			string cmdText = "Select [Value] From Setting WITH (NOLOCK) Where [Name] = @name";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@name", "media.images.storeindb"));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							result = GlobalClass.ObjectToBool(sqlDataReader.GetString(0));
							return result;
						}
					}
				}
			}
			return result;
		}

		public int GetMeasureWeightMSSQL(string systemKeyword)
		{
			int result = 0;
			string cmdText = "Select Id From [MeasureWeight] WITH (NOLOCK) Where [SystemKeyword] = @systemKeyword or [Name] = @name";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@systemKeyword", systemKeyword.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@name", systemKeyword.ReplaceToParam()));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							result = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		public void DeleteWishListMSSQL(int customerId)
		{
			string cmdText = "Delete From [ShoppingCartItem] Where [ShoppingCartTypeId] = 2  AND [CustomerId] = @customerId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@customerId", customerId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void SelectProductMySQL(string searchValue, int vendorId, int productIdInStore, out int productId, out decimal productPrice, out decimal productCost)
		{
			productId = 0;
			productPrice = default(decimal);
			productCost = default(decimal);
			bool flag = false;
			string obj = "Select Id, Price, ProductCost, Deleted From Product WITH (NOLOCK) Where Sku = @searchValue";
			if (productIdInStore == 1)
			{
				obj = "Select Id, Price, ProductCost, Deleted From Product WITH (NOLOCK) Where Sku = @searchValue AND vendorId=@vendorId";
			}
			if (productIdInStore == 2)
			{
				obj = "Select Id, Price, ProductCost, Deleted From Product WITH (NOLOCK) Where Id = @searchValue";
			}
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@searchValue", MySqlDbType.VarString)).Value = searchValue;
					if (productIdInStore == 1)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@vendorId", vendorId));
					}
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							productId = mySqlDataReader.GetInt32(0);
							productPrice = mySqlDataReader.GetDecimal(1);
							productCost = mySqlDataReader.GetDecimal(2);
							flag = mySqlDataReader.GetBoolean(3);
						}
					}
				}
			}
			if (!flag || productId <= 0)
			{
			}
		}

		public int? SelectCountryMySQL(string country)
		{
			if (string.IsNullOrEmpty(country))
			{
				return null;
			}
			int? result = null;
			string obj = "Select TOP 1 Id From [Country] WITH (NOLOCK) Where [Name] = @name OR [TwoLetterIsoCode] = @code OR [ThreeLetterIsoCode] = @code2";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@name", country.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@code", country.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@code2", country.ReplaceToParam()));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							result = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		public int? SelectStateMySQL(string state)
		{
			if (string.IsNullOrEmpty(state))
			{
				return null;
			}
			int? result = null;
			string obj = "Select TOP 1 Id From [StateProvince] WITH (NOLOCK) Where [Name] = @name OR [Abbreviation] = @code";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@name", state.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@code", state.ReplaceToParam()));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							result = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		public void SqlUpdateMySQL(string table, Dictionary<string, object> values, string where)
		{
			List<string> list = new List<string>();
			List<MySqlParameter> list2 = new List<MySqlParameter>();
			int num = 0;
			foreach (KeyValuePair<string, object> value in values)
			{
				string text = "@sp" + num;
				list.Add($"{value.Key}={text}");
				list2.Add(new MySqlParameter(text, (value.Value == null) ? DBNull.Value : value.Value));
				num++;
			}
			string obj = string.Format("update {0} set {1} where {2}", table, string.Join(", ", list.ToArray()), where);
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.AddRange(list2.ToArray());
					mySqlConnection.Open();
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int GetCustomerRoleMySQL(string searchValue)
		{
			int num = 0;
			if (string.IsNullOrEmpty(searchValue))
			{
				return num;
			}
			string obj = "Select Id From [CustomerRole] WITH (NOLOCK) Where Name = @searchValue";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@searchValue", searchValue));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							num = mySqlDataReader.GetInt32(0);
						}
					}
					if (num == 0)
					{
						num = InsertCustomerRole(searchValue);
					}
				}
			}
			return num;
		}

		private int InsertCustomerRoleMySQL(string name)
		{
			int result = 0;
			string format = "\r\n            INSERT INTO [CustomerRole]\r\n                       ([Name]\r\n                       ,[FreeShipping]\r\n                       ,[TaxExempt]\r\n                       ,[Active]\r\n                       ,[IsSystemRole]\r\n                       ,[SystemName]\r\n                       ,[PurchasedWithProductId]\r\n                       {0}) \r\n                            VALUES\r\n                       (@Name\r\n                       ,@FreeShipping\r\n                       ,@TaxExempt\r\n                       ,@Active\r\n                       ,@IsSystemRole\r\n                       ,@SystemName\r\n                       ,@PurchasedWithProductId\r\n                        {1})";
			if (_nopCommerceVersion >= NopCommerceVer.Ver39)
			{
				string text = ",[EnablePasswordLifetime]";
				string text2 = ",@EnablePasswordLifetime";
				if (_nopCommerceVersion >= NopCommerceVer.Ver40)
				{
					text += ",[OverrideTaxDisplayType]";
					text2 += ",@OverrideTaxDisplayType";
					text += ",[DefaultTaxDisplayTypeId]";
					text2 += ",@DefaultTaxDisplayTypeId";
				}
				format = string.Format(format, text, text2);
			}
			else
			{
				format = string.Format(format, "", "");
			}
			format += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(format.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@Name", name));
					mySqlCommand.Parameters.Add(new MySqlParameter("@FreeShipping", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@TaxExempt", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Active", "1"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsSystemRole", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@SystemName", name));
					mySqlCommand.Parameters.Add(new MySqlParameter("@PurchasedWithProductId", "0"));
					if (_nopCommerceVersion >= NopCommerceVer.Ver39)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@EnablePasswordLifetime", "0"));
						if (_nopCommerceVersion >= NopCommerceVer.Ver40)
						{
							mySqlCommand.Parameters.Add(new MySqlParameter("@OverrideTaxDisplayType", "0"));
							mySqlCommand.Parameters.Add(new MySqlParameter("@DefaultTaxDisplayTypeId", "0"));
						}
					}
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void UnpublishNotUpdatedProductsMySQL(string AdminComment, DateTime taskStartTime)
		{
			string obj = "\r\n             UPDATE [Product]\r\n               SET  [Published] = @Published\r\n                   ,[UpdatedOnUtc] = @date\r\n             WHERE AdminComment LIKE '%' + @AdminComment+ '%' AND [UpdatedOnUtc] < @TaskStartTime\r\n             ";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@AdminComment", AdminComment.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@date", DateTime.Now));
					mySqlCommand.Parameters.Add(new MySqlParameter("@TaskStartTime", taskStartTime.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Published", "0"));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertProductMySQL(NopProduct pProduct)
		{
			if (pProduct.ParentGroupedProductId > 0 && pProduct.ParentGroupedProductId == pProduct.ProductId)
			{
				pProduct.ParentGroupedProductId = 0;
			}
			int result = 0;
			string str = "INSERT INTO [Product]\r\n           ([ProductTypeId]\r\n           ,[ParentGroupedProductId]\r\n           ,[VisibleIndividually]\r\n           ,[Name]\r\n           ,[ShortDescription]\r\n           ,[FullDescription]\r\n           ,[AdminComment]\r\n           ,[ProductTemplateId]\r\n           ,[VendorId]\r\n           ,[ShowOnHomePage]\r\n           ,[MetaKeywords]\r\n           ,[MetaDescription]\r\n           ,[MetaTitle]\r\n           ,[AllowCustomerReviews]\r\n           ,[ApprovedRatingSum]\r\n           ,[NotApprovedRatingSum]\r\n           ,[ApprovedTotalReviews]\r\n           ,[NotApprovedTotalReviews]\r\n           ,[SubjectToAcl]\r\n           ,[LimitedToStores]\r\n           ,[Sku]\r\n           ,[ManufacturerPartNumber]\r\n           ,[Gtin]\r\n           ,[IsGiftCard]\r\n           ,[GiftCardTypeId]\r\n           ,[RequireOtherProducts]\r\n           ,[RequiredProductIds]\r\n           ,[AutomaticallyAddRequiredProducts]\r\n           ,[IsDownload]\r\n           ,[DownloadId]\r\n           ,[UnlimitedDownloads]\r\n           ,[MaxNumberOfDownloads]\r\n           ,[DownloadExpirationDays]\r\n           ,[DownloadActivationTypeId]\r\n           ,[HasSampleDownload]\r\n           ,[SampleDownloadId]\r\n           ,[HasUserAgreement]\r\n           ,[UserAgreementText]\r\n           ,[IsRecurring]\r\n           ,[RecurringCycleLength]\r\n           ,[RecurringCyclePeriodId]\r\n           ,[RecurringTotalCycles]\r\n           ,[IsRental]\r\n           ,[RentalPriceLength]\r\n           ,[RentalPricePeriodId]\r\n           ,[IsShipEnabled]\r\n           ,[IsFreeShipping]\r\n           ,[ShipSeparately]\r\n           ,[AdditionalShippingCharge]\r\n           ,[DeliveryDateId]\r\n           ,[IsTaxExempt]\r\n           ,[TaxCategoryId]\r\n           ,[IsTelecommunicationsOrBroadcastingOrElectronicServices]\r\n           ,[ManageInventoryMethodId]\r\n           ,[UseMultipleWarehouses]\r\n           ,[WarehouseId]\r\n           ,[StockQuantity]\r\n           ,[DisplayStockAvailability]\r\n           ,[DisplayStockQuantity]\r\n           ,[MinStockQuantity]\r\n           ,[LowStockActivityId]\r\n           ,[NotifyAdminForQuantityBelow]\r\n           ,[BackorderModeId]\r\n           ,[AllowBackInStockSubscriptions]\r\n           ,[OrderMinimumQuantity]\r\n           ,[OrderMaximumQuantity]\r\n           ,[AllowedQuantities]\r\n           ,[AllowAddingOnlyExistingAttributeCombinations]\r\n           ,[DisableBuyButton]\r\n           ,[DisableWishlistButton]\r\n           ,[AvailableForPreOrder]\r\n           ,[PreOrderAvailabilityStartDateTimeUtc]\r\n           ,[CallForPrice]\r\n           ,[Price]\r\n           ,[OldPrice]\r\n           ,[ProductCost]\r\n           ,[SpecialPrice]\r\n           ,[SpecialPriceStartDateTimeUtc]\r\n           ,[SpecialPriceEndDateTimeUtc]\r\n           ,[CustomerEntersPrice]\r\n           ,[MinimumCustomerEnteredPrice]\r\n           ,[MaximumCustomerEnteredPrice]\r\n           ,[HasTierPrices]\r\n           ,[HasDiscountsApplied]\r\n           ,[Weight]\r\n           ,[Length]\r\n           ,[Width]\r\n           ,[Height]\r\n           ,[AvailableStartDateTimeUtc]\r\n           ,[AvailableEndDateTimeUtc]\r\n           ,[DisplayOrder]\r\n           ,[Published]\r\n           ,[Deleted]\r\n           ,[CreatedOnUtc]\r\n           ,[UpdatedOnUtc]\r\n           {0}      \r\n            ) \r\n     VALUES\r\n           (@ProductTypeId\r\n           ,@ParentGroupedProductId\r\n           ,@VisibleIndividually\r\n           ,@Name\r\n           ,@ShortDescription\r\n           ,@FullDescription\r\n           ,@AdminComment\r\n           ,@ProductTemplateId\r\n           ,@VendorId\r\n           ,@ShowOnHomePage\r\n           ,@MetaKeywords\r\n           ,@MetaDescription\r\n           ,@MetaTitle\r\n           ,@AllowCustomerReviews\r\n           ,@ApprovedRatingSum\r\n           ,@NotApprovedRatingSum\r\n           ,@ApprovedTotalReviews\r\n           ,@NotApprovedTotalReviews\r\n           ,@SubjectToAcl\r\n           ,@LimitedToStores\r\n           ,@Sku\r\n           ,@ManufacturerPartNumber\r\n           ,@Gtin\r\n           ,@IsGiftCard\r\n           ,@GiftCardTypeId\r\n           ,@RequireOtherProducts\r\n           ,@RequiredProductIds\r\n           ,@AutomaticallyAddRequiredProducts\r\n           ,@IsDownload\r\n           ,@DownloadId\r\n           ,@UnlimitedDownloads\r\n           ,@MaxNumberOfDownloads\r\n           ,@DownloadExpirationDays\r\n           ,@DownloadActivationTypeId\r\n           ,@HasSampleDownload\r\n           ,@SampleDownloadId\r\n           ,@HasUserAgreement\r\n           ,@UserAgreementText\r\n           ,@IsRecurring\r\n           ,@RecurringCycleLength\r\n           ,@RecurringCyclePeriodId\r\n           ,@RecurringTotalCycles\r\n           ,@IsRental\r\n           ,@RentalPriceLength\r\n           ,@RentalPricePeriodId\r\n           ,@IsShipEnabled\r\n           ,@IsFreeShipping\r\n           ,@ShipSeparately\r\n           ,@AdditionalShippingCharge\r\n           ,@DeliveryDateId\r\n           ,@IsTaxExempt\r\n           ,@TaxCategoryId\r\n           ,@IsTelecommunicationsOrBroadcastingOrElectronicServices\r\n           ,@ManageInventoryMethodId\r\n           ,@UseMultipleWarehouses\r\n           ,@WarehouseId\r\n           ,@StockQuantity\r\n           ,@DisplayStockAvailability\r\n           ,@DisplayStockQuantity\r\n           ,@MinStockQuantity\r\n           ,@LowStockActivityId\r\n           ,@NotifyAdminForQuantityBelow\r\n           ,@BackorderModeId\r\n           ,@AllowBackInStockSubscriptions\r\n           ,@OrderMinimumQuantity\r\n           ,@OrderMaximumQuantity\r\n           ,@AllowedQuantities\r\n           ,@AllowAddingOnlyExistingAttributeCombinations\r\n           ,@DisableBuyButton\r\n           ,@DisableWishlistButton\r\n           ,@AvailableForPreOrder\r\n           ,@PreOrderAvailabilityStartDateTimeUtc\r\n           ,@CallForPrice\r\n           ,@Price\r\n           ,@OldPrice\r\n           ,@ProductCost\r\n           ,@SpecialPrice\r\n           ,@SpecialPriceStartDateTimeUtc\r\n           ,@SpecialPriceEndDateTimeUtc\r\n           ,@CustomerEntersPrice\r\n           ,@MinimumCustomerEnteredPrice\r\n           ,@MaximumCustomerEnteredPrice\r\n           ,@HasTierPrices\r\n           ,@HasDiscountsApplied\r\n           ,@Weight\r\n           ,@Length\r\n           ,@Width\r\n           ,@Height\r\n           ,@AvailableStartDateTimeUtc\r\n           ,@AvailableEndDateTimeUtc\r\n           ,@DisplayOrder\r\n           ,@Published\r\n           ,@Deleted\r\n           ,NOW()\r\n           ,NOW()\r\n           {1}          \r\n            )";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			if (_nopCommerceVersion == NopCommerceVer.Ver35)
			{
				str = string.Format(str, "", "");
			}
			else
			{
				string text = ",[BasepriceEnabled]\r\n                                  ,[BasepriceAmount]\r\n                                  ,[BasepriceUnitId]\r\n                                  ,[BasepriceBaseAmount]\r\n                                  ,[BasepriceBaseUnitId]";
				string text2 = ",@BasepriceEnabled\r\n                                  ,@BasepriceAmount\r\n                                  ,@BasepriceUnitId\r\n                                  ,@BasepriceBaseAmount\r\n                                  ,@BasepriceBaseUnitId";
				if (_nopCommerceVersion >= NopCommerceVer.Ver37)
				{
					text += ",[MarkAsNew]";
					text2 += ",@MarkAsNew";
				}
				if (_nopCommerceVersion >= NopCommerceVer.Ver38)
				{
					text += ",[NotReturnable]";
					text2 += ",@NotReturnable";
					if (_nopCommerceVersion >= NopCommerceVer.Ver39)
					{
						text += ",[ProductAvailabilityRangeId]";
						text2 += ",@ProductAvailabilityRangeId";
						str = str.Replace(",[SpecialPriceStartDateTimeUtc]", "");
						str = str.Replace(",@SpecialPriceStartDateTimeUtc", "");
						str = str.Replace(",[SpecialPriceEndDateTimeUtc]", "");
						str = str.Replace(",@SpecialPriceEndDateTimeUtc", "");
						str = str.Replace(",[SpecialPrice]", "");
						str = str.Replace(",@SpecialPrice", "");
					}
				}
				str = string.Format(str, text, text2);
			}
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@ProductTypeId", pProduct.ProductTypeId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ParentGroupedProductId", pProduct.ParentGroupedProductId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@VisibleIndividually", pProduct.VisibleIndividually));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Name", pProduct.Name));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ShortDescription", pProduct.ShortDescription ?? DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@FullDescription", pProduct.FullDescription ?? DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AdminComment", pProduct.AdminComment ?? DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ProductTemplateId", pProduct.ProductTemplateId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@VendorId", pProduct.VendorId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ShowOnHomePage", pProduct.ShowOnHomePage));
					mySqlCommand.Parameters.Add(new MySqlParameter("@MetaKeywords", pProduct.MetaKeywords ?? DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@MetaDescription", pProduct.MetaDescription ?? DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@MetaTitle", pProduct.MetaTitle ?? DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AllowCustomerReviews", pProduct.AllowCustomerReviews));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ApprovedRatingSum", pProduct.ApprovedRatingSum));
					mySqlCommand.Parameters.Add(new MySqlParameter("@NotApprovedRatingSum", pProduct.NotApprovedRatingSum));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ApprovedTotalReviews", pProduct.ApprovedTotalReviews));
					mySqlCommand.Parameters.Add(new MySqlParameter("@NotApprovedTotalReviews", pProduct.NotApprovedTotalReviews));
					mySqlCommand.Parameters.Add(new MySqlParameter("@SubjectToAcl", pProduct.SubjectToAcl.GetValueOrDefault()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@LimitedToStores", pProduct.LimitedToStores));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Sku", pProduct.Sku ?? DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ManufacturerPartNumber", pProduct.ManufacturerPartNumber ?? DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Gtin", pProduct.Gtin ?? DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsGiftCard", pProduct.IsGiftCard));
					mySqlCommand.Parameters.Add(new MySqlParameter("@GiftCardTypeId", pProduct.GiftCardTypeId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@RequiredProductIds", pProduct.RequiredProductIds ?? DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@RequireOtherProducts", pProduct.RequireOtherProducts));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AutomaticallyAddRequiredProducts", pProduct.AutomaticallyAddRequiredProducts));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsDownload", pProduct.IsDownload));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DownloadId", pProduct.DownloadId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@UnlimitedDownloads", pProduct.UnlimitedDownloads));
					mySqlCommand.Parameters.Add(new MySqlParameter("@MaxNumberOfDownloads", pProduct.MaxNumberOfDownloads));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DownloadExpirationDays", pProduct.DownloadExpirationDays));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DownloadActivationTypeId", pProduct.DownloadActivationTypeId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@HasSampleDownload", pProduct.HasSampleDownload));
					mySqlCommand.Parameters.Add(new MySqlParameter("@SampleDownloadId", pProduct.SampleDownloadId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@HasUserAgreement", pProduct.HasUserAgreement));
					mySqlCommand.Parameters.Add(new MySqlParameter("@UserAgreementText", pProduct.UserAgreementText ?? DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsRecurring", pProduct.IsRecurring));
					mySqlCommand.Parameters.Add(new MySqlParameter("@RecurringCycleLength", pProduct.RecurringCycleLength));
					mySqlCommand.Parameters.Add(new MySqlParameter("@RecurringCyclePeriodId", pProduct.RecurringCyclePeriodId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@RecurringTotalCycles", pProduct.RecurringTotalCycles));
					mySqlCommand.Parameters.Add(new MySqlParameter("@RentalPricePeriodId", pProduct.RentalPricePeriodId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsRental", pProduct.IsRental));
					mySqlCommand.Parameters.Add(new MySqlParameter("@RentalPriceLength", pProduct.RentalPriceLength));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsShipEnabled", pProduct.IsShipEnabled));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsFreeShipping", pProduct.IsFreeShipping));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ShipSeparately", pProduct.ShipSeparately));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AdditionalShippingCharge", pProduct.AdditionalShippingCharge));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DeliveryDateId", pProduct.DeliveryDateId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsTaxExempt", pProduct.IsTaxExempt));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsTelecommunicationsOrBroadcastingOrElectronicServices", pProduct.IsTelecommunicationsOrBroadcastingOrElectronicServices));
					mySqlCommand.Parameters.Add(new MySqlParameter("@TaxCategoryId", pProduct.TaxCategoryId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ManageInventoryMethodId", pProduct.ManageInventoryMethodId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@UseMultipleWarehouses", pProduct.UseMultipleWarehouses));
					mySqlCommand.Parameters.Add(new MySqlParameter("@WarehouseId", pProduct.WarehouseId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@StockQuantity", pProduct.StockQuantity));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayStockAvailability", pProduct.DisplayStockAvailability));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayStockQuantity", pProduct.DisplayStockQuantity));
					mySqlCommand.Parameters.Add(new MySqlParameter("@MinStockQuantity", pProduct.MinStockQuantity));
					mySqlCommand.Parameters.Add(new MySqlParameter("@LowStockActivityId", pProduct.LowStockActivityId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@NotifyAdminForQuantityBelow", pProduct.NotifyAdminForQuantityBelow));
					mySqlCommand.Parameters.Add(new MySqlParameter("@BackorderModeId", pProduct.BackorderModeId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AllowBackInStockSubscriptions", pProduct.AllowBackInStockSubscriptions));
					mySqlCommand.Parameters.Add(new MySqlParameter("@OrderMinimumQuantity", pProduct.OrderMinimumQuantity));
					mySqlCommand.Parameters.Add(new MySqlParameter("@OrderMaximumQuantity", pProduct.OrderMaximumQuantity));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AllowedQuantities", pProduct.AllowedQuantities ?? DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AllowAddingOnlyExistingAttributeCombinations", pProduct.AllowAddingOnlyExistingAttributeCombinations));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisableBuyButton", pProduct.DisableBuyButton));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisableWishlistButton", pProduct.DisableWishlistButton));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AvailableForPreOrder", pProduct.AvailableForPreOrder));
					mySqlCommand.Parameters.Add(new MySqlParameter("@PreOrderAvailabilityStartDateTimeUtc", pProduct.PreOrderAvailabilityStartDateTimeUtc ?? DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@CallForPrice", pProduct.CallForPrice));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Price", pProduct.Price));
					mySqlCommand.Parameters.Add(new MySqlParameter("@OldPrice", pProduct.OldPrice));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ProductCost", pProduct.ProductCost));
					if (_nopCommerceVersion < NopCommerceVer.Ver39)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@SpecialPrice", pProduct.SpecialPrice));
						mySqlCommand.Parameters.Add(new MySqlParameter("@SpecialPriceStartDateTimeUtc", pProduct.SpecialPriceStartDateTimeUtc ?? DBNull.Value));
						mySqlCommand.Parameters.Add(new MySqlParameter("@SpecialPriceEndDateTimeUtc", pProduct.SpecialPriceEndDateTimeUtc ?? DBNull.Value));
					}
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerEntersPrice", pProduct.CustomerEntersPrice));
					mySqlCommand.Parameters.Add(new MySqlParameter("@MinimumCustomerEnteredPrice", pProduct.MinimumCustomerEnteredPrice));
					mySqlCommand.Parameters.Add(new MySqlParameter("@MaximumCustomerEnteredPrice", pProduct.MaximumCustomerEnteredPrice));
					mySqlCommand.Parameters.Add(new MySqlParameter("@HasTierPrices", pProduct.HasTierPrices));
					mySqlCommand.Parameters.Add(new MySqlParameter("@HasDiscountsApplied", pProduct.HasDiscountsApplied));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Weight", pProduct.Weight));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Length", pProduct.Length));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Width", pProduct.Width));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Height", pProduct.Height));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AvailableStartDateTimeUtc", pProduct.AvailableStartDateTimeUtc ?? DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AvailableEndDateTimeUtc", pProduct.AvailableEndDateTimeUtc ?? DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", pProduct.DisplayOrder));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Published", pProduct.Published));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Deleted", pProduct.Deleted));
					if (_nopCommerceVersion != 0)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@BasepriceEnabled", pProduct.BasepriceEnabled));
						mySqlCommand.Parameters.Add(new MySqlParameter("@BasepriceAmount", pProduct.BasepriceAmount));
						mySqlCommand.Parameters.Add(new MySqlParameter("@BasepriceUnitId", pProduct.BasepriceUnitId));
						mySqlCommand.Parameters.Add(new MySqlParameter("@BasepriceBaseAmount", pProduct.BasepriceBaseAmount));
						mySqlCommand.Parameters.Add(new MySqlParameter("@BasepriceBaseUnitId", pProduct.BasepriceBaseUnitId));
						if (_nopCommerceVersion >= NopCommerceVer.Ver37)
						{
							mySqlCommand.Parameters.Add(new MySqlParameter("@MarkAsNew", "1"));
						}
						if (_nopCommerceVersion >= NopCommerceVer.Ver38)
						{
							mySqlCommand.Parameters.Add(new MySqlParameter("@NotReturnable", pProduct.NotReturnable));
							if (_nopCommerceVersion >= NopCommerceVer.Ver39)
							{
								mySqlCommand.Parameters.Add(new MySqlParameter("@ProductAvailabilityRangeId", "0"));
							}
						}
					}
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int InsertUpdateUrlRecordMySQL(string entityName, int entityId, string slug)
		{
			int num = 0;
			slug = slug.NormalizeText();
			slug = Regex.Replace(slug, "[^\\w\\d_-]", "-", RegexOptions.None);
			if (entityName == "Product")
			{
				DeleteUrlRecord(entityName, entityId, slug);
			}
			num = SelectUrlRecordById(entityName, entityId);
			if (num == 0)
			{
				num = 1;
				while (num > 0)
				{
					num = SelectUrlRecordBySlug(slug);
					if (num > 0)
					{
						slug = slug + "-" + entityId;
					}
				}
				string obj = "insert into UrlRecord(EntityId,EntityName,Slug,IsActive,LanguageId) values (@entityId,@entityName,@slug,@isActive,@languageId)";
				using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
				{
					using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
					{
						mySqlConnection.Open();
						mySqlCommand.Parameters.Add(new MySqlParameter("@entityId", entityId));
						mySqlCommand.Parameters.Add(new MySqlParameter("@entityName", entityName));
						mySqlCommand.Parameters.Add(new MySqlParameter("@slug", slug));
						mySqlCommand.Parameters.Add(new MySqlParameter("@isActive", 1));
						mySqlCommand.Parameters.Add(new MySqlParameter("@languageId", "0"));
						mySqlCommand.ExecuteNonQuery();
					}
				}
			}
			return num;
		}

		public int SelectLocalizedPropertyMySQL(int EntityId, int LanguageId, string LocaleKeyGroup, string LocaleKey)
		{
			int result = 0;
			string obj = "Select Id From [LocalizedProperty] WITH (NOLOCK) Where EntityId = @EntityId And LanguageId = @LanguageId and LocaleKeyGroup = @LocaleKeyGroup and LocaleKey = @LocaleKey";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@EntityId", EntityId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@LanguageId", LanguageId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@LocaleKeyGroup", LocaleKeyGroup));
					mySqlCommand.Parameters.Add(new MySqlParameter("@LocaleKey", LocaleKey));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							result = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		private int InsertLocalizedPropertyMySQL(int EntityId, int LanguageId, string LocaleKeyGroup, string LocaleKey, string LocaleValue)
		{
			int result = 0;
			string str = "\r\n                                INSERT INTO [LocalizedProperty]\r\n                               ([EntityId]\r\n                               ,[LanguageId]\r\n                               ,[LocaleKeyGroup]\r\n                               ,[LocaleKey]\r\n                               ,[LocaleValue])\r\n                                VALUES\r\n                               (@EntityId\r\n                               ,@LanguageId\r\n                               ,@LocaleKeyGroup\r\n                               ,@LocaleKey\r\n                               ,@LocaleValue)";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@EntityId", EntityId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@LanguageId", LanguageId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@LocaleKeyGroup", LocaleKeyGroup));
					mySqlCommand.Parameters.Add(new MySqlParameter("@LocaleKey", LocaleKey));
					mySqlCommand.Parameters.Add(new MySqlParameter("@LocaleValue", LocaleValue));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void UpdateLocalizedPropertyMySQL(int ItemId, string LocaleValue)
		{
			string obj = "Update LocalizedProperty Set LocaleValue = @LocaleValue Where Id = @ItemId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@LocaleValue", LocaleValue));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ItemId", ItemId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void DeleteUrlRecordMySQL(string entityName, int entityId, string slug)
		{
			string obj = "Update UrlRecord Set IsActive = @isActive Where EntityId = @entityId And EntityName = @entityName And Slug != @slug";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@entityName", entityName));
					mySqlCommand.Parameters.Add(new MySqlParameter("@entityId", entityId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@isActive", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@slug", slug));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int SelectUrlRecordByIdMySQL(string entityName, int entityId)
		{
			int result = 0;
			string obj = "Select Id From UrlRecord WITH (NOLOCK) Where EntityId = @entityId And EntityName = @entityName and IsActive = @isActive";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@entityName", entityName));
					mySqlCommand.Parameters.Add(new MySqlParameter("@entityId", entityId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@isActive", "1"));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							result = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		public int SelectUrlRecordBySlugMySQL(string searchValue)
		{
			int result = 0;
			string obj = "Select Id From UrlRecord WITH (NOLOCK) Where Slug = @searchValue and IsActive = @isActive";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@searchValue", searchValue.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@isActive", "1"));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							result = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		public int SelectProductIDMySQL(string productSKU)
		{
			int result = 0;
			string obj = "Select Id From Product WITH (NOLOCK) Where Sku = @productSKU";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productSKU", MySqlDbType.VarString)).Value = productSKU;
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							result = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		public int GetTaxCategoryMySQL(string searchValue)
		{
			int num = 0;
			if (!string.IsNullOrEmpty(searchValue))
			{
				string obj = "Select Id From TaxCategory WITH (NOLOCK) Where Name = @searchValue";
				using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
				{
					using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
					{
						mySqlConnection.Open();
						mySqlCommand.Parameters.Add(new MySqlParameter("@searchValue", searchValue));
						using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
						{
							if (mySqlDataReader.Read())
							{
								num = mySqlDataReader.GetInt32(0);
							}
						}
					}
				}
				if (num == 0)
				{
					num = InsertTaxCategory(searchValue);
				}
			}
			return num;
		}

		private int InsertTaxCategoryMySQL(string categoryName)
		{
			int result = 0;
			string str = "\r\n            INSERT INTO [TaxCategory]\r\n                       ([Name]\r\n                       ,[DisplayOrder]\r\n                        ) \r\n                 VALUES\r\n                       (@Name\r\n                       ,@DisplayOrder\r\n                        )";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@Name", categoryName));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", "0"));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int GetManufacturerMySQL(string searchValue, bool limitedToStores)
		{
			int num = 0;
			string obj = "Select Id From Manufacturer WITH (NOLOCK) Where Name = @searchValue";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@searchValue", searchValue));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							num = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertManufacter(searchValue, limitedToStores);
			}
			return num;
		}

		public int GetVendorMySQL(string searchValue)
		{
			int num = 0;
			if (string.IsNullOrEmpty(searchValue))
			{
				return num;
			}
			string obj = "Select Id From [Vendor] WITH (NOLOCK) Where Name = @searchValue";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@searchValue", searchValue));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							num = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertVendor(searchValue);
			}
			return num;
		}

		private int InsertVendorMySQL(string name)
		{
			int result = 0;
			string str = "\r\n                                INSERT INTO [Vendor]\r\n                               ([Name]\r\n                               ,[Email]\r\n                               ,[Description]\r\n                               ,[AdminComment]\r\n                               ,[Active]\r\n                               ,[Deleted]\r\n                               ,[DisplayOrder]\r\n                               ,[MetaKeywords]\r\n                               ,[MetaDescription]\r\n                               ,[MetaTitle]\r\n                               ,[PageSize]\r\n                               ,[AllowCustomersToSelectPageSize]\r\n                               ,[PageSizeOptions]\r\n                               {0}) \r\n                         VALUES\r\n                               (@Name\r\n                               ,@Email\r\n                               ,@Description\r\n                               ,@AdminComment\r\n                               ,@Active\r\n                               ,@Deleted\r\n                               ,@DisplayOrder\r\n                               ,@MetaKeywords\r\n                               ,@MetaDescription\r\n                               ,@MetaTitle\r\n                               ,@PageSize\r\n                               ,@AllowCustomersToSelectPageSize\r\n                               ,@PageSizeOptions\r\n                                {1})";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			if (_nopCommerceVersion > NopCommerceVer.Ver36)
			{
				string arg = ",[PictureId],[AddressId]";
				string arg2 = ",@PictureId,@AddressId";
				str = string.Format(str, arg, arg2);
			}
			else
			{
				str = string.Format(str, "", "");
			}
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@Name", name));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Email", ""));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Description", ""));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AdminComment", "Vendor from NopTalk"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Active", "1"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Deleted", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AllowCustomersToSelectPageSize", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@MetaKeywords", ""));
					mySqlCommand.Parameters.Add(new MySqlParameter("@MetaDescription", ""));
					mySqlCommand.Parameters.Add(new MySqlParameter("@MetaTitle", ""));
					mySqlCommand.Parameters.Add(new MySqlParameter("@PageSize", "4"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@PageSizeOptions", ""));
					if (_nopCommerceVersion > NopCommerceVer.Ver36)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@PictureId", "0"));
						mySqlCommand.Parameters.Add(new MySqlParameter("@AddressId", "0"));
					}
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int GetDeliveryDateMySQL(string searchValue)
		{
			int num = 0;
			if (string.IsNullOrEmpty(searchValue))
			{
				return num;
			}
			string obj = "Select Id From [DeliveryDate] WITH (NOLOCK) Where Name = @searchValue";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@searchValue", searchValue));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							num = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertDeliveryDate(searchValue);
			}
			return num;
		}

		private int InsertDeliveryDateMySQL(string name)
		{
			int result = 0;
			string str = "\r\n                                INSERT INTO [DeliveryDate]\r\n                               ([Name]\r\n                               ,[DisplayOrder]) \r\n                                    VALUES\r\n                               (@Name\r\n                               ,@DisplayOrder\r\n                                )";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@Name", name));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", "0"));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		private int InsertManufacterMySQL(string manufacterName, bool limitedToStores)
		{
			int result = 0;
			string format = "\r\n\r\n            INSERT INTO [Manufacturer]\r\n                       ([Name]\r\n                       ,[ManufacturerTemplateId]\r\n                       ,[PictureId]\r\n                       ,[PageSize]\r\n                       ,[AllowCustomersToSelectPageSize]\r\n                       ,[SubjectToAcl]\r\n                       ,[LimitedToStores]\r\n                       ,[Published]\r\n                       ,[Deleted]\r\n                       ,[DisplayOrder]\r\n                       ,[CreatedOnUtc]\r\n                       ,[UpdatedOnUtc]\r\n                       {0}\r\n                        ) \r\n                 VALUES\r\n                       (@Name\r\n                       ,@ManufacturerTemplateId\r\n                       ,@PictureId\r\n                       ,@PageSize\r\n                       ,@AllowCustomersToSelectPageSize\r\n                       ,@SubjectToAcl\r\n                       ,@LimitedToStores\r\n                       ,@Published\r\n                       ,@Deleted\r\n                       ,@DisplayOrder\r\n                       ,NOW()\r\n                       ,NOW()\r\n                       {1}\r\n                        )";
			if (_nopCommerceVersion == NopCommerceVer.Ver35 || _nopCommerceVersion >= NopCommerceVer.Ver37)
			{
				format = string.Format(format, "", "");
			}
			else
			{
				string arg = ",[HasDiscountsApplied]";
				string arg2 = ",@HasDiscountsApplied";
				format = string.Format(format, arg, arg2);
			}
			format += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(format.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@Name", manufacterName.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ManufacturerTemplateId", 1));
					mySqlCommand.Parameters.Add(new MySqlParameter("@PictureId", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@PageSize", "4"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AllowCustomersToSelectPageSize", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@SubjectToAcl", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@LimitedToStores", limitedToStores));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Published", "1"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Deleted", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", "0"));
					if (_nopCommerceVersion == NopCommerceVer.Ver36)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@HasDiscountsApplied", "0"));
					}
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int GetCategoryMySQL(string searchValue, int paretnCategoryId, bool limitedToStores, out bool isNew)
		{
			isNew = false;
			int num = 0;
			if (string.IsNullOrEmpty(searchValue))
			{
				return num;
			}
			string obj = "Select Id From [Category] WITH (NOLOCK) Where Name = @searchValue and [ParentCategoryId] = @paretnCategoryId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@searchValue", searchValue));
					mySqlCommand.Parameters.Add(new MySqlParameter("@paretnCategoryId", paretnCategoryId));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							num = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertCategory(searchValue, paretnCategoryId, limitedToStores);
				isNew = true;
			}
			return num;
		}

		private int InsertCategoryMySQL(string categoryName, int paretnCategoryId, bool limitedToStores)
		{
			int result = 0;
			string str = "\r\n                    INSERT INTO [Category]\r\n                               ([Name]\r\n                               ,[CategoryTemplateId]\r\n                               ,[ParentCategoryId]\r\n                               ,[PictureId]\r\n                               ,[PageSize]\r\n                               ,[AllowCustomersToSelectPageSize]\r\n                               ,[ShowOnHomePage]\r\n                               ,[IncludeInTopMenu]\r\n                               ,[SubjectToAcl]\r\n                               ,[LimitedToStores]\r\n                               ,[Published]\r\n                               ,[Deleted]\r\n                               ,[DisplayOrder]\r\n                               ,[CreatedOnUtc]\r\n                               ,[UpdatedOnUtc]\r\n                                {0}) \r\n                         VALUES\r\n                               (@Name\r\n                               ,@CategoryTemplateId\r\n                               ,@ParentCategoryId\r\n                               ,@PictureId\r\n                               ,@PageSize\r\n                               ,@AllowCustomersToSelectPageSize\r\n                               ,@ShowOnHomePage\r\n                               ,@IncludeInTopMenu\r\n                               ,@SubjectToAcl\r\n                               ,@LimitedToStores\r\n                               ,@Published\r\n                               ,@Deleted\r\n                               ,@DisplayOrder\r\n                               ,NOW()\r\n                               ,NOW()\r\n                                {1})";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			if (_nopCommerceVersion >= NopCommerceVer.Ver37)
			{
				str = string.Format(str, "", "");
			}
			else
			{
				string arg = ",[HasDiscountsApplied]";
				string arg2 = ",@HasDiscountsApplied";
				str = string.Format(str, arg, arg2);
			}
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@Name", categoryName));
					mySqlCommand.Parameters.Add(new MySqlParameter("@CategoryTemplateId", "1"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ParentCategoryId", paretnCategoryId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@PictureId", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@PageSize", "4"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AllowCustomersToSelectPageSize", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ShowOnHomePage", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IncludeInTopMenu", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@SubjectToAcl", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@LimitedToStores", limitedToStores));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Published", "1"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Deleted", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", "0"));
					if (_nopCommerceVersion < NopCommerceVer.Ver37)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@HasDiscountsApplied", "0"));
					}
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int GetProductAttributeMySQL(string searchValue)
		{
			int num = 0;
			string obj = "Select Id From [ProductAttribute] WITH (NOLOCK) Where Name = @searchValue";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@searchValue", searchValue));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							num = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertProductAttribute(searchValue);
			}
			return num;
		}

		public int GetSpecAttributeMySQL(string searchValue)
		{
			int num = 0;
			string obj = "Select Id From [SpecificationAttribute] WITH (NOLOCK) Where Name = @searchValue";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@searchValue", searchValue));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							num = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertSpecAttribute(searchValue);
			}
			return num;
		}

		public int GetSpecAttributeOptionMySQL(int specificationAttributeId, string searchValue)
		{
			int num = 0;
			string obj = "Select Id From [SpecificationAttributeOption] WITH (NOLOCK) Where SpecificationAttributeId = @specificationAttributeId AND Name = @searchValue";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@specificationAttributeId", specificationAttributeId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@searchValue", searchValue));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							num = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertSpecAttributeOption(specificationAttributeId, searchValue);
			}
			return num;
		}

		private int InsertSpecAttributeOptionMySQL(int specificationAttributeId, string productAttributeName)
		{
			int result = 0;
			string str = "\r\n                    INSERT INTO [SpecificationAttributeOption]\r\n                           ([SpecificationAttributeId]\r\n                           ,[Name]\r\n                           ,[ColorSquaresRgb]\r\n                           ,[DisplayOrder])\r\n                     VALUES\r\n                           (@SpecificationAttributeId\r\n                           ,@Name\r\n                           ,@ColorSquaresRgb\r\n                           ,@DisplayOrder)";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@SpecificationAttributeId", specificationAttributeId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Name", productAttributeName));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ColorSquaresRgb", DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", "0"));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		private int InsertSpecAttributeMySQL(string productAttributeName)
		{
			int result = 0;
			string str = "\r\n                    INSERT INTO [SpecificationAttribute]\r\n                           ([Name]\r\n                          , [DisplayOrder]\r\n                           ) \r\n                     VALUES\r\n                           (@Name\r\n                            ,@DisplayOrder\r\n                            )";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@Name", productAttributeName));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", "0"));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		private int InsertProductAttributeMySQL(string productAttributeName)
		{
			int result = 0;
			string str = "\r\n                    INSERT INTO [ProductAttribute]\r\n                           ([Name]\r\n                           ) \r\n                     VALUES\r\n                           (@Name)";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@Name", productAttributeName));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public List<int> ExistProductImagesMySQL(int productId)
		{
			List<int> list = new List<int>();
			string obj = "Select PictureId From Product_Picture_Mapping  WITH (NOLOCK) Where [ProductId] = @productID";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						while (mySqlDataReader.Read())
						{
							list.Add(mySqlDataReader.GetInt32(0));
						}
					}
				}
			}
			return list;
		}

		public List<int> ExistProductImageMySQL(int productId, int pictureId)
		{
			List<int> list = new List<int>();
			string obj = "Select PictureId From Product_Picture_Mapping  WITH (NOLOCK) Where [ProductId] = @productID And [PictureId] = @pictureId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@pictureId", pictureId));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						while (mySqlDataReader.Read())
						{
							list.Add(mySqlDataReader.GetInt32(0));
						}
					}
				}
			}
			return list;
		}

		public void ExistProductsMySQL(out Dictionary<int, string> idSkuList)
		{
			idSkuList = new Dictionary<int, string>();
			string obj = "Select Id, Sku From Product WITH (NOLOCK) Where [Deleted] = @deleted";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@deleted", "0"));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						while (mySqlDataReader.Read())
						{
							int num = GlobalClass.StringToInteger(mySqlDataReader[0]);
							string value = GlobalClass.ObjectToString(mySqlDataReader[1])?.ToLower().Replace("null", "");
							if (num > 0 && !idSkuList.ContainsKey(num))
							{
								idSkuList.Add(num, value);
							}
						}
					}
				}
			}
		}

		public void DeleteProductMySQL(int productId)
		{
			string obj = "Delete From Product Where Id = @productID";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void DeleteProductImagesMySQL(int productId, bool deleteFromFile, string pictureSavePath, int pictureId, bool deleteIfNotMapped)
		{
			if (!deleteIfNotMapped && deleteFromFile && !string.IsNullOrEmpty(pictureSavePath))
			{
				try
				{
					DeleteProductImagesFromFileSystem(productId, pictureSavePath, pictureId);
				}
				catch
				{
				}
			}
			string obj2 = "Delete From Picture Where Id IN (select [PictureId] FROM [Product_Picture_Mapping] where [ProductId] = @productID)";
			if (pictureId > 0)
			{
				obj2 = "Delete From Picture Where Id = @pictureId";
			}
			if (pictureId > 0 && deleteIfNotMapped)
			{
				List<int> list = ExistProductImage(productId, pictureId);
				if (list != null && list.Any())
				{
					return;
				}
				try
				{
					DeleteProductImagesFromFileSystem(productId, pictureSavePath, pictureId);
				}
				catch
				{
				}
				obj2 = "Delete From Picture Where Id = @pictureId";
			}
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj2.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					if (pictureId > 0)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@pictureId", pictureId));
					}
					else
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					}
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void DeleteProductImagesFromFileSystemMySQL(int productId, string pictureSavePath, int existingPictureId)
		{
			int num = existingPictureId;
			if (num > 0)
			{
				if (pictureSavePath.StartsWith("ftp") || !Directory.Exists(pictureSavePath))
				{
					return;
				}
				string searchPattern = num.ToString("0000000") + "_0.*";
				string[] files = Directory.GetFiles(pictureSavePath, searchPattern);
				string[] array = files;
				foreach (string path in array)
				{
					if (File.Exists(path))
					{
						File.Delete(path);
					}
				}
			}
			else
			{
				string obj = "Select Id From Picture WITH (NOLOCK) Where Id IN (select [PictureId] FROM [Product_Picture_Mapping] where [ProductId] = @productID)";
				using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
				{
					using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
					{
						mySqlConnection.Open();
						mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
						using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
						{
							if (mySqlDataReader.HasRows)
							{
								while (mySqlDataReader.Read())
								{
									try
									{
										string searchPattern2 = mySqlDataReader.GetInt32(0).ToString("0000000") + "_0.*";
										if (Directory.Exists(pictureSavePath))
										{
											string[] files2 = Directory.GetFiles(pictureSavePath, searchPattern2);
											string[] array2 = files2;
											foreach (string path2 in array2)
											{
												if (File.Exists(path2))
												{
													File.Delete(path2);
												}
											}
										}
									}
									catch
									{
									}
								}
							}
						}
					}
				}
			}
		}

		public void DeleteProductManufacturerMappingMySQL(int productId)
		{
			string obj = "Delete From Product_Manufacturer_Mapping Where ProductID = @productID";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void DeleteProductAttributesMySQL(int productId)
		{
			string obj = "Delete From Product_ProductAttribute_Mapping Where ProductID = @productID";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
			DeleteProductAttCombination(productId);
		}

		public void DeleteProductAttributesMappingMySQL(int productId, int productAttributeId)
		{
			string obj = "Delete From Product_ProductAttribute_Mapping Where ProductID = @productID And ProductAttributeId = @productAttributeId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@productAttributeId", productAttributeId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
			DeleteProductAttCombination(productId);
		}

		public void DeleteSpecAttributesMappingMySQL(int productId)
		{
			string obj = "Delete From Product_SpecificationAttribute_Mapping Where ProductId = @productID";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int GetSpecAttributeMappingMySQL(int productId, int productOptionId, int? attributeTypeId, string customValue, int showOnProductPage, int allowFiltering)
		{
			int num = 0;
			string obj = "Select Id From [Product_SpecificationAttribute_Mapping] WITH (NOLOCK) Where ProductId = @productId AND SpecificationAttributeOptionId = @productOptionId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productId", productId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@productOptionId", productOptionId));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							num = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertSpecAttributeMapping(productId, productOptionId, attributeTypeId, customValue, showOnProductPage, allowFiltering);
			}
			return num;
		}

		public int InsertSpecAttributeMappingMySQL(int productId, int productOptionId, int? attributeTypeId, string customValue, int showOnProductPage, int allowFiltering)
		{
			int result = 0;
			string str = "INSERT INTO [Product_SpecificationAttribute_Mapping]\r\n                                   ([ProductId]\r\n                                   ,[AttributeTypeId]\r\n                                   ,[SpecificationAttributeOptionId]\r\n                                   ,[CustomValue]\r\n                                   ,[AllowFiltering]\r\n                                   ,[ShowOnProductPage]\r\n                                   ,[DisplayOrder])\r\n                             VALUES\r\n                                   (@ProductId \r\n                                   ,@AttributeTypeId\r\n                                   ,@SpecificationAttributeOptionId\r\n                                   ,@CustomValue\r\n                                   ,@AllowFiltering\r\n                                   ,@ShowOnProductPage\r\n                                   ,@DisplayOrder)";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@ProductId", productId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AttributeTypeId", attributeTypeId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@SpecificationAttributeOptionId", productOptionId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AllowFiltering", allowFiltering));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ShowOnProductPage", showOnProductPage));
					if (!string.IsNullOrEmpty(customValue))
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@CustomValue", customValue));
					}
					else
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@CustomValue", DBNull.Value));
					}
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int GetProductAttributeMappingMySQL(TaskProductAttributesMapping taskProductAttributesMapping, int productId, int productAttributeId, int attributeControlType, string defaultValue, bool isRequired, string textPrompt, int displayOrder)
		{
			int num = 0;
			string obj = "Select Id From [Product_ProductAttribute_Mapping] WITH (NOLOCK) Where ProductId = @productId AND ProductAttributeId = @productAttributeId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productId", productId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@productAttributeId", productAttributeId));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							num = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertProductAttributeMapping(productId, productAttributeId, attributeControlType, defaultValue, isRequired, textPrompt, displayOrder);
			}
			else
			{
				UpdateProductAttributeMapping(taskProductAttributesMapping, num, attributeControlType, defaultValue, isRequired, textPrompt, displayOrder);
			}
			return num;
		}

		public int InsertProductAttributeMappingMySQL(int productId, int productAttributeId, int attributeControlType, string defaultValue, bool isRequired, string textPrompt, int displayOrder)
		{
			int result = 0;
			string str = "INSERT INTO [Product_ProductAttribute_Mapping]\r\n           ([ProductId]\r\n           ,[ProductAttributeId]\r\n           ,[TextPrompt]\r\n           ,[IsRequired]\r\n           ,[AttributeControlTypeId]\r\n           ,[DisplayOrder]\r\n           ,[ValidationMinLength]\r\n           ,[ValidationMaxLength]\r\n           ,[ValidationFileAllowedExtensions]\r\n           ,[ValidationFileMaximumSize]\r\n           ,[DefaultValue]) \r\n     VALUES\r\n           (@ProductId\r\n           ,@ProductAttributeId\r\n           ,@TextPrompt\r\n           ,@IsRequired\r\n           ,@AttributeControlTypeId\r\n           ,@DisplayOrder\r\n           ,@ValidationMinLength\r\n           ,@ValidationMaxLength\r\n           ,@ValidationFileAllowedExtensions\r\n           ,@ValidationFileMaximumSize\r\n           ,@DefaultValue)";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@ProductId", productId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ProductAttributeId", productAttributeId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@TextPrompt", textPrompt.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsRequired", isRequired.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AttributeControlTypeId", attributeControlType));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", displayOrder.ReplaceToParam2()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ValidationMinLength", DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ValidationMaxLength", DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ValidationFileAllowedExtensions", DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ValidationFileMaximumSize", DBNull.Value));
					if (attributeControlType == 30)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@DefaultValue", Guid.NewGuid()));
					}
					else if (!string.IsNullOrEmpty(defaultValue) && attributeControlType == 4)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@DefaultValue", defaultValue));
					}
					else
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@DefaultValue", DBNull.Value));
					}
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteProductAttributeValuesMySQL(int productAttributeMappingId)
		{
			string obj = "Delete From ProductAttributeValue Where ProductAttributeMappingId = @ProductAttributeMappingId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@ProductAttributeMappingId", productAttributeMappingId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertUpdateProductAttributeValueMySQL(TaskProductAttributesMapping taskProductAttributesMapping, int productId, bool deleteFromFile, string filePath, int productAttributeMappingId, string productAttributeName, int quantity, byte isPreselected, string color, int pictureId, int pictureSquareId, int displayOrder, decimal priceAdjustment, decimal cost, bool priceAdjustmentUsePercentage, decimal weightAdjustment)
		{
			int num = 0;
			int num2 = 0;
			string text = "Select Id {0} From ProductAttributeValue Where ProductAttributeMappingId = @ProductAttributeMappingId AND Name = @Name";
			if (_nopCommerceVersion > NopCommerceVer.Ver37)
			{
				string arg = ", ImageSquaresPictureId";
				text = string.Format(text, arg);
			}
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(text.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@ProductAttributeMappingId", productAttributeMappingId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Name", productAttributeName));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							num = mySqlDataReader.GetInt32(0);
							if (_nopCommerceVersion > NopCommerceVer.Ver37)
							{
								num2 = mySqlDataReader.GetInt32(1);
							}
						}
					}
				}
			}
			if (num2 > 0 && pictureSquareId != num2)
			{
				DeleteProductImages(productId, deleteFromFile, filePath, num2, deleteIfNotMapped: true);
			}
			if (num == 0)
			{
				num = InsertProductAttributeValue(productAttributeMappingId, productAttributeName, quantity, isPreselected, color, pictureId, pictureSquareId, displayOrder, priceAdjustment, cost, priceAdjustmentUsePercentage, weightAdjustment);
			}
			else
			{
				UpdateProductAttributeValue(taskProductAttributesMapping, num, productAttributeName, quantity, isPreselected, color, pictureId, pictureSquareId, displayOrder, priceAdjustment, cost, priceAdjustmentUsePercentage, weightAdjustment);
			}
			return num;
		}

		public void DeleteProductAttributeValueMySQL(int productAttributeMappingId, string productAttributeName)
		{
			string obj = "Delete From ProductAttributeValue Where ProductAttributeMappingId = @ProductAttributeMappingId AND Name = @Name";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@ProductAttributeMappingId", productAttributeMappingId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Name", productAttributeName));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertProductAttributeValueMySQL(int ProductAttributeId, string ProductAttributeName, int Quantity, byte IsPreselected, string color, int? pictureId, int? pictureSquareId, int displayOrder, decimal priceAdjustment, decimal cost, bool priceAdjustmentUsePercentage, decimal weightAdjustment)
		{
			int result = 0;
			string format = "\r\n          INSERT INTO [ProductAttributeValue]\r\n               ([ProductAttributeMappingId]\r\n               ,[AttributeValueTypeId]\r\n               ,[AssociatedProductId]\r\n               ,[Name]\r\n               ,[ColorSquaresRgb]\r\n               ,[PriceAdjustment]\r\n               ,[WeightAdjustment]\r\n               ,[Cost]\r\n               ,[Quantity]\r\n               ,[IsPreSelected]\r\n               ,[DisplayOrder]\r\n               ,[PictureId]\r\n                {0}) \r\n         VALUES\r\n               (@ProductAttributeMappingId\r\n               ,@AttributeValueTypeId\r\n               ,@AssociatedProductId\r\n               ,@Name\r\n               ,@ColorSquaresRgb\r\n               ,@PriceAdjustment\r\n               ,@WeightAdjustment\r\n               ,@Cost\r\n               ,@Quantity\r\n               ,@IsPreSelected\r\n               ,@DisplayOrder\r\n               ,@PictureId\r\n               {1})";
			if (_nopCommerceVersion > NopCommerceVer.Ver37)
			{
				string text = ",[ImageSquaresPictureId]";
				string text2 = ",@ImageSquaresPictureId";
				if (_nopCommerceVersion > NopCommerceVer.Ver38)
				{
					text += ",[CustomerEntersQty]";
					text2 += ",@CustomerEntersQty";
				}
				if (_nopCommerceVersion > NopCommerceVer.Ver40)
				{
					text += ",[PriceAdjustmentUsePercentage]";
					text2 += ",@PriceAdjustmentUsePercentage";
				}
				format = string.Format(format, text, text2);
			}
			else
			{
				format = string.Format(format, "", "");
			}
			format += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(format.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@ProductAttributeMappingId", ProductAttributeId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AttributeValueTypeId", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AssociatedProductId", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Name", ProductAttributeName.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@PriceAdjustment", priceAdjustment.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@WeightAdjustment", weightAdjustment.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Cost", cost.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Quantity", Quantity.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsPreSelected", IsPreselected));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", displayOrder.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ColorSquaresRgb", color.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@PictureId", pictureId.GetValueOrDefault().ReplaceToParam()));
					if (_nopCommerceVersion > NopCommerceVer.Ver37)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@ImageSquaresPictureId", pictureSquareId.GetValueOrDefault().ReplaceToParam()));
						if (_nopCommerceVersion > NopCommerceVer.Ver38)
						{
							mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerEntersQty", "0"));
						}
						if (_nopCommerceVersion > NopCommerceVer.Ver40)
						{
							mySqlCommand.Parameters.Add(new MySqlParameter("@PriceAdjustmentUsePercentage", priceAdjustmentUsePercentage.ReplaceToParam()));
						}
					}
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteProductAndCategoriesMappingMySQL(int productId)
		{
			string obj = "Delete From Product_Category_Mapping Where ProductID = @productID";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void DeleteProductAndCategoryMappingMySQL(int productId, int categoryId)
		{
			string obj = "Delete From Product_Category_Mapping Where ProductID = @productID AND CategoryId = @categoryId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@categoryId", categoryId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertProductAndCatecoryMappingMySQL(int productId, int categoryId)
		{
			DeleteProductAndCategoryMapping(productId, categoryId);
			int result = 0;
			if (categoryId == 0)
			{
				return result;
			}
			string str = "\r\n            INSERT INTO [Product_Category_Mapping]\r\n               ([ProductId]\r\n               ,[CategoryId]\r\n               ,[IsFeaturedProduct]\r\n               ,[DisplayOrder])  \r\n            VALUES\r\n               (@ProductId\r\n               ,@CategoryId\r\n               ,@IsFeaturedProduct\r\n               ,@DisplayOrder)";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@ProductId", productId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@CategoryId", categoryId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsFeaturedProduct", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", "0"));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int InsertProductAndManufacterMySQL(int productId, int manufacterId)
		{
			DeleteProductAndManufacterMapping(productId);
			int result = 0;
			if (manufacterId == 0)
			{
				return result;
			}
			string str = "\r\n            INSERT INTO [Product_Manufacturer_Mapping]\r\n                   ([ProductId]\r\n                   ,[ManufacturerId]\r\n                   ,[IsFeaturedProduct]\r\n                   ,[DisplayOrder])  \r\n             VALUES\r\n                   (@ProductId\r\n                   ,@ManufacturerId\r\n                   ,@IsFeaturedProduct\r\n                   ,@DisplayOrder)";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@ProductId", productId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ManufacturerId", manufacterId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsFeaturedProduct", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", "0"));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteProductAndManufacterMappingMySQL(int productId)
		{
			string obj = "Delete From Product_Manufacturer_Mapping Where ProductID = @productID";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public string GetProductProductAttCombinationMySQL(int productId)
		{
			string result = "";
			string obj = "Select [AttributesXml] From [ProductAttributeCombination] WITH (NOLOCK) Where ProductId = @productId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productId", productId));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							result = mySqlDataReader.GetString(0);
						}
					}
				}
			}
			return result;
		}

		public int GetProductProductAttCombinationMySQL(int productId, string productSku)
		{
			int result = 0;
			string obj = "Select [Id] From [ProductAttributeCombination] WITH (NOLOCK) Where ProductId = @productId AND SKU = @productSku";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productId", productId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@productSku", productSku));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							result = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		public void DeleteProductAttCombinationMySQL(int productId)
		{
			string obj = "Delete From ProductAttributeCombination Where ProductID = @productID";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertUpdateProductAttCombinationsMySQL(int productId, SourceItem.VariantItem variant, NopCommerceVer version, int? pictureId)
		{
			int result = 0;
			string text = "";
			text = "<Attributes>";
			foreach (SourceItem.AttributeValues variantItem in variant.VariantItems)
			{
				text = ((version != 0) ? (text + $"<ProductAttribute ID=\"{variantItem.AttMapId}\"><ProductAttributeValue><Value>{variantItem.AttValueId}</Value></ProductAttributeValue></ProductAttribute>") : (text + $"<ProductVariantAttribute ID=\"{variantItem.AttMapId}\"><ProductVariantAttributeValue><Value>{variantItem.AttValueId}</Value></ProductVariantAttributeValue></ProductVariantAttribute>"));
			}
			text += "</Attributes>";
			int productProductAttCombination = GetProductProductAttCombination(productId, variant.Sku);
			if (productProductAttCombination > 0)
			{
				string format = "UPDATE [ProductAttributeCombination]\r\n                                       SET \r\n                                           [AttributesXml] = @AttributesXml\r\n                                          ,[StockQuantity] = @StockQuantity\r\n                                          ,[OverriddenPrice] = @OverriddenPrice\r\n                                          {0}\r\n                                     WHERE Id=@ProductAttributeCombinationId";
				if (_nopCommerceVersion > NopCommerceVer.Ver40 && pictureId.HasValue)
				{
					string arg = ",[PictureId] = @PictureId";
					format = string.Format(format, arg);
				}
				else
				{
					format = string.Format(format, "", "");
				}
				using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
				{
					using (MySqlCommand mySqlCommand = new MySqlCommand(format.SqlQueryToMySql(), mySqlConnection))
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@ProductAttributeCombinationId", productProductAttCombination));
						mySqlCommand.Parameters.Add(new MySqlParameter("@AttributesXml", text));
						mySqlCommand.Parameters.Add(new MySqlParameter("@StockQuantity", variant.StockQuantity.ReplaceToParam()));
						mySqlCommand.Parameters.Add(new MySqlParameter("@OverriddenPrice", variant.OverriddenPrice.ReplaceToParam()));
						if (_nopCommerceVersion > NopCommerceVer.Ver40 && pictureId.HasValue)
						{
							mySqlCommand.Parameters.Add(new MySqlParameter("@PictureId", pictureId.ReplaceToParam()));
						}
						mySqlConnection.Open();
						mySqlCommand.ExecuteNonQuery();
					}
				}
			}
			else
			{
				string format2 = "\r\n            INSERT INTO [ProductAttributeCombination]\r\n                   ([ProductId]\r\n                   ,[AttributesXml]\r\n                   ,[StockQuantity]\r\n                   ,[AllowOutOfStockOrders]\r\n                   ,[Sku]\r\n                   ,[ManufacturerPartNumber]\r\n                   ,[Gtin]\r\n                   ,[OverriddenPrice]\r\n                   ,[NotifyAdminForQuantityBelow]\r\n                   {0}) \r\n             VALUES\r\n                   (@ProductId\r\n                   ,@AttributesXml\r\n                   ,@StockQuantity\r\n                   ,@AllowOutOfStockOrders\r\n                   ,@Sku\r\n                   ,@ManufacturerPartNumber\r\n                   ,@Gtin\r\n                   ,@OverriddenPrice\r\n                   ,@NotifyAdminForQuantityBelow\r\n                   {1})";
				if (_nopCommerceVersion > NopCommerceVer.Ver40)
				{
					string arg2 = ",[PictureId]";
					string arg3 = ",@PictureId";
					format2 = string.Format(format2, arg2, arg3);
				}
				else
				{
					format2 = string.Format(format2, "", "");
				}
				format2 += ";SELECT SCOPE_IDENTITY() AS insertId;";
				using (MySqlConnection mySqlConnection2 = new MySqlConnection(_connString))
				{
					using (MySqlCommand mySqlCommand2 = new MySqlCommand(format2.SqlQueryToMySql(), mySqlConnection2))
					{
						mySqlCommand2.Parameters.Add(new MySqlParameter("@ProductId", productId));
						mySqlCommand2.Parameters.Add(new MySqlParameter("@AttributesXml", text));
						mySqlCommand2.Parameters.Add(new MySqlParameter("@StockQuantity", variant.StockQuantity.ReplaceToParam()));
						mySqlCommand2.Parameters.Add(new MySqlParameter("@AllowOutOfStockOrders", false));
						mySqlCommand2.Parameters.Add(new MySqlParameter("@Sku", variant.Sku.ReplaceToParam()));
						mySqlCommand2.Parameters.Add(new MySqlParameter("@ManufacturerPartNumber", DBNull.Value));
						mySqlCommand2.Parameters.Add(new MySqlParameter("@Gtin", DBNull.Value));
						mySqlCommand2.Parameters.Add(new MySqlParameter("@OverriddenPrice", variant.OverriddenPrice.ReplaceToParam()));
						mySqlCommand2.Parameters.Add(new MySqlParameter("@NotifyAdminForQuantityBelow", 1));
						if (_nopCommerceVersion > NopCommerceVer.Ver40)
						{
							mySqlCommand2.Parameters.Add(new MySqlParameter("@PictureId", pictureId.GetValueOrDefault().ReplaceToParam()));
						}
						mySqlConnection2.Open();
						result = GlobalClass.StringToInteger(mySqlCommand2.ExecuteScalar());
					}
				}
			}
			return result;
		}

		public int InsertProductAttCombinationMySQL(int productId, int ProductVariantAttribute, int ProductVariantAttributeValue, int Quantity, NopCommerceVer version, int? pictureId)
		{
			int result = 0;
			string text = "";
			text = ((version != 0) ? $"<Attributes><ProductAttribute ID=\"{ProductVariantAttribute}\"><ProductAttributeValue><Value>{ProductVariantAttributeValue}</Value></ProductAttributeValue></ProductAttribute></Attributes>" : $"<Attributes><ProductVariantAttribute ID=\"{ProductVariantAttribute}\"><ProductVariantAttributeValue><Value>{ProductVariantAttributeValue}</Value></ProductVariantAttributeValue></ProductVariantAttribute></Attributes>");
			string format = "\r\n            INSERT INTO [ProductAttributeCombination]\r\n                   ([ProductId]\r\n                   ,[AttributesXml]\r\n                   ,[StockQuantity]\r\n                   ,[AllowOutOfStockOrders]\r\n                   ,[Sku]\r\n                   ,[ManufacturerPartNumber]\r\n                   ,[Gtin]\r\n                   ,[OverriddenPrice]\r\n                   ,[NotifyAdminForQuantityBelow]\r\n                   {0}) \r\n             VALUES\r\n                   (@ProductId\r\n                   ,@AttributesXml\r\n                   ,@StockQuantity\r\n                   ,@AllowOutOfStockOrders\r\n                   ,@Sku\r\n                   ,@ManufacturerPartNumber\r\n                   ,@Gtin\r\n                   ,@OverriddenPrice\r\n                   ,@NotifyAdminForQuantityBelow\r\n                   {1})";
			if (_nopCommerceVersion > NopCommerceVer.Ver40)
			{
				string arg = ",[PictureId]";
				string arg2 = ",@PictureId";
				format = string.Format(format, arg, arg2);
			}
			else
			{
				format = string.Format(format, "", "");
			}
			format += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(format.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@ProductId", productId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AttributesXml", text));
					mySqlCommand.Parameters.Add(new MySqlParameter("@StockQuantity", Quantity));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AllowOutOfStockOrders", false));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Sku", DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ManufacturerPartNumber", DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Gtin", DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@OverriddenPrice", DBNull.Value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@NotifyAdminForQuantityBelow", 1));
					if (_nopCommerceVersion > NopCommerceVer.Ver40)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@PictureId", pictureId.GetValueOrDefault()));
					}
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeletePicturesMySQL(int productId)
		{
			string obj = "Delete From Picture Where ProductID = @productID";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertPictureMySQL(Image image, int productId, string pictureSeo, string imageExtension, int existPictureId, int categoryId)
		{
			int num = 0;
			ImageFormat formatOfImage = ImageHelper.ParseImageFormat(imageExtension, image);
			byte[] value = ImageHelper.ConvertImageToByteArray(image, formatOfImage);
			if (existPictureId == 0)
			{
				string str = "insert into Picture(PictureBinary,MimeType,IsNew,SeoFilename) values (@byteData,@extension,@isnew,@seoFilename)";
				if (_nopCommerceVersion > NopCommerceVer.Ver40)
				{
					str = "insert into Picture(MimeType,IsNew,SeoFilename) values (@extension,@isnew,@seoFilename)";
				}
				str += ";SELECT SCOPE_IDENTITY() AS insertId;";
				string obj = "insert into Product_Picture_Mapping(ProductID,PictureID,DisplayOrder) values (@productID,@pictureID,@displayOrder)";
				if (categoryId > 0)
				{
					obj = "UPDATE [Category] SET [PictureId]=@pictureID WHERE Id=@categoryID";
				}
				using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
				{
					using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
					{
						if (_nopCommerceVersion < NopCommerceVer.Ver41)
						{
							mySqlCommand.Parameters.Add(new MySqlParameter("@byteData", value));
						}
						mySqlCommand.Parameters.Add(new MySqlParameter("@extension", image.GetMimeType()));
						mySqlCommand.Parameters.Add(new MySqlParameter("@isnew", true));
						if (string.IsNullOrEmpty(pictureSeo))
						{
							mySqlCommand.Parameters.Add(new MySqlParameter("@seoFilename", DBNull.Value));
						}
						else
						{
							mySqlCommand.Parameters.Add(new MySqlParameter("@seoFilename", pictureSeo));
						}
						mySqlConnection.Open();
						num = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
						if (_nopCommerceVersion > NopCommerceVer.Ver40)
						{
							InsertPictureBinary(image, num, imageExtension);
						}
					}
					using (MySqlCommand mySqlCommand2 = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
					{
						mySqlCommand2.Parameters.Add(new MySqlParameter("@pictureID", num));
						if (categoryId > 0)
						{
							mySqlCommand2.Parameters.Add(new MySqlParameter("@categoryID", categoryId));
							mySqlCommand2.ExecuteNonQuery();
						}
						else if (productId > 0)
						{
							mySqlCommand2.Parameters.Add(new MySqlParameter("@productID", productId));
							mySqlCommand2.Parameters.Add(new MySqlParameter("@displayOrder", 1));
							mySqlCommand2.ExecuteNonQuery();
						}
					}
				}
			}
			else
			{
				num = existPictureId;
				string obj2 = "update Picture SET PictureBinary=@byteData, MimeType=@extension, IsNew=@isnew,SeoFilename=@seoFilename WHERE Id=@pictureID";
				if (_nopCommerceVersion > NopCommerceVer.Ver40)
				{
					obj2 = "update Picture SET MimeType=@extension, IsNew=@isnew,SeoFilename=@seoFilename WHERE Id=@pictureID";
				}
				using (MySqlConnection mySqlConnection2 = new MySqlConnection(_connString))
				{
					using (MySqlCommand mySqlCommand3 = new MySqlCommand(obj2.SqlQueryToMySql(), mySqlConnection2))
					{
						if (_nopCommerceVersion < NopCommerceVer.Ver41)
						{
							mySqlCommand3.Parameters.Add(new MySqlParameter("@byteData", new byte[0]));
						}
						mySqlCommand3.Parameters.Add(new MySqlParameter("@extension", image.GetMimeType()));
						mySqlCommand3.Parameters.Add(new MySqlParameter("@isnew", true));
						mySqlCommand3.Parameters.Add(new MySqlParameter("@pictureID", num));
						if (string.IsNullOrEmpty(pictureSeo))
						{
							mySqlCommand3.Parameters.Add(new MySqlParameter("@seoFilename", DBNull.Value));
						}
						else
						{
							mySqlCommand3.Parameters.Add(new MySqlParameter("@seoFilename", pictureSeo));
						}
						mySqlConnection2.Open();
						mySqlCommand3.ExecuteNonQuery();
					}
				}
				if (_nopCommerceVersion > NopCommerceVer.Ver40)
				{
					UpdatePictureBinary(image, num, imageExtension);
				}
			}
			return num;
		}

		public int InsertPictureBinaryMySQL(Image image, int pictureId, string imageExtension)
		{
			int result = 0;
			ImageFormat formatOfImage = ImageHelper.ParseImageFormat(imageExtension, image);
			byte[] value = ImageHelper.ConvertImageToByteArray(image, formatOfImage);
			string str = "insert into PictureBinary(BinaryData,PictureId) values (@BinaryData,@PictureId)";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@BinaryData", value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@PictureId", pictureId));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void UpdatePictureBinaryMySQL(Image image, int pictureId, string imageExtension)
		{
			ImageFormat formatOfImage = ImageHelper.ParseImageFormat(imageExtension, image);
			byte[] value = ImageHelper.ConvertImageToByteArray(image, formatOfImage);
			string obj = "Update PictureBinary Set BinaryData=@BinaryData WHERE PictureId=@PictureId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@BinaryData", value));
					mySqlCommand.Parameters.Add(new MySqlParameter("@PictureId", pictureId));
					mySqlConnection.Open();
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertPictureFileMySQL(Image image, int productId, string pictureSavePath, string pictureSeo, string imageExtension, int existPictureId, int categoryId)
		{
			if (string.IsNullOrEmpty(pictureSavePath) || (!pictureSavePath.StartsWith("ftp") && !Directory.Exists(pictureSavePath)))
			{
				throw new Exception("Parameter 'Save pictures directory location' in Task mapping is empty or is not correct.");
			}
			int num = 0;
			ImageFormat ımageFormat = ImageHelper.ParseImageFormat(imageExtension, image);
			byte[] array = ImageHelper.ConvertImageToByteArray(image, ımageFormat);
			if (existPictureId == 0)
			{
				string str = "insert into Picture(PictureBinary,MimeType,IsNew,SeoFilename) values (@byteData,@extension,@isnew,@seoFilename)";
				if (_nopCommerceVersion > NopCommerceVer.Ver40)
				{
					str = "insert into Picture(MimeType,IsNew,SeoFilename) values (@extension,@isnew,@seoFilename)";
				}
				str += ";SELECT SCOPE_IDENTITY() AS insertId;";
				string obj = "insert into Product_Picture_Mapping(ProductID,PictureID,DisplayOrder) values (@productID,@pictureID,@displayOrder)";
				if (categoryId > 0)
				{
					obj = "UPDATE [Category] SET [PictureId]=@pictureID WHERE Id=@categoryID";
				}
				using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
				{
					using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
					{
						if (_nopCommerceVersion < NopCommerceVer.Ver41)
						{
							mySqlCommand.Parameters.Add(new MySqlParameter("@byteData", new byte[0]));
						}
						mySqlCommand.Parameters.Add(new MySqlParameter("@extension", imageExtension.Contains("png") ? "image/png" : image.GetMimeType()));
						mySqlCommand.Parameters.Add(new MySqlParameter("@isnew", true));
						if (string.IsNullOrEmpty(pictureSeo))
						{
							mySqlCommand.Parameters.Add(new MySqlParameter("@seoFilename", DBNull.Value));
						}
						else
						{
							mySqlCommand.Parameters.Add(new MySqlParameter("@seoFilename", pictureSeo));
						}
						mySqlConnection.Open();
						num = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
					}
					using (MySqlCommand mySqlCommand2 = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
					{
						mySqlCommand2.Parameters.Add(new MySqlParameter("@pictureID", num));
						if (categoryId > 0)
						{
							mySqlCommand2.Parameters.Add(new MySqlParameter("@categoryID", categoryId));
							mySqlCommand2.ExecuteNonQuery();
						}
						else if (productId > 0)
						{
							mySqlCommand2.Parameters.Add(new MySqlParameter("@productID", productId));
							mySqlCommand2.Parameters.Add(new MySqlParameter("@displayOrder", 1));
							mySqlCommand2.ExecuteNonQuery();
						}
					}
				}
			}
			else
			{
				num = existPictureId;
				string obj2 = "update Picture SET PictureBinary=@byteData, MimeType=@extension, IsNew=@isnew,SeoFilename=@seoFilename WHERE Id=@pictureID";
				if (_nopCommerceVersion > NopCommerceVer.Ver40)
				{
					obj2 = "update Picture SET MimeType=@extension, IsNew=@isnew,SeoFilename=@seoFilename WHERE Id=@pictureID";
				}
				using (MySqlConnection mySqlConnection2 = new MySqlConnection(_connString))
				{
					using (MySqlCommand mySqlCommand3 = new MySqlCommand(obj2.SqlQueryToMySql(), mySqlConnection2))
					{
						if (_nopCommerceVersion < NopCommerceVer.Ver41)
						{
							mySqlCommand3.Parameters.Add(new MySqlParameter("@byteData", new byte[0]));
						}
						mySqlCommand3.Parameters.Add(new MySqlParameter("@extension", imageExtension.Contains("png") ? "image/png" : image.GetMimeType()));
						mySqlCommand3.Parameters.Add(new MySqlParameter("@isnew", true));
						mySqlCommand3.Parameters.Add(new MySqlParameter("@pictureID", num));
						if (string.IsNullOrEmpty(pictureSeo))
						{
							mySqlCommand3.Parameters.Add(new MySqlParameter("@seoFilename", DBNull.Value));
						}
						else
						{
							mySqlCommand3.Parameters.Add(new MySqlParameter("@seoFilename", pictureSeo));
						}
						mySqlConnection2.Open();
						mySqlCommand3.ExecuteNonQuery();
					}
				}
			}
			string path = string.Format("{0}_0{1}", num.ToString("0000000"), imageExtension.Contains("png") ? ".png" : ımageFormat.FileExtensionFromEncoder());
			if (pictureSavePath.StartsWith("ftp"))
			{
				string[] array2 = pictureSavePath.Split(';');
				string path2 = array2[0];
				string user = (array2.Length > 1) ? array2[1] : null;
				string password = (array2.Length > 2) ? array2[2] : null;
				pictureSavePath = Path.Combine(path2, path);
				SourceClient.UploadFileToFtp(user, password, pictureSavePath, array);
			}
			else
			{
				pictureSavePath = Path.Combine(pictureSavePath, path);
				File.WriteAllBytes(pictureSavePath, array);
			}
			return num;
		}

		public void DeleteStoreMappingMySQL(int entityId, string entityName)
		{
			string obj = "Delete From StoreMapping Where EntityId = @entityId AND EntityName = @entityName";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@entityId", entityId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@entityName", entityName));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int GetStoreMappingMySQL(int entityId, string entityName, int storeId)
		{
			int num = 0;
			string obj = "Select Id From [StoreMapping] WITH (NOLOCK) Where EntityId = @entityId AND EntityName = @entityName AND StoreId = @storeId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@entityId", entityId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@entityName", entityName));
					mySqlCommand.Parameters.Add(new MySqlParameter("@storeId", storeId));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							num = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertStoreMapping(entityId, entityName, storeId);
			}
			return num;
		}

		public int InsertStoreMappingMySQL(int entityId, string entityName, int storeId)
		{
			int result = 0;
			string str = "\r\n              INSERT INTO [StoreMapping]\r\n                       ([EntityId]\r\n                       ,[EntityName]\r\n                       ,[StoreId]) \r\n                 VALUES\r\n                       (@entityId\r\n                       ,@entityName\r\n                       ,@storeId)";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@entityId", entityId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@entityName", entityName));
					mySqlCommand.Parameters.Add(new MySqlParameter("@storeId", storeId));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void CloseStoreMySQL(bool disableStore)
		{
			string obj = "Update Setting Set value = @disableStore Where name= 'storeinformationsettings.storeclosed'";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@disableStore", disableStore ? "True" : "False"));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertAclMappingRecordMySQL(string entityName, int entityId, int roleId)
		{
			int result = 0;
			string str = "INSERT INTO [AclRecord]\r\n                                   ([EntityId]\r\n                                   ,[EntityName]\r\n                                   ,[CustomerRoleId])\r\n                                  VALUES\r\n                                   (@EntityId\r\n                                   ,@EntityName\r\n                                   ,@CustomerRoleId)";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@EntityName", entityName));
					mySqlCommand.Parameters.Add(new MySqlParameter("@EntityId", entityId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerRoleId", roleId));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteAclMappingsMySQL(string entityName, int productId)
		{
			string obj = "Delete From AclRecord Where [EntityId] = @productID AND [EntityName] = @entityName";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@entityName", entityName));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void DeleteTierPricingMySQL(int productId)
		{
			string obj = "Delete From TierPrice Where [ProductId] = @productID";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void RunCustomSQLMySQL(string customSQL)
		{
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand())
				{
					mySqlCommand.Connection = mySqlConnection;
					string[] array = customSQL.Split(new string[3]
					{
						"GO\r\n",
						"GO ",
						"GO\t"
					}, StringSplitOptions.RemoveEmptyEntries);
					string[] array2 = array;
					foreach (string obj in array2)
					{
						mySqlConnection.Open();
						mySqlCommand.CommandText = obj.SqlQueryToMySql();
						mySqlCommand.ExecuteNonQuery();
						mySqlConnection.Close();
					}
				}
			}
		}

		public int GetProductTagMySQL(string searchValue)
		{
			int num = 0;
			if (!string.IsNullOrEmpty(searchValue))
			{
				string obj = "Select Id From [ProductTag] WITH (NOLOCK) Where Name = @searchValue";
				using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
				{
					using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
					{
						mySqlConnection.Open();
						mySqlCommand.Parameters.Add(new MySqlParameter("@searchValue", searchValue));
						using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
						{
							if (mySqlDataReader.Read())
							{
								num = mySqlDataReader.GetInt32(0);
							}
						}
					}
				}
				if (num == 0)
				{
					num = InsertProductTag(searchValue);
				}
			}
			return num;
		}

		private int InsertProductTagMySQL(string tagName)
		{
			int result = 0;
			string str = "\r\n            INSERT INTO [ProductTag]\r\n                       ([Name]\r\n                        ) \r\n                 VALUES\r\n                       (@Name\r\n                        )";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@Name", tagName));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int InsertProductTagMappingMySQL(int productId, int tagId)
		{
			int result = 0;
			string str = "\r\n              INSERT INTO [Product_ProductTag_Mapping]\r\n                       ([Product_Id]\r\n                       ,[ProductTag_Id]\r\n                       ) \r\n                 VALUES\r\n                       (@productId\r\n                       ,@tagId\r\n                       )";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@productId", productId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@tagId", tagId));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteProductTagMappingMySQL(int productId)
		{
			string obj = "Delete From Product_ProductTag_Mapping Where Product_Id = @productID";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void DeleteProductCrossSellMappingMySQL(int productId)
		{
			string obj = "Delete From [CrossSellProduct] Where [ProductId1] = @productID";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertProductCrossSellMappingMySQL(int productId1, int productId2)
		{
			int result = 0;
			string str = "\r\n              INSERT INTO [CrossSellProduct]\r\n                       ([ProductId1]\r\n                       ,[ProductId2]\r\n                       ) \r\n                 VALUES\r\n                       (@productId1\r\n                       ,@productId2\r\n                       )";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@productId1", productId1));
					mySqlCommand.Parameters.Add(new MySqlParameter("@productId2", productId2));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteProductRelatedMappingMySQL(int productId)
		{
			string obj = "Delete From [RelatedProduct] Where [ProductId1] = @productID";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@productID", productId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertProductRelatedMappingMySQL(int productId1, int productId2)
		{
			int result = 0;
			string str = "\r\n              INSERT INTO [RelatedProduct]\r\n                       ([ProductId1]\r\n                       ,[ProductId2]\r\n                       ) \r\n                 VALUES\r\n                       (@productId1\r\n                       ,@productId2\r\n                       )";
			str += ";SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@productId1", productId1));
					mySqlCommand.Parameters.Add(new MySqlParameter("@productId2", productId2));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public bool? IsMediaStorageDatabaseMySQL()
		{
			bool? result = null;
			string obj = "Select [Value] From Setting WITH (NOLOCK) Where [Name] = @name";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@name", "media.images.storeindb"));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							result = GlobalClass.ObjectToBool(mySqlDataReader.GetString(0));
							return result;
						}
					}
				}
			}
			return result;
		}

		public void DeleteWishListMySQL(int customerId)
		{
			string obj = "Delete From [ShoppingCartItem] Where [ShoppingCartTypeId] = 2  AND [CustomerId] = @customerId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@customerId", customerId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void SelectCustomer(int userIdInStore, string userName, string email, string sAPCustomer, out int customerId, out int billingAddressId, out int shippingAddressId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				SelectCustomerMySQL(userIdInStore, userName, email, sAPCustomer, out customerId, out billingAddressId, out shippingAddressId);
			}
			else
			{
				SelectCustomerMSSQL(userIdInStore, userName, email, sAPCustomer, out customerId, out billingAddressId, out shippingAddressId);
			}
		}

		public int InsertCustomer(NopCustomer nopCustomer)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertCustomerMySQL(nopCustomer);
			}
			return InsertCustomerMSSQL(nopCustomer);
		}

		public int SelectAddress(string email)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return SelectAddressMySQL(email);
			}
			return SelectAddressMSSQL(email);
		}

		public int InsertAddress(NopAddress nopAddress)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertAddressMySQL(nopAddress);
			}
			return InsertAddressMSSQL(nopAddress);
		}

		public void DeleteCustomerRoleMapping(int customerId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteCustomerRoleMappingMySQL(customerId);
			}
			else
			{
				DeleteCustomerRoleMappingMSSQL(customerId);
			}
		}

		public void DeleteCustomerRoleMapping(int customerId, int customerRole_Id)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteCustomerRoleMappingMySQL(customerId, customerRole_Id);
			}
			else
			{
				DeleteCustomerRoleMappingMSSQL(customerId, customerRole_Id);
			}
		}

		public int InsertCustomerRoleMapping(int customerId, int customerRoleId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertCustomerRoleMappingMySQL(customerId, customerRoleId);
			}
			return InsertCustomerRoleMappingMSSQL(customerId, customerRoleId);
		}

		public void DeleteCustomerAddressMapping(int customerId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteCustomerAddressMappingMySQL(customerId);
			}
			else
			{
				DeleteCustomerAddressMappingMSSQL(customerId);
			}
		}

		public void DeleteCustomerAddressMapping(int customerId, int addressId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteCustomerAddressMappingMySQL(customerId, addressId);
			}
			else
			{
				DeleteCustomerAddressMappingMSSQL(customerId, addressId);
			}
		}

		public int InsertCustomerAddressMapping(int customerId, int addressId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertCustomerAddressMappingMySQL(customerId, addressId);
			}
			return InsertCustomerAddressMappingMSSQL(customerId, addressId);
		}

		public void DeleteCustomerPassword(int customerId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteCustomerPasswordMySQL(customerId);
			}
			else
			{
				DeleteCustomerPasswordMSSQL(customerId);
			}
		}

		public int InsertCustomerPassword(int customerId, string password)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertCustomerPasswordMySQL(customerId, password);
			}
			return InsertCustomerPasswordMSSQL(customerId, password);
		}

		public void UpdateAddress(NopAddress pAddress)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			if (pAddress.FirstName != null)
			{
				dictionary.Add("FirstName", pAddress.FirstName.ReplaceToParam());
			}
			if (pAddress.LastName != null)
			{
				dictionary.Add("LastName", pAddress.LastName.ReplaceToParam());
			}
			if (pAddress.Email != null)
			{
				dictionary.Add("Email", pAddress.Email.ReplaceToParam());
			}
			if (pAddress.Company != null)
			{
				dictionary.Add("Company", pAddress.Company.ReplaceToParam());
			}
			if (pAddress.CountryId.HasValue)
			{
				dictionary.Add("CountryId", pAddress.CountryId.ReplaceToParam());
			}
			if (pAddress.StateProvinceId.HasValue)
			{
				dictionary.Add("StateProvinceId", pAddress.StateProvinceId.ReplaceToParam());
			}
			if (pAddress.City != null)
			{
				dictionary.Add("City", pAddress.City.ReplaceToParam());
			}
			if (pAddress.County != null)
			{
				dictionary.Add("County", pAddress.County.ReplaceToParam());
			}
			if (pAddress.Address1 != null)
			{
				dictionary.Add("Address1", pAddress.Address1.ReplaceToParam());
			}
			if (pAddress.Address2 != null)
			{
				dictionary.Add("Address2", pAddress.Address2.ReplaceToParam());
			}
			if (pAddress.ZipPostalCode != null)
			{
				dictionary.Add("ZipPostalCode", pAddress.ZipPostalCode.ReplaceToParam());
			}
			if (pAddress.PhoneNumber != null)
			{
				dictionary.Add("PhoneNumber", pAddress.PhoneNumber.ReplaceToParam());
			}
			if (pAddress.FaxNumber != null)
			{
				dictionary.Add("FaxNumber", pAddress.FaxNumber.ReplaceToParam());
			}
			if (pAddress.AddressName != null)
			{
				dictionary.Add("AddressName", pAddress.AddressName.ReplaceToParam());
			}
			if (pAddress.SAPCustomer != null)
			{
				dictionary.Add("SAPCustomer", pAddress.SAPCustomer.ReplaceToParam());
			}
			if (pAddress.MainAddress.HasValue)
			{
				dictionary.Add("MainAddress", pAddress.MainAddress.ReplaceToParam());
			}
			SqlUpdate("Address", dictionary, "Id=" + pAddress.Id);
		}

		public int? SelectCountry(string country)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return SelectCountryMySQL(country);
			}
			return SelectCountryMSSQL(country);
		}

		public int? SelectState(string state)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return SelectStateMySQL(state);
			}
			return SelectStateMSSQL(state);
		}

		public void DeleteGenericAttribute(int customerId, string keyGroup, string key)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				DeleteGenericAttributeMySQL(customerId, keyGroup, key);
			}
			else
			{
				DeleteGenericAttributeMSSQL(customerId, keyGroup, key);
			}
		}

		public int SelectGenericAttribute(int customerId, string keyGroup, string key)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return SelectGenericAttributeMySQL(customerId, keyGroup, key);
			}
			return SelectGenericAttributeMSSQL(customerId, keyGroup, key);
		}

		public string SelectGenericAttributeValue(int customerId, string keyGroup, string key)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return SelectGenericAttributeValueMySQL(customerId, keyGroup, key);
			}
			return SelectGenericAttributeValueMSSQL(customerId, keyGroup, key);
		}

		public int InsertUpdateGenericAttribute(int customerId, string keyGroup, string key, string value, int storeId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertUpdateGenericAttributeMySQL(customerId, keyGroup, key, value, storeId);
			}
			return InsertUpdateGenericAttributeMSSQL(customerId, keyGroup, key, value, storeId);
		}

		public int UpdateGenericAttribute(int attId, string value, int storeId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return UpdateGenericAttributeMySQL(attId, value, storeId);
			}
			return UpdateGenericAttributeMSSQL(attId, value, storeId);
		}

		public int GetNewsLetterSubscription(string email, string storeId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetNewsLetterSubscriptionMySQL(email, storeId);
			}
			return GetNewsLetterSubscriptionMSSQL(email, storeId);
		}

		public int InsertNewsLetterSubscription(string email, string storeId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertNewsLetterSubscriptionMySQL(email, storeId);
			}
			return InsertNewsLetterSubscriptionMSSQL(email, storeId);
		}

		public int InsertUpdateCustomerAttribute(string customerAttributeName, int? attributeControlTypeId, bool? isRequired)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertUpdateCustomerAttributeMySQL(customerAttributeName, attributeControlTypeId, isRequired);
			}
			return InsertUpdateCustomerAttributeMSSQL(customerAttributeName, attributeControlTypeId, isRequired);
		}

		public int InsertCustomerAttribute(string customerAttributeName, int? attributeControlTypeId, bool? isRequired)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertCustomerAttributeMySQL(customerAttributeName, attributeControlTypeId, isRequired);
			}
			return InsertCustomerAttributeMSSQL(customerAttributeName, attributeControlTypeId, isRequired);
		}

		public int UpdateCustomerAttribute(int customerAttId, string customerAttributeName, int? attributeControlTypeId, bool? isRequired)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return UpdateCustomerAttributeMySQL(customerAttId, customerAttributeName, attributeControlTypeId, isRequired);
			}
			return UpdateCustomerAttributeMSSQL(customerAttId, customerAttributeName, attributeControlTypeId, isRequired);
		}

		public int InsertUpdateCustomerAttributeValue(int customerAttributeId, string customerAttributeName, bool? isPreselected)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertUpdateCustomerAttributeValueMySQL(customerAttributeId, customerAttributeName, isPreselected);
			}
			return InsertUpdateCustomerAttributeValueMSSQL(customerAttributeId, customerAttributeName, isPreselected);
		}

		public int InsertCustomerAttributeValue(int customerAttributeValueId, string customerAttributeName, bool? isPreselected)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertCustomerAttributeValueMySQL(customerAttributeValueId, customerAttributeName, isPreselected);
			}
			return InsertCustomerAttributeValueMSSQL(customerAttributeValueId, customerAttributeName, isPreselected);
		}

		public void UpdateCustomerAttributeValue(int customerAttributeValueId, string customerAttributeName, bool? isPreselected)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				UpdateCustomerAttributeValueMySQL(customerAttributeValueId, customerAttributeName, isPreselected);
			}
			else
			{
				UpdateCustomerAttributeValueMSSQL(customerAttributeValueId, customerAttributeName, isPreselected);
			}
		}

		public string SelectAddressAttributeValue(long addressId)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return SelectAddressAttributeValueMySQL(addressId);
			}
			return SelectAddressAttributeValueMSSQL(addressId);
		}

		public int InsertUpdateAddressAttribute(string attributeName, int? attributeControlTypeId, bool? isRequired)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertUpdateAddressAttributeMySQL(attributeName, attributeControlTypeId, isRequired);
			}
			return InsertUpdateAddressAttributeMSSQL(attributeName, attributeControlTypeId, isRequired);
		}

		public int InsertAddressAttribute(string customerAttributeName, int? attributeControlTypeId, bool? isRequired)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return InsertAddressAttributeMySQL(customerAttributeName, attributeControlTypeId, isRequired);
			}
			return InsertAddressAttributeMSSQL(customerAttributeName, attributeControlTypeId, isRequired);
		}

		public int UpdateAddressAttribute(int attId, string customerAttributeName, int? attributeControlTypeId, bool? isRequired)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return UpdateAddressAttributeMySQL(attId, customerAttributeName, attributeControlTypeId, isRequired);
			}
			return UpdateAddressAttributeMSSQL(attId, customerAttributeName, attributeControlTypeId, isRequired);
		}

		public int UpdateAddressCustomAttribute(long addressId, string attributeValue)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return UpdateAddressCustomAttributeMySQL(addressId, attributeValue);
			}
			return UpdateAddressCustomAttributeMSSQL(addressId, attributeValue);
		}

		public int GetMeasureWeight(string systemKeyword)
		{
			if (_databaseType == DatabaseType.MySQL)
			{
				return GetMeasureWeightMySQL(systemKeyword);
			}
			return GetMeasureWeightMSSQL(systemKeyword);
		}

		public void SelectCustomerMSSQL(int userIdInStore, string userName, string email, string sAPCustomer, out int customerId, out int billingAddressId, out int shippingAddressId)
		{
			customerId = 0;
			billingAddressId = 0;
			shippingAddressId = 0;
			string cmdText = "Select TOP 1 Id, BillingAddress_Id, ShippingAddress_Id From [Customer] WITH (NOLOCK) Where [Username] = @username And [Email] = @email";
			switch (userIdInStore)
			{
			case 0:
				cmdText = "Select TOP 1 Id, BillingAddress_Id, ShippingAddress_Id From [Customer] WITH (NOLOCK) Where [Email] = @email";
				break;
			case 1:
				cmdText = "Select TOP 1 Id, BillingAddress_Id, ShippingAddress_Id From [Customer] WITH (NOLOCK) Where [Username] = @username";
				break;
			case 2:
				cmdText = "Select TOP 1 Id, BillingAddress_Id, ShippingAddress_Id From [Customer] WITH (NOLOCK) Where [Username] = @username And [Email] = @email";
				break;
			case 3:
				cmdText = "Select TOP 1 Id, BillingAddress_Id, ShippingAddress_Id From [Customer] WITH (NOLOCK) Where [SAPCustomer] = @sAPCustomer";
				break;
			}
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					switch (userIdInStore)
					{
					case 0:
						sqlCommand.Parameters.Add(new SqlParameter("@email", email));
						break;
					case 1:
						sqlCommand.Parameters.Add(new SqlParameter("@username", userName));
						break;
					case 2:
						sqlCommand.Parameters.Add(new SqlParameter("@username", userName));
						sqlCommand.Parameters.Add(new SqlParameter("@email", email));
						break;
					case 3:
						sqlCommand.Parameters.Add(new SqlParameter("@sAPCustomer", sAPCustomer));
						break;
					}
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							customerId = sqlDataReader.GetInt32(0);
							billingAddressId = GlobalClass.StringToInteger(sqlDataReader[1]);
							shippingAddressId = GlobalClass.StringToInteger(sqlDataReader[2]);
						}
					}
				}
			}
		}

		public int InsertCustomerMSSQL(NopCustomer nopCustomer)
		{
			int result = 0;
			string format = "\r\n            INSERT INTO [Customer]\r\n                   ([CustomerGuid]\r\n                   ,[Username]\r\n                   ,[Email]\r\n                   ,[EmailToRevalidate]\r\n                   ,[AdminComment]\r\n                   ,[IsTaxExempt]\r\n                   ,[AffiliateId]\r\n                   ,[VendorId]\r\n                   ,[HasShoppingCartItems]\r\n                   ,[RequireReLogin]\r\n                   ,[FailedLoginAttempts]\r\n                   ,[CannotLoginUntilDateUtc]\r\n                   ,[Active]\r\n                   ,[Deleted]\r\n                   ,[IsSystemAccount]\r\n                   ,[SystemName]\r\n                   ,[LastIpAddress]\r\n                   ,[CreatedOnUtc]\r\n                   ,[LastLoginDateUtc]\r\n                   ,[LastActivityDateUtc]\r\n                   ,[RegisteredInStoreId]\r\n                   ,[BillingAddress_Id]\r\n                   ,[ShippingAddress_Id]\r\n                   {0})\r\n             VALUES\r\n                   (@CustomerGuid\r\n                   ,@Username\r\n                   ,@Email\r\n                   ,@EmailToRevalidate\r\n                   ,@AdminComment\r\n                   ,@IsTaxExempt\r\n                   ,@AffiliateId\r\n                   ,@VendorId\r\n                   ,@HasShoppingCartItems\r\n                   ,@RequireReLogin\r\n                   ,@FailedLoginAttempts\r\n                   ,@CannotLoginUntilDateUtc\r\n                   ,@Active\r\n                   ,@Deleted\r\n                   ,@IsSystemAccount\r\n                   ,@SystemName\r\n                   ,@LastIpAddress\r\n                   ,GETDATE()\r\n                   ,@LastLoginDateUtc\r\n                   ,@LastActivityDateUtc\r\n                   ,@RegisteredInStoreId\r\n                   ,@BillingAddress_Id\r\n                   ,@ShippingAddress_Id\r\n                   {1})";
			string text = "";
			string text2 = "";
			if (nopCustomer.SAPCustomer != null)
			{
				text = ",[SAPCustomer]";
				text2 = ",@SAPCustomer";
			}
			if (nopCustomer.GPCustomer != null)
			{
				text += ",[GPCustomer]";
				text2 += ",@GPCustomer";
			}
			format = ((!string.IsNullOrEmpty(text)) ? string.Format(format, text, text2) : string.Format(format, "", ""));
			format += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(format, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@CustomerGuid", nopCustomer.CustomerGuid.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@Username", nopCustomer.Username.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@Email", nopCustomer.Email.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@EmailToRevalidate", nopCustomer.EmailToRevalidate.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@AdminComment", nopCustomer.AdminComment.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@IsTaxExempt", nopCustomer.IsTaxExempt.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@AffiliateId", nopCustomer.AffiliateId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@VendorId", nopCustomer.VendorId.GetValueOrDefault().ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@HasShoppingCartItems", nopCustomer.HasShoppingCartItems.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@RequireReLogin", nopCustomer.RequireReLogin.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@FailedLoginAttempts", nopCustomer.FailedLoginAttempts.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@CannotLoginUntilDateUtc", nopCustomer.CannotLoginUntilDateUtc.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@Active", (nopCustomer.Active ?? true).ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@Deleted", nopCustomer.Deleted.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@IsSystemAccount", nopCustomer.IsSystemAccount.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@SystemName", nopCustomer.SystemName.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@LastIpAddress", nopCustomer.LastIpAddress.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@LastLoginDateUtc", nopCustomer.LastLoginDateUtc.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@LastActivityDateUtc", nopCustomer.LastActivityDateUtc.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@RegisteredInStoreId", nopCustomer.RegisteredInStoreId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@BillingAddress_Id", nopCustomer.BillingAddress_Id.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@ShippingAddress_Id", nopCustomer.ShippingAddress_Id.ReplaceToParam()));
					if (nopCustomer.SAPCustomer != null)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@SAPCustomer", nopCustomer.SAPCustomer.ReplaceToParam()));
					}
					if (nopCustomer.GPCustomer != null)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@GPCustomer", nopCustomer.GPCustomer.ReplaceToParam()));
					}
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int InsertAddressMSSQL(NopAddress nopAddress)
		{
			int result = 0;
			string text = "";
			string text2 = "";
			string format = "\r\n\r\n            INSERT INTO [Address]\r\n                   ([FirstName]\r\n                   ,[LastName]\r\n                   ,[Email]\r\n                   ,[Company]\r\n                   ,[CountryId]\r\n                   ,[StateProvinceId]\r\n                   ,[City]\r\n                   ,[Address1]\r\n                   ,[Address2]\r\n                   ,[ZipPostalCode]\r\n                   ,[PhoneNumber]\r\n                   ,[FaxNumber]\r\n                   ,[CustomAttributes]\r\n                   ,[CreatedOnUtc]\r\n                   {0})\r\n             VALUES\r\n                   (@FirstName\r\n                   ,@LastName\r\n                   ,@Email\r\n                   ,@Company\r\n                   ,@CountryId\r\n                   ,@StateProvinceId\r\n                   ,@City\r\n                   ,@Address1\r\n                   ,@Address2\r\n                   ,@ZipPostalCode\r\n                   ,@PhoneNumber\r\n                   ,@FaxNumber\r\n                   ,@CustomAttributes\r\n                   ,GETDATE()\r\n                   {1})";
			if (nopAddress.AddressName != null)
			{
				text = ",[AddressName]";
				text2 = ",@AddressName";
			}
			if (nopAddress.SAPCustomer != null)
			{
				text += ",[SAPCustomer]";
				text2 += ",@SAPCustomer";
			}
			if (nopAddress.MainAddress.HasValue)
			{
				text += ",[MainAddress]";
				text2 += ",@MainAddress";
			}
			format = string.Format(format, text, text2);
			format += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(format, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@FirstName", nopAddress.FirstName.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@LastName", nopAddress.LastName.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@Email", nopAddress.Email.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@Company", nopAddress.Company.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@CountryId", nopAddress.CountryId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@StateProvinceId", nopAddress.StateProvinceId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@City", nopAddress.City.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@Address1", nopAddress.Address1.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@Address2", nopAddress.Address2.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@ZipPostalCode", nopAddress.ZipPostalCode.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@PhoneNumber", nopAddress.PhoneNumber.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@FaxNumber", nopAddress.FaxNumber.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@CustomAttributes", nopAddress.CustomAttributes.ReplaceToParam()));
					if (nopAddress.AddressName != null)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@AddressName", nopAddress.AddressName.ReplaceToParam()));
					}
					if (nopAddress.SAPCustomer != null)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@SAPCustomer", nopAddress.SAPCustomer.ReplaceToParam()));
					}
					if (nopAddress.MainAddress.HasValue)
					{
						sqlCommand.Parameters.Add(new SqlParameter("@MainAddress", nopAddress.MainAddress.ReplaceToParam()));
					}
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteCustomerRoleMappingMSSQL(int customerId)
		{
			string cmdText = "Delete From Customer_CustomerRole_Mapping Where [Customer_Id] = @Customer_Id";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@Customer_Id", customerId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int SelectAddressMSSQL(string email)
		{
			if (string.IsNullOrEmpty(email))
			{
				return 0;
			}
			int result = 0;
			string cmdText = "Select TOP 1 Id From [Address] WITH (NOLOCK) Where [Email] = @email";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@email", email));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							result = sqlDataReader.GetInt32(0);
						}
					}
				}
				return result;
			}
		}

		public void DeleteCustomerRoleMappingMSSQL(int customerId, int customerRole_Id)
		{
			string cmdText = "Delete From Customer_CustomerRole_Mapping Where [Customer_Id] = @Customer_Id AND [CustomerRole_Id] = @CustomerRole_Id";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@Customer_Id", customerId));
					sqlCommand.Parameters.Add(new SqlParameter("@CustomerRole_Id", customerRole_Id));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertCustomerRoleMappingMSSQL(int customerId, int customerRoleId)
		{
			int result = 0;
			string str = "\r\n            INSERT INTO [Customer_CustomerRole_Mapping]\r\n                       ([Customer_Id]\r\n                       ,[CustomerRole_Id])\r\n                 VALUES\r\n                       (@Customer_Id\r\n                       ,@CustomerRole_Id)";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@Customer_Id", customerId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@CustomerRole_Id", customerRoleId.ReplaceToParam()));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteCustomerAddressMappingMSSQL(int customerId)
		{
			string cmdText = "Delete From Address Where Id IN (select [Address_Id] FROM [CustomerAddresses] where [Customer_Id] = @Customer_Id AND NOT EXISTS (select Id from Customer WHERE [BillingAddress_Id]  = CustomerAddresses.Address_Id OR [ShippingAddress_Id]  = CustomerAddresses.Address_Id))";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@Customer_Id", customerId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void DeleteCustomerAddressMappingMSSQL(int customerId, int addressId)
		{
			string cmdText = "Delete From CustomerAddresses Where [Customer_Id] = @Customer_Id AND [Address_Id] = @Address_Id";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@Customer_Id", customerId));
					sqlCommand.Parameters.Add(new SqlParameter("@Address_Id", addressId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertCustomerAddressMappingMSSQL(int customerId, int addressId)
		{
			int result = 0;
			string str = "\r\n           INSERT INTO [CustomerAddresses]\r\n                       ([Customer_Id]\r\n                       ,[Address_Id])\r\n                 VALUES\r\n                       (@Customer_Id\r\n                       ,@Address_Id)";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@Customer_Id", customerId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@Address_Id", addressId.ReplaceToParam()));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteCustomerPasswordMSSQL(int customerId)
		{
			string cmdText = "Delete From CustomerPassword Where [CustomerId] = @customerId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@customerId", customerId));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertCustomerPasswordMSSQL(int customerId, string password)
		{
			int result = 0;
			string str = "\r\n           INSERT INTO [CustomerPassword]\r\n                   ([CustomerId]\r\n                   ,[Password]\r\n                   ,[PasswordFormatId]\r\n                   ,[PasswordSalt]\r\n                   ,[CreatedOnUtc])\r\n             VALUES\r\n                   (@CustomerId\r\n                   ,@Password\r\n                   ,@PasswordFormatId\r\n                   ,@PasswordSalt\r\n                   ,GETDATE())";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", customerId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@Password", password.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@PasswordFormatId", "0"));
					sqlCommand.Parameters.Add(new SqlParameter("@PasswordSalt", DBNull.Value));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteGenericAttributeMSSQL(int customerId, string keyGroup, string key)
		{
			string cmdText = "Delete From GenericAttribute Where [EntityId] = @customerId AND KeyGroup = @keyGroup AND [Key] = @key";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@customerId", customerId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@keyGroup", keyGroup.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@key", key.ReplaceToParam()));
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int SelectGenericAttributeMSSQL(int customerId, string keyGroup, string key)
		{
			if (customerId == 0 || string.IsNullOrEmpty(keyGroup) || string.IsNullOrEmpty(key))
			{
				return 0;
			}
			int result = 0;
			string cmdText = "Select TOP 1 Id From [GenericAttribute] WITH (NOLOCK) Where [EntityId] = @EntityId AND [KeyGroup] = @KeyGroup AND [Key] = @Key";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@EntityId", customerId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@KeyGroup", keyGroup.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@Key", key.ReplaceToParam()));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							result = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		public string SelectGenericAttributeValueMSSQL(int customerId, string keyGroup, string key)
		{
			if (customerId == 0 || string.IsNullOrEmpty(keyGroup) || string.IsNullOrEmpty(key))
			{
				return null;
			}
			string result = null;
			string cmdText = "Select TOP 1 [Value] From [GenericAttribute] WITH (NOLOCK) Where [EntityId] = @EntityId AND [KeyGroup] = @KeyGroup AND [Key] = @Key";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@EntityId", customerId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@KeyGroup", keyGroup.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@Key", key.ReplaceToParam()));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							result = sqlDataReader.GetString(0);
						}
					}
				}
			}
			return result;
		}

		public int InsertUpdateGenericAttributeMSSQL(int customerId, string keyGroup, string key, string value, int storeId)
		{
			int result = 0;
			int num = SelectGenericAttribute(customerId, keyGroup, key);
			if (num > 0)
			{
				UpdateGenericAttribute(num, value, storeId);
				result = num;
			}
			else
			{
				string str = "\r\n                                           INSERT INTO [GenericAttribute]\r\n                                           ([EntityId]\r\n                                           ,[KeyGroup]\r\n                                           ,[Key]\r\n                                           ,[Value]\r\n                                           ,[StoreId])\r\n                                     VALUES\r\n                                           (@EntityId\r\n                                           ,@KeyGroup\r\n                                           ,@Key\r\n                                           ,@Value\r\n                                           ,@StoreId)";
				str += "; SELECT SCOPE_IDENTITY() AS insertId;";
				using (SqlConnection sqlConnection = new SqlConnection(_connString))
				{
					using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
					{
						sqlCommand.Parameters.Add(new SqlParameter("@EntityId", customerId.ReplaceToParam()));
						sqlCommand.Parameters.Add(new SqlParameter("@KeyGroup", keyGroup.ReplaceToParam()));
						sqlCommand.Parameters.Add(new SqlParameter("@Key", key.ReplaceToParam()));
						sqlCommand.Parameters.Add(new SqlParameter("@Value", value.ReplaceToParam()));
						sqlCommand.Parameters.Add(new SqlParameter("@StoreId", storeId.ReplaceToParam()));
						sqlConnection.Open();
						result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
					}
				}
			}
			return result;
		}

		public int UpdateGenericAttributeMSSQL(int attId, string value, int storeId)
		{
			int result = 0;
			string cmdText = "\r\n                   UPDATE [GenericAttribute] SET\r\n                   [Value] = @AttValue\r\n                   ,[StoreId] = @StoreId\r\n                   WHERE Id=@AttId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@AttId", attId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@AttValue", value.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@StoreId", storeId.ReplaceToParam()));
					sqlConnection.Open();
					sqlCommand.ExecuteNonQuery();
				}
			}
			return result;
		}

		public int GetNewsLetterSubscriptionMSSQL(string email, string storeId)
		{
			int num = 0;
			string cmdText = "Select Id From [NewsLetterSubscription] WITH (NOLOCK) Where [Email] = @email AND StoreId = @storeId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@email", email.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@storeId", storeId.ReplaceToParam()));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							num = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertNewsLetterSubscription(email, storeId);
			}
			return num;
		}

		public int InsertNewsLetterSubscriptionMSSQL(string email, string storeId)
		{
			int result = 0;
			string str = "\r\n              INSERT INTO [NewsLetterSubscription]\r\n                       ([NewsLetterSubscriptionGuid]\r\n                       ,[Email]\r\n                       ,[Active]\r\n                       ,[StoreId]\r\n                       ,[CreatedOnUtc])\r\n                 VALUES\r\n                       (@NewsLetterSubscriptionGuid\r\n                       ,@Email\r\n                       ,@Active\r\n                       ,@StoreId\r\n                       ,GETDATE())";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@NewsLetterSubscriptionGuid", Guid.NewGuid()));
					sqlCommand.Parameters.Add(new SqlParameter("@Email", email.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@Active", "1"));
					sqlCommand.Parameters.Add(new SqlParameter("@storeId", storeId.ReplaceToParam()));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int InsertUpdateCustomerAttributeMSSQL(string customerAttributeName, int? attributeControlTypeId, bool? isRequired)
		{
			int num = 0;
			string cmdText = "Select Id From [CustomerAttribute] WITH (NOLOCK) Where Name = @searchValue";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@searchValue", customerAttributeName));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							num = sqlDataReader.GetInt32(0);
							if (num > 0)
							{
								UpdateCustomerAttribute(num, customerAttributeName, attributeControlTypeId, isRequired);
							}
						}
					}
					if (num == 0)
					{
						num = InsertCustomerAttribute(customerAttributeName, attributeControlTypeId, isRequired);
					}
				}
			}
			return num;
		}

		public int InsertCustomerAttributeMSSQL(string customerAttributeName, int? attributeControlTypeId, bool? isRequired)
		{
			int result = 0;
			string str = "\r\n                              INSERT INTO [CustomerAttribute]\r\n                                   ([Name]\r\n                                   ,[AttributeControlTypeId]\r\n                                   ,[IsRequired]\r\n                                   ,[DisplayOrder])\r\n                             VALUES\r\n                                   (@CustomerAttributeName\r\n                                   ,@AttributeControlTypeId\r\n                                   ,@IsRequired\r\n                                   ,@DisplayOrder)";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@CustomerAttributeName", customerAttributeName.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@AttributeControlTypeId", attributeControlTypeId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@IsRequired", isRequired));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", "0".ReplaceToParam()));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int UpdateCustomerAttributeMSSQL(int customerAttId, string customerAttributeName, int? attributeControlTypeId, bool? isRequired)
		{
			int result = 0;
			string cmdText = "\r\n                              UPDATE [CustomerAttribute]\r\n                               SET [Name] = @CustomerAttributeName\r\n                                  ,[IsRequired] = @IsRequired\r\n                                  ,[AttributeControlTypeId] = @AttributeControlTypeId\r\n                                  ,[DisplayOrder] = @DisplayOrder\r\n                               WHERE Id=@CustomerAttributeId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@CustomerAttributeName", customerAttributeName.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@AttributeControlTypeId", attributeControlTypeId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@IsRequired", isRequired));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", "0".ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@CustomerAttributeId", customerAttId.ReplaceToParam()));
					sqlConnection.Open();
					sqlCommand.ExecuteNonQuery();
				}
			}
			return result;
		}

		public int InsertUpdateCustomerAttributeValueMSSQL(int customerAttributeId, string customerAttributeName, bool? isPreselected)
		{
			int num = 0;
			string cmdText = "Select Id From CustomerAttributeValue Where CustomerAttributeId = @customerAttributeId AND Name = @customerAttributeName";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@customerAttributeId", customerAttributeId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@customerAttributeName", customerAttributeName.ReplaceToParam()));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							num = sqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertCustomerAttributeValue(customerAttributeId, customerAttributeName, isPreselected);
			}
			else
			{
				UpdateCustomerAttributeValue(num, customerAttributeName, isPreselected);
			}
			return num;
		}

		public int InsertCustomerAttributeValueMSSQL(int customerAttributeValueId, string customerAttributeName, bool? isPreselected)
		{
			int result = 0;
			string str = "\r\n                              INSERT INTO [CustomerAttributeValue]\r\n                                   ([CustomerAttributeId]\r\n                                   ,[Name]\r\n                                   ,[IsPreSelected]\r\n                                   ,[DisplayOrder])\r\n                             VALUES\r\n                                   (@CustomerAttributeValueId\r\n                                   ,@CustomerAttributeName\r\n                                   ,@IsPreSelected\r\n                                   ,@DisplayOrder)";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@CustomerAttributeValueId", customerAttributeValueId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@CustomerAttributeName", customerAttributeName.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@IsPreSelected", isPreselected));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", "0".ReplaceToParam()));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void UpdateCustomerAttributeValueMSSQL(int customerAttributeValueId, string customerAttributeName, bool? isPreselected)
		{
			string cmdText = "\r\n                              UPDATE [CustomerAttributeValue]\r\n                                   SET [Name] = @CustomerAttributeName,\r\n                                       [IsPreSelected] = @IsPreSelected\r\n                              WHERE Id = @CustomerAttributeValueId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@CustomerAttributeValueId", customerAttributeValueId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@CustomerAttributeName", customerAttributeName.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@IsPreSelected", isPreselected.ReplaceToParam()));
					sqlConnection.Open();
					sqlCommand.ExecuteNonQuery();
				}
			}
		}

		public string SelectAddressAttributeValueMSSQL(long addressId)
		{
			if (addressId == 0)
			{
				return null;
			}
			string result = null;
			string cmdText = "Select TOP 1 [CustomAttributes] From [Address] WITH (NOLOCK) Where [Id] = @AddressId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@AddressId", addressId.ReplaceToParam()));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							result = sqlDataReader[0].ToString();
						}
					}
				}
			}
			return result;
		}

		public int InsertUpdateAddressAttributeMSSQL(string attributeName, int? attributeControlTypeId, bool? isRequired)
		{
			int num = 0;
			string cmdText = "Select Id From [AddressAttribute] WITH (NOLOCK) Where Name = @searchValue";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlConnection.Open();
					sqlCommand.Parameters.Add(new SqlParameter("@searchValue", attributeName.ReplaceToParam()));
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
					{
						if (sqlDataReader.Read())
						{
							num = sqlDataReader.GetInt32(0);
							if (num > 0)
							{
								UpdateAddressAttribute(num, attributeName, attributeControlTypeId, isRequired);
							}
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertAddressAttribute(attributeName, attributeControlTypeId, isRequired);
			}
			return num;
		}

		public int InsertAddressAttributeMSSQL(string customerAttributeName, int? attributeControlTypeId, bool? isRequired)
		{
			int result = 0;
			string str = "\r\n                              INSERT INTO [AddressAttribute]\r\n                                   ([Name]\r\n                                   ,[AttributeControlTypeId]\r\n                                   ,[IsRequired]\r\n                                   ,[DisplayOrder])\r\n                             VALUES\r\n                                   (@CustomerAttributeName\r\n                                   ,@AttributeControlTypeId\r\n                                   ,@IsRequired\r\n                                   ,@DisplayOrder)";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(str, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@CustomerAttributeName", customerAttributeName.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@AttributeControlTypeId", attributeControlTypeId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@IsRequired", isRequired));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", "0".ReplaceToParam()));
					sqlConnection.Open();
					result = GlobalClass.StringToInteger(sqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int UpdateAddressAttributeMSSQL(int attId, string customerAttributeName, int? attributeControlTypeId, bool? isRequired)
		{
			int result = 0;
			string cmdText = "\r\n                              UPDATE [AddressAttribute]\r\n                               SET [Name] = @CustomerAttributeName\r\n                                  ,[IsRequired] = @IsRequired\r\n                                  ,[AttributeControlTypeId] = @AttributeControlTypeId\r\n                                  ,[DisplayOrder] = @DisplayOrder\r\n                               WHERE Id=@CustomerAttributeId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@CustomerAttributeName", customerAttributeName.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@AttributeControlTypeId", attributeControlTypeId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@IsRequired", isRequired));
					sqlCommand.Parameters.Add(new SqlParameter("@DisplayOrder", "0".ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@CustomerAttributeId", attId.ReplaceToParam()));
					sqlConnection.Open();
					sqlCommand.ExecuteNonQuery();
				}
			}
			return result;
		}

		public int UpdateAddressCustomAttributeMSSQL(long addressId, string attributeValue)
		{
			int result = 0;
			string cmdText = "\r\n                              UPDATE [Address]\r\n                               SET\r\n                                   [CustomAttributes] = @AttributeValue\r\n                               WHERE Id=@AddressId";
			using (SqlConnection sqlConnection = new SqlConnection(_connString))
			{
				using (SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("@AddressId", addressId.ReplaceToParam()));
					sqlCommand.Parameters.Add(new SqlParameter("@AttributeValue", attributeValue.ReplaceToParam()));
					sqlConnection.Open();
					sqlCommand.ExecuteNonQuery();
				}
			}
			return result;
		}

		public void SelectCustomerMySQL(int userIdInStore, string userName, string email, string sAPCustomer, out int customerId, out int billingAddressId, out int shippingAddressId)
		{
			customerId = 0;
			billingAddressId = 0;
			shippingAddressId = 0;
			string str = "Select Id, BillingAddress_Id, ShippingAddress_Id From Customer  Where Username = @username And Email = @email";
			switch (userIdInStore)
			{
			case 0:
				str = "Select  Id, BillingAddress_Id, ShippingAddress_Id From Customer  Where Email = @email";
				break;
			case 1:
				str = "Select  Id, BillingAddress_Id, ShippingAddress_Id From Customer  Where Username = @username";
				break;
			case 2:
				str = "Select  Id, BillingAddress_Id, ShippingAddress_Id From Customer  Where Username = @username And Email = @email";
				break;
			case 3:
				str = "Select  Id, BillingAddress_Id, ShippingAddress_Id From Customer  Where SAPCustomer = @sAPCustomer";
				break;
			}
			str += " LIMIT 1";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					switch (userIdInStore)
					{
					case 0:
						mySqlCommand.Parameters.Add(new MySqlParameter("@email", email));
						break;
					case 1:
						mySqlCommand.Parameters.Add(new MySqlParameter("@username", userName));
						break;
					case 2:
						mySqlCommand.Parameters.Add(new MySqlParameter("@username", userName));
						mySqlCommand.Parameters.Add(new MySqlParameter("@email", email));
						break;
					case 3:
						mySqlCommand.Parameters.Add(new MySqlParameter("@sAPCustomer", sAPCustomer));
						break;
					}
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							customerId = mySqlDataReader.GetInt32(0);
							billingAddressId = GlobalClass.StringToInteger(mySqlDataReader[1]);
							shippingAddressId = GlobalClass.StringToInteger(mySqlDataReader[2]);
						}
					}
				}
			}
		}

		public int InsertCustomerMySQL(NopCustomer nopCustomer)
		{
			int result = 0;
			string format = "\r\n            INSERT INTO Customer\r\n                   (CustomerGuid\r\n                   ,Username\r\n                   ,Email\r\n                   ,EmailToRevalidate\r\n                   ,AdminComment\r\n                   ,IsTaxExempt\r\n                   ,AffiliateId\r\n                   ,VendorId\r\n                   ,HasShoppingCartItems\r\n                   ,RequireReLogin\r\n                   ,FailedLoginAttempts\r\n                   ,CannotLoginUntilDateUtc\r\n                   ,Active\r\n                   ,Deleted\r\n                   ,IsSystemAccount\r\n                   ,SystemName\r\n                   ,LastIpAddress\r\n                   ,CreatedOnUtc\r\n                   ,LastLoginDateUtc\r\n                   ,LastActivityDateUtc\r\n                   ,RegisteredInStoreId\r\n                   ,BillingAddress_Id\r\n                   ,ShippingAddress_Id\r\n                   {0})\r\n             VALUES\r\n                   (@CustomerGuid\r\n                   ,@Username\r\n                   ,@Email\r\n                   ,@EmailToRevalidate\r\n                   ,@AdminComment\r\n                   ,@IsTaxExempt\r\n                   ,@AffiliateId\r\n                   ,@VendorId\r\n                   ,@HasShoppingCartItems\r\n                   ,@RequireReLogin\r\n                   ,@FailedLoginAttempts\r\n                   ,@CannotLoginUntilDateUtc\r\n                   ,@Active\r\n                   ,@Deleted\r\n                   ,@IsSystemAccount\r\n                   ,@SystemName\r\n                   ,@LastIpAddress\r\n                   ,NOW()\r\n                   ,@LastLoginDateUtc\r\n                   ,@LastActivityDateUtc\r\n                   ,@RegisteredInStoreId\r\n                   ,@BillingAddress_Id\r\n                   ,@ShippingAddress_Id\r\n                   {1})";
			string text = "";
			string text2 = "";
			if (nopCustomer.SAPCustomer != null)
			{
				text = ",SAPCustomer";
				text2 = ",@SAPCustomer";
			}
			if (nopCustomer.GPCustomer != null)
			{
				text += ",GPCustomer";
				text2 += ",@GPCustomer";
			}
			format = ((!string.IsNullOrEmpty(text)) ? string.Format(format, text, text2) : string.Format(format, "", ""));
			format += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(format.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerGuid", nopCustomer.CustomerGuid.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Username", nopCustomer.Username.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Email", nopCustomer.Email.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@EmailToRevalidate", nopCustomer.EmailToRevalidate.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AdminComment", nopCustomer.AdminComment.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsTaxExempt", nopCustomer.IsTaxExempt.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AffiliateId", nopCustomer.AffiliateId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@VendorId", nopCustomer.VendorId.GetValueOrDefault().ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@HasShoppingCartItems", nopCustomer.HasShoppingCartItems.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@RequireReLogin", nopCustomer.RequireReLogin.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@FailedLoginAttempts", nopCustomer.FailedLoginAttempts.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@CannotLoginUntilDateUtc", nopCustomer.CannotLoginUntilDateUtc.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Active", (nopCustomer.Active ?? true).ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Deleted", nopCustomer.Deleted.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsSystemAccount", nopCustomer.IsSystemAccount.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@SystemName", nopCustomer.SystemName.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@LastIpAddress", nopCustomer.LastIpAddress.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@LastLoginDateUtc", nopCustomer.LastLoginDateUtc.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@LastActivityDateUtc", nopCustomer.LastActivityDateUtc.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@RegisteredInStoreId", nopCustomer.RegisteredInStoreId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@BillingAddress_Id", nopCustomer.BillingAddress_Id.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ShippingAddress_Id", nopCustomer.ShippingAddress_Id.ReplaceToParam()));
					if (nopCustomer.SAPCustomer != null)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@SAPCustomer", nopCustomer.SAPCustomer.ReplaceToParam()));
					}
					if (nopCustomer.GPCustomer != null)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@GPCustomer", nopCustomer.GPCustomer.ReplaceToParam()));
					}
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int InsertAddressMySQL(NopAddress nopAddress)
		{
			int result = 0;
			string text = "";
			string text2 = "";
			string format = "\r\n\r\n            INSERT INTO [Address]\r\n                   ([FirstName]\r\n                   ,[LastName]\r\n                   ,[Email]\r\n                   ,[Company]\r\n                   ,[CountryId]\r\n                   ,[StateProvinceId]\r\n                   ,[City]\r\n                   ,[Address1]\r\n                   ,[Address2]\r\n                   ,[ZipPostalCode]\r\n                   ,[PhoneNumber]\r\n                   ,[FaxNumber]\r\n                   ,[CustomAttributes]\r\n                   ,[CreatedOnUtc]\r\n                   {0})\r\n             VALUES\r\n                   (@FirstName\r\n                   ,@LastName\r\n                   ,@Email\r\n                   ,@Company\r\n                   ,@CountryId\r\n                   ,@StateProvinceId\r\n                   ,@City\r\n                   ,@Address1\r\n                   ,@Address2\r\n                   ,@ZipPostalCode\r\n                   ,@PhoneNumber\r\n                   ,@FaxNumber\r\n                   ,@CustomAttributes\r\n                   ,NOW()\r\n                   {1})";
			if (nopAddress.AddressName != null)
			{
				text = ",[AddressName]";
				text2 = ",@AddressName";
			}
			if (nopAddress.SAPCustomer != null)
			{
				text += ",[SAPCustomer]";
				text2 += ",@SAPCustomer";
			}
			if (nopAddress.MainAddress.HasValue)
			{
				text += ",[MainAddress]";
				text2 += ",@MainAddress";
			}
			format = string.Format(format, text, text2);
			format += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(format.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@FirstName", nopAddress.FirstName.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@LastName", nopAddress.LastName.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Email", nopAddress.Email.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Company", nopAddress.Company.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@CountryId", nopAddress.CountryId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@StateProvinceId", nopAddress.StateProvinceId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@City", nopAddress.City.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Address1", nopAddress.Address1.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Address2", nopAddress.Address2.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@ZipPostalCode", nopAddress.ZipPostalCode.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@PhoneNumber", nopAddress.PhoneNumber.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@FaxNumber", nopAddress.FaxNumber.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomAttributes", nopAddress.CustomAttributes.ReplaceToParam()));
					if (nopAddress.AddressName != null)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@AddressName", nopAddress.AddressName.ReplaceToParam()));
					}
					if (nopAddress.SAPCustomer != null)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@SAPCustomer", nopAddress.SAPCustomer.ReplaceToParam()));
					}
					if (nopAddress.MainAddress.HasValue)
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@MainAddress", nopAddress.MainAddress.ReplaceToParam()));
					}
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteCustomerRoleMappingMySQL(int customerId)
		{
			string cmdText = "Delete From Customer_CustomerRole_Mapping Where Customer_Id = @Customer_Id";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(cmdText, mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@Customer_Id", customerId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int SelectAddressMySQL(string email)
		{
			if (string.IsNullOrEmpty(email))
			{
				return 0;
			}
			int result = 0;
			string obj = "Select Id From Address Where Email = @email LIMIT 1";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@email", email));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							result = mySqlDataReader.GetInt32(0);
						}
					}
				}
				return result;
			}
		}

		public void DeleteCustomerRoleMappingMySQL(int customerId, int customerRole_Id)
		{
			string obj = "Delete From Customer_CustomerRole_Mapping Where [Customer_Id] = @Customer_Id AND [CustomerRole_Id] = @CustomerRole_Id";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@Customer_Id", customerId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerRole_Id", customerRole_Id));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertCustomerRoleMappingMySQL(int customerId, int customerRoleId)
		{
			int result = 0;
			string str = "\r\n            INSERT INTO [Customer_CustomerRole_Mapping]\r\n                       ([Customer_Id]\r\n                       ,[CustomerRole_Id])\r\n                 VALUES\r\n                       (@Customer_Id\r\n                       ,@CustomerRole_Id)";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@Customer_Id", customerId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerRole_Id", customerRoleId.ReplaceToParam()));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteCustomerAddressMappingMySQL(int customerId)
		{
			string obj = "Delete From Address Where Id IN (select [Address_Id] FROM [CustomerAddresses] where [Customer_Id] = @Customer_Id AND NOT EXISTS (select Id from Customer WHERE [BillingAddress_Id]  = CustomerAddresses.Address_Id OR [ShippingAddress_Id]  = CustomerAddresses.Address_Id))";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@Customer_Id", customerId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public void DeleteCustomerAddressMappingMySQL(int customerId, int addressId)
		{
			string obj = "Delete From CustomerAddresses Where [Customer_Id] = @Customer_Id AND [Address_Id] = @Address_Id";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@Customer_Id", customerId));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Address_Id", addressId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertCustomerAddressMappingMySQL(int customerId, int addressId)
		{
			int result = 0;
			string str = "\r\n           INSERT INTO [CustomerAddresses]\r\n                       ([Customer_Id]\r\n                       ,[Address_Id])\r\n                 VALUES\r\n                       (@Customer_Id\r\n                       ,@Address_Id)";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@Customer_Id", customerId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Address_Id", addressId.ReplaceToParam()));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteCustomerPasswordMySQL(int customerId)
		{
			string obj = "Delete From CustomerPassword Where [CustomerId] = @customerId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@customerId", customerId));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int InsertCustomerPasswordMySQL(int customerId, string password)
		{
			int result = 0;
			string str = "\r\n           INSERT INTO [CustomerPassword]\r\n                   ([CustomerId]\r\n                   ,[Password]\r\n                   ,[PasswordFormatId]\r\n                   ,[PasswordSalt]\r\n                   ,[CreatedOnUtc])\r\n             VALUES\r\n                   (@CustomerId\r\n                   ,@Password\r\n                   ,@PasswordFormatId\r\n                   ,@PasswordSalt\r\n                   ,NOW())";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerId", customerId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Password", password.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@PasswordFormatId", "0"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@PasswordSalt", DBNull.Value));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void DeleteGenericAttributeMySQL(int customerId, string keyGroup, string key)
		{
			string obj = "Delete From GenericAttribute Where [EntityId] = @customerId AND KeyGroup = @keyGroup AND `Key` = @key";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@customerId", customerId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@keyGroup", keyGroup.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@key", key.ReplaceToParam()));
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public int SelectGenericAttributeMySQL(int customerId, string keyGroup, string key)
		{
			if (customerId == 0 || string.IsNullOrEmpty(keyGroup) || string.IsNullOrEmpty(key))
			{
				return 0;
			}
			int result = 0;
			string obj = "Select TOP 1 Id From [GenericAttribute] WITH (NOLOCK) Where [EntityId] = @EntityId AND [KeyGroup] = @KeyGroup AND `Key` = @Key";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@EntityId", customerId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@KeyGroup", keyGroup.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Key", key.ReplaceToParam()));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							result = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}

		public string SelectGenericAttributeValueMySQL(int customerId, string keyGroup, string key)
		{
			if (customerId == 0 || string.IsNullOrEmpty(keyGroup) || string.IsNullOrEmpty(key))
			{
				return null;
			}
			string result = null;
			string obj = "Select TOP 1 [Value] From [GenericAttribute] WITH (NOLOCK) Where [EntityId] = @EntityId AND [KeyGroup] = @KeyGroup AND `Key` = @Key";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@EntityId", customerId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@KeyGroup", keyGroup.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Key", key.ReplaceToParam()));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							result = mySqlDataReader.GetString(0);
						}
					}
				}
			}
			return result;
		}

		public int InsertUpdateGenericAttributeMySQL(int customerId, string keyGroup, string key, string value, int storeId)
		{
			int result = 0;
			int num = SelectGenericAttribute(customerId, keyGroup, key);
			if (num > 0)
			{
				UpdateGenericAttribute(num, value, storeId);
				result = num;
			}
			else
			{
				string str = "\r\n                                           INSERT INTO [GenericAttribute]\r\n                                           ([EntityId]\r\n                                           ,[KeyGroup]\r\n                                           ,`Key`\r\n                                           ,[Value]\r\n                                           ,[StoreId])\r\n                                     VALUES\r\n                                           (@EntityId\r\n                                           ,@KeyGroup\r\n                                           ,@Key\r\n                                           ,@Value\r\n                                           ,@StoreId)";
				str += "; SELECT SCOPE_IDENTITY() AS insertId;";
				using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
				{
					using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
					{
						mySqlCommand.Parameters.Add(new MySqlParameter("@EntityId", customerId.ReplaceToParam()));
						mySqlCommand.Parameters.Add(new MySqlParameter("@KeyGroup", keyGroup.ReplaceToParam()));
						mySqlCommand.Parameters.Add(new MySqlParameter("@Key", key.ReplaceToParam()));
						mySqlCommand.Parameters.Add(new MySqlParameter("@Value", value.ReplaceToParam()));
						mySqlCommand.Parameters.Add(new MySqlParameter("@StoreId", storeId.ReplaceToParam()));
						mySqlConnection.Open();
						result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
					}
				}
			}
			return result;
		}

		public int UpdateGenericAttributeMySQL(int attId, string value, int storeId)
		{
			int result = 0;
			string obj = "\r\n                   UPDATE [GenericAttribute] SET\r\n                   [Value] = @AttValue\r\n                   ,[StoreId] = @StoreId\r\n                   WHERE Id=@AttId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@AttId", attId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AttValue", value.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@StoreId", storeId.ReplaceToParam()));
					mySqlConnection.Open();
					mySqlCommand.ExecuteNonQuery();
				}
			}
			return result;
		}

		public int GetNewsLetterSubscriptionMySQL(string email, string storeId)
		{
			int num = 0;
			string obj = "Select Id From [NewsLetterSubscription] WITH (NOLOCK) Where [Email] = @email AND StoreId = @storeId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@email", email.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@storeId", storeId.ReplaceToParam()));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							num = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertNewsLetterSubscription(email, storeId);
			}
			return num;
		}

		public int InsertNewsLetterSubscriptionMySQL(string email, string storeId)
		{
			int result = 0;
			string str = "\r\n              INSERT INTO [NewsLetterSubscription]\r\n                       ([NewsLetterSubscriptionGuid]\r\n                       ,[Email]\r\n                       ,[Active]\r\n                       ,[StoreId]\r\n                       ,[CreatedOnUtc])\r\n                 VALUES\r\n                       (@NewsLetterSubscriptionGuid\r\n                       ,@Email\r\n                       ,@Active\r\n                       ,@StoreId\r\n                       ,NOW())";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@NewsLetterSubscriptionGuid", Guid.NewGuid()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Email", email.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@Active", "1"));
					mySqlCommand.Parameters.Add(new MySqlParameter("@storeId", storeId.ReplaceToParam()));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int InsertUpdateCustomerAttributeMySQL(string customerAttributeName, int? attributeControlTypeId, bool? isRequired)
		{
			int num = 0;
			string obj = "Select Id From [CustomerAttribute] WITH (NOLOCK) Where Name = @searchValue";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@searchValue", customerAttributeName));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							num = mySqlDataReader.GetInt32(0);
							if (num > 0)
							{
								UpdateCustomerAttribute(num, customerAttributeName, attributeControlTypeId, isRequired);
							}
						}
					}
					if (num == 0)
					{
						num = InsertCustomerAttribute(customerAttributeName, attributeControlTypeId, isRequired);
					}
				}
			}
			return num;
		}

		public int InsertCustomerAttributeMySQL(string customerAttributeName, int? attributeControlTypeId, bool? isRequired)
		{
			int result = 0;
			string str = "\r\n                              INSERT INTO [CustomerAttribute]\r\n                                   ([Name]\r\n                                   ,[AttributeControlTypeId]\r\n                                   ,[IsRequired]\r\n                                   ,[DisplayOrder])\r\n                             VALUES\r\n                                   (@CustomerAttributeName\r\n                                   ,@AttributeControlTypeId\r\n                                   ,@IsRequired\r\n                                   ,@DisplayOrder)";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerAttributeName", customerAttributeName.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AttributeControlTypeId", attributeControlTypeId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsRequired", isRequired));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", "0".ReplaceToParam()));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int UpdateCustomerAttributeMySQL(int customerAttId, string customerAttributeName, int? attributeControlTypeId, bool? isRequired)
		{
			int result = 0;
			string obj = "\r\n                              UPDATE [CustomerAttribute]\r\n                               SET [Name] = @CustomerAttributeName\r\n                                  ,[IsRequired] = @IsRequired\r\n                                  ,[AttributeControlTypeId] = @AttributeControlTypeId\r\n                                  ,[DisplayOrder] = @DisplayOrder\r\n                               WHERE Id=@CustomerAttributeId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerAttributeName", customerAttributeName.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AttributeControlTypeId", attributeControlTypeId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsRequired", isRequired));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", "0".ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerAttributeId", customerAttId.ReplaceToParam()));
					mySqlConnection.Open();
					mySqlCommand.ExecuteNonQuery();
				}
			}
			return result;
		}

		public int InsertUpdateCustomerAttributeValueMySQL(int customerAttributeId, string customerAttributeName, bool? isPreselected)
		{
			int num = 0;
			string obj = "Select Id From CustomerAttributeValue Where CustomerAttributeId = @customerAttributeId AND Name = @customerAttributeName";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@customerAttributeId", customerAttributeId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@customerAttributeName", customerAttributeName.ReplaceToParam()));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							num = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertCustomerAttributeValue(customerAttributeId, customerAttributeName, isPreselected);
			}
			else
			{
				UpdateCustomerAttributeValue(num, customerAttributeName, isPreselected);
			}
			return num;
		}

		public int InsertCustomerAttributeValueMySQL(int customerAttributeValueId, string customerAttributeName, bool? isPreselected)
		{
			int result = 0;
			string str = "\r\n                              INSERT INTO [CustomerAttributeValue]\r\n                                   ([CustomerAttributeId]\r\n                                   ,[Name]\r\n                                   ,[IsPreSelected]\r\n                                   ,[DisplayOrder])\r\n                             VALUES\r\n                                   (@CustomerAttributeValueId\r\n                                   ,@CustomerAttributeName\r\n                                   ,@IsPreSelected\r\n                                   ,@DisplayOrder)";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerAttributeValueId", customerAttributeValueId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerAttributeName", customerAttributeName.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsPreSelected", isPreselected));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", "0".ReplaceToParam()));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public void UpdateCustomerAttributeValueMySQL(int customerAttributeValueId, string customerAttributeName, bool? isPreselected)
		{
			string obj = "\r\n                              UPDATE [CustomerAttributeValue]\r\n                                   SET [Name] = @CustomerAttributeName,\r\n                                       [IsPreSelected] = @IsPreSelected\r\n                              WHERE Id = @CustomerAttributeValueId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerAttributeValueId", customerAttributeValueId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerAttributeName", customerAttributeName.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsPreSelected", isPreselected.ReplaceToParam()));
					mySqlConnection.Open();
					mySqlCommand.ExecuteNonQuery();
				}
			}
		}

		public string SelectAddressAttributeValueMySQL(long addressId)
		{
			if (addressId == 0)
			{
				return null;
			}
			string result = null;
			string obj = "Select TOP 1 [CustomAttributes] From [Address] WITH (NOLOCK) Where [Id] = @AddressId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@AddressId", addressId.ReplaceToParam()));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							result = mySqlDataReader[0].ToString();
						}
					}
				}
			}
			return result;
		}

		public int InsertUpdateAddressAttributeMySQL(string attributeName, int? attributeControlTypeId, bool? isRequired)
		{
			int num = 0;
			string obj = "Select Id From [AddressAttribute] WITH (NOLOCK) Where Name = @searchValue";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@searchValue", attributeName.ReplaceToParam()));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							num = mySqlDataReader.GetInt32(0);
							if (num > 0)
							{
								UpdateAddressAttribute(num, attributeName, attributeControlTypeId, isRequired);
							}
						}
					}
				}
			}
			if (num == 0)
			{
				num = InsertAddressAttribute(attributeName, attributeControlTypeId, isRequired);
			}
			return num;
		}

		public int InsertAddressAttributeMySQL(string customerAttributeName, int? attributeControlTypeId, bool? isRequired)
		{
			int result = 0;
			string str = "\r\n                              INSERT INTO [AddressAttribute]\r\n                                   ([Name]\r\n                                   ,[AttributeControlTypeId]\r\n                                   ,[IsRequired]\r\n                                   ,[DisplayOrder])\r\n                             VALUES\r\n                                   (@CustomerAttributeName\r\n                                   ,@AttributeControlTypeId\r\n                                   ,@IsRequired\r\n                                   ,@DisplayOrder)";
			str += "; SELECT SCOPE_IDENTITY() AS insertId;";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(str.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerAttributeName", customerAttributeName.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AttributeControlTypeId", attributeControlTypeId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsRequired", isRequired));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", "0".ReplaceToParam()));
					mySqlConnection.Open();
					result = GlobalClass.StringToInteger(mySqlCommand.ExecuteScalar());
				}
			}
			return result;
		}

		public int UpdateAddressAttributeMySQL(int attId, string customerAttributeName, int? attributeControlTypeId, bool? isRequired)
		{
			int result = 0;
			string obj = "\r\n                              UPDATE [AddressAttribute]\r\n                               SET [Name] = @CustomerAttributeName\r\n                                  ,[IsRequired] = @IsRequired\r\n                                  ,[AttributeControlTypeId] = @AttributeControlTypeId\r\n                                  ,[DisplayOrder] = @DisplayOrder\r\n                               WHERE Id=@CustomerAttributeId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerAttributeName", customerAttributeName.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AttributeControlTypeId", attributeControlTypeId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@IsRequired", isRequired));
					mySqlCommand.Parameters.Add(new MySqlParameter("@DisplayOrder", "0".ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@CustomerAttributeId", attId.ReplaceToParam()));
					mySqlConnection.Open();
					mySqlCommand.ExecuteNonQuery();
				}
			}
			return result;
		}

		public int UpdateAddressCustomAttributeMySQL(long addressId, string attributeValue)
		{
			int result = 0;
			string obj = "\r\n                              UPDATE [Address]\r\n                               SET\r\n                                   [CustomAttributes] = @AttributeValue\r\n                               WHERE Id=@AddressId";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlCommand.Parameters.Add(new MySqlParameter("@AddressId", addressId.ReplaceToParam()));
					mySqlCommand.Parameters.Add(new MySqlParameter("@AttributeValue", attributeValue.ReplaceToParam()));
					mySqlConnection.Open();
					mySqlCommand.ExecuteNonQuery();
				}
			}
			return result;
		}

		public int GetMeasureWeightMySQL(string systemKeyword)
		{
			int result = 0;
			string obj = "Select Id From [MeasureWeight] WITH (NOLOCK) Where [SystemKeyword] = @systemKeyword";
			using (MySqlConnection mySqlConnection = new MySqlConnection(_connString))
			{
				using (MySqlCommand mySqlCommand = new MySqlCommand(obj.SqlQueryToMySql(), mySqlConnection))
				{
					mySqlConnection.Open();
					mySqlCommand.Parameters.Add(new MySqlParameter("@systemKeyword", systemKeyword));
					using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
					{
						if (mySqlDataReader.Read())
						{
							result = mySqlDataReader.GetInt32(0);
						}
					}
				}
			}
			return result;
		}
	}
}
