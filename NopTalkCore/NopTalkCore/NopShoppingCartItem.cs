using System;

namespace NopTalkCore
{
	public class NopShoppingCartItem
	{
		public int Id
		{
			get;
			set;
		}

		public int StoreId
		{
			get;
			set;
		}

		public int ShoppingCartTypeId
		{
			get;
			set;
		}

		public int CustomerId
		{
			get;
			set;
		}

		public int ProductId
		{
			get;
			set;
		}

		public string AttributesXml
		{
			get;
			set;
		}

		public decimal CustomerEnteredPrice
		{
			get;
			set;
		}

		public int Quantity
		{
			get;
			set;
		}

		public DateTime? RentalStartDateUtc
		{
			get;
			set;
		}

		public DateTime? RentalEndDateUtc
		{
			get;
			set;
		}

		public DateTime CreatedOnUtc
		{
			get;
			set;
		}

		public DateTime UpdatedOnUtc
		{
			get;
			set;
		}

		public string SKUID
		{
			get;
			set;
		}

		public bool Updated
		{
			get;
			set;
		}

		public bool Inserted
		{
			get;
			set;
		}

		public NopShoppingCartItem(ShoppingCartType shoppingCartType)
		{
			ShoppingCartTypeId = (int)shoppingCartType;
			Quantity = 0;
			CustomerEnteredPrice = 0m;
			CreatedOnUtc = DateTime.Now;
			UpdatedOnUtc = DateTime.Now;
		}
	}
}
