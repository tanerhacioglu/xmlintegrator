namespace NopTalkCore
{
	public class CategoryItem
	{
		public int CatId
		{
			get;
			set;
		}

		public string Category
		{
			get;
			set;
		}

		public int? ParentCatId
		{
			get;
			set;
		}

		public int CatDbId
		{
			get;
			set;
		}
	}
}
