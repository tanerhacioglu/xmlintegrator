namespace NopTalkCore
{
	public class OptionItem
	{
		public string OptionId
		{
			get;
			set;
		}

		public string OptionValue
		{
			get;
			set;
		}

		public string ImageUrl
		{
			get;
			set;
		}

		public string Title
		{
			get;
			set;
		}

		public int Quantity
		{
			get;
			set;
		}
	}
}
