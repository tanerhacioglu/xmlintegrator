namespace NopTalkCore
{
	public class SkuVal
	{
		public string actSkuBulkCalPrice
		{
			get;
			set;
		}

		public string actSkuCalPrice
		{
			get;
			set;
		}

		public string actSkuDisplayBulkPrice
		{
			get;
			set;
		}

		public string actSkuMultiCurrencyBulkPrice
		{
			get;
			set;
		}

		public string actSkuMultiCurrencyCalPrice
		{
			get;
			set;
		}

		public string actSkuMultiCurrencyDisplayPrice
		{
			get;
			set;
		}

		public int availQuantity
		{
			get;
			set;
		}

		public int bulkOrder
		{
			get;
			set;
		}

		public int inventory
		{
			get;
			set;
		}

		public bool isActivity
		{
			get;
			set;
		}

		public string skuBulkCalPrice
		{
			get;
			set;
		}

		public string skuCalPrice
		{
			get;
			set;
		}

		public string skuDisplayBulkPrice
		{
			get;
			set;
		}

		public string skuMultiCurrencyBulkPrice
		{
			get;
			set;
		}

		public string skuMultiCurrencyCalPrice
		{
			get;
			set;
		}

		public string skuMultiCurrencyDisplayPrice
		{
			get;
			set;
		}
	}
}
