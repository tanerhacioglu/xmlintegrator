namespace NopTalkCore
{
	public enum SourceMappingType
	{
		TierPricing,
		ProductAttributes,
		ProductSpecAttributes,
		AddressCustomAttributtes,
		CustomerCustomAttributtes,
		ProductInfo,
		ProductPictures,
		Customer,
		ShippingAddress,
		CustomerRole,
		CustomerOrders,
		CustomerOthers,
		BillingAddress,
		Product_Prices,
		WishList
	}
}
