using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace NopTalkCore
{
	public class ImageHelper
	{
		public static Image ResizeImage(Image image, int maximumWidth, int maximumHeight, long quality, bool enforceRatio, bool addPadding)
		{
			ImageCodecInfo[] ımageEncoders = ImageCodecInfo.GetImageEncoders();
			EncoderParameters encoderParameters = new EncoderParameters(1);
			encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, quality);
			int width = maximumWidth;
			int height = maximumHeight;
			int num = maximumWidth;
			int num2 = maximumHeight;
			int x = 0;
			int y = 0;
			if (enforceRatio)
			{
				double num3 = (double)maximumWidth / (double)image.Width;
				double num4 = (double)maximumHeight / (double)image.Height;
				double num5 = (num3 < num4) ? num3 : num4;
				num2 = (int)((double)image.Height * num5);
				num = (int)((double)image.Width * num5);
				if (addPadding)
				{
					x = (int)(((double)maximumWidth - (double)image.Width * num5) / 2.0);
					y = (int)(((double)maximumHeight - (double)image.Height * num5) / 2.0);
				}
				else
				{
					width = num;
					height = num2;
				}
			}
			Bitmap bitmap = new Bitmap(width, height);
			Graphics graphics = Graphics.FromImage(bitmap);
			if (enforceRatio && addPadding)
			{
				graphics.Clear(Color.White);
			}
			graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
			graphics.SmoothingMode = SmoothingMode.HighQuality;
			graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
			graphics.CompositingQuality = CompositingQuality.HighQuality;
			graphics.DrawImage(image, x, y, num, num2);
			MemoryStream stream = new MemoryStream();
			bitmap.Save(stream, ımageEncoders[1], encoderParameters);
			graphics.Dispose();
			bitmap.Dispose();
			return Image.FromStream(stream);
		}

		public static ImageFormat ParseImageFormat(string str, Image image)
		{
			ImageFormat result = ImageFormat.Png;
			try
			{
				switch (str.ToLower())
				{
				case "bmp":
					result = ImageFormat.Bmp;
					break;
				case "gif":
					result = ImageFormat.Gif;
					break;
				case "jpg":
				case "jpeg":
					result = ImageFormat.Jpeg;
					break;
				case "ico":
					result = ImageFormat.Icon;
					break;
				case "png":
					result = ImageFormat.Png;
					break;
				case "tif":
					result = ImageFormat.Tiff;
					break;
				default:
				{
					string text = new ImageFormatConverter().ConvertToString(image.RawFormat);
					switch (text.ToLower())
					{
					case "bmp":
						result = ImageFormat.Bmp;
						break;
					case "gif":
						result = ImageFormat.Gif;
						break;
					case "jpg":
					case "jpeg":
						result = ImageFormat.Jpeg;
						break;
					case "ico":
						result = ImageFormat.Icon;
						break;
					case "png":
						result = ImageFormat.Png;
						break;
					case "tif":
						result = ImageFormat.Tiff;
						break;
					}
					break;
				}
				}
				return result;
			}
			catch (Exception)
			{
				return ImageFormat.Png;
			}
		}

		public static byte[] ConvertImageToByteArray(Image imageToConvert, ImageFormat formatOfImage)
		{
			try
			{
				using (MemoryStream memoryStream = new MemoryStream())
				{
					imageToConvert.Save(memoryStream, formatOfImage);
					return memoryStream.ToArray();
				}
			}
			catch (Exception)
			{
				throw;
			}
		}
	}
}
