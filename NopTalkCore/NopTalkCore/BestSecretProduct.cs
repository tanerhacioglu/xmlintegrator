using System.Collections.Generic;

namespace NopTalkCore
{
	public class BestSecretProduct
	{
		public string ProductId
		{
			get;
			set;
		}

		public string ProductSku
		{
			get;
			set;
		}

		public string ProductName
		{
			get;
			set;
		}

		public string Manufacturer
		{
			get;
			set;
		}

		public string ProductShortDescription
		{
			get;
			set;
		}

		public string ProductFullDescription
		{
			get;
			set;
		}

		public decimal ProductOldPrice
		{
			get;
			set;
		}

		public decimal ProductPrice
		{
			get;
			set;
		}

		public decimal ProductCostPrice
		{
			get;
			set;
		}

		public int ProductQuantity
		{
			get;
			set;
		}

		public bool Unavailable
		{
			get;
			set;
		}

		public List<AttributeItem> AttributeItems
		{
			get;
			set;
		}

		public List<ImageItem> ImageItems
		{
			get;
			set;
		}

		public List<PropertyItem> PropertyItems
		{
			get;
			set;
		}

		public List<CategoryItem> CategoriesItems
		{
			get;
			set;
		}

		public BestSecretProduct()
		{
			AttributeItems = new List<AttributeItem>();
			ImageItems = new List<ImageItem>();
			PropertyItems = new List<PropertyItem>();
			CategoriesItems = new List<CategoryItem>();
		}
	}
}
