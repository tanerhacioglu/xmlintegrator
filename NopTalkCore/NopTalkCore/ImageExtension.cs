using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace NopTalkCore
{
	public static class ImageExtension
	{
		public static string GetMimeType(this Image image)
		{
			return image.RawFormat.GetMimeType();
		}

		public static string GetMimeType(this ImageFormat imageFormat)
		{
			ImageCodecInfo[] ımageEncoders = ImageCodecInfo.GetImageEncoders();
			return ımageEncoders.First((ImageCodecInfo codec) => codec.FormatID == imageFormat.Guid).MimeType;
		}

		public static string FileExtensionFromEncoder(this ImageFormat format)
		{
			try
			{
				string text = ImageCodecInfo.GetImageEncoders().First((ImageCodecInfo x) => x.FormatID == format.Guid).FilenameExtension.Split(new char[1]
				{
					';'
				}, StringSplitOptions.RemoveEmptyEntries).First().Trim('*')
					.ToLower();
				return text.Replace("jpg", "jpeg");
			}
			catch (Exception)
			{
				return "." + format.ToString().ToLower();
			}
		}
	}
}
