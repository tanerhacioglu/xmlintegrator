﻿namespace NopTalk
{
	// Token: 0x02000012 RID: 18
	public partial class productsEdit : global::System.Windows.Forms.Form
	{
		// Token: 0x06000083 RID: 131 RVA: 0x000026D1 File Offset: 0x000008D1
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000084 RID: 132 RVA: 0x0000B6C0 File Offset: 0x000098C0
		private void InitializeComponent()
		{
			this.groupBox1 = new global::System.Windows.Forms.GroupBox();
			this.clbColumnsList = new global::System.Windows.Forms.CheckedListBox();
			this.label6 = new global::System.Windows.Forms.Label();
			this.btnFilterClear = new global::System.Windows.Forms.Button();
			this.chPublished = new global::System.Windows.Forms.CheckBox();
			this.label5 = new global::System.Windows.Forms.Label();
			this.clbCategoriesList = new global::System.Windows.Forms.CheckedListBox();
			this.lblCategories = new global::System.Windows.Forms.Label();
			this.btnFilter = new global::System.Windows.Forms.Button();
			this.tbSearch_ProductSku = new global::System.Windows.Forms.TextBox();
			this.label4 = new global::System.Windows.Forms.Label();
			this.tbSearch_ProductName = new global::System.Windows.Forms.TextBox();
			this.label3 = new global::System.Windows.Forms.Label();
			this.ddItemsLimit = new global::System.Windows.Forms.ComboBox();
			this.label2 = new global::System.Windows.Forms.Label();
			this.ddStoresConn = new global::System.Windows.Forms.ComboBox();
			this.label1 = new global::System.Windows.Forms.Label();
			this.dgDataEditor = new global::System.Windows.Forms.DataGridView();
			this.btnClose = new global::System.Windows.Forms.Button();
			this.btnSave = new global::System.Windows.Forms.Button();
			this.statusArea = new global::System.Windows.Forms.StatusStrip();
			this.statusAreaText = new global::System.Windows.Forms.ToolStripStatusLabel();
			this.lblStatistic = new global::System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			((global::System.ComponentModel.ISupportInitialize)this.dgDataEditor).BeginInit();
			this.statusArea.SuspendLayout();
			base.SuspendLayout();
			this.groupBox1.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.groupBox1.Controls.Add(this.clbColumnsList);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.btnFilterClear);
			this.groupBox1.Controls.Add(this.chPublished);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.clbCategoriesList);
			this.groupBox1.Controls.Add(this.lblCategories);
			this.groupBox1.Controls.Add(this.btnFilter);
			this.groupBox1.Controls.Add(this.tbSearch_ProductSku);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.tbSearch_ProductName);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.ddItemsLimit);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.ddStoresConn);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new global::System.Drawing.Point(13, 13);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new global::System.Drawing.Size(1188, 207);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Filter";
			this.clbColumnsList.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.clbColumnsList.CheckOnClick = true;
			this.clbColumnsList.Enabled = false;
			this.clbColumnsList.FormattingEnabled = true;
			this.clbColumnsList.Location = new global::System.Drawing.Point(726, 40);
			this.clbColumnsList.MultiColumn = true;
			this.clbColumnsList.Name = "clbColumnsList";
			this.clbColumnsList.Size = new global::System.Drawing.Size(267, 157);
			this.clbColumnsList.TabIndex = 25;
			this.label6.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Right);
			this.label6.AutoSize = true;
			this.label6.Location = new global::System.Drawing.Point(723, 20);
			this.label6.Name = "label6";
			this.label6.Size = new global::System.Drawing.Size(62, 17);
			this.label6.TabIndex = 24;
			this.label6.Text = "Columns";
			this.btnFilterClear.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnFilterClear.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnFilterClear.Location = new global::System.Drawing.Point(1000, 122);
			this.btnFilterClear.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnFilterClear.Name = "btnFilterClear";
			this.btnFilterClear.Size = new global::System.Drawing.Size(181, 30);
			this.btnFilterClear.TabIndex = 23;
			this.btnFilterClear.Text = "Clear Filter";
			this.btnFilterClear.UseVisualStyleBackColor = true;
			this.btnFilterClear.Click += new global::System.EventHandler(this.btnFilterClear_Click);
			this.chPublished.AutoSize = true;
			this.chPublished.Location = new global::System.Drawing.Point(130, 127);
			this.chPublished.Name = "chPublished";
			this.chPublished.Size = new global::System.Drawing.Size(18, 17);
			this.chPublished.TabIndex = 22;
			this.chPublished.UseVisualStyleBackColor = true;
			this.label5.AutoSize = true;
			this.label5.Location = new global::System.Drawing.Point(6, 127);
			this.label5.Name = "label5";
			this.label5.Size = new global::System.Drawing.Size(70, 17);
			this.label5.TabIndex = 21;
			this.label5.Text = "Published";
			this.clbCategoriesList.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.clbCategoriesList.CheckOnClick = true;
			this.clbCategoriesList.Enabled = false;
			this.clbCategoriesList.FormattingEnabled = true;
			this.clbCategoriesList.Location = new global::System.Drawing.Point(529, 40);
			this.clbCategoriesList.MultiColumn = true;
			this.clbCategoriesList.Name = "clbCategoriesList";
			this.clbCategoriesList.Size = new global::System.Drawing.Size(191, 157);
			this.clbCategoriesList.TabIndex = 20;
			this.lblCategories.AutoSize = true;
			this.lblCategories.Location = new global::System.Drawing.Point(526, 18);
			this.lblCategories.Name = "lblCategories";
			this.lblCategories.Size = new global::System.Drawing.Size(76, 17);
			this.lblCategories.TabIndex = 19;
			this.lblCategories.Text = "Categories";
			this.btnFilter.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnFilter.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnFilter.Location = new global::System.Drawing.Point(1000, 163);
			this.btnFilter.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnFilter.Name = "btnFilter";
			this.btnFilter.Size = new global::System.Drawing.Size(181, 32);
			this.btnFilter.TabIndex = 5;
			this.btnFilter.Text = "Filter";
			this.btnFilter.UseVisualStyleBackColor = true;
			this.btnFilter.Click += new global::System.EventHandler(this.btnFilter_Click);
			this.tbSearch_ProductSku.Location = new global::System.Drawing.Point(130, 92);
			this.tbSearch_ProductSku.Name = "tbSearch_ProductSku";
			this.tbSearch_ProductSku.Size = new global::System.Drawing.Size(376, 22);
			this.tbSearch_ProductSku.TabIndex = 7;
			this.label4.AutoSize = true;
			this.label4.Location = new global::System.Drawing.Point(6, 92);
			this.label4.Name = "label4";
			this.label4.Size = new global::System.Drawing.Size(85, 17);
			this.label4.TabIndex = 6;
			this.label4.Text = "Product Sku";
			this.tbSearch_ProductName.Location = new global::System.Drawing.Point(130, 56);
			this.tbSearch_ProductName.Name = "tbSearch_ProductName";
			this.tbSearch_ProductName.Size = new global::System.Drawing.Size(376, 22);
			this.tbSearch_ProductName.TabIndex = 5;
			this.label3.AutoSize = true;
			this.label3.Location = new global::System.Drawing.Point(6, 56);
			this.label3.Name = "label3";
			this.label3.Size = new global::System.Drawing.Size(98, 17);
			this.label3.TabIndex = 4;
			this.label3.Text = "Product Name";
			this.ddItemsLimit.DropDownStyle = global::System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ddItemsLimit.FormattingEnabled = true;
			this.ddItemsLimit.Items.AddRange(new object[]
			{
				"20",
				"50",
				"100"
			});
			this.ddItemsLimit.Location = new global::System.Drawing.Point(429, 20);
			this.ddItemsLimit.Name = "ddItemsLimit";
			this.ddItemsLimit.Size = new global::System.Drawing.Size(77, 24);
			this.ddItemsLimit.TabIndex = 3;
			this.label2.AutoSize = true;
			this.label2.Location = new global::System.Drawing.Point(340, 23);
			this.label2.Name = "label2";
			this.label2.Size = new global::System.Drawing.Size(74, 17);
			this.label2.TabIndex = 2;
			this.label2.Text = "Items Limit";
			this.ddStoresConn.BackColor = global::System.Drawing.SystemColors.Window;
			this.ddStoresConn.DropDownStyle = global::System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ddStoresConn.FormattingEnabled = true;
			this.ddStoresConn.Location = new global::System.Drawing.Point(130, 20);
			this.ddStoresConn.Name = "ddStoresConn";
			this.ddStoresConn.Size = new global::System.Drawing.Size(204, 24);
			this.ddStoresConn.TabIndex = 1;
			this.ddStoresConn.SelectedIndexChanged += new global::System.EventHandler(this.ddStoresConn_SelectedIndexChanged);
			this.label1.AutoSize = true;
			this.label1.Location = new global::System.Drawing.Point(6, 23);
			this.label1.Name = "label1";
			this.label1.Size = new global::System.Drawing.Size(117, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "Store Connection";
			this.dgDataEditor.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.dgDataEditor.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgDataEditor.Location = new global::System.Drawing.Point(13, 226);
			this.dgDataEditor.Name = "dgDataEditor";
			this.dgDataEditor.RowTemplate.Height = 24;
			this.dgDataEditor.Size = new global::System.Drawing.Size(1188, 256);
			this.dgDataEditor.TabIndex = 1;
			this.btnClose.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnClose.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnClose.Location = new global::System.Drawing.Point(1100, 489);
			this.btnClose.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new global::System.Drawing.Size(100, 28);
			this.btnClose.TabIndex = 4;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new global::System.EventHandler(this.btnClose_Click);
			this.btnSave.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnSave.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnSave.Location = new global::System.Drawing.Point(992, 489);
			this.btnSave.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new global::System.Drawing.Size(100, 28);
			this.btnSave.TabIndex = 3;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new global::System.EventHandler(this.btnSave_Click);
			this.statusArea.AutoSize = false;
			this.statusArea.ImageScalingSize = new global::System.Drawing.Size(20, 20);
			this.statusArea.Items.AddRange(new global::System.Windows.Forms.ToolStripItem[]
			{
				this.statusAreaText
			});
			this.statusArea.Location = new global::System.Drawing.Point(0, 521);
			this.statusArea.Name = "statusArea";
			this.statusArea.Padding = new global::System.Windows.Forms.Padding(1, 0, 19, 0);
			this.statusArea.Size = new global::System.Drawing.Size(1213, 27);
			this.statusArea.TabIndex = 30;
			this.statusArea.Text = "statusArea";
			this.statusAreaText.Name = "statusAreaText";
			this.statusAreaText.Size = new global::System.Drawing.Size(0, 22);
			this.lblStatistic.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left);
			this.lblStatistic.AutoSize = true;
			this.lblStatistic.Location = new global::System.Drawing.Point(22, 499);
			this.lblStatistic.Name = "lblStatistic";
			this.lblStatistic.Size = new global::System.Drawing.Size(108, 17);
			this.lblStatistic.TabIndex = 31;
			this.lblStatistic.Text = "Items Filtered: 0";
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(8f, 16f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(1213, 548);
			base.Controls.Add(this.lblStatistic);
			base.Controls.Add(this.statusArea);
			base.Controls.Add(this.btnClose);
			base.Controls.Add(this.btnSave);
			base.Controls.Add(this.dgDataEditor);
			base.Controls.Add(this.groupBox1);
			base.Name = "productsEdit";
			this.Text = "Products Editor";
			base.Load += new global::System.EventHandler(this.dataEdit_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((global::System.ComponentModel.ISupportInitialize)this.dgDataEditor).EndInit();
			this.statusArea.ResumeLayout(false);
			this.statusArea.PerformLayout();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		// Token: 0x04000064 RID: 100
		private global::System.ComponentModel.IContainer components;

		// Token: 0x04000065 RID: 101
		private global::System.Windows.Forms.GroupBox groupBox1;

		// Token: 0x04000066 RID: 102
		private global::System.Windows.Forms.DataGridView dgDataEditor;

		// Token: 0x04000067 RID: 103
		private global::System.Windows.Forms.Button btnClose;

		// Token: 0x04000068 RID: 104
		private global::System.Windows.Forms.Button btnSave;

		// Token: 0x04000069 RID: 105
		private global::System.Windows.Forms.CheckedListBox clbCategoriesList;

		// Token: 0x0400006A RID: 106
		private global::System.Windows.Forms.Label lblCategories;

		// Token: 0x0400006B RID: 107
		private global::System.Windows.Forms.Button btnFilter;

		// Token: 0x0400006C RID: 108
		private global::System.Windows.Forms.TextBox tbSearch_ProductSku;

		// Token: 0x0400006D RID: 109
		private global::System.Windows.Forms.Label label4;

		// Token: 0x0400006E RID: 110
		private global::System.Windows.Forms.TextBox tbSearch_ProductName;

		// Token: 0x0400006F RID: 111
		private global::System.Windows.Forms.Label label3;

		// Token: 0x04000070 RID: 112
		private global::System.Windows.Forms.ComboBox ddItemsLimit;

		// Token: 0x04000071 RID: 113
		private global::System.Windows.Forms.Label label2;

		// Token: 0x04000072 RID: 114
		private global::System.Windows.Forms.ComboBox ddStoresConn;

		// Token: 0x04000073 RID: 115
		private global::System.Windows.Forms.Label label1;

		// Token: 0x04000074 RID: 116
		private global::System.Windows.Forms.Button btnFilterClear;

		// Token: 0x04000075 RID: 117
		private global::System.Windows.Forms.CheckBox chPublished;

		// Token: 0x04000076 RID: 118
		private global::System.Windows.Forms.Label label5;

		// Token: 0x04000077 RID: 119
		private global::System.Windows.Forms.StatusStrip statusArea;

		// Token: 0x04000078 RID: 120
		private global::System.Windows.Forms.ToolStripStatusLabel statusAreaText;

		// Token: 0x04000079 RID: 121
		private global::System.Windows.Forms.Label lblStatistic;

		// Token: 0x0400007A RID: 122
		private global::System.Windows.Forms.CheckedListBox clbColumnsList;

		// Token: 0x0400007B RID: 123
		private global::System.Windows.Forms.Label label6;
	}
}
