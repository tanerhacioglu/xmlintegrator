﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using NopTalkCore;

namespace NopTalk
{
	// Token: 0x0200000A RID: 10
	internal partial class AboutBox1 : Form
	{
		// Token: 0x0600002C RID: 44 RVA: 0x00005D1C File Offset: 0x00003F1C
		public AboutBox1()
		{
			
			this.components = null;
		
			this.InitializeComponent();
			this.Text = string.Format("About {0}", this.AssemblyTitle);
			if (ImportManager.productsLimits == 50)
			{
				this.Text += " Starter";
			}
			if (ImportManager.productsLimits == 1000)
			{
				this.Text += " Standard";
			}
			if (ImportManager.productsLimits == 5000)
			{
				this.Text += " Professional";
			}
			if (ImportManager.productsLimits == 0)
			{
				this.Text += " Enterprise";
			}
			this.labelProductName.Text = this.AssemblyProduct;
			if (ImportManager.productsLimits == 50)
			{
				this.labelProductName.Text = this.labelProductName.Text + " Starter";
			}
			if (ImportManager.productsLimits == 1000)
			{
				this.labelProductName.Text = this.labelProductName.Text + " Standard";
			}
			if (ImportManager.productsLimits == 5000)
			{
				this.labelProductName.Text = this.labelProductName.Text + " Professional";
			}
			if (ImportManager.productsLimits == 0)
			{
				this.labelProductName.Text = this.labelProductName.Text + " Enterprise";
			}
			this.labelVersion.Text = string.Format("Version {0}", this.AssemblyVersion);
			this.labelCopyright.Text = this.AssemblyCopyright;
			this.labelCompanyName.Text = this.AssemblyCompany;
			this.textBoxDescription.Text = this.AssemblyDescription;
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x0600002D RID: 45 RVA: 0x00005EF0 File Offset: 0x000040F0
		public string AssemblyTitle
		{
			get
			{
				object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
				if (customAttributes.Length != 0)
				{
					AssemblyTitleAttribute assemblyTitleAttribute = (AssemblyTitleAttribute)customAttributes[0];
					if (assemblyTitleAttribute.Title != "")
					{
						return assemblyTitleAttribute.Title;
					}
				}
				return Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x0600002E RID: 46 RVA: 0x00005F50 File Offset: 0x00004150
		public string AssemblyVersion
		{
			get
			{
				return Assembly.GetExecutingAssembly().GetName().Version.ToString();
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x0600002F RID: 47 RVA: 0x00005F74 File Offset: 0x00004174
		public string AssemblyDescription
		{
			get
			{
				object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
				string result;
				if (customAttributes.Length == 0)
				{
					result = "";
				}
				else
				{
					result = ((AssemblyDescriptionAttribute)customAttributes[0]).Description;
				}
				return result;
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000030 RID: 48 RVA: 0x00005FB8 File Offset: 0x000041B8
		public string AssemblyProduct
		{
			get
			{
				object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
				string result;
				if (customAttributes.Length == 0)
				{
					result = "";
				}
				else
				{
					result = ((AssemblyProductAttribute)customAttributes[0]).Product;
				}
				return result;
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000031 RID: 49 RVA: 0x00005FFC File Offset: 0x000041FC
		public string AssemblyCopyright
		{
			get
			{
				object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
				string result;
				if (customAttributes.Length == 0)
				{
					result = "";
				}
				else
				{
					result = ((AssemblyCopyrightAttribute)customAttributes[0]).Copyright;
				}
				return result;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000032 RID: 50 RVA: 0x00006040 File Offset: 0x00004240
		public string AssemblyCompany
		{
			get
			{
				object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
				string result;
				if (customAttributes.Length == 0)
				{
					result = "";
				}
				else
				{
					result = ((AssemblyCompanyAttribute)customAttributes[0]).Company;
				}
				return result;
			}
		}

		// Token: 0x06000033 RID: 51 RVA: 0x000024A0 File Offset: 0x000006A0
		private void AboutBox1_Load(object sender, EventArgs e)
		{
		}

		// Token: 0x06000034 RID: 52 RVA: 0x000024A2 File Offset: 0x000006A2
		private void okButton_Click(object sender, EventArgs e)
		{
			base.Close();
		}
	}
}
