﻿using System;

namespace NopTalk.uc
{
	public enum DefaultValueType
	{
		Text,
		YesNo,
		Combo,
		NumericInteger,
		NumericDecimal
	}
}
