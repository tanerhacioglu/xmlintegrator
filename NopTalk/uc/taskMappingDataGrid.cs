﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using NopTalk.Helpers;
using NopTalkCore;

namespace NopTalk.uc
{
	public class taskMappingDataGrid : UserControl
	{
		private SourceMappingType _sourceMappingType { get; set; }

		private StructureFormat _structureFormat { get; set; }

		public taskMappingDataGrid(SourceMappingType sourceMappingType, StructureFormat structureFormat)
		{
			
			this.components = null;
			this.InitializeComponent();
			this.dgTaskMapping.RowPrePaint += this.dgTaskMapping_RowPrePaint;
			this.dgTaskMapping.CellFormatting += this.dgTaskMapping_CellFormatting;
			this.dgTaskMapping.CellValueChanged += this.dgTaskMapping_CellValueChanged;
			this.dgTaskMapping.DataBindingComplete += this.dgTaskMapping_BindingComplete;
			this.dgTaskMapping.GetType().GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(this.dgTaskMapping, true, null);
			this._sourceMappingType = sourceMappingType;
			this._structureFormat = structureFormat;
		}

		public void TaskMappingListLoad(SourceMapping sourceMapping, TaskMapping taskMapping, bool loadDefaultDataFromSource)
		{
			this.dgTaskMapping.AutoGenerateColumns = false;
			List<TaskItemMapping> data = this.TaskMappingDataInitial(sourceMapping, taskMapping, this._structureFormat, loadDefaultDataFromSource);
			this.dgTaskMapping.DataSource = data.ConvertToDataTable<TaskItemMapping>();
			this.dgTaskMapping.ShowEditingIcon = true;
			this.dgTaskMapping.AllowUserToAddRows = false;
			this.dgTaskMapping.AllowUserToDeleteRows = false;
			this.dgTaskMapping.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
			this.dgTaskMapping.MultiSelect = false;
			this.dgTaskMapping.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			this.dgTaskMapping.EditMode = DataGridViewEditMode.EditOnEnter;
			this.dgTaskMapping.Refresh();
			this.dgTaskMapping.ClearSelection();
			this.TaskMappingColumnsLoad(this._structureFormat);
		}

		public void TaskMappingListSave(TaskMapping taskMapping)
		{
			if (this.dgTaskMapping.DataSource != null)
			{
				List<TaskItemMapping> list = ((DataTable)this.dgTaskMapping.DataSource).ConvertToList<TaskItemMapping>();
				this.SetTaskMappingFieldNames(list);
				if (this._sourceMappingType == SourceMappingType.WishList)
				{
					taskMapping.TaskWishListMapping.Items = list;
				}
				if (this._sourceMappingType == SourceMappingType.ProductInfo)
				{
					taskMapping.TaskProductInfoMapping.Items = list;
				}
				if (this._sourceMappingType == SourceMappingType.Product_Prices)
				{
					taskMapping.TaskProductPriceMapping.Items = list;
				}
				if (this._sourceMappingType == SourceMappingType.Product_Prices)
				{
					taskMapping.TaskProductPriceMapping.Items = list;
				}
				if (this._sourceMappingType == SourceMappingType.ProductPictures)
				{
					taskMapping.TaskProdPicturesMapping.Items = list;
				}
				if (this._sourceMappingType == SourceMappingType.TierPricing)
				{
					taskMapping.TaskTierPricingMapping.TaskTierPricingMappingItems = list;
				}
				if (this._sourceMappingType == SourceMappingType.ProductAttributes)
				{
					taskMapping.TaskProductAttributesMapping.TaskProductAttributesMappingItems = list;
				}
				if (this._sourceMappingType == SourceMappingType.ProductSpecAttributes)
				{
					taskMapping.TaskProductSpecAttributesMapping.TaskProductSpecAttributesMappingItems = list;
				}
				if (this._sourceMappingType == SourceMappingType.AddressCustomAttributtes)
				{
					taskMapping.TaskAddressCustomAttMapping.Items = list;
				}
				if (this._sourceMappingType == SourceMappingType.CustomerCustomAttributtes)
				{
					taskMapping.TaskCustomerCustomAttMapping.Items = list;
				}
				if (this._sourceMappingType == SourceMappingType.Customer)
				{
					taskMapping.TaskCustomerMapping.Items = list;
				}
				if (this._sourceMappingType == SourceMappingType.ShippingAddress)
				{
					taskMapping.TaskCustomerShippingAddressMapping.Items = list;
				}
				if (this._sourceMappingType == SourceMappingType.BillingAddress)
				{
					taskMapping.TaskCustomerBillingAddressMapping.Items = list;
				}
				if (this._sourceMappingType == SourceMappingType.CustomerRole)
				{
					taskMapping.TaskCustomerRoleMapping.Items = list;
				}
				if (this._sourceMappingType == SourceMappingType.CustomerOthers)
				{
					taskMapping.TaskCustomerOthersMapping.Items = list;
				}
			}
		}

		private List<TaskItemMapping> TaskMappingDataInitial(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat, bool loadDefaultDataFromSource)
		{
			List<TaskItemMapping> list = new List<TaskItemMapping>();
			if (this._sourceMappingType == SourceMappingType.Customer)
			{
				using (List<SourceItemMapping>.Enumerator enumerator = sourceMapping.SourceCustomerMapping.Items.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						SourceItemMapping sourceItem = enumerator.Current;
						TaskItemMapping taskItemMapping = new TaskItemMapping
						{
							Id = sourceItem.Id,
							FieldName = sourceItem.FieldName,
							MappingItemType = sourceItem.MappingItemType
						};
						string obj = (structureFormat == StructureFormat.Position) ? sourceItem.FieldMappingForIndex.ToString() : sourceItem.FieldMappingForXml;
						if (GlobalClass.IsMapped(obj, sourceItem.FieldRule))
						{
							taskItemMapping.MappedToSource = true;
							if (loadDefaultDataFromSource)
							{
								taskItemMapping.FieldImport = true;
							}
						}
						if (loadDefaultDataFromSource)
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						if (((taskMapping != null) ? taskMapping.TaskCustomerMapping.Items : null) != null)
						{
							TaskItemMapping taskItemMapping2 = taskMapping.TaskCustomerMapping.Items.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
							if (taskItemMapping2 != null)
							{
								taskItemMapping.FieldRule = (string.IsNullOrEmpty(taskItemMapping2.FieldRule) ? sourceItem.FieldRule : taskItemMapping2.FieldRule);
								taskItemMapping.FieldRule1 = taskItemMapping2.FieldRule1;
								taskItemMapping.FieldRule2 = taskItemMapping2.FieldRule2;
								taskItemMapping.FieldImport = taskItemMapping2.FieldImport;
								taskMapping.TaskCustomerMapping.Items.Remove(taskItemMapping2);
							}
						}
						if (string.IsNullOrEmpty(taskItemMapping.FieldRule))
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						list.Add(taskItemMapping);
					}
				}
			}
			if (this._sourceMappingType == SourceMappingType.CustomerOthers)
			{
				using (List<SourceItemMapping>.Enumerator enumerator2 = sourceMapping.SourceCustomerOthersMapping.Items.GetEnumerator())
				{
					while (enumerator2.MoveNext())
					{
						SourceItemMapping sourceItem = enumerator2.Current;
						TaskItemMapping taskItemMapping = new TaskItemMapping
						{
							Id = sourceItem.Id,
							FieldName = sourceItem.FieldName,
							MappingItemType = sourceItem.MappingItemType
						};
						string obj2 = (structureFormat == StructureFormat.Position) ? sourceItem.FieldMappingForIndex.ToString() : sourceItem.FieldMappingForXml;
						if (GlobalClass.IsMapped(obj2, sourceItem.FieldRule))
						{
							taskItemMapping.MappedToSource = true;
							if (loadDefaultDataFromSource)
							{
								taskItemMapping.FieldImport = true;
							}
						}
						if (loadDefaultDataFromSource)
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						if (((taskMapping != null) ? taskMapping.TaskCustomerOthersMapping.Items : null) != null)
						{
							TaskItemMapping taskItemMapping3 = taskMapping.TaskCustomerOthersMapping.Items.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
							if (taskItemMapping3 != null)
							{
								taskItemMapping.FieldRule = (string.IsNullOrEmpty(taskItemMapping3.FieldRule) ? sourceItem.FieldRule : taskItemMapping3.FieldRule);
								taskItemMapping.FieldRule1 = taskItemMapping3.FieldRule1;
								taskItemMapping.FieldRule2 = taskItemMapping3.FieldRule2;
								taskItemMapping.FieldImport = taskItemMapping3.FieldImport;
								taskMapping.TaskCustomerOthersMapping.Items.Remove(taskItemMapping3);
							}
						}
						if (string.IsNullOrEmpty(taskItemMapping.FieldRule))
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						list.Add(taskItemMapping);
					}
				}
			}
			if (this._sourceMappingType == SourceMappingType.CustomerRole)
			{
				using (List<SourceItemMapping>.Enumerator enumerator3 = sourceMapping.SourceCustomerRoleMapping.Items.GetEnumerator())
				{
					while (enumerator3.MoveNext())
					{
						SourceItemMapping sourceItem = enumerator3.Current;
						TaskItemMapping taskItemMapping = new TaskItemMapping
						{
							Id = sourceItem.Id,
							FieldName = sourceItem.FieldName,
							MappingItemType = sourceItem.MappingItemType
						};
						string obj3 = (structureFormat == StructureFormat.Position) ? sourceItem.FieldMappingForIndex.ToString() : sourceItem.FieldMappingForXml;
						if (GlobalClass.IsMapped(obj3, sourceItem.FieldRule))
						{
							taskItemMapping.MappedToSource = true;
							if (loadDefaultDataFromSource)
							{
								taskItemMapping.FieldImport = true;
							}
						}
						if (loadDefaultDataFromSource)
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						if (((taskMapping != null) ? taskMapping.TaskCustomerRoleMapping.Items : null) != null)
						{
							TaskItemMapping taskItemMapping4 = taskMapping.TaskCustomerRoleMapping.Items.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
							if (taskItemMapping4 != null)
							{
								taskItemMapping.FieldRule = (string.IsNullOrEmpty(taskItemMapping4.FieldRule) ? sourceItem.FieldRule : taskItemMapping4.FieldRule);
								taskItemMapping.FieldRule1 = taskItemMapping4.FieldRule1;
								taskItemMapping.FieldRule2 = taskItemMapping4.FieldRule2;
								taskItemMapping.FieldImport = taskItemMapping4.FieldImport;
								taskMapping.TaskCustomerRoleMapping.Items.Remove(taskItemMapping4);
							}
						}
						if (string.IsNullOrEmpty(taskItemMapping.FieldRule))
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						list.Add(taskItemMapping);
					}
				}
			}
			if (this._sourceMappingType == SourceMappingType.ShippingAddress)
			{
				using (List<SourceItemMapping>.Enumerator enumerator4 = sourceMapping.SourceCustomerShippingAddressMapping.Items.GetEnumerator())
				{
					while (enumerator4.MoveNext())
					{
						SourceItemMapping sourceItem = enumerator4.Current;
						TaskItemMapping taskItemMapping = new TaskItemMapping
						{
							Id = sourceItem.Id,
							FieldName = sourceItem.FieldName,
							MappingItemType = sourceItem.MappingItemType
						};
						string obj4 = (structureFormat == StructureFormat.Position) ? sourceItem.FieldMappingForIndex.ToString() : sourceItem.FieldMappingForXml;
						if (GlobalClass.IsMapped(obj4, sourceItem.FieldRule))
						{
							taskItemMapping.MappedToSource = true;
							if (loadDefaultDataFromSource)
							{
								taskItemMapping.FieldImport = true;
							}
						}
						if (loadDefaultDataFromSource)
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						if (((taskMapping != null) ? taskMapping.TaskCustomerShippingAddressMapping.Items : null) != null)
						{
							TaskItemMapping taskItemMapping5 = taskMapping.TaskCustomerShippingAddressMapping.Items.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
							if (taskItemMapping5 != null)
							{
								taskItemMapping.FieldRule = (string.IsNullOrEmpty(taskItemMapping5.FieldRule) ? sourceItem.FieldRule : taskItemMapping5.FieldRule);
								taskItemMapping.FieldRule1 = taskItemMapping5.FieldRule1;
								taskItemMapping.FieldRule2 = taskItemMapping5.FieldRule2;
								taskItemMapping.FieldImport = taskItemMapping5.FieldImport;
								taskMapping.TaskCustomerShippingAddressMapping.Items.Remove(taskItemMapping5);
							}
						}
						if (string.IsNullOrEmpty(taskItemMapping.FieldRule))
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						list.Add(taskItemMapping);
					}
				}
			}
			if (this._sourceMappingType == SourceMappingType.BillingAddress)
			{
				using (List<SourceItemMapping>.Enumerator enumerator5 = sourceMapping.SourceCustomerBillingAddressMapping.Items.GetEnumerator())
				{
					while (enumerator5.MoveNext())
					{
						SourceItemMapping sourceItem = enumerator5.Current;
						TaskItemMapping taskItemMapping = new TaskItemMapping
						{
							Id = sourceItem.Id,
							FieldName = sourceItem.FieldName,
							MappingItemType = sourceItem.MappingItemType
						};
						string obj5 = (structureFormat == StructureFormat.Position) ? sourceItem.FieldMappingForIndex.ToString() : sourceItem.FieldMappingForXml;
						if (GlobalClass.IsMapped(obj5, sourceItem.FieldRule))
						{
							taskItemMapping.MappedToSource = true;
							if (loadDefaultDataFromSource)
							{
								taskItemMapping.FieldImport = true;
							}
						}
						if (loadDefaultDataFromSource)
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						if (((taskMapping != null) ? taskMapping.TaskCustomerBillingAddressMapping.Items : null) != null)
						{
							TaskItemMapping taskItemMapping6 = taskMapping.TaskCustomerBillingAddressMapping.Items.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
							if (taskItemMapping6 != null)
							{
								taskItemMapping.FieldRule = (string.IsNullOrEmpty(taskItemMapping6.FieldRule) ? sourceItem.FieldRule : taskItemMapping6.FieldRule);
								taskItemMapping.FieldRule1 = taskItemMapping6.FieldRule1;
								taskItemMapping.FieldRule2 = taskItemMapping6.FieldRule2;
								taskItemMapping.FieldImport = taskItemMapping6.FieldImport;
								taskMapping.TaskCustomerBillingAddressMapping.Items.Remove(taskItemMapping6);
							}
						}
						if (string.IsNullOrEmpty(taskItemMapping.FieldRule))
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						list.Add(taskItemMapping);
					}
				}
			}
			if (this._sourceMappingType == SourceMappingType.ProductInfo)
			{
				using (List<SourceItemMapping>.Enumerator enumerator6 = sourceMapping.SourceProdInfoMapping.Items.GetEnumerator())
				{
					while (enumerator6.MoveNext())
					{
						SourceItemMapping sourceItem = enumerator6.Current;
						TaskItemMapping taskItemMapping = new TaskItemMapping
						{
							Id = sourceItem.Id,
							FieldName = sourceItem.FieldName,
							MappingItemType = sourceItem.MappingItemType
						};
						string obj6 = (structureFormat == StructureFormat.Position) ? sourceItem.FieldMappingForIndex.ToString() : sourceItem.FieldMappingForXml;
						if (GlobalClass.IsMapped(obj6, sourceItem.FieldRule))
						{
							taskItemMapping.MappedToSource = true;
							if (loadDefaultDataFromSource)
							{
								taskItemMapping.FieldImport = true;
							}
						}
						if (loadDefaultDataFromSource)
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						if (((taskMapping != null) ? taskMapping.TaskProductInfoMapping.Items : null) != null)
						{
							TaskItemMapping taskItemMapping7 = taskMapping.TaskProductInfoMapping.Items.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
							if (taskItemMapping7 != null)
							{
								taskItemMapping.FieldRule = (string.IsNullOrEmpty(taskItemMapping7.FieldRule) ? sourceItem.FieldRule : taskItemMapping7.FieldRule);
								taskItemMapping.FieldRule1 = taskItemMapping7.FieldRule1;
								taskItemMapping.FieldRule2 = taskItemMapping7.FieldRule2;
								taskItemMapping.FieldImport = taskItemMapping7.FieldImport;
								taskMapping.TaskProductInfoMapping.Items.Remove(taskItemMapping7);
							}
						}
						if (string.IsNullOrEmpty(taskItemMapping.FieldRule))
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						list.Add(taskItemMapping);
					}
				}
			}
			if (this._sourceMappingType == SourceMappingType.ProductPictures)
			{
				using (List<SourceItemMapping>.Enumerator enumerator7 = sourceMapping.SourceProdPicturesMapping.Items.GetEnumerator())
				{
					while (enumerator7.MoveNext())
					{
						SourceItemMapping sourceItem = enumerator7.Current;
						TaskItemMapping taskItemMapping = new TaskItemMapping
						{
							Id = sourceItem.Id,
							FieldName = sourceItem.FieldName,
							MappingItemType = sourceItem.MappingItemType
						};
						string obj7 = (structureFormat == StructureFormat.Position) ? sourceItem.FieldMappingForIndex.ToString() : sourceItem.FieldMappingForXml;
						if (GlobalClass.IsMapped(obj7, sourceItem.FieldRule))
						{
							taskItemMapping.MappedToSource = true;
							if (loadDefaultDataFromSource)
							{
								taskItemMapping.FieldImport = true;
							}
						}
						if (loadDefaultDataFromSource)
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						if (((taskMapping != null) ? taskMapping.TaskProdPicturesMapping.Items : null) != null)
						{
							TaskItemMapping taskItemMapping8 = taskMapping.TaskProdPicturesMapping.Items.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
							if (taskItemMapping8 != null)
							{
								taskItemMapping.FieldRule = (string.IsNullOrEmpty(taskItemMapping8.FieldRule) ? sourceItem.FieldRule : taskItemMapping8.FieldRule);
								taskItemMapping.FieldRule1 = taskItemMapping8.FieldRule1;
								taskItemMapping.FieldRule2 = taskItemMapping8.FieldRule2;
								taskItemMapping.FieldImport = taskItemMapping8.FieldImport;
								taskMapping.TaskProdPicturesMapping.Items.Remove(taskItemMapping8);
							}
						}
						if (string.IsNullOrEmpty(taskItemMapping.FieldRule))
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						list.Add(taskItemMapping);
					}
				}
			}
			if (this._sourceMappingType == SourceMappingType.TierPricing)
			{
				using (List<SourceItemMapping>.Enumerator enumerator8 = sourceMapping.SourceTierPricingMapping.SourceTierPricingMappingItems.GetEnumerator())
				{
					while (enumerator8.MoveNext())
					{
						SourceItemMapping sourceItem = enumerator8.Current;
						TaskItemMapping taskItemMapping = new TaskItemMapping
						{
							Id = sourceItem.Id,
							FieldName = sourceItem.FieldName,
							MappingItemType = sourceItem.MappingItemType
						};
						string obj8 = (structureFormat == StructureFormat.Position) ? sourceItem.FieldMappingForIndex.ToString() : sourceItem.FieldMappingForXml;
						if (GlobalClass.IsMapped(obj8, sourceItem.FieldRule))
						{
							taskItemMapping.MappedToSource = true;
							if (loadDefaultDataFromSource)
							{
								taskItemMapping.FieldImport = true;
							}
						}
						if (loadDefaultDataFromSource)
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						if (((taskMapping != null) ? taskMapping.TaskTierPricingMapping.TaskTierPricingMappingItems : null) != null)
						{
							TaskItemMapping taskItemMapping9 = taskMapping.TaskTierPricingMapping.TaskTierPricingMappingItems.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
							if (taskItemMapping9 != null)
							{
								taskItemMapping.FieldRule = (string.IsNullOrEmpty(taskItemMapping9.FieldRule) ? sourceItem.FieldRule : taskItemMapping9.FieldRule);
								taskItemMapping.FieldRule1 = taskItemMapping9.FieldRule1;
								taskItemMapping.FieldRule2 = taskItemMapping9.FieldRule2;
								taskItemMapping.FieldImport = taskItemMapping9.FieldImport;
								taskMapping.TaskTierPricingMapping.TaskTierPricingMappingItems.Remove(taskItemMapping9);
							}
						}
						if (string.IsNullOrEmpty(taskItemMapping.FieldRule))
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						list.Add(taskItemMapping);
					}
				}
			}
			if (this._sourceMappingType == SourceMappingType.Product_Prices)
			{
				using (List<SourceItemMapping>.Enumerator enumerator9 = sourceMapping.SourceProdPriceMapping.Items.GetEnumerator())
				{
					while (enumerator9.MoveNext())
					{
						SourceItemMapping sourceItem = enumerator9.Current;
						TaskItemMapping taskItemMapping = new TaskItemMapping
						{
							Id = sourceItem.Id,
							FieldName = sourceItem.FieldName,
							MappingItemType = sourceItem.MappingItemType
						};
						string obj9 = (structureFormat == StructureFormat.Position) ? sourceItem.FieldMappingForIndex.ToString() : sourceItem.FieldMappingForXml;
						if (GlobalClass.IsMapped(obj9, sourceItem.FieldRule))
						{
							taskItemMapping.MappedToSource = true;
							if (loadDefaultDataFromSource)
							{
								taskItemMapping.FieldImport = true;
							}
						}
						if (loadDefaultDataFromSource)
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						if (((taskMapping != null) ? taskMapping.TaskProductPriceMapping.Items : null) != null)
						{
							TaskItemMapping taskItemMapping10 = taskMapping.TaskProductPriceMapping.Items.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
							if (taskItemMapping10 != null)
							{
								taskItemMapping.FieldRule = (string.IsNullOrEmpty(taskItemMapping10.FieldRule) ? sourceItem.FieldRule : taskItemMapping10.FieldRule);
								taskItemMapping.FieldRule1 = taskItemMapping10.FieldRule1;
								taskItemMapping.FieldRule2 = taskItemMapping10.FieldRule2;
								taskItemMapping.FieldImport = taskItemMapping10.FieldImport;
								taskMapping.TaskProductPriceMapping.Items.Remove(taskItemMapping10);
							}
						}
						if (string.IsNullOrEmpty(taskItemMapping.FieldRule))
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						list.Add(taskItemMapping);
					}
				}
			}
			if (this._sourceMappingType == SourceMappingType.ProductAttributes)
			{
				IOrderedEnumerable<SourceItemMapping> orderedEnumerable = (from o in sourceMapping.SourceProdAttMapping.SourceProdAttMappingItems
				orderby o.Id
				select o).ThenBy((SourceItemMapping t) => t.Order);
				using (IEnumerator<SourceItemMapping> enumerator10 = orderedEnumerable.GetEnumerator())
				{
					while (enumerator10.MoveNext())
					{
						SourceItemMapping sourceItem = enumerator10.Current;
						TaskItemMapping taskItemMapping = new TaskItemMapping
						{
							Id = sourceItem.Id,
							FieldName = sourceItem.FieldName,
							MappingItemType = sourceItem.MappingItemType,
							FieldRule5 = 1,
							FieldRule3 = true,
							FieldRule4 = false
						};
						string obj10 = (structureFormat == StructureFormat.Position) ? sourceItem.FieldMappingForIndex.ToString() : sourceItem.FieldMappingForXml;
						if (GlobalClass.IsMapped(obj10, sourceItem.FieldRule))
						{
							taskItemMapping.MappedToSource = true;
							if (loadDefaultDataFromSource)
							{
								taskItemMapping.FieldImport = true;
							}
						}
						if (loadDefaultDataFromSource)
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						if (((taskMapping != null) ? taskMapping.TaskProductAttributesMapping.TaskProductAttributesMappingItems : null) != null)
						{
							TaskItemMapping taskItemMapping11 = taskMapping.TaskProductAttributesMapping.TaskProductAttributesMappingItems.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
							if (taskItemMapping11 != null)
							{
								taskItemMapping.FieldRule = (string.IsNullOrEmpty(taskItemMapping11.FieldRule) ? sourceItem.FieldRule : taskItemMapping11.FieldRule);
								taskItemMapping.FieldRule3 = taskItemMapping11.FieldRule3;
								taskItemMapping.FieldRule4 = taskItemMapping11.FieldRule4;
								taskItemMapping.FieldRule5 = ((taskItemMapping11.FieldRule5 > 0) ? taskItemMapping11.FieldRule5 : 1);
								taskItemMapping.FieldImport = taskItemMapping11.FieldImport;
								taskMapping.TaskProductAttributesMapping.TaskProductAttributesMappingItems.Remove(taskItemMapping11);
							}
						}
						if (string.IsNullOrEmpty(taskItemMapping.FieldRule))
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						list.Add(taskItemMapping);
					}
				}
			}
			if (this._sourceMappingType == SourceMappingType.ProductSpecAttributes)
			{
				using (List<SourceItemMapping>.Enumerator enumerator11 = sourceMapping.SourceProdSpecAttMapping.SourceProdSpecAttMappingItems.GetEnumerator())
				{
					while (enumerator11.MoveNext())
					{
						SourceItemMapping sourceItem = enumerator11.Current;
						TaskItemMapping taskItemMapping = new TaskItemMapping
						{
							Id = sourceItem.Id,
							FieldName = sourceItem.FieldName,
							MappingItemType = sourceItem.MappingItemType,
							FieldRule5 = 0,
							FieldRule3 = true,
							FieldRule4 = true
						};
						string obj11 = (structureFormat == StructureFormat.Position) ? sourceItem.FieldMappingForIndex.ToString() : sourceItem.FieldMappingForXml;
						if (GlobalClass.IsMapped(obj11, sourceItem.FieldRule))
						{
							taskItemMapping.MappedToSource = true;
							if (loadDefaultDataFromSource)
							{
								taskItemMapping.FieldImport = true;
							}
						}
						if (loadDefaultDataFromSource)
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						if (((taskMapping != null) ? taskMapping.TaskProductSpecAttributesMapping.TaskProductSpecAttributesMappingItems : null) != null)
						{
							TaskItemMapping taskItemMapping12 = taskMapping.TaskProductSpecAttributesMapping.TaskProductSpecAttributesMappingItems.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
							if (taskItemMapping12 != null)
							{
								taskItemMapping.FieldRule = (string.IsNullOrEmpty(taskItemMapping12.FieldRule) ? sourceItem.FieldRule : taskItemMapping12.FieldRule);
								taskItemMapping.FieldRule3 = taskItemMapping12.FieldRule3;
								taskItemMapping.FieldRule4 = taskItemMapping12.FieldRule4;
								taskItemMapping.FieldRule5 = ((taskItemMapping12.FieldRule5 >= 0) ? taskItemMapping12.FieldRule5 : 0);
								taskItemMapping.FieldImport = taskItemMapping12.FieldImport;
								taskMapping.TaskProductSpecAttributesMapping.TaskProductSpecAttributesMappingItems.Remove(taskItemMapping12);
							}
						}
						if (string.IsNullOrEmpty(taskItemMapping.FieldRule))
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						list.Add(taskItemMapping);
					}
				}
			}
			if (this._sourceMappingType == SourceMappingType.AddressCustomAttributtes)
			{
				List<SourceItemMapping> items = sourceMapping.SourceAddressCustomAttMapping.Items;
				List<TaskItemMapping> list2 = (taskMapping != null) ? taskMapping.TaskAddressCustomAttMapping.Items : null;
				using (List<SourceItemMapping>.Enumerator enumerator12 = items.GetEnumerator())
				{
					while (enumerator12.MoveNext())
					{
						SourceItemMapping sourceItem = enumerator12.Current;
						TaskItemMapping taskItemMapping = new TaskItemMapping
						{
							Id = sourceItem.Id,
							FieldName = sourceItem.FieldName,
							MappingItemType = sourceItem.MappingItemType,
							FieldRule5 = 4,
							FieldRule3 = true,
							FieldRule4 = true
						};
						string obj12 = (structureFormat == StructureFormat.Position) ? sourceItem.FieldMappingForIndex.ToString() : sourceItem.FieldMappingForXml;
						if (GlobalClass.IsMapped(obj12, sourceItem.FieldRule))
						{
							taskItemMapping.MappedToSource = true;
							if (loadDefaultDataFromSource)
							{
								taskItemMapping.FieldImport = true;
							}
						}
						if (loadDefaultDataFromSource)
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						if (list2 != null)
						{
							TaskItemMapping taskItemMapping13 = list2.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
							if (taskItemMapping13 != null)
							{
								taskItemMapping.FieldRule3 = taskItemMapping13.FieldRule3;
								taskItemMapping.FieldRule4 = taskItemMapping13.FieldRule4;
								taskItemMapping.FieldRule5 = ((taskItemMapping13.FieldRule5 >= 0) ? taskItemMapping13.FieldRule5 : 4);
								taskItemMapping.FieldRule = (string.IsNullOrEmpty(taskItemMapping13.FieldRule) ? sourceItem.FieldRule : taskItemMapping13.FieldRule);
								taskItemMapping.FieldImport = taskItemMapping13.FieldImport;
								list2.Remove(taskItemMapping13);
							}
						}
						if (string.IsNullOrEmpty(taskItemMapping.FieldRule))
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						list.Add(taskItemMapping);
					}
				}
			}
			if (this._sourceMappingType == SourceMappingType.CustomerCustomAttributtes)
			{
				List<SourceItemMapping> items2 = sourceMapping.SourceCustomerCustomAttMapping.Items;
				List<TaskItemMapping> list3 = (taskMapping != null) ? taskMapping.TaskCustomerCustomAttMapping.Items : null;
				using (List<SourceItemMapping>.Enumerator enumerator13 = items2.GetEnumerator())
				{
					while (enumerator13.MoveNext())
					{
						SourceItemMapping sourceItem = enumerator13.Current;
						TaskItemMapping taskItemMapping = new TaskItemMapping
						{
							Id = sourceItem.Id,
							FieldName = sourceItem.FieldName,
							MappingItemType = sourceItem.MappingItemType,
							FieldRule5 = 4,
							FieldRule3 = true,
							FieldRule4 = true
						};
						string obj13 = (structureFormat == StructureFormat.Position) ? sourceItem.FieldMappingForIndex.ToString() : sourceItem.FieldMappingForXml;
						if (GlobalClass.IsMapped(obj13, sourceItem.FieldRule))
						{
							taskItemMapping.MappedToSource = true;
							if (loadDefaultDataFromSource)
							{
								taskItemMapping.FieldImport = true;
							}
						}
						if (loadDefaultDataFromSource)
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						if (list3 != null)
						{
							TaskItemMapping taskItemMapping14 = list3.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
							if (taskItemMapping14 != null)
							{
								taskItemMapping.FieldRule3 = taskItemMapping14.FieldRule3;
								taskItemMapping.FieldRule4 = taskItemMapping14.FieldRule4;
								taskItemMapping.FieldRule5 = ((taskItemMapping14.FieldRule5 >= 0) ? taskItemMapping14.FieldRule5 : 4);
								taskItemMapping.FieldRule = (string.IsNullOrEmpty(taskItemMapping14.FieldRule) ? sourceItem.FieldRule : taskItemMapping14.FieldRule);
								taskItemMapping.FieldImport = taskItemMapping14.FieldImport;
								list3.Remove(taskItemMapping14);
							}
						}
						if (string.IsNullOrEmpty(taskItemMapping.FieldRule))
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						list.Add(taskItemMapping);
					}
				}
			}
			if (this._sourceMappingType == SourceMappingType.WishList)
			{
				List<SourceItemMapping> items3 = sourceMapping.SourceWishListMapping.Items;
				List<TaskItemMapping> list4 = (taskMapping != null) ? taskMapping.TaskWishListMapping.Items : null;
				using (List<SourceItemMapping>.Enumerator enumerator14 = items3.GetEnumerator())
				{
					while (enumerator14.MoveNext())
					{
						SourceItemMapping sourceItem = enumerator14.Current;
						TaskItemMapping taskItemMapping = new TaskItemMapping
						{
							Id = sourceItem.Id,
							FieldName = sourceItem.FieldName,
							MappingItemType = sourceItem.MappingItemType
						};
						string obj14 = (structureFormat == StructureFormat.Position) ? sourceItem.FieldMappingForIndex.ToString() : sourceItem.FieldMappingForXml;
						if (GlobalClass.IsMapped(obj14, sourceItem.FieldRule))
						{
							taskItemMapping.MappedToSource = true;
							if (loadDefaultDataFromSource)
							{
								taskItemMapping.FieldImport = true;
							}
						}
						if (loadDefaultDataFromSource)
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						if (list4 != null)
						{
							TaskItemMapping taskItemMapping15 = list4.FirstOrDefault((TaskItemMapping x) => x.Id == sourceItem.Id && x.MappingItemType == sourceItem.MappingItemType);
							if (taskItemMapping15 != null)
							{
								taskItemMapping.FieldRule = (string.IsNullOrEmpty(taskItemMapping15.FieldRule) ? sourceItem.FieldRule : taskItemMapping15.FieldRule);
								taskItemMapping.FieldRule1 = taskItemMapping15.FieldRule1;
								taskItemMapping.FieldRule2 = taskItemMapping15.FieldRule2;
								taskItemMapping.FieldImport = taskItemMapping15.FieldImport;
								list4.Remove(taskItemMapping15);
							}
						}
						if (string.IsNullOrEmpty(taskItemMapping.FieldRule))
						{
							taskItemMapping.FieldRule = sourceItem.FieldRule;
						}
						list.Add(taskItemMapping);
					}
				}
			}
			this.SetTaskMappingFieldNames(list);
			return (from x in list
			where x.MappingItemType > -1
			select x).ToList<TaskItemMapping>();
		}

		private void TaskMappingColumnsLoad(StructureFormat structureFormat)
		{
			this.dgTaskMapping.Columns["Id"].Visible = false;
			this.dgTaskMapping.Columns["MappingItemType"].Visible = false;
			this.dgTaskMapping.Columns["FieldIndex"].Visible = false;
			this.dgTaskMapping.Columns["FieldRule1"].Visible = false;
			this.dgTaskMapping.Columns["FieldRule2"].Visible = false;
			this.dgTaskMapping.Columns["FieldRule3"].Visible = false;
			this.dgTaskMapping.Columns["FieldRule4"].Visible = false;
			this.dgTaskMapping.Columns["FieldRule5"].Visible = false;
			this.dgTaskMapping.Columns["MappedToSource"].Width = 80;
			this.dgTaskMapping.Columns["Import"].Width = 80;
			this.dgTaskMapping.Columns["FieldName"].Width = 250;
			this.dgTaskMapping.Columns["FieldRule"].Width = 150;
			this.dgTaskMapping.Columns["FieldRule1"].Width = 150;
			this.dgTaskMapping.Columns["FieldRule2"].Width = 150;
			this.dgTaskMapping.Columns["FieldRule3"].Width = 150;
			this.dgTaskMapping.Columns["FieldRule4"].Width = 150;
			this.dgTaskMapping.Columns["FieldRule5"].Width = 150;
			this.dgTaskMapping.Columns["FieldRule"].HeaderText = "Default Value /+Add Value";
			if (this._sourceMappingType == SourceMappingType.Product_Prices || this._sourceMappingType == SourceMappingType.TierPricing)
			{
				this.dgTaskMapping.Columns["FieldRule1"].Visible = true;
				this.dgTaskMapping.Columns["FieldRule2"].Visible = true;
				this.dgTaskMapping.Columns["FieldRule1"].HeaderText = "Add Value To Price";
				this.dgTaskMapping.Columns["FieldRule2"].HeaderText = "Add Percent To Price";
			}
			if (this._sourceMappingType != SourceMappingType.ProductAttributes)
			{
			}
			if (this._sourceMappingType == SourceMappingType.ProductSpecAttributes)
			{
				this.LoadSpecAttributeControlType();
				this.dgTaskMapping.Columns["FieldRule3"].Visible = true;
				this.dgTaskMapping.Columns["FieldRule4"].Visible = true;
				this.dgTaskMapping.Columns["FieldRule5"].Visible = true;
				this.dgTaskMapping.Columns["FieldRule3"].HeaderText = "Show On Product";
				this.dgTaskMapping.Columns["FieldRule4"].HeaderText = "Allow Filtering";
				this.dgTaskMapping.Columns["FieldRule5"].HeaderText = "Type";
			}
			if (this._sourceMappingType == SourceMappingType.CustomerCustomAttributtes)
			{
				this.LoadAttributeControlType();
				this.dgTaskMapping.Columns["FieldRule3"].Visible = true;
				this.dgTaskMapping.Columns["FieldRule4"].Visible = true;
				this.dgTaskMapping.Columns["FieldRule5"].Visible = true;
				this.dgTaskMapping.Columns["FieldRule3"].HeaderText = "Is Required";
				this.dgTaskMapping.Columns["FieldRule4"].HeaderText = "Is PreSelected";
				this.dgTaskMapping.Columns["FieldRule5"].HeaderText = "Type";
			}
			if (this._sourceMappingType == SourceMappingType.AddressCustomAttributtes)
			{
				this.LoadAttributeControlType();
				this.dgTaskMapping.Columns["FieldRule3"].Visible = true;
				this.dgTaskMapping.Columns["FieldRule4"].Visible = true;
				this.dgTaskMapping.Columns["FieldRule5"].Visible = true;
				this.dgTaskMapping.Columns["FieldRule3"].HeaderText = "Is Required";
				this.dgTaskMapping.Columns["FieldRule4"].HeaderText = "Is PreSelected";
				this.dgTaskMapping.Columns["FieldRule5"].HeaderText = "Type";
			}
			foreach (object obj in this.dgTaskMapping.Columns)
			{
				DataGridViewColumn dataGridViewColumn = (DataGridViewColumn)obj;
				dataGridViewColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
			}
		}

		private void SetTaskMappingFieldNames(List<TaskItemMapping> listResult)
		{
			int index = 1;
			foreach (Guid guid in listResult.Select<TaskItemMapping, Guid>((Func<TaskItemMapping, Guid>)(x => x.Id)).ToList<Guid>().Distinct<Guid>())
			{
				Guid id = guid;
				if ((listResult.FirstOrDefault<TaskItemMapping>((Func<TaskItemMapping, bool>)(x => x.Id == id)).MappingItemType <= -1 || this._sourceMappingType == SourceMappingType.ProductInfo || (this._sourceMappingType == SourceMappingType.Product_Prices || this._sourceMappingType == SourceMappingType.Customer) || (this._sourceMappingType == SourceMappingType.ShippingAddress || this._sourceMappingType == SourceMappingType.BillingAddress) ? 0 : (this._sourceMappingType != SourceMappingType.CustomerOthers ? 1 : 0)) != 0)
				{
					listResult.Where<TaskItemMapping>((Func<TaskItemMapping, bool>)(x => x.Id == id)).ToList<TaskItemMapping>().ForEach((Action<TaskItemMapping>)(x => x.FieldNameForDisplay = x.FieldName + "(" + index.ToString() + ")"));
					listResult.Where<TaskItemMapping>((Func<TaskItemMapping, bool>)(x => x.Id == id)).ToList<TaskItemMapping>().ForEach((Action<TaskItemMapping>)(x => x.FieldIndex = index));
					index++;
				}
				else
				{
					listResult.Where<TaskItemMapping>((Func<TaskItemMapping, bool>)(x => x.Id == id)).ToList<TaskItemMapping>().ForEach((Action<TaskItemMapping>)(x => x.FieldNameForDisplay = x.FieldName));
					listResult.Where<TaskItemMapping>((Func<TaskItemMapping, bool>)(x => x.Id == id)).ToList<TaskItemMapping>().ForEach((Action<TaskItemMapping>)(x => x.FieldIndex = 0));
				}
			}
		}

		private void SetTaskItemMappingFieldRule(List<TaskItemMapping> listResult)
		{
			foreach (object obj in ((IEnumerable)this.dgTaskMapping.Rows))
			{
				DataGridViewRow dataGridViewRow = (DataGridViewRow)obj;
			}
		}

		private void dgTaskMapping_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			string.Format("Finished Editing Cell at ({0}, {1})", e.ColumnIndex, e.RowIndex);
			if (e.ColumnIndex == 3 && e.RowIndex > -1)
			{
				this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRuleTemp"].Value = this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule"].Value;
			}
		}

		private void dgTaskMapping_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
		{
			int num = GlobalClass.StringToInteger(this.dgTaskMapping["FieldIndex", e.RowIndex].Value.ToString(), 0);
			if (num > 0 && num % 2 == 0)
			{
				this.dgTaskMapping.Rows[e.RowIndex].DefaultCellStyle.BackColor = ColorTranslator.FromHtml("#DCDCDC");
			}
			MappingItemType mappingItemType = (MappingItemType)GlobalClass.StringToInteger(this.dgTaskMapping["MappingItemType", e.RowIndex].Value.ToString(), 0);
			if (mappingItemType == NopTalkCore.MappingItemType.TierPricing_Price || mappingItemType == NopTalkCore.MappingItemType.ProductPrice_ProductPrice)
			{
				this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule1"].ReadOnly = false;
				this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule2"].ReadOnly = false;
			}
			else
			{
				this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule1"].ReadOnly = true;
				this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule2"].ReadOnly = true;
				this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule1"].Style.Padding = new Padding(this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule1"].OwningColumn.Width, 0, 0, 0);
				this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule2"].Style.Padding = new Padding(this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule2"].OwningColumn.Width, 0, 0, 0);
			}
			if (mappingItemType == NopTalkCore.MappingItemType.ProdAttCombination_Sku || mappingItemType == NopTalkCore.MappingItemType.ProdAttValue_Value || mappingItemType == NopTalkCore.MappingItemType.ProdAttValue_Quantity || mappingItemType == NopTalkCore.MappingItemType.ProdAttValue_Price || mappingItemType == NopTalkCore.MappingItemType.ProdSpecAtt_Value || mappingItemType == NopTalkCore.MappingItemType.ProdAttValue_AssociatedPicture || mappingItemType == NopTalkCore.MappingItemType.ProdAttValue_SquarePicture || mappingItemType == NopTalkCore.MappingItemType.ProdAttValue_Color)
			{
				this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule3"].ReadOnly = true;
				this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule4"].ReadOnly = true;
				this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule5"].ReadOnly = true;
				this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule3"].Style.Padding = new Padding(this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule3"].OwningColumn.Width, 0, 0, 0);
				this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule4"].Style.Padding = new Padding(this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule4"].OwningColumn.Width, 0, 0, 0);
				this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule5"].Style.Padding = new Padding(this.dgTaskMapping.Rows[e.RowIndex].Cells["FieldRule5"].OwningColumn.Width, 0, 0, 0);
			}
		}

		private void dgTaskMapping_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
		}

		private void dgTaskMapping_BindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			DataGridView dataGridView = sender as DataGridView;
			if (dataGridView != null)
			{
				dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
				dataGridView.Columns[dataGridView.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			}
			foreach (object obj in (dataGridView.Rows))
			{
				DataGridViewRow dataGridViewRow = (DataGridViewRow)obj;				
				  string setValue = (string)dataGridViewRow.Cells["FieldRuleTemp"].Value;
				DataGridViewCell dataGridViewCell = this.dgTaskMapping["MappingItemType", dataGridViewRow.Index];
				object value;
				if (dataGridViewCell == null)
				{
					value = null;
				}
				else
				{
					object value2 = dataGridViewCell.Value;
					value = ((value2 != null) ? value2.ToString() : null);
				}
				MappingItemType mappingItemType = (MappingItemType)GlobalClass.StringToInteger(value, 0);
				if (mappingItemType == NopTalkCore.MappingItemType.ProdAttInfo_ControlType)
				{
					var list = (from AttributeControlType mode in Enum.GetValues(typeof(AttributeControlType))
					select new
					{
						Value = (int)mode,
						Title = mode.ToString()
					}).ToList();
					if (!list.Exists(w => w.Value == GlobalClass.StringToInteger(setValue, 0)) && !list.Exists(w => w.Title.ToLower() == GlobalClass.ObjectToString(setValue).ToLower()))
					{
						setValue = "";
					}
					list.Insert(0, new
					{
						Value = -1,
						Title = ""
					});
					this.dgTaskMapping.Rows[dataGridViewRow.Index].Cells["FieldRule"] = this.CreateDefaultValueCell(DefaultValueType.Combo, list, setValue);
				}
				else if (mappingItemType == NopTalkCore.MappingItemType.ProdAttCombination_AllowOutOfStock || mappingItemType == NopTalkCore.MappingItemType.ProdAttInfo_IsRequired || mappingItemType == NopTalkCore.MappingItemType.ProdAttValue_PriceUsePercent || mappingItemType == NopTalkCore.MappingItemType.ProdAttValue_IsPreselected || mappingItemType == NopTalkCore.MappingItemType.ProductPrice_BasepriceEnabled || mappingItemType == NopTalkCore.MappingItemType.ProductPrice_TaxExempt || mappingItemType == NopTalkCore.MappingItemType.ProductStock_AllowBackInStockSubscriptions || mappingItemType == NopTalkCore.MappingItemType.ProductStock_DisableBuyButton)
				{
					this.dgTaskMapping.Rows[dataGridViewRow.Index].Cells["FieldRule"] = this.CreateDefaultValueCell(DefaultValueType.YesNo, GlobalClass.ObjectToBool(setValue), null);
				}
				else
				{
					this.dgTaskMapping.Rows[dataGridViewRow.Index].Cells["FieldRule"] = this.CreateDefaultValueCell(DefaultValueType.Text, setValue, null);
				}
			}
		}

		private void LoadAttributeControlType()
		{
			var source = (from AttributeControlType mode in Enum.GetValues(typeof(AttributeControlType))
			select new
			{
				Value = (int)mode,
				Title = mode
			}).ToList();
			this.FieldRule5.ValueMember = "Value";
			this.FieldRule5.DisplayMember = "Title";
			this.FieldRule5.DataSource = source.ToList();
		}

		private void LoadSpecAttributeControlType()
		{
			var source = (from SpecAttributeType mode in Enum.GetValues(typeof(SpecAttributeType))
			select new
			{
				Value = (int)mode,
				Title = mode
			}).ToList();
			this.FieldRule5.ValueMember = "Value";
			this.FieldRule5.DisplayMember = "Title";
			this.FieldRule5.DataSource = source.ToList();
		}

		private void BtnAddTaskMapping_Click(object sender, EventArgs e)
		{
			List<TaskItemMapping> list = (this.dgTaskMapping.DataSource != null) ? ((DataTable)this.dgTaskMapping.DataSource).ConvertToList<TaskItemMapping>() : null;
			list.AddRange(TaskMappingHelper.GetTierPricingNewItem());
			this.SetTaskMappingFieldNames(list);
			this.dgTaskMapping.DataSource = list.ToList<TaskItemMapping>().ConvertToDataTable<TaskItemMapping>();
		}

		private void BtnDeleteTaskMapping_Click(object sender, EventArgs e)
		{
			try
			{
				List<TaskItemMapping> list = (this.dgTaskMapping.DataSource != null) ? ((DataTable)this.dgTaskMapping.DataSource).ConvertToList<TaskItemMapping>() : null;
				if (this.dgTaskMapping.SelectedRows.Count > 0)
				{
					if (GlobalClass.ObjectToBool(this.dgTaskMapping.SelectedRows[0].Cells["MappedToSource"].Value))
					{
						MessageBox.Show("The record mapped to the source file cannot be deleted.");
					}
					else
					{
						Guid id = Guid.Parse(this.dgTaskMapping.SelectedRows[0].Cells["Id"].Value.ToString());
						DialogResult dialogResult = MessageBox.Show("Are sure you want to delete this record?", "Delete record", MessageBoxButtons.YesNo);
						if (dialogResult == DialogResult.Yes)
						{
							list.RemoveAll((TaskItemMapping x) => x.Id == id);
							this.SetTaskMappingFieldNames(list);
							this.dgTaskMapping.DataSource = list.ToList<TaskItemMapping>().ConvertToDataTable<TaskItemMapping>();
						}
					}
				}
				else
				{
					MessageBox.Show("Please select the record.");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private DataGridViewCell CreateDefaultValueCell(DefaultValueType dataType, object dataValue, object dataValue2 = null)
		{
			DataGridViewCell result;
			switch (dataType)
			{
			case DefaultValueType.YesNo:
				result = new DataGridViewCheckBoxCell
				{
					ValueType = typeof(bool),
					Value = dataValue
				};
				break;
			case DefaultValueType.Combo:
				result = new DataGridViewComboBoxCell
				{
					ValueMember = "Value",
					DisplayMember = "Title",
					DataSource = dataValue,
					Value = GlobalClass.StringToInteger(dataValue2, -1)
				};
				break;
			case DefaultValueType.NumericInteger:
				result = new DataGridViewNumericUpDownCell
				{
					ValueType = typeof(int),
					Increment = 1m,
					ThousandsSeparator = false,
					Value = GlobalClass.StringToInteger(dataValue, 0)
				};
				break;
			case DefaultValueType.NumericDecimal:
				result = new DataGridViewNumericUpDownCell
				{
					Increment = 0.1m,
					ThousandsSeparator = true,
					Value = GlobalClass.StringToDecimal(dataValue, 0)
				};
				break;
			default:
				result = new DataGridViewTextBoxCell
				{
					ValueType = typeof(string),
					Value = dataValue
				};
				break;
			}
			return result;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.dgTaskMapping = new DataGridView();
			this.dataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn2 = new DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn3 = new DataGridViewTextBoxColumn();
			this.dataGridViewNumericUpDownColumn1 = new DataGridViewNumericUpDownColumn();
			this.dataGridViewNumericUpDownColumn2 = new DataGridViewNumericUpDownColumn();
			this.dataGridViewTextBoxColumn4 = new DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn5 = new DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn6 = new DataGridViewTextBoxColumn();
			this.Id = new DataGridViewTextBoxColumn();
			this.FieldName = new DataGridViewTextBoxColumn();
			this.MappedToSource = new DataGridViewCheckBoxColumn();
			this.FieldRule = new DataGridViewTextBoxColumn();
			this.FieldRule1 = new DataGridViewNumericUpDownColumn();
			this.FieldRule2 = new DataGridViewNumericUpDownColumn();
			this.FieldRule3 = new DataGridViewCheckBoxColumn();
			this.FieldRule4 = new DataGridViewCheckBoxColumn();
			this.FieldRule5 = new DataGridViewComboBoxColumn();
			this.Import = new DataGridViewCheckBoxColumn();
			this.MappingItemType = new DataGridViewTextBoxColumn();
			this.FieldIndex = new DataGridViewTextBoxColumn();
			this.FieldRuleTemp = new DataGridViewTextBoxColumn();
			this.Empty = new DataGridViewTextBoxColumn();
			((ISupportInitialize)this.dgTaskMapping).BeginInit();
			base.SuspendLayout();
			this.dgTaskMapping.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.dgTaskMapping.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgTaskMapping.Columns.AddRange(new DataGridViewColumn[]
			{
				this.Id,
				this.FieldName,
				this.MappedToSource,
				this.FieldRule,
				this.FieldRule1,
				this.FieldRule2,
				this.FieldRule3,
				this.FieldRule4,
				this.FieldRule5,
				this.Import,
				this.MappingItemType,
				this.FieldIndex,
				this.FieldRuleTemp,
				this.Empty
			});
			this.dgTaskMapping.Location = new Point(0, 4);
			this.dgTaskMapping.Margin = new Padding(4);
			this.dgTaskMapping.MultiSelect = false;
			this.dgTaskMapping.Name = "dgTaskMapping";
			this.dgTaskMapping.RowHeadersWidth = 51;
			this.dgTaskMapping.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			this.dgTaskMapping.Size = new Size(1249, 451);
			this.dgTaskMapping.TabIndex = 2;
			this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
			this.dataGridViewTextBoxColumn1.HeaderText = "Id";
			this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
			this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
			this.dataGridViewTextBoxColumn1.ReadOnly = true;
			this.dataGridViewTextBoxColumn1.Width = 125;
			this.dataGridViewTextBoxColumn2.DataPropertyName = "FieldNameForDisplay";
			this.dataGridViewTextBoxColumn2.HeaderText = "FieldName";
			this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
			this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
			this.dataGridViewTextBoxColumn2.ReadOnly = true;
			this.dataGridViewTextBoxColumn2.Width = 125;
			this.dataGridViewTextBoxColumn3.HeaderText = "Field Rule";
			this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			this.dataGridViewTextBoxColumn3.Width = 125;
			this.dataGridViewNumericUpDownColumn1.DataPropertyName = "FieldRule2";
			this.dataGridViewNumericUpDownColumn1.DecimalPlaces = 2;
			this.dataGridViewNumericUpDownColumn1.HeaderText = "Field Rule1";
			this.dataGridViewNumericUpDownColumn1.Increment = new decimal(new int[]
			{
				1,
				0,
				0,
				65536
			});
			DataGridViewNumericUpDownColumn dataGridViewNumericUpDownColumn = this.dataGridViewNumericUpDownColumn1;
			int[] array = new int[4];
			array[0] = 9999;
			dataGridViewNumericUpDownColumn.Maximum = new decimal(array);
			this.dataGridViewNumericUpDownColumn1.MinimumWidth = 6;
			this.dataGridViewNumericUpDownColumn1.Name = "dataGridViewNumericUpDownColumn1";
			this.dataGridViewNumericUpDownColumn1.Resizable = DataGridViewTriState.True;
			this.dataGridViewNumericUpDownColumn1.SortMode = DataGridViewColumnSortMode.Automatic;
			this.dataGridViewNumericUpDownColumn1.ThousandsSeparator = true;
			this.dataGridViewNumericUpDownColumn1.Width = 125;
			this.dataGridViewNumericUpDownColumn2.DataPropertyName = "FieldRule1";
			this.dataGridViewNumericUpDownColumn2.HeaderText = "Field Rule2";
			this.dataGridViewNumericUpDownColumn2.MinimumWidth = 6;
			this.dataGridViewNumericUpDownColumn2.Name = "dataGridViewNumericUpDownColumn2";
			this.dataGridViewNumericUpDownColumn2.Resizable = DataGridViewTriState.True;
			this.dataGridViewNumericUpDownColumn2.SortMode = DataGridViewColumnSortMode.Automatic;
			this.dataGridViewNumericUpDownColumn2.Width = 125;
			this.dataGridViewTextBoxColumn4.DataPropertyName = "MappingItemType";
			this.dataGridViewTextBoxColumn4.HeaderText = "MappingItemType";
			this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
			this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
			this.dataGridViewTextBoxColumn4.ReadOnly = true;
			this.dataGridViewTextBoxColumn4.Width = 125;
			this.dataGridViewTextBoxColumn5.DataPropertyName = "FieldIndex";
			this.dataGridViewTextBoxColumn5.HeaderText = "FieldIndex";
			this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
			this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
			this.dataGridViewTextBoxColumn5.ReadOnly = true;
			this.dataGridViewTextBoxColumn5.Width = 125;
			this.dataGridViewTextBoxColumn6.DataPropertyName = "FieldRule";
			this.dataGridViewTextBoxColumn6.HeaderText = "FieldRuleTemp";
			this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
			this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
			this.dataGridViewTextBoxColumn6.Visible = false;
			this.dataGridViewTextBoxColumn6.Width = 125;
			this.Id.DataPropertyName = "Id";
			this.Id.HeaderText = "Id";
			this.Id.MinimumWidth = 6;
			this.Id.Name = "Id";
			this.Id.ReadOnly = true;
			this.Id.Width = 125;
			this.FieldName.DataPropertyName = "FieldNameForDisplay";
			this.FieldName.HeaderText = "FieldName";
			this.FieldName.MinimumWidth = 6;
			this.FieldName.Name = "FieldName";
			this.FieldName.ReadOnly = true;
			this.FieldName.Width = 125;
			this.MappedToSource.DataPropertyName = "MappedToSource";
			this.MappedToSource.HeaderText = "Mapped To Source";
			this.MappedToSource.MinimumWidth = 6;
			this.MappedToSource.Name = "MappedToSource";
			this.MappedToSource.ReadOnly = true;
			this.MappedToSource.Resizable = DataGridViewTriState.True;
			this.MappedToSource.SortMode = DataGridViewColumnSortMode.Automatic;
			this.MappedToSource.Width = 125;
			this.FieldRule.HeaderText = "Field Rule";
			this.FieldRule.MinimumWidth = 6;
			this.FieldRule.Name = "FieldRule";
			this.FieldRule.Width = 125;
			this.FieldRule1.DataPropertyName = "FieldRule2";
			this.FieldRule1.DecimalPlaces = 2;
			this.FieldRule1.HeaderText = "Field Rule1";
			this.FieldRule1.Increment = new decimal(new int[]
			{
				1,
				0,
				0,
				65536
			});
			DataGridViewNumericUpDownColumn fieldRule = this.FieldRule1;
			int[] array2 = new int[4];
			array2[0] = 9999;
			fieldRule.Maximum = new decimal(array2);
			this.FieldRule1.MinimumWidth = 6;
			this.FieldRule1.Name = "FieldRule1";
			this.FieldRule1.Resizable = DataGridViewTriState.True;
			this.FieldRule1.SortMode = DataGridViewColumnSortMode.Automatic;
			this.FieldRule1.ThousandsSeparator = true;
			this.FieldRule1.Width = 125;
			this.FieldRule2.DataPropertyName = "FieldRule1";
			this.FieldRule2.HeaderText = "Field Rule2";
			this.FieldRule2.MinimumWidth = 6;
			this.FieldRule2.Name = "FieldRule2";
			this.FieldRule2.Resizable = DataGridViewTriState.True;
			this.FieldRule2.SortMode = DataGridViewColumnSortMode.Automatic;
			this.FieldRule2.Width = 125;
			this.FieldRule3.DataPropertyName = "FieldRule3";
			this.FieldRule3.HeaderText = "Field Rule3";
			this.FieldRule3.MinimumWidth = 6;
			this.FieldRule3.Name = "FieldRule3";
			this.FieldRule3.Width = 125;
			this.FieldRule4.DataPropertyName = "FieldRule4";
			this.FieldRule4.HeaderText = "Field Rule4";
			this.FieldRule4.MinimumWidth = 6;
			this.FieldRule4.Name = "FieldRule4";
			this.FieldRule4.Resizable = DataGridViewTriState.True;
			this.FieldRule4.Width = 125;
			this.FieldRule5.DataPropertyName = "FieldRule5";
			this.FieldRule5.HeaderText = "Field Rule5";
			this.FieldRule5.MinimumWidth = 6;
			this.FieldRule5.Name = "FieldRule5";
			this.FieldRule5.Width = 125;
			this.Import.DataPropertyName = "FieldImport";
			this.Import.HeaderText = "Field To Import";
			this.Import.MinimumWidth = 6;
			this.Import.Name = "Import";
			this.Import.Resizable = DataGridViewTriState.True;
			this.Import.SortMode = DataGridViewColumnSortMode.Automatic;
			this.Import.Width = 125;
			this.MappingItemType.DataPropertyName = "MappingItemType";
			this.MappingItemType.HeaderText = "MappingItemType";
			this.MappingItemType.MinimumWidth = 6;
			this.MappingItemType.Name = "MappingItemType";
			this.MappingItemType.ReadOnly = true;
			this.MappingItemType.Width = 125;
			this.FieldIndex.DataPropertyName = "FieldIndex";
			this.FieldIndex.HeaderText = "FieldIndex";
			this.FieldIndex.MinimumWidth = 6;
			this.FieldIndex.Name = "FieldIndex";
			this.FieldIndex.ReadOnly = true;
			this.FieldIndex.Width = 125;
			this.FieldRuleTemp.DataPropertyName = "FieldRule";
			this.FieldRuleTemp.HeaderText = "FieldRuleTemp";
			this.FieldRuleTemp.MinimumWidth = 6;
			this.FieldRuleTemp.Name = "FieldRuleTemp";
			this.FieldRuleTemp.Visible = false;
			this.FieldRuleTemp.Width = 125;
			this.Empty.HeaderText = "";
			this.Empty.MinimumWidth = 6;
			this.Empty.Name = "Empty";
			this.Empty.ReadOnly = true;
			this.Empty.Width = 125;
			base.AutoScaleDimensions = new SizeF(8f, 16f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.Controls.Add(this.dgTaskMapping);
			base.Name = "taskMappingDataGrid";
			base.Size = new Size(1253, 459);
			((ISupportInitialize)this.dgTaskMapping).EndInit();
			base.ResumeLayout(false);
		}

		private IContainer components;

		private DataGridView dgTaskMapping;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;

		private DataGridViewNumericUpDownColumn dataGridViewNumericUpDownColumn1;

		private DataGridViewNumericUpDownColumn dataGridViewNumericUpDownColumn2;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;

		private DataGridViewTextBoxColumn Id;

		private DataGridViewTextBoxColumn FieldName;

		private DataGridViewCheckBoxColumn MappedToSource;

		private DataGridViewTextBoxColumn FieldRule;

		private DataGridViewNumericUpDownColumn FieldRule1;

		private DataGridViewNumericUpDownColumn FieldRule2;

		private DataGridViewCheckBoxColumn FieldRule3;

		private DataGridViewCheckBoxColumn FieldRule4;

		private DataGridViewComboBoxColumn FieldRule5;

		private DataGridViewCheckBoxColumn Import;

		private DataGridViewTextBoxColumn MappingItemType;

		private DataGridViewTextBoxColumn FieldIndex;

		private DataGridViewTextBoxColumn FieldRuleTemp;

		private DataGridViewTextBoxColumn Empty;
	}
}
