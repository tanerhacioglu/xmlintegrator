﻿namespace NopTalk
{
	// Token: 0x0200001F RID: 31
	public partial class logList : global::System.Windows.Forms.Form
	{
		// Token: 0x060000C3 RID: 195 RVA: 0x0000292B File Offset: 0x00000B2B
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x0000E110 File Offset: 0x0000C310
		private void InitializeComponent()
		{
			global::System.ComponentModel.ComponentResourceManager componentResourceManager = new global::System.ComponentModel.ComponentResourceManager(typeof(global::NopTalk.logList));
			this.groupBox1 = new global::System.Windows.Forms.GroupBox();
			this.nmKeepDays = new global::System.Windows.Forms.NumericUpDown();
			this.label4 = new global::System.Windows.Forms.Label();
			this.label3 = new global::System.Windows.Forms.Label();
			this.chAutoRefresh = new global::System.Windows.Forms.CheckBox();
			this.dtDateTo = new global::System.Windows.Forms.DateTimePicker();
			this.dtDateFrom = new global::System.Windows.Forms.DateTimePicker();
			this.label2 = new global::System.Windows.Forms.Label();
			this.label1 = new global::System.Windows.Forms.Label();
			this.dgLogs = new global::System.Windows.Forms.DataGridView();
			this.btnClearLogs = new global::System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			((global::System.ComponentModel.ISupportInitialize)this.nmKeepDays).BeginInit();
			((global::System.ComponentModel.ISupportInitialize)this.dgLogs).BeginInit();
			base.SuspendLayout();
			this.groupBox1.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.groupBox1.Controls.Add(this.nmKeepDays);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.chAutoRefresh);
			this.groupBox1.Controls.Add(this.dtDateTo);
			this.groupBox1.Controls.Add(this.dtDateFrom);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			///this.groupBox1.Location = new global::System.Drawing.Point(13, 13);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new global::System.Drawing.Size(809, 74);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Filter";
			this.nmKeepDays.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Right);
			this.nmKeepDays.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.nmKeepDays.Enabled = false;
			this.nmKeepDays.Location = new global::System.Drawing.Point(767, 48);
			global::System.Windows.Forms.NumericUpDown numericUpDown = this.nmKeepDays;
			int[] array = new int[4];
			array[0] = 30;
			numericUpDown.Maximum = new decimal(array);
			global::System.Windows.Forms.NumericUpDown numericUpDown2 = this.nmKeepDays;
			int[] array2 = new int[4];
			array2[0] = 1;
			numericUpDown2.Minimum = new decimal(array2);
			this.nmKeepDays.Name = "nmKeepDays";
			this.nmKeepDays.Size = new global::System.Drawing.Size(35, 20);
			this.nmKeepDays.TabIndex = 7;
			global::System.Windows.Forms.NumericUpDown numericUpDown3 = this.nmKeepDays;
			int[] array3 = new int[4];
			array3[0] = 30;
			numericUpDown3.Value = new decimal(array3);
			this.label4.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Right);
			this.label4.AutoSize = true;
			this.label4.Location = new global::System.Drawing.Point(704, 52);
			this.label4.Name = "label4";
			this.label4.Size = new global::System.Drawing.Size(57, 13);
			this.label4.TabIndex = 6;
			this.label4.Text = "Keep days";
			this.label3.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Right);
			this.label3.AutoSize = true;
			this.label3.Location = new global::System.Drawing.Point(704, 15);
			this.label3.Name = "label3";
			this.label3.Size = new global::System.Drawing.Size(79, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "Auto refresh list";
			this.chAutoRefresh.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Right);
			this.chAutoRefresh.AutoSize = true;
			this.chAutoRefresh.Checked = true;
			this.chAutoRefresh.CheckState = global::System.Windows.Forms.CheckState.Checked;
			this.chAutoRefresh.Location = new global::System.Drawing.Point(787, 15);
			this.chAutoRefresh.Name = "chAutoRefresh";
			this.chAutoRefresh.Size = new global::System.Drawing.Size(15, 14);
			this.chAutoRefresh.TabIndex = 4;
			this.chAutoRefresh.UseVisualStyleBackColor = true;
			this.dtDateTo.Location = new global::System.Drawing.Point(83, 46);
			this.dtDateTo.Name = "dtDateTo";
			this.dtDateTo.Size = new global::System.Drawing.Size(200, 20);
			this.dtDateTo.TabIndex = 3;
			this.dtDateTo.ValueChanged += new global::System.EventHandler(this.dtDateTo_ValueChanged);
			this.dtDateFrom.Location = new global::System.Drawing.Point(83, 15);
			this.dtDateFrom.Name = "dtDateFrom";
			this.dtDateFrom.Size = new global::System.Drawing.Size(200, 20);
			this.dtDateFrom.TabIndex = 2;
			this.dtDateFrom.ValueChanged += new global::System.EventHandler(this.dtDateFrom_ValueChanged);
			this.label2.AutoSize = true;
			this.label2.Location = new global::System.Drawing.Point(7, 53);
			this.label2.Name = "label2";
			this.label2.Size = new global::System.Drawing.Size(46, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Date To";
			this.label1.AutoSize = true;
			this.label1.Location = new global::System.Drawing.Point(7, 22);
			this.label1.Name = "label1";
			this.label1.Size = new global::System.Drawing.Size(56, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date From";
			this.dgLogs.AllowUserToAddRows = false;
			this.dgLogs.AllowUserToDeleteRows = false;
			this.dgLogs.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.dgLogs.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgLogs.Location = new global::System.Drawing.Point(12, 93);
			this.dgLogs.MultiSelect = false;
			this.dgLogs.Name = "dgLogs";
			this.dgLogs.SelectionMode = global::System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
			this.dgLogs.Size = new global::System.Drawing.Size(810, 287);
			this.dgLogs.TabIndex = 9;
			this.btnClearLogs.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnClearLogs.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnClearLogs.Location = new global::System.Drawing.Point(747, 386);
			this.btnClearLogs.Name = "btnClearLogs";
			this.btnClearLogs.Size = new global::System.Drawing.Size(75, 23);
			this.btnClearLogs.TabIndex = 12;
			this.btnClearLogs.Text = "Clear logs";
			this.btnClearLogs.UseVisualStyleBackColor = true;
			this.btnClearLogs.Click += new global::System.EventHandler(this.btnClearLogs_Click);
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(834, 413);
			base.Controls.Add(this.btnClearLogs);
			base.Controls.Add(this.dgLogs);
			base.Controls.Add(this.groupBox1);
			base.Icon = (global::System.Drawing.Icon)componentResourceManager.GetObject("$this.Icon");
			base.Name = "logList";
			this.Text = "logList";
			base.FormClosed += new global::System.Windows.Forms.FormClosedEventHandler(this.logList_FormClosed);
			base.Load += new global::System.EventHandler(this.logList_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((global::System.ComponentModel.ISupportInitialize)this.nmKeepDays).EndInit();
			((global::System.ComponentModel.ISupportInitialize)this.dgLogs).EndInit();
			base.ResumeLayout(false);
		}

		// Token: 0x040000AB RID: 171
		private global::System.ComponentModel.IContainer components;

		// Token: 0x040000AC RID: 172
		private global::System.Windows.Forms.GroupBox groupBox1;

		// Token: 0x040000AD RID: 173
		private global::System.Windows.Forms.DataGridView dgLogs;

		// Token: 0x040000AE RID: 174
		private global::System.Windows.Forms.DateTimePicker dtDateTo;

		// Token: 0x040000AF RID: 175
		private global::System.Windows.Forms.DateTimePicker dtDateFrom;

		// Token: 0x040000B0 RID: 176
		private global::System.Windows.Forms.Label label2;

		// Token: 0x040000B1 RID: 177
		private global::System.Windows.Forms.Label label1;

		// Token: 0x040000B2 RID: 178
		private global::System.Windows.Forms.Label label3;

		// Token: 0x040000B3 RID: 179
		private global::System.Windows.Forms.CheckBox chAutoRefresh;

		// Token: 0x040000B4 RID: 180
		private global::System.Windows.Forms.NumericUpDown nmKeepDays;

		// Token: 0x040000B5 RID: 181
		private global::System.Windows.Forms.Label label4;

		// Token: 0x040000B6 RID: 182
		private global::System.Windows.Forms.Button btnClearLogs;
	}
}
