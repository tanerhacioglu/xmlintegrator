﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace NopTalk
{
	public partial class SystemTrayNotify : Form
	{
		public SystemTrayNotify(mainForm owner)
		{
		
			this.components = null;
			this._owner = owner;
			this.InitializeComponent();
		}

		private void SystemTrayNotify_Load(object sender, EventArgs e)
		{
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			base.Close();
			this._owner.GotoSystemTray();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		private mainForm _owner;
	}
}
