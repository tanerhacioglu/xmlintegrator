﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Security.Principal;
using System.Windows.Forms;
using NopTalkCore;

namespace NopTalk
{
	// Token: 0x02000020 RID: 32
	public partial class nopTalkDB : Form
	{
		// Token: 0x060000C5 RID: 197 RVA: 0x00002950 File Offset: 0x00000B50
		public nopTalkDB()
		{
			
			this.dataSettingsManager = new DataSettingsManager();
			this.components = null;
			
			this.InitializeComponent();
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x000024A0 File Offset: 0x000006A0
		private void nopTalkDB_Load(object sender, EventArgs e)
		{
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x000024A0 File Offset: 0x000006A0
		private void btnSave_Click(object sender, EventArgs e)
		{
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x0000E89C File Offset: 0x0000CA9C
		private string IsValidDb()
		{
			string result = "";
			try
			{
				DatabaseFunc.DatabaseInitial();
			}
			catch (Exception ex)
			{
				result = ex.ToString();
			}
			return result;
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x0000E8D4 File Offset: 0x0000CAD4
		private void button1_Click(object sender, EventArgs e)
		{
			string text = this.IsValidDb();
			if (!string.IsNullOrEmpty(text))
			{
				DialogResult dialogResult = MessageBox.Show(text, "Set database configuration", MessageBoxButtons.OKCancel);
				if (dialogResult == DialogResult.Cancel)
				{
					Application.Exit();
				}
			}
			else
			{
				base.Close();
			}
		}

		// Token: 0x060000CA RID: 202 RVA: 0x00002975 File Offset: 0x00000B75
		private void btnExit_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		// Token: 0x060000CB RID: 203 RVA: 0x0000E914 File Offset: 0x0000CB14
		private void ddAuthType_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.ddAuthType.SelectedIndex == 0)
			{
				this.tbUserName.Text = WindowsIdentity.GetCurrent().Name;
				this.tbPassword.Text = "";
				this.tbUserName.ReadOnly = true;
				this.tbPassword.ReadOnly = true;
			}
			else if (this.ddAuthType.SelectedIndex == 1)
			{
				this.tbUserName.Text = "";
				this.tbPassword.Text = "";
				this.tbUserName.ReadOnly = false;
				this.tbPassword.ReadOnly = false;
			}
		}

		// Token: 0x040000B7 RID: 183
		private DataSettingsManager dataSettingsManager;
	}
}
