﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Windows.Forms;
using NopTalkCore;

namespace NopTalk
{
	public partial class frmCopy : Form
	{
		private int _vendorId;

		private int _vendorSourceId;

		private int _vendorSourceMappingId;

		private int _taskId;

		private string _name;

		private TaskMappingFunc _taskMappingFunc;

		public frmCopy(int vendorId, int vendorSourceId, int vendorSourceMappingId, int taskId, string name, TaskMappingFunc taskMappingFunc)
		{
			
			this.components = null;			
			this._vendorId = vendorId;
			this._vendorSourceId = vendorSourceId;
			this._vendorSourceMappingId = vendorSourceMappingId;
			this._taskMappingFunc = taskMappingFunc;
			this._taskId = taskId;
			this._name = name + "_Copy";
			this.InitializeComponent();
		}

		private void frmCopy_Load(object sender, EventArgs e)
		{
			this.tbName.Text = this._name;
			if (this._vendorId > 0)
			{
				this.Text = "Vendor Copy";
			}
			if (this._taskId > 0)
			{
				this.Text = "Task Copy";
			}
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(this.tbName.Text))
			{
				MessageBox.Show("Cannot be copied. The name value is empty.");
			}
			else
			{
				if (this._taskId > 0)
				{
					this.TaskCopy();
				}
				if (this._vendorId > 0)
				{
					this.VendorCopy();
				}
				this.CloseAfterSave();
			}
		}

		private void TaskCopy()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			TaskMapping taskMapping = this._taskMappingFunc.GetTaskMapping(this._taskId);
			taskMapping.TaskId = new int?(this._taskId);
			string dataConnectionString = dataSettingsManager.LoadSettings(null).DataConnectionString;
			using (SqlCeConnection sqlCeConnection = new SqlCeConnection(dataConnectionString))
			{
				string commandText = string.Format("insert into [Task] ([VendorSourceId],\r\n                                            [EShopId],\r\n                                            [TaskName],\r\n                                            [TaskAction],\r\n                                            [TaskStatusId],\r\n                                            [DisableShop],\r\n                                            [ScheduleType],\r\n                                            [ScheduleInterval],\r\n                                            [ScheduleHour],\r\n                                            [ScheduleMinute],\r\n                                            [CreatedDate])\r\n                                        select [VendorSourceId],\r\n                                            [EShopId],\r\n                                            '{0}',\r\n                                            [TaskAction],\r\n                                            [TaskStatusId],\r\n                                            [DisableShop],\r\n                                            [ScheduleType],\r\n                                            [ScheduleInterval],\r\n                                            [ScheduleHour],\r\n                                            [ScheduleMinute],\r\n                                            [CreatedDate]\r\n                                        from [Task]\r\n                                        where Id = @TaskId", this.tbName.Text);
				SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@TaskId", this._taskId));
				sqlCeConnection.Open();
				sqlCeCommand.ExecuteNonQuery();
				sqlCeCommand.CommandText = "SELECT @@IDENTITY";
				Convert.ToInt32(sqlCeCommand.ExecuteScalar());
				sqlCeConnection.Close();
				if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
				if (sqlCeCommand != null)
				{
					sqlCeCommand.Dispose();
				}
				this._taskMappingFunc.UpdateTaskMapping(taskMapping);
			}
		}

		private void VendorCopy()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			string dataConnectionString = dataSettingsManager.LoadSettings(null).DataConnectionString;
			using (SqlCeConnection sqlCeConnection = new SqlCeConnection(dataConnectionString))
			{
				string commandText = string.Format("insert into [Vendor] (\r\n                                            [Name],\r\n                                            [Description])\r\n                                        select \r\n                                            '{0}',\r\n                                            [Description]\r\n                                        from [Vendor]\r\n                                        where Id = @VendorId", this.tbName.Text);
				SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorId", this._vendorId));
				sqlCeConnection.Open();
				sqlCeCommand.ExecuteNonQuery();
				sqlCeCommand.CommandText = "SELECT @@IDENTITY";
				int vendorId = Convert.ToInt32(sqlCeCommand.ExecuteScalar());
				sqlCeConnection.Close();
				if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
				if (sqlCeCommand != null)
				{
					sqlCeCommand.Dispose();
				}
				this.VendorSourceCopy(vendorId);
			}
		}

		private void VendorSourceCopy(int vendorId)
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			string dataConnectionString = dataSettingsManager.LoadSettings(null).DataConnectionString;
			using (SqlCeConnection sqlCeConnection = new SqlCeConnection(dataConnectionString))
			{
				string commandText = string.Format("INSERT INTO [VendorSource]\r\n                                           ([VendorId]\r\n                                           ,[SourceType]\r\n                                           ,[SourceAddress]\r\n                                           ,[SourceUser]\r\n                                           ,[SourcePassword]\r\n                                           ,[SourceFormat]\r\n                                           ,[FirstRowIndex]\r\n                                           ,[CsvDelimeter]\r\n                                           ,[SoapAction]\r\n                                           ,[SoapRequest]\r\n                                           ,[ProductIdInStore]\r\n                                           ,[UserIdInStore]\r\n                                           ,[EntityType]\r\n                                           ,[SourceEncoding]\r\n                                            )\r\n                                        select \r\n                                            '{0}'\r\n                                           ,[SourceType]\r\n                                           ,[SourceAddress]\r\n                                           ,[SourceUser]\r\n                                           ,[SourcePassword]\r\n                                           ,[SourceFormat]\r\n                                           ,[FirstRowIndex]\r\n                                           ,[CsvDelimeter]\r\n                                           ,[SoapAction]\r\n                                           ,[SoapRequest]\r\n                                           ,[ProductIdInStore]\r\n                                           ,[UserIdInStore]\r\n                                           ,[EntityType]\r\n                                           ,[SourceEncoding]\r\n                                        from [VendorSource]\r\n                                        where Id = @VendorSourceId", vendorId);
				SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorSourceId", this._vendorSourceId));
				sqlCeConnection.Open();
				sqlCeCommand.ExecuteNonQuery();
				sqlCeCommand.CommandText = "SELECT @@IDENTITY";
				object value = sqlCeCommand.ExecuteScalar();
				if (GlobalClass.IsInteger(value))
				{
					int vendorSourceId = Convert.ToInt32(value);
					sqlCeConnection.Close();
					if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
					{
						sqlCeConnection.Close();
					}
					if (sqlCeCommand != null)
					{
						sqlCeCommand.Dispose();
					}
					this.VendorSourceMappingCopy(vendorSourceId);
				}
			}
		}

		private void VendorSourceMappingCopy(int vendorSourceId)
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			string dataConnectionString = dataSettingsManager.LoadSettings(null).DataConnectionString;
			using (SqlCeConnection sqlCeConnection = new SqlCeConnection(dataConnectionString))
			{
				string commandText = string.Format("INSERT INTO [SourceMapping]\r\n                                           ([VendorSourceId]\r\n                                           ,[ConfigData]\r\n                                            )\r\n                                        select \r\n                                            '{0}'\r\n                                           ,[ConfigData]\r\n                                        from [SourceMapping]\r\n                                        where VendorSourceId = @VendorSourceId", vendorSourceId);
				SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
				sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorSourceId", this._vendorSourceId));
				sqlCeConnection.Open();
				sqlCeCommand.ExecuteNonQuery();
				sqlCeCommand.CommandText = "SELECT @@IDENTITY";
				Convert.ToInt32(sqlCeCommand.ExecuteScalar());
				sqlCeConnection.Close();
				if (sqlCeConnection != null && sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}
				if (sqlCeCommand != null)
				{
					sqlCeCommand.Dispose();
				}
			}
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		private void CloseAfterSave()
		{
			this.CloseAllOpenForms();
		}

		private void CloseAllOpenForms()
		{
			for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
			{
				if (Application.OpenForms[i].Name != "mainForm" && Application.OpenForms[i].Name != "frmTasksList" && Application.OpenForms[i].Name != "frmVendorsList")
				{
					Application.OpenForms[i].Close();
				}
				if (Application.OpenForms[i].Name == "frmTasksList")
				{
					((frmTasksList)Application.OpenForms[i]).gridFill();
				}
				if (Application.OpenForms[i].Name == "frmVendorsList")
				{
					((frmVendorsList)Application.OpenForms[i]).gridFill();
				}
			}
		}

		
	}
}
