﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace NopTalk
{
	// Token: 0x0200004E RID: 78
	public partial class itemPreview : Form
	{
		// Token: 0x060003EB RID: 1003 RVA: 0x00003B80 File Offset: 0x00001D80
		public itemPreview(SourceDocument sourceDocument, SourceMapping sourceMapping, TaskMapping taskMapping, FileFormat fileFormat)
		{
			
			this.itemIndex = 0;
			this.components = null;
			
			this._sourceDocument = sourceDocument;
			this._sourceMapping = sourceMapping;
			this._taskMapping = taskMapping;
			this._fileFormat = fileFormat;
			this.InitializeComponent();
		}

		// Token: 0x060003EC RID: 1004 RVA: 0x00058074 File Offset: 0x00056274
		private async void itemPreview_Load(object sender, EventArgs e)
		{
			this.lblFoundItems.Text = this.lblFoundItems.Text + " " + this._sourceDocument.Total.ToString();
			Cursor.Current = Cursors.WaitCursor;
			this.ShowItem();
			Cursor.Current = Cursors.Arrow;
			base.WindowState = FormWindowState.Maximized;
		}

		// Token: 0x060003ED RID: 1005 RVA: 0x000580C0 File Offset: 0x000562C0
		private decimal RoundDecimal(string defaultValue, decimal retVal)
		{
			if (!string.IsNullOrEmpty(defaultValue) && defaultValue.Trim().IndexOf('~') == 0)
			{
				int decimals = GlobalClass.StringToInteger(defaultValue.Substring(1), 0);
				retVal = Math.Round(GlobalClass.StringToDecimal(retVal, 0), decimals, MidpointRounding.AwayFromZero);
			}
			return retVal;
		}

		// Token: 0x060003EE RID: 1006 RVA: 0x00058110 File Offset: 0x00056310
		private void ShowItem()
		{
			if (this._sourceDocument.SourceItems.Count > 0 || this._sourceDocument.Total > 0)
			{
				SourceItem sourceItem = this._sourceDocument.LoadByOne ? this._sourceDocument.GetItemByIndex(this._fileFormat, this.itemIndex) : this._sourceDocument.SourceItems[this.itemIndex];
				this.tbProdId.Text = sourceItem.ProdId;
				this.tbProdGroupBy.Text = sourceItem.ProdGroupBy;
				this.tbProdName.Text = sourceItem.ProdName;
				this.tbProdShortDesc.Text = sourceItem.ProdShortDesc;
				this.tbProdFullDesc.Text = sourceItem.ProdFullDesc;
				this.tbProdManPartNum.Text = sourceItem.ProdManPartNum;
				this.tbProdCost.Text = GlobalClass.ObjectToString(sourceItem.ProdCost);
				if (this._taskMapping != null)
				{
					sourceItem.ProdPrice = new decimal?(sourceItem.ProdPrice.GetValueOrDefault());
					sourceItem.ProdPrice = new decimal?(this.RoundDecimal(this._taskMapping.ProdPriceVal, sourceItem.ProdPrice.Value));
				}
				this.tbProdPrice.Text = GlobalClass.ObjectToString(sourceItem.ProdPrice);
				this.tbStockQuantity.Text = GlobalClass.ObjectToString(sourceItem.ProdStock);
				this.tbMetaKeys.Text = GlobalClass.ObjectToString(sourceItem.SeoMetaKey);
				this.tbMetaDescription.Text = GlobalClass.ObjectToString(sourceItem.SeoMetaDesc);
				this.tbMetaTitle.Text = GlobalClass.ObjectToString(sourceItem.SeoMetaTitle);
				this.tbSearchPage.Text = GlobalClass.ObjectToString(sourceItem.SearchPage);
				this.tbManufacturer.Text = GlobalClass.ObjectToString(sourceItem.Manufacturer);
				this.tbVendor.Text = GlobalClass.ObjectToString(sourceItem.Vendor);
				this.tbTaxCategory.Text = GlobalClass.ObjectToString(sourceItem.TaxCategory);
				this.tbGtin.Text = GlobalClass.ObjectToString(sourceItem.Gtin);
				this.tbWeight.Text = GlobalClass.ObjectToString(sourceItem.Weight);
				this.tbLength.Text = GlobalClass.ObjectToString(sourceItem.Length);
				this.tbWidth.Text = GlobalClass.ObjectToString(sourceItem.Width);
				this.tbHeight.Text = GlobalClass.ObjectToString(sourceItem.Height);
				this.tbOldPrice.Text = GlobalClass.ObjectToString(sourceItem.ProdOldPrice);
				this.tbMinStockQty.Text = GlobalClass.ObjectToString(sourceItem.ProdSettingsMinStockQty);
				this.tbMinCartQty.Text = GlobalClass.ObjectToString(sourceItem.ProdSettingsMinCartQty);
				this.tbMaxCartQty.Text = GlobalClass.ObjectToString(sourceItem.ProdSettingsMaxCartQty);
				this.tbNotifyForQty.Text = GlobalClass.ObjectToString(sourceItem.ProdSettingsNotifyQty);
				this.tbAllowedQty.Text = GlobalClass.ObjectToString(sourceItem.ProdSettingsAllowedQty);
				this.dgTierPricing.DataSource = null;
				if (sourceItem.Filters != null && sourceItem.Filters.Count > 0)
				{
					this.dgTierPricing.AutoGenerateColumns = true;
					this.dgTierPricing.DataSource = sourceItem.Filters;
					this.dgTierPricing.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
					this.dgTierPricing.ReadOnly = true;
				}
				this.dgCategories.DataSource = null;
				if (sourceItem.Categories != null && sourceItem.Categories.Count > 0)
				{
					this.dgCategories.AutoGenerateColumns = true;
					IEnumerable<SourceItem.CategoryItem> source = from w in sourceItem.Categories
					where w.Category != ""
					select w;
					this.dgCategories.DataSource = (from x in source
					select new
					{
						x.Category
					}).ToList();
					this.dgCategories.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
					this.dgCategories.ReadOnly = true;
				}
				this.dgAttributes.DataSource = null;
				if ((sourceItem.Attributes != null && sourceItem.Attributes.Count > 0) || (sourceItem.SpecAttributes != null && sourceItem.SpecAttributes.Count > 0))
				{
					List<SourceItem.AttributeValues> list = new List<SourceItem.AttributeValues>();
					if (sourceItem.Attributes != null && sourceItem.Attributes.Count > 0)
					{
						list.AddRange(sourceItem.Attributes);
					}
					if (sourceItem.SpecAttributes != null && sourceItem.SpecAttributes.Count > 0)
					{
						list.AddRange(sourceItem.SpecAttributes);
					}
					var dataSource = (from x in list
					select new
					{
						x.AttInfo_Name,
						x.AttValue_Value,
						x.AttValue_Price,
						x.AttValue_Stock
					}).Distinct().ToList();
					this.dgAttributes.AutoGenerateColumns = true;
					this.dgAttributes.DataSource = dataSource;
					this.dgAttributes.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
					this.dgAttributes.ReadOnly = true;
				}
				this.dgCustomerRoles.DataSource = null;
				if (sourceItem.CustomerRoles != null && sourceItem.CustomerRoles.Count > 0)
				{
					this.dgCustomerRoles.AutoGenerateColumns = true;
					IEnumerable<SourceItem.CustomerRuleItem> source2 = from w in sourceItem.CustomerRoles
					where w.CustomerRule != ""
					select w;
					this.dgCustomerRoles.DataSource = (from x in source2
					select new
					{
						CustomerRole = x.CustomerRule
					}).ToList();
					this.dgCustomerRoles.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
					this.dgCustomerRoles.ReadOnly = true;
				}
				if (sourceItem.TierPricingItems != null && sourceItem.TierPricingItems.Count > 0)
				{
					List<SourceItem.TierPricingItem> list2 = new List<SourceItem.TierPricingItem>();
					list2.AddRange(sourceItem.TierPricingItems);
					var dataSource2 = (from x in list2
					select new
					{
						x.Price,
						x.Quantity,
						x.StartDateTime,
						x.EndDateTime
					}).Distinct().ToList();
					this.dgTierPricing.AutoGenerateColumns = true;
					this.dgTierPricing.DataSource = dataSource2;
					this.dgTierPricing.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
					this.dgTierPricing.ReadOnly = true;
				}
				int num = 0;
				this.pictureBox1.ImageLocation = null;
				this.pictureBox2.ImageLocation = null;
				this.pictureBox3.ImageLocation = null;
				this.pictureBox4.ImageLocation = null;
				this.pictureBox5.ImageLocation = null;
				this.pictureBox1.Image = null;
				this.pictureBox2.Image = null;
				this.pictureBox3.Image = null;
				this.pictureBox4.Image = null;
				this.pictureBox5.Image = null;
				if (sourceItem.Images != null && sourceItem.Images.Count > 0)
				{
					foreach (SourceItem.ImageItem imageItem in sourceItem.Images)
					{
						num++;
						if (num == 1)
						{
							this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
							this.pictureBox1.ClientSize = new Size(100, 100);
							try
							{
								this.pictureBox1.Image = sourceItem.GetImage(imageItem);
								goto IL_8AA;
							}
							catch (Exception)
							{
								goto IL_8AA;
							}
							goto IL_77A;
							IL_8AA:
							this.pictureBox1.Refresh();
						}
						IL_77A:
						if (num == 2)
						{
							this.pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
							this.pictureBox2.ClientSize = new Size(100, 100);
							try
							{
								this.pictureBox2.Image = sourceItem.GetImage(imageItem);
							}
							catch
							{
							}
							this.pictureBox2.Refresh();
						}
						if (num == 3)
						{
							this.pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
							this.pictureBox3.ClientSize = new Size(100, 100);
							try
							{
								this.pictureBox3.Image = sourceItem.GetImage(imageItem);
							}
							catch
							{
							}
							this.pictureBox3.Refresh();
						}
						if (num == 4)
						{
							this.pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;
							this.pictureBox4.ClientSize = new Size(100, 100);
							try
							{
								this.pictureBox4.Image = sourceItem.GetImage(imageItem);
							}
							catch
							{
							}
							this.pictureBox4.Refresh();
						}
						if (num == 5)
						{
							this.pictureBox5.SizeMode = PictureBoxSizeMode.StretchImage;
							this.pictureBox5.ClientSize = new Size(100, 100);
							try
							{
								this.pictureBox5.Image = sourceItem.GetImage(imageItem);
							}
							catch
							{
							}
							this.pictureBox5.Refresh();
						}
					}
				}
			}
			this.lblShow.Text = "Show item: " + (this.itemIndex + 1).ToString() + "/" + this._sourceDocument.Total.ToString();
			if (this.itemIndex <= 0)
			{
				this.btnPrev.Enabled = false;
			}
			else
			{
				this.btnPrev.Enabled = true;
			}
			if (this.itemIndex == this._sourceDocument.Total - 1 || this._sourceDocument.Total == 0)
			{
				this.btnNext.Enabled = false;
			}
			else
			{
				this.btnNext.Enabled = true;
			}
		}

		// Token: 0x060003EF RID: 1007 RVA: 0x00058B20 File Offset: 0x00056D20
		private void btnNext_Click(object sender, EventArgs e)
		{
			if (this.itemIndex < this._sourceDocument.Total - 1)
			{
				this.itemIndex++;
				Cursor.Current = Cursors.WaitCursor;
				this.ShowItem();
				Cursor.Current = Cursors.Arrow;
			}
		}

		// Token: 0x060003F0 RID: 1008 RVA: 0x00003BBE File Offset: 0x00001DBE
		private void btnPrev_Click(object sender, EventArgs e)
		{
			if (this.itemIndex > 0)
			{
				this.itemIndex--;
				Cursor.Current = Cursors.WaitCursor;
				this.ShowItem();
				Cursor.Current = Cursors.Arrow;
			}
		}

		// Token: 0x060003F1 RID: 1009 RVA: 0x000024A2 File Offset: 0x000006A2
		private void btnClose_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060003F2 RID: 1010 RVA: 0x0000E0E4 File Offset: 0x0000C2E4
		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams createParams = base.CreateParams;
				createParams.ExStyle |= 33554432;
				return createParams;
			}
		}

		// Token: 0x040007A1 RID: 1953
		private SourceDocument _sourceDocument;

		// Token: 0x040007A2 RID: 1954
		private SourceMapping _sourceMapping;

		// Token: 0x040007A3 RID: 1955
		private TaskMapping _taskMapping;

		// Token: 0x040007A4 RID: 1956
		private FileFormat _fileFormat;

		// Token: 0x040007A5 RID: 1957
		private int itemIndex;
	}
}
