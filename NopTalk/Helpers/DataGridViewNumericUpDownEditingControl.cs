using System;
using System.Drawing;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace NopTalk.Helpers
{
	internal class DataGridViewNumericUpDownEditingControl : NumericUpDown, IDataGridViewEditingControl
	{
		private DataGridView dataGridView;

		private bool valueChanged;

		private int rowIndex;

		public virtual DataGridView EditingControlDataGridView
		{
			get
			{
				return dataGridView;
			}
			set
			{
				dataGridView = value;
			}
		}

		public virtual object EditingControlFormattedValue
		{
			get
			{
				return GetEditingControlFormattedValue(DataGridViewDataErrorContexts.Formatting);
			}
			set
			{
				Text = (string)value;
			}
		}

		public virtual int EditingControlRowIndex
		{
			get
			{
				return rowIndex;
			}
			set
			{
				rowIndex = value;
			}
		}

		public virtual bool EditingControlValueChanged
		{
			get
			{
				return valueChanged;
			}
			set
			{
				valueChanged = value;
			}
		}

		public virtual Cursor EditingPanelCursor => Cursors.Default;

		public virtual bool RepositionEditingControlOnValueChange => false;

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams createParams = base.CreateParams;
				createParams.ExStyle |= 33554432;
				return createParams;
			}
		}

		[DllImport("USER32.DLL", CharSet = CharSet.Auto)]
		private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

		public DataGridViewNumericUpDownEditingControl()
		{
			
			base.TabStop = false;
		}

		public virtual void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
		{
			Font = dataGridViewCellStyle.Font;
			if (dataGridViewCellStyle.BackColor.A < byte.MaxValue)
			{
				Color backColor = BackColor = Color.FromArgb(255, dataGridViewCellStyle.BackColor);
				dataGridView.EditingPanel.BackColor = backColor;
			}
			else
			{
				BackColor = dataGridViewCellStyle.BackColor;
			}
			ForeColor = dataGridViewCellStyle.ForeColor;
			base.TextAlign = DataGridViewNumericUpDownCell.TranslateAlignment(dataGridViewCellStyle.Alignment);
		}

		public virtual bool EditingControlWantsInputKey(Keys keyData, bool dataGridViewWantsInputKey)
		{
			switch (keyData & Keys.KeyCode)
			{
			case Keys.End:
			case Keys.Home:
			{
				TextBox textBox3 = base.Controls[1] as TextBox;
				if (textBox3 != null && textBox3.SelectionLength != textBox3.Text.Length)
				{
					return true;
				}
				break;
			}
			case Keys.Left:
			{
				TextBox textBox4 = base.Controls[1] as TextBox;
				if (textBox4 != null && ((RightToLeft == RightToLeft.No && (textBox4.SelectionLength != 0 || textBox4.SelectionStart != 0)) || (RightToLeft == RightToLeft.Yes && (textBox4.SelectionLength != 0 || textBox4.SelectionStart != textBox4.Text.Length))))
				{
					return true;
				}
				break;
			}
			case Keys.Up:
				if (base.Value < base.Maximum)
				{
					return true;
				}
				break;
			case Keys.Right:
			{
				TextBox textBox2 = base.Controls[1] as TextBox;
				if (textBox2 != null && ((RightToLeft == RightToLeft.No && (textBox2.SelectionLength != 0 || textBox2.SelectionStart != textBox2.Text.Length)) || (RightToLeft == RightToLeft.Yes && (textBox2.SelectionLength != 0 || textBox2.SelectionStart != 0))))
				{
					return true;
				}
				break;
			}
			case Keys.Down:
				if (base.Value > base.Minimum)
				{
					return true;
				}
				break;
			case Keys.Delete:
			{
				TextBox textBox = base.Controls[1] as TextBox;
				if (textBox != null && (textBox.SelectionLength > 0 || textBox.SelectionStart < textBox.Text.Length))
				{
					return true;
				}
				break;
			}
			}
			return !dataGridViewWantsInputKey;
		}

		public virtual object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
		{
			bool userEdit = base.UserEdit;
			try
			{
				base.UserEdit = ((context & DataGridViewDataErrorContexts.Display) == 0);
				return base.Value.ToString((base.ThousandsSeparator ? "N" : "F") + base.DecimalPlaces);
			}
			finally
			{
				base.UserEdit = userEdit;
			}
		}

		public virtual void PrepareEditingControlForEdit(bool selectAll)
		{
			TextBox textBox = base.Controls[1] as TextBox;
			if (textBox != null)
			{
				if (selectAll)
				{
					textBox.SelectAll();
				}
				else
				{
					textBox.SelectionStart = textBox.Text.Length;
				}
			}
		}

		private void NotifyDataGridViewOfValueChange()
		{
			if (!valueChanged)
			{
				valueChanged = true;
				dataGridView.NotifyCurrentCellDirty(dirty: true);
			}
		}

		protected override void OnKeyPress(KeyPressEventArgs e)
		{
			base.OnKeyPress(e);
			bool flag = false;
			if (char.IsDigit(e.KeyChar))
			{
				flag = true;
			}
			else
			{
				NumberFormatInfo numberFormat = CultureInfo.CurrentCulture.NumberFormat;
				string numberDecimalSeparator = numberFormat.NumberDecimalSeparator;
				string numberGroupSeparator = numberFormat.NumberGroupSeparator;
				string negativeSign = numberFormat.NegativeSign;
				if (!string.IsNullOrEmpty(numberDecimalSeparator) && numberDecimalSeparator.Length == 1)
				{
					flag = (numberDecimalSeparator[0] == e.KeyChar);
				}
				if (!flag && !string.IsNullOrEmpty(numberGroupSeparator) && numberGroupSeparator.Length == 1)
				{
					flag = (numberGroupSeparator[0] == e.KeyChar);
				}
				if (!flag && !string.IsNullOrEmpty(negativeSign) && negativeSign.Length == 1)
				{
					flag = (negativeSign[0] == e.KeyChar);
				}
			}
			if (flag)
			{
				NotifyDataGridViewOfValueChange();
			}
		}

		protected override void OnValueChanged(EventArgs e)
		{
			base.OnValueChanged(e);
			if (Focused)
			{
				NotifyDataGridViewOfValueChange();
			}
		}

		protected override bool ProcessKeyEventArgs(ref Message m)
		{
			TextBox textBox = base.Controls[1] as TextBox;
			if (textBox != null)
			{
				SendMessage(textBox.Handle, m.Msg, m.WParam, m.LParam);
				return true;
			}
			return base.ProcessKeyEventArgs(ref m);
		}
	}
}
