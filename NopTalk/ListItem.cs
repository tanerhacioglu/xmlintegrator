﻿using System;

namespace NopTalk
{
	public class ListItem
	{
		public ListItem(string text, int Value)
		{
			
			Value = Value;
			this.Text = text;
		}

		public override string ToString()
		{
			return this.Text;
		}

		public int Value;

		public string Text;
	}
}
