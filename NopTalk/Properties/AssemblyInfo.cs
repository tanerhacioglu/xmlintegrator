﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;

[assembly: AssemblyVersion("4.3.0.0")]
[assembly: AssemblyTitle("ART")]
[assembly: AssemblyDescription("Automated Product Import tools allows you to synchronize product import from external sources of wholesale partner into your nopCommerce online store.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("VaskSoft (www.vasksoft.com)")]
[assembly: AssemblyProduct("NopTalk Pro")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("2411f47f-0f80-4ab2-8bd3-4134bb7e59d6")]
[assembly: AssemblyFileVersion("4.3.0.0")]
[assembly: AssemblyKeyName("")]
[assembly: AssemblyCopyright("Copyright © 2015-2020 VaskSoft. All rights reserved.")]
[assembly: AssemblyDelaySign(false)]
