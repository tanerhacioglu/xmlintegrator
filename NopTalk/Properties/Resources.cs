﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace NopTalk.Properties
{
	[DebuggerNonUserCode]
	[CompilerGenerated]
	[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
	internal class Resources
	{

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static ResourceManager ResourceManager
		{
			get
			{
				if (Resources.resourceMan == null)
				{
					ResourceManager resourceManager = new ResourceManager("NopTalk.Properties.Resources", typeof(Resources).Assembly);
					Resources.resourceMan = resourceManager;
				}
				return Resources.resourceMan;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static CultureInfo Culture
		{
			get
			{
				return Resources.resourceCulture;
			}
			set
			{
				Resources.resourceCulture = value;
			}
		}

		internal static Icon nop_example2
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("nop_example2", Resources.resourceCulture);
				return (Icon)@object;
			}
		}

		internal static Icon nop_example3
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("nop_example3", Resources.resourceCulture);
				return (Icon)@object;
			}
		}

		internal static Bitmap nopCommerceLogo
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("nopCommerceLogo", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap noptalk_diagram
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("noptalk_diagram", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static string NopTalkDb_samples
		{
			get
			{
				return Resources.ResourceManager.GetString("NopTalkDb_samples", Resources.resourceCulture);
			}
		}

		private static ResourceManager resourceMan;

		private static CultureInfo resourceCulture;
	}
}
