﻿namespace NopTalk
{
	// Token: 0x0200004C RID: 76
	public partial class frmeShopsList : global::System.Windows.Forms.Form
	{
		// Token: 0x060003E6 RID: 998 RVA: 0x00003B5B File Offset: 0x00001D5B
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x060003E7 RID: 999 RVA: 0x00057B94 File Offset: 0x00055D94
		private void InitializeComponent()
		{
			this.components = new global::System.ComponentModel.Container();
			global::System.ComponentModel.ComponentResourceManager componentResourceManager = new global::System.ComponentModel.ComponentResourceManager(typeof(global::NopTalk.frmeShopsList));
			this.dgEshops = new global::System.Windows.Forms.DataGridView();
			this.eShopBindingSource = new global::System.Windows.Forms.BindingSource(this.components);
			this.btnNew = new global::System.Windows.Forms.Button();
			this.btnEdit = new global::System.Windows.Forms.Button();
			this.btnDelete = new global::System.Windows.Forms.Button();
			((global::System.ComponentModel.ISupportInitialize)this.dgEshops).BeginInit();
			((global::System.ComponentModel.ISupportInitialize)this.eShopBindingSource).BeginInit();
			base.SuspendLayout();
			this.dgEshops.AllowUserToAddRows = false;
			this.dgEshops.AllowUserToDeleteRows = false;
			this.dgEshops.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.dgEshops.AutoGenerateColumns = false;
			this.dgEshops.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgEshops.DataSource = this.eShopBindingSource;
			this.dgEshops.Location = new global::System.Drawing.Point(12, 12);
			this.dgEshops.MultiSelect = false;
			this.dgEshops.Name = "dgEshops";
			this.dgEshops.SelectionMode = global::System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgEshops.Size = new global::System.Drawing.Size(770, 305);
			this.dgEshops.TabIndex = 0;
			this.dgEshops.CellDoubleClick += new global::System.Windows.Forms.DataGridViewCellEventHandler(this.dgEshops_CellDoubleClick);
			this.eShopBindingSource.DataMember = "EShop";
			this.btnNew.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnNew.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnNew.Location = new global::System.Drawing.Point(545, 337);
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new global::System.Drawing.Size(75, 23);
			this.btnNew.TabIndex = 1;
			this.btnNew.Text = "Insert";
			this.btnNew.UseVisualStyleBackColor = true;
			this.btnNew.Click += new global::System.EventHandler(this.btnNew_Click);
			this.btnEdit.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnEdit.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnEdit.Location = new global::System.Drawing.Point(626, 337);
			this.btnEdit.Name = "btnEdit";
			this.btnEdit.Size = new global::System.Drawing.Size(75, 23);
			this.btnEdit.TabIndex = 2;
			this.btnEdit.Text = "Edit";
			this.btnEdit.UseVisualStyleBackColor = true;
			this.btnEdit.Click += new global::System.EventHandler(this.btnEdit_Click);
			this.btnDelete.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnDelete.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnDelete.Location = new global::System.Drawing.Point(707, 337);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new global::System.Drawing.Size(75, 23);
			this.btnDelete.TabIndex = 3;
			this.btnDelete.Text = "Delete";
			this.btnDelete.UseVisualStyleBackColor = true;
			this.btnDelete.Click += new global::System.EventHandler(this.btnDelete_Click);
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(794, 372);
			base.Controls.Add(this.btnDelete);
			base.Controls.Add(this.btnEdit);
			base.Controls.Add(this.btnNew);
			base.Controls.Add(this.dgEshops);
			base.Icon = (global::System.Drawing.Icon)componentResourceManager.GetObject("$this.Icon");
			base.Name = "frmeShopsList";
			base.StartPosition = global::System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Stores list";
			base.Load += new global::System.EventHandler(this.eShops_Load);
			((global::System.ComponentModel.ISupportInitialize)this.dgEshops).EndInit();
			((global::System.ComponentModel.ISupportInitialize)this.eShopBindingSource).EndInit();
			base.ResumeLayout(false);
		}

		// Token: 0x04000790 RID: 1936
		private global::System.ComponentModel.IContainer components;

		// Token: 0x04000791 RID: 1937
		private global::System.Windows.Forms.DataGridView dgEshops;

		// Token: 0x04000792 RID: 1938
		private global::System.Windows.Forms.BindingSource eShopBindingSource;

		// Token: 0x04000797 RID: 1943
		private global::System.Windows.Forms.Button btnNew;

		// Token: 0x04000798 RID: 1944
		private global::System.Windows.Forms.Button btnEdit;

		// Token: 0x04000799 RID: 1945
		private global::System.Windows.Forms.Button btnDelete;
	}
}
