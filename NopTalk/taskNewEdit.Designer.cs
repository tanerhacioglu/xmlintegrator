﻿namespace NopTalk
{
	// Token: 0x02000029 RID: 41
	public partial class taskNewEdit : global::System.Windows.Forms.Form
	{
		// Token: 0x06000165 RID: 357 RVA: 0x00002DC1 File Offset: 0x00000FC1
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000166 RID: 358 RVA: 0x00018984 File Offset: 0x00016B84
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.tabTask = new System.Windows.Forms.TabControl();
            this.tabTaskData = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tbSourceCopyPath = new System.Windows.Forms.TextBox();
            this.chTitleCase = new System.Windows.Forms.CheckBox();
            this.label77 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbTaskName = new System.Windows.Forms.TextBox();
            this.ddStoreWeb = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ddVendorSourceMap = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ddTaskStatus = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ddTaskAction = new System.Windows.Forms.ComboBox();
            this.ddScheduleType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chDisableShop = new System.Windows.Forms.CheckBox();
            this.chLimitedStore = new System.Windows.Forms.CheckedListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.chUnpublish = new System.Windows.Forms.CheckBox();
            this.label70 = new System.Windows.Forms.Label();
            this.chDeleteFileAfterImport = new System.Windows.Forms.CheckBox();
            this.pnSchedulerTime = new System.Windows.Forms.Panel();
            this.label67 = new System.Windows.Forms.Label();
            this.nmHour = new System.Windows.Forms.NumericUpDown();
            this.label68 = new System.Windows.Forms.Label();
            this.nmMinute = new System.Windows.Forms.NumericUpDown();
            this.label69 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.chPicturesInFiles = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label84 = new System.Windows.Forms.Label();
            this.tbPicturesPathSave = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.nmInterval = new System.Windows.Forms.NumericUpDown();
            this.label66 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this.label192 = new System.Windows.Forms.Label();
            this.chLimitedToSoresEntities = new System.Windows.Forms.CheckedListBox();
            this.tabProductInfo = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.chProdIdMap = new System.Windows.Forms.CheckBox();
            this.chProdNameMap = new System.Windows.Forms.CheckBox();
            this.chProdShortDescMap = new System.Windows.Forms.CheckBox();
            this.chProdFullDescMap = new System.Windows.Forms.CheckBox();
            this.chProdManPartNumMap = new System.Windows.Forms.CheckBox();
            this.chProdStockMap = new System.Windows.Forms.CheckBox();
            this.nmStockQuantity = new System.Windows.Forms.TextBox();
            this.tbManufacturerPnum = new System.Windows.Forms.TextBox();
            this.tbProdFullDesc = new System.Windows.Forms.TextBox();
            this.tbProdShortDesc = new System.Windows.Forms.TextBox();
            this.tbProductName = new System.Windows.Forms.TextBox();
            this.chProdNameImport = new System.Windows.Forms.CheckBox();
            this.chProdShortDescImport = new System.Windows.Forms.CheckBox();
            this.chProdFullDescImport = new System.Windows.Forms.CheckBox();
            this.chProdManPartNumImport = new System.Windows.Forms.CheckBox();
            this.chProdStockImport = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.chManufacturerMap = new System.Windows.Forms.CheckBox();
            this.tbManufacturer = new System.Windows.Forms.TextBox();
            this.chManufacturerImport = new System.Windows.Forms.CheckBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.chVendorMap = new System.Windows.Forms.CheckBox();
            this.chGtinMap = new System.Windows.Forms.CheckBox();
            this.chWeightMap = new System.Windows.Forms.CheckBox();
            this.chLengthMap = new System.Windows.Forms.CheckBox();
            this.chWidthMap = new System.Windows.Forms.CheckBox();
            this.chHeightMap = new System.Windows.Forms.CheckBox();
            this.tbVendor = new System.Windows.Forms.TextBox();
            this.tbGtin = new System.Windows.Forms.TextBox();
            this.nmWeight = new System.Windows.Forms.TextBox();
            this.tbLength = new System.Windows.Forms.TextBox();
            this.tbWidth = new System.Windows.Forms.TextBox();
            this.tbHeight = new System.Windows.Forms.TextBox();
            this.chVendorImport = new System.Windows.Forms.CheckBox();
            this.chGtinImport = new System.Windows.Forms.CheckBox();
            this.chWeightImport = new System.Windows.Forms.CheckBox();
            this.chLengthImport = new System.Windows.Forms.CheckBox();
            this.chWidthImport = new System.Windows.Forms.CheckBox();
            this.chHeightImport = new System.Windows.Forms.CheckBox();
            this.tabProductSettings = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.label176 = new System.Windows.Forms.Label();
            this.label175 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.chProductPublishImport = new System.Windows.Forms.CheckBox();
            this.label117 = new System.Windows.Forms.Label();
            this.ddDeliveryDate = new System.Windows.Forms.ComboBox();
            this.chDayDeliveryImport = new System.Windows.Forms.CheckBox();
            this.label119 = new System.Windows.Forms.Label();
            this.chInventoryMethodImport = new System.Windows.Forms.CheckBox();
            this.chProductPublish = new System.Windows.Forms.CheckBox();
            this.chProductReviewEnable = new System.Windows.Forms.CheckBox();
            this.chProductReviewEnableImport = new System.Windows.Forms.CheckBox();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.chProductDisplayAvailability = new System.Windows.Forms.CheckBox();
            this.chProductDisplayAvailabilityImport = new System.Windows.Forms.CheckBox();
            this.tbProductMinStockQty = new System.Windows.Forms.TextBox();
            this.tbProductNotifyQty = new System.Windows.Forms.TextBox();
            this.tbProductMinCartQty = new System.Windows.Forms.TextBox();
            this.tbProductMaxCartQty = new System.Windows.Forms.TextBox();
            this.tbProductAllowQty = new System.Windows.Forms.TextBox();
            this.chProductMinStockQtyAction = new System.Windows.Forms.CheckBox();
            this.chProductNotifyQtyAction = new System.Windows.Forms.CheckBox();
            this.chProductMinCartQtyAction = new System.Windows.Forms.CheckBox();
            this.chProductMaxCartQtyAction = new System.Windows.Forms.CheckBox();
            this.chProductAllowQtyAction = new System.Windows.Forms.CheckBox();
            this.chProductMinStockQtyMap = new System.Windows.Forms.CheckBox();
            this.chProductNotifyQtyMap = new System.Windows.Forms.CheckBox();
            this.chProductMinCartQtyMap = new System.Windows.Forms.CheckBox();
            this.chProductMaxCartQtyMap = new System.Windows.Forms.CheckBox();
            this.chProductAllowQtyMap = new System.Windows.Forms.CheckBox();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.chProductDisplayStockQuantity = new System.Windows.Forms.CheckBox();
            this.chProductDisplayStockQuantityImport = new System.Windows.Forms.CheckBox();
            this.label149 = new System.Windows.Forms.Label();
            this.ddlBackorderMode = new System.Windows.Forms.ComboBox();
            this.chBackorderModeImport = new System.Windows.Forms.CheckBox();
            this.chDayDeliveryMap = new System.Windows.Forms.CheckBox();
            this.label157 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.chProductShippingEnabledMap = new System.Windows.Forms.CheckBox();
            this.chProductFreeShippingMap = new System.Windows.Forms.CheckBox();
            this.chProductShipSeparatelyMap = new System.Windows.Forms.CheckBox();
            this.chProductShippingChargeMap = new System.Windows.Forms.CheckBox();
            this.chProductShippingEnabled = new System.Windows.Forms.CheckBox();
            this.chProductFreeShipping = new System.Windows.Forms.CheckBox();
            this.chProductShipSeparately = new System.Windows.Forms.CheckBox();
            this.chProductShippingEnabledImport = new System.Windows.Forms.CheckBox();
            this.chProductFreeShippingImport = new System.Windows.Forms.CheckBox();
            this.chProductShipSeparatelyImport = new System.Windows.Forms.CheckBox();
            this.chProductShippingChargeImport = new System.Windows.Forms.CheckBox();
            this.nmProductShippingCharge = new System.Windows.Forms.NumericUpDown();
            this.ddInventoryMethod = new System.Windows.Forms.ComboBox();
            this.label194 = new System.Windows.Forms.Label();
            this.ddLowStockActivity = new System.Windows.Forms.ComboBox();
            this.chLowStockActivityImport = new System.Windows.Forms.CheckBox();
            this.label174 = new System.Windows.Forms.Label();
            this.chVisibleIndividually = new System.Windows.Forms.CheckBox();
            this.chVisibleIndividuallyImport = new System.Windows.Forms.CheckBox();
            this.ddWarehouse = new System.Windows.Forms.ComboBox();
            this.chWarehouseImport = new System.Windows.Forms.CheckBox();
            this.ddProductType = new System.Windows.Forms.ComboBox();
            this.chProductTypeImport = new System.Windows.Forms.CheckBox();
            this.chVisibleIndividuallyMap = new System.Windows.Forms.CheckBox();
            this.chProductIsDeletedMap = new System.Windows.Forms.CheckBox();
            this.label57 = new System.Windows.Forms.Label();
            this.chProductIsDeleted = new System.Windows.Forms.CheckBox();
            this.chProductIsDeletedImport = new System.Windows.Forms.CheckBox();
            this.tabSeo = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tbSearchEnginePage = new System.Windows.Forms.TextBox();
            this.tbMetaTitle = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tbMetaDescription = new System.Windows.Forms.TextBox();
            this.tbMetaKeywords = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.chSeoMetaKeyMap = new System.Windows.Forms.CheckBox();
            this.chSeoMetaDescMap = new System.Windows.Forms.CheckBox();
            this.chSeoMetaTitleMap = new System.Windows.Forms.CheckBox();
            this.chSearchPageMap = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.chSeoMetaKeyImport = new System.Windows.Forms.CheckBox();
            this.chSeoMetaDescImport = new System.Windows.Forms.CheckBox();
            this.chSeoMetaTitleImport = new System.Windows.Forms.CheckBox();
            this.chSearchPageImport = new System.Windows.Forms.CheckBox();
            this.tabCategories = new System.Windows.Forms.TabPage();
            this.chDisableNestedCategories = new System.Windows.Forms.CheckBox();
            this.label55 = new System.Windows.Forms.Label();
            this.chInsertCategoryPictureOnInsert = new System.Windows.Forms.CheckBox();
            this.label170 = new System.Windows.Forms.Label();
            this.chInsertProductsToLastSubCategory = new System.Windows.Forms.CheckBox();
            this.label147 = new System.Windows.Forms.Label();
            this.chDeleteOldCatMapping = new System.Windows.Forms.CheckBox();
            this.label98 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label35 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.tbCategory = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.tbCategory1 = new System.Windows.Forms.TextBox();
            this.tbCategory2 = new System.Windows.Forms.TextBox();
            this.tbCategory3 = new System.Windows.Forms.TextBox();
            this.tbCategory4 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.chCategoryMap = new System.Windows.Forms.CheckBox();
            this.chCategory1Map = new System.Windows.Forms.CheckBox();
            this.chCategory2Map = new System.Windows.Forms.CheckBox();
            this.chCategory3Map = new System.Windows.Forms.CheckBox();
            this.chCategory4Map = new System.Windows.Forms.CheckBox();
            this.label34 = new System.Windows.Forms.Label();
            this.chCategoryImport = new System.Windows.Forms.CheckBox();
            this.chCategory1Import = new System.Windows.Forms.CheckBox();
            this.chCategory2Import = new System.Windows.Forms.CheckBox();
            this.chCategory3Import = new System.Windows.Forms.CheckBox();
            this.chCategory4Import = new System.Windows.Forms.CheckBox();
            this.label75 = new System.Windows.Forms.Label();
            this.ddParentCat1 = new System.Windows.Forms.ComboBox();
            this.ddParentCat2 = new System.Windows.Forms.ComboBox();
            this.ddParentCat3 = new System.Windows.Forms.ComboBox();
            this.ddParentCat4 = new System.Windows.Forms.ComboBox();
            this.ddParentCat0 = new System.Windows.Forms.ComboBox();
            this.tabAccessControl = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.chCustomerRole4Map = new System.Windows.Forms.CheckBox();
            this.tbCustomerRole3 = new System.Windows.Forms.TextBox();
            this.tbCustomerRole2 = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.tbCustomerRole1 = new System.Windows.Forms.TextBox();
            this.tbCustomerRole = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.chCustomerRoleMap = new System.Windows.Forms.CheckBox();
            this.chCustomerRole1Map = new System.Windows.Forms.CheckBox();
            this.chCustomerRole2Map = new System.Windows.Forms.CheckBox();
            this.chCustomerRole3Map = new System.Windows.Forms.CheckBox();
            this.label92 = new System.Windows.Forms.Label();
            this.chCustomerRoleImport = new System.Windows.Forms.CheckBox();
            this.chCustomerRole1Import = new System.Windows.Forms.CheckBox();
            this.chCustomerRole2Import = new System.Windows.Forms.CheckBox();
            this.chCustomerRole3Import = new System.Windows.Forms.CheckBox();
            this.label93 = new System.Windows.Forms.Label();
            this.tbCustomerRole4 = new System.Windows.Forms.TextBox();
            this.chCustomerRole4Import = new System.Windows.Forms.CheckBox();
            this.tabImages = new System.Windows.Forms.TabPage();
            this.panelProdPictures = new System.Windows.Forms.Panel();
            this.nmPictureResizeHeight = new System.Windows.Forms.NumericUpDown();
            this.nmPictureResizeWidth = new System.Windows.Forms.NumericUpDown();
            this.nmPictureResizeQuality = new System.Windows.Forms.NumericUpDown();
            this.label190 = new System.Windows.Forms.Label();
            this.label189 = new System.Windows.Forms.Label();
            this.label185 = new System.Windows.Forms.Label();
            this.chResizePicture = new System.Windows.Forms.CheckBox();
            this.label184 = new System.Windows.Forms.Label();
            this.chDeleteOldPictures = new System.Windows.Forms.CheckBox();
            this.label151 = new System.Windows.Forms.Label();
            this.tabProdAttributes = new System.Windows.Forms.TabPage();
            this.panelProdAttributtes = new System.Windows.Forms.Panel();
            this.chDeleteOldAttributtes = new System.Windows.Forms.CheckBox();
            this.label152 = new System.Windows.Forms.Label();
            this.tabSpecAttributes = new System.Windows.Forms.TabPage();
            this.panelProdSpecAttributtes = new System.Windows.Forms.Panel();
            this.chAddSpecAttributesAsTable = new System.Windows.Forms.CheckBox();
            this.label191 = new System.Windows.Forms.Label();
            this.chDeleteOldSpecAttributes = new System.Windows.Forms.CheckBox();
            this.label156 = new System.Windows.Forms.Label();
            this.tabFilters = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.chFilter9Map = new System.Windows.Forms.CheckBox();
            this.lblFilter9 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.chFilterMap = new System.Windows.Forms.CheckBox();
            this.chFilter1Map = new System.Windows.Forms.CheckBox();
            this.chFilter2Map = new System.Windows.Forms.CheckBox();
            this.chFilter3Map = new System.Windows.Forms.CheckBox();
            this.chFilter4Map = new System.Windows.Forms.CheckBox();
            this.lblFilter = new System.Windows.Forms.Label();
            this.lblFilter1 = new System.Windows.Forms.Label();
            this.lblFilter2 = new System.Windows.Forms.Label();
            this.lblFilter3 = new System.Windows.Forms.Label();
            this.lblFilter4 = new System.Windows.Forms.Label();
            this.lblFilter5 = new System.Windows.Forms.Label();
            this.lblFilter6 = new System.Windows.Forms.Label();
            this.lblFilter7 = new System.Windows.Forms.Label();
            this.lblFilter8 = new System.Windows.Forms.Label();
            this.chFilter5Map = new System.Windows.Forms.CheckBox();
            this.chFilter6Map = new System.Windows.Forms.CheckBox();
            this.chFilter7Map = new System.Windows.Forms.CheckBox();
            this.chFilter8Map = new System.Windows.Forms.CheckBox();
            this.tbFilterValue = new System.Windows.Forms.TextBox();
            this.tbFilter1Value = new System.Windows.Forms.TextBox();
            this.tbFilter2Value = new System.Windows.Forms.TextBox();
            this.tbFilter3Value = new System.Windows.Forms.TextBox();
            this.tbFilter4Value = new System.Windows.Forms.TextBox();
            this.tbFilter5Value = new System.Windows.Forms.TextBox();
            this.tbFilter6Value = new System.Windows.Forms.TextBox();
            this.tbFilter7Value = new System.Windows.Forms.TextBox();
            this.tbFilter8Value = new System.Windows.Forms.TextBox();
            this.ddFilterOp = new System.Windows.Forms.ComboBox();
            this.label63 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.comboBox_0 = new System.Windows.Forms.ComboBox();
            this.comboBox_1 = new System.Windows.Forms.ComboBox();
            this.comboBox_2 = new System.Windows.Forms.ComboBox();
            this.comboBox_3 = new System.Windows.Forms.ComboBox();
            this.comboBox_4 = new System.Windows.Forms.ComboBox();
            this.comboBox_5 = new System.Windows.Forms.ComboBox();
            this.comboBox_6 = new System.Windows.Forms.ComboBox();
            this.comboBox_7 = new System.Windows.Forms.ComboBox();
            this.comboBox_8 = new System.Windows.Forms.ComboBox();
            this.tbFilter9Value = new System.Windows.Forms.TextBox();
            this.tabRelatedTasks = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.label153 = new System.Windows.Forms.Label();
            this.tbRelatedSourceAddress = new System.Windows.Forms.TextBox();
            this.ddRelatedTask = new System.Windows.Forms.ComboBox();
            this.label154 = new System.Windows.Forms.Label();
            this.tabPrices = new System.Windows.Forms.TabPage();
            this.panelPrices = new System.Windows.Forms.Panel();
            this.tabTierPricing = new System.Windows.Forms.TabPage();
            this.label48 = new System.Windows.Forms.Label();
            this.panelTierPricing = new System.Windows.Forms.Panel();
            this.chDeleteExistingTierPricingOnUpdate = new System.Windows.Forms.CheckBox();
            this.tabTranslations = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.label178 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.label181 = new System.Windows.Forms.Label();
            this.label182 = new System.Windows.Forms.Label();
            this.label183 = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this.label187 = new System.Windows.Forms.Label();
            this.label188 = new System.Windows.Forms.Label();
            this.tbTranslateApiUrl = new System.Windows.Forms.TextBox();
            this.tbTranslateSubscribtionKey = new System.Windows.Forms.TextBox();
            this.label197 = new System.Windows.Forms.Label();
            this.ddTranslateLanguagesList = new System.Windows.Forms.ComboBox();
            this.chTranslateProductName = new System.Windows.Forms.CheckBox();
            this.chTranslateProductShortDesc = new System.Windows.Forms.CheckBox();
            this.chTranslateProductFullDesc = new System.Windows.Forms.CheckBox();
            this.chTranslateProductCategory = new System.Windows.Forms.CheckBox();
            this.chTranslateProductAtt = new System.Windows.Forms.CheckBox();
            this.tabCustomSql = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.cbRunSqlOn = new System.Windows.Forms.ComboBox();
            this.tbSqlQueries = new System.Windows.Forms.TextBox();
            this.tabCustomerCustomAtt = new System.Windows.Forms.TabPage();
            this.panelCustomerCustomAtt = new System.Windows.Forms.Panel();
            this.chDeleteOldCustomerCustomAtt = new System.Windows.Forms.CheckBox();
            this.label54 = new System.Windows.Forms.Label();
            this.tabAddressCustomAtt = new System.Windows.Forms.TabPage();
            this.panelCustomerGenericAtt = new System.Windows.Forms.Panel();
            this.chDeleteOldAddressCustomAtt = new System.Windows.Forms.CheckBox();
            this.label44 = new System.Windows.Forms.Label();
            this.tabProdInfo = new System.Windows.Forms.TabPage();
            this.panelProdInfo = new System.Windows.Forms.Panel();
            this.tabLogSettings = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label108 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.tbEmailTo = new System.Windows.Forms.TextBox();
            this.label106 = new System.Windows.Forms.Label();
            this.tbEmailAddressFrom = new System.Windows.Forms.TextBox();
            this.checkBox_0 = new System.Windows.Forms.CheckBox();
            this.tbEmailDisplayName = new System.Windows.Forms.TextBox();
            this.tbEmailHost = new System.Windows.Forms.TextBox();
            this.tbEmailPort = new System.Windows.Forms.TextBox();
            this.tbEmailUser = new System.Windows.Forms.TextBox();
            this.tbEmailPassword = new System.Windows.Forms.TextBox();
            this.label107 = new System.Windows.Forms.Label();
            this.tbEmailSubject = new System.Windows.Forms.TextBox();
            this.tbEmailBody = new System.Windows.Forms.TextBox();
            this.tabCustomer = new System.Windows.Forms.TabPage();
            this.panelCustomer = new System.Windows.Forms.Panel();
            this.tabCustomerShippingAddress = new System.Windows.Forms.TabPage();
            this.chForceShippingAddressOnUpdate = new System.Windows.Forms.CheckBox();
            this.label46 = new System.Windows.Forms.Label();
            this.panelCustomerShippingAddress = new System.Windows.Forms.Panel();
            this.chCustomerAddress_DeleteOld = new System.Windows.Forms.CheckBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tabCustomerBillingAddress = new System.Windows.Forms.TabPage();
            this.chForceBillingAddressOnUpdate = new System.Windows.Forms.CheckBox();
            this.label47 = new System.Windows.Forms.Label();
            this.panelCustomerBillingAddress = new System.Windows.Forms.Panel();
            this.chCustomerAddress_DeleteOld2 = new System.Windows.Forms.CheckBox();
            this.label45 = new System.Windows.Forms.Label();
            this.tabCustomerRole = new System.Windows.Forms.TabPage();
            this.panelCustomerRole = new System.Windows.Forms.Panel();
            this.chCustomerRole_DeleteOld = new System.Windows.Forms.CheckBox();
            this.label36 = new System.Windows.Forms.Label();
            this.tabCustomerOther = new System.Windows.Forms.TabPage();
            this.panelCustomerOthers = new System.Windows.Forms.Panel();
            this.tabWishList = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.panelWishList = new System.Windows.Forms.Panel();
            this.chDeleteWishListOnUpdate = new System.Windows.Forms.CheckBox();
            this.btnTestMapping = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnRunTask = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.statusArea = new System.Windows.Forms.StatusStrip();
            this.statusAreaText = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnQuickImport = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCopy = new System.Windows.Forms.Button();
            this.xmlModify = new System.Windows.Forms.CheckBox();
            this.tabTask.SuspendLayout();
            this.tabTaskData.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.pnSchedulerTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmMinute)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmInterval)).BeginInit();
            this.tabProductInfo.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabProductSettings.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmProductShippingCharge)).BeginInit();
            this.tabSeo.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tabCategories.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabAccessControl.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tabImages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmPictureResizeHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPictureResizeWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPictureResizeQuality)).BeginInit();
            this.tabProdAttributes.SuspendLayout();
            this.tabSpecAttributes.SuspendLayout();
            this.tabFilters.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tabRelatedTasks.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.tabPrices.SuspendLayout();
            this.tabTierPricing.SuspendLayout();
            this.tabTranslations.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tabCustomSql.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tabCustomerCustomAtt.SuspendLayout();
            this.tabAddressCustomAtt.SuspendLayout();
            this.tabProdInfo.SuspendLayout();
            this.tabLogSettings.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tabCustomer.SuspendLayout();
            this.tabCustomerShippingAddress.SuspendLayout();
            this.tabCustomerBillingAddress.SuspendLayout();
            this.tabCustomerRole.SuspendLayout();
            this.tabCustomerOther.SuspendLayout();
            this.tabWishList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.statusArea.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabTask
            // 
            this.tabTask.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabTask.Controls.Add(this.tabTaskData);
            this.tabTask.Controls.Add(this.tabProductInfo);
            this.tabTask.Controls.Add(this.tabProductSettings);
            this.tabTask.Controls.Add(this.tabSeo);
            this.tabTask.Controls.Add(this.tabCategories);
            this.tabTask.Controls.Add(this.tabAccessControl);
            this.tabTask.Controls.Add(this.tabImages);
            this.tabTask.Controls.Add(this.tabProdAttributes);
            this.tabTask.Controls.Add(this.tabSpecAttributes);
            this.tabTask.Controls.Add(this.tabFilters);
            this.tabTask.Controls.Add(this.tabRelatedTasks);
            this.tabTask.Controls.Add(this.tabPrices);
            this.tabTask.Controls.Add(this.tabTierPricing);
            this.tabTask.Controls.Add(this.tabTranslations);
            this.tabTask.Controls.Add(this.tabCustomSql);
            this.tabTask.Controls.Add(this.tabCustomerCustomAtt);
            this.tabTask.Controls.Add(this.tabAddressCustomAtt);
            this.tabTask.Controls.Add(this.tabProdInfo);
            this.tabTask.Controls.Add(this.tabLogSettings);
            this.tabTask.Controls.Add(this.tabCustomer);
            this.tabTask.Controls.Add(this.tabCustomerShippingAddress);
            this.tabTask.Controls.Add(this.tabCustomerBillingAddress);
            this.tabTask.Controls.Add(this.tabCustomerRole);
            this.tabTask.Controls.Add(this.tabCustomerOther);
            this.tabTask.Controls.Add(this.tabWishList);
            this.tabTask.Location = new System.Drawing.Point(-2, -3);
            this.tabTask.Name = "tabTask";
            this.tabTask.SelectedIndex = 0;
            this.tabTask.Size = new System.Drawing.Size(913, 631);
            this.tabTask.TabIndex = 0;
            // 
            // tabTaskData
            // 
            this.tabTaskData.Controls.Add(this.tableLayoutPanel4);
            this.tabTaskData.Location = new System.Drawing.Point(4, 22);
            this.tabTaskData.Name = "tabTaskData";
            this.tabTaskData.Padding = new System.Windows.Forms.Padding(3);
            this.tabTaskData.Size = new System.Drawing.Size(905, 605);
            this.tabTaskData.TabIndex = 0;
            this.tabTaskData.Text = "Task Data";
            this.tabTaskData.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.AutoScroll = true;
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel4.Controls.Add(this.tbSourceCopyPath, 1, 17);
            this.tableLayoutPanel4.Controls.Add(this.chTitleCase, 1, 16);
            this.tableLayoutPanel4.Controls.Add(this.label77, 0, 13);
            this.tableLayoutPanel4.Controls.Add(this.label65, 0, 10);
            this.tableLayoutPanel4.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.tbTaskName, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.ddStoreWeb, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.ddVendorSourceMap, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.ddTaskStatus, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.ddTaskAction, 1, 5);
            this.tableLayoutPanel4.Controls.Add(this.ddScheduleType, 1, 10);
            this.tableLayoutPanel4.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel4.Controls.Add(this.chDisableShop, 1, 6);
            this.tableLayoutPanel4.Controls.Add(this.chLimitedStore, 1, 7);
            this.tableLayoutPanel4.Controls.Add(this.label8, 0, 7);
            this.tableLayoutPanel4.Controls.Add(this.chUnpublish, 1, 9);
            this.tableLayoutPanel4.Controls.Add(this.label70, 0, 9);
            this.tableLayoutPanel4.Controls.Add(this.chDeleteFileAfterImport, 1, 13);
            this.tableLayoutPanel4.Controls.Add(this.pnSchedulerTime, 1, 12);
            this.tableLayoutPanel4.Controls.Add(this.label69, 0, 12);
            this.tableLayoutPanel4.Controls.Add(this.label82, 0, 14);
            this.tableLayoutPanel4.Controls.Add(this.chPicturesInFiles, 1, 14);
            this.tableLayoutPanel4.Controls.Add(this.panel2, 1, 15);
            this.tableLayoutPanel4.Controls.Add(this.label83, 0, 15);
            this.tableLayoutPanel4.Controls.Add(this.label150, 0, 16);
            this.tableLayoutPanel4.Controls.Add(this.nmInterval, 1, 11);
            this.tableLayoutPanel4.Controls.Add(this.label66, 0, 11);
            this.tableLayoutPanel4.Controls.Add(this.label169, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label177, 0, 17);
            this.tableLayoutPanel4.Controls.Add(this.label192, 0, 8);
            this.tableLayoutPanel4.Controls.Add(this.chLimitedToSoresEntities, 1, 8);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(-1, 2);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 18;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(908, 603);
            this.tableLayoutPanel4.TabIndex = 19;
            // 
            // tbSourceCopyPath
            // 
            this.tbSourceCopyPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSourceCopyPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSourceCopyPath.Location = new System.Drawing.Point(276, 570);
            this.tbSourceCopyPath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbSourceCopyPath.Name = "tbSourceCopyPath";
            this.tbSourceCopyPath.Size = new System.Drawing.Size(611, 20);
            this.tbSourceCopyPath.TabIndex = 49;
            // 
            // chTitleCase
            // 
            this.chTitleCase.AutoSize = true;
            this.chTitleCase.Location = new System.Drawing.Point(276, 543);
            this.chTitleCase.Name = "chTitleCase";
            this.chTitleCase.Size = new System.Drawing.Size(15, 14);
            this.chTitleCase.TabIndex = 46;
            this.chTitleCase.UseVisualStyleBackColor = true;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(4, 412);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(208, 13);
            this.label77.TabIndex = 39;
            this.label77.Text = "Delete File After Import (only for file system)";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(4, 321);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(82, 13);
            this.label65.TabIndex = 28;
            this.label65.Text = "Scheduler Type";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.Location = new System.Drawing.Point(4, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Property Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Name";
            // 
            // tbTaskName
            // 
            this.tbTaskName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTaskName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbTaskName.Location = new System.Drawing.Point(276, 31);
            this.tbTaskName.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbTaskName.Name = "tbTaskName";
            this.tbTaskName.Size = new System.Drawing.Size(611, 20);
            this.tbTaskName.TabIndex = 13;
            this.tbTaskName.Validating += new System.ComponentModel.CancelEventHandler(this.tbTaskName_Validating);
            this.tbTaskName.Validated += new System.EventHandler(this.tbTaskName_Validated);
            // 
            // ddStoreWeb
            // 
            this.ddStoreWeb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddStoreWeb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddStoreWeb.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddStoreWeb.FormattingEnabled = true;
            this.ddStoreWeb.Location = new System.Drawing.Point(276, 58);
            this.ddStoreWeb.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddStoreWeb.Name = "ddStoreWeb";
            this.ddStoreWeb.Size = new System.Drawing.Size(611, 21);
            this.ddStoreWeb.TabIndex = 17;
            this.ddStoreWeb.SelectedIndexChanged += new System.EventHandler(this.ddStoreWeb_SelectedIndexChanged);
            this.ddStoreWeb.Validating += new System.ComponentModel.CancelEventHandler(this.ddStoreWeb_Validating);
            this.ddStoreWeb.Validated += new System.EventHandler(this.ddStoreWeb_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Store";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Vendor Source Mapping";
            // 
            // ddVendorSourceMap
            // 
            this.ddVendorSourceMap.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddVendorSourceMap.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddVendorSourceMap.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddVendorSourceMap.FormattingEnabled = true;
            this.ddVendorSourceMap.Location = new System.Drawing.Point(276, 85);
            this.ddVendorSourceMap.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddVendorSourceMap.Name = "ddVendorSourceMap";
            this.ddVendorSourceMap.Size = new System.Drawing.Size(611, 21);
            this.ddVendorSourceMap.TabIndex = 19;
            this.ddVendorSourceMap.SelectedIndexChanged += new System.EventHandler(this.ddVendorSourceMap_SelectedIndexChanged);
            this.ddVendorSourceMap.Validating += new System.ComponentModel.CancelEventHandler(this.ddVendorSourceMap_Validating);
            this.ddVendorSourceMap.Validated += new System.EventHandler(this.ddVendorSourceMap_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Status";
            // 
            // ddTaskStatus
            // 
            this.ddTaskStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddTaskStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddTaskStatus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddTaskStatus.FormattingEnabled = true;
            this.ddTaskStatus.Location = new System.Drawing.Point(276, 112);
            this.ddTaskStatus.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddTaskStatus.Name = "ddTaskStatus";
            this.ddTaskStatus.Size = new System.Drawing.Size(611, 21);
            this.ddTaskStatus.TabIndex = 21;
            this.ddTaskStatus.Validating += new System.ComponentModel.CancelEventHandler(this.ddTaskStatus_Validating);
            this.ddTaskStatus.Validated += new System.EventHandler(this.ddTaskStatus_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Action";
            // 
            // ddTaskAction
            // 
            this.ddTaskAction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddTaskAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddTaskAction.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddTaskAction.FormattingEnabled = true;
            this.ddTaskAction.Items.AddRange(new object[] {
            "Insert",
            "Update",
            "Insert&Update"});
            this.ddTaskAction.Location = new System.Drawing.Point(276, 139);
            this.ddTaskAction.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddTaskAction.Name = "ddTaskAction";
            this.ddTaskAction.Size = new System.Drawing.Size(611, 21);
            this.ddTaskAction.TabIndex = 23;
            this.ddTaskAction.SelectedIndexChanged += new System.EventHandler(this.ddTaskAction_SelectedIndexChanged);
            this.ddTaskAction.Validating += new System.ComponentModel.CancelEventHandler(this.ddTaskAction_Validating);
            this.ddTaskAction.Validated += new System.EventHandler(this.ddTaskAction_Validated);
            // 
            // ddScheduleType
            // 
            this.ddScheduleType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddScheduleType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddScheduleType.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddScheduleType.FormattingEnabled = true;
            this.ddScheduleType.Items.AddRange(new object[] {
            "Minutely",
            "Hourly",
            "Daily",
            "Manual"});
            this.ddScheduleType.Location = new System.Drawing.Point(276, 324);
            this.ddScheduleType.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddScheduleType.Name = "ddScheduleType";
            this.ddScheduleType.Size = new System.Drawing.Size(611, 21);
            this.ddScheduleType.TabIndex = 29;
            this.ddScheduleType.SelectedIndexChanged += new System.EventHandler(this.ddScheduleType_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 163);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(155, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "Disable Store While Processing";
            // 
            // chDisableShop
            // 
            this.chDisableShop.AutoSize = true;
            this.chDisableShop.Location = new System.Drawing.Point(276, 166);
            this.chDisableShop.Name = "chDisableShop";
            this.chDisableShop.Size = new System.Drawing.Size(15, 14);
            this.chDisableShop.TabIndex = 25;
            this.chDisableShop.UseVisualStyleBackColor = true;
            // 
            // chLimitedStore
            // 
            this.chLimitedStore.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chLimitedStore.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.chLimitedStore.FormattingEnabled = true;
            this.chLimitedStore.Location = new System.Drawing.Point(276, 193);
            this.chLimitedStore.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.chLimitedStore.Name = "chLimitedStore";
            this.chLimitedStore.Size = new System.Drawing.Size(611, 62);
            this.chLimitedStore.TabIndex = 27;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 190);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Limited To Store";
            // 
            // chUnpublish
            // 
            this.chUnpublish.AutoSize = true;
            this.chUnpublish.Location = new System.Drawing.Point(276, 293);
            this.chUnpublish.Name = "chUnpublish";
            this.chUnpublish.Size = new System.Drawing.Size(15, 14);
            this.chUnpublish.TabIndex = 36;
            this.chUnpublish.UseVisualStyleBackColor = true;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(4, 290);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(200, 13);
            this.label70.TabIndex = 37;
            this.label70.Text = "Unpublish products if not found in source";
            // 
            // chDeleteFileAfterImport
            // 
            this.chDeleteFileAfterImport.AutoSize = true;
            this.chDeleteFileAfterImport.Location = new System.Drawing.Point(276, 415);
            this.chDeleteFileAfterImport.Name = "chDeleteFileAfterImport";
            this.chDeleteFileAfterImport.Size = new System.Drawing.Size(15, 14);
            this.chDeleteFileAfterImport.TabIndex = 38;
            this.chDeleteFileAfterImport.UseVisualStyleBackColor = true;
            // 
            // pnSchedulerTime
            // 
            this.pnSchedulerTime.Controls.Add(this.label67);
            this.pnSchedulerTime.Controls.Add(this.nmHour);
            this.pnSchedulerTime.Controls.Add(this.label68);
            this.pnSchedulerTime.Controls.Add(this.nmMinute);
            this.pnSchedulerTime.Location = new System.Drawing.Point(276, 386);
            this.pnSchedulerTime.Name = "pnSchedulerTime";
            this.pnSchedulerTime.Size = new System.Drawing.Size(200, 21);
            this.pnSchedulerTime.TabIndex = 34;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(127, 8);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(25, 13);
            this.label67.TabIndex = 35;
            this.label67.Text = "MM";
            // 
            // nmHour
            // 
            this.nmHour.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmHour.Location = new System.Drawing.Point(1, 2);
            this.nmHour.Name = "nmHour";
            this.nmHour.Size = new System.Drawing.Size(47, 20);
            this.nmHour.TabIndex = 32;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(49, 8);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(23, 13);
            this.label68.TabIndex = 34;
            this.label68.Text = "HH";
            // 
            // nmMinute
            // 
            this.nmMinute.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmMinute.Location = new System.Drawing.Point(75, 3);
            this.nmMinute.Name = "nmMinute";
            this.nmMinute.Size = new System.Drawing.Size(47, 20);
            this.nmMinute.TabIndex = 33;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(4, 383);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(81, 13);
            this.label69.TabIndex = 35;
            this.label69.Text = "Scheduler Time";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(4, 441);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(165, 13);
            this.label82.TabIndex = 40;
            this.label82.Text = "Save Pictures In File System Only";
            // 
            // chPicturesInFiles
            // 
            this.chPicturesInFiles.AutoSize = true;
            this.chPicturesInFiles.Location = new System.Drawing.Point(276, 444);
            this.chPicturesInFiles.Name = "chPicturesInFiles";
            this.chPicturesInFiles.Size = new System.Drawing.Size(15, 14);
            this.chPicturesInFiles.TabIndex = 43;
            this.chPicturesInFiles.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label84);
            this.panel2.Controls.Add(this.tbPicturesPathSave);
            this.panel2.Location = new System.Drawing.Point(276, 473);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(606, 63);
            this.panel2.TabIndex = 44;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(3, 26);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(504, 26);
            this.label84.TabIndex = 43;
            this.label84.Text = "If NopTalk can`t access the store location files system, you must copy manually p" +
    "ictures from this location \r\nto your store images location:  \"..\\storelocation\\C" +
    "ontent\\Images\\\" after import ends.";
            // 
            // tbPicturesPathSave
            // 
            this.tbPicturesPathSave.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPicturesPathSave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbPicturesPathSave.Location = new System.Drawing.Point(3, 3);
            this.tbPicturesPathSave.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbPicturesPathSave.Name = "tbPicturesPathSave";
            this.tbPicturesPathSave.Size = new System.Drawing.Size(595, 20);
            this.tbPicturesPathSave.TabIndex = 15;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(4, 470);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(162, 13);
            this.label83.TabIndex = 42;
            this.label83.Text = "Save Pictures Directory Location";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(3, 540);
            this.label150.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(90, 13);
            this.label150.TabIndex = 45;
            this.label150.Text = "Title Case In Text";
            // 
            // nmInterval
            // 
            this.nmInterval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmInterval.Location = new System.Drawing.Point(276, 355);
            this.nmInterval.Name = "nmInterval";
            this.nmInterval.Size = new System.Drawing.Size(120, 20);
            this.nmInterval.TabIndex = 30;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(4, 352);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(93, 13);
            this.label66.TabIndex = 31;
            this.label66.Text = "Scheduler Interval";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label169.Location = new System.Drawing.Point(275, 1);
            this.label169.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(116, 17);
            this.label169.TabIndex = 47;
            this.label169.Text = "Property Value";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Location = new System.Drawing.Point(4, 567);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(96, 13);
            this.label177.TabIndex = 48;
            this.label177.Text = "Save Source Copy";
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Location = new System.Drawing.Point(3, 263);
            this.label192.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(139, 13);
            this.label192.TabIndex = 50;
            this.label192.Text = "Limited To Stores Assign To";
            // 
            // chLimitedToSoresEntities
            // 
            this.chLimitedToSoresEntities.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chLimitedToSoresEntities.FormattingEnabled = true;
            this.chLimitedToSoresEntities.Location = new System.Drawing.Point(276, 266);
            this.chLimitedToSoresEntities.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.chLimitedToSoresEntities.Name = "chLimitedToSoresEntities";
            this.chLimitedToSoresEntities.Size = new System.Drawing.Size(611, 19);
            this.chLimitedToSoresEntities.TabIndex = 51;
            // 
            // tabProductInfo
            // 
            this.tabProductInfo.Controls.Add(this.tableLayoutPanel1);
            this.tabProductInfo.Location = new System.Drawing.Point(4, 22);
            this.tabProductInfo.Name = "tabProductInfo";
            this.tabProductInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabProductInfo.Size = new System.Drawing.Size(905, 605);
            this.tabProductInfo.TabIndex = 1;
            this.tabProductInfo.Text = "Product Info";
            this.tabProductInfo.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.AutoScroll = true;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Controls.Add(this.label9, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label20, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label21, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label26, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label17, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label18, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label24, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label25, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.chProdIdMap, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.chProdNameMap, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.chProdShortDescMap, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.chProdFullDescMap, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.chProdManPartNumMap, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.chProdStockMap, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.nmStockQuantity, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.tbManufacturerPnum, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.tbProdFullDesc, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.tbProdShortDesc, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.tbProductName, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.chProdNameImport, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.chProdShortDescImport, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.chProdFullDescImport, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.chProdManPartNumImport, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.chProdStockImport, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.chManufacturerMap, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.tbManufacturer, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.chManufacturerImport, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.label79, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label81, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label155, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.label94, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.label95, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.label96, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.chVendorMap, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.chGtinMap, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.chWeightMap, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.chLengthMap, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.chWidthMap, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.chHeightMap, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.tbVendor, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.tbGtin, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.nmWeight, 2, 10);
            this.tableLayoutPanel1.Controls.Add(this.tbLength, 2, 11);
            this.tableLayoutPanel1.Controls.Add(this.tbWidth, 2, 12);
            this.tableLayoutPanel1.Controls.Add(this.tbHeight, 2, 13);
            this.tableLayoutPanel1.Controls.Add(this.chVendorImport, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.chGtinImport, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.chWeightImport, 3, 10);
            this.tableLayoutPanel1.Controls.Add(this.chLengthImport, 3, 11);
            this.tableLayoutPanel1.Controls.Add(this.chWidthImport, 3, 12);
            this.tableLayoutPanel1.Controls.Add(this.chHeightImport, 3, 13);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(-5, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 14;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(914, 607);
            this.tableLayoutPanel1.TabIndex = 1;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(788, 1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 25);
            this.label9.TabIndex = 43;
            this.label9.Text = "Fields To Import";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(4, 1);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(89, 17);
            this.label20.TabIndex = 11;
            this.label20.Text = "Field Name";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(155, 1);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(94, 25);
            this.label21.TabIndex = 12;
            this.label21.Text = "Mapped To Source";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(282, 1);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(199, 17);
            this.label26.TabIndex = 21;
            this.label26.Text = "Default Value /+Add Value";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 27);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Product ID";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 51);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Product Name";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(4, 75);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Short Description";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 99);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(79, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "Full Description";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(4, 149);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(132, 13);
            this.label24.TabIndex = 17;
            this.label24.Text = "Manufacturer Part Number";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(4, 173);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(77, 13);
            this.label25.TabIndex = 19;
            this.label25.Text = "Stock Quantity";
            // 
            // chProdIdMap
            // 
            this.chProdIdMap.AutoSize = true;
            this.chProdIdMap.Enabled = false;
            this.chProdIdMap.Location = new System.Drawing.Point(155, 30);
            this.chProdIdMap.Name = "chProdIdMap";
            this.chProdIdMap.Size = new System.Drawing.Size(15, 14);
            this.chProdIdMap.TabIndex = 35;
            this.chProdIdMap.UseVisualStyleBackColor = true;
            // 
            // chProdNameMap
            // 
            this.chProdNameMap.AutoSize = true;
            this.chProdNameMap.Enabled = false;
            this.chProdNameMap.Location = new System.Drawing.Point(155, 54);
            this.chProdNameMap.Name = "chProdNameMap";
            this.chProdNameMap.Size = new System.Drawing.Size(15, 14);
            this.chProdNameMap.TabIndex = 36;
            this.chProdNameMap.UseVisualStyleBackColor = true;
            // 
            // chProdShortDescMap
            // 
            this.chProdShortDescMap.AutoSize = true;
            this.chProdShortDescMap.Enabled = false;
            this.chProdShortDescMap.Location = new System.Drawing.Point(155, 78);
            this.chProdShortDescMap.Name = "chProdShortDescMap";
            this.chProdShortDescMap.Size = new System.Drawing.Size(15, 14);
            this.chProdShortDescMap.TabIndex = 37;
            this.chProdShortDescMap.UseVisualStyleBackColor = true;
            // 
            // chProdFullDescMap
            // 
            this.chProdFullDescMap.AutoSize = true;
            this.chProdFullDescMap.Enabled = false;
            this.chProdFullDescMap.Location = new System.Drawing.Point(155, 102);
            this.chProdFullDescMap.Name = "chProdFullDescMap";
            this.chProdFullDescMap.Size = new System.Drawing.Size(15, 14);
            this.chProdFullDescMap.TabIndex = 38;
            this.chProdFullDescMap.UseVisualStyleBackColor = true;
            // 
            // chProdManPartNumMap
            // 
            this.chProdManPartNumMap.AutoSize = true;
            this.chProdManPartNumMap.Enabled = false;
            this.chProdManPartNumMap.Location = new System.Drawing.Point(155, 152);
            this.chProdManPartNumMap.Name = "chProdManPartNumMap";
            this.chProdManPartNumMap.Size = new System.Drawing.Size(15, 14);
            this.chProdManPartNumMap.TabIndex = 39;
            this.chProdManPartNumMap.UseVisualStyleBackColor = true;
            // 
            // chProdStockMap
            // 
            this.chProdStockMap.AutoSize = true;
            this.chProdStockMap.Enabled = false;
            this.chProdStockMap.Location = new System.Drawing.Point(155, 176);
            this.chProdStockMap.Name = "chProdStockMap";
            this.chProdStockMap.Size = new System.Drawing.Size(15, 14);
            this.chProdStockMap.TabIndex = 42;
            this.chProdStockMap.UseVisualStyleBackColor = true;
            // 
            // nmStockQuantity
            // 
            this.nmStockQuantity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmStockQuantity.Location = new System.Drawing.Point(282, 176);
            this.nmStockQuantity.Name = "nmStockQuantity";
            this.nmStockQuantity.Size = new System.Drawing.Size(277, 20);
            this.nmStockQuantity.TabIndex = 34;
            // 
            // tbManufacturerPnum
            // 
            this.tbManufacturerPnum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbManufacturerPnum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbManufacturerPnum.Location = new System.Drawing.Point(282, 152);
            this.tbManufacturerPnum.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbManufacturerPnum.Name = "tbManufacturerPnum";
            this.tbManufacturerPnum.Size = new System.Drawing.Size(482, 20);
            this.tbManufacturerPnum.TabIndex = 25;
            // 
            // tbProdFullDesc
            // 
            this.tbProdFullDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdFullDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdFullDesc.Location = new System.Drawing.Point(282, 102);
            this.tbProdFullDesc.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdFullDesc.Multiline = true;
            this.tbProdFullDesc.Name = "tbProdFullDesc";
            this.tbProdFullDesc.Size = new System.Drawing.Size(482, 43);
            this.tbProdFullDesc.TabIndex = 24;
            // 
            // tbProdShortDesc
            // 
            this.tbProdShortDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdShortDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdShortDesc.Location = new System.Drawing.Point(282, 78);
            this.tbProdShortDesc.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdShortDesc.Multiline = true;
            this.tbProdShortDesc.Name = "tbProdShortDesc";
            this.tbProdShortDesc.Size = new System.Drawing.Size(482, 17);
            this.tbProdShortDesc.TabIndex = 23;
            // 
            // tbProductName
            // 
            this.tbProductName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProductName.Location = new System.Drawing.Point(282, 54);
            this.tbProductName.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProductName.Name = "tbProductName";
            this.tbProductName.Size = new System.Drawing.Size(482, 20);
            this.tbProductName.TabIndex = 22;
            // 
            // chProdNameImport
            // 
            this.chProdNameImport.AutoSize = true;
            this.chProdNameImport.Location = new System.Drawing.Point(788, 54);
            this.chProdNameImport.Name = "chProdNameImport";
            this.chProdNameImport.Size = new System.Drawing.Size(15, 14);
            this.chProdNameImport.TabIndex = 44;
            this.chProdNameImport.UseVisualStyleBackColor = true;
            // 
            // chProdShortDescImport
            // 
            this.chProdShortDescImport.AutoSize = true;
            this.chProdShortDescImport.Location = new System.Drawing.Point(788, 78);
            this.chProdShortDescImport.Name = "chProdShortDescImport";
            this.chProdShortDescImport.Size = new System.Drawing.Size(15, 14);
            this.chProdShortDescImport.TabIndex = 47;
            this.chProdShortDescImport.UseVisualStyleBackColor = true;
            // 
            // chProdFullDescImport
            // 
            this.chProdFullDescImport.AutoSize = true;
            this.chProdFullDescImport.Location = new System.Drawing.Point(788, 102);
            this.chProdFullDescImport.Name = "chProdFullDescImport";
            this.chProdFullDescImport.Size = new System.Drawing.Size(15, 14);
            this.chProdFullDescImport.TabIndex = 48;
            this.chProdFullDescImport.UseVisualStyleBackColor = true;
            // 
            // chProdManPartNumImport
            // 
            this.chProdManPartNumImport.AutoSize = true;
            this.chProdManPartNumImport.Location = new System.Drawing.Point(788, 152);
            this.chProdManPartNumImport.Name = "chProdManPartNumImport";
            this.chProdManPartNumImport.Size = new System.Drawing.Size(15, 14);
            this.chProdManPartNumImport.TabIndex = 49;
            this.chProdManPartNumImport.UseVisualStyleBackColor = true;
            // 
            // chProdStockImport
            // 
            this.chProdStockImport.AutoSize = true;
            this.chProdStockImport.Location = new System.Drawing.Point(788, 176);
            this.chProdStockImport.Name = "chProdStockImport";
            this.chProdStockImport.Size = new System.Drawing.Size(15, 14);
            this.chProdStockImport.TabIndex = 52;
            this.chProdStockImport.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 197);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 53;
            this.label10.Text = "Manufacturer";
            // 
            // chManufacturerMap
            // 
            this.chManufacturerMap.AutoSize = true;
            this.chManufacturerMap.Enabled = false;
            this.chManufacturerMap.Location = new System.Drawing.Point(155, 200);
            this.chManufacturerMap.Name = "chManufacturerMap";
            this.chManufacturerMap.Size = new System.Drawing.Size(15, 14);
            this.chManufacturerMap.TabIndex = 54;
            this.chManufacturerMap.UseVisualStyleBackColor = true;
            // 
            // tbManufacturer
            // 
            this.tbManufacturer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbManufacturer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbManufacturer.Location = new System.Drawing.Point(282, 200);
            this.tbManufacturer.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbManufacturer.Name = "tbManufacturer";
            this.tbManufacturer.Size = new System.Drawing.Size(482, 20);
            this.tbManufacturer.TabIndex = 55;
            // 
            // chManufacturerImport
            // 
            this.chManufacturerImport.AutoSize = true;
            this.chManufacturerImport.Location = new System.Drawing.Point(788, 200);
            this.chManufacturerImport.Name = "chManufacturerImport";
            this.chManufacturerImport.Size = new System.Drawing.Size(15, 14);
            this.chManufacturerImport.TabIndex = 56;
            this.chManufacturerImport.UseVisualStyleBackColor = true;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(4, 221);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(41, 13);
            this.label79.TabIndex = 66;
            this.label79.Text = "Vendor";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(4, 245);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(33, 13);
            this.label81.TabIndex = 74;
            this.label81.Text = "GTIN";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(4, 269);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(41, 13);
            this.label155.TabIndex = 75;
            this.label155.Text = "Weight";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(3, 293);
            this.label94.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(40, 13);
            this.label94.TabIndex = 82;
            this.label94.Text = "Length";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(3, 317);
            this.label95.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(35, 13);
            this.label95.TabIndex = 83;
            this.label95.Text = "Width";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(3, 341);
            this.label96.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(38, 13);
            this.label96.TabIndex = 84;
            this.label96.Text = "Height";
            // 
            // chVendorMap
            // 
            this.chVendorMap.AutoSize = true;
            this.chVendorMap.Enabled = false;
            this.chVendorMap.Location = new System.Drawing.Point(155, 224);
            this.chVendorMap.Name = "chVendorMap";
            this.chVendorMap.Size = new System.Drawing.Size(15, 14);
            this.chVendorMap.TabIndex = 69;
            this.chVendorMap.UseVisualStyleBackColor = true;
            // 
            // chGtinMap
            // 
            this.chGtinMap.AutoSize = true;
            this.chGtinMap.Enabled = false;
            this.chGtinMap.Location = new System.Drawing.Point(155, 248);
            this.chGtinMap.Name = "chGtinMap";
            this.chGtinMap.Size = new System.Drawing.Size(15, 14);
            this.chGtinMap.TabIndex = 79;
            this.chGtinMap.UseVisualStyleBackColor = true;
            // 
            // chWeightMap
            // 
            this.chWeightMap.AutoSize = true;
            this.chWeightMap.Enabled = false;
            this.chWeightMap.Location = new System.Drawing.Point(155, 272);
            this.chWeightMap.Name = "chWeightMap";
            this.chWeightMap.Size = new System.Drawing.Size(15, 14);
            this.chWeightMap.TabIndex = 78;
            this.chWeightMap.UseVisualStyleBackColor = true;
            // 
            // chLengthMap
            // 
            this.chLengthMap.AutoSize = true;
            this.chLengthMap.Enabled = false;
            this.chLengthMap.Location = new System.Drawing.Point(155, 296);
            this.chLengthMap.Name = "chLengthMap";
            this.chLengthMap.Size = new System.Drawing.Size(15, 14);
            this.chLengthMap.TabIndex = 85;
            this.chLengthMap.UseVisualStyleBackColor = true;
            // 
            // chWidthMap
            // 
            this.chWidthMap.AutoSize = true;
            this.chWidthMap.Enabled = false;
            this.chWidthMap.Location = new System.Drawing.Point(155, 320);
            this.chWidthMap.Name = "chWidthMap";
            this.chWidthMap.Size = new System.Drawing.Size(15, 14);
            this.chWidthMap.TabIndex = 86;
            this.chWidthMap.UseVisualStyleBackColor = true;
            // 
            // chHeightMap
            // 
            this.chHeightMap.AutoSize = true;
            this.chHeightMap.Enabled = false;
            this.chHeightMap.Location = new System.Drawing.Point(155, 344);
            this.chHeightMap.Name = "chHeightMap";
            this.chHeightMap.Size = new System.Drawing.Size(15, 14);
            this.chHeightMap.TabIndex = 87;
            this.chHeightMap.UseVisualStyleBackColor = true;
            // 
            // tbVendor
            // 
            this.tbVendor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbVendor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbVendor.Location = new System.Drawing.Point(282, 224);
            this.tbVendor.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbVendor.Name = "tbVendor";
            this.tbVendor.Size = new System.Drawing.Size(482, 20);
            this.tbVendor.TabIndex = 67;
            // 
            // tbGtin
            // 
            this.tbGtin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbGtin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbGtin.Location = new System.Drawing.Point(282, 248);
            this.tbGtin.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbGtin.Name = "tbGtin";
            this.tbGtin.Size = new System.Drawing.Size(482, 20);
            this.tbGtin.TabIndex = 76;
            // 
            // nmWeight
            // 
            this.nmWeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmWeight.Location = new System.Drawing.Point(282, 272);
            this.nmWeight.Name = "nmWeight";
            this.nmWeight.Size = new System.Drawing.Size(277, 20);
            this.nmWeight.TabIndex = 77;
            // 
            // tbLength
            // 
            this.tbLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbLength.Location = new System.Drawing.Point(282, 296);
            this.tbLength.Name = "tbLength";
            this.tbLength.Size = new System.Drawing.Size(277, 20);
            this.tbLength.TabIndex = 88;
            // 
            // tbWidth
            // 
            this.tbWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbWidth.Location = new System.Drawing.Point(282, 320);
            this.tbWidth.Name = "tbWidth";
            this.tbWidth.Size = new System.Drawing.Size(277, 20);
            this.tbWidth.TabIndex = 89;
            // 
            // tbHeight
            // 
            this.tbHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbHeight.Location = new System.Drawing.Point(282, 344);
            this.tbHeight.Name = "tbHeight";
            this.tbHeight.Size = new System.Drawing.Size(277, 20);
            this.tbHeight.TabIndex = 90;
            // 
            // chVendorImport
            // 
            this.chVendorImport.AutoSize = true;
            this.chVendorImport.Location = new System.Drawing.Point(788, 224);
            this.chVendorImport.Name = "chVendorImport";
            this.chVendorImport.Size = new System.Drawing.Size(15, 14);
            this.chVendorImport.TabIndex = 68;
            this.chVendorImport.UseVisualStyleBackColor = true;
            // 
            // chGtinImport
            // 
            this.chGtinImport.AutoSize = true;
            this.chGtinImport.Location = new System.Drawing.Point(788, 248);
            this.chGtinImport.Name = "chGtinImport";
            this.chGtinImport.Size = new System.Drawing.Size(15, 14);
            this.chGtinImport.TabIndex = 81;
            this.chGtinImport.UseVisualStyleBackColor = true;
            // 
            // chWeightImport
            // 
            this.chWeightImport.AutoSize = true;
            this.chWeightImport.Location = new System.Drawing.Point(788, 272);
            this.chWeightImport.Name = "chWeightImport";
            this.chWeightImport.Size = new System.Drawing.Size(15, 14);
            this.chWeightImport.TabIndex = 80;
            this.chWeightImport.UseVisualStyleBackColor = true;
            // 
            // chLengthImport
            // 
            this.chLengthImport.AutoSize = true;
            this.chLengthImport.Location = new System.Drawing.Point(787, 295);
            this.chLengthImport.Margin = new System.Windows.Forms.Padding(2);
            this.chLengthImport.Name = "chLengthImport";
            this.chLengthImport.Size = new System.Drawing.Size(15, 14);
            this.chLengthImport.TabIndex = 93;
            this.chLengthImport.UseVisualStyleBackColor = true;
            // 
            // chWidthImport
            // 
            this.chWidthImport.AutoSize = true;
            this.chWidthImport.Location = new System.Drawing.Point(787, 319);
            this.chWidthImport.Margin = new System.Windows.Forms.Padding(2);
            this.chWidthImport.Name = "chWidthImport";
            this.chWidthImport.Size = new System.Drawing.Size(15, 14);
            this.chWidthImport.TabIndex = 92;
            this.chWidthImport.UseVisualStyleBackColor = true;
            // 
            // chHeightImport
            // 
            this.chHeightImport.AutoSize = true;
            this.chHeightImport.Location = new System.Drawing.Point(788, 344);
            this.chHeightImport.Name = "chHeightImport";
            this.chHeightImport.Size = new System.Drawing.Size(15, 14);
            this.chHeightImport.TabIndex = 91;
            this.chHeightImport.UseVisualStyleBackColor = true;
            // 
            // tabProductSettings
            // 
            this.tabProductSettings.Controls.Add(this.tableLayoutPanel9);
            this.tabProductSettings.Location = new System.Drawing.Point(4, 22);
            this.tabProductSettings.Margin = new System.Windows.Forms.Padding(2);
            this.tabProductSettings.Name = "tabProductSettings";
            this.tabProductSettings.Size = new System.Drawing.Size(905, 605);
            this.tabProductSettings.TabIndex = 9;
            this.tabProductSettings.Text = "Product Settings";
            this.tabProductSettings.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel9.AutoScroll = true;
            this.tableLayoutPanel9.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel9.ColumnCount = 4;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.07692F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.69231F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.23077F));
            this.tableLayoutPanel9.Controls.Add(this.label176, 0, 21);
            this.tableLayoutPanel9.Controls.Add(this.label175, 0, 20);
            this.tableLayoutPanel9.Controls.Add(this.label71, 0, 4);
            this.tableLayoutPanel9.Controls.Add(this.label101, 3, 0);
            this.tableLayoutPanel9.Controls.Add(this.label102, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.label103, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.label104, 2, 0);
            this.tableLayoutPanel9.Controls.Add(this.label118, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.chProductPublishImport, 3, 1);
            this.tableLayoutPanel9.Controls.Add(this.label117, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.ddDeliveryDate, 2, 2);
            this.tableLayoutPanel9.Controls.Add(this.chDayDeliveryImport, 3, 2);
            this.tableLayoutPanel9.Controls.Add(this.label119, 0, 3);
            this.tableLayoutPanel9.Controls.Add(this.chInventoryMethodImport, 3, 3);
            this.tableLayoutPanel9.Controls.Add(this.chProductPublish, 2, 1);
            this.tableLayoutPanel9.Controls.Add(this.chProductReviewEnable, 2, 4);
            this.tableLayoutPanel9.Controls.Add(this.chProductReviewEnableImport, 3, 4);
            this.tableLayoutPanel9.Controls.Add(this.label140, 0, 5);
            this.tableLayoutPanel9.Controls.Add(this.label141, 0, 6);
            this.tableLayoutPanel9.Controls.Add(this.label142, 0, 7);
            this.tableLayoutPanel9.Controls.Add(this.label143, 0, 8);
            this.tableLayoutPanel9.Controls.Add(this.label144, 0, 9);
            this.tableLayoutPanel9.Controls.Add(this.chProductDisplayAvailability, 2, 5);
            this.tableLayoutPanel9.Controls.Add(this.chProductDisplayAvailabilityImport, 3, 5);
            this.tableLayoutPanel9.Controls.Add(this.tbProductMinStockQty, 2, 6);
            this.tableLayoutPanel9.Controls.Add(this.tbProductNotifyQty, 2, 7);
            this.tableLayoutPanel9.Controls.Add(this.tbProductMinCartQty, 2, 8);
            this.tableLayoutPanel9.Controls.Add(this.tbProductMaxCartQty, 2, 9);
            this.tableLayoutPanel9.Controls.Add(this.tbProductAllowQty, 2, 10);
            this.tableLayoutPanel9.Controls.Add(this.chProductMinStockQtyAction, 3, 6);
            this.tableLayoutPanel9.Controls.Add(this.chProductNotifyQtyAction, 3, 7);
            this.tableLayoutPanel9.Controls.Add(this.chProductMinCartQtyAction, 3, 8);
            this.tableLayoutPanel9.Controls.Add(this.chProductMaxCartQtyAction, 3, 9);
            this.tableLayoutPanel9.Controls.Add(this.chProductAllowQtyAction, 3, 10);
            this.tableLayoutPanel9.Controls.Add(this.chProductMinStockQtyMap, 1, 6);
            this.tableLayoutPanel9.Controls.Add(this.chProductNotifyQtyMap, 1, 7);
            this.tableLayoutPanel9.Controls.Add(this.chProductMinCartQtyMap, 1, 8);
            this.tableLayoutPanel9.Controls.Add(this.chProductMaxCartQtyMap, 1, 9);
            this.tableLayoutPanel9.Controls.Add(this.chProductAllowQtyMap, 1, 10);
            this.tableLayoutPanel9.Controls.Add(this.label145, 0, 10);
            this.tableLayoutPanel9.Controls.Add(this.label146, 0, 11);
            this.tableLayoutPanel9.Controls.Add(this.chProductDisplayStockQuantity, 2, 11);
            this.tableLayoutPanel9.Controls.Add(this.chProductDisplayStockQuantityImport, 3, 11);
            this.tableLayoutPanel9.Controls.Add(this.label149, 0, 13);
            this.tableLayoutPanel9.Controls.Add(this.ddlBackorderMode, 2, 13);
            this.tableLayoutPanel9.Controls.Add(this.chBackorderModeImport, 3, 13);
            this.tableLayoutPanel9.Controls.Add(this.chDayDeliveryMap, 1, 2);
            this.tableLayoutPanel9.Controls.Add(this.label157, 0, 14);
            this.tableLayoutPanel9.Controls.Add(this.label158, 0, 15);
            this.tableLayoutPanel9.Controls.Add(this.label159, 0, 16);
            this.tableLayoutPanel9.Controls.Add(this.label160, 0, 17);
            this.tableLayoutPanel9.Controls.Add(this.chProductShippingEnabledMap, 1, 14);
            this.tableLayoutPanel9.Controls.Add(this.chProductFreeShippingMap, 1, 15);
            this.tableLayoutPanel9.Controls.Add(this.chProductShipSeparatelyMap, 1, 16);
            this.tableLayoutPanel9.Controls.Add(this.chProductShippingChargeMap, 1, 17);
            this.tableLayoutPanel9.Controls.Add(this.chProductShippingEnabled, 2, 14);
            this.tableLayoutPanel9.Controls.Add(this.chProductFreeShipping, 2, 15);
            this.tableLayoutPanel9.Controls.Add(this.chProductShipSeparately, 2, 16);
            this.tableLayoutPanel9.Controls.Add(this.chProductShippingEnabledImport, 3, 14);
            this.tableLayoutPanel9.Controls.Add(this.chProductFreeShippingImport, 3, 15);
            this.tableLayoutPanel9.Controls.Add(this.chProductShipSeparatelyImport, 3, 16);
            this.tableLayoutPanel9.Controls.Add(this.chProductShippingChargeImport, 3, 17);
            this.tableLayoutPanel9.Controls.Add(this.nmProductShippingCharge, 2, 17);
            this.tableLayoutPanel9.Controls.Add(this.ddInventoryMethod, 2, 3);
            this.tableLayoutPanel9.Controls.Add(this.label194, 0, 18);
            this.tableLayoutPanel9.Controls.Add(this.ddLowStockActivity, 2, 18);
            this.tableLayoutPanel9.Controls.Add(this.chLowStockActivityImport, 3, 18);
            this.tableLayoutPanel9.Controls.Add(this.label174, 0, 19);
            this.tableLayoutPanel9.Controls.Add(this.chVisibleIndividually, 2, 19);
            this.tableLayoutPanel9.Controls.Add(this.chVisibleIndividuallyImport, 3, 19);
            this.tableLayoutPanel9.Controls.Add(this.ddWarehouse, 2, 20);
            this.tableLayoutPanel9.Controls.Add(this.chWarehouseImport, 3, 20);
            this.tableLayoutPanel9.Controls.Add(this.ddProductType, 2, 21);
            this.tableLayoutPanel9.Controls.Add(this.chProductTypeImport, 3, 21);
            this.tableLayoutPanel9.Controls.Add(this.chVisibleIndividuallyMap, 1, 19);
            this.tableLayoutPanel9.Controls.Add(this.chProductIsDeletedMap, 1, 22);
            this.tableLayoutPanel9.Controls.Add(this.label57, 0, 22);
            this.tableLayoutPanel9.Controls.Add(this.chProductIsDeleted, 2, 22);
            this.tableLayoutPanel9.Controls.Add(this.chProductIsDeletedImport, 3, 22);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 23;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(901, 606);
            this.tableLayoutPanel9.TabIndex = 2;
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(4, 518);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(71, 13);
            this.label176.TabIndex = 128;
            this.label176.Text = "Product Type";
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(4, 491);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(62, 13);
            this.label175.TabIndex = 125;
            this.label175.Text = "Warehouse";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(4, 100);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(123, 13);
            this.label71.TabIndex = 66;
            this.label71.Text = "Allow Customer Reviews";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.Location = new System.Drawing.Point(759, 1);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(125, 17);
            this.label101.TabIndex = 43;
            this.label101.Text = "Fields To Import";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.Location = new System.Drawing.Point(4, 1);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(89, 17);
            this.label102.TabIndex = 11;
            this.label102.Text = "Field Name";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.Location = new System.Drawing.Point(155, 1);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(145, 17);
            this.label103.TabIndex = 12;
            this.label103.Text = "Mapped To Source";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.Location = new System.Drawing.Point(328, 1);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(199, 17);
            this.label104.TabIndex = 21;
            this.label104.Text = "Default Value /+Add Value";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(4, 23);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(41, 13);
            this.label118.TabIndex = 57;
            this.label118.Text = "Publish";
            // 
            // chProductPublishImport
            // 
            this.chProductPublishImport.AutoSize = true;
            this.chProductPublishImport.Location = new System.Drawing.Point(759, 26);
            this.chProductPublishImport.Name = "chProductPublishImport";
            this.chProductPublishImport.Size = new System.Drawing.Size(15, 14);
            this.chProductPublishImport.TabIndex = 65;
            this.chProductPublishImport.UseVisualStyleBackColor = true;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(4, 46);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(69, 13);
            this.label117.TabIndex = 59;
            this.label117.Text = "Delivery date";
            // 
            // ddDeliveryDate
            // 
            this.ddDeliveryDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddDeliveryDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddDeliveryDate.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddDeliveryDate.FormattingEnabled = true;
            this.ddDeliveryDate.Location = new System.Drawing.Point(328, 49);
            this.ddDeliveryDate.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddDeliveryDate.Name = "ddDeliveryDate";
            this.ddDeliveryDate.Size = new System.Drawing.Size(407, 21);
            this.ddDeliveryDate.TabIndex = 60;
            // 
            // chDayDeliveryImport
            // 
            this.chDayDeliveryImport.AutoSize = true;
            this.chDayDeliveryImport.Location = new System.Drawing.Point(759, 49);
            this.chDayDeliveryImport.Name = "chDayDeliveryImport";
            this.chDayDeliveryImport.Size = new System.Drawing.Size(15, 14);
            this.chDayDeliveryImport.TabIndex = 63;
            this.chDayDeliveryImport.UseVisualStyleBackColor = true;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(4, 73);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(130, 13);
            this.label119.TabIndex = 61;
            this.label119.Text = "Manage inventory method";
            // 
            // chInventoryMethodImport
            // 
            this.chInventoryMethodImport.AutoSize = true;
            this.chInventoryMethodImport.Location = new System.Drawing.Point(759, 76);
            this.chInventoryMethodImport.Name = "chInventoryMethodImport";
            this.chInventoryMethodImport.Size = new System.Drawing.Size(15, 14);
            this.chInventoryMethodImport.TabIndex = 64;
            this.chInventoryMethodImport.UseVisualStyleBackColor = true;
            // 
            // chProductPublish
            // 
            this.chProductPublish.AutoSize = true;
            this.chProductPublish.Location = new System.Drawing.Point(328, 26);
            this.chProductPublish.Name = "chProductPublish";
            this.chProductPublish.Size = new System.Drawing.Size(15, 14);
            this.chProductPublish.TabIndex = 58;
            this.chProductPublish.UseVisualStyleBackColor = true;
            // 
            // chProductReviewEnable
            // 
            this.chProductReviewEnable.AutoSize = true;
            this.chProductReviewEnable.Location = new System.Drawing.Point(328, 103);
            this.chProductReviewEnable.Name = "chProductReviewEnable";
            this.chProductReviewEnable.Size = new System.Drawing.Size(15, 14);
            this.chProductReviewEnable.TabIndex = 67;
            this.chProductReviewEnable.UseVisualStyleBackColor = true;
            // 
            // chProductReviewEnableImport
            // 
            this.chProductReviewEnableImport.AutoSize = true;
            this.chProductReviewEnableImport.Location = new System.Drawing.Point(759, 103);
            this.chProductReviewEnableImport.Name = "chProductReviewEnableImport";
            this.chProductReviewEnableImport.Size = new System.Drawing.Size(15, 14);
            this.chProductReviewEnableImport.TabIndex = 68;
            this.chProductReviewEnableImport.UseVisualStyleBackColor = true;
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(4, 122);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(92, 13);
            this.label140.TabIndex = 69;
            this.label140.Text = "Display availability";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(4, 144);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(94, 13);
            this.label141.TabIndex = 70;
            this.label141.Text = "Minimum stock qty";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(4, 171);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(97, 13);
            this.label142.TabIndex = 71;
            this.label142.Text = "Notify for qty below";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(4, 198);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(86, 13);
            this.label143.TabIndex = 72;
            this.label143.Text = "Minimum cart qty";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(4, 225);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(89, 13);
            this.label144.TabIndex = 73;
            this.label144.Text = "Maximum cart qty";
            // 
            // chProductDisplayAvailability
            // 
            this.chProductDisplayAvailability.AutoSize = true;
            this.chProductDisplayAvailability.Location = new System.Drawing.Point(328, 125);
            this.chProductDisplayAvailability.Name = "chProductDisplayAvailability";
            this.chProductDisplayAvailability.Size = new System.Drawing.Size(15, 14);
            this.chProductDisplayAvailability.TabIndex = 75;
            this.chProductDisplayAvailability.UseVisualStyleBackColor = true;
            // 
            // chProductDisplayAvailabilityImport
            // 
            this.chProductDisplayAvailabilityImport.AutoSize = true;
            this.chProductDisplayAvailabilityImport.Location = new System.Drawing.Point(759, 125);
            this.chProductDisplayAvailabilityImport.Name = "chProductDisplayAvailabilityImport";
            this.chProductDisplayAvailabilityImport.Size = new System.Drawing.Size(15, 14);
            this.chProductDisplayAvailabilityImport.TabIndex = 76;
            this.chProductDisplayAvailabilityImport.UseVisualStyleBackColor = true;
            // 
            // tbProductMinStockQty
            // 
            this.tbProductMinStockQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductMinStockQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProductMinStockQty.Location = new System.Drawing.Point(328, 147);
            this.tbProductMinStockQty.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProductMinStockQty.Name = "tbProductMinStockQty";
            this.tbProductMinStockQty.Size = new System.Drawing.Size(407, 20);
            this.tbProductMinStockQty.TabIndex = 77;
            // 
            // tbProductNotifyQty
            // 
            this.tbProductNotifyQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductNotifyQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProductNotifyQty.Location = new System.Drawing.Point(328, 174);
            this.tbProductNotifyQty.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProductNotifyQty.Name = "tbProductNotifyQty";
            this.tbProductNotifyQty.Size = new System.Drawing.Size(407, 20);
            this.tbProductNotifyQty.TabIndex = 78;
            // 
            // tbProductMinCartQty
            // 
            this.tbProductMinCartQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductMinCartQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProductMinCartQty.Location = new System.Drawing.Point(328, 201);
            this.tbProductMinCartQty.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProductMinCartQty.Name = "tbProductMinCartQty";
            this.tbProductMinCartQty.Size = new System.Drawing.Size(407, 20);
            this.tbProductMinCartQty.TabIndex = 79;
            // 
            // tbProductMaxCartQty
            // 
            this.tbProductMaxCartQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductMaxCartQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProductMaxCartQty.Location = new System.Drawing.Point(328, 228);
            this.tbProductMaxCartQty.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProductMaxCartQty.Name = "tbProductMaxCartQty";
            this.tbProductMaxCartQty.Size = new System.Drawing.Size(407, 20);
            this.tbProductMaxCartQty.TabIndex = 80;
            // 
            // tbProductAllowQty
            // 
            this.tbProductAllowQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductAllowQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProductAllowQty.Location = new System.Drawing.Point(328, 255);
            this.tbProductAllowQty.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProductAllowQty.Name = "tbProductAllowQty";
            this.tbProductAllowQty.Size = new System.Drawing.Size(407, 20);
            this.tbProductAllowQty.TabIndex = 81;
            // 
            // chProductMinStockQtyAction
            // 
            this.chProductMinStockQtyAction.AutoSize = true;
            this.chProductMinStockQtyAction.Location = new System.Drawing.Point(759, 147);
            this.chProductMinStockQtyAction.Name = "chProductMinStockQtyAction";
            this.chProductMinStockQtyAction.Size = new System.Drawing.Size(15, 14);
            this.chProductMinStockQtyAction.TabIndex = 82;
            this.chProductMinStockQtyAction.UseVisualStyleBackColor = true;
            // 
            // chProductNotifyQtyAction
            // 
            this.chProductNotifyQtyAction.AutoSize = true;
            this.chProductNotifyQtyAction.Location = new System.Drawing.Point(759, 174);
            this.chProductNotifyQtyAction.Name = "chProductNotifyQtyAction";
            this.chProductNotifyQtyAction.Size = new System.Drawing.Size(15, 14);
            this.chProductNotifyQtyAction.TabIndex = 83;
            this.chProductNotifyQtyAction.UseVisualStyleBackColor = true;
            // 
            // chProductMinCartQtyAction
            // 
            this.chProductMinCartQtyAction.AutoSize = true;
            this.chProductMinCartQtyAction.Location = new System.Drawing.Point(759, 201);
            this.chProductMinCartQtyAction.Name = "chProductMinCartQtyAction";
            this.chProductMinCartQtyAction.Size = new System.Drawing.Size(15, 14);
            this.chProductMinCartQtyAction.TabIndex = 84;
            this.chProductMinCartQtyAction.UseVisualStyleBackColor = true;
            // 
            // chProductMaxCartQtyAction
            // 
            this.chProductMaxCartQtyAction.AutoSize = true;
            this.chProductMaxCartQtyAction.Location = new System.Drawing.Point(759, 228);
            this.chProductMaxCartQtyAction.Name = "chProductMaxCartQtyAction";
            this.chProductMaxCartQtyAction.Size = new System.Drawing.Size(15, 14);
            this.chProductMaxCartQtyAction.TabIndex = 85;
            this.chProductMaxCartQtyAction.UseVisualStyleBackColor = true;
            // 
            // chProductAllowQtyAction
            // 
            this.chProductAllowQtyAction.AutoSize = true;
            this.chProductAllowQtyAction.Location = new System.Drawing.Point(759, 255);
            this.chProductAllowQtyAction.Name = "chProductAllowQtyAction";
            this.chProductAllowQtyAction.Size = new System.Drawing.Size(15, 14);
            this.chProductAllowQtyAction.TabIndex = 86;
            this.chProductAllowQtyAction.UseVisualStyleBackColor = true;
            // 
            // chProductMinStockQtyMap
            // 
            this.chProductMinStockQtyMap.AutoSize = true;
            this.chProductMinStockQtyMap.Enabled = false;
            this.chProductMinStockQtyMap.Location = new System.Drawing.Point(155, 147);
            this.chProductMinStockQtyMap.Name = "chProductMinStockQtyMap";
            this.chProductMinStockQtyMap.Size = new System.Drawing.Size(15, 14);
            this.chProductMinStockQtyMap.TabIndex = 87;
            this.chProductMinStockQtyMap.UseVisualStyleBackColor = true;
            // 
            // chProductNotifyQtyMap
            // 
            this.chProductNotifyQtyMap.AutoSize = true;
            this.chProductNotifyQtyMap.Enabled = false;
            this.chProductNotifyQtyMap.Location = new System.Drawing.Point(155, 174);
            this.chProductNotifyQtyMap.Name = "chProductNotifyQtyMap";
            this.chProductNotifyQtyMap.Size = new System.Drawing.Size(15, 14);
            this.chProductNotifyQtyMap.TabIndex = 88;
            this.chProductNotifyQtyMap.UseVisualStyleBackColor = true;
            // 
            // chProductMinCartQtyMap
            // 
            this.chProductMinCartQtyMap.AutoSize = true;
            this.chProductMinCartQtyMap.Enabled = false;
            this.chProductMinCartQtyMap.Location = new System.Drawing.Point(155, 201);
            this.chProductMinCartQtyMap.Name = "chProductMinCartQtyMap";
            this.chProductMinCartQtyMap.Size = new System.Drawing.Size(15, 14);
            this.chProductMinCartQtyMap.TabIndex = 89;
            this.chProductMinCartQtyMap.UseVisualStyleBackColor = true;
            // 
            // chProductMaxCartQtyMap
            // 
            this.chProductMaxCartQtyMap.AutoSize = true;
            this.chProductMaxCartQtyMap.Enabled = false;
            this.chProductMaxCartQtyMap.Location = new System.Drawing.Point(155, 228);
            this.chProductMaxCartQtyMap.Name = "chProductMaxCartQtyMap";
            this.chProductMaxCartQtyMap.Size = new System.Drawing.Size(15, 14);
            this.chProductMaxCartQtyMap.TabIndex = 90;
            this.chProductMaxCartQtyMap.UseVisualStyleBackColor = true;
            // 
            // chProductAllowQtyMap
            // 
            this.chProductAllowQtyMap.AutoSize = true;
            this.chProductAllowQtyMap.Enabled = false;
            this.chProductAllowQtyMap.Location = new System.Drawing.Point(155, 255);
            this.chProductAllowQtyMap.Name = "chProductAllowQtyMap";
            this.chProductAllowQtyMap.Size = new System.Drawing.Size(15, 14);
            this.chProductAllowQtyMap.TabIndex = 91;
            this.chProductAllowQtyMap.UseVisualStyleBackColor = true;
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(4, 252);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(92, 13);
            this.label145.TabIndex = 74;
            this.label145.Text = "Allowed quantities";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(4, 275);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(114, 13);
            this.label146.TabIndex = 92;
            this.label146.Text = "Display Stock Quantity";
            // 
            // chProductDisplayStockQuantity
            // 
            this.chProductDisplayStockQuantity.AutoSize = true;
            this.chProductDisplayStockQuantity.Location = new System.Drawing.Point(328, 278);
            this.chProductDisplayStockQuantity.Name = "chProductDisplayStockQuantity";
            this.chProductDisplayStockQuantity.Size = new System.Drawing.Size(15, 14);
            this.chProductDisplayStockQuantity.TabIndex = 93;
            this.chProductDisplayStockQuantity.UseVisualStyleBackColor = true;
            // 
            // chProductDisplayStockQuantityImport
            // 
            this.chProductDisplayStockQuantityImport.AutoSize = true;
            this.chProductDisplayStockQuantityImport.Location = new System.Drawing.Point(759, 278);
            this.chProductDisplayStockQuantityImport.Name = "chProductDisplayStockQuantityImport";
            this.chProductDisplayStockQuantityImport.Size = new System.Drawing.Size(15, 14);
            this.chProductDisplayStockQuantityImport.TabIndex = 94;
            this.chProductDisplayStockQuantityImport.UseVisualStyleBackColor = true;
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(4, 321);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(86, 13);
            this.label149.TabIndex = 98;
            this.label149.Text = "Backorder Mode";
            // 
            // ddlBackorderMode
            // 
            this.ddlBackorderMode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddlBackorderMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlBackorderMode.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddlBackorderMode.FormattingEnabled = true;
            this.ddlBackorderMode.Location = new System.Drawing.Point(328, 324);
            this.ddlBackorderMode.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddlBackorderMode.Name = "ddlBackorderMode";
            this.ddlBackorderMode.Size = new System.Drawing.Size(407, 21);
            this.ddlBackorderMode.TabIndex = 99;
            // 
            // chBackorderModeImport
            // 
            this.chBackorderModeImport.AutoSize = true;
            this.chBackorderModeImport.Location = new System.Drawing.Point(759, 324);
            this.chBackorderModeImport.Name = "chBackorderModeImport";
            this.chBackorderModeImport.Size = new System.Drawing.Size(15, 14);
            this.chBackorderModeImport.TabIndex = 100;
            this.chBackorderModeImport.UseVisualStyleBackColor = true;
            // 
            // chDayDeliveryMap
            // 
            this.chDayDeliveryMap.AutoSize = true;
            this.chDayDeliveryMap.Enabled = false;
            this.chDayDeliveryMap.Location = new System.Drawing.Point(155, 49);
            this.chDayDeliveryMap.Name = "chDayDeliveryMap";
            this.chDayDeliveryMap.Size = new System.Drawing.Size(15, 14);
            this.chDayDeliveryMap.TabIndex = 101;
            this.chDayDeliveryMap.UseVisualStyleBackColor = true;
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(3, 348);
            this.label157.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(89, 13);
            this.label157.TabIndex = 102;
            this.label157.Text = "Shipping enabled";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(3, 371);
            this.label158.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(72, 13);
            this.label158.TabIndex = 103;
            this.label158.Text = "Free Shipping";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Location = new System.Drawing.Point(3, 394);
            this.label159.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(81, 13);
            this.label159.TabIndex = 104;
            this.label159.Text = "Ship Separately";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(3, 417);
            this.label160.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(134, 13);
            this.label160.TabIndex = 105;
            this.label160.Text = "Additional Shipping Charge";
            // 
            // chProductShippingEnabledMap
            // 
            this.chProductShippingEnabledMap.AutoSize = true;
            this.chProductShippingEnabledMap.Enabled = false;
            this.chProductShippingEnabledMap.Location = new System.Drawing.Point(155, 351);
            this.chProductShippingEnabledMap.Name = "chProductShippingEnabledMap";
            this.chProductShippingEnabledMap.Size = new System.Drawing.Size(15, 14);
            this.chProductShippingEnabledMap.TabIndex = 106;
            this.chProductShippingEnabledMap.UseVisualStyleBackColor = true;
            // 
            // chProductFreeShippingMap
            // 
            this.chProductFreeShippingMap.AutoSize = true;
            this.chProductFreeShippingMap.Enabled = false;
            this.chProductFreeShippingMap.Location = new System.Drawing.Point(155, 374);
            this.chProductFreeShippingMap.Name = "chProductFreeShippingMap";
            this.chProductFreeShippingMap.Size = new System.Drawing.Size(15, 14);
            this.chProductFreeShippingMap.TabIndex = 107;
            this.chProductFreeShippingMap.UseVisualStyleBackColor = true;
            // 
            // chProductShipSeparatelyMap
            // 
            this.chProductShipSeparatelyMap.AutoSize = true;
            this.chProductShipSeparatelyMap.Enabled = false;
            this.chProductShipSeparatelyMap.Location = new System.Drawing.Point(155, 397);
            this.chProductShipSeparatelyMap.Name = "chProductShipSeparatelyMap";
            this.chProductShipSeparatelyMap.Size = new System.Drawing.Size(15, 14);
            this.chProductShipSeparatelyMap.TabIndex = 108;
            this.chProductShipSeparatelyMap.UseVisualStyleBackColor = true;
            // 
            // chProductShippingChargeMap
            // 
            this.chProductShippingChargeMap.AutoSize = true;
            this.chProductShippingChargeMap.Enabled = false;
            this.chProductShippingChargeMap.Location = new System.Drawing.Point(155, 420);
            this.chProductShippingChargeMap.Name = "chProductShippingChargeMap";
            this.chProductShippingChargeMap.Size = new System.Drawing.Size(15, 14);
            this.chProductShippingChargeMap.TabIndex = 109;
            this.chProductShippingChargeMap.UseVisualStyleBackColor = true;
            // 
            // chProductShippingEnabled
            // 
            this.chProductShippingEnabled.AutoSize = true;
            this.chProductShippingEnabled.Location = new System.Drawing.Point(328, 351);
            this.chProductShippingEnabled.Name = "chProductShippingEnabled";
            this.chProductShippingEnabled.Size = new System.Drawing.Size(15, 14);
            this.chProductShippingEnabled.TabIndex = 110;
            this.chProductShippingEnabled.UseVisualStyleBackColor = true;
            // 
            // chProductFreeShipping
            // 
            this.chProductFreeShipping.AutoSize = true;
            this.chProductFreeShipping.Location = new System.Drawing.Point(328, 374);
            this.chProductFreeShipping.Name = "chProductFreeShipping";
            this.chProductFreeShipping.Size = new System.Drawing.Size(15, 14);
            this.chProductFreeShipping.TabIndex = 111;
            this.chProductFreeShipping.UseVisualStyleBackColor = true;
            // 
            // chProductShipSeparately
            // 
            this.chProductShipSeparately.AutoSize = true;
            this.chProductShipSeparately.Location = new System.Drawing.Point(328, 397);
            this.chProductShipSeparately.Name = "chProductShipSeparately";
            this.chProductShipSeparately.Size = new System.Drawing.Size(15, 14);
            this.chProductShipSeparately.TabIndex = 112;
            this.chProductShipSeparately.UseVisualStyleBackColor = true;
            // 
            // chProductShippingEnabledImport
            // 
            this.chProductShippingEnabledImport.AutoSize = true;
            this.chProductShippingEnabledImport.Location = new System.Drawing.Point(759, 351);
            this.chProductShippingEnabledImport.Name = "chProductShippingEnabledImport";
            this.chProductShippingEnabledImport.Size = new System.Drawing.Size(15, 14);
            this.chProductShippingEnabledImport.TabIndex = 113;
            this.chProductShippingEnabledImport.UseVisualStyleBackColor = true;
            // 
            // chProductFreeShippingImport
            // 
            this.chProductFreeShippingImport.AutoSize = true;
            this.chProductFreeShippingImport.Location = new System.Drawing.Point(759, 374);
            this.chProductFreeShippingImport.Name = "chProductFreeShippingImport";
            this.chProductFreeShippingImport.Size = new System.Drawing.Size(15, 14);
            this.chProductFreeShippingImport.TabIndex = 114;
            this.chProductFreeShippingImport.UseVisualStyleBackColor = true;
            // 
            // chProductShipSeparatelyImport
            // 
            this.chProductShipSeparatelyImport.AutoSize = true;
            this.chProductShipSeparatelyImport.Location = new System.Drawing.Point(759, 397);
            this.chProductShipSeparatelyImport.Name = "chProductShipSeparatelyImport";
            this.chProductShipSeparatelyImport.Size = new System.Drawing.Size(15, 14);
            this.chProductShipSeparatelyImport.TabIndex = 115;
            this.chProductShipSeparatelyImport.UseVisualStyleBackColor = true;
            // 
            // chProductShippingChargeImport
            // 
            this.chProductShippingChargeImport.AutoSize = true;
            this.chProductShippingChargeImport.Location = new System.Drawing.Point(759, 420);
            this.chProductShippingChargeImport.Name = "chProductShippingChargeImport";
            this.chProductShippingChargeImport.Size = new System.Drawing.Size(15, 14);
            this.chProductShippingChargeImport.TabIndex = 116;
            this.chProductShippingChargeImport.UseVisualStyleBackColor = true;
            // 
            // nmProductShippingCharge
            // 
            this.nmProductShippingCharge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProductShippingCharge.DecimalPlaces = 2;
            this.nmProductShippingCharge.Location = new System.Drawing.Point(328, 420);
            this.nmProductShippingCharge.Minimum = new decimal(new int[] {
            99999,
            0,
            0,
            -2147483648});
            this.nmProductShippingCharge.Name = "nmProductShippingCharge";
            this.nmProductShippingCharge.Size = new System.Drawing.Size(46, 20);
            this.nmProductShippingCharge.TabIndex = 117;
            // 
            // ddInventoryMethod
            // 
            this.ddInventoryMethod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddInventoryMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddInventoryMethod.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddInventoryMethod.FormattingEnabled = true;
            this.ddInventoryMethod.Location = new System.Drawing.Point(328, 76);
            this.ddInventoryMethod.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddInventoryMethod.Name = "ddInventoryMethod";
            this.ddInventoryMethod.Size = new System.Drawing.Size(407, 21);
            this.ddInventoryMethod.TabIndex = 118;
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Location = new System.Drawing.Point(3, 441);
            this.label194.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(95, 13);
            this.label194.TabIndex = 119;
            this.label194.Text = "Low Stock Activity";
            // 
            // ddLowStockActivity
            // 
            this.ddLowStockActivity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddLowStockActivity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddLowStockActivity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ddLowStockActivity.FormattingEnabled = true;
            this.ddLowStockActivity.Location = new System.Drawing.Point(328, 444);
            this.ddLowStockActivity.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddLowStockActivity.Name = "ddLowStockActivity";
            this.ddLowStockActivity.Size = new System.Drawing.Size(407, 21);
            this.ddLowStockActivity.TabIndex = 120;
            // 
            // chLowStockActivityImport
            // 
            this.chLowStockActivityImport.AutoSize = true;
            this.chLowStockActivityImport.Location = new System.Drawing.Point(759, 444);
            this.chLowStockActivityImport.Name = "chLowStockActivityImport";
            this.chLowStockActivityImport.Size = new System.Drawing.Size(15, 14);
            this.chLowStockActivityImport.TabIndex = 121;
            this.chLowStockActivityImport.UseVisualStyleBackColor = true;
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(4, 468);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(92, 13);
            this.label174.TabIndex = 122;
            this.label174.Text = "Visible Individually";
            // 
            // chVisibleIndividually
            // 
            this.chVisibleIndividually.AutoSize = true;
            this.chVisibleIndividually.Location = new System.Drawing.Point(328, 471);
            this.chVisibleIndividually.Name = "chVisibleIndividually";
            this.chVisibleIndividually.Size = new System.Drawing.Size(15, 14);
            this.chVisibleIndividually.TabIndex = 123;
            this.chVisibleIndividually.UseVisualStyleBackColor = true;
            // 
            // chVisibleIndividuallyImport
            // 
            this.chVisibleIndividuallyImport.AutoSize = true;
            this.chVisibleIndividuallyImport.Location = new System.Drawing.Point(759, 471);
            this.chVisibleIndividuallyImport.Name = "chVisibleIndividuallyImport";
            this.chVisibleIndividuallyImport.Size = new System.Drawing.Size(15, 14);
            this.chVisibleIndividuallyImport.TabIndex = 124;
            this.chVisibleIndividuallyImport.UseVisualStyleBackColor = true;
            // 
            // ddWarehouse
            // 
            this.ddWarehouse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddWarehouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddWarehouse.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddWarehouse.FormattingEnabled = true;
            this.ddWarehouse.Location = new System.Drawing.Point(328, 494);
            this.ddWarehouse.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddWarehouse.Name = "ddWarehouse";
            this.ddWarehouse.Size = new System.Drawing.Size(407, 21);
            this.ddWarehouse.TabIndex = 126;
            // 
            // chWarehouseImport
            // 
            this.chWarehouseImport.AutoSize = true;
            this.chWarehouseImport.Location = new System.Drawing.Point(759, 494);
            this.chWarehouseImport.Name = "chWarehouseImport";
            this.chWarehouseImport.Size = new System.Drawing.Size(15, 14);
            this.chWarehouseImport.TabIndex = 127;
            this.chWarehouseImport.UseVisualStyleBackColor = true;
            // 
            // ddProductType
            // 
            this.ddProductType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddProductType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddProductType.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddProductType.FormattingEnabled = true;
            this.ddProductType.Location = new System.Drawing.Point(328, 521);
            this.ddProductType.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddProductType.Name = "ddProductType";
            this.ddProductType.Size = new System.Drawing.Size(407, 21);
            this.ddProductType.TabIndex = 129;
            // 
            // chProductTypeImport
            // 
            this.chProductTypeImport.AutoSize = true;
            this.chProductTypeImport.Location = new System.Drawing.Point(759, 521);
            this.chProductTypeImport.Name = "chProductTypeImport";
            this.chProductTypeImport.Size = new System.Drawing.Size(15, 14);
            this.chProductTypeImport.TabIndex = 130;
            this.chProductTypeImport.UseVisualStyleBackColor = true;
            // 
            // chVisibleIndividuallyMap
            // 
            this.chVisibleIndividuallyMap.AutoSize = true;
            this.chVisibleIndividuallyMap.Enabled = false;
            this.chVisibleIndividuallyMap.Location = new System.Drawing.Point(155, 471);
            this.chVisibleIndividuallyMap.Name = "chVisibleIndividuallyMap";
            this.chVisibleIndividuallyMap.Size = new System.Drawing.Size(15, 14);
            this.chVisibleIndividuallyMap.TabIndex = 131;
            this.chVisibleIndividuallyMap.UseVisualStyleBackColor = true;
            // 
            // chProductIsDeletedMap
            // 
            this.chProductIsDeletedMap.AutoSize = true;
            this.chProductIsDeletedMap.Enabled = false;
            this.chProductIsDeletedMap.Location = new System.Drawing.Point(155, 548);
            this.chProductIsDeletedMap.Name = "chProductIsDeletedMap";
            this.chProductIsDeletedMap.Size = new System.Drawing.Size(15, 14);
            this.chProductIsDeletedMap.TabIndex = 133;
            this.chProductIsDeletedMap.UseVisualStyleBackColor = true;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(4, 545);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(55, 13);
            this.label57.TabIndex = 132;
            this.label57.Text = "Is Deleted";
            // 
            // chProductIsDeleted
            // 
            this.chProductIsDeleted.AutoSize = true;
            this.chProductIsDeleted.Location = new System.Drawing.Point(328, 548);
            this.chProductIsDeleted.Name = "chProductIsDeleted";
            this.chProductIsDeleted.Size = new System.Drawing.Size(15, 14);
            this.chProductIsDeleted.TabIndex = 134;
            this.chProductIsDeleted.UseVisualStyleBackColor = true;
            // 
            // chProductIsDeletedImport
            // 
            this.chProductIsDeletedImport.AutoSize = true;
            this.chProductIsDeletedImport.Location = new System.Drawing.Point(759, 548);
            this.chProductIsDeletedImport.Name = "chProductIsDeletedImport";
            this.chProductIsDeletedImport.Size = new System.Drawing.Size(15, 14);
            this.chProductIsDeletedImport.TabIndex = 135;
            this.chProductIsDeletedImport.UseVisualStyleBackColor = true;
            // 
            // tabSeo
            // 
            this.tabSeo.Controls.Add(this.tableLayoutPanel2);
            this.tabSeo.Location = new System.Drawing.Point(4, 22);
            this.tabSeo.Name = "tabSeo";
            this.tabSeo.Padding = new System.Windows.Forms.Padding(3);
            this.tabSeo.Size = new System.Drawing.Size(905, 605);
            this.tabSeo.TabIndex = 2;
            this.tabSeo.Text = "SEO";
            this.tabSeo.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.Controls.Add(this.tbSearchEnginePage, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.tbMetaTitle, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.label32, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.tbMetaDescription, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.tbMetaKeywords, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label27, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label29, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label30, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label31, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label33, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label14, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.chSeoMetaKeyMap, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.chSeoMetaDescMap, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.chSeoMetaTitleMap, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.chSearchPageMap, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.label22, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.chSeoMetaKeyImport, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.chSeoMetaDescImport, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.chSeoMetaTitleImport, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.chSearchPageImport, 3, 4);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(908, 608);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // tbSearchEnginePage
            // 
            this.tbSearchEnginePage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearchEnginePage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSearchEnginePage.Location = new System.Drawing.Point(281, 138);
            this.tbSearchEnginePage.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbSearchEnginePage.Name = "tbSearchEnginePage";
            this.tbSearchEnginePage.Size = new System.Drawing.Size(478, 20);
            this.tbSearchEnginePage.TabIndex = 34;
            // 
            // tbMetaTitle
            // 
            this.tbMetaTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMetaTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMetaTitle.Location = new System.Drawing.Point(281, 107);
            this.tbMetaTitle.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbMetaTitle.Name = "tbMetaTitle";
            this.tbMetaTitle.Size = new System.Drawing.Size(478, 20);
            this.tbMetaTitle.TabIndex = 32;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(4, 104);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(54, 13);
            this.label32.TabIndex = 29;
            this.label32.Text = "Meta Title";
            // 
            // tbMetaDescription
            // 
            this.tbMetaDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMetaDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMetaDescription.Location = new System.Drawing.Point(281, 76);
            this.tbMetaDescription.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbMetaDescription.Name = "tbMetaDescription";
            this.tbMetaDescription.Size = new System.Drawing.Size(478, 20);
            this.tbMetaDescription.TabIndex = 28;
            // 
            // tbMetaKeywords
            // 
            this.tbMetaKeywords.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMetaKeywords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMetaKeywords.Location = new System.Drawing.Point(281, 45);
            this.tbMetaKeywords.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbMetaKeywords.Name = "tbMetaKeywords";
            this.tbMetaKeywords.Size = new System.Drawing.Size(478, 20);
            this.tbMetaKeywords.TabIndex = 27;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(4, 1);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(89, 17);
            this.label27.TabIndex = 12;
            this.label27.Text = "Field Name";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(281, 1);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(199, 17);
            this.label29.TabIndex = 22;
            this.label29.Text = "Default Value /+Add Value";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(4, 42);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(80, 13);
            this.label30.TabIndex = 23;
            this.label30.Text = "Meta Keywords";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(4, 73);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(87, 13);
            this.label31.TabIndex = 25;
            this.label31.Text = "Meta Description";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(4, 135);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(144, 26);
            this.label33.TabIndex = 30;
            this.label33.Text = "Search Engine Friendly Page Name";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label14.Location = new System.Drawing.Point(155, 1);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(94, 34);
            this.label14.TabIndex = 35;
            this.label14.Text = "Mapped To Source";
            // 
            // chSeoMetaKeyMap
            // 
            this.chSeoMetaKeyMap.AutoSize = true;
            this.chSeoMetaKeyMap.Enabled = false;
            this.chSeoMetaKeyMap.Location = new System.Drawing.Point(155, 45);
            this.chSeoMetaKeyMap.Name = "chSeoMetaKeyMap";
            this.chSeoMetaKeyMap.Size = new System.Drawing.Size(15, 14);
            this.chSeoMetaKeyMap.TabIndex = 36;
            this.chSeoMetaKeyMap.UseVisualStyleBackColor = true;
            // 
            // chSeoMetaDescMap
            // 
            this.chSeoMetaDescMap.AutoSize = true;
            this.chSeoMetaDescMap.Enabled = false;
            this.chSeoMetaDescMap.Location = new System.Drawing.Point(155, 76);
            this.chSeoMetaDescMap.Name = "chSeoMetaDescMap";
            this.chSeoMetaDescMap.Size = new System.Drawing.Size(15, 14);
            this.chSeoMetaDescMap.TabIndex = 37;
            this.chSeoMetaDescMap.UseVisualStyleBackColor = true;
            // 
            // chSeoMetaTitleMap
            // 
            this.chSeoMetaTitleMap.AutoSize = true;
            this.chSeoMetaTitleMap.Enabled = false;
            this.chSeoMetaTitleMap.Location = new System.Drawing.Point(155, 107);
            this.chSeoMetaTitleMap.Name = "chSeoMetaTitleMap";
            this.chSeoMetaTitleMap.Size = new System.Drawing.Size(15, 14);
            this.chSeoMetaTitleMap.TabIndex = 38;
            this.chSeoMetaTitleMap.UseVisualStyleBackColor = true;
            // 
            // chSearchPageMap
            // 
            this.chSearchPageMap.AutoSize = true;
            this.chSearchPageMap.Enabled = false;
            this.chSearchPageMap.Location = new System.Drawing.Point(155, 138);
            this.chSearchPageMap.Name = "chSearchPageMap";
            this.chSearchPageMap.Size = new System.Drawing.Size(15, 14);
            this.chSearchPageMap.TabIndex = 39;
            this.chSearchPageMap.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(783, 1);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(80, 34);
            this.label22.TabIndex = 44;
            this.label22.Text = "Fields To Import";
            // 
            // chSeoMetaKeyImport
            // 
            this.chSeoMetaKeyImport.AutoSize = true;
            this.chSeoMetaKeyImport.Location = new System.Drawing.Point(783, 45);
            this.chSeoMetaKeyImport.Name = "chSeoMetaKeyImport";
            this.chSeoMetaKeyImport.Size = new System.Drawing.Size(15, 14);
            this.chSeoMetaKeyImport.TabIndex = 45;
            this.chSeoMetaKeyImport.UseVisualStyleBackColor = true;
            // 
            // chSeoMetaDescImport
            // 
            this.chSeoMetaDescImport.AutoSize = true;
            this.chSeoMetaDescImport.Location = new System.Drawing.Point(783, 76);
            this.chSeoMetaDescImport.Name = "chSeoMetaDescImport";
            this.chSeoMetaDescImport.Size = new System.Drawing.Size(15, 14);
            this.chSeoMetaDescImport.TabIndex = 46;
            this.chSeoMetaDescImport.UseVisualStyleBackColor = true;
            // 
            // chSeoMetaTitleImport
            // 
            this.chSeoMetaTitleImport.AutoSize = true;
            this.chSeoMetaTitleImport.Location = new System.Drawing.Point(783, 107);
            this.chSeoMetaTitleImport.Name = "chSeoMetaTitleImport";
            this.chSeoMetaTitleImport.Size = new System.Drawing.Size(15, 14);
            this.chSeoMetaTitleImport.TabIndex = 47;
            this.chSeoMetaTitleImport.UseVisualStyleBackColor = true;
            // 
            // chSearchPageImport
            // 
            this.chSearchPageImport.AutoSize = true;
            this.chSearchPageImport.Location = new System.Drawing.Point(783, 138);
            this.chSearchPageImport.Name = "chSearchPageImport";
            this.chSearchPageImport.Size = new System.Drawing.Size(15, 14);
            this.chSearchPageImport.TabIndex = 48;
            this.chSearchPageImport.UseVisualStyleBackColor = true;
            // 
            // tabCategories
            // 
            this.tabCategories.Controls.Add(this.chDisableNestedCategories);
            this.tabCategories.Controls.Add(this.label55);
            this.tabCategories.Controls.Add(this.chInsertCategoryPictureOnInsert);
            this.tabCategories.Controls.Add(this.label170);
            this.tabCategories.Controls.Add(this.chInsertProductsToLastSubCategory);
            this.tabCategories.Controls.Add(this.label147);
            this.tabCategories.Controls.Add(this.chDeleteOldCatMapping);
            this.tabCategories.Controls.Add(this.label98);
            this.tabCategories.Controls.Add(this.tableLayoutPanel3);
            this.tabCategories.Location = new System.Drawing.Point(4, 22);
            this.tabCategories.Name = "tabCategories";
            this.tabCategories.Padding = new System.Windows.Forms.Padding(3);
            this.tabCategories.Size = new System.Drawing.Size(905, 605);
            this.tabCategories.TabIndex = 3;
            this.tabCategories.Text = "Categories";
            this.tabCategories.UseVisualStyleBackColor = true;
            // 
            // chDisableNestedCategories
            // 
            this.chDisableNestedCategories.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chDisableNestedCategories.AutoSize = true;
            this.chDisableNestedCategories.Location = new System.Drawing.Point(275, 302);
            this.chDisableNestedCategories.Name = "chDisableNestedCategories";
            this.chDisableNestedCategories.Size = new System.Drawing.Size(15, 14);
            this.chDisableNestedCategories.TabIndex = 47;
            this.chDisableNestedCategories.UseVisualStyleBackColor = true;
            // 
            // label55
            // 
            this.label55.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(9, 302);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(138, 13);
            this.label55.TabIndex = 46;
            this.label55.Text = "Disable Nested Ccategories";
            // 
            // chInsertCategoryPictureOnInsert
            // 
            this.chInsertCategoryPictureOnInsert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chInsertCategoryPictureOnInsert.AutoSize = true;
            this.chInsertCategoryPictureOnInsert.Checked = true;
            this.chInsertCategoryPictureOnInsert.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chInsertCategoryPictureOnInsert.Location = new System.Drawing.Point(275, 272);
            this.chInsertCategoryPictureOnInsert.Name = "chInsertCategoryPictureOnInsert";
            this.chInsertCategoryPictureOnInsert.Size = new System.Drawing.Size(15, 14);
            this.chInsertCategoryPictureOnInsert.TabIndex = 45;
            this.chInsertCategoryPictureOnInsert.UseVisualStyleBackColor = true;
            // 
            // label170
            // 
            this.label170.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(9, 273);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(208, 13);
            this.label170.TabIndex = 44;
            this.label170.Text = "Insert Picture From First Product On Create";
            // 
            // chInsertProductsToLastSubCategory
            // 
            this.chInsertProductsToLastSubCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chInsertProductsToLastSubCategory.AutoSize = true;
            this.chInsertProductsToLastSubCategory.Checked = true;
            this.chInsertProductsToLastSubCategory.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chInsertProductsToLastSubCategory.Location = new System.Drawing.Point(275, 245);
            this.chInsertProductsToLastSubCategory.Name = "chInsertProductsToLastSubCategory";
            this.chInsertProductsToLastSubCategory.Size = new System.Drawing.Size(15, 14);
            this.chInsertProductsToLastSubCategory.TabIndex = 43;
            this.chInsertProductsToLastSubCategory.UseVisualStyleBackColor = true;
            // 
            // label147
            // 
            this.label147.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(9, 244);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(229, 13);
            this.label147.TabIndex = 42;
            this.label147.Text = "Import Products To The Last Subcategory Only";
            // 
            // chDeleteOldCatMapping
            // 
            this.chDeleteOldCatMapping.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chDeleteOldCatMapping.AutoSize = true;
            this.chDeleteOldCatMapping.Checked = true;
            this.chDeleteOldCatMapping.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chDeleteOldCatMapping.Location = new System.Drawing.Point(275, 216);
            this.chDeleteOldCatMapping.Name = "chDeleteOldCatMapping";
            this.chDeleteOldCatMapping.Size = new System.Drawing.Size(15, 14);
            this.chDeleteOldCatMapping.TabIndex = 41;
            this.chDeleteOldCatMapping.UseVisualStyleBackColor = true;
            // 
            // label98
            // 
            this.label98.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(9, 217);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(261, 13);
            this.label98.TabIndex = 40;
            this.label98.Text = "Delete Existing Product-Category Mapping On Update";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.Controls.Add(this.label35, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label38, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label37, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.tbCategory, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.label39, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label40, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label41, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.label42, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.tbCategory1, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.tbCategory2, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.tbCategory3, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.tbCategory4, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.label28, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.chCategoryMap, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.chCategory1Map, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.chCategory2Map, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.chCategory3Map, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.chCategory4Map, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.label34, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.chCategoryImport, 4, 1);
            this.tableLayoutPanel3.Controls.Add(this.chCategory1Import, 4, 2);
            this.tableLayoutPanel3.Controls.Add(this.chCategory2Import, 4, 3);
            this.tableLayoutPanel3.Controls.Add(this.chCategory3Import, 4, 4);
            this.tableLayoutPanel3.Controls.Add(this.chCategory4Import, 4, 5);
            this.tableLayoutPanel3.Controls.Add(this.label75, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.ddParentCat1, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.ddParentCat2, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.ddParentCat3, 3, 4);
            this.tableLayoutPanel3.Controls.Add(this.ddParentCat4, 3, 5);
            this.tableLayoutPanel3.Controls.Add(this.ddParentCat0, 3, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(-5, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 7;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(914, 197);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(4, 1);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(89, 17);
            this.label35.TabIndex = 12;
            this.label35.Text = "Field Name";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(4, 42);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(68, 13);
            this.label38.TabIndex = 23;
            this.label38.Text = "Category(ies)";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(282, 1);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(199, 17);
            this.label37.TabIndex = 22;
            this.label37.Text = "Default Value /+Add Value";
            // 
            // tbCategory
            // 
            this.tbCategory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCategory.Location = new System.Drawing.Point(282, 45);
            this.tbCategory.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCategory.Name = "tbCategory";
            this.tbCategory.Size = new System.Drawing.Size(355, 20);
            this.tbCategory.TabIndex = 27;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(4, 73);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(55, 13);
            this.label39.TabIndex = 30;
            this.label39.Text = "Category1";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(4, 104);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(55, 13);
            this.label40.TabIndex = 31;
            this.label40.Text = "Category2";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(4, 135);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(55, 13);
            this.label41.TabIndex = 32;
            this.label41.Text = "Category3";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(4, 166);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(55, 13);
            this.label42.TabIndex = 33;
            this.label42.Text = "Category4";
            // 
            // tbCategory1
            // 
            this.tbCategory1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCategory1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCategory1.Location = new System.Drawing.Point(282, 76);
            this.tbCategory1.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCategory1.Name = "tbCategory1";
            this.tbCategory1.Size = new System.Drawing.Size(355, 20);
            this.tbCategory1.TabIndex = 38;
            // 
            // tbCategory2
            // 
            this.tbCategory2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCategory2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCategory2.Location = new System.Drawing.Point(282, 107);
            this.tbCategory2.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCategory2.Name = "tbCategory2";
            this.tbCategory2.Size = new System.Drawing.Size(355, 20);
            this.tbCategory2.TabIndex = 39;
            // 
            // tbCategory3
            // 
            this.tbCategory3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCategory3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCategory3.Location = new System.Drawing.Point(282, 138);
            this.tbCategory3.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCategory3.Name = "tbCategory3";
            this.tbCategory3.Size = new System.Drawing.Size(355, 20);
            this.tbCategory3.TabIndex = 40;
            // 
            // tbCategory4
            // 
            this.tbCategory4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCategory4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCategory4.Location = new System.Drawing.Point(282, 169);
            this.tbCategory4.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCategory4.Name = "tbCategory4";
            this.tbCategory4.Size = new System.Drawing.Size(355, 20);
            this.tbCategory4.TabIndex = 41;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(155, 1);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(94, 34);
            this.label28.TabIndex = 42;
            this.label28.Text = "Mapped To Source";
            // 
            // chCategoryMap
            // 
            this.chCategoryMap.AutoSize = true;
            this.chCategoryMap.Enabled = false;
            this.chCategoryMap.Location = new System.Drawing.Point(155, 45);
            this.chCategoryMap.Name = "chCategoryMap";
            this.chCategoryMap.Size = new System.Drawing.Size(15, 14);
            this.chCategoryMap.TabIndex = 43;
            this.chCategoryMap.UseVisualStyleBackColor = true;
            // 
            // chCategory1Map
            // 
            this.chCategory1Map.AutoSize = true;
            this.chCategory1Map.Enabled = false;
            this.chCategory1Map.Location = new System.Drawing.Point(155, 76);
            this.chCategory1Map.Name = "chCategory1Map";
            this.chCategory1Map.Size = new System.Drawing.Size(15, 14);
            this.chCategory1Map.TabIndex = 44;
            this.chCategory1Map.UseVisualStyleBackColor = true;
            // 
            // chCategory2Map
            // 
            this.chCategory2Map.AutoSize = true;
            this.chCategory2Map.Enabled = false;
            this.chCategory2Map.Location = new System.Drawing.Point(155, 107);
            this.chCategory2Map.Name = "chCategory2Map";
            this.chCategory2Map.Size = new System.Drawing.Size(15, 14);
            this.chCategory2Map.TabIndex = 45;
            this.chCategory2Map.UseVisualStyleBackColor = true;
            // 
            // chCategory3Map
            // 
            this.chCategory3Map.AutoSize = true;
            this.chCategory3Map.Enabled = false;
            this.chCategory3Map.Location = new System.Drawing.Point(155, 138);
            this.chCategory3Map.Name = "chCategory3Map";
            this.chCategory3Map.Size = new System.Drawing.Size(15, 14);
            this.chCategory3Map.TabIndex = 46;
            this.chCategory3Map.UseVisualStyleBackColor = true;
            // 
            // chCategory4Map
            // 
            this.chCategory4Map.AutoSize = true;
            this.chCategory4Map.Enabled = false;
            this.chCategory4Map.Location = new System.Drawing.Point(155, 169);
            this.chCategory4Map.Name = "chCategory4Map";
            this.chCategory4Map.Size = new System.Drawing.Size(15, 14);
            this.chCategory4Map.TabIndex = 47;
            this.chCategory4Map.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(788, 1);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(121, 34);
            this.label34.TabIndex = 48;
            this.label34.Text = "Import products to this category";
            // 
            // chCategoryImport
            // 
            this.chCategoryImport.AutoSize = true;
            this.chCategoryImport.Location = new System.Drawing.Point(788, 45);
            this.chCategoryImport.Name = "chCategoryImport";
            this.chCategoryImport.Size = new System.Drawing.Size(15, 14);
            this.chCategoryImport.TabIndex = 49;
            this.chCategoryImport.UseVisualStyleBackColor = true;
            // 
            // chCategory1Import
            // 
            this.chCategory1Import.AutoSize = true;
            this.chCategory1Import.Location = new System.Drawing.Point(788, 76);
            this.chCategory1Import.Name = "chCategory1Import";
            this.chCategory1Import.Size = new System.Drawing.Size(15, 14);
            this.chCategory1Import.TabIndex = 50;
            this.chCategory1Import.UseVisualStyleBackColor = true;
            // 
            // chCategory2Import
            // 
            this.chCategory2Import.AutoSize = true;
            this.chCategory2Import.Location = new System.Drawing.Point(788, 107);
            this.chCategory2Import.Name = "chCategory2Import";
            this.chCategory2Import.Size = new System.Drawing.Size(15, 14);
            this.chCategory2Import.TabIndex = 51;
            this.chCategory2Import.UseVisualStyleBackColor = true;
            // 
            // chCategory3Import
            // 
            this.chCategory3Import.AutoSize = true;
            this.chCategory3Import.Location = new System.Drawing.Point(788, 138);
            this.chCategory3Import.Name = "chCategory3Import";
            this.chCategory3Import.Size = new System.Drawing.Size(15, 14);
            this.chCategory3Import.TabIndex = 52;
            this.chCategory3Import.UseVisualStyleBackColor = true;
            // 
            // chCategory4Import
            // 
            this.chCategory4Import.AutoSize = true;
            this.chCategory4Import.Location = new System.Drawing.Point(788, 169);
            this.chCategory4Import.Name = "chCategory4Import";
            this.chCategory4Import.Size = new System.Drawing.Size(15, 14);
            this.chCategory4Import.TabIndex = 53;
            this.chCategory4Import.UseVisualStyleBackColor = true;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(661, 1);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(71, 34);
            this.label75.TabIndex = 54;
            this.label75.Text = "Parent category";
            // 
            // ddParentCat1
            // 
            this.ddParentCat1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddParentCat1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddParentCat1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddParentCat1.FormattingEnabled = true;
            this.ddParentCat1.Items.AddRange(new object[] {
            "",
            "Category0"});
            this.ddParentCat1.Location = new System.Drawing.Point(661, 76);
            this.ddParentCat1.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddParentCat1.Name = "ddParentCat1";
            this.ddParentCat1.Size = new System.Drawing.Size(103, 21);
            this.ddParentCat1.TabIndex = 63;
            // 
            // ddParentCat2
            // 
            this.ddParentCat2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddParentCat2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddParentCat2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddParentCat2.FormattingEnabled = true;
            this.ddParentCat2.Items.AddRange(new object[] {
            "",
            "Category0",
            "Category1"});
            this.ddParentCat2.Location = new System.Drawing.Point(661, 107);
            this.ddParentCat2.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddParentCat2.Name = "ddParentCat2";
            this.ddParentCat2.Size = new System.Drawing.Size(103, 21);
            this.ddParentCat2.TabIndex = 64;
            // 
            // ddParentCat3
            // 
            this.ddParentCat3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddParentCat3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddParentCat3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddParentCat3.FormattingEnabled = true;
            this.ddParentCat3.Items.AddRange(new object[] {
            "",
            "Category0",
            "Category1",
            "Category2"});
            this.ddParentCat3.Location = new System.Drawing.Point(661, 138);
            this.ddParentCat3.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddParentCat3.Name = "ddParentCat3";
            this.ddParentCat3.Size = new System.Drawing.Size(103, 21);
            this.ddParentCat3.TabIndex = 65;
            // 
            // ddParentCat4
            // 
            this.ddParentCat4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddParentCat4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddParentCat4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddParentCat4.FormattingEnabled = true;
            this.ddParentCat4.Items.AddRange(new object[] {
            "",
            "Category0",
            "Category1",
            "Category2",
            "Category3"});
            this.ddParentCat4.Location = new System.Drawing.Point(661, 169);
            this.ddParentCat4.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddParentCat4.Name = "ddParentCat4";
            this.ddParentCat4.Size = new System.Drawing.Size(103, 21);
            this.ddParentCat4.TabIndex = 66;
            // 
            // ddParentCat0
            // 
            this.ddParentCat0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddParentCat0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddParentCat0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddParentCat0.FormattingEnabled = true;
            this.ddParentCat0.Items.AddRange(new object[] {
            "",
            "By Split Order"});
            this.ddParentCat0.Location = new System.Drawing.Point(661, 45);
            this.ddParentCat0.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddParentCat0.Name = "ddParentCat0";
            this.ddParentCat0.Size = new System.Drawing.Size(103, 21);
            this.ddParentCat0.TabIndex = 67;
            // 
            // tabAccessControl
            // 
            this.tabAccessControl.Controls.Add(this.tableLayoutPanel8);
            this.tabAccessControl.Location = new System.Drawing.Point(4, 22);
            this.tabAccessControl.Margin = new System.Windows.Forms.Padding(2);
            this.tabAccessControl.Name = "tabAccessControl";
            this.tabAccessControl.Size = new System.Drawing.Size(905, 605);
            this.tabAccessControl.TabIndex = 8;
            this.tabAccessControl.Text = "Access Control";
            this.tabAccessControl.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel8.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel8.ColumnCount = 4;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel8.Controls.Add(this.chCustomerRole4Map, 0, 5);
            this.tableLayoutPanel8.Controls.Add(this.tbCustomerRole3, 2, 4);
            this.tableLayoutPanel8.Controls.Add(this.tbCustomerRole2, 2, 3);
            this.tableLayoutPanel8.Controls.Add(this.label85, 0, 3);
            this.tableLayoutPanel8.Controls.Add(this.tbCustomerRole1, 2, 2);
            this.tableLayoutPanel8.Controls.Add(this.tbCustomerRole, 2, 1);
            this.tableLayoutPanel8.Controls.Add(this.label86, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.label87, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.label88, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label89, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.label90, 0, 4);
            this.tableLayoutPanel8.Controls.Add(this.label91, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.chCustomerRoleMap, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.chCustomerRole1Map, 1, 2);
            this.tableLayoutPanel8.Controls.Add(this.chCustomerRole2Map, 1, 3);
            this.tableLayoutPanel8.Controls.Add(this.chCustomerRole3Map, 1, 4);
            this.tableLayoutPanel8.Controls.Add(this.label92, 3, 0);
            this.tableLayoutPanel8.Controls.Add(this.chCustomerRoleImport, 3, 1);
            this.tableLayoutPanel8.Controls.Add(this.chCustomerRole1Import, 3, 2);
            this.tableLayoutPanel8.Controls.Add(this.chCustomerRole2Import, 3, 3);
            this.tableLayoutPanel8.Controls.Add(this.chCustomerRole3Import, 3, 4);
            this.tableLayoutPanel8.Controls.Add(this.label93, 0, 5);
            this.tableLayoutPanel8.Controls.Add(this.tbCustomerRole4, 2, 5);
            this.tableLayoutPanel8.Controls.Add(this.chCustomerRole4Import, 3, 5);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 2);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 6;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(907, 199);
            this.tableLayoutPanel8.TabIndex = 2;
            // 
            // chCustomerRole4Map
            // 
            this.chCustomerRole4Map.AutoSize = true;
            this.chCustomerRole4Map.Enabled = false;
            this.chCustomerRole4Map.Location = new System.Drawing.Point(155, 169);
            this.chCustomerRole4Map.Name = "chCustomerRole4Map";
            this.chCustomerRole4Map.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRole4Map.TabIndex = 52;
            this.chCustomerRole4Map.UseVisualStyleBackColor = true;
            // 
            // tbCustomerRole3
            // 
            this.tbCustomerRole3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCustomerRole3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCustomerRole3.Location = new System.Drawing.Point(281, 138);
            this.tbCustomerRole3.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCustomerRole3.Name = "tbCustomerRole3";
            this.tbCustomerRole3.Size = new System.Drawing.Size(478, 20);
            this.tbCustomerRole3.TabIndex = 34;
            // 
            // tbCustomerRole2
            // 
            this.tbCustomerRole2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCustomerRole2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCustomerRole2.Location = new System.Drawing.Point(281, 107);
            this.tbCustomerRole2.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCustomerRole2.Name = "tbCustomerRole2";
            this.tbCustomerRole2.Size = new System.Drawing.Size(478, 20);
            this.tbCustomerRole2.TabIndex = 32;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(4, 104);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(82, 13);
            this.label85.TabIndex = 29;
            this.label85.Text = "Customer Role2";
            // 
            // tbCustomerRole1
            // 
            this.tbCustomerRole1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCustomerRole1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCustomerRole1.Location = new System.Drawing.Point(281, 76);
            this.tbCustomerRole1.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCustomerRole1.Name = "tbCustomerRole1";
            this.tbCustomerRole1.Size = new System.Drawing.Size(478, 20);
            this.tbCustomerRole1.TabIndex = 28;
            // 
            // tbCustomerRole
            // 
            this.tbCustomerRole.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCustomerRole.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCustomerRole.Location = new System.Drawing.Point(281, 45);
            this.tbCustomerRole.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCustomerRole.Name = "tbCustomerRole";
            this.tbCustomerRole.Size = new System.Drawing.Size(478, 20);
            this.tbCustomerRole.TabIndex = 27;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(4, 1);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(89, 17);
            this.label86.TabIndex = 12;
            this.label86.Text = "Field Name";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(281, 1);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(199, 17);
            this.label87.TabIndex = 22;
            this.label87.Text = "Default Value /+Add Value";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(4, 42);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(87, 13);
            this.label88.TabIndex = 23;
            this.label88.Text = "Customer Role(s)";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(4, 73);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(82, 13);
            this.label89.TabIndex = 25;
            this.label89.Text = "Customer Role1";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(4, 135);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(82, 13);
            this.label90.TabIndex = 30;
            this.label90.Text = "Customer Role3";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(155, 1);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(94, 34);
            this.label91.TabIndex = 35;
            this.label91.Text = "Mapped To Source";
            // 
            // chCustomerRoleMap
            // 
            this.chCustomerRoleMap.AutoSize = true;
            this.chCustomerRoleMap.Enabled = false;
            this.chCustomerRoleMap.Location = new System.Drawing.Point(155, 45);
            this.chCustomerRoleMap.Name = "chCustomerRoleMap";
            this.chCustomerRoleMap.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRoleMap.TabIndex = 36;
            this.chCustomerRoleMap.UseVisualStyleBackColor = true;
            // 
            // chCustomerRole1Map
            // 
            this.chCustomerRole1Map.AutoSize = true;
            this.chCustomerRole1Map.Enabled = false;
            this.chCustomerRole1Map.Location = new System.Drawing.Point(155, 76);
            this.chCustomerRole1Map.Name = "chCustomerRole1Map";
            this.chCustomerRole1Map.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRole1Map.TabIndex = 37;
            this.chCustomerRole1Map.UseVisualStyleBackColor = true;
            // 
            // chCustomerRole2Map
            // 
            this.chCustomerRole2Map.AutoSize = true;
            this.chCustomerRole2Map.Enabled = false;
            this.chCustomerRole2Map.Location = new System.Drawing.Point(155, 107);
            this.chCustomerRole2Map.Name = "chCustomerRole2Map";
            this.chCustomerRole2Map.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRole2Map.TabIndex = 38;
            this.chCustomerRole2Map.UseVisualStyleBackColor = true;
            // 
            // chCustomerRole3Map
            // 
            this.chCustomerRole3Map.AutoSize = true;
            this.chCustomerRole3Map.Enabled = false;
            this.chCustomerRole3Map.Location = new System.Drawing.Point(155, 138);
            this.chCustomerRole3Map.Name = "chCustomerRole3Map";
            this.chCustomerRole3Map.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRole3Map.TabIndex = 39;
            this.chCustomerRole3Map.UseVisualStyleBackColor = true;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(783, 1);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(80, 34);
            this.label92.TabIndex = 44;
            this.label92.Text = "Fields To Import";
            // 
            // chCustomerRoleImport
            // 
            this.chCustomerRoleImport.AutoSize = true;
            this.chCustomerRoleImport.Location = new System.Drawing.Point(783, 45);
            this.chCustomerRoleImport.Name = "chCustomerRoleImport";
            this.chCustomerRoleImport.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRoleImport.TabIndex = 45;
            this.chCustomerRoleImport.UseVisualStyleBackColor = true;
            // 
            // chCustomerRole1Import
            // 
            this.chCustomerRole1Import.AutoSize = true;
            this.chCustomerRole1Import.Location = new System.Drawing.Point(783, 76);
            this.chCustomerRole1Import.Name = "chCustomerRole1Import";
            this.chCustomerRole1Import.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRole1Import.TabIndex = 46;
            this.chCustomerRole1Import.UseVisualStyleBackColor = true;
            // 
            // chCustomerRole2Import
            // 
            this.chCustomerRole2Import.AutoSize = true;
            this.chCustomerRole2Import.Location = new System.Drawing.Point(783, 107);
            this.chCustomerRole2Import.Name = "chCustomerRole2Import";
            this.chCustomerRole2Import.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRole2Import.TabIndex = 47;
            this.chCustomerRole2Import.UseVisualStyleBackColor = true;
            // 
            // chCustomerRole3Import
            // 
            this.chCustomerRole3Import.AutoSize = true;
            this.chCustomerRole3Import.Location = new System.Drawing.Point(783, 138);
            this.chCustomerRole3Import.Name = "chCustomerRole3Import";
            this.chCustomerRole3Import.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRole3Import.TabIndex = 48;
            this.chCustomerRole3Import.UseVisualStyleBackColor = true;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(4, 166);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(82, 13);
            this.label93.TabIndex = 49;
            this.label93.Text = "Customer Role4";
            // 
            // tbCustomerRole4
            // 
            this.tbCustomerRole4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCustomerRole4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCustomerRole4.Location = new System.Drawing.Point(281, 169);
            this.tbCustomerRole4.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCustomerRole4.Name = "tbCustomerRole4";
            this.tbCustomerRole4.Size = new System.Drawing.Size(478, 20);
            this.tbCustomerRole4.TabIndex = 51;
            // 
            // chCustomerRole4Import
            // 
            this.chCustomerRole4Import.AutoSize = true;
            this.chCustomerRole4Import.Location = new System.Drawing.Point(783, 169);
            this.chCustomerRole4Import.Name = "chCustomerRole4Import";
            this.chCustomerRole4Import.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRole4Import.TabIndex = 50;
            this.chCustomerRole4Import.UseVisualStyleBackColor = true;
            // 
            // tabImages
            // 
            this.tabImages.Controls.Add(this.panelProdPictures);
            this.tabImages.Controls.Add(this.nmPictureResizeHeight);
            this.tabImages.Controls.Add(this.nmPictureResizeWidth);
            this.tabImages.Controls.Add(this.nmPictureResizeQuality);
            this.tabImages.Controls.Add(this.label190);
            this.tabImages.Controls.Add(this.label189);
            this.tabImages.Controls.Add(this.label185);
            this.tabImages.Controls.Add(this.chResizePicture);
            this.tabImages.Controls.Add(this.label184);
            this.tabImages.Controls.Add(this.chDeleteOldPictures);
            this.tabImages.Controls.Add(this.label151);
            this.tabImages.Location = new System.Drawing.Point(4, 22);
            this.tabImages.Name = "tabImages";
            this.tabImages.Padding = new System.Windows.Forms.Padding(3);
            this.tabImages.Size = new System.Drawing.Size(905, 605);
            this.tabImages.TabIndex = 4;
            this.tabImages.Text = "Pictures";
            this.tabImages.UseVisualStyleBackColor = true;
            // 
            // panelProdPictures
            // 
            this.panelProdPictures.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelProdPictures.Location = new System.Drawing.Point(3, 4);
            this.panelProdPictures.Margin = new System.Windows.Forms.Padding(2);
            this.panelProdPictures.Name = "panelProdPictures";
            this.panelProdPictures.Size = new System.Drawing.Size(898, 408);
            this.panelProdPictures.TabIndex = 83;
            // 
            // nmPictureResizeHeight
            // 
            this.nmPictureResizeHeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.nmPictureResizeHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmPictureResizeHeight.Location = new System.Drawing.Point(192, 492);
            this.nmPictureResizeHeight.Name = "nmPictureResizeHeight";
            this.nmPictureResizeHeight.Size = new System.Drawing.Size(46, 20);
            this.nmPictureResizeHeight.TabIndex = 82;
            this.nmPictureResizeHeight.Tag = "";
            // 
            // nmPictureResizeWidth
            // 
            this.nmPictureResizeWidth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.nmPictureResizeWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmPictureResizeWidth.Location = new System.Drawing.Point(192, 518);
            this.nmPictureResizeWidth.Name = "nmPictureResizeWidth";
            this.nmPictureResizeWidth.Size = new System.Drawing.Size(46, 20);
            this.nmPictureResizeWidth.TabIndex = 81;
            this.nmPictureResizeWidth.Tag = "";
            // 
            // nmPictureResizeQuality
            // 
            this.nmPictureResizeQuality.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.nmPictureResizeQuality.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmPictureResizeQuality.Location = new System.Drawing.Point(191, 542);
            this.nmPictureResizeQuality.Name = "nmPictureResizeQuality";
            this.nmPictureResizeQuality.Size = new System.Drawing.Size(46, 20);
            this.nmPictureResizeQuality.TabIndex = 80;
            this.nmPictureResizeQuality.Tag = "";
            // 
            // label190
            // 
            this.label190.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label190.AutoSize = true;
            this.label190.Location = new System.Drawing.Point(7, 546);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(39, 13);
            this.label190.TabIndex = 78;
            this.label190.Text = "Quality";
            // 
            // label189
            // 
            this.label189.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label189.AutoSize = true;
            this.label189.Location = new System.Drawing.Point(7, 522);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(58, 13);
            this.label189.TabIndex = 76;
            this.label189.Text = "Max Width";
            // 
            // label185
            // 
            this.label185.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(7, 497);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(61, 13);
            this.label185.TabIndex = 46;
            this.label185.Text = "Max Height";
            // 
            // chResizePicture
            // 
            this.chResizePicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chResizePicture.AutoSize = true;
            this.chResizePicture.Location = new System.Drawing.Point(192, 471);
            this.chResizePicture.Name = "chResizePicture";
            this.chResizePicture.Size = new System.Drawing.Size(15, 14);
            this.chResizePicture.TabIndex = 45;
            this.chResizePicture.UseVisualStyleBackColor = true;
            // 
            // label184
            // 
            this.label184.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(7, 472);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(80, 13);
            this.label184.TabIndex = 44;
            this.label184.Text = "Resize Pictures";
            // 
            // chDeleteOldPictures
            // 
            this.chDeleteOldPictures.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chDeleteOldPictures.AutoSize = true;
            this.chDeleteOldPictures.Checked = true;
            this.chDeleteOldPictures.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chDeleteOldPictures.Location = new System.Drawing.Point(192, 449);
            this.chDeleteOldPictures.Name = "chDeleteOldPictures";
            this.chDeleteOldPictures.Size = new System.Drawing.Size(15, 14);
            this.chDeleteOldPictures.TabIndex = 43;
            this.chDeleteOldPictures.UseVisualStyleBackColor = true;
            // 
            // label151
            // 
            this.label151.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(6, 448);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(173, 13);
            this.label151.TabIndex = 42;
            this.label151.Text = "Delete Existing Pictures On Update";
            // 
            // tabProdAttributes
            // 
            this.tabProdAttributes.Controls.Add(this.panelProdAttributtes);
            this.tabProdAttributes.Controls.Add(this.chDeleteOldAttributtes);
            this.tabProdAttributes.Controls.Add(this.label152);
            this.tabProdAttributes.Location = new System.Drawing.Point(4, 22);
            this.tabProdAttributes.Name = "tabProdAttributes";
            this.tabProdAttributes.Padding = new System.Windows.Forms.Padding(3);
            this.tabProdAttributes.Size = new System.Drawing.Size(905, 605);
            this.tabProdAttributes.TabIndex = 5;
            this.tabProdAttributes.Text = "Product Attributes";
            this.tabProdAttributes.UseVisualStyleBackColor = true;
            // 
            // panelProdAttributtes
            // 
            this.panelProdAttributtes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelProdAttributtes.Location = new System.Drawing.Point(3, 3);
            this.panelProdAttributtes.Margin = new System.Windows.Forms.Padding(2);
            this.panelProdAttributtes.Name = "panelProdAttributtes";
            this.panelProdAttributtes.Size = new System.Drawing.Size(898, 557);
            this.panelProdAttributtes.TabIndex = 46;
            // 
            // chDeleteOldAttributtes
            // 
            this.chDeleteOldAttributtes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chDeleteOldAttributtes.AutoSize = true;
            this.chDeleteOldAttributtes.Checked = true;
            this.chDeleteOldAttributtes.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chDeleteOldAttributtes.Location = new System.Drawing.Point(256, 577);
            this.chDeleteOldAttributtes.Name = "chDeleteOldAttributtes";
            this.chDeleteOldAttributtes.Size = new System.Drawing.Size(15, 14);
            this.chDeleteOldAttributtes.TabIndex = 45;
            this.chDeleteOldAttributtes.UseVisualStyleBackColor = true;
            // 
            // label152
            // 
            this.label152.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(3, 578);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(209, 13);
            this.label152.TabIndex = 44;
            this.label152.Text = "Delete Existing Attribute Values On Update";
            // 
            // tabSpecAttributes
            // 
            this.tabSpecAttributes.Controls.Add(this.panelProdSpecAttributtes);
            this.tabSpecAttributes.Controls.Add(this.chAddSpecAttributesAsTable);
            this.tabSpecAttributes.Controls.Add(this.label191);
            this.tabSpecAttributes.Controls.Add(this.chDeleteOldSpecAttributes);
            this.tabSpecAttributes.Controls.Add(this.label156);
            this.tabSpecAttributes.Location = new System.Drawing.Point(4, 22);
            this.tabSpecAttributes.Margin = new System.Windows.Forms.Padding(2);
            this.tabSpecAttributes.Name = "tabSpecAttributes";
            this.tabSpecAttributes.Size = new System.Drawing.Size(905, 605);
            this.tabSpecAttributes.TabIndex = 10;
            this.tabSpecAttributes.Text = "Specification Attributes ";
            this.tabSpecAttributes.UseVisualStyleBackColor = true;
            // 
            // panelProdSpecAttributtes
            // 
            this.panelProdSpecAttributtes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelProdSpecAttributtes.Location = new System.Drawing.Point(3, 2);
            this.panelProdSpecAttributtes.Margin = new System.Windows.Forms.Padding(2);
            this.panelProdSpecAttributtes.Name = "panelProdSpecAttributtes";
            this.panelProdSpecAttributtes.Size = new System.Drawing.Size(898, 408);
            this.panelProdSpecAttributtes.TabIndex = 50;
            // 
            // chAddSpecAttributesAsTable
            // 
            this.chAddSpecAttributesAsTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chAddSpecAttributesAsTable.AutoSize = true;
            this.chAddSpecAttributesAsTable.Location = new System.Drawing.Point(256, 453);
            this.chAddSpecAttributesAsTable.Name = "chAddSpecAttributesAsTable";
            this.chAddSpecAttributesAsTable.Size = new System.Drawing.Size(15, 14);
            this.chAddSpecAttributesAsTable.TabIndex = 49;
            this.chAddSpecAttributesAsTable.UseVisualStyleBackColor = true;
            // 
            // label191
            // 
            this.label191.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label191.AutoSize = true;
            this.label191.Location = new System.Drawing.Point(3, 453);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(193, 13);
            this.label191.TabIndex = 48;
            this.label191.Text = "Add Attributes To Fulldescription As List";
            // 
            // chDeleteOldSpecAttributes
            // 
            this.chDeleteOldSpecAttributes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chDeleteOldSpecAttributes.AutoSize = true;
            this.chDeleteOldSpecAttributes.Checked = true;
            this.chDeleteOldSpecAttributes.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chDeleteOldSpecAttributes.Location = new System.Drawing.Point(256, 424);
            this.chDeleteOldSpecAttributes.Name = "chDeleteOldSpecAttributes";
            this.chDeleteOldSpecAttributes.Size = new System.Drawing.Size(15, 14);
            this.chDeleteOldSpecAttributes.TabIndex = 47;
            this.chDeleteOldSpecAttributes.UseVisualStyleBackColor = true;
            // 
            // label156
            // 
            this.label156.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(3, 425);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(179, 13);
            this.label156.TabIndex = 46;
            this.label156.Text = "Delete Existing Attributes On Update";
            // 
            // tabFilters
            // 
            this.tabFilters.Controls.Add(this.tableLayoutPanel7);
            this.tabFilters.Location = new System.Drawing.Point(4, 22);
            this.tabFilters.Name = "tabFilters";
            this.tabFilters.Padding = new System.Windows.Forms.Padding(3);
            this.tabFilters.Size = new System.Drawing.Size(905, 605);
            this.tabFilters.TabIndex = 6;
            this.tabFilters.Text = "Filters";
            this.tabFilters.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel7.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel7.ColumnCount = 4;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Controls.Add(this.chFilter9Map, 0, 10);
            this.tableLayoutPanel7.Controls.Add(this.lblFilter9, 0, 10);
            this.tableLayoutPanel7.Controls.Add(this.label62, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.label64, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.chFilterMap, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.chFilter1Map, 1, 2);
            this.tableLayoutPanel7.Controls.Add(this.chFilter2Map, 1, 3);
            this.tableLayoutPanel7.Controls.Add(this.chFilter3Map, 1, 4);
            this.tableLayoutPanel7.Controls.Add(this.chFilter4Map, 1, 5);
            this.tableLayoutPanel7.Controls.Add(this.lblFilter, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.lblFilter1, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.lblFilter2, 0, 3);
            this.tableLayoutPanel7.Controls.Add(this.lblFilter3, 0, 4);
            this.tableLayoutPanel7.Controls.Add(this.lblFilter4, 0, 5);
            this.tableLayoutPanel7.Controls.Add(this.lblFilter5, 0, 6);
            this.tableLayoutPanel7.Controls.Add(this.lblFilter6, 0, 7);
            this.tableLayoutPanel7.Controls.Add(this.lblFilter7, 0, 8);
            this.tableLayoutPanel7.Controls.Add(this.lblFilter8, 0, 9);
            this.tableLayoutPanel7.Controls.Add(this.chFilter5Map, 1, 6);
            this.tableLayoutPanel7.Controls.Add(this.chFilter6Map, 1, 7);
            this.tableLayoutPanel7.Controls.Add(this.chFilter7Map, 1, 8);
            this.tableLayoutPanel7.Controls.Add(this.chFilter8Map, 1, 9);
            this.tableLayoutPanel7.Controls.Add(this.tbFilterValue, 3, 1);
            this.tableLayoutPanel7.Controls.Add(this.tbFilter1Value, 3, 2);
            this.tableLayoutPanel7.Controls.Add(this.tbFilter2Value, 3, 3);
            this.tableLayoutPanel7.Controls.Add(this.tbFilter3Value, 3, 4);
            this.tableLayoutPanel7.Controls.Add(this.tbFilter4Value, 3, 5);
            this.tableLayoutPanel7.Controls.Add(this.tbFilter5Value, 3, 6);
            this.tableLayoutPanel7.Controls.Add(this.tbFilter6Value, 3, 7);
            this.tableLayoutPanel7.Controls.Add(this.tbFilter7Value, 3, 8);
            this.tableLayoutPanel7.Controls.Add(this.tbFilter8Value, 3, 9);
            this.tableLayoutPanel7.Controls.Add(this.ddFilterOp, 2, 1);
            this.tableLayoutPanel7.Controls.Add(this.label63, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this.label76, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.comboBox_0, 2, 2);
            this.tableLayoutPanel7.Controls.Add(this.comboBox_1, 2, 3);
            this.tableLayoutPanel7.Controls.Add(this.comboBox_2, 2, 4);
            this.tableLayoutPanel7.Controls.Add(this.comboBox_3, 2, 5);
            this.tableLayoutPanel7.Controls.Add(this.comboBox_4, 2, 6);
            this.tableLayoutPanel7.Controls.Add(this.comboBox_5, 2, 7);
            this.tableLayoutPanel7.Controls.Add(this.comboBox_6, 2, 8);
            this.tableLayoutPanel7.Controls.Add(this.comboBox_7, 2, 9);
            this.tableLayoutPanel7.Controls.Add(this.comboBox_8, 2, 10);
            this.tableLayoutPanel7.Controls.Add(this.tbFilter9Value, 3, 10);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(-5, -2);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 11;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(912, 349);
            this.tableLayoutPanel7.TabIndex = 4;
            // 
            // chFilter9Map
            // 
            this.chFilter9Map.AutoSize = true;
            this.chFilter9Map.Enabled = false;
            this.chFilter9Map.Location = new System.Drawing.Point(155, 324);
            this.chFilter9Map.Name = "chFilter9Map";
            this.chFilter9Map.Size = new System.Drawing.Size(15, 14);
            this.chFilter9Map.TabIndex = 89;
            this.chFilter9Map.UseVisualStyleBackColor = true;
            // 
            // lblFilter9
            // 
            this.lblFilter9.AutoSize = true;
            this.lblFilter9.Location = new System.Drawing.Point(4, 321);
            this.lblFilter9.Name = "lblFilter9";
            this.lblFilter9.Size = new System.Drawing.Size(35, 13);
            this.lblFilter9.TabIndex = 75;
            this.lblFilter9.Text = "Filter9";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(4, 1);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(89, 17);
            this.label62.TabIndex = 12;
            this.label62.Text = "Field Name";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(155, 1);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(94, 34);
            this.label64.TabIndex = 42;
            this.label64.Text = "Mapped To Source";
            // 
            // chFilterMap
            // 
            this.chFilterMap.AutoSize = true;
            this.chFilterMap.Enabled = false;
            this.chFilterMap.Location = new System.Drawing.Point(155, 45);
            this.chFilterMap.Name = "chFilterMap";
            this.chFilterMap.Size = new System.Drawing.Size(15, 14);
            this.chFilterMap.TabIndex = 43;
            this.chFilterMap.UseVisualStyleBackColor = true;
            // 
            // chFilter1Map
            // 
            this.chFilter1Map.AutoSize = true;
            this.chFilter1Map.Enabled = false;
            this.chFilter1Map.Location = new System.Drawing.Point(155, 76);
            this.chFilter1Map.Name = "chFilter1Map";
            this.chFilter1Map.Size = new System.Drawing.Size(15, 14);
            this.chFilter1Map.TabIndex = 44;
            this.chFilter1Map.UseVisualStyleBackColor = true;
            // 
            // chFilter2Map
            // 
            this.chFilter2Map.AutoSize = true;
            this.chFilter2Map.Enabled = false;
            this.chFilter2Map.Location = new System.Drawing.Point(155, 107);
            this.chFilter2Map.Name = "chFilter2Map";
            this.chFilter2Map.Size = new System.Drawing.Size(15, 14);
            this.chFilter2Map.TabIndex = 45;
            this.chFilter2Map.UseVisualStyleBackColor = true;
            // 
            // chFilter3Map
            // 
            this.chFilter3Map.AutoSize = true;
            this.chFilter3Map.Enabled = false;
            this.chFilter3Map.Location = new System.Drawing.Point(155, 138);
            this.chFilter3Map.Name = "chFilter3Map";
            this.chFilter3Map.Size = new System.Drawing.Size(15, 14);
            this.chFilter3Map.TabIndex = 46;
            this.chFilter3Map.UseVisualStyleBackColor = true;
            // 
            // chFilter4Map
            // 
            this.chFilter4Map.AutoSize = true;
            this.chFilter4Map.Enabled = false;
            this.chFilter4Map.Location = new System.Drawing.Point(155, 169);
            this.chFilter4Map.Name = "chFilter4Map";
            this.chFilter4Map.Size = new System.Drawing.Size(15, 14);
            this.chFilter4Map.TabIndex = 47;
            this.chFilter4Map.UseVisualStyleBackColor = true;
            // 
            // lblFilter
            // 
            this.lblFilter.AutoSize = true;
            this.lblFilter.Location = new System.Drawing.Point(4, 42);
            this.lblFilter.Name = "lblFilter";
            this.lblFilter.Size = new System.Drawing.Size(29, 13);
            this.lblFilter.TabIndex = 54;
            this.lblFilter.Text = "Filter";
            // 
            // lblFilter1
            // 
            this.lblFilter1.AutoSize = true;
            this.lblFilter1.Location = new System.Drawing.Point(4, 73);
            this.lblFilter1.Name = "lblFilter1";
            this.lblFilter1.Size = new System.Drawing.Size(35, 13);
            this.lblFilter1.TabIndex = 55;
            this.lblFilter1.Text = "Filter1";
            // 
            // lblFilter2
            // 
            this.lblFilter2.AutoSize = true;
            this.lblFilter2.Location = new System.Drawing.Point(4, 104);
            this.lblFilter2.Name = "lblFilter2";
            this.lblFilter2.Size = new System.Drawing.Size(35, 13);
            this.lblFilter2.TabIndex = 56;
            this.lblFilter2.Text = "Filter2";
            // 
            // lblFilter3
            // 
            this.lblFilter3.AutoSize = true;
            this.lblFilter3.Location = new System.Drawing.Point(4, 135);
            this.lblFilter3.Name = "lblFilter3";
            this.lblFilter3.Size = new System.Drawing.Size(35, 13);
            this.lblFilter3.TabIndex = 57;
            this.lblFilter3.Text = "Filter3";
            // 
            // lblFilter4
            // 
            this.lblFilter4.AutoSize = true;
            this.lblFilter4.Location = new System.Drawing.Point(4, 166);
            this.lblFilter4.Name = "lblFilter4";
            this.lblFilter4.Size = new System.Drawing.Size(35, 13);
            this.lblFilter4.TabIndex = 58;
            this.lblFilter4.Text = "Filter4";
            // 
            // lblFilter5
            // 
            this.lblFilter5.AutoSize = true;
            this.lblFilter5.Location = new System.Drawing.Point(4, 197);
            this.lblFilter5.Name = "lblFilter5";
            this.lblFilter5.Size = new System.Drawing.Size(35, 13);
            this.lblFilter5.TabIndex = 59;
            this.lblFilter5.Text = "Filter5";
            // 
            // lblFilter6
            // 
            this.lblFilter6.AutoSize = true;
            this.lblFilter6.Location = new System.Drawing.Point(4, 228);
            this.lblFilter6.Name = "lblFilter6";
            this.lblFilter6.Size = new System.Drawing.Size(35, 13);
            this.lblFilter6.TabIndex = 60;
            this.lblFilter6.Text = "Filter6";
            // 
            // lblFilter7
            // 
            this.lblFilter7.AutoSize = true;
            this.lblFilter7.Location = new System.Drawing.Point(4, 259);
            this.lblFilter7.Name = "lblFilter7";
            this.lblFilter7.Size = new System.Drawing.Size(35, 13);
            this.lblFilter7.TabIndex = 61;
            this.lblFilter7.Text = "Filter7";
            // 
            // lblFilter8
            // 
            this.lblFilter8.AutoSize = true;
            this.lblFilter8.Location = new System.Drawing.Point(4, 290);
            this.lblFilter8.Name = "lblFilter8";
            this.lblFilter8.Size = new System.Drawing.Size(35, 13);
            this.lblFilter8.TabIndex = 62;
            this.lblFilter8.Text = "Filter8";
            // 
            // chFilter5Map
            // 
            this.chFilter5Map.AutoSize = true;
            this.chFilter5Map.Enabled = false;
            this.chFilter5Map.Location = new System.Drawing.Point(155, 200);
            this.chFilter5Map.Name = "chFilter5Map";
            this.chFilter5Map.Size = new System.Drawing.Size(15, 14);
            this.chFilter5Map.TabIndex = 63;
            this.chFilter5Map.UseVisualStyleBackColor = true;
            // 
            // chFilter6Map
            // 
            this.chFilter6Map.AutoSize = true;
            this.chFilter6Map.Enabled = false;
            this.chFilter6Map.Location = new System.Drawing.Point(155, 231);
            this.chFilter6Map.Name = "chFilter6Map";
            this.chFilter6Map.Size = new System.Drawing.Size(15, 14);
            this.chFilter6Map.TabIndex = 64;
            this.chFilter6Map.UseVisualStyleBackColor = true;
            // 
            // chFilter7Map
            // 
            this.chFilter7Map.AutoSize = true;
            this.chFilter7Map.Enabled = false;
            this.chFilter7Map.Location = new System.Drawing.Point(155, 262);
            this.chFilter7Map.Name = "chFilter7Map";
            this.chFilter7Map.Size = new System.Drawing.Size(15, 14);
            this.chFilter7Map.TabIndex = 65;
            this.chFilter7Map.UseVisualStyleBackColor = true;
            // 
            // chFilter8Map
            // 
            this.chFilter8Map.AutoSize = true;
            this.chFilter8Map.Enabled = false;
            this.chFilter8Map.Location = new System.Drawing.Point(155, 293);
            this.chFilter8Map.Name = "chFilter8Map";
            this.chFilter8Map.Size = new System.Drawing.Size(15, 14);
            this.chFilter8Map.TabIndex = 66;
            this.chFilter8Map.UseVisualStyleBackColor = true;
            // 
            // tbFilterValue
            // 
            this.tbFilterValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterValue.Location = new System.Drawing.Point(457, 45);
            this.tbFilterValue.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterValue.Name = "tbFilterValue";
            this.tbFilterValue.Size = new System.Drawing.Size(434, 20);
            this.tbFilterValue.TabIndex = 27;
            // 
            // tbFilter1Value
            // 
            this.tbFilter1Value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter1Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter1Value.Location = new System.Drawing.Point(457, 76);
            this.tbFilter1Value.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter1Value.Name = "tbFilter1Value";
            this.tbFilter1Value.Size = new System.Drawing.Size(434, 20);
            this.tbFilter1Value.TabIndex = 38;
            // 
            // tbFilter2Value
            // 
            this.tbFilter2Value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter2Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter2Value.Location = new System.Drawing.Point(457, 107);
            this.tbFilter2Value.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter2Value.Name = "tbFilter2Value";
            this.tbFilter2Value.Size = new System.Drawing.Size(434, 20);
            this.tbFilter2Value.TabIndex = 39;
            // 
            // tbFilter3Value
            // 
            this.tbFilter3Value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter3Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter3Value.Location = new System.Drawing.Point(457, 138);
            this.tbFilter3Value.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter3Value.Name = "tbFilter3Value";
            this.tbFilter3Value.Size = new System.Drawing.Size(434, 20);
            this.tbFilter3Value.TabIndex = 40;
            // 
            // tbFilter4Value
            // 
            this.tbFilter4Value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter4Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter4Value.Location = new System.Drawing.Point(457, 169);
            this.tbFilter4Value.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter4Value.Name = "tbFilter4Value";
            this.tbFilter4Value.Size = new System.Drawing.Size(434, 20);
            this.tbFilter4Value.TabIndex = 41;
            // 
            // tbFilter5Value
            // 
            this.tbFilter5Value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter5Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter5Value.Location = new System.Drawing.Point(457, 200);
            this.tbFilter5Value.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter5Value.Name = "tbFilter5Value";
            this.tbFilter5Value.Size = new System.Drawing.Size(434, 20);
            this.tbFilter5Value.TabIndex = 71;
            // 
            // tbFilter6Value
            // 
            this.tbFilter6Value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter6Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter6Value.Location = new System.Drawing.Point(457, 231);
            this.tbFilter6Value.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter6Value.Name = "tbFilter6Value";
            this.tbFilter6Value.Size = new System.Drawing.Size(434, 20);
            this.tbFilter6Value.TabIndex = 72;
            // 
            // tbFilter7Value
            // 
            this.tbFilter7Value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter7Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter7Value.Location = new System.Drawing.Point(457, 262);
            this.tbFilter7Value.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter7Value.Name = "tbFilter7Value";
            this.tbFilter7Value.Size = new System.Drawing.Size(434, 20);
            this.tbFilter7Value.TabIndex = 73;
            // 
            // tbFilter8Value
            // 
            this.tbFilter8Value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter8Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter8Value.Location = new System.Drawing.Point(457, 293);
            this.tbFilter8Value.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter8Value.Name = "tbFilter8Value";
            this.tbFilter8Value.Size = new System.Drawing.Size(434, 20);
            this.tbFilter8Value.TabIndex = 74;
            // 
            // ddFilterOp
            // 
            this.ddFilterOp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddFilterOp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddFilterOp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddFilterOp.FormattingEnabled = true;
            this.ddFilterOp.Items.AddRange(new object[] {
            "",
            "== (equal)",
            "<= (less or equal)",
            ">= (more or equal)",
            "!= (not equal)",
            "%Like% (contains)",
            "%Like (starts with)",
            "Like% (ends with)",
            "%Not Like% (not contains)"});
            this.ddFilterOp.Location = new System.Drawing.Point(256, 45);
            this.ddFilterOp.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddFilterOp.Name = "ddFilterOp";
            this.ddFilterOp.Size = new System.Drawing.Size(177, 21);
            this.ddFilterOp.TabIndex = 77;
            this.ddFilterOp.SelectedIndexChanged += new System.EventHandler(this.ddFilterOp_SelectedIndexChanged);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(457, 1);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(49, 17);
            this.label63.TabIndex = 22;
            this.label63.Text = "Value";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(256, 1);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(80, 17);
            this.label76.TabIndex = 78;
            this.label76.Text = "Operation";
            // 
            // comboBox_0
            // 
            this.comboBox_0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox_0.FormattingEnabled = true;
            this.comboBox_0.Items.AddRange(new object[] {
            "",
            "== (equal)",
            "<= (less or equal)",
            ">= (more or equal)",
            "!= (not equal)",
            "%Like% (contains)",
            "%Like (starts with)",
            "Like% (ends with)",
            "%Not Like% (not contains)"});
            this.comboBox_0.Location = new System.Drawing.Point(256, 76);
            this.comboBox_0.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.comboBox_0.Name = "comboBox_0";
            this.comboBox_0.Size = new System.Drawing.Size(177, 21);
            this.comboBox_0.TabIndex = 79;
            // 
            // comboBox_1
            // 
            this.comboBox_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox_1.FormattingEnabled = true;
            this.comboBox_1.Items.AddRange(new object[] {
            "",
            "== (equal)",
            "<= (less or equal)",
            ">= (more or equal)",
            "!= (not equal)",
            "%Like% (contains)",
            "%Like (starts with)",
            "Like% (ends with)",
            "%Not Like% (not contains)"});
            this.comboBox_1.Location = new System.Drawing.Point(256, 107);
            this.comboBox_1.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.comboBox_1.Name = "comboBox_1";
            this.comboBox_1.Size = new System.Drawing.Size(177, 21);
            this.comboBox_1.TabIndex = 80;
            // 
            // comboBox_2
            // 
            this.comboBox_2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox_2.FormattingEnabled = true;
            this.comboBox_2.Items.AddRange(new object[] {
            "",
            "== (equal)",
            "<= (less or equal)",
            ">= (more or equal)",
            "!= (not equal)",
            "%Like% (contains)",
            "%Like (starts with)",
            "Like% (ends with)",
            "%Not Like% (not contains)"});
            this.comboBox_2.Location = new System.Drawing.Point(256, 138);
            this.comboBox_2.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.comboBox_2.Name = "comboBox_2";
            this.comboBox_2.Size = new System.Drawing.Size(177, 21);
            this.comboBox_2.TabIndex = 81;
            // 
            // comboBox_3
            // 
            this.comboBox_3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox_3.FormattingEnabled = true;
            this.comboBox_3.Items.AddRange(new object[] {
            "",
            "== (equal)",
            "<= (less or equal)",
            ">= (more or equal)",
            "!= (not equal)",
            "%Like% (contains)",
            "%Like (starts with)",
            "Like% (ends with)",
            "%Not Like% (not contains)"});
            this.comboBox_3.Location = new System.Drawing.Point(256, 169);
            this.comboBox_3.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.comboBox_3.Name = "comboBox_3";
            this.comboBox_3.Size = new System.Drawing.Size(177, 21);
            this.comboBox_3.TabIndex = 82;
            // 
            // comboBox_4
            // 
            this.comboBox_4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox_4.FormattingEnabled = true;
            this.comboBox_4.Items.AddRange(new object[] {
            "",
            "== (equal)",
            "<= (less or equal)",
            ">= (more or equal)",
            "!= (not equal)",
            "%Like% (contains)",
            "%Like (starts with)",
            "Like% (ends with)",
            "%Not Like% (not contains)"});
            this.comboBox_4.Location = new System.Drawing.Point(256, 200);
            this.comboBox_4.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.comboBox_4.Name = "comboBox_4";
            this.comboBox_4.Size = new System.Drawing.Size(177, 21);
            this.comboBox_4.TabIndex = 83;
            // 
            // comboBox_5
            // 
            this.comboBox_5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox_5.FormattingEnabled = true;
            this.comboBox_5.Items.AddRange(new object[] {
            "",
            "== (equal)",
            "<= (less or equal)",
            ">= (more or equal)",
            "!= (not equal)",
            "%Like% (contains)",
            "%Like (starts with)",
            "Like% (ends with)",
            "%Not Like% (not contains)"});
            this.comboBox_5.Location = new System.Drawing.Point(256, 231);
            this.comboBox_5.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.comboBox_5.Name = "comboBox_5";
            this.comboBox_5.Size = new System.Drawing.Size(177, 21);
            this.comboBox_5.TabIndex = 84;
            // 
            // comboBox_6
            // 
            this.comboBox_6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox_6.FormattingEnabled = true;
            this.comboBox_6.Items.AddRange(new object[] {
            "",
            "== (equal)",
            "<= (less or equal)",
            ">= (more or equal)",
            "!= (not equal)",
            "%Like% (contains)",
            "%Like (starts with)",
            "Like% (ends with)",
            "%Not Like% (not contains)"});
            this.comboBox_6.Location = new System.Drawing.Point(256, 262);
            this.comboBox_6.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.comboBox_6.Name = "comboBox_6";
            this.comboBox_6.Size = new System.Drawing.Size(177, 21);
            this.comboBox_6.TabIndex = 85;
            // 
            // comboBox_7
            // 
            this.comboBox_7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox_7.FormattingEnabled = true;
            this.comboBox_7.Items.AddRange(new object[] {
            "",
            "== (equal)",
            "<= (less or equal)",
            ">= (more or equal)",
            "!= (not equal)",
            "%Like% (contains)",
            "%Like (starts with)",
            "Like% (ends with)",
            "%Not Like% (not contains)"});
            this.comboBox_7.Location = new System.Drawing.Point(256, 293);
            this.comboBox_7.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.comboBox_7.Name = "comboBox_7";
            this.comboBox_7.Size = new System.Drawing.Size(177, 21);
            this.comboBox_7.TabIndex = 86;
            // 
            // comboBox_8
            // 
            this.comboBox_8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox_8.FormattingEnabled = true;
            this.comboBox_8.Items.AddRange(new object[] {
            "",
            "== (equal)",
            "<= (less or equal)",
            ">= (more or equal)",
            "!= (not equal)",
            "%Like% (contains)",
            "%Like (starts with)",
            "Like% (ends with)",
            "%Not Like% (not contains)"});
            this.comboBox_8.Location = new System.Drawing.Point(256, 324);
            this.comboBox_8.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.comboBox_8.Name = "comboBox_8";
            this.comboBox_8.Size = new System.Drawing.Size(177, 21);
            this.comboBox_8.TabIndex = 87;
            // 
            // tbFilter9Value
            // 
            this.tbFilter9Value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter9Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter9Value.Location = new System.Drawing.Point(457, 324);
            this.tbFilter9Value.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter9Value.Name = "tbFilter9Value";
            this.tbFilter9Value.Size = new System.Drawing.Size(434, 20);
            this.tbFilter9Value.TabIndex = 88;
            // 
            // tabRelatedTasks
            // 
            this.tabRelatedTasks.Controls.Add(this.tableLayoutPanel17);
            this.tabRelatedTasks.Location = new System.Drawing.Point(4, 22);
            this.tabRelatedTasks.Name = "tabRelatedTasks";
            this.tabRelatedTasks.Padding = new System.Windows.Forms.Padding(3);
            this.tabRelatedTasks.Size = new System.Drawing.Size(905, 605);
            this.tabRelatedTasks.TabIndex = 7;
            this.tabRelatedTasks.Text = "Related Tasks";
            this.tabRelatedTasks.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel17.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel17.ColumnCount = 2;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel17.Controls.Add(this.label153, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.tbRelatedSourceAddress, 1, 1);
            this.tableLayoutPanel17.Controls.Add(this.ddRelatedTask, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.label154, 1, 0);
            this.tableLayoutPanel17.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 12;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(904, 351);
            this.tableLayoutPanel17.TabIndex = 4;
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label153.Location = new System.Drawing.Point(4, 1);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(104, 17);
            this.label153.TabIndex = 13;
            this.label153.Text = "Related Task";
            // 
            // tbRelatedSourceAddress
            // 
            this.tbRelatedSourceAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRelatedSourceAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbRelatedSourceAddress.Location = new System.Drawing.Point(255, 45);
            this.tbRelatedSourceAddress.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbRelatedSourceAddress.Name = "tbRelatedSourceAddress";
            this.tbRelatedSourceAddress.Size = new System.Drawing.Size(628, 20);
            this.tbRelatedSourceAddress.TabIndex = 27;
            // 
            // ddRelatedTask
            // 
            this.ddRelatedTask.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddRelatedTask.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddRelatedTask.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddRelatedTask.FormattingEnabled = true;
            this.ddRelatedTask.Items.AddRange(new object[] {
            "Minutely",
            "Hourly",
            "Daily"});
            this.ddRelatedTask.Location = new System.Drawing.Point(4, 45);
            this.ddRelatedTask.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddRelatedTask.Name = "ddRelatedTask";
            this.ddRelatedTask.Size = new System.Drawing.Size(227, 21);
            this.ddRelatedTask.TabIndex = 30;
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label154.Location = new System.Drawing.Point(255, 1);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(224, 17);
            this.label154.TabIndex = 22;
            this.label154.Text = "Related Task Source Address";
            // 
            // tabPrices
            // 
            this.tabPrices.Controls.Add(this.panelPrices);
            this.tabPrices.Location = new System.Drawing.Point(4, 22);
            this.tabPrices.Margin = new System.Windows.Forms.Padding(2);
            this.tabPrices.Name = "tabPrices";
            this.tabPrices.Size = new System.Drawing.Size(905, 605);
            this.tabPrices.TabIndex = 23;
            this.tabPrices.Text = "Prices";
            this.tabPrices.UseVisualStyleBackColor = true;
            // 
            // panelPrices
            // 
            this.panelPrices.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelPrices.Location = new System.Drawing.Point(2, 2);
            this.panelPrices.Margin = new System.Windows.Forms.Padding(2);
            this.panelPrices.Name = "panelPrices";
            this.panelPrices.Size = new System.Drawing.Size(898, 408);
            this.panelPrices.TabIndex = 59;
            // 
            // tabTierPricing
            // 
            this.tabTierPricing.Controls.Add(this.label48);
            this.tabTierPricing.Controls.Add(this.panelTierPricing);
            this.tabTierPricing.Controls.Add(this.chDeleteExistingTierPricingOnUpdate);
            this.tabTierPricing.Location = new System.Drawing.Point(4, 22);
            this.tabTierPricing.Margin = new System.Windows.Forms.Padding(2);
            this.tabTierPricing.Name = "tabTierPricing";
            this.tabTierPricing.Size = new System.Drawing.Size(905, 605);
            this.tabTierPricing.TabIndex = 12;
            this.tabTierPricing.Text = "Tier Pricing";
            this.tabTierPricing.UseVisualStyleBackColor = true;
            // 
            // label48
            // 
            this.label48.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(8, 425);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(188, 13);
            this.label48.TabIndex = 59;
            this.label48.Text = "Delete Existing Tier Pricing On Update";
            // 
            // panelTierPricing
            // 
            this.panelTierPricing.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelTierPricing.Location = new System.Drawing.Point(2, 2);
            this.panelTierPricing.Margin = new System.Windows.Forms.Padding(2);
            this.panelTierPricing.Name = "panelTierPricing";
            this.panelTierPricing.Size = new System.Drawing.Size(898, 408);
            this.panelTierPricing.TabIndex = 58;
            // 
            // chDeleteExistingTierPricingOnUpdate
            // 
            this.chDeleteExistingTierPricingOnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chDeleteExistingTierPricingOnUpdate.AutoSize = true;
            this.chDeleteExistingTierPricingOnUpdate.Checked = true;
            this.chDeleteExistingTierPricingOnUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chDeleteExistingTierPricingOnUpdate.Location = new System.Drawing.Point(255, 424);
            this.chDeleteExistingTierPricingOnUpdate.Name = "chDeleteExistingTierPricingOnUpdate";
            this.chDeleteExistingTierPricingOnUpdate.Size = new System.Drawing.Size(15, 14);
            this.chDeleteExistingTierPricingOnUpdate.TabIndex = 57;
            this.chDeleteExistingTierPricingOnUpdate.UseVisualStyleBackColor = true;
            // 
            // tabTranslations
            // 
            this.tabTranslations.Controls.Add(this.tableLayoutPanel12);
            this.tabTranslations.Location = new System.Drawing.Point(4, 22);
            this.tabTranslations.Margin = new System.Windows.Forms.Padding(2);
            this.tabTranslations.Name = "tabTranslations";
            this.tabTranslations.Padding = new System.Windows.Forms.Padding(2);
            this.tabTranslations.Size = new System.Drawing.Size(905, 605);
            this.tabTranslations.TabIndex = 13;
            this.tabTranslations.Text = "Translations";
            this.tabTranslations.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel12.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Controls.Add(this.label178, 0, 4);
            this.tableLayoutPanel12.Controls.Add(this.label179, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.label180, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.label181, 0, 2);
            this.tableLayoutPanel12.Controls.Add(this.label182, 0, 3);
            this.tableLayoutPanel12.Controls.Add(this.label183, 0, 5);
            this.tableLayoutPanel12.Controls.Add(this.label186, 0, 6);
            this.tableLayoutPanel12.Controls.Add(this.label187, 0, 7);
            this.tableLayoutPanel12.Controls.Add(this.label188, 0, 8);
            this.tableLayoutPanel12.Controls.Add(this.tbTranslateApiUrl, 1, 7);
            this.tableLayoutPanel12.Controls.Add(this.tbTranslateSubscribtionKey, 1, 8);
            this.tableLayoutPanel12.Controls.Add(this.label197, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.ddTranslateLanguagesList, 1, 1);
            this.tableLayoutPanel12.Controls.Add(this.chTranslateProductName, 1, 2);
            this.tableLayoutPanel12.Controls.Add(this.chTranslateProductShortDesc, 1, 3);
            this.tableLayoutPanel12.Controls.Add(this.chTranslateProductFullDesc, 1, 4);
            this.tableLayoutPanel12.Controls.Add(this.chTranslateProductCategory, 1, 5);
            this.tableLayoutPanel12.Controls.Add(this.chTranslateProductAtt, 1, 6);
            this.tableLayoutPanel12.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 11;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(908, 602);
            this.tableLayoutPanel12.TabIndex = 3;
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(4, 114);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(119, 13);
            this.label178.TabIndex = 29;
            this.label178.Text = "Product Full Description";
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label179.Location = new System.Drawing.Point(4, 1);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(102, 17);
            this.label179.TabIndex = 12;
            this.label179.Text = "Option Name";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label180.Location = new System.Drawing.Point(155, 1);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(102, 17);
            this.label180.TabIndex = 22;
            this.label180.Text = "Option Value";
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Location = new System.Drawing.Point(4, 66);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(75, 13);
            this.label181.TabIndex = 23;
            this.label181.Text = "Product Name";
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(4, 90);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(128, 13);
            this.label182.TabIndex = 25;
            this.label182.Text = "Product Short Description";
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Location = new System.Drawing.Point(4, 138);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(80, 13);
            this.label183.TabIndex = 30;
            this.label183.Text = "Category Name";
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(4, 162);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(91, 13);
            this.label186.TabIndex = 49;
            this.label186.Text = "Product Attributes";
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Location = new System.Drawing.Point(4, 186);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(134, 13);
            this.label187.TabIndex = 50;
            this.label187.Text = "Microsoft Translator Api Url";
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Location = new System.Drawing.Point(4, 210);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(86, 13);
            this.label188.TabIndex = 51;
            this.label188.Text = "Subscription Key";
            // 
            // tbTranslateApiUrl
            // 
            this.tbTranslateApiUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTranslateApiUrl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbTranslateApiUrl.Location = new System.Drawing.Point(155, 189);
            this.tbTranslateApiUrl.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbTranslateApiUrl.Name = "tbTranslateApiUrl";
            this.tbTranslateApiUrl.Size = new System.Drawing.Size(732, 20);
            this.tbTranslateApiUrl.TabIndex = 70;
            // 
            // tbTranslateSubscribtionKey
            // 
            this.tbTranslateSubscribtionKey.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTranslateSubscribtionKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbTranslateSubscribtionKey.Location = new System.Drawing.Point(155, 213);
            this.tbTranslateSubscribtionKey.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbTranslateSubscribtionKey.Name = "tbTranslateSubscribtionKey";
            this.tbTranslateSubscribtionKey.Size = new System.Drawing.Size(732, 20);
            this.tbTranslateSubscribtionKey.TabIndex = 71;
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Location = new System.Drawing.Point(4, 39);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(55, 13);
            this.label197.TabIndex = 89;
            this.label197.Text = "Language";
            // 
            // ddTranslateLanguagesList
            // 
            this.ddTranslateLanguagesList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddTranslateLanguagesList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddTranslateLanguagesList.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddTranslateLanguagesList.FormattingEnabled = true;
            this.ddTranslateLanguagesList.Location = new System.Drawing.Point(155, 42);
            this.ddTranslateLanguagesList.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddTranslateLanguagesList.Name = "ddTranslateLanguagesList";
            this.ddTranslateLanguagesList.Size = new System.Drawing.Size(732, 21);
            this.ddTranslateLanguagesList.TabIndex = 127;
            // 
            // chTranslateProductName
            // 
            this.chTranslateProductName.AutoSize = true;
            this.chTranslateProductName.Location = new System.Drawing.Point(155, 69);
            this.chTranslateProductName.Name = "chTranslateProductName";
            this.chTranslateProductName.Size = new System.Drawing.Size(15, 14);
            this.chTranslateProductName.TabIndex = 128;
            this.chTranslateProductName.UseVisualStyleBackColor = true;
            // 
            // chTranslateProductShortDesc
            // 
            this.chTranslateProductShortDesc.AutoSize = true;
            this.chTranslateProductShortDesc.Location = new System.Drawing.Point(155, 93);
            this.chTranslateProductShortDesc.Name = "chTranslateProductShortDesc";
            this.chTranslateProductShortDesc.Size = new System.Drawing.Size(15, 14);
            this.chTranslateProductShortDesc.TabIndex = 129;
            this.chTranslateProductShortDesc.UseVisualStyleBackColor = true;
            // 
            // chTranslateProductFullDesc
            // 
            this.chTranslateProductFullDesc.AutoSize = true;
            this.chTranslateProductFullDesc.Location = new System.Drawing.Point(155, 117);
            this.chTranslateProductFullDesc.Name = "chTranslateProductFullDesc";
            this.chTranslateProductFullDesc.Size = new System.Drawing.Size(15, 14);
            this.chTranslateProductFullDesc.TabIndex = 130;
            this.chTranslateProductFullDesc.UseVisualStyleBackColor = true;
            // 
            // chTranslateProductCategory
            // 
            this.chTranslateProductCategory.AutoSize = true;
            this.chTranslateProductCategory.Location = new System.Drawing.Point(155, 141);
            this.chTranslateProductCategory.Name = "chTranslateProductCategory";
            this.chTranslateProductCategory.Size = new System.Drawing.Size(15, 14);
            this.chTranslateProductCategory.TabIndex = 131;
            this.chTranslateProductCategory.UseVisualStyleBackColor = true;
            // 
            // chTranslateProductAtt
            // 
            this.chTranslateProductAtt.AutoSize = true;
            this.chTranslateProductAtt.Location = new System.Drawing.Point(155, 165);
            this.chTranslateProductAtt.Name = "chTranslateProductAtt";
            this.chTranslateProductAtt.Size = new System.Drawing.Size(15, 14);
            this.chTranslateProductAtt.TabIndex = 132;
            this.chTranslateProductAtt.UseVisualStyleBackColor = true;
            // 
            // tabCustomSql
            // 
            this.tabCustomSql.Controls.Add(this.tableLayoutPanel6);
            this.tabCustomSql.Location = new System.Drawing.Point(4, 22);
            this.tabCustomSql.Margin = new System.Windows.Forms.Padding(2);
            this.tabCustomSql.Name = "tabCustomSql";
            this.tabCustomSql.Padding = new System.Windows.Forms.Padding(2);
            this.tabCustomSql.Size = new System.Drawing.Size(905, 605);
            this.tabCustomSql.TabIndex = 14;
            this.tabCustomSql.Text = "Custom SQL Queries";
            this.tabCustomSql.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.label51, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label52, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.label53, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.label59, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.cbRunSqlOn, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.tbSqlQueries, 1, 2);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(-2, 7);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 3;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(908, 595);
            this.tableLayoutPanel6.TabIndex = 4;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(4, 1);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(102, 17);
            this.label51.TabIndex = 12;
            this.label51.Text = "Option Name";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(155, 1);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(102, 17);
            this.label52.TabIndex = 22;
            this.label52.Text = "Option Value";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(4, 66);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(67, 13);
            this.label53.TabIndex = 23;
            this.label53.Text = "SQL Queries";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(4, 39);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(131, 13);
            this.label59.TabIndex = 89;
            this.label59.Text = "Run Custom SQL Queries ";
            // 
            // cbRunSqlOn
            // 
            this.cbRunSqlOn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbRunSqlOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRunSqlOn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbRunSqlOn.FormattingEnabled = true;
            this.cbRunSqlOn.Location = new System.Drawing.Point(155, 42);
            this.cbRunSqlOn.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.cbRunSqlOn.Name = "cbRunSqlOn";
            this.cbRunSqlOn.Size = new System.Drawing.Size(732, 21);
            this.cbRunSqlOn.TabIndex = 127;
            // 
            // tbSqlQueries
            // 
            this.tbSqlQueries.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSqlQueries.Location = new System.Drawing.Point(154, 68);
            this.tbSqlQueries.Margin = new System.Windows.Forms.Padding(2);
            this.tbSqlQueries.Multiline = true;
            this.tbSqlQueries.Name = "tbSqlQueries";
            this.tbSqlQueries.Size = new System.Drawing.Size(751, 524);
            this.tbSqlQueries.TabIndex = 128;
            // 
            // tabCustomerCustomAtt
            // 
            this.tabCustomerCustomAtt.Controls.Add(this.panelCustomerCustomAtt);
            this.tabCustomerCustomAtt.Controls.Add(this.chDeleteOldCustomerCustomAtt);
            this.tabCustomerCustomAtt.Controls.Add(this.label54);
            this.tabCustomerCustomAtt.Location = new System.Drawing.Point(4, 22);
            this.tabCustomerCustomAtt.Margin = new System.Windows.Forms.Padding(2);
            this.tabCustomerCustomAtt.Name = "tabCustomerCustomAtt";
            this.tabCustomerCustomAtt.Size = new System.Drawing.Size(905, 605);
            this.tabCustomerCustomAtt.TabIndex = 16;
            this.tabCustomerCustomAtt.Text = "Customer Custom Attributtes";
            this.tabCustomerCustomAtt.UseVisualStyleBackColor = true;
            // 
            // panelCustomerCustomAtt
            // 
            this.panelCustomerCustomAtt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelCustomerCustomAtt.Location = new System.Drawing.Point(3, 2);
            this.panelCustomerCustomAtt.Margin = new System.Windows.Forms.Padding(2);
            this.panelCustomerCustomAtt.Name = "panelCustomerCustomAtt";
            this.panelCustomerCustomAtt.Size = new System.Drawing.Size(898, 408);
            this.panelCustomerCustomAtt.TabIndex = 53;
            // 
            // chDeleteOldCustomerCustomAtt
            // 
            this.chDeleteOldCustomerCustomAtt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chDeleteOldCustomerCustomAtt.AutoSize = true;
            this.chDeleteOldCustomerCustomAtt.Checked = true;
            this.chDeleteOldCustomerCustomAtt.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chDeleteOldCustomerCustomAtt.Location = new System.Drawing.Point(256, 424);
            this.chDeleteOldCustomerCustomAtt.Name = "chDeleteOldCustomerCustomAtt";
            this.chDeleteOldCustomerCustomAtt.Size = new System.Drawing.Size(15, 14);
            this.chDeleteOldCustomerCustomAtt.TabIndex = 52;
            this.chDeleteOldCustomerCustomAtt.UseVisualStyleBackColor = true;
            // 
            // label54
            // 
            this.label54.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(3, 425);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(179, 13);
            this.label54.TabIndex = 51;
            this.label54.Text = "Delete Existing Attributes On Update";
            // 
            // tabAddressCustomAtt
            // 
            this.tabAddressCustomAtt.Controls.Add(this.panelCustomerGenericAtt);
            this.tabAddressCustomAtt.Controls.Add(this.chDeleteOldAddressCustomAtt);
            this.tabAddressCustomAtt.Controls.Add(this.label44);
            this.tabAddressCustomAtt.Location = new System.Drawing.Point(4, 22);
            this.tabAddressCustomAtt.Margin = new System.Windows.Forms.Padding(2);
            this.tabAddressCustomAtt.Name = "tabAddressCustomAtt";
            this.tabAddressCustomAtt.Size = new System.Drawing.Size(905, 605);
            this.tabAddressCustomAtt.TabIndex = 15;
            this.tabAddressCustomAtt.Text = "Address Custom Attributtes";
            this.tabAddressCustomAtt.UseVisualStyleBackColor = true;
            // 
            // panelCustomerGenericAtt
            // 
            this.panelCustomerGenericAtt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelCustomerGenericAtt.Location = new System.Drawing.Point(2, 2);
            this.panelCustomerGenericAtt.Margin = new System.Windows.Forms.Padding(2);
            this.panelCustomerGenericAtt.Name = "panelCustomerGenericAtt";
            this.panelCustomerGenericAtt.Size = new System.Drawing.Size(898, 408);
            this.panelCustomerGenericAtt.TabIndex = 53;
            // 
            // chDeleteOldAddressCustomAtt
            // 
            this.chDeleteOldAddressCustomAtt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chDeleteOldAddressCustomAtt.AutoSize = true;
            this.chDeleteOldAddressCustomAtt.Checked = true;
            this.chDeleteOldAddressCustomAtt.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chDeleteOldAddressCustomAtt.Location = new System.Drawing.Point(255, 424);
            this.chDeleteOldAddressCustomAtt.Name = "chDeleteOldAddressCustomAtt";
            this.chDeleteOldAddressCustomAtt.Size = new System.Drawing.Size(15, 14);
            this.chDeleteOldAddressCustomAtt.TabIndex = 52;
            this.chDeleteOldAddressCustomAtt.UseVisualStyleBackColor = true;
            // 
            // label44
            // 
            this.label44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(2, 425);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(179, 13);
            this.label44.TabIndex = 51;
            this.label44.Text = "Delete Existing Attributes On Update";
            // 
            // tabProdInfo
            // 
            this.tabProdInfo.Controls.Add(this.panelProdInfo);
            this.tabProdInfo.Location = new System.Drawing.Point(4, 22);
            this.tabProdInfo.Margin = new System.Windows.Forms.Padding(2);
            this.tabProdInfo.Name = "tabProdInfo";
            this.tabProdInfo.Padding = new System.Windows.Forms.Padding(2);
            this.tabProdInfo.Size = new System.Drawing.Size(905, 605);
            this.tabProdInfo.TabIndex = 18;
            this.tabProdInfo.Text = "Others";
            this.tabProdInfo.UseVisualStyleBackColor = true;
            // 
            // panelProdInfo
            // 
            this.panelProdInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelProdInfo.Location = new System.Drawing.Point(2, 5);
            this.panelProdInfo.Margin = new System.Windows.Forms.Padding(2);
            this.panelProdInfo.Name = "panelProdInfo";
            this.panelProdInfo.Size = new System.Drawing.Size(898, 408);
            this.panelProdInfo.TabIndex = 54;
            // 
            // tabLogSettings
            // 
            this.tabLogSettings.Controls.Add(this.tableLayoutPanel10);
            this.tabLogSettings.Location = new System.Drawing.Point(4, 22);
            this.tabLogSettings.Margin = new System.Windows.Forms.Padding(2);
            this.tabLogSettings.Name = "tabLogSettings";
            this.tabLogSettings.Size = new System.Drawing.Size(905, 605);
            this.tabLogSettings.TabIndex = 17;
            this.tabLogSettings.Text = "Notification Settings";
            this.tabLogSettings.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel10.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.label108, 0, 10);
            this.tableLayoutPanel10.Controls.Add(this.label60, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.label61, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.label72, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.label73, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.label74, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.label78, 0, 5);
            this.tableLayoutPanel10.Controls.Add(this.label99, 0, 6);
            this.tableLayoutPanel10.Controls.Add(this.label100, 0, 7);
            this.tableLayoutPanel10.Controls.Add(this.label105, 0, 8);
            this.tableLayoutPanel10.Controls.Add(this.tbEmailTo, 1, 8);
            this.tableLayoutPanel10.Controls.Add(this.label106, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.tbEmailAddressFrom, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.checkBox_0, 1, 7);
            this.tableLayoutPanel10.Controls.Add(this.tbEmailDisplayName, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.tbEmailHost, 1, 3);
            this.tableLayoutPanel10.Controls.Add(this.tbEmailPort, 1, 4);
            this.tableLayoutPanel10.Controls.Add(this.tbEmailUser, 1, 5);
            this.tableLayoutPanel10.Controls.Add(this.tbEmailPassword, 1, 6);
            this.tableLayoutPanel10.Controls.Add(this.label107, 0, 9);
            this.tableLayoutPanel10.Controls.Add(this.tbEmailSubject, 1, 9);
            this.tableLayoutPanel10.Controls.Add(this.tbEmailBody, 1, 10);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(2, 3);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 11;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(902, 601);
            this.tableLayoutPanel10.TabIndex = 4;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(4, 258);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(59, 13);
            this.label108.TabIndex = 140;
            this.label108.Text = "Email Body";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(4, 114);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(26, 13);
            this.label60.TabIndex = 29;
            this.label60.Text = "Port";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(4, 1);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(102, 17);
            this.label61.TabIndex = 12;
            this.label61.Text = "Option Name";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(155, 1);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(102, 17);
            this.label72.TabIndex = 22;
            this.label72.Text = "Option Value";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(4, 66);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(100, 13);
            this.label73.TabIndex = 23;
            this.label73.Text = "Email Display Name";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(4, 90);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(29, 13);
            this.label74.TabIndex = 25;
            this.label74.Text = "Host";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(4, 138);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(29, 13);
            this.label78.TabIndex = 30;
            this.label78.Text = "User";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(4, 162);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(53, 13);
            this.label99.TabIndex = 49;
            this.label99.Text = "Password";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(4, 186);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(27, 13);
            this.label100.TabIndex = 50;
            this.label100.Text = "SSL";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(4, 210);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(76, 13);
            this.label105.TabIndex = 51;
            this.label105.Text = "Send Email To";
            // 
            // tbEmailTo
            // 
            this.tbEmailTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEmailTo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbEmailTo.Location = new System.Drawing.Point(155, 213);
            this.tbEmailTo.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbEmailTo.Name = "tbEmailTo";
            this.tbEmailTo.Size = new System.Drawing.Size(726, 20);
            this.tbEmailTo.TabIndex = 71;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(4, 39);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(99, 13);
            this.label106.TabIndex = 89;
            this.label106.Text = "Email Address From";
            // 
            // tbEmailAddressFrom
            // 
            this.tbEmailAddressFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEmailAddressFrom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbEmailAddressFrom.Location = new System.Drawing.Point(155, 42);
            this.tbEmailAddressFrom.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbEmailAddressFrom.Name = "tbEmailAddressFrom";
            this.tbEmailAddressFrom.Size = new System.Drawing.Size(726, 20);
            this.tbEmailAddressFrom.TabIndex = 70;
            // 
            // checkBox_0
            // 
            this.checkBox_0.AutoSize = true;
            this.checkBox_0.Location = new System.Drawing.Point(155, 189);
            this.checkBox_0.Name = "checkBox_0";
            this.checkBox_0.Size = new System.Drawing.Size(15, 14);
            this.checkBox_0.TabIndex = 132;
            this.checkBox_0.UseVisualStyleBackColor = true;
            // 
            // tbEmailDisplayName
            // 
            this.tbEmailDisplayName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEmailDisplayName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbEmailDisplayName.Location = new System.Drawing.Point(155, 69);
            this.tbEmailDisplayName.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbEmailDisplayName.Name = "tbEmailDisplayName";
            this.tbEmailDisplayName.Size = new System.Drawing.Size(726, 20);
            this.tbEmailDisplayName.TabIndex = 133;
            // 
            // tbEmailHost
            // 
            this.tbEmailHost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEmailHost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbEmailHost.Location = new System.Drawing.Point(155, 93);
            this.tbEmailHost.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbEmailHost.Name = "tbEmailHost";
            this.tbEmailHost.Size = new System.Drawing.Size(726, 20);
            this.tbEmailHost.TabIndex = 134;
            // 
            // tbEmailPort
            // 
            this.tbEmailPort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEmailPort.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbEmailPort.Location = new System.Drawing.Point(155, 117);
            this.tbEmailPort.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbEmailPort.Name = "tbEmailPort";
            this.tbEmailPort.Size = new System.Drawing.Size(726, 20);
            this.tbEmailPort.TabIndex = 135;
            // 
            // tbEmailUser
            // 
            this.tbEmailUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEmailUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbEmailUser.Location = new System.Drawing.Point(155, 141);
            this.tbEmailUser.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbEmailUser.Name = "tbEmailUser";
            this.tbEmailUser.Size = new System.Drawing.Size(726, 20);
            this.tbEmailUser.TabIndex = 136;
            // 
            // tbEmailPassword
            // 
            this.tbEmailPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEmailPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbEmailPassword.Location = new System.Drawing.Point(155, 165);
            this.tbEmailPassword.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbEmailPassword.Name = "tbEmailPassword";
            this.tbEmailPassword.Size = new System.Drawing.Size(726, 20);
            this.tbEmailPassword.TabIndex = 137;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(4, 234);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(71, 13);
            this.label107.TabIndex = 138;
            this.label107.Text = "Email Subject";
            // 
            // tbEmailSubject
            // 
            this.tbEmailSubject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEmailSubject.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbEmailSubject.Location = new System.Drawing.Point(155, 237);
            this.tbEmailSubject.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbEmailSubject.Name = "tbEmailSubject";
            this.tbEmailSubject.Size = new System.Drawing.Size(726, 20);
            this.tbEmailSubject.TabIndex = 139;
            // 
            // tbEmailBody
            // 
            this.tbEmailBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEmailBody.Location = new System.Drawing.Point(154, 260);
            this.tbEmailBody.Margin = new System.Windows.Forms.Padding(2);
            this.tbEmailBody.Multiline = true;
            this.tbEmailBody.Name = "tbEmailBody";
            this.tbEmailBody.Size = new System.Drawing.Size(745, 338);
            this.tbEmailBody.TabIndex = 141;
            // 
            // tabCustomer
            // 
            this.tabCustomer.Controls.Add(this.panelCustomer);
            this.tabCustomer.Location = new System.Drawing.Point(4, 22);
            this.tabCustomer.Margin = new System.Windows.Forms.Padding(2);
            this.tabCustomer.Name = "tabCustomer";
            this.tabCustomer.Size = new System.Drawing.Size(905, 605);
            this.tabCustomer.TabIndex = 11;
            this.tabCustomer.Text = "Customer";
            this.tabCustomer.UseVisualStyleBackColor = true;
            // 
            // panelCustomer
            // 
            this.panelCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelCustomer.Location = new System.Drawing.Point(2, 2);
            this.panelCustomer.Margin = new System.Windows.Forms.Padding(2);
            this.panelCustomer.Name = "panelCustomer";
            this.panelCustomer.Size = new System.Drawing.Size(898, 408);
            this.panelCustomer.TabIndex = 55;
            // 
            // tabCustomerShippingAddress
            // 
            this.tabCustomerShippingAddress.Controls.Add(this.chForceShippingAddressOnUpdate);
            this.tabCustomerShippingAddress.Controls.Add(this.label46);
            this.tabCustomerShippingAddress.Controls.Add(this.panelCustomerShippingAddress);
            this.tabCustomerShippingAddress.Controls.Add(this.chCustomerAddress_DeleteOld);
            this.tabCustomerShippingAddress.Controls.Add(this.label43);
            this.tabCustomerShippingAddress.Location = new System.Drawing.Point(4, 22);
            this.tabCustomerShippingAddress.Margin = new System.Windows.Forms.Padding(2);
            this.tabCustomerShippingAddress.Name = "tabCustomerShippingAddress";
            this.tabCustomerShippingAddress.Size = new System.Drawing.Size(905, 605);
            this.tabCustomerShippingAddress.TabIndex = 19;
            this.tabCustomerShippingAddress.Text = "Shipping Address";
            this.tabCustomerShippingAddress.UseVisualStyleBackColor = true;
            // 
            // chForceShippingAddressOnUpdate
            // 
            this.chForceShippingAddressOnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chForceShippingAddressOnUpdate.AutoSize = true;
            this.chForceShippingAddressOnUpdate.Checked = true;
            this.chForceShippingAddressOnUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chForceShippingAddressOnUpdate.Location = new System.Drawing.Point(255, 450);
            this.chForceShippingAddressOnUpdate.Name = "chForceShippingAddressOnUpdate";
            this.chForceShippingAddressOnUpdate.Size = new System.Drawing.Size(15, 14);
            this.chForceShippingAddressOnUpdate.TabIndex = 58;
            this.chForceShippingAddressOnUpdate.UseVisualStyleBackColor = true;
            // 
            // label46
            // 
            this.label46.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(2, 451);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(169, 13);
            this.label46.TabIndex = 57;
            this.label46.Text = "Force Existing Address On Update";
            // 
            // panelCustomerShippingAddress
            // 
            this.panelCustomerShippingAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelCustomerShippingAddress.Location = new System.Drawing.Point(2, 2);
            this.panelCustomerShippingAddress.Margin = new System.Windows.Forms.Padding(2);
            this.panelCustomerShippingAddress.Name = "panelCustomerShippingAddress";
            this.panelCustomerShippingAddress.Size = new System.Drawing.Size(898, 408);
            this.panelCustomerShippingAddress.TabIndex = 56;
            // 
            // chCustomerAddress_DeleteOld
            // 
            this.chCustomerAddress_DeleteOld.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chCustomerAddress_DeleteOld.AutoSize = true;
            this.chCustomerAddress_DeleteOld.Checked = true;
            this.chCustomerAddress_DeleteOld.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chCustomerAddress_DeleteOld.Location = new System.Drawing.Point(255, 424);
            this.chCustomerAddress_DeleteOld.Name = "chCustomerAddress_DeleteOld";
            this.chCustomerAddress_DeleteOld.Size = new System.Drawing.Size(15, 14);
            this.chCustomerAddress_DeleteOld.TabIndex = 55;
            this.chCustomerAddress_DeleteOld.UseVisualStyleBackColor = true;
            this.chCustomerAddress_DeleteOld.CheckedChanged += new System.EventHandler(this.chCustomerAddress_DeleteOld_CheckedChanged);
            // 
            // label43
            // 
            this.label43.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(2, 425);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(179, 13);
            this.label43.TabIndex = 54;
            this.label43.Text = "Delete Existing Addreses On Update";
            // 
            // tabCustomerBillingAddress
            // 
            this.tabCustomerBillingAddress.Controls.Add(this.chForceBillingAddressOnUpdate);
            this.tabCustomerBillingAddress.Controls.Add(this.label47);
            this.tabCustomerBillingAddress.Controls.Add(this.panelCustomerBillingAddress);
            this.tabCustomerBillingAddress.Controls.Add(this.chCustomerAddress_DeleteOld2);
            this.tabCustomerBillingAddress.Controls.Add(this.label45);
            this.tabCustomerBillingAddress.Location = new System.Drawing.Point(4, 22);
            this.tabCustomerBillingAddress.Margin = new System.Windows.Forms.Padding(2);
            this.tabCustomerBillingAddress.Name = "tabCustomerBillingAddress";
            this.tabCustomerBillingAddress.Size = new System.Drawing.Size(905, 605);
            this.tabCustomerBillingAddress.TabIndex = 22;
            this.tabCustomerBillingAddress.Text = "Billing Address";
            this.tabCustomerBillingAddress.UseVisualStyleBackColor = true;
            // 
            // chForceBillingAddressOnUpdate
            // 
            this.chForceBillingAddressOnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chForceBillingAddressOnUpdate.AutoSize = true;
            this.chForceBillingAddressOnUpdate.Checked = true;
            this.chForceBillingAddressOnUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chForceBillingAddressOnUpdate.Location = new System.Drawing.Point(255, 449);
            this.chForceBillingAddressOnUpdate.Name = "chForceBillingAddressOnUpdate";
            this.chForceBillingAddressOnUpdate.Size = new System.Drawing.Size(15, 14);
            this.chForceBillingAddressOnUpdate.TabIndex = 61;
            this.chForceBillingAddressOnUpdate.UseVisualStyleBackColor = true;
            // 
            // label47
            // 
            this.label47.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(2, 449);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(169, 13);
            this.label47.TabIndex = 60;
            this.label47.Text = "Force Existing Address On Update";
            // 
            // panelCustomerBillingAddress
            // 
            this.panelCustomerBillingAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelCustomerBillingAddress.Location = new System.Drawing.Point(2, 2);
            this.panelCustomerBillingAddress.Margin = new System.Windows.Forms.Padding(2);
            this.panelCustomerBillingAddress.Name = "panelCustomerBillingAddress";
            this.panelCustomerBillingAddress.Size = new System.Drawing.Size(898, 408);
            this.panelCustomerBillingAddress.TabIndex = 59;
            // 
            // chCustomerAddress_DeleteOld2
            // 
            this.chCustomerAddress_DeleteOld2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chCustomerAddress_DeleteOld2.AutoSize = true;
            this.chCustomerAddress_DeleteOld2.Checked = true;
            this.chCustomerAddress_DeleteOld2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chCustomerAddress_DeleteOld2.Enabled = false;
            this.chCustomerAddress_DeleteOld2.Location = new System.Drawing.Point(255, 424);
            this.chCustomerAddress_DeleteOld2.Name = "chCustomerAddress_DeleteOld2";
            this.chCustomerAddress_DeleteOld2.Size = new System.Drawing.Size(15, 14);
            this.chCustomerAddress_DeleteOld2.TabIndex = 58;
            this.chCustomerAddress_DeleteOld2.UseVisualStyleBackColor = true;
            // 
            // label45
            // 
            this.label45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(2, 425);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(179, 13);
            this.label45.TabIndex = 57;
            this.label45.Text = "Delete Existing Addreses On Update";
            // 
            // tabCustomerRole
            // 
            this.tabCustomerRole.Controls.Add(this.panelCustomerRole);
            this.tabCustomerRole.Controls.Add(this.chCustomerRole_DeleteOld);
            this.tabCustomerRole.Controls.Add(this.label36);
            this.tabCustomerRole.Location = new System.Drawing.Point(4, 22);
            this.tabCustomerRole.Margin = new System.Windows.Forms.Padding(2);
            this.tabCustomerRole.Name = "tabCustomerRole";
            this.tabCustomerRole.Size = new System.Drawing.Size(905, 605);
            this.tabCustomerRole.TabIndex = 20;
            this.tabCustomerRole.Text = "Customer Role";
            this.tabCustomerRole.UseVisualStyleBackColor = true;
            // 
            // panelCustomerRole
            // 
            this.panelCustomerRole.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelCustomerRole.Location = new System.Drawing.Point(3, 2);
            this.panelCustomerRole.Margin = new System.Windows.Forms.Padding(2);
            this.panelCustomerRole.Name = "panelCustomerRole";
            this.panelCustomerRole.Size = new System.Drawing.Size(898, 408);
            this.panelCustomerRole.TabIndex = 56;
            // 
            // chCustomerRole_DeleteOld
            // 
            this.chCustomerRole_DeleteOld.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chCustomerRole_DeleteOld.AutoSize = true;
            this.chCustomerRole_DeleteOld.Checked = true;
            this.chCustomerRole_DeleteOld.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chCustomerRole_DeleteOld.Location = new System.Drawing.Point(256, 424);
            this.chCustomerRole_DeleteOld.Name = "chCustomerRole_DeleteOld";
            this.chCustomerRole_DeleteOld.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRole_DeleteOld.TabIndex = 55;
            this.chCustomerRole_DeleteOld.UseVisualStyleBackColor = true;
            // 
            // label36
            // 
            this.label36.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(3, 425);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(162, 13);
            this.label36.TabIndex = 54;
            this.label36.Text = "Delete Existing Roles On Update";
            // 
            // tabCustomerOther
            // 
            this.tabCustomerOther.Controls.Add(this.panelCustomerOthers);
            this.tabCustomerOther.Location = new System.Drawing.Point(4, 22);
            this.tabCustomerOther.Margin = new System.Windows.Forms.Padding(2);
            this.tabCustomerOther.Name = "tabCustomerOther";
            this.tabCustomerOther.Size = new System.Drawing.Size(905, 605);
            this.tabCustomerOther.TabIndex = 21;
            this.tabCustomerOther.Text = "Customer Other";
            this.tabCustomerOther.UseVisualStyleBackColor = true;
            // 
            // panelCustomerOthers
            // 
            this.panelCustomerOthers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelCustomerOthers.Location = new System.Drawing.Point(3, 2);
            this.panelCustomerOthers.Margin = new System.Windows.Forms.Padding(2);
            this.panelCustomerOthers.Name = "panelCustomerOthers";
            this.panelCustomerOthers.Size = new System.Drawing.Size(898, 408);
            this.panelCustomerOthers.TabIndex = 55;
            // 
            // tabWishList
            // 
            this.tabWishList.Controls.Add(this.label11);
            this.tabWishList.Controls.Add(this.panelWishList);
            this.tabWishList.Controls.Add(this.chDeleteWishListOnUpdate);
            this.tabWishList.Location = new System.Drawing.Point(4, 22);
            this.tabWishList.Margin = new System.Windows.Forms.Padding(2);
            this.tabWishList.Name = "tabWishList";
            this.tabWishList.Size = new System.Drawing.Size(905, 605);
            this.tabWishList.TabIndex = 24;
            this.tabWishList.Text = "Wish List";
            this.tabWishList.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 425);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(178, 13);
            this.label11.TabIndex = 61;
            this.label11.Text = "Delete Existing Wish List On Update";
            // 
            // panelWishList
            // 
            this.panelWishList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelWishList.Location = new System.Drawing.Point(2, 2);
            this.panelWishList.Margin = new System.Windows.Forms.Padding(2);
            this.panelWishList.Name = "panelWishList";
            this.panelWishList.Size = new System.Drawing.Size(898, 408);
            this.panelWishList.TabIndex = 60;
            // 
            // chDeleteWishListOnUpdate
            // 
            this.chDeleteWishListOnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chDeleteWishListOnUpdate.AutoSize = true;
            this.chDeleteWishListOnUpdate.Checked = true;
            this.chDeleteWishListOnUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chDeleteWishListOnUpdate.Location = new System.Drawing.Point(255, 424);
            this.chDeleteWishListOnUpdate.Name = "chDeleteWishListOnUpdate";
            this.chDeleteWishListOnUpdate.Size = new System.Drawing.Size(15, 14);
            this.chDeleteWishListOnUpdate.TabIndex = 59;
            this.chDeleteWishListOnUpdate.UseVisualStyleBackColor = true;
            // 
            // btnTestMapping
            // 
            this.btnTestMapping.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTestMapping.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTestMapping.Location = new System.Drawing.Point(556, 635);
            this.btnTestMapping.Name = "btnTestMapping";
            this.btnTestMapping.Size = new System.Drawing.Size(100, 23);
            this.btnTestMapping.TabIndex = 26;
            this.btnTestMapping.Text = "Preview Items";
            this.btnTestMapping.UseVisualStyleBackColor = true;
            this.btnTestMapping.Click += new System.EventHandler(this.btnTestMapping_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(824, 635);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 25;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(742, 635);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 24;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRunTask
            // 
            this.btnRunTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRunTask.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRunTask.Location = new System.Drawing.Point(12, 635);
            this.btnRunTask.Name = "btnRunTask";
            this.btnRunTask.Size = new System.Drawing.Size(75, 23);
            this.btnRunTask.TabIndex = 28;
            this.btnRunTask.Text = "Run now";
            this.btnRunTask.UseVisualStyleBackColor = true;
            this.btnRunTask.Click += new System.EventHandler(this.btnRunTask_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // statusArea
            // 
            this.statusArea.AutoSize = false;
            this.statusArea.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusArea.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusAreaText});
            this.statusArea.Location = new System.Drawing.Point(0, 661);
            this.statusArea.Name = "statusArea";
            this.statusArea.Size = new System.Drawing.Size(911, 22);
            this.statusArea.TabIndex = 29;
            this.statusArea.Text = "statusArea";
            this.statusArea.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusArea_ItemClicked);
            // 
            // statusAreaText
            // 
            this.statusAreaText.Name = "statusAreaText";
            this.statusAreaText.Size = new System.Drawing.Size(0, 17);
            // 
            // btnQuickImport
            // 
            this.btnQuickImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuickImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuickImport.Location = new System.Drawing.Point(455, 635);
            this.btnQuickImport.Name = "btnQuickImport";
            this.btnQuickImport.Size = new System.Drawing.Size(94, 23);
            this.btnQuickImport.TabIndex = 30;
            this.btnQuickImport.Text = "Quick Import";
            this.btnQuickImport.UseVisualStyleBackColor = true;
            this.btnQuickImport.Visible = false;
            this.btnQuickImport.Click += new System.EventHandler(this.btnQuickImport_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 125;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "FieldName";
            this.dataGridViewTextBoxColumn2.HeaderText = "FieldName";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 125;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "FieldRule";
            this.dataGridViewTextBoxColumn3.HeaderText = "Custom Rule";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 125;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "MappingItemType";
            this.dataGridViewTextBoxColumn4.HeaderText = "MappingItemType";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 125;
            // 
            // btnCopy
            // 
            this.btnCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCopy.Location = new System.Drawing.Point(662, 635);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(75, 23);
            this.btnCopy.TabIndex = 31;
            this.btnCopy.Text = "Copy";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // xmlModify
            // 
            this.xmlModify.AutoSize = true;
            this.xmlModify.Location = new System.Drawing.Point(107, 639);
            this.xmlModify.Name = "xmlModify";
            this.xmlModify.Size = new System.Drawing.Size(77, 17);
            this.xmlModify.TabIndex = 32;
            this.xmlModify.Text = "Xml Modify";
            this.xmlModify.UseVisualStyleBackColor = true;
            // 
            // taskNewEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 683);
            this.Controls.Add(this.xmlModify);
            this.Controls.Add(this.btnCopy);
            this.Controls.Add(this.btnQuickImport);
            this.Controls.Add(this.statusArea);
            this.Controls.Add(this.btnRunTask);
            this.Controls.Add(this.btnTestMapping);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tabTask);
            this.Name = "taskNewEdit";
            this.Text = "Task configuration data";
            this.Load += new System.EventHandler(this.taskNewEdit_Load);
            this.Shown += new System.EventHandler(this.taskNewEdit_Shown);
            this.tabTask.ResumeLayout(false);
            this.tabTaskData.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.pnSchedulerTime.ResumeLayout(false);
            this.pnSchedulerTime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmMinute)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmInterval)).EndInit();
            this.tabProductInfo.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabProductSettings.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmProductShippingCharge)).EndInit();
            this.tabSeo.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tabCategories.ResumeLayout(false);
            this.tabCategories.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tabAccessControl.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tabImages.ResumeLayout(false);
            this.tabImages.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmPictureResizeHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPictureResizeWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPictureResizeQuality)).EndInit();
            this.tabProdAttributes.ResumeLayout(false);
            this.tabProdAttributes.PerformLayout();
            this.tabSpecAttributes.ResumeLayout(false);
            this.tabSpecAttributes.PerformLayout();
            this.tabFilters.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tabRelatedTasks.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.tabPrices.ResumeLayout(false);
            this.tabTierPricing.ResumeLayout(false);
            this.tabTierPricing.PerformLayout();
            this.tabTranslations.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tabCustomSql.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tabCustomerCustomAtt.ResumeLayout(false);
            this.tabCustomerCustomAtt.PerformLayout();
            this.tabAddressCustomAtt.ResumeLayout(false);
            this.tabAddressCustomAtt.PerformLayout();
            this.tabProdInfo.ResumeLayout(false);
            this.tabLogSettings.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tabCustomer.ResumeLayout(false);
            this.tabCustomerShippingAddress.ResumeLayout(false);
            this.tabCustomerShippingAddress.PerformLayout();
            this.tabCustomerBillingAddress.ResumeLayout(false);
            this.tabCustomerBillingAddress.PerformLayout();
            this.tabCustomerRole.ResumeLayout(false);
            this.tabCustomerRole.PerformLayout();
            this.tabCustomerOther.ResumeLayout(false);
            this.tabWishList.ResumeLayout(false);
            this.tabWishList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.statusArea.ResumeLayout(false);
            this.statusArea.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		// Token: 0x04000161 RID: 353
		private global::System.ComponentModel.IContainer components;

		// Token: 0x04000162 RID: 354
		private global::System.Windows.Forms.TabControl tabTask;

		// Token: 0x04000163 RID: 355
		private global::System.Windows.Forms.TabPage tabTaskData;

		// Token: 0x04000164 RID: 356
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;

		// Token: 0x04000165 RID: 357
		private global::System.Windows.Forms.Label label1;

		// Token: 0x04000166 RID: 358
		private global::System.Windows.Forms.Label label5;

		// Token: 0x04000167 RID: 359
		private global::System.Windows.Forms.Label label2;

		// Token: 0x04000168 RID: 360
		private global::System.Windows.Forms.TextBox tbTaskName;

		// Token: 0x04000169 RID: 361
		private global::System.Windows.Forms.TabPage tabProductInfo;

		// Token: 0x0400016A RID: 362
		private global::System.Windows.Forms.Button btnTestMapping;

		// Token: 0x0400016B RID: 363
		private global::System.Windows.Forms.Button btnClose;

		// Token: 0x0400016C RID: 364
		private global::System.Windows.Forms.Button btnSave;

		// Token: 0x0400016D RID: 365
		private global::System.Windows.Forms.ComboBox ddStoreWeb;

		// Token: 0x0400016E RID: 366
		private global::System.Windows.Forms.Label label3;

		// Token: 0x0400016F RID: 367
		private global::System.Windows.Forms.ComboBox ddVendorSourceMap;

		// Token: 0x04000170 RID: 368
		private global::System.Windows.Forms.Label label4;

		// Token: 0x04000171 RID: 369
		private global::System.Windows.Forms.ComboBox ddTaskStatus;

		// Token: 0x04000172 RID: 370
		private global::System.Windows.Forms.Label label6;

		// Token: 0x04000173 RID: 371
		private global::System.Windows.Forms.ComboBox ddTaskAction;

		// Token: 0x04000174 RID: 372
		private global::System.Windows.Forms.Label label7;

		// Token: 0x04000175 RID: 373
		private global::System.Windows.Forms.CheckBox chDisableShop;

		// Token: 0x04000176 RID: 374
		private global::System.Windows.Forms.Label label8;

		// Token: 0x04000177 RID: 375
		private global::System.Windows.Forms.CheckedListBox chLimitedStore;

		// Token: 0x04000178 RID: 376
		private global::System.Windows.Forms.Button btnRunTask;

		// Token: 0x04000179 RID: 377
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

		// Token: 0x0400017A RID: 378
		private global::System.Windows.Forms.Label label24;

		// Token: 0x0400017B RID: 379
		private global::System.Windows.Forms.Label label20;

		// Token: 0x0400017C RID: 380
		private global::System.Windows.Forms.Label label18;

		// Token: 0x0400017D RID: 381
		private global::System.Windows.Forms.Label label17;

		// Token: 0x0400017E RID: 382
		private global::System.Windows.Forms.Label label16;

		// Token: 0x0400017F RID: 383
		private global::System.Windows.Forms.Label label15;

		// Token: 0x04000180 RID: 384
		private global::System.Windows.Forms.Label label21;

		// Token: 0x04000181 RID: 385
		private global::System.Windows.Forms.TextBox tbProductName;

		// Token: 0x04000182 RID: 386
		private global::System.Windows.Forms.TextBox tbProdShortDesc;

		// Token: 0x04000183 RID: 387
		private global::System.Windows.Forms.TextBox tbProdFullDesc;

		// Token: 0x04000184 RID: 388
		private global::System.Windows.Forms.TextBox tbManufacturerPnum;

		// Token: 0x04000185 RID: 389
		private global::System.Windows.Forms.Label label26;

		// Token: 0x04000186 RID: 390
		private global::System.Windows.Forms.Label label25;

		// Token: 0x04000187 RID: 391
		private global::System.Windows.Forms.TextBox nmStockQuantity;

		// Token: 0x04000188 RID: 392
		private global::System.Windows.Forms.Label label9;

		// Token: 0x04000189 RID: 393
		private global::System.Windows.Forms.CheckBox chProdIdMap;

		// Token: 0x0400018A RID: 394
		private global::System.Windows.Forms.CheckBox chProdNameMap;

		// Token: 0x0400018B RID: 395
		private global::System.Windows.Forms.CheckBox chProdShortDescMap;

		// Token: 0x0400018C RID: 396
		private global::System.Windows.Forms.CheckBox chProdFullDescMap;

		// Token: 0x0400018D RID: 397
		private global::System.Windows.Forms.CheckBox chProdManPartNumMap;

		// Token: 0x0400018E RID: 398
		private global::System.Windows.Forms.CheckBox chProdStockMap;

		// Token: 0x0400018F RID: 399
		private global::System.Windows.Forms.CheckBox chProdNameImport;

		// Token: 0x04000190 RID: 400
		private global::System.Windows.Forms.CheckBox chProdShortDescImport;

		// Token: 0x04000191 RID: 401
		private global::System.Windows.Forms.CheckBox chProdFullDescImport;

		// Token: 0x04000192 RID: 402
		private global::System.Windows.Forms.CheckBox chProdManPartNumImport;

		// Token: 0x04000193 RID: 403
		private global::System.Windows.Forms.CheckBox chProdStockImport;

		// Token: 0x04000194 RID: 404
		private global::System.Windows.Forms.Label label10;

		// Token: 0x04000195 RID: 405
		private global::System.Windows.Forms.CheckBox chManufacturerMap;

		// Token: 0x04000196 RID: 406
		private global::System.Windows.Forms.TextBox tbManufacturer;

		// Token: 0x04000197 RID: 407
		private global::System.Windows.Forms.CheckBox chManufacturerImport;

		// Token: 0x04000198 RID: 408
		private global::System.Windows.Forms.TabPage tabSeo;

		// Token: 0x04000199 RID: 409
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;

		// Token: 0x0400019A RID: 410
		private global::System.Windows.Forms.TextBox tbSearchEnginePage;

		// Token: 0x0400019B RID: 411
		private global::System.Windows.Forms.TextBox tbMetaTitle;

		// Token: 0x0400019C RID: 412
		private global::System.Windows.Forms.Label label32;

		// Token: 0x0400019D RID: 413
		private global::System.Windows.Forms.TextBox tbMetaDescription;

		// Token: 0x0400019E RID: 414
		private global::System.Windows.Forms.TextBox tbMetaKeywords;

		// Token: 0x0400019F RID: 415
		private global::System.Windows.Forms.Label label27;

		// Token: 0x040001A0 RID: 416
		private global::System.Windows.Forms.Label label29;

		// Token: 0x040001A1 RID: 417
		private global::System.Windows.Forms.Label label30;

		// Token: 0x040001A2 RID: 418
		private global::System.Windows.Forms.Label label31;

		// Token: 0x040001A3 RID: 419
		private global::System.Windows.Forms.Label label33;

		// Token: 0x040001A4 RID: 420
		private global::System.Windows.Forms.Label label14;

		// Token: 0x040001A5 RID: 421
		private global::System.Windows.Forms.CheckBox chSeoMetaKeyMap;

		// Token: 0x040001A6 RID: 422
		private global::System.Windows.Forms.CheckBox chSeoMetaDescMap;

		// Token: 0x040001A7 RID: 423
		private global::System.Windows.Forms.CheckBox chSeoMetaTitleMap;

		// Token: 0x040001A8 RID: 424
		private global::System.Windows.Forms.CheckBox chSearchPageMap;

		// Token: 0x040001A9 RID: 425
		private global::System.Windows.Forms.Label label22;

		// Token: 0x040001AA RID: 426
		private global::System.Windows.Forms.CheckBox chSeoMetaKeyImport;

		// Token: 0x040001AB RID: 427
		private global::System.Windows.Forms.CheckBox chSeoMetaDescImport;

		// Token: 0x040001AC RID: 428
		private global::System.Windows.Forms.CheckBox chSeoMetaTitleImport;

		// Token: 0x040001AD RID: 429
		private global::System.Windows.Forms.CheckBox chSearchPageImport;

		// Token: 0x040001AE RID: 430
		private global::System.Windows.Forms.TabPage tabCategories;

		// Token: 0x040001AF RID: 431
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;

		// Token: 0x040001B0 RID: 432
		private global::System.Windows.Forms.Label label35;

		// Token: 0x040001B1 RID: 433
		private global::System.Windows.Forms.Label label38;

		// Token: 0x040001B2 RID: 434
		private global::System.Windows.Forms.Label label37;

		// Token: 0x040001B3 RID: 435
		private global::System.Windows.Forms.TextBox tbCategory;

		// Token: 0x040001B4 RID: 436
		private global::System.Windows.Forms.Label label39;

		// Token: 0x040001B5 RID: 437
		private global::System.Windows.Forms.Label label40;

		// Token: 0x040001B6 RID: 438
		private global::System.Windows.Forms.Label label41;

		// Token: 0x040001B7 RID: 439
		private global::System.Windows.Forms.Label label42;

		// Token: 0x040001B8 RID: 440
		private global::System.Windows.Forms.TextBox tbCategory1;

		// Token: 0x040001B9 RID: 441
		private global::System.Windows.Forms.TextBox tbCategory2;

		// Token: 0x040001BA RID: 442
		private global::System.Windows.Forms.TextBox tbCategory3;

		// Token: 0x040001BB RID: 443
		private global::System.Windows.Forms.TextBox tbCategory4;

		// Token: 0x040001BC RID: 444
		private global::System.Windows.Forms.Label label28;

		// Token: 0x040001BD RID: 445
		private global::System.Windows.Forms.CheckBox chCategoryMap;

		// Token: 0x040001BE RID: 446
		private global::System.Windows.Forms.CheckBox chCategory1Map;

		// Token: 0x040001BF RID: 447
		private global::System.Windows.Forms.CheckBox chCategory2Map;

		// Token: 0x040001C0 RID: 448
		private global::System.Windows.Forms.CheckBox chCategory3Map;

		// Token: 0x040001C1 RID: 449
		private global::System.Windows.Forms.CheckBox chCategory4Map;

		// Token: 0x040001C2 RID: 450
		private global::System.Windows.Forms.Label label34;

		// Token: 0x040001C3 RID: 451
		private global::System.Windows.Forms.CheckBox chCategoryImport;

		// Token: 0x040001C4 RID: 452
		private global::System.Windows.Forms.CheckBox chCategory1Import;

		// Token: 0x040001C5 RID: 453
		private global::System.Windows.Forms.CheckBox chCategory2Import;

		// Token: 0x040001C6 RID: 454
		private global::System.Windows.Forms.CheckBox chCategory3Import;

		// Token: 0x040001C7 RID: 455
		private global::System.Windows.Forms.CheckBox chCategory4Import;

		// Token: 0x040001C8 RID: 456
		private global::System.Windows.Forms.TabPage tabImages;

		// Token: 0x040001C9 RID: 457
		private global::System.Windows.Forms.TabPage tabProdAttributes;

		// Token: 0x040001CA RID: 458
		private global::System.Windows.Forms.TabPage tabFilters;

		// Token: 0x040001CB RID: 459
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;

		// Token: 0x040001CC RID: 460
		private global::System.Windows.Forms.CheckBox chFilter9Map;

		// Token: 0x040001CD RID: 461
		private global::System.Windows.Forms.Label lblFilter9;

		// Token: 0x040001CE RID: 462
		private global::System.Windows.Forms.Label label62;

		// Token: 0x040001CF RID: 463
		private global::System.Windows.Forms.Label label64;

		// Token: 0x040001D0 RID: 464
		private global::System.Windows.Forms.CheckBox chFilterMap;

		// Token: 0x040001D1 RID: 465
		private global::System.Windows.Forms.CheckBox chFilter1Map;

		// Token: 0x040001D2 RID: 466
		private global::System.Windows.Forms.CheckBox chFilter2Map;

		// Token: 0x040001D3 RID: 467
		private global::System.Windows.Forms.CheckBox chFilter3Map;

		// Token: 0x040001D4 RID: 468
		private global::System.Windows.Forms.CheckBox chFilter4Map;

		// Token: 0x040001D5 RID: 469
		private global::System.Windows.Forms.Label lblFilter;

		// Token: 0x040001D6 RID: 470
		private global::System.Windows.Forms.Label lblFilter1;

		// Token: 0x040001D7 RID: 471
		private global::System.Windows.Forms.Label lblFilter2;

		// Token: 0x040001D8 RID: 472
		private global::System.Windows.Forms.Label lblFilter3;

		// Token: 0x040001D9 RID: 473
		private global::System.Windows.Forms.Label lblFilter4;

		// Token: 0x040001DA RID: 474
		private global::System.Windows.Forms.Label lblFilter5;

		// Token: 0x040001DB RID: 475
		private global::System.Windows.Forms.Label lblFilter6;

		// Token: 0x040001DC RID: 476
		private global::System.Windows.Forms.Label lblFilter7;

		// Token: 0x040001DD RID: 477
		private global::System.Windows.Forms.Label lblFilter8;

		// Token: 0x040001DE RID: 478
		private global::System.Windows.Forms.CheckBox chFilter5Map;

		// Token: 0x040001DF RID: 479
		private global::System.Windows.Forms.CheckBox chFilter6Map;

		// Token: 0x040001E0 RID: 480
		private global::System.Windows.Forms.CheckBox chFilter7Map;

		// Token: 0x040001E1 RID: 481
		private global::System.Windows.Forms.CheckBox chFilter8Map;

		// Token: 0x040001E2 RID: 482
		private global::System.Windows.Forms.TextBox tbFilterValue;

		// Token: 0x040001E3 RID: 483
		private global::System.Windows.Forms.TextBox tbFilter1Value;

		// Token: 0x040001E4 RID: 484
		private global::System.Windows.Forms.TextBox tbFilter2Value;

		// Token: 0x040001E5 RID: 485
		private global::System.Windows.Forms.TextBox tbFilter3Value;

		// Token: 0x040001E6 RID: 486
		private global::System.Windows.Forms.TextBox tbFilter4Value;

		// Token: 0x040001E7 RID: 487
		private global::System.Windows.Forms.TextBox tbFilter5Value;

		// Token: 0x040001E8 RID: 488
		private global::System.Windows.Forms.TextBox tbFilter6Value;

		// Token: 0x040001E9 RID: 489
		private global::System.Windows.Forms.TextBox tbFilter7Value;

		// Token: 0x040001EA RID: 490
		private global::System.Windows.Forms.TextBox tbFilter8Value;

		// Token: 0x040001EB RID: 491
		private global::System.Windows.Forms.ComboBox ddFilterOp;

		// Token: 0x040001EC RID: 492
		private global::System.Windows.Forms.Label label63;

		// Token: 0x040001ED RID: 493
		private global::System.Windows.Forms.Label label76;

		// Token: 0x040001EE RID: 494
		private global::System.Windows.Forms.ComboBox comboBox_0;

		// Token: 0x040001EF RID: 495
		private global::System.Windows.Forms.ComboBox comboBox_1;

		// Token: 0x040001F0 RID: 496
		private global::System.Windows.Forms.ComboBox comboBox_2;

		// Token: 0x040001F1 RID: 497
		private global::System.Windows.Forms.ComboBox comboBox_3;

		// Token: 0x040001F2 RID: 498
		private global::System.Windows.Forms.ComboBox comboBox_4;

		// Token: 0x040001F3 RID: 499
		private global::System.Windows.Forms.ComboBox comboBox_5;

		// Token: 0x040001F4 RID: 500
		private global::System.Windows.Forms.ComboBox comboBox_6;

		// Token: 0x040001F5 RID: 501
		private global::System.Windows.Forms.ComboBox comboBox_7;

		// Token: 0x040001F6 RID: 502
		private global::System.Windows.Forms.ComboBox comboBox_8;

		// Token: 0x040001F7 RID: 503
		private global::System.Windows.Forms.TextBox tbFilter9Value;

		// Token: 0x040001F8 RID: 504
		private global::System.Windows.Forms.Label label65;

		// Token: 0x040001F9 RID: 505
		private global::System.Windows.Forms.ComboBox ddScheduleType;

		// Token: 0x040001FA RID: 506
		private global::System.Windows.Forms.NumericUpDown nmInterval;

		// Token: 0x040001FB RID: 507
		private global::System.Windows.Forms.Label label66;

		// Token: 0x040001FC RID: 508
		private global::System.Windows.Forms.Panel pnSchedulerTime;

		// Token: 0x040001FD RID: 509
		private global::System.Windows.Forms.Label label67;

		// Token: 0x040001FE RID: 510
		private global::System.Windows.Forms.NumericUpDown nmHour;

		// Token: 0x040001FF RID: 511
		private global::System.Windows.Forms.Label label68;

		// Token: 0x04000200 RID: 512
		private global::System.Windows.Forms.NumericUpDown nmMinute;

		// Token: 0x04000201 RID: 513
		private global::System.Windows.Forms.Label label69;

		// Token: 0x04000202 RID: 514
		private global::System.Windows.Forms.ErrorProvider errorProvider1;

		// Token: 0x04000203 RID: 515
		private global::System.Windows.Forms.CheckBox chUnpublish;

		// Token: 0x04000204 RID: 516
		private global::System.Windows.Forms.Label label70;

		// Token: 0x04000205 RID: 517
		private global::System.Windows.Forms.StatusStrip statusArea;

		// Token: 0x04000206 RID: 518
		private global::System.Windows.Forms.ToolStripStatusLabel statusAreaText;

		// Token: 0x04000207 RID: 519
		private global::System.Windows.Forms.Label label75;

		// Token: 0x04000208 RID: 520
		private global::System.Windows.Forms.ComboBox ddParentCat1;

		// Token: 0x04000209 RID: 521
		private global::System.Windows.Forms.ComboBox ddParentCat2;

		// Token: 0x0400020A RID: 522
		private global::System.Windows.Forms.ComboBox ddParentCat3;

		// Token: 0x0400020B RID: 523
		private global::System.Windows.Forms.ComboBox ddParentCat4;

		// Token: 0x0400020C RID: 524
		private global::System.Windows.Forms.Label label77;

		// Token: 0x0400020D RID: 525
		private global::System.Windows.Forms.CheckBox chDeleteFileAfterImport;

		// Token: 0x0400020E RID: 526
		private global::System.Windows.Forms.CheckBox chVendorMap;

		// Token: 0x0400020F RID: 527
		private global::System.Windows.Forms.Label label79;

		// Token: 0x04000210 RID: 528
		private global::System.Windows.Forms.TextBox tbVendor;

		// Token: 0x04000211 RID: 529
		private global::System.Windows.Forms.CheckBox chVendorImport;

		// Token: 0x04000212 RID: 530
		private global::System.Windows.Forms.TabPage tabRelatedTasks;

		// Token: 0x04000213 RID: 531
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;

		// Token: 0x04000214 RID: 532
		private global::System.Windows.Forms.Label label153;

		// Token: 0x04000215 RID: 533
		private global::System.Windows.Forms.TextBox tbRelatedSourceAddress;

		// Token: 0x04000216 RID: 534
		private global::System.Windows.Forms.ComboBox ddRelatedTask;

		// Token: 0x04000217 RID: 535
		private global::System.Windows.Forms.Label label154;

		// Token: 0x04000218 RID: 536
		private global::System.Windows.Forms.Label label81;

		// Token: 0x04000219 RID: 537
		private global::System.Windows.Forms.Label label155;

		// Token: 0x0400021A RID: 538
		private global::System.Windows.Forms.TextBox tbGtin;

		// Token: 0x0400021B RID: 539
		private global::System.Windows.Forms.TextBox nmWeight;

		// Token: 0x0400021C RID: 540
		private global::System.Windows.Forms.CheckBox chGtinMap;

		// Token: 0x0400021D RID: 541
		private global::System.Windows.Forms.CheckBox chWeightMap;

		// Token: 0x0400021E RID: 542
		private global::System.Windows.Forms.CheckBox chWeightImport;

		// Token: 0x0400021F RID: 543
		private global::System.Windows.Forms.CheckBox chGtinImport;

		// Token: 0x04000220 RID: 544
		private global::System.Windows.Forms.Label label83;

		// Token: 0x04000221 RID: 545
		private global::System.Windows.Forms.Label label82;

		// Token: 0x04000222 RID: 546
		private global::System.Windows.Forms.CheckBox chPicturesInFiles;

		// Token: 0x04000223 RID: 547
		private global::System.Windows.Forms.Panel panel2;

		// Token: 0x04000224 RID: 548
		private global::System.Windows.Forms.Label label84;

		// Token: 0x04000225 RID: 549
		private global::System.Windows.Forms.TextBox tbPicturesPathSave;

		// Token: 0x04000226 RID: 550
		private global::System.Windows.Forms.TabPage tabAccessControl;

		// Token: 0x04000227 RID: 551
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;

		// Token: 0x04000228 RID: 552
		private global::System.Windows.Forms.TextBox tbCustomerRole3;

		// Token: 0x04000229 RID: 553
		private global::System.Windows.Forms.TextBox tbCustomerRole2;

		// Token: 0x0400022A RID: 554
		private global::System.Windows.Forms.Label label85;

		// Token: 0x0400022B RID: 555
		private global::System.Windows.Forms.TextBox tbCustomerRole1;

		// Token: 0x0400022C RID: 556
		private global::System.Windows.Forms.TextBox tbCustomerRole;

		// Token: 0x0400022D RID: 557
		private global::System.Windows.Forms.Label label86;

		// Token: 0x0400022E RID: 558
		private global::System.Windows.Forms.Label label87;

		// Token: 0x0400022F RID: 559
		private global::System.Windows.Forms.Label label88;

		// Token: 0x04000230 RID: 560
		private global::System.Windows.Forms.Label label89;

		// Token: 0x04000231 RID: 561
		private global::System.Windows.Forms.Label label90;

		// Token: 0x04000232 RID: 562
		private global::System.Windows.Forms.Label label91;

		// Token: 0x04000233 RID: 563
		private global::System.Windows.Forms.CheckBox chCustomerRoleMap;

		// Token: 0x04000234 RID: 564
		private global::System.Windows.Forms.CheckBox chCustomerRole1Map;

		// Token: 0x04000235 RID: 565
		private global::System.Windows.Forms.CheckBox chCustomerRole2Map;

		// Token: 0x04000236 RID: 566
		private global::System.Windows.Forms.CheckBox chCustomerRole3Map;

		// Token: 0x04000237 RID: 567
		private global::System.Windows.Forms.Label label92;

		// Token: 0x04000238 RID: 568
		private global::System.Windows.Forms.CheckBox chCustomerRoleImport;

		// Token: 0x04000239 RID: 569
		private global::System.Windows.Forms.CheckBox chCustomerRole1Import;

		// Token: 0x0400023A RID: 570
		private global::System.Windows.Forms.CheckBox chCustomerRole2Import;

		// Token: 0x0400023B RID: 571
		private global::System.Windows.Forms.CheckBox chCustomerRole3Import;

		// Token: 0x0400023C RID: 572
		private global::System.Windows.Forms.CheckBox chCustomerRole4Map;

		// Token: 0x0400023D RID: 573
		private global::System.Windows.Forms.Label label93;

		// Token: 0x0400023E RID: 574
		private global::System.Windows.Forms.TextBox tbCustomerRole4;

		// Token: 0x0400023F RID: 575
		private global::System.Windows.Forms.CheckBox chCustomerRole4Import;

		// Token: 0x04000240 RID: 576
		private global::System.Windows.Forms.Label label94;

		// Token: 0x04000241 RID: 577
		private global::System.Windows.Forms.Label label95;

		// Token: 0x04000242 RID: 578
		private global::System.Windows.Forms.Label label96;

		// Token: 0x04000243 RID: 579
		private global::System.Windows.Forms.CheckBox chLengthMap;

		// Token: 0x04000244 RID: 580
		private global::System.Windows.Forms.CheckBox chWidthMap;

		// Token: 0x04000245 RID: 581
		private global::System.Windows.Forms.CheckBox chHeightMap;

		// Token: 0x04000246 RID: 582
		private global::System.Windows.Forms.TextBox tbLength;

		// Token: 0x04000247 RID: 583
		private global::System.Windows.Forms.TextBox tbWidth;

		// Token: 0x04000248 RID: 584
		private global::System.Windows.Forms.TextBox tbHeight;

		// Token: 0x04000249 RID: 585
		private global::System.Windows.Forms.CheckBox chHeightImport;

		// Token: 0x0400024A RID: 586
		private global::System.Windows.Forms.CheckBox chWidthImport;

		// Token: 0x0400024B RID: 587
		private global::System.Windows.Forms.CheckBox chLengthImport;

		// Token: 0x0400024C RID: 588
		private global::System.Windows.Forms.Label label98;

		// Token: 0x0400024D RID: 589
		private global::System.Windows.Forms.CheckBox chDeleteOldCatMapping;

		// Token: 0x0400024E RID: 590
		private global::System.Windows.Forms.TabPage tabProductSettings;

		// Token: 0x0400024F RID: 591
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;

		// Token: 0x04000250 RID: 592
		private global::System.Windows.Forms.Label label101;

		// Token: 0x04000251 RID: 593
		private global::System.Windows.Forms.Label label102;

		// Token: 0x04000252 RID: 594
		private global::System.Windows.Forms.Label label103;

		// Token: 0x04000253 RID: 595
		private global::System.Windows.Forms.Label label104;

		// Token: 0x04000254 RID: 596
		private global::System.Windows.Forms.Label label118;

		// Token: 0x04000255 RID: 597
		private global::System.Windows.Forms.CheckBox chProductPublishImport;

		// Token: 0x04000256 RID: 598
		private global::System.Windows.Forms.Label label117;

		// Token: 0x04000257 RID: 599
		private global::System.Windows.Forms.ComboBox ddDeliveryDate;

		// Token: 0x04000258 RID: 600
		private global::System.Windows.Forms.CheckBox chDayDeliveryImport;

		// Token: 0x04000259 RID: 601
		private global::System.Windows.Forms.Label label119;

		// Token: 0x0400025A RID: 602
		private global::System.Windows.Forms.CheckBox chInventoryMethodImport;

		// Token: 0x0400025B RID: 603
		private global::System.Windows.Forms.CheckBox chProductPublish;

		// Token: 0x0400025C RID: 604
		private global::System.Windows.Forms.Label label71;

		// Token: 0x0400025D RID: 605
		private global::System.Windows.Forms.CheckBox chProductReviewEnable;

		// Token: 0x0400025E RID: 606
		private global::System.Windows.Forms.CheckBox chProductReviewEnableImport;

		// Token: 0x0400025F RID: 607
		private global::System.Windows.Forms.TabPage tabSpecAttributes;

		// Token: 0x04000260 RID: 608
		private global::System.Windows.Forms.TabPage tabCustomer;

		// Token: 0x04000261 RID: 609
		private global::System.Windows.Forms.CheckBox chProductAllowQtyMap;

		// Token: 0x04000262 RID: 610
		private global::System.Windows.Forms.Label label145;

		// Token: 0x04000263 RID: 611
		private global::System.Windows.Forms.Label label140;

		// Token: 0x04000264 RID: 612
		private global::System.Windows.Forms.Label label141;

		// Token: 0x04000265 RID: 613
		private global::System.Windows.Forms.Label label142;

		// Token: 0x04000266 RID: 614
		private global::System.Windows.Forms.Label label143;

		// Token: 0x04000267 RID: 615
		private global::System.Windows.Forms.Label label144;

		// Token: 0x04000268 RID: 616
		private global::System.Windows.Forms.CheckBox chProductDisplayAvailability;

		// Token: 0x04000269 RID: 617
		private global::System.Windows.Forms.CheckBox chProductDisplayAvailabilityImport;

		// Token: 0x0400026A RID: 618
		private global::System.Windows.Forms.TextBox tbProductMinStockQty;

		// Token: 0x0400026B RID: 619
		private global::System.Windows.Forms.TextBox tbProductNotifyQty;

		// Token: 0x0400026C RID: 620
		private global::System.Windows.Forms.TextBox tbProductMinCartQty;

		// Token: 0x0400026D RID: 621
		private global::System.Windows.Forms.TextBox tbProductMaxCartQty;

		// Token: 0x0400026E RID: 622
		private global::System.Windows.Forms.TextBox tbProductAllowQty;

		// Token: 0x0400026F RID: 623
		private global::System.Windows.Forms.CheckBox chProductMinStockQtyAction;

		// Token: 0x04000270 RID: 624
		private global::System.Windows.Forms.CheckBox chProductNotifyQtyAction;

		// Token: 0x04000271 RID: 625
		private global::System.Windows.Forms.CheckBox chProductMinCartQtyAction;

		// Token: 0x04000272 RID: 626
		private global::System.Windows.Forms.CheckBox chProductMaxCartQtyAction;

		// Token: 0x04000273 RID: 627
		private global::System.Windows.Forms.CheckBox chProductAllowQtyAction;

		// Token: 0x04000274 RID: 628
		private global::System.Windows.Forms.CheckBox chProductMinStockQtyMap;

		// Token: 0x04000275 RID: 629
		private global::System.Windows.Forms.CheckBox chProductNotifyQtyMap;

		// Token: 0x04000276 RID: 630
		private global::System.Windows.Forms.CheckBox chProductMinCartQtyMap;

		// Token: 0x04000277 RID: 631
		private global::System.Windows.Forms.CheckBox chProductMaxCartQtyMap;

		// Token: 0x04000278 RID: 632
		private global::System.Windows.Forms.Label label146;

		// Token: 0x04000279 RID: 633
		private global::System.Windows.Forms.CheckBox chProductDisplayStockQuantity;

		// Token: 0x0400027A RID: 634
		private global::System.Windows.Forms.CheckBox chProductDisplayStockQuantityImport;

		// Token: 0x0400027B RID: 635
		private global::System.Windows.Forms.CheckBox chInsertProductsToLastSubCategory;

		// Token: 0x0400027C RID: 636
		private global::System.Windows.Forms.Label label147;

		// Token: 0x0400027D RID: 637
		private global::System.Windows.Forms.Label label149;

		// Token: 0x0400027E RID: 638
		private global::System.Windows.Forms.ComboBox ddlBackorderMode;

		// Token: 0x0400027F RID: 639
		private global::System.Windows.Forms.CheckBox chBackorderModeImport;

		// Token: 0x04000280 RID: 640
		private global::System.Windows.Forms.CheckBox chTitleCase;

		// Token: 0x04000281 RID: 641
		private global::System.Windows.Forms.Label label150;

		// Token: 0x04000282 RID: 642
		private global::System.Windows.Forms.CheckBox chDeleteOldPictures;

		// Token: 0x04000283 RID: 643
		private global::System.Windows.Forms.Label label151;

		// Token: 0x04000284 RID: 644
		private global::System.Windows.Forms.CheckBox chDeleteOldAttributtes;

		// Token: 0x04000285 RID: 645
		private global::System.Windows.Forms.Label label152;

		// Token: 0x04000286 RID: 646
		private global::System.Windows.Forms.CheckBox chDeleteOldSpecAttributes;

		// Token: 0x04000287 RID: 647
		private global::System.Windows.Forms.Label label156;

		// Token: 0x04000288 RID: 648
		private global::System.Windows.Forms.CheckBox chDayDeliveryMap;

		// Token: 0x04000289 RID: 649
		private global::System.Windows.Forms.Label label157;

		// Token: 0x0400028A RID: 650
		private global::System.Windows.Forms.Label label158;

		// Token: 0x0400028B RID: 651
		private global::System.Windows.Forms.Label label159;

		// Token: 0x0400028C RID: 652
		private global::System.Windows.Forms.Label label160;

		// Token: 0x0400028D RID: 653
		private global::System.Windows.Forms.CheckBox chProductShippingEnabledMap;

		// Token: 0x0400028E RID: 654
		private global::System.Windows.Forms.CheckBox chProductFreeShippingMap;

		// Token: 0x0400028F RID: 655
		private global::System.Windows.Forms.CheckBox chProductShipSeparatelyMap;

		// Token: 0x04000290 RID: 656
		private global::System.Windows.Forms.CheckBox chProductShippingChargeMap;

		// Token: 0x04000291 RID: 657
		private global::System.Windows.Forms.CheckBox chProductShippingEnabled;

		// Token: 0x04000292 RID: 658
		private global::System.Windows.Forms.CheckBox chProductFreeShipping;

		// Token: 0x04000293 RID: 659
		private global::System.Windows.Forms.CheckBox chProductShipSeparately;

		// Token: 0x04000294 RID: 660
		private global::System.Windows.Forms.CheckBox chProductShippingEnabledImport;

		// Token: 0x04000295 RID: 661
		private global::System.Windows.Forms.CheckBox chProductFreeShippingImport;

		// Token: 0x04000296 RID: 662
		private global::System.Windows.Forms.CheckBox chProductShipSeparatelyImport;

		// Token: 0x04000297 RID: 663
		private global::System.Windows.Forms.CheckBox chProductShippingChargeImport;

		// Token: 0x04000298 RID: 664
		private global::System.Windows.Forms.NumericUpDown nmProductShippingCharge;

		// Token: 0x04000299 RID: 665
		private global::System.Windows.Forms.ComboBox ddInventoryMethod;

		// Token: 0x0400029A RID: 666
		private global::System.Windows.Forms.Button btnQuickImport;

		// Token: 0x0400029B RID: 667
		private global::System.Windows.Forms.Label label169;

		// Token: 0x0400029C RID: 668
		private global::System.Windows.Forms.TabPage tabTierPricing;

		// Token: 0x0400029D RID: 669
		private global::System.Windows.Forms.Label label194;

		// Token: 0x0400029E RID: 670
		private global::System.Windows.Forms.ComboBox ddLowStockActivity;

		// Token: 0x0400029F RID: 671
		private global::System.Windows.Forms.CheckBox chLowStockActivityImport;

		// Token: 0x040002A0 RID: 672
		private global::System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;

		// Token: 0x040002A1 RID: 673
		private global::System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;

		// Token: 0x040002A2 RID: 674
		private global::System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;

		// Token: 0x040002A5 RID: 677
		private global::System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;

		// Token: 0x040002A6 RID: 678
		private global::System.Windows.Forms.CheckBox chInsertCategoryPictureOnInsert;

		// Token: 0x040002A7 RID: 679
		private global::System.Windows.Forms.Label label170;

		// Token: 0x040002A8 RID: 680
		private global::System.Windows.Forms.Label label175;

		// Token: 0x040002A9 RID: 681
		private global::System.Windows.Forms.Label label174;

		// Token: 0x040002AA RID: 682
		private global::System.Windows.Forms.CheckBox chVisibleIndividually;

		// Token: 0x040002AB RID: 683
		private global::System.Windows.Forms.CheckBox chVisibleIndividuallyImport;

		// Token: 0x040002AC RID: 684
		private global::System.Windows.Forms.ComboBox ddWarehouse;

		// Token: 0x040002AD RID: 685
		private global::System.Windows.Forms.CheckBox chWarehouseImport;

		// Token: 0x040002AE RID: 686
		private global::System.Windows.Forms.Label label176;

		// Token: 0x040002AF RID: 687
		private global::System.Windows.Forms.ComboBox ddProductType;

		// Token: 0x040002B0 RID: 688
		private global::System.Windows.Forms.CheckBox chProductTypeImport;

		// Token: 0x040002B1 RID: 689
		private global::System.Windows.Forms.TextBox tbSourceCopyPath;

		// Token: 0x040002B2 RID: 690
		private global::System.Windows.Forms.Label label177;

		// Token: 0x040002B3 RID: 691
		private global::System.Windows.Forms.TabPage tabTranslations;

		// Token: 0x040002B4 RID: 692
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;

		// Token: 0x040002B5 RID: 693
		private global::System.Windows.Forms.Label label178;

		// Token: 0x040002B6 RID: 694
		private global::System.Windows.Forms.Label label179;

		// Token: 0x040002B7 RID: 695
		private global::System.Windows.Forms.Label label180;

		// Token: 0x040002B8 RID: 696
		private global::System.Windows.Forms.Label label181;

		// Token: 0x040002B9 RID: 697
		private global::System.Windows.Forms.Label label182;

		// Token: 0x040002BA RID: 698
		private global::System.Windows.Forms.Label label183;

		// Token: 0x040002BB RID: 699
		private global::System.Windows.Forms.Label label186;

		// Token: 0x040002BC RID: 700
		private global::System.Windows.Forms.Label label187;

		// Token: 0x040002BD RID: 701
		private global::System.Windows.Forms.Label label188;

		// Token: 0x040002BE RID: 702
		private global::System.Windows.Forms.TextBox tbTranslateApiUrl;

		// Token: 0x040002BF RID: 703
		private global::System.Windows.Forms.TextBox tbTranslateSubscribtionKey;

		// Token: 0x040002C0 RID: 704
		private global::System.Windows.Forms.Label label197;

		// Token: 0x040002C1 RID: 705
		private global::System.Windows.Forms.ComboBox ddTranslateLanguagesList;

		// Token: 0x040002C2 RID: 706
		private global::System.Windows.Forms.CheckBox chTranslateProductName;

		// Token: 0x040002C3 RID: 707
		private global::System.Windows.Forms.CheckBox chTranslateProductShortDesc;

		// Token: 0x040002C4 RID: 708
		private global::System.Windows.Forms.CheckBox chTranslateProductFullDesc;

		// Token: 0x040002C5 RID: 709
		private global::System.Windows.Forms.CheckBox chTranslateProductCategory;

		// Token: 0x040002C6 RID: 710
		private global::System.Windows.Forms.CheckBox chTranslateProductAtt;

		// Token: 0x040002C7 RID: 711
		private global::System.Windows.Forms.NumericUpDown nmPictureResizeHeight;

		// Token: 0x040002C8 RID: 712
		private global::System.Windows.Forms.NumericUpDown nmPictureResizeWidth;

		// Token: 0x040002C9 RID: 713
		private global::System.Windows.Forms.NumericUpDown nmPictureResizeQuality;

		// Token: 0x040002CA RID: 714
		private global::System.Windows.Forms.Label label190;

		// Token: 0x040002CB RID: 715
		private global::System.Windows.Forms.Label label189;

		// Token: 0x040002CC RID: 716
		private global::System.Windows.Forms.Label label185;

		// Token: 0x040002CD RID: 717
		private global::System.Windows.Forms.CheckBox chResizePicture;

		// Token: 0x040002CE RID: 718
		private global::System.Windows.Forms.Label label184;

		// Token: 0x040002CF RID: 719
		private global::System.Windows.Forms.CheckBox chAddSpecAttributesAsTable;

		// Token: 0x040002D0 RID: 720
		private global::System.Windows.Forms.Label label191;

		// Token: 0x040002D1 RID: 721
		private global::System.Windows.Forms.Label label192;

		// Token: 0x040002D2 RID: 722
		private global::System.Windows.Forms.CheckedListBox chLimitedToSoresEntities;

		// Token: 0x040002D3 RID: 723
		private global::System.Windows.Forms.Panel panelProdAttributtes;

		// Token: 0x040002D4 RID: 724
		private global::System.Windows.Forms.Panel panelProdSpecAttributtes;

		// Token: 0x040002D5 RID: 725
		private global::System.Windows.Forms.TabPage tabCustomSql;

		// Token: 0x040002D6 RID: 726
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;

		// Token: 0x040002D7 RID: 727
		private global::System.Windows.Forms.Label label51;

		// Token: 0x040002D8 RID: 728
		private global::System.Windows.Forms.Label label52;

		// Token: 0x040002D9 RID: 729
		private global::System.Windows.Forms.Label label53;

		// Token: 0x040002DA RID: 730
		private global::System.Windows.Forms.Label label59;

		// Token: 0x040002DB RID: 731
		private global::System.Windows.Forms.ComboBox cbRunSqlOn;

		// Token: 0x040002DC RID: 732
		private global::System.Windows.Forms.TextBox tbSqlQueries;

		// Token: 0x040002DD RID: 733
		private global::System.Windows.Forms.TabPage tabAddressCustomAtt;

		// Token: 0x040002DE RID: 734
		private global::System.Windows.Forms.Panel panelCustomerGenericAtt;

		// Token: 0x040002DF RID: 735
		private global::System.Windows.Forms.CheckBox chDeleteOldAddressCustomAtt;

		// Token: 0x040002E0 RID: 736
		private global::System.Windows.Forms.Label label44;

		// Token: 0x040002E1 RID: 737
		private global::System.Windows.Forms.TabPage tabCustomerCustomAtt;

		// Token: 0x040002E2 RID: 738
		private global::System.Windows.Forms.Panel panelCustomerCustomAtt;

		// Token: 0x040002E3 RID: 739
		private global::System.Windows.Forms.CheckBox chDeleteOldCustomerCustomAtt;

		// Token: 0x040002E4 RID: 740
		private global::System.Windows.Forms.Label label54;

		// Token: 0x040002E5 RID: 741
		private global::System.Windows.Forms.CheckBox chVisibleIndividuallyMap;

		// Token: 0x040002E6 RID: 742
		private global::System.Windows.Forms.CheckBox chDisableNestedCategories;

		// Token: 0x040002E7 RID: 743
		private global::System.Windows.Forms.Label label55;

		// Token: 0x040002E8 RID: 744
		private global::System.Windows.Forms.CheckBox chProductIsDeletedMap;

		// Token: 0x040002E9 RID: 745
		private global::System.Windows.Forms.Label label57;

		// Token: 0x040002EA RID: 746
		private global::System.Windows.Forms.CheckBox chProductIsDeleted;

		// Token: 0x040002EB RID: 747
		private global::System.Windows.Forms.CheckBox chProductIsDeletedImport;

		// Token: 0x040002EC RID: 748
		private global::System.Windows.Forms.TabPage tabLogSettings;

		// Token: 0x040002ED RID: 749
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;

		// Token: 0x040002EE RID: 750
		private global::System.Windows.Forms.Label label60;

		// Token: 0x040002EF RID: 751
		private global::System.Windows.Forms.Label label61;

		// Token: 0x040002F0 RID: 752
		private global::System.Windows.Forms.Label label72;

		// Token: 0x040002F1 RID: 753
		private global::System.Windows.Forms.Label label73;

		// Token: 0x040002F2 RID: 754
		private global::System.Windows.Forms.Label label74;

		// Token: 0x040002F3 RID: 755
		private global::System.Windows.Forms.Label label78;

		// Token: 0x040002F4 RID: 756
		private global::System.Windows.Forms.Label label99;

		// Token: 0x040002F5 RID: 757
		private global::System.Windows.Forms.Label label100;

		// Token: 0x040002F6 RID: 758
		private global::System.Windows.Forms.Label label105;

		// Token: 0x040002F7 RID: 759
		private global::System.Windows.Forms.TextBox tbEmailTo;

		// Token: 0x040002F8 RID: 760
		private global::System.Windows.Forms.Label label106;

		// Token: 0x040002F9 RID: 761
		private global::System.Windows.Forms.TextBox tbEmailAddressFrom;

		// Token: 0x040002FA RID: 762
		private global::System.Windows.Forms.CheckBox checkBox_0;

		// Token: 0x040002FB RID: 763
		private global::System.Windows.Forms.TextBox tbEmailDisplayName;

		// Token: 0x040002FC RID: 764
		private global::System.Windows.Forms.TextBox tbEmailHost;

		// Token: 0x040002FD RID: 765
		private global::System.Windows.Forms.TextBox tbEmailPort;

		// Token: 0x040002FE RID: 766
		private global::System.Windows.Forms.TextBox tbEmailUser;

		// Token: 0x040002FF RID: 767
		private global::System.Windows.Forms.TextBox tbEmailPassword;

		// Token: 0x04000300 RID: 768
		private global::System.Windows.Forms.Label label108;

		// Token: 0x04000301 RID: 769
		private global::System.Windows.Forms.Label label107;

		// Token: 0x04000302 RID: 770
		private global::System.Windows.Forms.TextBox tbEmailSubject;

		// Token: 0x04000303 RID: 771
		private global::System.Windows.Forms.TextBox tbEmailBody;

		// Token: 0x04000304 RID: 772
		private global::System.Windows.Forms.TabPage tabProdInfo;

		// Token: 0x04000305 RID: 773
		private global::System.Windows.Forms.Panel panelProdInfo;

		// Token: 0x04000306 RID: 774
		private global::System.Windows.Forms.Button btnCopy;

		// Token: 0x04000307 RID: 775
		private global::System.Windows.Forms.Panel panelProdPictures;

		// Token: 0x04000308 RID: 776
		private global::System.Windows.Forms.Panel panelCustomer;

		// Token: 0x04000309 RID: 777
		private global::System.Windows.Forms.TabPage tabCustomerShippingAddress;

		// Token: 0x0400030A RID: 778
		private global::System.Windows.Forms.Panel panelCustomerShippingAddress;

		// Token: 0x0400030B RID: 779
		private global::System.Windows.Forms.CheckBox chCustomerAddress_DeleteOld;

		// Token: 0x0400030C RID: 780
		private global::System.Windows.Forms.Label label43;

		// Token: 0x0400030D RID: 781
		private global::System.Windows.Forms.TabPage tabCustomerRole;

		// Token: 0x0400030E RID: 782
		private global::System.Windows.Forms.Panel panelCustomerRole;

		// Token: 0x0400030F RID: 783
		private global::System.Windows.Forms.CheckBox chCustomerRole_DeleteOld;

		// Token: 0x04000310 RID: 784
		private global::System.Windows.Forms.Label label36;

		// Token: 0x04000311 RID: 785
		private global::System.Windows.Forms.TabPage tabCustomerOther;

		// Token: 0x04000312 RID: 786
		private global::System.Windows.Forms.Panel panelCustomerOthers;

		// Token: 0x04000313 RID: 787
		private global::System.Windows.Forms.TabPage tabCustomerBillingAddress;

		// Token: 0x04000314 RID: 788
		private global::System.Windows.Forms.Panel panelCustomerBillingAddress;

		// Token: 0x04000315 RID: 789
		private global::System.Windows.Forms.CheckBox chCustomerAddress_DeleteOld2;

		// Token: 0x04000316 RID: 790
		private global::System.Windows.Forms.Label label45;

		// Token: 0x04000317 RID: 791
		private global::System.Windows.Forms.CheckBox chForceShippingAddressOnUpdate;

		// Token: 0x04000318 RID: 792
		private global::System.Windows.Forms.Label label46;

		// Token: 0x04000319 RID: 793
		private global::System.Windows.Forms.CheckBox chForceBillingAddressOnUpdate;

		// Token: 0x0400031A RID: 794
		private global::System.Windows.Forms.Label label47;

		// Token: 0x0400031B RID: 795
		private global::System.Windows.Forms.Label label48;

		// Token: 0x0400031C RID: 796
		private global::System.Windows.Forms.Panel panelTierPricing;

		// Token: 0x0400031D RID: 797
		private global::System.Windows.Forms.CheckBox chDeleteExistingTierPricingOnUpdate;

		// Token: 0x0400031E RID: 798
		private global::System.Windows.Forms.ComboBox ddParentCat0;

		// Token: 0x0400031F RID: 799
		private global::System.Windows.Forms.TabPage tabPrices;

		// Token: 0x04000320 RID: 800
		private global::System.Windows.Forms.Panel panelPrices;

		// Token: 0x04000321 RID: 801
		private global::System.Windows.Forms.TabPage tabWishList;

		// Token: 0x04000322 RID: 802
		private global::System.Windows.Forms.Label label11;

		// Token: 0x04000323 RID: 803
		private global::System.Windows.Forms.Panel panelWishList;

		// Token: 0x04000324 RID: 804
		private global::System.Windows.Forms.CheckBox chDeleteWishListOnUpdate;
        private System.Windows.Forms.CheckBox xmlModify;
    }
}
