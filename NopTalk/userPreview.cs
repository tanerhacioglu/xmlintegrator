﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using NopTalkCore;

namespace NopTalk
{
	public partial class userPreview : Form
	{
		public userPreview(SourceDocument sourceDocument, SourceMapping sourceMapping, TaskMapping taskMapping)
		{
			
			this.itemIndex = 0;
			this.components = null;
		
			this._sourceDocument = sourceDocument;
			this._sourceMapping = sourceMapping;
			this._taskMapping = taskMapping;
			this.InitializeComponent();
		}

		private void userPreview_Load(object sender, EventArgs e)
		{
			this.lblFoundItems.Text = this.lblFoundItems.Text + " " + this._sourceDocument.SourceItems.Count.ToString();
			this.ShowItem();
			base.WindowState = FormWindowState.Maximized;
		}

		private void ShowItem()
		{
			if (this._sourceDocument.SourceItems.Count > 0)
			{
				SourceItem sourceItem = this._sourceDocument.SourceItems[this.itemIndex];
				this.dgGenericAttributes.DataSource = null;
				if (sourceItem.AddressCustomAttributes != null && sourceItem.AddressCustomAttributes.Count > 0)
				{
					List<SourceItem.AttributeValues> list = new List<SourceItem.AttributeValues>();
					list.AddRange(sourceItem.AddressCustomAttributes);
					var dataSource = (from x in list
					select new
					{
						x.AttInfo_Name,
						x.AttValue_Value
					}).Distinct().ToList();
					this.dgGenericAttributes.AutoGenerateColumns = true;
					this.dgGenericAttributes.DataSource = dataSource;
					this.dgGenericAttributes.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
					this.dgGenericAttributes.ReadOnly = true;
				}
				this.dgCustomAttributes.DataSource = null;
				if (sourceItem.CustomerCustomAttributes != null && sourceItem.CustomerCustomAttributes.Count > 0)
				{
					List<SourceItem.AttributeValues> list2 = new List<SourceItem.AttributeValues>();
					list2.AddRange(sourceItem.CustomerCustomAttributes);
					var dataSource2 = (from x in list2
					select new
					{
						x.AttInfo_Name,
						x.AttValue_Value
					}).Distinct().ToList();
					this.dgCustomAttributes.AutoGenerateColumns = true;
					this.dgCustomAttributes.DataSource = dataSource2;
					this.dgCustomAttributes.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
					this.dgCustomAttributes.ReadOnly = true;
				}
				this.dgAccessRoles.DataSource = null;
				if (sourceItem.User.CustomerRoleItems != null && sourceItem.User.CustomerRoleItems.Count > 0)
				{
					this.dgAccessRoles.AutoGenerateColumns = true;
					IEnumerable<SourceItem.CustomerRuleItem> source = from w in sourceItem.User.CustomerRoleItems
					where w.CustomerRule != ""
					select w;
					this.dgAccessRoles.DataSource = (from x in source
					select new
					{
						CustomerRole = x.CustomerRule
					}).ToList();
					this.dgAccessRoles.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
					this.dgAccessRoles.ReadOnly = true;
				}
				this.dgOthers.DataSource = null;
				if (sourceItem.User.CustomerOthersItem != null)
				{
					this.dgOthers.AutoGenerateColumns = true;
					SourceItem.CustomerOthersItem customerOthersItem = sourceItem.User.CustomerOthersItem;
					List<userPreview.DataForView> list3 = new List<userPreview.DataForView>();
					List<userPreview.DataForView> list4 = list3;
					userPreview.DataForView dataForView = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping = this._sourceMapping.SourceCustomerOthersMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 261);
					dataForView.FieldName = ((sourceItemMapping != null) ? sourceItemMapping.FieldName : null);
					dataForView.FieldValue = customerOthersItem.Address3;
					list4.Add(dataForView);
					List<userPreview.DataForView> list5 = list3;
					userPreview.DataForView dataForView2 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping2 = this._sourceMapping.SourceCustomerOthersMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 262);
					dataForView2.FieldName = ((sourceItemMapping2 != null) ? sourceItemMapping2.FieldName : null);
					dataForView2.FieldValue = customerOthersItem.AddressName;
					list5.Add(dataForView2);
					List<userPreview.DataForView> list6 = list3;
					userPreview.DataForView dataForView3 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping3 = this._sourceMapping.SourceCustomerOthersMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 264);
					dataForView3.FieldName = ((sourceItemMapping3 != null) ? sourceItemMapping3.FieldName : null);
					dataForView3.FieldValue = customerOthersItem.GPCustomer;
					list6.Add(dataForView3);
					List<userPreview.DataForView> list7 = list3;
					userPreview.DataForView dataForView4 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping4 = this._sourceMapping.SourceCustomerOthersMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 266);
					dataForView4.FieldName = ((sourceItemMapping4 != null) ? sourceItemMapping4.FieldName : null);
					dataForView4.FieldValue = GlobalClass.ObjectToString(customerOthersItem.MainAddress);
					list7.Add(dataForView4);
					List<userPreview.DataForView> list8 = list3;
					userPreview.DataForView dataForView5 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping5 = this._sourceMapping.SourceCustomerOthersMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 263);
					dataForView5.FieldName = ((sourceItemMapping5 != null) ? sourceItemMapping5.FieldName : null);
					dataForView5.FieldValue = customerOthersItem.SAPCustomer;
					list8.Add(dataForView5);
					List<userPreview.DataForView> list9 = list3;
					userPreview.DataForView dataForView6 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping6 = this._sourceMapping.SourceCustomerOthersMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 265);
					dataForView6.FieldName = ((sourceItemMapping6 != null) ? sourceItemMapping6.FieldName : null);
					dataForView6.FieldValue = customerOthersItem.AddressSAPCustomer;
					list9.Add(dataForView6);
					this.dgOthers.DataSource = list3.ToList<userPreview.DataForView>();
					this.dgOthers.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
					this.dgOthers.ReadOnly = true;
				}
				this.dgCustomer.DataSource = null;
				if (sourceItem.User != null)
				{
					this.dgCustomer.AutoGenerateColumns = true;
					SourceItem.CustomerItem user = sourceItem.User;
					List<userPreview.DataForView> list10 = new List<userPreview.DataForView>();
					List<userPreview.DataForView> list11 = list10;
					userPreview.DataForView dataForView7 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping7 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 210);
					dataForView7.FieldName = ((sourceItemMapping7 != null) ? sourceItemMapping7.FieldName : null);
					dataForView7.FieldValue = user.Username;
					list11.Add(dataForView7);
					List<userPreview.DataForView> list12 = list10;
					userPreview.DataForView dataForView8 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping8 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 212);
					dataForView8.FieldName = ((sourceItemMapping8 != null) ? sourceItemMapping8.FieldName : null);
					dataForView8.FieldValue = user.Password;
					list12.Add(dataForView8);
					List<userPreview.DataForView> list13 = list10;
					userPreview.DataForView dataForView9 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping9 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 211);
					dataForView9.FieldName = ((sourceItemMapping9 != null) ? sourceItemMapping9.FieldName : null);
					dataForView9.FieldValue = user.Email;
					list13.Add(dataForView9);
					List<userPreview.DataForView> list14 = list10;
					userPreview.DataForView dataForView10 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping10 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 213);
					dataForView10.FieldName = ((sourceItemMapping10 != null) ? sourceItemMapping10.FieldName : null);
					dataForView10.FieldValue = user.FirstName;
					list14.Add(dataForView10);
					List<userPreview.DataForView> list15 = list10;
					userPreview.DataForView dataForView11 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping11 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 214);
					dataForView11.FieldName = ((sourceItemMapping11 != null) ? sourceItemMapping11.FieldName : null);
					dataForView11.FieldValue = user.LastName;
					list15.Add(dataForView11);
					List<userPreview.DataForView> list16 = list10;
					userPreview.DataForView dataForView12 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping12 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 217);
					dataForView12.FieldName = ((sourceItemMapping12 != null) ? sourceItemMapping12.FieldName : null);
					dataForView12.FieldValue = user.CustomerMainAddress.Company;
					list16.Add(dataForView12);
					List<userPreview.DataForView> list17 = list10;
					userPreview.DataForView dataForView13 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping13 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 216);
					dataForView13.FieldName = ((sourceItemMapping13 != null) ? sourceItemMapping13.FieldName : null);
					dataForView13.FieldValue = user.BirthDay;
					list17.Add(dataForView13);
					List<userPreview.DataForView> list18 = list10;
					userPreview.DataForView dataForView14 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping14 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 215);
					dataForView14.FieldName = ((sourceItemMapping14 != null) ? sourceItemMapping14.FieldName : null);
					dataForView14.FieldValue = user.Gender;
					list18.Add(dataForView14);
					List<userPreview.DataForView> list19 = list10;
					userPreview.DataForView dataForView15 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping15 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 235);
					dataForView15.FieldName = ((sourceItemMapping15 != null) ? sourceItemMapping15.FieldName : null);
					dataForView15.FieldValue = user.CustomerMainAddress.Country;
					list19.Add(dataForView15);
					List<userPreview.DataForView> list20 = list10;
					userPreview.DataForView dataForView16 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping16 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 237);
					dataForView16.FieldName = ((sourceItemMapping16 != null) ? sourceItemMapping16.FieldName : null);
					dataForView16.FieldValue = user.CustomerMainAddress.City;
					list20.Add(dataForView16);
					List<userPreview.DataForView> list21 = list10;
					userPreview.DataForView dataForView17 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping17 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 236);
					dataForView17.FieldName = ((sourceItemMapping17 != null) ? sourceItemMapping17.FieldName : null);
					dataForView17.FieldValue = user.CustomerMainAddress.State;
					list21.Add(dataForView17);
					List<userPreview.DataForView> list22 = list10;
					userPreview.DataForView dataForView18 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping18 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 241);
					dataForView18.FieldName = ((sourceItemMapping18 != null) ? sourceItemMapping18.FieldName : null);
					dataForView18.FieldValue = user.CustomerMainAddress.Phone;
					list22.Add(dataForView18);
					List<userPreview.DataForView> list23 = list10;
					userPreview.DataForView dataForView19 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping19 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 244);
					dataForView19.FieldName = ((sourceItemMapping19 != null) ? sourceItemMapping19.FieldName : null);
					dataForView19.FieldValue = user.CustomerMainAddress.County;
					list23.Add(dataForView19);
					List<userPreview.DataForView> list24 = list10;
					userPreview.DataForView dataForView20 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping20 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 240);
					dataForView20.FieldName = ((sourceItemMapping20 != null) ? sourceItemMapping20.FieldName : null);
					dataForView20.FieldValue = user.CustomerMainAddress.ZipPostalCode;
					list24.Add(dataForView20);
					List<userPreview.DataForView> list25 = list10;
					userPreview.DataForView dataForView21 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping21 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 242);
					dataForView21.FieldName = ((sourceItemMapping21 != null) ? sourceItemMapping21.FieldName : null);
					dataForView21.FieldValue = user.CustomerMainAddress.Fax;
					list25.Add(dataForView21);
					List<userPreview.DataForView> list26 = list10;
					userPreview.DataForView dataForView22 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping22 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 238);
					dataForView22.FieldName = ((sourceItemMapping22 != null) ? sourceItemMapping22.FieldName : null);
					dataForView22.FieldValue = user.CustomerMainAddress.Address1;
					list26.Add(dataForView22);
					List<userPreview.DataForView> list27 = list10;
					userPreview.DataForView dataForView23 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping23 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 239);
					dataForView23.FieldName = ((sourceItemMapping23 != null) ? sourceItemMapping23.FieldName : null);
					dataForView23.FieldValue = user.CustomerMainAddress.Address2;
					list27.Add(dataForView23);
					List<userPreview.DataForView> list28 = list10;
					userPreview.DataForView dataForView24 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping24 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 220);
					dataForView24.FieldName = ((sourceItemMapping24 != null) ? sourceItemMapping24.FieldName : null);
					dataForView24.FieldValue = user.ManagerOfVendor;
					list28.Add(dataForView24);
					List<userPreview.DataForView> list29 = list10;
					userPreview.DataForView dataForView25 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping25 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 219);
					dataForView25.FieldName = ((sourceItemMapping25 != null) ? sourceItemMapping25.FieldName : null);
					dataForView25.FieldValue = GlobalClass.ObjectToString(user.Newsletter);
					list29.Add(dataForView25);
					List<userPreview.DataForView> list30 = list10;
					userPreview.DataForView dataForView26 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping26 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 218);
					dataForView26.FieldName = ((sourceItemMapping26 != null) ? sourceItemMapping26.FieldName : null);
					dataForView26.FieldValue = GlobalClass.ObjectToString(user.IsTaxExempt);
					list30.Add(dataForView26);
					List<userPreview.DataForView> list31 = list10;
					userPreview.DataForView dataForView27 = new userPreview.DataForView();
					SourceItemMapping sourceItemMapping27 = this._sourceMapping.SourceCustomerMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 221);
					dataForView27.FieldName = ((sourceItemMapping27 != null) ? sourceItemMapping27.FieldName : null);
					dataForView27.FieldValue = GlobalClass.ObjectToString(user.Active);
					list31.Add(dataForView27);
					this.dgCustomer.DataSource = list10.ToList<userPreview.DataForView>();
					this.dgCustomer.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
					this.dgCustomer.ReadOnly = true;
				}
				this.dgAddresses.DataSource = null;
				if (sourceItem.User != null)
				{
					this.dgAddresses.AutoGenerateColumns = true;
					this.dgAddresses.DataSource = sourceItem.User.CustomerAddressItems.ToList<SourceItem.CustomerAddressItem>();
					this.dgAddresses.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
					this.dgAddresses.ReadOnly = true;
				}
			}
			this.lblShow.Text = "Show item: " + (this.itemIndex + 1).ToString() + "/" + this._sourceDocument.SourceItems.Count.ToString();
			if (this.itemIndex <= 0)
			{
				this.btnPrev.Enabled = false;
			}
			else
			{
				this.btnPrev.Enabled = true;
			}
			if (this.itemIndex == this._sourceDocument.SourceItems.Count - 1 || this._sourceDocument.SourceItems.Count == 0)
			{
				this.btnNext.Enabled = false;
			}
			else
			{
				this.btnNext.Enabled = true;
			}
		}

		private void btnNext_Click(object sender, EventArgs e)
		{
			if (this.itemIndex < this._sourceDocument.SourceItems.Count - 1)
			{
				this.itemIndex++;
				this.ShowItem();
			}
		}

		private void btnPrev_Click(object sender, EventArgs e)
		{
			if (this.itemIndex > 0)
			{
				this.itemIndex--;
				this.ShowItem();
			}
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams createParams = base.CreateParams;
				createParams.ExStyle |= 33554432;
				return createParams;
			}
		}

		private SourceDocument _sourceDocument;

		private SourceMapping _sourceMapping;

		private TaskMapping _taskMapping;

		private int itemIndex;

		public class DataForView
		{
			public string FieldName { get; set; }

			public string FieldValue { get; set; }

		
		}
	}
}
