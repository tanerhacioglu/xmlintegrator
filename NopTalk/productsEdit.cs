﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using NopTalkCore;

namespace NopTalk
{
	// Token: 0x02000012 RID: 18
	public partial class productsEdit : Form
	{
		// Token: 0x06000072 RID: 114 RVA: 0x0000A678 File Offset: 0x00008878
		public productsEdit()
		{
			
			this.formLoaded = false;
			this.total = 0L;
			this.databaseType = DatabaseType.MSSQL;
			this.bs = new BindingSource();
			this.sqlDp = new SqlDataAdapter();
			this.mySqlDp = new MySqlDataAdapter();
			this.components = null;
			this.InitializeComponent();
		}

		// Token: 0x06000073 RID: 115 RVA: 0x0000A6DC File Offset: 0x000088DC
		private async void dataEdit_Load(object sender, EventArgs e)
		{
			this.InitialData();
			
		}

		// Token: 0x06000074 RID: 116 RVA: 0x0000A728 File Offset: 0x00008928
		private void InitialData()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			string dataConnectionString = dataSettingsManager.LoadSettings(null).DataConnectionString;
			this.bs.ListChanged += new ListChangedEventHandler(this.ListChanged);
			this.dgDataEditor.CellBeginEdit += this.dgv_CellBeginEdit;
			this.dgDataEditor.CellEndEdit += this.dgv_CellEndEdit;
			helper.FillDropDownList("select Id, Name From EShop", this.ddStoresConn, dataConnectionString);
			this.ddStoresConn.SelectedIndex = -1;
			this.ddItemsLimit.SelectedIndex = 0;
			this.FilterEnable(false);
			this.formLoaded = true;
		}

		// Token: 0x06000075 RID: 117 RVA: 0x0000A7C4 File Offset: 0x000089C4
		private void dgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (this.dgDataEditor[e.ColumnIndex, e.RowIndex].Value != this.previousValue)
			{
				this.dgDataEditor[e.ColumnIndex, e.RowIndex].Style.BackColor = ColorTranslator.FromHtml("#F19FB9");
				this.SetStatusArea(false, "Data changed. To save click button Save.", true);
			}
		}

		// Token: 0x06000076 RID: 118 RVA: 0x0000265E File Offset: 0x0000085E
		private void dgv_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			this.previousValue = this.dgDataEditor[e.ColumnIndex, e.RowIndex].Value;
		}

		// Token: 0x06000077 RID: 119 RVA: 0x0000A834 File Offset: 0x00008A34
		private void ListChanged(object sender, EventArgs e)
		{
			BindingSource bindingSource = this.bs;
			int? num;
			if (bindingSource == null)
			{
				num = null;
			}
			else
			{
				IList list = bindingSource.List;
				num = ((list != null) ? new int?(list.Count) : null);
			}
			int? num2 = num;
			int valueOrDefault = num2.GetValueOrDefault();
			this.lblStatistic.Text = string.Format("Items Filtered: {0}", valueOrDefault);
		}

		// Token: 0x06000078 RID: 120 RVA: 0x0000A898 File Offset: 0x00008A98
		private void FilterEnable(bool enable)
		{
			this.clbColumnsList.Enabled = enable;
			this.clbCategoriesList.Enabled = enable;
			this.tbSearch_ProductName.Enabled = enable;
			this.tbSearch_ProductSku.Enabled = enable;
			this.chPublished.Enabled = enable;
			this.btnFilter.Enabled = enable;
			this.btnFilterClear.Enabled = enable;
			this.btnSave.Enabled = enable;
			this.chPublished.Enabled = enable;
		}

		// Token: 0x06000079 RID: 121 RVA: 0x0000A914 File Offset: 0x00008B14
		private async void LoadStoreData(string sqlQuery, ComboBox control, CheckedListBox control2, DataGridView control3, bool loadEditorDefaultColumns = false, EditorQueryType? editorQueryType = null)
		{
			if (this.ddStoresConn.SelectedIndex > -1 && GlobalClass.IsInteger(this.ddStoresConn.SelectedValue) && this.formLoaded)
			{
				try
				{
					StoreData storeData = StoreFunc.GetStoreData(Convert.ToInt64(this.ddStoresConn.SelectedValue));
					if (this.databaseType == DatabaseType.MSSQL)
					{
						EditorQueryType? editorQueryType2 = editorQueryType;
						if (editorQueryType2.GetValueOrDefault() == EditorQueryType.ProductTableSchema & editorQueryType2 != null)
						{
							sqlQuery = "SELECT Distinct Name as Id, Name as Name FROM sys.columns WHERE object_id = OBJECT_ID('Product') ";
						}
					}
					if (this.databaseType == DatabaseType.MySQL)
					{
						EditorQueryType? editorQueryType2 = editorQueryType;
						if (editorQueryType2.GetValueOrDefault() == EditorQueryType.ProductTableSchema & editorQueryType2 != null)
						{
							sqlQuery = "SELECT COLUMN_NAME as Id, COLUMN_NAME as Name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME LIKE 'Product' ";
						}
					}
					string storeConnString = storeData.DatabaseConnString;
					if (control != null)
					{
						helper.FillDropDownList2(sqlQuery, control, storeConnString, storeData.DatabaseType);
					}
					if (control2 != null)
					{
						helper.FillCheckBoxList2(sqlQuery, control2, storeConnString, storeData.DatabaseType);
					}
					if (control3 != null)
					{
						if (storeData.DatabaseType == DatabaseType.MSSQL)
						{
							this.LoadProductsMSSQL(sqlQuery, storeConnString);
						}
						else
						{
							this.LoadProductsMySQL(sqlQuery, storeConnString);
						}
					}
					if (loadEditorDefaultColumns)
					{
						EditorConfigData editorConfigData = storeData.EditorConfigData;
						if (((editorConfigData != null) ? editorConfigData.ProductColumns : null) == null)
						{
							storeData.EditorConfigData = new EditorConfigData();
						}
						if (!storeData.EditorConfigData.ProductColumns.Any<string>())
						{
							storeData.EditorConfigData.ProductColumns.Add("Id");
							storeData.EditorConfigData.ProductColumns.Add("Name");
							storeData.EditorConfigData.ProductColumns.Add("ShortDescription");
							storeData.EditorConfigData.ProductColumns.Add("FullDescription");
							storeData.EditorConfigData.ProductColumns.Add("Sku");
							storeData.EditorConfigData.ProductColumns.Add("Price");
							storeData.EditorConfigData.ProductColumns.Add("OldPrice");
							storeData.EditorConfigData.ProductColumns.Add("StockQuantity");
							storeData.EditorConfigData.ProductColumns.Add("Published");
							storeData.EditorConfigData.ProductColumns.Add("DisplayOrder");
							storeData.EditorConfigData.ProductColumns.Add("CreatedOnUtc");
							storeData.EditorConfigData.ProductColumns.Add("UpdatedOnUtc");
						}
						int num;
						//for (int i = 0; i <= control2.Items.Count - 1; i = num + 1)
						//{
						//	productsEdit.<>c__DisplayClass14_0 CS$<>8__locals1 = new productsEdit.<>c__DisplayClass14_0();
						//	CS$<>8__locals1.value = (control2.Items[i] as DataRowView)[0].ToString();
						//	if (storeData.EditorConfigData.ProductColumns.Exists((string x) => x == CS$<>8__locals1.value))
						//	{
						//		control2.SetItemChecked(i, true);
						//	}
						//	CS$<>8__locals1 = null;
						//	num = i;
						//}
					}
					storeData = null;
					storeConnString = null;
					return;
				}
				catch (Exception ex2)
				{
					Exception ex = ex2;
					MessageBox.Show(this, "Need provide the correct connection string to the nopCommerce store. Go to->settings->stores. Error: " + ex.Message, "Error");
					return;
				}
			}
			if (control != null)
			{
				control.DataSource = null;
			}
			if (control2 != null)
			{
				control2.DataSource = null;
			}
		}

		// Token: 0x0600007A RID: 122 RVA: 0x0000A980 File Offset: 0x00008B80
		private void LoadProductsMSSQL(string sqlQuery, string storeConnString)
		{
			this.dgDataEditor.AutoGenerateColumns = true;
			SqlConnection sqlConnection = new SqlConnection(storeConnString);
			this.sqlDp.SelectCommand = new SqlCommand(sqlQuery, sqlConnection);
			List<string> list = new List<string>();
			for (int i = 0; i <= this.clbColumnsList.CheckedItems.Count - 1; i++)
			{
				string text = (this.clbColumnsList.CheckedItems[i] as DataRowView)[0].ToString();
				if (!string.IsNullOrEmpty(text) && text.ToLower() != "id" && text.ToLower() != "updatedonutc" && text.ToLower() != "createdonutc")
				{
					list.Add(text);
				}
			}
			DataTable dataTable = new DataTable();
			this.sqlDp.Fill(dataTable);
			this.sqlDp.UpdateCommand = helper.GetSqlUpdateCommandMSSQL("Product", sqlConnection, list);
			this.bs.DataSource = dataTable;
			this.dgDataEditor.DataSource = this.bs;
			this.dgDataEditor.BorderStyle = BorderStyle.FixedSingle;
			this.dgDataEditor.AllowUserToAddRows = false;
			this.dgDataEditor.AllowUserToDeleteRows = false;
			if (this.dgDataEditor.Columns.Contains("Id"))
			{
				this.dgDataEditor.Columns["Id"].ReadOnly = true;
				this.dgDataEditor.Columns["Id"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("Name"))
			{
				this.dgDataEditor.Columns["Name"].FillWeight = 10f;
				this.dgDataEditor.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			}
			if (this.dgDataEditor.Columns.Contains("ShortDescription"))
			{
				this.dgDataEditor.Columns["ShortDescription"].FillWeight = 5f;
				this.dgDataEditor.Columns["ShortDescription"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			}
			if (this.dgDataEditor.Columns.Contains("FullDescription"))
			{
				this.dgDataEditor.Columns["FullDescription"].FillWeight = 2f;
				this.dgDataEditor.Columns["FullDescription"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			}
			if (this.dgDataEditor.Columns.Contains("Sku"))
			{
				this.dgDataEditor.Columns["Sku"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("Price"))
			{
				this.dgDataEditor.Columns["Price"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("OldPrice"))
			{
				this.dgDataEditor.Columns["OldPrice"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("StockQuantity"))
			{
				this.dgDataEditor.Columns["StockQuantity"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("Published"))
			{
				this.dgDataEditor.Columns["Published"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("DisplayOrder"))
			{
				this.dgDataEditor.Columns["DisplayOrder"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("CreatedOnUtc"))
			{
				this.dgDataEditor.Columns["CreatedOnUtc"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("UpdatedOnUtc"))
			{
				this.dgDataEditor.Columns["UpdatedOnUtc"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("CreatedOnUtc"))
			{
				this.dgDataEditor.Columns["CreatedOnUtc"].ReadOnly = true;
			}
			if (this.dgDataEditor.Columns.Contains("UpdatedOnUtc"))
			{
				this.dgDataEditor.Columns["UpdatedOnUtc"].ReadOnly = true;
			}
		}

		// Token: 0x0600007B RID: 123 RVA: 0x0000ADF8 File Offset: 0x00008FF8
		private void LoadProductsMySQL(string sqlQuery, string storeConnString)
		{
			this.dgDataEditor.AutoGenerateColumns = true;
			MySqlConnection mySqlConnection = new MySqlConnection(storeConnString);
			this.mySqlDp.SelectCommand = new MySqlCommand(sqlQuery.SqlQueryToMySql<string>(), mySqlConnection);
			List<string> list = new List<string>();
			for (int i = 0; i <= this.clbColumnsList.CheckedItems.Count - 1; i++)
			{
				string text = (this.clbColumnsList.CheckedItems[i] as DataRowView)[0].ToString();
				if (!string.IsNullOrEmpty(text) && text.ToLower() != "id" && text.ToLower() != "updatedonutc" && text.ToLower() != "createdonutc")
				{
					list.Add(text);
				}
			}
			DataTable dataTable = new DataTable();
			this.mySqlDp.Fill(dataTable);
			this.mySqlDp.UpdateCommand = helper.GetMySqlUpdateCommandMSSQL("Product", mySqlConnection, list);
			this.bs.DataSource = dataTable;
			this.dgDataEditor.DataSource = this.bs;
			this.dgDataEditor.BorderStyle = BorderStyle.FixedSingle;
			this.dgDataEditor.AllowUserToAddRows = false;
			this.dgDataEditor.AllowUserToDeleteRows = false;
			if (this.dgDataEditor.Columns.Contains("Id"))
			{
				this.dgDataEditor.Columns["Id"].ReadOnly = true;
				this.dgDataEditor.Columns["Id"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("Name"))
			{
				this.dgDataEditor.Columns["Name"].FillWeight = 10f;
				this.dgDataEditor.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			}
			if (this.dgDataEditor.Columns.Contains("ShortDescription"))
			{
				this.dgDataEditor.Columns["ShortDescription"].FillWeight = 5f;
				this.dgDataEditor.Columns["ShortDescription"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			}
			if (this.dgDataEditor.Columns.Contains("FullDescription"))
			{
				this.dgDataEditor.Columns["FullDescription"].FillWeight = 2f;
				this.dgDataEditor.Columns["FullDescription"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			}
			if (this.dgDataEditor.Columns.Contains("Sku"))
			{
				this.dgDataEditor.Columns["Sku"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("Price"))
			{
				this.dgDataEditor.Columns["Price"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("OldPrice"))
			{
				this.dgDataEditor.Columns["OldPrice"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("StockQuantity"))
			{
				this.dgDataEditor.Columns["StockQuantity"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("Published"))
			{
				this.dgDataEditor.Columns["Published"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("DisplayOrder"))
			{
				this.dgDataEditor.Columns["DisplayOrder"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("CreatedOnUtc"))
			{
				this.dgDataEditor.Columns["CreatedOnUtc"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("UpdatedOnUtc"))
			{
				this.dgDataEditor.Columns["UpdatedOnUtc"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("CreatedOnUtc"))
			{
				this.dgDataEditor.Columns["CreatedOnUtc"].ReadOnly = true;
			}
			if (this.dgDataEditor.Columns.Contains("UpdatedOnUtc"))
			{
				this.dgDataEditor.Columns["UpdatedOnUtc"].ReadOnly = true;
			}
		}

		// Token: 0x0600007C RID: 124 RVA: 0x0000B274 File Offset: 0x00009474
		private void ddStoresConn_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.ddStoresConn.SelectedIndex > -1)
			{
				if (this.ddStoresConn.SelectedIndex > -1 && GlobalClass.IsInteger(this.ddStoresConn.SelectedValue) && this.formLoaded)
				{
					StoreData storeData = StoreFunc.GetStoreData(Convert.ToInt64(this.ddStoresConn.SelectedValue));
					this.databaseType = storeData.DatabaseType;
				}
				this.LoadStoreData(null, null, this.clbColumnsList, null, true, new EditorQueryType?(EditorQueryType.ProductTableSchema));
				this.LoadStoreData("select Id, Name From Category", null, this.clbCategoriesList, null, false, null);
				this.FilterEnable(true);
			}
			else
			{
				this.FilterEnable(false);
			}
		}

		// Token: 0x0600007D RID: 125 RVA: 0x00002682 File Offset: 0x00000882
		private void btnFilter_Click(object sender, EventArgs e)
		{
			this.LoadItems();
			this.SetStatusArea(false, "", true);
		}

		// Token: 0x0600007E RID: 126 RVA: 0x0000B324 File Offset: 0x00009524
		private void LoadItems()
		{
			List<string> list = new List<string>();
			for (int i = 0; i <= this.clbColumnsList.CheckedItems.Count - 1; i++)
			{
				string item = (this.clbColumnsList.CheckedItems[i] as DataRowView)[0].ToString();
				list.Add(item);
			}
			string text = "Product." + string.Join(", Product.", list.ToArray());
			string text2 = string.Format("SELECT DISTINCT {0} ", text);
			if (this.databaseType == DatabaseType.MSSQL)
			{
				text2 = string.Format("SELECT DISTINCT TOP {0} {1} ", this.ddItemsLimit.Text, text);
			}
			text2 += string.Format(" FROM Product LEFT OUTER JOIN Product_Category_Mapping ON Product.Id = Product_Category_Mapping.ProductId", new object[0]);
			text2 += string.Format(" WHERE (1=1) ", new object[0]);
			List<string> list2 = new List<string>();
			for (int j = 0; j <= this.clbCategoriesList.CheckedItems.Count - 1; j++)
			{
				string item2 = (this.clbCategoriesList.CheckedItems[j] as DataRowView)[0].ToString();
				list2.Add(item2);
			}
			if (this.tbSearch_ProductName.Text.Trim().Length > 0)
			{
				text2 += string.Format("AND Product.Name like '%{0}%'", this.tbSearch_ProductName.Text.Trim());
			}
			if (this.tbSearch_ProductSku.Text.Trim().Length > 0)
			{
				text2 += string.Format("AND Product.Sku like '%{0}%'", this.tbSearch_ProductSku.Text.Trim());
			}
			if (this.chPublished.Checked)
			{
				text2 += string.Format("AND Product.Published = 1", new object[0]);
			}
			if (list2.Any<string>())
			{
				text2 += string.Format("AND Product_Category_Mapping.CategoryId  in ('{0}')", string.Join("', '", list2.ToArray()));
			}
			if (this.databaseType == DatabaseType.MySQL)
			{
				text2 += string.Format("LIMIT {0}", this.ddItemsLimit.Text);
			}
			this.LoadStoreData(text2, null, null, this.dgDataEditor, false, null);
		}

		// Token: 0x0600007F RID: 127 RVA: 0x0000B56C File Offset: 0x0000976C
		private void btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				if (ImportManager.productsLimits > 0)
				{
					MessageBox.Show("This functionality is not working in free version");
				}
				else
				{
					if (this.databaseType == DatabaseType.MySQL)
					{
						this.mySqlDp.Update((DataTable)this.bs.DataSource);
					}
					else
					{
						this.sqlDp.Update((DataTable)this.bs.DataSource);
					}
					this.SetStatusArea(true, "Data saved successfully.", false);
					this.LoadItems();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
				this.SetStatusArea(false, "Data not saved.", false);
				Thread.Sleep(2000);
				this.SetStatusArea(false, null, false);
			}
		}

		// Token: 0x06000080 RID: 128 RVA: 0x0000B630 File Offset: 0x00009830
		private void SetStatusArea(bool valid = false, string message = null, bool info = false)
		{
			if (string.IsNullOrEmpty(message))
			{
				this.statusArea.Hide();
			}
			else
			{
				this.statusArea.Show();
				this.statusAreaText.Text = message;
				if (valid)
				{
					this.statusArea.BackColor = ColorTranslator.FromHtml("#009D00");
				}
				else
				{
					this.statusArea.BackColor = ColorTranslator.FromHtml("#C00000");
				}
				if (info)
				{
					this.statusArea.BackColor = ColorTranslator.FromHtml("#C07464");
				}
				this.statusAreaText.ForeColor = Color.White;
			}
		}

		// Token: 0x06000081 RID: 129 RVA: 0x000024A2 File Offset: 0x000006A2
		private void btnClose_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		// Token: 0x06000082 RID: 130 RVA: 0x00002697 File Offset: 0x00000897
		private void btnFilterClear_Click(object sender, EventArgs e)
		{
			this.tbSearch_ProductName.Text = "";
			this.tbSearch_ProductSku.Text = "";
			this.ddStoresConn.SelectedIndex = -1;
			this.dgDataEditor.DataSource = null;
		}

		// Token: 0x0400005D RID: 93
		private bool formLoaded;

		// Token: 0x0400005E RID: 94
		private long total;

		// Token: 0x0400005F RID: 95
		private DatabaseType databaseType;

		// Token: 0x04000060 RID: 96
		private BindingSource bs;

		// Token: 0x04000061 RID: 97
		private SqlDataAdapter sqlDp;

		// Token: 0x04000062 RID: 98
		private MySqlDataAdapter mySqlDp;

		// Token: 0x04000063 RID: 99
		private object previousValue;
	}
}
