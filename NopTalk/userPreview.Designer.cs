﻿namespace NopTalk
{
	// Token: 0x0200003B RID: 59
	public partial class userPreview : global::System.Windows.Forms.Form
	{
		// Token: 0x06000211 RID: 529 RVA: 0x00003195 File Offset: 0x00001395
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000212 RID: 530 RVA: 0x00035094 File Offset: 0x00033294
		private void InitializeComponent()
		{
			this.lblShow = new global::System.Windows.Forms.Label();
			this.btnPrev = new global::System.Windows.Forms.Button();
			this.btnNext = new global::System.Windows.Forms.Button();
			this.btnClose = new global::System.Windows.Forms.Button();
			this.lblFoundItems = new global::System.Windows.Forms.Label();
			this.tableLayoutPanel3 = new global::System.Windows.Forms.TableLayoutPanel();
			this.label2 = new global::System.Windows.Forms.Label();
			this.dgOthers = new global::System.Windows.Forms.DataGridView();
			this.dgAccessRoles = new global::System.Windows.Forms.DataGridView();
			this.dgCustomAttributes = new global::System.Windows.Forms.DataGridView();
			this.label23 = new global::System.Windows.Forms.Label();
			this.dgGenericAttributes = new global::System.Windows.Forms.DataGridView();
			this.label21 = new global::System.Windows.Forms.Label();
			this.label22 = new global::System.Windows.Forms.Label();
			this.label1 = new global::System.Windows.Forms.Label();
			this.tableLayoutPanel1 = new global::System.Windows.Forms.TableLayoutPanel();
			this.label3 = new global::System.Windows.Forms.Label();
			this.dgCustomer = new global::System.Windows.Forms.DataGridView();
			this.dgAddresses = new global::System.Windows.Forms.DataGridView();
			this.tableLayoutPanel3.SuspendLayout();
			((global::System.ComponentModel.ISupportInitialize)this.dgOthers).BeginInit();
			((global::System.ComponentModel.ISupportInitialize)this.dgAccessRoles).BeginInit();
			((global::System.ComponentModel.ISupportInitialize)this.dgCustomAttributes).BeginInit();
			((global::System.ComponentModel.ISupportInitialize)this.dgGenericAttributes).BeginInit();
			this.tableLayoutPanel1.SuspendLayout();
			((global::System.ComponentModel.ISupportInitialize)this.dgCustomer).BeginInit();
			((global::System.ComponentModel.ISupportInitialize)this.dgAddresses).BeginInit();
			base.SuspendLayout();
			this.lblShow.AutoSize = true;
			this.lblShow.Font = new global::System.Drawing.Font("Microsoft Sans Serif", 12f, global::System.Drawing.FontStyle.Bold, global::System.Drawing.GraphicsUnit.Point, 0);
			this.lblShow.Location = new global::System.Drawing.Point(400, 13);
			this.lblShow.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShow.Name = "lblShow";
			this.lblShow.Size = new global::System.Drawing.Size(138, 25);
			this.lblShow.TabIndex = 12;
			this.lblShow.Text = "Current Item:";
			this.btnPrev.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnPrev.Location = new global::System.Drawing.Point(1012, 676);
			this.btnPrev.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnPrev.Name = "btnPrev";
			this.btnPrev.Size = new global::System.Drawing.Size(100, 28);
			this.btnPrev.TabIndex = 11;
			this.btnPrev.Text = "Previuos";
			this.btnPrev.UseVisualStyleBackColor = true;
			this.btnPrev.Click += new global::System.EventHandler(this.btnPrev_Click);
			this.btnNext.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnNext.Location = new global::System.Drawing.Point(1120, 676);
			this.btnNext.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnNext.Name = "btnNext";
			this.btnNext.Size = new global::System.Drawing.Size(100, 28);
			this.btnNext.TabIndex = 10;
			this.btnNext.Text = "Next";
			this.btnNext.UseVisualStyleBackColor = true;
			this.btnNext.Click += new global::System.EventHandler(this.btnNext_Click);
			this.btnClose.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnClose.Location = new global::System.Drawing.Point(1228, 676);
			this.btnClose.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new global::System.Drawing.Size(100, 28);
			this.btnClose.TabIndex = 9;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new global::System.EventHandler(this.btnClose_Click);
			this.lblFoundItems.AutoSize = true;
			this.lblFoundItems.Font = new global::System.Drawing.Font("Microsoft Sans Serif", 12f, global::System.Drawing.FontStyle.Bold, global::System.Drawing.GraphicsUnit.Point, 0);
			this.lblFoundItems.Location = new global::System.Drawing.Point(16, 13);
			this.lblFoundItems.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFoundItems.Name = "lblFoundItems";
			this.lblFoundItems.Size = new global::System.Drawing.Size(144, 25);
			this.lblFoundItems.TabIndex = 8;
			this.lblFoundItems.Text = "Found Items: ";
			this.tableLayoutPanel3.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.tableLayoutPanel3.CellBorderStyle = global::System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel3.ColumnCount = 2;
			this.tableLayoutPanel3.ColumnStyles.Add(new global::System.Windows.Forms.ColumnStyle(global::System.Windows.Forms.SizeType.Percent, 14.73297f));
			this.tableLayoutPanel3.ColumnStyles.Add(new global::System.Windows.Forms.ColumnStyle(global::System.Windows.Forms.SizeType.Percent, 85.26704f));
			this.tableLayoutPanel3.Controls.Add(this.label2, 0, 3);
			this.tableLayoutPanel3.Controls.Add(this.dgOthers, 1, 3);
			this.tableLayoutPanel3.Controls.Add(this.dgAccessRoles, 1, 2);
			this.tableLayoutPanel3.Controls.Add(this.dgCustomAttributes, 1, 1);
			this.tableLayoutPanel3.Controls.Add(this.label23, 0, 2);
			this.tableLayoutPanel3.Controls.Add(this.dgGenericAttributes, 1, 0);
			this.tableLayoutPanel3.Controls.Add(this.label21, 0, 0);
			this.tableLayoutPanel3.Controls.Add(this.label22, 0, 1);
			this.tableLayoutPanel3.Location = new global::System.Drawing.Point(784, 45);
			this.tableLayoutPanel3.Margin = new global::System.Windows.Forms.Padding(4);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 4;
			this.tableLayoutPanel3.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Percent, 25f));
			this.tableLayoutPanel3.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Percent, 25f));
			this.tableLayoutPanel3.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Percent, 25f));
			this.tableLayoutPanel3.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Percent, 25f));
			this.tableLayoutPanel3.Size = new global::System.Drawing.Size(544, 577);
			this.tableLayoutPanel3.TabIndex = 13;
			this.label2.AutoSize = true;
			this.label2.Location = new global::System.Drawing.Point(5, 433);
			this.label2.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new global::System.Drawing.Size(51, 17);
			this.label2.TabIndex = 4;
			this.label2.Text = "Others";
			this.dgOthers.AllowUserToAddRows = false;
			this.dgOthers.AllowUserToDeleteRows = false;
			this.dgOthers.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.dgOthers.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgOthers.Location = new global::System.Drawing.Point(85, 437);
			this.dgOthers.Margin = new global::System.Windows.Forms.Padding(4);
			this.dgOthers.MultiSelect = false;
			this.dgOthers.Name = "dgOthers";
			this.dgOthers.RowHeadersWidth = 51;
			this.dgOthers.SelectionMode = global::System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgOthers.Size = new global::System.Drawing.Size(454, 135);
			this.dgOthers.TabIndex = 5;
			this.dgAccessRoles.AllowUserToAddRows = false;
			this.dgAccessRoles.AllowUserToDeleteRows = false;
			this.dgAccessRoles.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.dgAccessRoles.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgAccessRoles.Location = new global::System.Drawing.Point(85, 293);
			this.dgAccessRoles.Margin = new global::System.Windows.Forms.Padding(4);
			this.dgAccessRoles.MultiSelect = false;
			this.dgAccessRoles.Name = "dgAccessRoles";
			this.dgAccessRoles.RowHeadersWidth = 51;
			this.dgAccessRoles.SelectionMode = global::System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgAccessRoles.Size = new global::System.Drawing.Size(454, 135);
			this.dgAccessRoles.TabIndex = 5;
			this.dgCustomAttributes.AllowUserToAddRows = false;
			this.dgCustomAttributes.AllowUserToDeleteRows = false;
			this.dgCustomAttributes.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.dgCustomAttributes.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgCustomAttributes.Location = new global::System.Drawing.Point(85, 149);
			this.dgCustomAttributes.Margin = new global::System.Windows.Forms.Padding(4);
			this.dgCustomAttributes.MultiSelect = false;
			this.dgCustomAttributes.Name = "dgCustomAttributes";
			this.dgCustomAttributes.RowHeadersWidth = 51;
			this.dgCustomAttributes.SelectionMode = global::System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgCustomAttributes.Size = new global::System.Drawing.Size(454, 135);
			this.dgCustomAttributes.TabIndex = 5;
			this.label23.AutoSize = true;
			this.label23.Location = new global::System.Drawing.Point(5, 289);
			this.label23.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label23.Name = "label23";
			this.label23.Size = new global::System.Drawing.Size(57, 34);
			this.label23.TabIndex = 4;
			this.label23.Text = "Access Roles";
			this.dgGenericAttributes.AllowUserToAddRows = false;
			this.dgGenericAttributes.AllowUserToDeleteRows = false;
			this.dgGenericAttributes.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.dgGenericAttributes.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgGenericAttributes.Location = new global::System.Drawing.Point(85, 5);
			this.dgGenericAttributes.Margin = new global::System.Windows.Forms.Padding(4);
			this.dgGenericAttributes.MultiSelect = false;
			this.dgGenericAttributes.Name = "dgGenericAttributes";
			this.dgGenericAttributes.RowHeadersWidth = 51;
			this.dgGenericAttributes.SelectionMode = global::System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgGenericAttributes.Size = new global::System.Drawing.Size(454, 135);
			this.dgGenericAttributes.TabIndex = 5;
			this.label21.AutoSize = true;
			this.label21.Location = new global::System.Drawing.Point(5, 1);
			this.label21.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label21.Name = "label21";
			this.label21.Size = new global::System.Drawing.Size(68, 51);
			this.label21.TabIndex = 4;
			this.label21.Text = "Address Custom Attributes";
			this.label22.AutoSize = true;
			this.label22.Location = new global::System.Drawing.Point(5, 145);
			this.label22.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label22.Name = "label22";
			this.label22.Size = new global::System.Drawing.Size(68, 51);
			this.label22.TabIndex = 4;
			this.label22.Text = "Customer Custom Attributes";
			this.label1.AutoSize = true;
			this.label1.Location = new global::System.Drawing.Point(5, 1);
			this.label1.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new global::System.Drawing.Size(68, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "Customer";
			this.tableLayoutPanel1.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tableLayoutPanel1.CellBorderStyle = global::System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new global::System.Windows.Forms.ColumnStyle(global::System.Windows.Forms.SizeType.Percent, 14.91803f));
			this.tableLayoutPanel1.ColumnStyles.Add(new global::System.Windows.Forms.ColumnStyle(global::System.Windows.Forms.SizeType.Percent, 85.08197f));
			this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.dgCustomer, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.dgAddresses, 1, 1);
			this.tableLayoutPanel1.Location = new global::System.Drawing.Point(16, 45);
			this.tableLayoutPanel1.Margin = new global::System.Windows.Forms.Padding(4);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Percent, 50f));
			this.tableLayoutPanel1.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Percent, 50f));
			this.tableLayoutPanel1.Size = new global::System.Drawing.Size(760, 577);
			this.tableLayoutPanel1.TabIndex = 7;
			this.label3.AutoSize = true;
			this.label3.Location = new global::System.Drawing.Point(5, 289);
			this.label3.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new global::System.Drawing.Size(75, 17);
			this.label3.TabIndex = 7;
			this.label3.Text = "Addresses";
			this.dgCustomer.AllowUserToAddRows = false;
			this.dgCustomer.AllowUserToDeleteRows = false;
			this.dgCustomer.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.dgCustomer.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgCustomer.Location = new global::System.Drawing.Point(118, 5);
			this.dgCustomer.Margin = new global::System.Windows.Forms.Padding(4);
			this.dgCustomer.MultiSelect = false;
			this.dgCustomer.Name = "dgCustomer";
			this.dgCustomer.RowHeadersWidth = 51;
			this.dgCustomer.SelectionMode = global::System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgCustomer.Size = new global::System.Drawing.Size(637, 279);
			this.dgCustomer.TabIndex = 6;
			this.dgAddresses.AllowUserToAddRows = false;
			this.dgAddresses.AllowUserToDeleteRows = false;
			this.dgAddresses.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.dgAddresses.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgAddresses.Location = new global::System.Drawing.Point(118, 293);
			this.dgAddresses.Margin = new global::System.Windows.Forms.Padding(4);
			this.dgAddresses.MultiSelect = false;
			this.dgAddresses.Name = "dgAddresses";
			this.dgAddresses.RowHeadersWidth = 51;
			this.dgAddresses.SelectionMode = global::System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgAddresses.Size = new global::System.Drawing.Size(637, 279);
			this.dgAddresses.TabIndex = 8;
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(8f, 16f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(1345, 717);
			base.Controls.Add(this.tableLayoutPanel3);
			base.Controls.Add(this.lblShow);
			base.Controls.Add(this.btnPrev);
			base.Controls.Add(this.btnNext);
			base.Controls.Add(this.btnClose);
			base.Controls.Add(this.lblFoundItems);
			base.Controls.Add(this.tableLayoutPanel1);
			base.Name = "userPreview";
			this.Text = "User Preview";
			base.Load += new global::System.EventHandler(this.userPreview_Load);
			this.tableLayoutPanel3.ResumeLayout(false);
			this.tableLayoutPanel3.PerformLayout();
			((global::System.ComponentModel.ISupportInitialize)this.dgOthers).EndInit();
			((global::System.ComponentModel.ISupportInitialize)this.dgAccessRoles).EndInit();
			((global::System.ComponentModel.ISupportInitialize)this.dgCustomAttributes).EndInit();
			((global::System.ComponentModel.ISupportInitialize)this.dgGenericAttributes).EndInit();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			((global::System.ComponentModel.ISupportInitialize)this.dgCustomer).EndInit();
			((global::System.ComponentModel.ISupportInitialize)this.dgAddresses).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		// Token: 0x04000401 RID: 1025
		private global::System.ComponentModel.IContainer components;

		// Token: 0x04000402 RID: 1026
		private global::System.Windows.Forms.Label lblShow;

		// Token: 0x04000403 RID: 1027
		private global::System.Windows.Forms.Button btnPrev;

		// Token: 0x04000404 RID: 1028
		private global::System.Windows.Forms.Button btnNext;

		// Token: 0x04000405 RID: 1029
		private global::System.Windows.Forms.Button btnClose;

		// Token: 0x04000406 RID: 1030
		private global::System.Windows.Forms.Label lblFoundItems;

		// Token: 0x04000407 RID: 1031
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;

		// Token: 0x04000408 RID: 1032
		private global::System.Windows.Forms.DataGridView dgGenericAttributes;

		// Token: 0x04000409 RID: 1033
		private global::System.Windows.Forms.Label label21;

		// Token: 0x0400040A RID: 1034
		private global::System.Windows.Forms.DataGridView dgCustomAttributes;

		// Token: 0x0400040B RID: 1035
		private global::System.Windows.Forms.Label label22;

		// Token: 0x0400040C RID: 1036
		private global::System.Windows.Forms.DataGridView dgAccessRoles;

		// Token: 0x0400040D RID: 1037
		private global::System.Windows.Forms.Label label23;

		// Token: 0x0400040E RID: 1038
		private global::System.Windows.Forms.Label label1;

		// Token: 0x0400040F RID: 1039
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

		// Token: 0x04000410 RID: 1040
		private global::System.Windows.Forms.Label label3;

		// Token: 0x04000411 RID: 1041
		private global::System.Windows.Forms.DataGridView dgCustomer;

		// Token: 0x04000412 RID: 1042
		private global::System.Windows.Forms.DataGridView dgAddresses;

		// Token: 0x04000413 RID: 1043
		private global::System.Windows.Forms.Label label2;

		// Token: 0x04000414 RID: 1044
		private global::System.Windows.Forms.DataGridView dgOthers;
	}
}
