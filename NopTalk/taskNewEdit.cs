﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using NopTalk.Helpers;
using NopTalk.uc;
using NopTalkCore;

namespace NopTalk
{
	// Token: 0x02000029 RID: 41
	public partial class taskNewEdit : Form
	{
		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000103 RID: 259 RVA: 0x00002A54 File Offset: 0x00000C54
		// (set) Token: 0x06000104 RID: 260 RVA: 0x00002A5C File Offset: 0x00000C5C
		private taskMappingDataGrid taskMappingDgCustomerOthers { get; set; }

		// Token: 0x06000105 RID: 261 RVA: 0x00014AEC File Offset: 0x00012CEC
		private void CustomerOthersListLoad(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat)
		{
			this.panelCustomerOthers.Controls.Clear();
			this.taskMappingDgCustomerOthers = new taskMappingDataGrid(SourceMappingType.CustomerOthers, structureFormat);
			this.taskMappingDgCustomerOthers.TaskMappingListLoad(sourceMapping, taskMapping, this.loadDefaultDataFromSource);
			this.taskMappingDgCustomerOthers.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.taskMappingDgCustomerOthers.Dock = DockStyle.Fill;
			this.panelCustomerOthers.Controls.Add(this.taskMappingDgCustomerOthers);
		}

		// Token: 0x06000106 RID: 262 RVA: 0x00002A65 File Offset: 0x00000C65
		private void CustomerOthersListSave(TaskMapping taskMapping)
		{
			if (this.taskMappingDgCustomerOthers != null)
			{
				this.taskMappingDgCustomerOthers.TaskMappingListSave(taskMapping);
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000107 RID: 263 RVA: 0x00002A7E File Offset: 0x00000C7E
		// (set) Token: 0x06000108 RID: 264 RVA: 0x00002A86 File Offset: 0x00000C86
		private taskMappingDataGrid taskMappingDgCustomerRole { get; set; }

		// Token: 0x06000109 RID: 265 RVA: 0x00014B5C File Offset: 0x00012D5C
		private void CustomerRoleListLoad(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat)
		{
			this.panelCustomerRole.Controls.Clear();
			this.taskMappingDgCustomerRole = new taskMappingDataGrid(SourceMappingType.CustomerRole, structureFormat);
			this.taskMappingDgCustomerRole.TaskMappingListLoad(sourceMapping, taskMapping, this.loadDefaultDataFromSource);
			this.taskMappingDgCustomerRole.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.taskMappingDgCustomerRole.Dock = DockStyle.Fill;
			this.panelCustomerRole.Controls.Add(this.taskMappingDgCustomerRole);
		}

		// Token: 0x0600010A RID: 266 RVA: 0x00002A8F File Offset: 0x00000C8F
		private void CustomerRoleListSave(TaskMapping taskMapping)
		{
			if (this.taskMappingDgCustomerRole != null)
			{
				this.taskMappingDgCustomerRole.TaskMappingListSave(taskMapping);
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x0600010B RID: 267 RVA: 0x00002AA8 File Offset: 0x00000CA8
		// (set) Token: 0x0600010C RID: 268 RVA: 0x00002AB0 File Offset: 0x00000CB0
		private taskMappingDataGrid taskMappingDgCustomerShippingAddress { get; set; }

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x0600010D RID: 269 RVA: 0x00002AB9 File Offset: 0x00000CB9
		// (set) Token: 0x0600010E RID: 270 RVA: 0x00002AC1 File Offset: 0x00000CC1
		private taskMappingDataGrid taskMappingDgCustomerBillingAddress { get; set; }

		// Token: 0x0600010F RID: 271 RVA: 0x00014BCC File Offset: 0x00012DCC
		private void CustomerAddressListLoad(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat)
		{
			this.panelCustomerShippingAddress.Controls.Clear();
			this.taskMappingDgCustomerShippingAddress = new taskMappingDataGrid(SourceMappingType.ShippingAddress, structureFormat);
			this.taskMappingDgCustomerShippingAddress.TaskMappingListLoad(sourceMapping, taskMapping, this.loadDefaultDataFromSource);
			this.taskMappingDgCustomerShippingAddress.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.taskMappingDgCustomerShippingAddress.Dock = DockStyle.Fill;
			this.panelCustomerShippingAddress.Controls.Add(this.taskMappingDgCustomerShippingAddress);
			this.panelCustomerBillingAddress.Controls.Clear();
			this.taskMappingDgCustomerBillingAddress = new taskMappingDataGrid(SourceMappingType.BillingAddress, structureFormat);
			this.taskMappingDgCustomerBillingAddress.TaskMappingListLoad(sourceMapping, taskMapping, this.loadDefaultDataFromSource);
			this.taskMappingDgCustomerBillingAddress.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.taskMappingDgCustomerBillingAddress.Dock = DockStyle.Fill;
			this.panelCustomerBillingAddress.Controls.Add(this.taskMappingDgCustomerBillingAddress);
		}

		// Token: 0x06000110 RID: 272 RVA: 0x00002ACA File Offset: 0x00000CCA
		private void CustomerAddressListSave(TaskMapping taskMapping)
		{
			if (this.taskMappingDgCustomerShippingAddress != null)
			{
				this.taskMappingDgCustomerShippingAddress.TaskMappingListSave(taskMapping);
			}
			if (this.taskMappingDgCustomerBillingAddress != null)
			{
				this.taskMappingDgCustomerBillingAddress.TaskMappingListSave(taskMapping);
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000111 RID: 273 RVA: 0x00002AFA File Offset: 0x00000CFA
		// (set) Token: 0x06000112 RID: 274 RVA: 0x00002B02 File Offset: 0x00000D02
		private taskMappingDataGrid taskMappingDgCustomerCustomAtt { get; set; }

		// Token: 0x06000113 RID: 275 RVA: 0x00014C98 File Offset: 0x00012E98
		private void CustomerCustomAttributesListLoad(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat)
		{
			this.panelCustomerCustomAtt.Controls.Clear();
			this.taskMappingDgCustomerCustomAtt = new taskMappingDataGrid(SourceMappingType.CustomerCustomAttributtes, structureFormat);
			this.taskMappingDgCustomerCustomAtt.TaskMappingListLoad(sourceMapping, taskMapping, this.loadDefaultDataFromSource);
			this.taskMappingDgCustomerCustomAtt.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.taskMappingDgCustomerCustomAtt.Dock = DockStyle.Fill;
			this.panelCustomerCustomAtt.Controls.Add(this.taskMappingDgCustomerCustomAtt);
			this.chDeleteOldCustomerCustomAtt.Checked = taskMapping.TaskCustomerCustomAttMapping.DeleteBeforeImport;
		}

		// Token: 0x06000114 RID: 276 RVA: 0x00002B0B File Offset: 0x00000D0B
		private void CustomerCustomAttributesListSave(TaskMapping taskMapping)
		{
			if (this.taskMappingDgCustomerCustomAtt != null)
			{
				this.taskMappingDgCustomerCustomAtt.TaskMappingListSave(taskMapping);
			}
			taskMapping.TaskCustomerCustomAttMapping.DeleteBeforeImport = this.chDeleteOldCustomerCustomAtt.Checked;
		}

		// Token: 0x06000115 RID: 277 RVA: 0x00014D1C File Offset: 0x00012F1C
		private void CustomSqlLoad(TaskMapping taskMapping)
		{
			this.tbSqlQueries.ScrollBars = ScrollBars.Vertical;
			this.cbRunSqlOn.SelectedValue = taskMapping.CustomSqlAction.GetValueOrDefault();
			this.tbSqlQueries.Text = taskMapping.CustomSqlQueries;
		}

		// Token: 0x06000116 RID: 278 RVA: 0x00002B3A File Offset: 0x00000D3A
		private void CustomSqlSave(TaskMapping taskMapping)
		{
			taskMapping.CustomSqlAction = new int?(GlobalClass.StringToInteger(this.cbRunSqlOn.SelectedValue, 0));
			taskMapping.CustomSqlQueries = this.tbSqlQueries.Text;
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000117 RID: 279 RVA: 0x00002B69 File Offset: 0x00000D69
		// (set) Token: 0x06000118 RID: 280 RVA: 0x00002B71 File Offset: 0x00000D71
		private taskMappingDataGrid taskMappingDgAddressCustomAtt { get; set; }

		// Token: 0x06000119 RID: 281 RVA: 0x00014D64 File Offset: 0x00012F64
		private void AddressCustomAttributesListLoad(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat)
		{
			this.panelCustomerGenericAtt.Controls.Clear();
			this.taskMappingDgAddressCustomAtt = new taskMappingDataGrid(SourceMappingType.AddressCustomAttributtes, structureFormat);
			this.taskMappingDgAddressCustomAtt.TaskMappingListLoad(sourceMapping, taskMapping, this.loadDefaultDataFromSource);
			this.taskMappingDgAddressCustomAtt.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.taskMappingDgAddressCustomAtt.Dock = DockStyle.Fill;
			this.panelCustomerGenericAtt.Controls.Add(this.taskMappingDgAddressCustomAtt);
			this.chDeleteOldAddressCustomAtt.Checked = taskMapping.TaskAddressCustomAttMapping.DeleteBeforeImport;
		}

		// Token: 0x0600011A RID: 282 RVA: 0x00002B7A File Offset: 0x00000D7A
		private void AddressCustomAttributesListSave(TaskMapping taskMapping)
		{
			if (this.taskMappingDgAddressCustomAtt != null)
			{
				this.taskMappingDgAddressCustomAtt.TaskMappingListSave(taskMapping);
			}
			taskMapping.TaskAddressCustomAttMapping.DeleteBeforeImport = this.chDeleteOldAddressCustomAtt.Checked;
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600011B RID: 283 RVA: 0x00002BA9 File Offset: 0x00000DA9
		// (set) Token: 0x0600011C RID: 284 RVA: 0x00002BB1 File Offset: 0x00000DB1
		private taskMappingDataGrid taskMappingDgCustomer { get; set; }

		// Token: 0x0600011D RID: 285 RVA: 0x00014DE8 File Offset: 0x00012FE8
		private void CustomerListLoad(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat)
		{
			this.panelCustomer.Controls.Clear();
			this.taskMappingDgCustomer = new taskMappingDataGrid(SourceMappingType.Customer, structureFormat);
			this.taskMappingDgCustomer.TaskMappingListLoad(sourceMapping, taskMapping, this.loadDefaultDataFromSource);
			this.taskMappingDgCustomer.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.taskMappingDgCustomer.Dock = DockStyle.Fill;
			this.panelCustomer.Controls.Add(this.taskMappingDgCustomer);
		}

		// Token: 0x0600011E RID: 286 RVA: 0x00002BBA File Offset: 0x00000DBA
		private void CustomerListSave(TaskMapping taskMapping)
		{
			if (this.taskMappingDgCustomer != null)
			{
				this.taskMappingDgCustomer.TaskMappingListSave(taskMapping);
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x0600011F RID: 287 RVA: 0x00002BD3 File Offset: 0x00000DD3
		// (set) Token: 0x06000120 RID: 288 RVA: 0x00002BDB File Offset: 0x00000DDB
		private taskMappingDataGrid taskMappingDgProdInfo { get; set; }

		// Token: 0x06000121 RID: 289 RVA: 0x00014E54 File Offset: 0x00013054
		private void ProdInfoListLoad(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat)
		{
			this.panelProdInfo.Controls.Clear();
			this.taskMappingDgProdInfo = new taskMappingDataGrid(SourceMappingType.ProductInfo, structureFormat);
			this.taskMappingDgProdInfo.TaskMappingListLoad(sourceMapping, taskMapping, this.loadDefaultDataFromSource);
			this.taskMappingDgProdInfo.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.taskMappingDgProdInfo.Dock = DockStyle.Fill;
			this.panelProdInfo.Controls.Add(this.taskMappingDgProdInfo);
		}

		// Token: 0x06000122 RID: 290 RVA: 0x00002BE4 File Offset: 0x00000DE4
		private void ProdInfoListSave(TaskMapping taskMapping)
		{
			if (this.taskMappingDgProdInfo != null)
			{
				this.taskMappingDgProdInfo.TaskMappingListSave(taskMapping);
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000123 RID: 291 RVA: 0x00002BFD File Offset: 0x00000DFD
		// (set) Token: 0x06000124 RID: 292 RVA: 0x00002C05 File Offset: 0x00000E05
		private taskMappingDataGrid taskMappingDgProdPictures { get; set; }

		// Token: 0x06000125 RID: 293 RVA: 0x00014EC0 File Offset: 0x000130C0
		private void ProdPicturesListLoad(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat)
		{
			this.panelProdPictures.Controls.Clear();
			this.taskMappingDgProdPictures = new taskMappingDataGrid(SourceMappingType.ProductPictures, structureFormat);
			this.ProductPicturesConvertOldMappingToNew(sourceMapping, taskMapping, structureFormat);
			this.taskMappingDgProdPictures.TaskMappingListLoad(sourceMapping, taskMapping, this.loadDefaultDataFromSource);
			this.taskMappingDgProdPictures.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.taskMappingDgProdPictures.Dock = DockStyle.Fill;
			this.panelProdPictures.Controls.Add(this.taskMappingDgProdPictures);
		}

		// Token: 0x06000126 RID: 294 RVA: 0x00002C0E File Offset: 0x00000E0E
		private void ProdPicturesListSave(TaskMapping taskMapping)
		{
			if (this.taskMappingDgProdPictures != null)
			{
				this.taskMappingDgProdPictures.TaskMappingListSave(taskMapping);
			}
		}

		// Token: 0x06000127 RID: 295 RVA: 0x00014F38 File Offset: 0x00013138
		private void ProductPicturesConvertOldMappingToNew(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat)
		{
			SourceProdPicturesMapping sourceProdPicturesMapping = sourceMapping.SourceProdPicturesMapping;
			if (((sourceProdPicturesMapping != null) ? sourceProdPicturesMapping.Items : null) == null || !sourceMapping.SourceProdPicturesMapping.Items.Any<SourceItemMapping>())
			{
				this.ProductPicturesConvertOldMappingItemToNew(sourceMapping, taskMapping, structureFormat, sourceMapping.ImageMap, sourceMapping.ImageMap, sourceMapping.ImageVal, sourceMapping.ImageSeoMap, sourceMapping.ImageSeoVal, sourceMapping.ImageVal, taskMapping.ImageAction, taskMapping.ImageSeoVal, taskMapping.ImageSeoAction, true);
				this.ProductPicturesConvertOldMappingItemToNew(sourceMapping, taskMapping, structureFormat, sourceMapping.Image1Map, sourceMapping.Image1Map, sourceMapping.Image1Val, sourceMapping.ImageSeo1Map, sourceMapping.ImageSeo1Val, sourceMapping.Image1Val, taskMapping.Image1Action, taskMapping.ImageSeo1Val, taskMapping.ImageSeo1Action, true);
				this.ProductPicturesConvertOldMappingItemToNew(sourceMapping, taskMapping, structureFormat, sourceMapping.Image2Map, sourceMapping.Image2Map, sourceMapping.Image2Val, sourceMapping.ImageSeo2Map, sourceMapping.ImageSeo2Val, sourceMapping.Image2Val, taskMapping.Image2Action, taskMapping.ImageSeo2Val, taskMapping.ImageSeo2Action, true);
				this.ProductPicturesConvertOldMappingItemToNew(sourceMapping, taskMapping, structureFormat, sourceMapping.Image3Map, sourceMapping.Image3Map, sourceMapping.Image3Val, sourceMapping.ImageSeo3Map, sourceMapping.ImageSeo3Val, sourceMapping.Image3Val, taskMapping.Image3Action, taskMapping.ImageSeo3Val, taskMapping.ImageSeo3Action, true);
				this.ProductPicturesConvertOldMappingItemToNew(sourceMapping, taskMapping, structureFormat, sourceMapping.Image4Map, sourceMapping.Image4Map, sourceMapping.Image4Val, sourceMapping.ImageSeo4Map, sourceMapping.ImageSeo4Val, sourceMapping.Image4Val, taskMapping.Image4Action, taskMapping.ImageSeo4Val, taskMapping.ImageSeo4Action, true);
				SourceProdPicturesMapping sourceProdPicturesMapping2 = sourceMapping.SourceProdPicturesMapping;
				if (((sourceProdPicturesMapping2 != null) ? sourceProdPicturesMapping2.Items : null) != null && sourceMapping.SourceProdPicturesMapping.Items.Any<SourceItemMapping>())
				{
					sourceMapping.ImageMap = null;
					sourceMapping.ImageVal = null;
					sourceMapping.ImageSeoMap = null;
					sourceMapping.ImageSeoVal = null;
					sourceMapping.Image1Map = null;
					sourceMapping.Image1Val = null;
					sourceMapping.ImageSeo1Map = null;
					sourceMapping.ImageSeo1Val = null;
					sourceMapping.Image2Map = null;
					sourceMapping.Image2Val = null;
					sourceMapping.ImageSeo2Map = null;
					sourceMapping.ImageSeo2Val = null;
					sourceMapping.Image3Map = null;
					sourceMapping.Image3Val = null;
					sourceMapping.ImageSeo3Map = null;
					sourceMapping.ImageSeo3Val = null;
					sourceMapping.Image4Map = null;
					sourceMapping.Image4Val = null;
					sourceMapping.ImageSeo4Map = null;
					sourceMapping.ImageSeo4Val = null;
					this.sourceMappingFunc.UpdateSourceMapping(sourceMapping);
					this.taskMappingFunc.UpdateTaskMapping(taskMapping);
				}
			}
		}

		// Token: 0x06000128 RID: 296 RVA: 0x00015184 File Offset: 0x00013384
		private void ProductPicturesConvertOldMappingItemToNew(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat, string picRepeatNodeMap, string picPathMap, string picPathVal, string picSeoMap, string picSeoVal, string picPathTaskMap, bool picPathTaskAction, string picSeoTaskMap, bool picSeoTaskAction, bool needrepeatnode)
		{
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			if (!string.IsNullOrEmpty(picPathMap) || !string.IsNullOrEmpty(picPathVal))
			{
				if ((list == null || !list.Any<SourceItemMapping>()) && structureFormat == StructureFormat.XML && needrepeatnode)
				{
					list.AddRange(SourceMappingHelper.GetRepeatNode());
				}
				list.AddRange(SourceMappingHelper.GetProdPictureNewItem());
				if (structureFormat == StructureFormat.XML)
				{
					if (needrepeatnode)
					{
						list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1).FieldMappingForXml = picRepeatNodeMap;
					}
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 601).FieldMappingForXml = picPathMap;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 601).FieldRule = picPathVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 602).FieldMappingForXml = picSeoMap;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 602).FieldRule = picSeoVal;
				}
				if (structureFormat == StructureFormat.Position)
				{
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 601).FieldMappingForIndex = GlobalClass.StringToInteger(picPathMap, -1);
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 601).FieldRule = picPathVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 602).FieldMappingForIndex = GlobalClass.StringToInteger(picSeoMap, -1);
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 602).FieldRule = picSeoVal;
				}
				sourceMapping.SourceProdPicturesMapping.Items.AddRange(list);
				foreach (SourceItemMapping sourceItemMapping in list)
				{
					TaskItemMapping item = new TaskItemMapping
					{
						Id = sourceItemMapping.Id,
						FieldName = sourceItemMapping.FieldName,
						MappingItemType = sourceItemMapping.MappingItemType,
						FieldRule = sourceItemMapping.FieldRule,
						FieldRule5 = 1,
						FieldRule3 = true,
						FieldImport = false
					};
					if (sourceItemMapping.MappingItemType == 601)
					{
						item = new TaskItemMapping
						{
							Id = sourceItemMapping.Id,
							FieldName = sourceItemMapping.FieldName,
							MappingItemType = sourceItemMapping.MappingItemType,
							FieldRule = sourceItemMapping.FieldRule,
							FieldRule5 = 1,
							FieldRule3 = true,
							FieldImport = picPathTaskAction
						};
						if (!string.IsNullOrEmpty(picPathTaskMap) && picPathTaskMap != picPathVal)
						{
							item = new TaskItemMapping
							{
								Id = sourceItemMapping.Id,
								FieldName = sourceItemMapping.FieldName,
								MappingItemType = sourceItemMapping.MappingItemType,
								FieldRule = picPathTaskMap,
								FieldRule5 = 1,
								FieldRule3 = true,
								FieldImport = picPathTaskAction
							};
						}
					}
					if (sourceItemMapping.MappingItemType == 602)
					{
						item = new TaskItemMapping
						{
							Id = sourceItemMapping.Id,
							FieldName = sourceItemMapping.FieldName,
							MappingItemType = sourceItemMapping.MappingItemType,
							FieldRule = sourceItemMapping.FieldRule,
							FieldRule5 = 1,
							FieldRule3 = true,
							FieldImport = picSeoTaskAction
						};
						if (!string.IsNullOrEmpty(picSeoTaskMap) && picSeoTaskMap != picSeoVal)
						{
							item = new TaskItemMapping
							{
								Id = sourceItemMapping.Id,
								FieldName = sourceItemMapping.FieldName,
								MappingItemType = sourceItemMapping.MappingItemType,
								FieldRule = picSeoTaskMap,
								FieldRule5 = 1,
								FieldRule3 = true,
								FieldImport = picSeoTaskAction
							};
						}
					}
					taskMapping.TaskProdPicturesMapping.Items.Add(item);
				}
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000129 RID: 297 RVA: 0x00002C27 File Offset: 0x00000E27
		// (set) Token: 0x0600012A RID: 298 RVA: 0x00002C2F File Offset: 0x00000E2F
		private taskMappingDataGrid taskMappingDgProductPrice { get; set; }

		// Token: 0x0600012B RID: 299 RVA: 0x000155C4 File Offset: 0x000137C4
		private void ProductPricesListLoad(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat)
		{
			this.panelPrices.Controls.Clear();
			this.taskMappingDgProductPrice = new taskMappingDataGrid(SourceMappingType.Product_Prices, structureFormat);
			this.taskMappingDgProductPrice.TaskMappingListLoad(sourceMapping, taskMapping, this.loadDefaultDataFromSource);
			this.taskMappingDgProductPrice.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.taskMappingDgProductPrice.Dock = DockStyle.Fill;
			this.panelPrices.Controls.Add(this.taskMappingDgProductPrice);
		}

		// Token: 0x0600012C RID: 300 RVA: 0x00002C38 File Offset: 0x00000E38
		private void ProductPricesListSave(TaskMapping taskMapping)
		{
			if (this.taskMappingDgProductPrice != null)
			{
				this.taskMappingDgProductPrice.TaskMappingListSave(taskMapping);
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x0600012D RID: 301 RVA: 0x00002C51 File Offset: 0x00000E51
		// (set) Token: 0x0600012E RID: 302 RVA: 0x00002C59 File Offset: 0x00000E59
		private taskMappingDataGrid taskMappingDgProdSpecAtt { get; set; }

		// Token: 0x0600012F RID: 303 RVA: 0x00015634 File Offset: 0x00013834
		private void ProductSpecAttributtesListLoad(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat)
		{
			this.panelProdSpecAttributtes.Controls.Clear();
			this.taskMappingDgProdSpecAtt = new taskMappingDataGrid(SourceMappingType.ProductSpecAttributes, structureFormat);
			this.ProductSpecAttributtesConvertOldMappingToNew(sourceMapping, taskMapping, structureFormat);
			this.taskMappingDgProdSpecAtt.TaskMappingListLoad(sourceMapping, taskMapping, this.loadDefaultDataFromSource);
			this.taskMappingDgProdSpecAtt.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.taskMappingDgProdSpecAtt.Dock = DockStyle.Fill;
			this.panelProdSpecAttributtes.Controls.Add(this.taskMappingDgProdSpecAtt);
		}

		// Token: 0x06000130 RID: 304 RVA: 0x00002C62 File Offset: 0x00000E62
		private void ProductSpecAttributtesListSave(TaskMapping taskMapping)
		{
			if (this.taskMappingDgProdSpecAtt != null)
			{
				this.taskMappingDgProdSpecAtt.TaskMappingListSave(taskMapping);
			}
		}

		// Token: 0x06000131 RID: 305 RVA: 0x000156AC File Offset: 0x000138AC
		private void ProductSpecAttributtesConvertOldMappingToNew(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat)
		{
			SourceProdSpecAttMapping sourceProdSpecAttMapping = sourceMapping.SourceProdSpecAttMapping;
			if (((sourceProdSpecAttMapping != null) ? sourceProdSpecAttMapping.SourceProdSpecAttMappingItems : null) == null || !sourceMapping.SourceProdSpecAttMapping.SourceProdSpecAttMappingItems.Any<SourceItemMapping>())
			{
				this.ProductSpecAttributtesConvertOldMappingItemToNew(sourceMapping, taskMapping, structureFormat, sourceMapping.SpecAttRepeatNodeMap, sourceMapping.SpecAttNameMap, sourceMapping.SpecAttNameVal, sourceMapping.SpecAttValueMap, sourceMapping.SpecAttValueVal, taskMapping.SpecAttNameVal, taskMapping.SpecAttNameAction, taskMapping.SpecAttValueVal, taskMapping.SpecAttValueAction, taskMapping.SpecAttShowInProduct, taskMapping.SpecAttAllowFiltering, taskMapping.SpecAttControlTypeId, true);
				this.ProductSpecAttributtesConvertOldMappingItemToNew(sourceMapping, taskMapping, structureFormat, null, sourceMapping.SpecAtt1NameMap, sourceMapping.SpecAtt1NameVal, sourceMapping.SpecAtt1ValueMap, sourceMapping.SpecAtt1ValueVal, taskMapping.SpecAtt1NameVal, taskMapping.SpecAtt1NameAction, taskMapping.SpecAtt1ValueVal, taskMapping.SpecAtt1ValueAction, taskMapping.SpecAtt1ShowInProduct, taskMapping.SpecAtt1AllowFiltering, taskMapping.SpecAtt1ControlTypeId, false);
				this.ProductSpecAttributtesConvertOldMappingItemToNew(sourceMapping, taskMapping, structureFormat, null, sourceMapping.SpecAtt2NameMap, sourceMapping.SpecAtt2NameVal, sourceMapping.SpecAtt2ValueMap, sourceMapping.SpecAtt2ValueVal, taskMapping.SpecAtt2NameVal, taskMapping.SpecAtt2NameAction, taskMapping.SpecAtt2ValueVal, taskMapping.SpecAtt2ValueAction, taskMapping.SpecAtt2ShowInProduct, taskMapping.SpecAtt2AllowFiltering, taskMapping.SpecAtt2ControlTypeId, false);
				this.ProductSpecAttributtesConvertOldMappingItemToNew(sourceMapping, taskMapping, structureFormat, null, sourceMapping.SpecAtt3NameMap, sourceMapping.SpecAtt3NameVal, sourceMapping.SpecAtt3ValueMap, sourceMapping.SpecAtt3ValueVal, taskMapping.SpecAtt3NameVal, taskMapping.SpecAtt3NameAction, taskMapping.SpecAtt3ValueVal, taskMapping.SpecAtt3ValueAction, taskMapping.SpecAtt3ShowInProduct, taskMapping.SpecAtt3AllowFiltering, taskMapping.SpecAtt3ControlTypeId, false);
				this.ProductSpecAttributtesConvertOldMappingItemToNew(sourceMapping, taskMapping, structureFormat, null, sourceMapping.SpecAtt4NameMap, sourceMapping.SpecAtt4NameVal, sourceMapping.SpecAtt4ValueMap, sourceMapping.SpecAtt4ValueVal, taskMapping.SpecAtt4NameVal, taskMapping.SpecAtt4NameAction, taskMapping.SpecAtt4ValueVal, taskMapping.SpecAtt4ValueAction, taskMapping.SpecAtt4ShowInProduct, taskMapping.SpecAtt4AllowFiltering, taskMapping.SpecAtt4ControlTypeId, false);
				SourceProdSpecAttMapping sourceProdSpecAttMapping2 = sourceMapping.SourceProdSpecAttMapping;
				if (((sourceProdSpecAttMapping2 != null) ? sourceProdSpecAttMapping2.SourceProdSpecAttMappingItems : null) != null && sourceMapping.SourceProdSpecAttMapping.SourceProdSpecAttMappingItems.Any<SourceItemMapping>())
				{
					sourceMapping.SpecAttNameMap = null;
					sourceMapping.SpecAttNameVal = null;
					sourceMapping.SpecAtt1NameMap = null;
					sourceMapping.SpecAtt1NameVal = null;
					sourceMapping.SpecAtt2NameMap = null;
					sourceMapping.SpecAtt2NameVal = null;
					sourceMapping.SpecAtt3NameMap = null;
					sourceMapping.SpecAtt3NameVal = null;
					sourceMapping.SpecAtt4NameMap = null;
					sourceMapping.SpecAtt4NameVal = null;
					this.sourceMappingFunc.UpdateSourceMapping(sourceMapping);
					this.taskMappingFunc.UpdateTaskMapping(taskMapping);
				}
			}
		}

		// Token: 0x06000132 RID: 306 RVA: 0x000158F8 File Offset: 0x00013AF8
		private void ProductSpecAttributtesConvertOldMappingItemToNew(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat, string attRepeatNodeMap, string attNameMap, string attNameVal, string attValueMap, string attValueVal, string attTaskNameMap, bool attTaskNameAction, string attTaskValueMap, bool attTaskValueAction, bool showOnProduct, bool allowFiltering, int? attTaskOptionType, bool needrepeatnode)
		{
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			if (!string.IsNullOrEmpty(attNameMap) || !string.IsNullOrEmpty(attNameVal))
			{
				if ((list == null || !list.Any<SourceItemMapping>()) && structureFormat == StructureFormat.XML && needrepeatnode)
				{
					list.AddRange(SourceMappingHelper.GetRepeatNode());
				}
				list.AddRange(SourceMappingHelper.GetProdSpecAttNewItem());
				if (structureFormat == StructureFormat.XML)
				{
					if (needrepeatnode)
					{
						list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1).FieldMappingForXml = attRepeatNodeMap;
					}
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 8).FieldMappingForXml = attNameMap;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 8).FieldRule = attNameVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 9).FieldMappingForXml = attValueMap;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 9).FieldRule = attValueVal;
				}
				if (structureFormat == StructureFormat.Position)
				{
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 8).FieldMappingForIndex = GlobalClass.StringToInteger(attNameMap, -1);
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 8).FieldRule = attNameVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 9).FieldMappingForIndex = GlobalClass.StringToInteger(attValueMap, -1);
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 9).FieldRule = attValueVal;
				}
				sourceMapping.SourceProdSpecAttMapping.SourceProdSpecAttMappingItems.AddRange(list);
				foreach (SourceItemMapping sourceItemMapping in list)
				{
					TaskItemMapping item = new TaskItemMapping
					{
						Id = sourceItemMapping.Id,
						FieldName = sourceItemMapping.FieldName,
						MappingItemType = sourceItemMapping.MappingItemType,
						FieldRule = sourceItemMapping.FieldRule,
						FieldRule5 = 0,
						FieldRule3 = true,
						FieldImport = true
					};
					if (sourceItemMapping.MappingItemType == 8)
					{
						item = new TaskItemMapping
						{
							Id = sourceItemMapping.Id,
							FieldName = sourceItemMapping.FieldName,
							MappingItemType = sourceItemMapping.MappingItemType,
							FieldRule = sourceItemMapping.FieldRule,
							FieldRule5 = attTaskOptionType.GetValueOrDefault(),
							FieldRule3 = true,
							FieldImport = attTaskNameAction
						};
						if (!string.IsNullOrEmpty(attTaskNameMap) && attTaskNameMap != attNameVal)
						{
							item = new TaskItemMapping
							{
								Id = sourceItemMapping.Id,
								FieldName = sourceItemMapping.FieldName,
								MappingItemType = sourceItemMapping.MappingItemType,
								FieldRule = attTaskNameMap,
								FieldRule5 = attTaskOptionType.GetValueOrDefault(),
								FieldRule3 = true,
								FieldImport = attTaskNameAction
							};
						}
					}
					if (sourceItemMapping.MappingItemType == 9)
					{
						item = new TaskItemMapping
						{
							Id = sourceItemMapping.Id,
							FieldName = sourceItemMapping.FieldName,
							MappingItemType = sourceItemMapping.MappingItemType,
							FieldRule = sourceItemMapping.FieldRule,
							FieldRule5 = attTaskOptionType.GetValueOrDefault(),
							FieldRule3 = true,
							FieldImport = attTaskValueAction
						};
						if (!string.IsNullOrEmpty(attTaskValueMap) && attTaskValueMap != attValueVal)
						{
							item = new TaskItemMapping
							{
								Id = sourceItemMapping.Id,
								FieldName = sourceItemMapping.FieldName,
								MappingItemType = sourceItemMapping.MappingItemType,
								FieldRule = attTaskValueMap,
								FieldRule5 = attTaskOptionType.GetValueOrDefault(),
								FieldRule3 = true,
								FieldImport = attTaskValueAction
							};
						}
					}
					taskMapping.TaskProductSpecAttributesMapping.TaskProductSpecAttributesMappingItems.Add(item);
				}
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000133 RID: 307 RVA: 0x00002C7B File Offset: 0x00000E7B
		// (set) Token: 0x06000134 RID: 308 RVA: 0x00002C83 File Offset: 0x00000E83
		private taskMappingDataGrid taskMappingDgProdAtt { get; set; }

		// Token: 0x06000135 RID: 309 RVA: 0x00015D48 File Offset: 0x00013F48
		private void ProductAttributtesListLoad(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat)
		{
			this.panelProdAttributtes.Controls.Clear();
			this.taskMappingDgProdAtt = new taskMappingDataGrid(SourceMappingType.ProductAttributes, structureFormat);
			this.ConvertOldMappingToNew(sourceMapping, taskMapping, structureFormat);
			this.taskMappingDgProdAtt.TaskMappingListLoad(sourceMapping, taskMapping, this.loadDefaultDataFromSource);
			this.taskMappingDgProdAtt.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.taskMappingDgProdAtt.Dock = DockStyle.Fill;
			this.panelProdAttributtes.Controls.Add(this.taskMappingDgProdAtt);
		}

		// Token: 0x06000136 RID: 310 RVA: 0x00002C8C File Offset: 0x00000E8C
		private void ProductAttributtesListSave(TaskMapping taskMapping)
		{
			if (this.taskMappingDgProdAtt != null)
			{
				this.taskMappingDgProdAtt.TaskMappingListSave(taskMapping);
			}
		}

		// Token: 0x06000137 RID: 311 RVA: 0x00015DC0 File Offset: 0x00013FC0
		private void ConvertOldMappingToNew(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat)
		{
			if (sourceMapping.SourceProdAttMapping?.SourceProdAttMappingItems == null || !sourceMapping.SourceProdAttMapping.SourceProdAttMappingItems.Any())
			{
				ConvertOldMappingItemToNew(sourceMapping, taskMapping, structureFormat, sourceMapping.AttRepeatNodeMap, sourceMapping.AttSkuMap, sourceMapping.AttSkuVal, sourceMapping.AttNameMap, sourceMapping.AttNameVal, sourceMapping.AttValueMap, sourceMapping.AttValueVal, sourceMapping.AttStockMap, sourceMapping.AttStockVal, sourceMapping.AttPriceMap, sourceMapping.AttPriceVal, taskMapping.AttNameVal, taskMapping.AttNameAction, taskMapping.AttValueVal, taskMapping.AttValueAction, taskMapping.AttStockVal, taskMapping.AttStockAction, taskMapping.AttControlTypeId, needrepeatnode: true);
				ConvertOldMappingItemToNew(sourceMapping, taskMapping, structureFormat, null, sourceMapping.Att1SkuMap, sourceMapping.Att1SkuVal, sourceMapping.Att1NameMap, sourceMapping.Att1NameVal, sourceMapping.Att1ValueMap, sourceMapping.Att1ValueVal, sourceMapping.Att1StockMap, sourceMapping.Att1StockVal, sourceMapping.Att1PriceMap, sourceMapping.Att1PriceVal, taskMapping.Att1NameVal, taskMapping.Att1NameAction, taskMapping.Att1ValueVal, taskMapping.Att1ValueAction, taskMapping.Att1StockVal, taskMapping.Att1StockAction, taskMapping.Att1ControlTypeId, needrepeatnode: false);
				ConvertOldMappingItemToNew(sourceMapping, taskMapping, structureFormat, null, sourceMapping.Att2SkuMap, sourceMapping.Att2SkuVal, sourceMapping.Att2NameMap, sourceMapping.Att2NameVal, sourceMapping.Att2ValueMap, sourceMapping.Att2ValueVal, sourceMapping.Att2StockMap, sourceMapping.Att2StockVal, sourceMapping.Att2PriceMap, sourceMapping.Att2PriceVal, taskMapping.Att2NameVal, taskMapping.Att2NameAction, taskMapping.Att2ValueVal, taskMapping.Att2ValueAction, taskMapping.Att2StockVal, taskMapping.Att2StockAction, taskMapping.Att2ControlTypeId, needrepeatnode: false);
				if (sourceMapping.SourceProdAttMapping?.SourceProdAttMappingItems != null && sourceMapping.SourceProdAttMapping.SourceProdAttMappingItems.Any())
				{
					sourceMapping.AttNameMap = null;
					sourceMapping.AttNameVal = null;
					sourceMapping.Att1NameMap = null;
					sourceMapping.Att1NameVal = null;
					sourceMapping.Att2NameMap = null;
					sourceMapping.Att2NameVal = null;
					sourceMappingFunc.UpdateSourceMapping(sourceMapping);
					taskMappingFunc.UpdateTaskMapping(taskMapping);
				}
				return;
			}
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			var list2 = (from n in sourceMapping.SourceProdAttMapping?.SourceProdAttMappingItems
						 group n by n.Id into g
						 select new
						 {
							 GroupedList = g
						 }).ToList();
			foreach (var item in list2)
			{
				list.AddRange(SourceMappingHelper.ProdAttNewItemNamingOrder(item.GroupedList.ToList()));
				List<SourceItemMapping> prodAttNewItem = SourceMappingHelper.GetProdAttNewItem(update: true, item.GroupedList.ToList());
				list.AddRange(SourceMappingHelper.ProdAttNewItemNamingOrder(prodAttNewItem));
			}
			sourceMapping.SourceProdAttMapping.SourceProdAttMappingItems = list.OrderBy((SourceItemMapping o) => o.Id).ThenBy((SourceItemMapping t) => t.Order).ToList();
		}

		// Token: 0x06000138 RID: 312 RVA: 0x000160F8 File Offset: 0x000142F8
		private void ConvertOldMappingItemToNew(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat, string attRepeatNodeMap, string attSkuMap, string attSkuVal, string attNameMap, string attNameVal, string attValueMap, string attValueVal, string attStockMap, string attStockVal, string attPriceMap, string attPriceVal, string attTaskNameMap, bool attTaskNameAction, string attTaskValueMap, bool attTaskValueAction, int? attTaskStockMap, bool attTaskStockAction, int? attTaskOptionType, bool needrepeatnode)
		{
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			if (!string.IsNullOrEmpty(attNameMap) || !string.IsNullOrEmpty(attNameVal))
			{
				if ((list == null || !list.Any<SourceItemMapping>()) && structureFormat == StructureFormat.XML && needrepeatnode)
				{
					list.AddRange(SourceMappingHelper.GetRepeatNode());
				}
				list.AddRange(SourceMappingHelper.GetProdAttNewItem(false, null));
				if (structureFormat == StructureFormat.XML)
				{
					if (needrepeatnode)
					{
						list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1).FieldMappingForXml = attRepeatNodeMap;
					}
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 4).FieldMappingForXml = attNameMap;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 4).FieldRule = attNameVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 5).FieldMappingForXml = attValueMap;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 5).FieldRule = attValueVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 3).FieldMappingForXml = attSkuMap;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 3).FieldRule = attSkuVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 6).FieldMappingForXml = attStockMap;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 6).FieldRule = attStockVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 112).FieldMappingForXml = attStockMap;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 112).FieldRule = attStockVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 7).FieldMappingForXml = attPriceMap;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 7).FieldRule = attPriceVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 111).FieldMappingForXml = attPriceMap;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 111).FieldRule = attPriceVal;
				}
				if (structureFormat == StructureFormat.Position)
				{
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 4).FieldMappingForIndex = GlobalClass.StringToInteger(attNameMap, -1);
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 4).FieldRule = attNameVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 5).FieldMappingForIndex = GlobalClass.StringToInteger(attValueMap, -1);
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 5).FieldRule = attValueVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 3).FieldMappingForIndex = GlobalClass.StringToInteger(attSkuMap, -1);
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 3).FieldRule = attSkuVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 6).FieldMappingForIndex = GlobalClass.StringToInteger(attStockMap, -1);
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 6).FieldRule = attStockVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 112).FieldMappingForIndex = GlobalClass.StringToInteger(attStockMap, -1);
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 112).FieldRule = attStockVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 7).FieldMappingForIndex = GlobalClass.StringToInteger(attPriceMap, -1);
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 7).FieldRule = attPriceVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 111).FieldMappingForIndex = GlobalClass.StringToInteger(attPriceMap, -1);
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 111).FieldRule = attPriceVal;
				}
				sourceMapping.SourceProdAttMapping.SourceProdAttMappingItems.AddRange(list);
				foreach (SourceItemMapping sourceItemMapping in list)
				{
					TaskItemMapping item = new TaskItemMapping
					{
						Id = sourceItemMapping.Id,
						FieldName = sourceItemMapping.FieldName,
						MappingItemType = sourceItemMapping.MappingItemType,
						FieldRule = sourceItemMapping.FieldRule,
						FieldRule5 = 1,
						FieldRule3 = true,
						FieldImport = false
					};
					if (sourceItemMapping.MappingItemType == 4)
					{
						item = new TaskItemMapping
						{
							Id = sourceItemMapping.Id,
							FieldName = sourceItemMapping.FieldName,
							MappingItemType = sourceItemMapping.MappingItemType,
							FieldRule = sourceItemMapping.FieldRule,
							FieldRule5 = (attTaskOptionType ?? 1),
							FieldRule3 = true,
							FieldImport = attTaskNameAction
						};
						if (!string.IsNullOrEmpty(attTaskNameMap) && attTaskNameMap != attNameVal)
						{
							item = new TaskItemMapping
							{
								Id = sourceItemMapping.Id,
								FieldName = sourceItemMapping.FieldName,
								MappingItemType = sourceItemMapping.MappingItemType,
								FieldRule = attTaskNameMap,
								FieldRule5 = (attTaskOptionType ?? 1),
								FieldRule3 = true,
								FieldImport = attTaskNameAction
							};
						}
					}
					if (sourceItemMapping.MappingItemType == 108)
					{
						item = new TaskItemMapping
						{
							Id = sourceItemMapping.Id,
							FieldName = sourceItemMapping.FieldName,
							MappingItemType = sourceItemMapping.MappingItemType,
							FieldRule = sourceItemMapping.FieldRule,
							FieldRule5 = (attTaskOptionType ?? 1),
							FieldRule3 = true,
							FieldImport = attTaskValueAction
						};
						if (!string.IsNullOrEmpty(attTaskValueMap) && attTaskValueMap != attValueVal)
						{
							item = new TaskItemMapping
							{
								Id = sourceItemMapping.Id,
								FieldName = sourceItemMapping.FieldName,
								MappingItemType = sourceItemMapping.MappingItemType,
								FieldRule = attTaskValueMap,
								FieldRule5 = (attTaskOptionType ?? 1),
								FieldRule3 = true,
								FieldImport = attTaskValueAction
							};
						}
					}
					if (sourceItemMapping.MappingItemType == 6)
					{
						item = new TaskItemMapping
						{
							Id = sourceItemMapping.Id,
							FieldName = sourceItemMapping.FieldName,
							MappingItemType = sourceItemMapping.MappingItemType,
							FieldRule = sourceItemMapping.FieldRule,
							FieldRule5 = (attTaskOptionType ?? 1),
							FieldRule3 = true,
							FieldImport = attTaskStockAction
						};
						bool flag;
						if (attTaskStockMap != null)
						{
							int? num = attTaskStockMap;
							int num2 = GlobalClass.StringToInteger(attStockVal, 0);
							flag = !(num.GetValueOrDefault() == num2 & num != null);
						}
						else
						{
							flag = false;
						}
						if (flag)
						{
							item = new TaskItemMapping
							{
								Id = sourceItemMapping.Id,
								FieldName = sourceItemMapping.FieldName,
								MappingItemType = sourceItemMapping.MappingItemType,
								FieldRule = attTaskStockMap.ToString(),
								FieldRule5 = (attTaskOptionType ?? 1),
								FieldRule3 = true,
								FieldImport = attTaskStockAction
							};
						}
					}
					taskMapping.TaskProductAttributesMapping.TaskProductAttributesMappingItems.Add(item);
				}
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000139 RID: 313 RVA: 0x00002CA5 File Offset: 0x00000EA5
		// (set) Token: 0x0600013A RID: 314 RVA: 0x00002CAD File Offset: 0x00000EAD
		private taskMappingDataGrid taskMappingDgTierPricing { get; set; }

		// Token: 0x0600013B RID: 315 RVA: 0x00016A24 File Offset: 0x00014C24
		private void TierPricingListLoad(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat)
		{
			this.panelTierPricing.Controls.Clear();
			this.taskMappingDgTierPricing = new taskMappingDataGrid(SourceMappingType.TierPricing, structureFormat);
			this.taskMappingDgTierPricing.TaskMappingListLoad(sourceMapping, taskMapping, this.loadDefaultDataFromSource);
			this.taskMappingDgTierPricing.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.taskMappingDgTierPricing.Dock = DockStyle.Fill;
			this.panelTierPricing.Controls.Add(this.taskMappingDgTierPricing);
		}

		// Token: 0x0600013C RID: 316 RVA: 0x00002CB6 File Offset: 0x00000EB6
		private void TierPricingListSave(TaskMapping taskMapping)
		{
			if (this.taskMappingDgTierPricing != null)
			{
				this.taskMappingDgTierPricing.TaskMappingListSave(taskMapping);
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600013D RID: 317 RVA: 0x00002CCF File Offset: 0x00000ECF
		// (set) Token: 0x0600013E RID: 318 RVA: 0x00002CD7 File Offset: 0x00000ED7
		private taskMappingDataGrid taskMappingDgWishList { get; set; }

		// Token: 0x0600013F RID: 319 RVA: 0x00016A90 File Offset: 0x00014C90
		private void WishListLoad(SourceMapping sourceMapping, TaskMapping taskMapping, StructureFormat structureFormat)
		{
			this.panelWishList.Controls.Clear();
			this.taskMappingDgWishList = new taskMappingDataGrid(SourceMappingType.WishList, structureFormat);
			this.taskMappingDgWishList.TaskMappingListLoad(sourceMapping, taskMapping, this.loadDefaultDataFromSource);
			this.taskMappingDgWishList.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.taskMappingDgWishList.Dock = DockStyle.Fill;
			this.panelWishList.Controls.Add(this.taskMappingDgWishList);
			this.chDeleteWishListOnUpdate.Checked = taskMapping.TaskWishListMapping.DeleteBeforeImport;
		}

		// Token: 0x06000140 RID: 320 RVA: 0x00002CE0 File Offset: 0x00000EE0
		private void WishListSave(TaskMapping taskMapping)
		{
			if (this.taskMappingDgWishList != null)
			{
				this.taskMappingDgWishList.TaskMappingListSave(taskMapping);
			}
			taskMapping.TaskWishListMapping.DeleteBeforeImport = this.chDeleteWishListOnUpdate.Checked;
		}

		// Token: 0x06000141 RID: 321 RVA: 0x00016B14 File Offset: 0x00014D14
		public taskNewEdit(int i, frmTasksList owner)
		{
			
			this.sourceMappingFunc = new SourceMappingFunc();
			this.taskMappingFunc = new TaskMappingFunc();
			this.taskManager = new TaskManager();
			this.entityType = EntityType.Products;
			this.vendorSourceId = 0;
			this.storeId = 0;
			this.storeConnString = "";
			this.nopConnString = "";
			this.taskId = 0;
			this.loadDefaultDataFromSource = true;
			this.formLoaded = false;
			this.loadVendorSourceMap = false;
			this.connectionErrorShow = false;
			this.originalExStyle = -1;
			this.enableFormLevelDoubleBuffering = true;
			this.components = null;
		
			this._owner = owner;
			this.InitializeComponent();
			this.taskId = i;
			this.AutoValidate = AutoValidate.Disable;
			this.tabTask.Visible = false;
			this.tableLayoutPanel4.Visible = false;
			this.chLimitedToSoresEntities.MultiColumn = true;
		}

		// Token: 0x06000142 RID: 322 RVA: 0x00016BF0 File Offset: 0x00014DF0
		private async void taskNewEdit_Load(object sender, EventArgs e)
		{
			this.AutoSize = true;
			base.AutoSizeMode = AutoSizeMode.GrowAndShrink;
			Cursor.Current = Cursors.WaitCursor;
			if (this.taskId > 0)
			{
				this.loadDefaultDataFromSource = false;
			}
			this.InitialData();
			this.formLoaded = true;
			this.LoadData();
			this.btnCopy.Enabled = (this.taskId > 0);
			
		}

		// Token: 0x06000143 RID: 323 RVA: 0x00016C3C File Offset: 0x00014E3C
		private async void InitialData()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			this.nopConnString = dataSettingsManager.LoadSettings(null).DataConnectionString;
			
			if (ImportManager.multipleStore)
			{
				helper.FillDropDownList("select Id, Name From EShop", this.ddStoreWeb, this.nopConnString);
			}
			else
			{
				helper.FillDropDownList("select Top 1 Id, Name From EShop", this.ddStoreWeb, this.nopConnString);
			}
			helper.FillDropDownList("select VendorSource.Id as Id, (Vendor.Name + ' ' + Vendor.Description) as Name From Vendor INNER JOIN VendorSource ON Vendor.Id = VendorSource.VendorId", this.ddVendorSourceMap, this.nopConnString);
			helper.FillDropDownList("select Id, Name From TaskStatus", this.ddTaskStatus, this.nopConnString);
			helper.FillDropDownList("select 0, '---' union select Id, TaskName From Task", this.ddRelatedTask, this.nopConnString);
			this.LoadManageInventoryMethod();
			this.LoadbackOrderModes();
			this.LoadLowStockActivities();
			this.ddStoreWeb.SelectedIndex = -1;
			this.ddVendorSourceMap.SelectedIndex = -1;
			this.ddTaskStatus.SelectedIndex = -1;
			this.ddRelatedTask.SelectedIndex = -1;
		}

		// Token: 0x06000144 RID: 324 RVA: 0x00016C78 File Offset: 0x00014E78
		private void LoadData()
		{
			if (this.taskId > 0)
			{
				this.LoadTaskDataToForm();
				this.btnTestMapping.Show();
				this.btnRunTask.Show();
			}
			else
			{
				this.LoadTabs();
				this.btnTestMapping.Hide();
				this.btnRunTask.Hide();
			}
			if (this.entityType == EntityType.WishList)
			{
				this.ddTaskAction.Enabled = false;
				this.ddTaskAction.SelectedIndex = 2;
			}
			else
			{
				this.ddTaskAction.Enabled = true;
			}
		}

		// Token: 0x06000145 RID: 325 RVA: 0x00016CFC File Offset: 0x00014EFC
		private void LoadTabs()
		{
			this.tabTask.TabPages.Clear();
			if (!this.tabTask.Contains(this.tabTaskData))
			{
				this.tabTask.TabPages.Add(this.tabTaskData);
			}
			if (this.taskId > 0)
			{
				if (this.entityType == EntityType.Products)
				{
					if (!this.tabTask.Contains(this.tabProductInfo))
					{
						this.tabTask.TabPages.Add(this.tabProductInfo);
					}
					if (!this.tabTask.Contains(this.tabProductSettings))
					{
						this.tabTask.TabPages.Add(this.tabProductSettings);
					}
					if (!this.tabTask.Contains(this.tabSeo))
					{
						this.tabTask.TabPages.Add(this.tabSeo);
					}
					if (!this.tabTask.Contains(this.tabCategories))
					{
						this.tabTask.TabPages.Add(this.tabCategories);
					}
					if (!this.tabTask.Contains(this.tabAccessControl))
					{
						this.tabTask.TabPages.Add(this.tabAccessControl);
					}
					if (!this.tabTask.Contains(this.tabImages))
					{
						this.tabTask.TabPages.Add(this.tabImages);
					}
					if (!this.tabTask.Contains(this.tabProdAttributes))
					{
						this.tabTask.TabPages.Add(this.tabProdAttributes);
					}
					if (!this.tabTask.Contains(this.tabSpecAttributes))
					{
						this.tabTask.TabPages.Add(this.tabSpecAttributes);
					}
					if (!this.tabTask.Contains(this.tabPrices))
					{
						this.tabTask.TabPages.Add(this.tabPrices);
					}
					if (!this.tabTask.Contains(this.tabTierPricing))
					{
						this.tabTask.TabPages.Add(this.tabTierPricing);
					}
					if (!this.tabTask.Contains(this.tabProdInfo))
					{
						this.tabTask.TabPages.Add(this.tabProdInfo);
					}
					if (!this.tabTask.Contains(this.tabCustomSql))
					{
						this.tabTask.TabPages.Add(this.tabCustomSql);
					}
					if (!this.tabTask.Contains(this.tabTranslations))
					{
						this.tabTask.TabPages.Add(this.tabTranslations);
					}
					if (!this.tabTask.Contains(this.tabFilters))
					{
						this.tabTask.TabPages.Add(this.tabFilters);
					}
					if (!this.tabTask.Contains(this.tabRelatedTasks))
					{
						this.tabTask.TabPages.Add(this.tabRelatedTasks);
					}
					if (!this.tabTask.Contains(this.tabLogSettings))
					{
						this.tabTask.TabPages.Add(this.tabLogSettings);
					}
				}
				if (this.entityType == EntityType.Users)
				{
					if (!this.tabTask.Contains(this.tabCustomer))
					{
						this.tabTask.TabPages.Add(this.tabCustomer);
					}
					if (!this.tabTask.Contains(this.tabCustomerShippingAddress))
					{
						this.tabTask.TabPages.Add(this.tabCustomerShippingAddress);
					}
					if (!this.tabTask.Contains(this.tabCustomerBillingAddress))
					{
						this.tabTask.TabPages.Add(this.tabCustomerBillingAddress);
					}
					if (!this.tabTask.Contains(this.tabCustomerRole))
					{
						this.tabTask.TabPages.Add(this.tabCustomerRole);
					}
					if (!this.tabTask.Contains(this.tabCustomerOther))
					{
						this.tabTask.TabPages.Add(this.tabCustomerOther);
					}
					if (!this.tabTask.Contains(this.tabCustomerCustomAtt))
					{
						this.tabTask.TabPages.Add(this.tabCustomerCustomAtt);
					}
					if (!this.tabTask.Contains(this.tabAddressCustomAtt))
					{
						this.tabTask.TabPages.Add(this.tabAddressCustomAtt);
					}
					if (!this.tabTask.Contains(this.tabCustomSql))
					{
						this.tabTask.TabPages.Add(this.tabCustomSql);
					}
					if (!this.tabTask.Contains(this.tabLogSettings))
					{
						this.tabTask.TabPages.Add(this.tabLogSettings);
					}
				}
				if (this.entityType == EntityType.WishList && !this.tabTask.Contains(this.tabWishList))
				{
					this.tabTask.TabPages.Add(this.tabWishList);
				}
				this.btnTestMapping.Show();
				this.btnRunTask.Show();
			}
			else
			{
				this.btnTestMapping.Hide();
				this.btnRunTask.Hide();
			}
		}

		// Token: 0x06000146 RID: 326 RVA: 0x000024A2 File Offset: 0x000006A2
		private void btnClose_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		// Token: 0x06000147 RID: 327 RVA: 0x00017220 File Offset: 0x00015420
		private void ddStoreWeb_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.LoadStoreData("select 1 as Id, 'Products' as Name UNION select 2 as Id, 'Categories' as Name UNION select 3 as Id, 'Manufacturers' as Name UNION select 4 as Id, 'TierPricing' as Name", null, this.chLimitedToSoresEntities);
			this.LoadStoreData("select Id, Name From Store", null, this.chLimitedStore);
			this.LoadStoreData("select 0 as Id, 'NoDeliveryDate' as Name UNION select Id, Name From DeliveryDate", this.ddDeliveryDate, null);
			this.LoadStoreData("select 0 as Id, 'Default Type' as Name UNION select 9989 as Id, 'Not Change If GroupBy Is null' as Name UNION select Id, Name From ProductTemplate", this.ddProductType, null);
			this.LoadStoreData("select 0 as Id, 'NoWarehouse' as Name UNION select Id, Name From Warehouse", this.ddWarehouse, null);
			this.LoadStoreData("select 0 as Id, 'NoTranslation' as Name UNION select Id, Name From Language", this.ddTranslateLanguagesList, null);
			this.LoadStoreData("select 0 as Id, 'NoAction' as Name UNION select 1 as Id, 'Run On Task Start' as Name UNION select 2 as Id, 'Run On Task Finish' as Name", this.cbRunSqlOn, null);
		}

		// Token: 0x06000148 RID: 328 RVA: 0x000172AC File Offset: 0x000154AC
		private TaskMapping GetTaskDataFromForm()
		{
			TaskMapping taskMapping = new TaskMapping();
			taskMapping.DeleteOldCatMapping = this.chDeleteOldCatMapping.Checked;
			taskMapping.DeleteOldPictures = this.chDeleteOldPictures.Checked;
			taskMapping.DeleteOldAttributtes = this.chDeleteOldAttributtes.Checked;
			taskMapping.DeleteOldSpecAttributtes = this.chDeleteOldSpecAttributes.Checked;
			taskMapping.DeleteOldCustomerAddresses = this.chCustomerAddress_DeleteOld.Checked;
			taskMapping.DeleteOldCustomerRoles = this.chCustomerRole_DeleteOld.Checked;
			taskMapping.DeleteExistingTierPricingOnUpdate = this.chDeleteExistingTierPricingOnUpdate.Checked;
			taskMapping.ForceShippingAddressOnUpdate = this.chForceShippingAddressOnUpdate.Checked;
			taskMapping.ForceBillingAddressOnUpdate = this.chForceBillingAddressOnUpdate.Checked;
			taskMapping.AddSpecAttributtesToFullDescAsTable = this.chAddSpecAttributesAsTable.Checked;
			taskMapping.ResizePicture = this.chResizePicture.Checked;
			taskMapping.ResizePictureHeight = GlobalClass.StringToInteger(this.nmPictureResizeHeight.Value, 0);
			taskMapping.ResizePictureWidth = GlobalClass.StringToInteger(this.nmPictureResizeWidth.Value, 0);
			taskMapping.ResizePictureQuality = GlobalClass.StringToInteger(this.nmPictureResizeQuality.Value, 0);
			taskMapping.ProductsInTheLastSubCategory = this.chInsertProductsToLastSubCategory.Checked;
			taskMapping.InsertCategoryPictureOnInsert = this.chInsertCategoryPictureOnInsert.Checked;
			taskMapping.DisableNestedCategories = this.chDisableNestedCategories.Checked;
			taskMapping.TaskId = new int?(this.taskId);
			taskMapping.TaskName = this.tbTaskName.Text.Trim();
			taskMapping.EShopId = new long?((long)GlobalClass.StringToInteger(this.ddStoreWeb.SelectedValue, 0));
			taskMapping.VendorSourceId = GlobalClass.StringToInteger(this.ddVendorSourceMap.SelectedValue, 0);
			taskMapping.TaskStatusId = GlobalClass.StringToInteger(this.ddTaskStatus.SelectedValue, 0);
			taskMapping.TaskAction = GlobalClass.StringToInteger(this.ddTaskAction.SelectedIndex, 0);
			taskMapping.DisableShop = this.chDisableShop.Checked;
			taskMapping.UnpublishProducts = this.chUnpublish.Checked;
			taskMapping.DeleteSourceFile = this.chDeleteFileAfterImport.Checked;
			taskMapping.SavePicturesInFile = this.chPicturesInFiles.Checked;
			taskMapping.UseTitleCase = this.chTitleCase.Checked;
			taskMapping.SavePicturesPath = this.tbPicturesPathSave.Text.Trim();
			taskMapping.SaveSourceCopyPath = this.tbSourceCopyPath.Text.Trim();
			taskMapping.LimitedToStores = "";
			taskMapping.LimitedToStoresEntities = "";
			for (int i = 0; i < this.chLimitedStore.Items.Count; i++)
			{
				if (this.chLimitedStore.GetItemCheckState(i) == CheckState.Checked)
				{
					TaskMapping taskMapping2 = taskMapping;
					taskMapping2.LimitedToStores = taskMapping2.LimitedToStores + ((DataRowView)this.chLimitedStore.Items[i]).Row[0].ToString() + ";";
				}
			}
			for (int j = 0; j < this.chLimitedToSoresEntities.Items.Count; j++)
			{
				if (this.chLimitedToSoresEntities.GetItemCheckState(j) == CheckState.Checked)
				{
					TaskMapping taskMapping3 = taskMapping;
					taskMapping3.LimitedToStoresEntities = taskMapping3.LimitedToStoresEntities + ((DataRowView)this.chLimitedToSoresEntities.Items[j]).Row[0].ToString() + ";";
				}
			}
			taskMapping.ScheduleType = new int?(GlobalClass.StringToInteger(this.ddScheduleType.SelectedIndex, 0));
			taskMapping.ScheduleInterval = new long?((long)GlobalClass.StringToInteger(this.nmInterval.Value, 0));
			taskMapping.ScheduleHour = new int?(GlobalClass.StringToInteger(this.nmHour.Value, 0));
			taskMapping.ScheduleMinute = new int?(GlobalClass.StringToInteger(this.nmMinute.Value, 0));
			taskMapping.EntityType = this.entityType;
			if (this.entityType == EntityType.Products)
			{
				taskMapping.RelatedSourceAddress = this.tbRelatedSourceAddress.Text.Trim();
				taskMapping.RelatedTaskId = new int?(GlobalClass.StringToInteger(this.ddRelatedTask.SelectedValue, 0));
				taskMapping.ProdNameVal = this.tbProductName.Text.Trim();
				taskMapping.ProdNameAction = this.chProdNameImport.Checked;
				taskMapping.ProdShortDescVal = this.tbProdShortDesc.Text.Trim();
				taskMapping.ProdShortDescAction = this.chProdShortDescImport.Checked;
				taskMapping.ProdFullDescVal = this.tbProdFullDesc.Text.Trim();
				taskMapping.ProdFullDescAction = this.chProdFullDescImport.Checked;
				taskMapping.ProdManPartNumVal = this.tbManufacturerPnum.Text.Trim();
				taskMapping.ProdManPartNumAction = this.chManufacturerImport.Checked;
				taskMapping.ProdStockVal = this.nmStockQuantity.Text;
				taskMapping.ProdStockAction = this.chProdStockImport.Checked;
				taskMapping.ManufactVal = this.tbManufacturer.Text.Trim();
				taskMapping.ManufactAction = this.chManufacturerImport.Checked;
				taskMapping.VendorVal = this.tbVendor.Text.Trim();
				taskMapping.VendorAction = this.chVendorImport.Checked;
				if (this.chDayDeliveryImport.Checked)
				{
					taskMapping.DeliveryDayId = new int?(GlobalClass.StringToInteger(this.ddDeliveryDate.SelectedValue, 0));
				}
				if (this.chProductTypeImport.Checked)
				{
					taskMapping.ProductTemplateId = new int?(GlobalClass.StringToInteger(this.ddProductType.SelectedValue, 0));
				}
				if (this.chWarehouseImport.Checked)
				{
					taskMapping.WarehouseId = new int?(GlobalClass.StringToInteger(this.ddWarehouse.SelectedValue, 0));
				}
				if (this.chInventoryMethodImport.Checked)
				{
					taskMapping.ManageInventoryMethodId = new int?(GlobalClass.StringToInteger(this.ddInventoryMethod.SelectedValue, 0));
				}
				if (this.chBackorderModeImport.Checked)
				{
					taskMapping.BackorderModeId = new int?(GlobalClass.StringToInteger(this.ddlBackorderMode.SelectedValue, 0));
				}
				if (this.chLowStockActivityImport.Checked)
				{
					taskMapping.LowStockActivityId = new int?(GlobalClass.StringToInteger(this.ddLowStockActivity.SelectedValue, 0));
				}
				if (this.chProductPublishImport.Checked)
				{
					taskMapping.ProductPublish = new bool?(this.chProductPublish.Checked);
				}
				if (this.chDayDeliveryImport.Checked)
				{
					taskMapping.ProdSettingsDeliveryDateAction = this.chDayDeliveryImport.Checked;
				}
				if (this.chProductReviewEnableImport.Checked)
				{
					taskMapping.AllowCustomerReviews = new bool?(this.chProductReviewEnable.Checked);
				}
				if (this.chProductDisplayAvailabilityImport.Checked)
				{
					taskMapping.DisplayAvailability = new bool?(this.chProductDisplayAvailability.Checked);
				}
				if (this.chProductDisplayStockQuantityImport.Checked)
				{
					taskMapping.DisplayStockQuantity = new bool?(this.chProductDisplayStockQuantity.Checked);
				}
				taskMapping.SeoMetaKeyVal = this.tbMetaKeywords.Text.Trim();
				taskMapping.SeoMetaKeyAction = this.chSeoMetaKeyImport.Checked;
				taskMapping.SeoMetaDescVal = this.tbMetaDescription.Text.Trim();
				taskMapping.SeoMetaDescAction = this.chSeoMetaDescImport.Checked;
				taskMapping.SeoMetaTitleVal = this.tbMetaTitle.Text.Trim();
				taskMapping.SeoMetaTitleAction = this.chSeoMetaTitleImport.Checked;
				taskMapping.SeoPageVal = this.tbSearchEnginePage.Text.Trim();
				taskMapping.SeoPageAction = this.chSearchPageImport.Checked;
				taskMapping.CatVal = this.tbCategory.Text.Trim();
				taskMapping.CatAction = this.chCategoryImport.Checked;
				taskMapping.Cat1Val = this.tbCategory1.Text.Trim();
				taskMapping.Cat1Action = this.chCategory1Import.Checked;
				taskMapping.Cat2Val = this.tbCategory2.Text.Trim();
				taskMapping.Cat2Action = this.chCategory2Import.Checked;
				taskMapping.Cat3Val = this.tbCategory3.Text.Trim();
				taskMapping.Cat3Action = this.chCategory3Import.Checked;
				taskMapping.Cat4Val = this.tbCategory4.Text.Trim();
				taskMapping.Cat4Action = this.chCategory4Import.Checked;
				taskMapping.CatParent = new int?(this.ddParentCat0.Visible ? GlobalClass.StringToInteger(this.ddParentCat0.SelectedIndex, 0) : 0);
				taskMapping.Cat1Parent = new int?(GlobalClass.StringToInteger(this.ddParentCat1.SelectedIndex, 0));
				taskMapping.Cat2Parent = new int?(GlobalClass.StringToInteger(this.ddParentCat2.SelectedIndex, 0));
				taskMapping.Cat3Parent = new int?(GlobalClass.StringToInteger(this.ddParentCat3.SelectedIndex, 0));
				taskMapping.Cat4Parent = new int?(GlobalClass.StringToInteger(this.ddParentCat4.SelectedIndex, 0));
				taskMapping.FilterVal = this.tbFilterValue.Text.Trim();
				taskMapping.FilterAction = new int?(this.ddFilterOp.SelectedIndex);
				taskMapping.Filter1Val = this.tbFilter1Value.Text.Trim();
				taskMapping.Filter1Action = new int?(this.comboBox_0.SelectedIndex);
				taskMapping.Filter2Val = this.tbFilter2Value.Text.Trim();
				taskMapping.Filter2Action = new int?(this.comboBox_1.SelectedIndex);
				taskMapping.Filter3Val = this.tbFilter3Value.Text.Trim();
				taskMapping.Filter3Action = new int?(this.comboBox_2.SelectedIndex);
				taskMapping.Filter4Val = this.tbFilter4Value.Text.Trim();
				taskMapping.Filter4Action = new int?(this.comboBox_3.SelectedIndex);
				taskMapping.Filter5Val = this.tbFilter5Value.Text.Trim();
				taskMapping.Filter5Action = new int?(this.comboBox_4.SelectedIndex);
				taskMapping.Filter6Val = this.tbFilter6Value.Text.Trim();
				taskMapping.Filter6Action = new int?(this.comboBox_5.SelectedIndex);
				taskMapping.Filter7Val = this.tbFilter7Value.Text.Trim();
				taskMapping.Filter7Action = new int?(this.comboBox_6.SelectedIndex);
				taskMapping.Filter8Val = this.tbFilter8Value.Text.Trim();
				taskMapping.Filter8Action = new int?(this.comboBox_7.SelectedIndex);
				taskMapping.Filter9Val = this.tbFilter9Value.Text.Trim();
				taskMapping.Filter9Action = new int?(this.comboBox_8.SelectedIndex);
				taskMapping.GtinVal = this.tbGtin.Text.Trim();
				taskMapping.GtinAction = this.chGtinImport.Checked;
				taskMapping.WeightVal = this.nmWeight.Text;
				taskMapping.WeightAction = this.chWeightImport.Checked;
				taskMapping.LengthVal = this.tbLength.Text;
				taskMapping.LengthAction = this.chLengthImport.Checked;
				taskMapping.WidthVal = this.tbWidth.Text;
				taskMapping.WidthAction = this.chWidthImport.Checked;
				taskMapping.HeightVal = this.tbHeight.Text;
				taskMapping.HeightAction = this.chHeightImport.Checked;
				taskMapping.ProdSettingsAllowedQtyVal = this.tbProductAllowQty.Text;
				taskMapping.ProdSettingsAllowedQtyAction = this.chProductAllowQtyAction.Checked;
				taskMapping.ProdSettingsMinCartQtyVal = this.tbProductMinCartQty.Text;
				taskMapping.ProdSettingsMinCartQtyAction = this.chProductMinCartQtyAction.Checked;
				taskMapping.ProdSettingsMaxCartQtyVal = this.tbProductMaxCartQty.Text;
				taskMapping.ProdSettingsMaxCartQtyAction = this.chProductMaxCartQtyAction.Checked;
				taskMapping.ProdSettingsMinStockQtyVal = this.tbProductMinStockQty.Text;
				taskMapping.ProdSettingsMinStockQtyAction = this.chProductMinStockQtyAction.Checked;
				taskMapping.ProdSettingsNotifyQtyVal = this.tbProductNotifyQty.Text;
				taskMapping.ProdSettingsNotifyQtyAction = this.chProductNotifyQtyAction.Checked;
				taskMapping.ProdSettingsShippingEnabledVal = this.chProductShippingEnabled.Checked;
				taskMapping.ProdSettingsShippingEnabledAction = this.chProductShippingEnabledImport.Checked;
				taskMapping.ProdSettingsFreeShippingVal = this.chProductFreeShipping.Checked;
				taskMapping.ProdSettingsFreeShippingAction = this.chProductFreeShippingImport.Checked;
				taskMapping.ProdSettingsShipSeparatelyVal = this.chProductShipSeparately.Checked;
				taskMapping.ProdSettingsShipSeparatelyAction = this.chProductShipSeparatelyImport.Checked;
				taskMapping.ProdSettingsShippingChargeVal = new decimal?(this.nmProductShippingCharge.Value);
				taskMapping.ProdSettingsShippingChargeAction = this.chProductShippingChargeImport.Checked;
				taskMapping.ProdSettingsVisibleIndividuallyVal = this.chVisibleIndividually.Checked;
				taskMapping.ProdSettingsVisibleIndividuallyAction = this.chVisibleIndividuallyImport.Checked;
				taskMapping.ProdSettingsIsDeletedVal = this.chProductIsDeleted.Checked;
				taskMapping.ProdSettingsIsDeletedAction = this.chProductIsDeletedImport.Checked;
				this.ProdInfoListSave(taskMapping);
				this.ProductPricesListSave(taskMapping);
				this.ProdPicturesListSave(taskMapping);
				this.TierPricingListSave(taskMapping);
				this.ProductAttributtesListSave(taskMapping);
				this.ProductSpecAttributtesListSave(taskMapping);
				this.TranslationSave(taskMapping);
				this.CustomSqlSave(taskMapping);
			}
			if (this.entityType == EntityType.Users || this.entityType == EntityType.Products)
			{
				taskMapping.CustomerRoleVal = this.tbCustomerRole.Text.Trim();
				taskMapping.CustomerRoleAction = this.chCustomerRoleImport.Checked;
				taskMapping.CustomerRole1Val = this.tbCustomerRole1.Text.Trim();
				taskMapping.CustomerRole1Action = this.chCustomerRole1Import.Checked;
				taskMapping.CustomerRole2Val = this.tbCustomerRole2.Text.Trim();
				taskMapping.CustomerRole2Action = this.chCustomerRole2Import.Checked;
				taskMapping.CustomerRole3Val = this.tbCustomerRole3.Text.Trim();
				taskMapping.CustomerRole3Action = this.chCustomerRole3Import.Checked;
				taskMapping.CustomerRole4Val = this.tbCustomerRole4.Text.Trim();
				taskMapping.CustomerRole4Action = this.chCustomerRole4Import.Checked;
				taskMapping.TaskEmailSettings.EmailAddressFrom = this.tbEmailAddressFrom.Text;
				taskMapping.TaskEmailSettings.EmailAddressTo = this.tbEmailTo.Text;
				taskMapping.TaskEmailSettings.EmailDisplayName = this.tbEmailDisplayName.Text;
				taskMapping.TaskEmailSettings.EmailHost = this.tbEmailHost.Text;
				taskMapping.TaskEmailSettings.EmailPort = this.tbEmailPort.Text;
				taskMapping.TaskEmailSettings.EmailUser = this.tbEmailUser.Text;
				taskMapping.TaskEmailSettings.EmailPassword = this.tbEmailPassword.Text;
				taskMapping.TaskEmailSettings.EmailSubject = this.tbEmailSubject.Text;
				taskMapping.TaskEmailSettings.EmailBody = this.tbEmailBody.Text;
				taskMapping.TaskEmailSettings.EmailSSL = this.checkBox_0.Checked;
			}
			if (this.entityType == EntityType.Users)
			{
				this.CustomerListSave(taskMapping);
				this.CustomerAddressListSave(taskMapping);
				this.CustomerRoleListSave(taskMapping);
				this.CustomerOthersListSave(taskMapping);
				this.AddressCustomAttributesListSave(taskMapping);
				this.CustomerCustomAttributesListSave(taskMapping);
				this.CustomSqlSave(taskMapping);
			}
			if (this.entityType == EntityType.WishList)
			{
				this.WishListSave(taskMapping);
			}
			return taskMapping;
		}

		// Token: 0x06000149 RID: 329 RVA: 0x000181B0 File Offset: 0x000163B0
		private async void LoadTaskDataToForm()
		{
			TaskMapping taskData = taskMappingFunc.GetTaskMapping(taskId);
			
			chDeleteOldCatMapping.Checked = taskData.DeleteOldCatMapping;
			chDeleteOldPictures.Checked = taskData.DeleteOldPictures;
			chDeleteOldAttributtes.Checked = taskData.DeleteOldAttributtes;
			chDeleteOldSpecAttributes.Checked = taskData.DeleteOldSpecAttributtes;
			chCustomerAddress_DeleteOld.Checked = taskData.DeleteOldCustomerAddresses;
			chCustomerAddress_DeleteOld2.Checked = taskData.DeleteOldCustomerAddresses;
			chForceShippingAddressOnUpdate.Checked = taskData.ForceShippingAddressOnUpdate;
			chForceBillingAddressOnUpdate.Checked = taskData.ForceBillingAddressOnUpdate;
			chCustomerRole_DeleteOld.Checked = taskData.DeleteOldCustomerRoles;
			chDeleteExistingTierPricingOnUpdate.Checked = taskData.DeleteExistingTierPricingOnUpdate;
			chInsertProductsToLastSubCategory.Checked = taskData.ProductsInTheLastSubCategory;
			chAddSpecAttributesAsTable.Checked = taskData.AddSpecAttributtesToFullDescAsTable;
			chInsertCategoryPictureOnInsert.Checked = taskData.InsertCategoryPictureOnInsert;
			chDisableNestedCategories.Checked = taskData.DisableNestedCategories;
			chResizePicture.Checked = taskData.ResizePicture;
			nmPictureResizeHeight.Value = taskData.ResizePictureHeight;
			nmPictureResizeWidth.Value = taskData.ResizePictureWidth;
			nmPictureResizeQuality.Value = taskData.ResizePictureQuality;
			tbTaskName.Text = taskData.TaskName;
			ddStoreWeb.SelectedValue = taskData.EShopId;
			ddTaskStatus.SelectedValue = taskData.TaskStatusId;
			ddTaskAction.SelectedIndex = taskData.TaskAction;
			chDisableShop.Checked = taskData.DisableShop;
			chUnpublish.Checked = taskData.UnpublishProducts;
			chDeleteFileAfterImport.Checked = taskData.DeleteSourceFile;
			chPicturesInFiles.Checked = taskData.SavePicturesInFile;
			chTitleCase.Checked = taskData.UseTitleCase;
			tbPicturesPathSave.Text = taskData.SavePicturesPath;
			tbSourceCopyPath.Text = taskData.SaveSourceCopyPath;
			ddScheduleType.SelectedIndex = taskData.ScheduleType.GetValueOrDefault();
			nmInterval.Value = taskData.ScheduleInterval.GetValueOrDefault();
			nmHour.Value = taskData.ScheduleHour.GetValueOrDefault();
			nmMinute.Value = taskData.ScheduleMinute.GetValueOrDefault();
			vendorSourceId = taskData.VendorSourceId;
			btnCopy.Enabled = (taskData.TaskId > 0);
			entityType = taskData.EntityType;
			if (entityType == EntityType.Products)
			{
				tbProductName.Text = taskData.ProdNameVal;
				chProdNameImport.Checked = taskData.ProdNameAction;
				tbProdShortDesc.Text = taskData.ProdShortDescVal;
				chProdShortDescImport.Checked = taskData.ProdShortDescAction;
				tbProdFullDesc.Text = taskData.ProdFullDescVal;
				chProdFullDescImport.Checked = taskData.ProdFullDescAction;
				tbManufacturerPnum.Text = taskData.ProdManPartNumVal;
				chManufacturerImport.Checked = taskData.ProdManPartNumAction;
				nmStockQuantity.Text = taskData.ProdStockVal;
				chProdStockImport.Checked = taskData.ProdStockAction;
				tbManufacturer.Text = taskData.ManufactVal;
				chManufacturerImport.Checked = taskData.ManufactAction;
				tbVendor.Text = taskData.VendorVal;
				chVendorImport.Checked = taskData.VendorAction;
				tbRelatedSourceAddress.Text = taskData.RelatedSourceAddress;
				ddRelatedTask.SelectedValue = taskData.RelatedTaskId;
				tbGtin.Text = taskData.GtinVal;
				chGtinImport.Checked = taskData.GtinAction;
				nmWeight.Text = taskData.WeightVal;
				chWeightImport.Checked = taskData.WeightAction;
				tbLength.Text = taskData.LengthVal;
				chLengthImport.Checked = taskData.LengthAction;
				tbWidth.Text = taskData.WidthVal;
				chWidthImport.Checked = taskData.WidthAction;
				tbHeight.Text = taskData.HeightVal;
				chHeightImport.Checked = taskData.HeightAction;
				if (taskData.DeliveryDayId.HasValue && taskData.DeliveryDayId > -1)
				{
					ddDeliveryDate.SelectedValue = taskData.DeliveryDayId;
				}
				chDayDeliveryImport.Checked = taskData.ProdSettingsDeliveryDateAction;
				if (taskData.WarehouseId.HasValue && taskData.WarehouseId > -1)
				{
					ddWarehouse.SelectedValue = taskData.WarehouseId;
				}
				chWarehouseImport.Checked = taskData.WarehouseId.HasValue;
				if (taskData.ProductTemplateId.HasValue && taskData.ProductTemplateId > -1)
				{
					ddProductType.SelectedValue = taskData.ProductTemplateId;
				}
				chProductTypeImport.Checked = taskData.ProductTemplateId.HasValue;
				if (taskData.ManageInventoryMethodId.HasValue && taskData.ManageInventoryMethodId > -1)
				{
					ddInventoryMethod.SelectedValue = taskData.ManageInventoryMethodId;
					chInventoryMethodImport.Checked = true;
				}
				if (taskData.BackorderModeId.HasValue && taskData.BackorderModeId > -1)
				{
					ddlBackorderMode.SelectedValue = taskData.BackorderModeId;
					chBackorderModeImport.Checked = true;
				}
				if (taskData.LowStockActivityId.HasValue && taskData.LowStockActivityId > -1)
				{
					ddLowStockActivity.SelectedValue = taskData.LowStockActivityId;
					chLowStockActivityImport.Checked = true;
				}
				if (taskData.ProductPublish.HasValue)
				{
					chProductPublish.Checked = taskData.ProductPublish.Value;
					chProductPublishImport.Checked = true;
				}
				if (taskData.AllowCustomerReviews.HasValue)
				{
					chProductReviewEnable.Checked = taskData.AllowCustomerReviews.Value;
					chProductReviewEnableImport.Checked = true;
				}
				if (taskData.DisplayAvailability.HasValue)
				{
					chProductDisplayAvailability.Checked = taskData.DisplayAvailability.Value;
					chProductDisplayAvailabilityImport.Checked = true;
				}
				if (taskData.DisplayStockQuantity.HasValue)
				{
					chProductDisplayStockQuantity.Checked = taskData.DisplayStockQuantity.Value;
					chProductDisplayStockQuantityImport.Checked = true;
				}
				tbMetaKeywords.Text = taskData.SeoMetaKeyVal;
				chSeoMetaKeyImport.Checked = taskData.SeoMetaKeyAction;
				tbMetaDescription.Text = taskData.SeoMetaDescVal;
				chSeoMetaDescImport.Checked = taskData.SeoMetaDescAction;
				tbMetaTitle.Text = taskData.SeoMetaTitleVal;
				chSeoMetaTitleImport.Checked = taskData.SeoMetaTitleAction;
				tbSearchEnginePage.Text = taskData.SeoPageVal;
				chSearchPageImport.Checked = taskData.SeoPageAction;
				tbCategory.Text = taskData.CatVal;
				chCategoryImport.Checked = taskData.CatAction;
				tbCategory1.Text = taskData.Cat1Val;
				chCategory1Import.Checked = taskData.Cat1Action;
				tbCategory2.Text = taskData.Cat2Val;
				chCategory2Import.Checked = taskData.Cat2Action;
				tbCategory3.Text = taskData.Cat3Val;
				chCategory3Import.Checked = taskData.Cat3Action;
				tbCategory4.Text = taskData.Cat4Val;
				chCategory4Import.Checked = taskData.Cat4Action;
				ddParentCat0.SelectedIndex = (ddParentCat0.Visible ? taskData.CatParent.GetValueOrDefault() : 0);
				ddParentCat1.SelectedIndex = taskData.Cat1Parent.GetValueOrDefault();
				ddParentCat2.SelectedIndex = taskData.Cat2Parent.GetValueOrDefault();
				ddParentCat3.SelectedIndex = taskData.Cat3Parent.GetValueOrDefault();
				ddParentCat4.SelectedIndex = taskData.Cat4Parent.GetValueOrDefault();
				tbProductAllowQty.Text = taskData.ProdSettingsAllowedQtyVal;
				chProductAllowQtyAction.Checked = taskData.ProdSettingsAllowedQtyAction;
				tbProductMinCartQty.Text = taskData.ProdSettingsMinCartQtyVal;
				chProductMinCartQtyAction.Checked = taskData.ProdSettingsMinCartQtyAction;
				tbProductMinStockQty.Text = taskData.ProdSettingsMinStockQtyVal;
				chProductMinStockQtyAction.Checked = taskData.ProdSettingsMinStockQtyAction;
				tbProductMaxCartQty.Text = taskData.ProdSettingsMaxCartQtyVal;
				chProductMaxCartQtyAction.Checked = taskData.ProdSettingsMaxCartQtyAction;
				tbProductNotifyQty.Text = taskData.ProdSettingsNotifyQtyVal;
				chProductNotifyQtyAction.Checked = taskData.ProdSettingsNotifyQtyAction;
				chProductShippingEnabled.Checked = taskData.ProdSettingsShippingEnabledVal;
				chProductShippingEnabledImport.Checked = taskData.ProdSettingsShippingEnabledAction;
				chProductFreeShipping.Checked = taskData.ProdSettingsFreeShippingVal;
				chProductFreeShippingImport.Checked = taskData.ProdSettingsFreeShippingAction;
				chProductShipSeparately.Checked = taskData.ProdSettingsShipSeparatelyVal;
				chProductShipSeparatelyImport.Checked = taskData.ProdSettingsShipSeparatelyAction;
				nmProductShippingCharge.Value = taskData.ProdSettingsShippingChargeVal.GetValueOrDefault();
				chProductShippingChargeImport.Checked = taskData.ProdSettingsShippingChargeAction;
				chVisibleIndividually.Checked = taskData.ProdSettingsVisibleIndividuallyVal;
				chVisibleIndividuallyImport.Checked = taskData.ProdSettingsVisibleIndividuallyAction;
				chProductIsDeleted.Checked = taskData.ProdSettingsIsDeletedVal;
				chProductIsDeletedImport.Checked = taskData.ProdSettingsIsDeletedAction;
				tbFilterValue.Text = taskData.FilterVal;
				ddFilterOp.SelectedIndex = taskData.FilterAction.GetValueOrDefault();
				tbFilter1Value.Text = taskData.Filter1Val;
				comboBox_0.SelectedIndex = taskData.Filter1Action.GetValueOrDefault();
				tbFilter2Value.Text = taskData.Filter2Val;
				comboBox_1.SelectedIndex = taskData.Filter2Action.GetValueOrDefault();
				tbFilter3Value.Text = taskData.Filter3Val;
				comboBox_2.SelectedIndex = taskData.Filter3Action.GetValueOrDefault();
				tbFilter4Value.Text = taskData.Filter4Val;
				comboBox_3.SelectedIndex = taskData.Filter4Action.GetValueOrDefault();
				tbFilter5Value.Text = taskData.Filter5Val;
				comboBox_4.SelectedIndex = taskData.Filter5Action.GetValueOrDefault();
				tbFilter6Value.Text = taskData.Filter6Val;
				comboBox_5.SelectedIndex = taskData.Filter6Action.GetValueOrDefault();
				tbFilter7Value.Text = taskData.Filter7Val;
				comboBox_6.SelectedIndex = taskData.Filter7Action.GetValueOrDefault();
				tbFilter8Value.Text = taskData.Filter8Val;
				comboBox_7.SelectedIndex = taskData.Filter8Action.GetValueOrDefault();
				tbFilter9Value.Text = taskData.Filter9Val;
				comboBox_8.SelectedIndex = taskData.Filter9Action.GetValueOrDefault();
				TranslationLoad(taskData);
				chLimitedToSoresEntities.Visible = true;
			}
			if (entityType == EntityType.Users || entityType == EntityType.Products)
			{
				tbCustomerRole.Text = taskData.CustomerRoleVal;
				chCustomerRoleImport.Checked = taskData.CustomerRoleAction;
				tbCustomerRole1.Text = taskData.CustomerRole1Val;
				chCustomerRole1Import.Checked = taskData.CustomerRole1Action;
				tbCustomerRole2.Text = taskData.CustomerRole2Val;
				chCustomerRole2Import.Checked = taskData.CustomerRole2Action;
				tbCustomerRole3.Text = taskData.CustomerRole3Val;
				chCustomerRole3Import.Checked = taskData.CustomerRole3Action;
				tbCustomerRole4.Text = taskData.CustomerRole4Val;
				chCustomerRole4Import.Checked = taskData.CustomerRole4Action;
				tbEmailAddressFrom.Text = taskData.TaskEmailSettings.EmailAddressFrom;
				tbEmailTo.Text = taskData.TaskEmailSettings.EmailAddressTo;
				tbEmailDisplayName.Text = taskData.TaskEmailSettings.EmailDisplayName;
				tbEmailHost.Text = taskData.TaskEmailSettings.EmailHost;
				tbEmailPort.Text = taskData.TaskEmailSettings.EmailPort;
				tbEmailUser.Text = taskData.TaskEmailSettings.EmailUser;
				tbEmailPassword.Text = taskData.TaskEmailSettings.EmailPassword;
				checkBox_0.Checked = taskData.TaskEmailSettings.EmailSSL;
				tbEmailSubject.Text = taskData.TaskEmailSettings.EmailSubject;
				tbEmailBody.Text = taskData.TaskEmailSettings.EmailBody;
			}
			if (entityType == EntityType.Users || entityType == EntityType.WishList)
			{
				chLimitedToSoresEntities.Visible = false;
			}
			loadVendorSourceMap = true;
			ddVendorSourceMap.SelectedValue = taskData.VendorSourceId;
			LoadSourceMappingToForm(taskData);
			LoadSchedulerFields();
			if (!string.IsNullOrEmpty(taskData.LimitedToStores))
			{
				List<string> limitedStores = taskData.LimitedToStores.Split(new char[1]
				{
				';'
				}, StringSplitOptions.RemoveEmptyEntries).ToList();
				foreach (string storeId in limitedStores)
				{
					for (int j = 0; j < chLimitedStore.Items.Count; j++)
					{
						if (((DataRowView)chLimitedStore.Items[j]).Row[0].ToString() == storeId)
						{
							chLimitedStore.SetItemChecked(j, value: true);
						}
					}
				}
			}
			if (!string.IsNullOrEmpty(taskData.LimitedToStoresEntities) && chLimitedToSoresEntities.Visible)
			{
				List<string> limitedStoresEntities = taskData.LimitedToStoresEntities.Split(new char[1]
				{
				';'
				}, StringSplitOptions.RemoveEmptyEntries).ToList();
				foreach (string storeEntityId in limitedStoresEntities)
				{
					for (int i = 0; i < chLimitedToSoresEntities.Items.Count; i++)
					{
						if (((DataRowView)chLimitedToSoresEntities.Items[i]).Row[0].ToString() == storeEntityId)
						{
							chLimitedToSoresEntities.SetItemChecked(i, value: true);
						}
					}
				}
			}
			chLimitedStore.ClearSelected();
			chLimitedToSoresEntities.ClearSelected();
		}

		// Token: 0x0600014A RID: 330 RVA: 0x000181EC File Offset: 0x000163EC
		private async void LoadSourceMappingToForm(TaskMapping taskMapping)
		{
			SourceData sourceData = this.sourceMappingFunc.GetSourceData(this.vendorSourceId);
			if (sourceData.SourceType == 4)
			{
				this.btnQuickImport.Visible = true;
			}
			else
			{
				this.btnQuickImport.Visible = false;
			}
			SourceMapping sourceMapping = this.sourceMappingFunc.GetSourceMapping(this.vendorSourceId);
			
			this.entityType = sourceMapping.EntityType;
			if (this.entityType == EntityType.Products)
			{
				this.chProdIdMap.Checked = GlobalClass.IsMapped(sourceMapping.ProdIdMap, null);
				this.chProdNameMap.Checked = GlobalClass.IsMapped(sourceMapping.ProdNameMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chProdNameImport.Checked = GlobalClass.IsMapped(sourceMapping.ProdNameMap, sourceMapping.ProdNameVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbProductName.Text = sourceMapping.ProdNameVal;
				}
				this.chProdShortDescMap.Checked = GlobalClass.IsMapped(sourceMapping.ProdShortDescMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chProdShortDescImport.Checked = GlobalClass.IsMapped(sourceMapping.ProdShortDescMap, sourceMapping.ProdShortDescVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbProdShortDesc.Text = sourceMapping.ProdShortDescVal;
				}
				this.chProdFullDescMap.Checked = GlobalClass.IsMapped(sourceMapping.ProdFullDescMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chProdFullDescImport.Checked = GlobalClass.IsMapped(sourceMapping.ProdFullDescMap, sourceMapping.ProdFullDescVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbProdFullDesc.Text = sourceMapping.ProdFullDescVal;
				}
				this.chProdManPartNumMap.Checked = GlobalClass.IsMapped(sourceMapping.ProdManPartNumMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chProdManPartNumImport.Checked = GlobalClass.IsMapped(sourceMapping.ProdManPartNumMap, sourceMapping.ProdManPartNumVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbManufacturerPnum.Text = sourceMapping.ProdManPartNumVal;
				}
				this.chProdStockMap.Checked = GlobalClass.IsMapped(sourceMapping.ProdStockMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chProdStockImport.Checked = GlobalClass.IsMapped(sourceMapping.ProdStockMap, sourceMapping.ProdStockVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.nmStockQuantity.Text = sourceMapping.ProdStockVal;
				}
				this.chSeoMetaKeyMap.Checked = GlobalClass.IsMapped(sourceMapping.SeoMetaKeyMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chSeoMetaKeyImport.Checked = GlobalClass.IsMapped(sourceMapping.SeoMetaKeyMap, sourceMapping.SeoMetaKeyVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbMetaKeywords.Text = sourceMapping.SeoMetaKeyVal;
				}
				this.chSeoMetaDescMap.Checked = GlobalClass.IsMapped(sourceMapping.SeoMetaDescMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chSeoMetaDescImport.Checked = GlobalClass.IsMapped(sourceMapping.SeoMetaDescMap, sourceMapping.SeoMetaDescVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbMetaDescription.Text = sourceMapping.SeoMetaDescVal;
				}
				this.chSeoMetaTitleMap.Checked = GlobalClass.IsMapped(sourceMapping.SeoMetaTitleMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chSeoMetaTitleImport.Checked = GlobalClass.IsMapped(sourceMapping.SeoMetaTitleMap, sourceMapping.SeoMetaTitleVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbMetaTitle.Text = sourceMapping.SeoMetaTitleVal;
				}
				this.chSearchPageMap.Checked = GlobalClass.IsMapped(sourceMapping.SeoPageMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chSearchPageImport.Checked = GlobalClass.IsMapped(sourceMapping.SeoPageMap, sourceMapping.SeoPageVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbSearchEnginePage.Text = sourceMapping.SeoPageVal;
				}
				this.ddParentCat0.Visible = (sourceMapping.CatDelimeter != null);
				this.chCategoryMap.Checked = GlobalClass.IsMapped(sourceMapping.CatMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chCategoryImport.Checked = GlobalClass.IsMapped(sourceMapping.CatMap, sourceMapping.CatVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbCategory.Text = sourceMapping.CatVal;
				}
				this.chCategory1Map.Checked = GlobalClass.IsMapped(sourceMapping.Cat1Map, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chCategory1Import.Checked = GlobalClass.IsMapped(sourceMapping.Cat1Map, sourceMapping.Cat1Val);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbCategory1.Text = sourceMapping.Cat1Val;
				}
				this.chCategory2Map.Checked = GlobalClass.IsMapped(sourceMapping.Cat2Map, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chCategory2Import.Checked = GlobalClass.IsMapped(sourceMapping.Cat2Map, sourceMapping.Cat2Val);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbCategory2.Text = sourceMapping.Cat2Val;
				}
				this.chCategory3Map.Checked = GlobalClass.IsMapped(sourceMapping.Cat3Map, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chCategory3Import.Checked = GlobalClass.IsMapped(sourceMapping.Cat3Map, sourceMapping.Cat3Val);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbCategory3.Text = sourceMapping.Cat3Val;
				}
				this.chCategory4Map.Checked = GlobalClass.IsMapped(sourceMapping.Cat4Map, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chCategory4Import.Checked = GlobalClass.IsMapped(sourceMapping.Cat4Map, sourceMapping.Cat4Val);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbCategory4.Text = sourceMapping.Cat4Val;
				}
				this.chManufacturerMap.Checked = GlobalClass.IsMapped(sourceMapping.ManufactMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chManufacturerImport.Checked = GlobalClass.IsMapped(sourceMapping.ManufactMap, sourceMapping.ManufactVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbManufacturer.Text = sourceMapping.ManufactVal;
				}
				this.chVendorMap.Checked = GlobalClass.IsMapped(sourceMapping.VendorMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chVendorImport.Checked = GlobalClass.IsMapped(sourceMapping.VendorMap, sourceMapping.VendorVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbVendor.Text = sourceMapping.VendorVal;
				}
				this.chGtinMap.Checked = GlobalClass.IsMapped(sourceMapping.GtinMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chGtinImport.Checked = GlobalClass.IsMapped(sourceMapping.GtinMap, sourceMapping.GtinVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbGtin.Text = sourceMapping.GtinVal;
				}
				this.chWeightMap.Checked = GlobalClass.IsMapped(sourceMapping.WeightMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chWeightImport.Checked = GlobalClass.IsMapped(sourceMapping.WeightMap, sourceMapping.WeightVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.nmWeight.Text = sourceMapping.WeightVal;
				}
				this.chLengthMap.Checked = GlobalClass.IsMapped(sourceMapping.LengthMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chLengthImport.Checked = GlobalClass.IsMapped(sourceMapping.LengthMap, sourceMapping.LengthVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbLength.Text = sourceMapping.LengthVal;
				}
				this.chWidthMap.Checked = GlobalClass.IsMapped(sourceMapping.WidthMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chWidthImport.Checked = GlobalClass.IsMapped(sourceMapping.WidthMap, sourceMapping.WidthVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbWidth.Text = sourceMapping.WidthVal;
				}
				this.chHeightMap.Checked = GlobalClass.IsMapped(sourceMapping.HeightMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chHeightImport.Checked = GlobalClass.IsMapped(sourceMapping.HeightMap, sourceMapping.HeightVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbHeight.Text = sourceMapping.HeightVal;
				}
				this.chFilterMap.Checked = GlobalClass.IsMapped(sourceMapping.FilterMap, null);
				this.ddFilterOp.Enabled = GlobalClass.IsMapped(sourceMapping.FilterMap, null);
				this.lblFilter.Text = "Filter - " + sourceMapping.FilterName;
				this.tbFilterValue.Enabled = GlobalClass.IsMapped(sourceMapping.FilterMap, null);
				this.chFilter1Map.Checked = GlobalClass.IsMapped(sourceMapping.Filter1Map, null);
				this.comboBox_0.Enabled = GlobalClass.IsMapped(sourceMapping.Filter1Map, null);
				this.lblFilter1.Text = "Filter1 - " + sourceMapping.Filter1Name;
				this.tbFilter1Value.Enabled = GlobalClass.IsMapped(sourceMapping.Filter1Map, null);
				this.chFilter2Map.Checked = GlobalClass.IsMapped(sourceMapping.Filter2Map, null);
				this.comboBox_1.Enabled = GlobalClass.IsMapped(sourceMapping.Filter2Map, null);
				this.lblFilter2.Text = "Filter2 - " + sourceMapping.Filter2Name;
				this.tbFilter2Value.Enabled = GlobalClass.IsMapped(sourceMapping.Filter2Map, null);
				this.chFilter3Map.Checked = GlobalClass.IsMapped(sourceMapping.Filter3Map, null);
				this.comboBox_2.Enabled = GlobalClass.IsMapped(sourceMapping.Filter3Map, null);
				this.lblFilter3.Text = "Filter3 - " + sourceMapping.Filter3Name;
				this.tbFilter3Value.Enabled = GlobalClass.IsMapped(sourceMapping.Filter3Map, null);
				this.chFilter4Map.Checked = GlobalClass.IsMapped(sourceMapping.Filter4Map, null);
				this.comboBox_3.Enabled = GlobalClass.IsMapped(sourceMapping.Filter4Map, null);
				this.lblFilter4.Text = "Filter4 - " + sourceMapping.Filter4Name;
				this.tbFilter4Value.Enabled = GlobalClass.IsMapped(sourceMapping.Filter4Map, null);
				this.chFilter5Map.Checked = GlobalClass.IsMapped(sourceMapping.Filter5Map, null);
				this.comboBox_4.Enabled = GlobalClass.IsMapped(sourceMapping.Filter5Map, null);
				this.lblFilter5.Text = "Filter5 - " + sourceMapping.Filter5Name;
				this.tbFilter5Value.Enabled = GlobalClass.IsMapped(sourceMapping.Filter5Map, null);
				this.chFilter6Map.Checked = GlobalClass.IsMapped(sourceMapping.Filter6Map, null);
				this.comboBox_5.Enabled = GlobalClass.IsMapped(sourceMapping.Filter6Map, null);
				this.lblFilter6.Text = "Filter6 - " + sourceMapping.Filter6Name;
				this.tbFilter6Value.Enabled = GlobalClass.IsMapped(sourceMapping.Filter6Map, null);
				this.chFilter7Map.Checked = GlobalClass.IsMapped(sourceMapping.Filter7Map, null);
				this.comboBox_6.Enabled = GlobalClass.IsMapped(sourceMapping.Filter7Map, null);
				this.lblFilter7.Text = "Filter7 - " + sourceMapping.Filter7Name;
				this.tbFilter7Value.Enabled = GlobalClass.IsMapped(sourceMapping.Filter7Map, null);
				this.chFilter8Map.Checked = GlobalClass.IsMapped(sourceMapping.Filter8Map, null);
				this.comboBox_7.Enabled = GlobalClass.IsMapped(sourceMapping.Filter8Map, null);
				this.lblFilter8.Text = "Filter8 - " + sourceMapping.Filter8Name;
				this.tbFilter8Value.Enabled = GlobalClass.IsMapped(sourceMapping.Filter8Map, null);
				this.chFilter9Map.Checked = GlobalClass.IsMapped(sourceMapping.Filter9Map, null);
				this.comboBox_8.Enabled = GlobalClass.IsMapped(sourceMapping.Filter9Map, null);
				this.lblFilter9.Text = "Filter9 - " + sourceMapping.Filter9Name;
				this.tbFilter9Value.Enabled = GlobalClass.IsMapped(sourceMapping.Filter9Map, null);
				if (GlobalClass.IsMapped(sourceMapping.ProdSettingsAllowedQtyMap, sourceMapping.ProdSettingsAllowedQtyVal))
				{
					this.chProductAllowQtyMap.Checked = true;
					if (this.loadDefaultDataFromSource)
					{
						this.chProductAllowQtyAction.Checked = true;
					}
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbProductAllowQty.Text = sourceMapping.ProdSettingsAllowedQtyVal;
				}
				if (GlobalClass.IsMapped(sourceMapping.ProdSettingsShippingEnabledMap, sourceMapping.ProdSettingsShippingEnabledVal))
				{
					this.chProductShippingEnabledMap.Checked = true;
					if (this.loadDefaultDataFromSource)
					{
						this.chProductShippingEnabledImport.Checked = true;
					}
				}
				if (this.loadDefaultDataFromSource)
				{
					this.chProductShippingEnabled.Checked = GlobalClass.ObjectToBool(sourceMapping.ProdSettingsShippingEnabledVal);
				}
				if (GlobalClass.IsMapped(sourceMapping.ProdSettingsFreeShippingMap, sourceMapping.ProdSettingsFreeShippingVal))
				{
					this.chProductFreeShippingMap.Checked = true;
					if (this.loadDefaultDataFromSource)
					{
						this.chProductFreeShippingImport.Checked = true;
					}
				}
				if (this.loadDefaultDataFromSource)
				{
					this.chProductFreeShipping.Checked = GlobalClass.ObjectToBool(sourceMapping.ProdSettingsFreeShippingVal);
				}
				if (GlobalClass.IsMapped(sourceMapping.ProdSettingsShipSeparatelyMap, sourceMapping.ProdSettingsShipSeparatelyVal))
				{
					this.chProductShipSeparatelyMap.Checked = true;
					if (this.loadDefaultDataFromSource)
					{
						this.chProductShipSeparatelyImport.Checked = true;
					}
				}
				if (this.loadDefaultDataFromSource)
				{
					this.chProductShipSeparately.Checked = GlobalClass.ObjectToBool(sourceMapping.ProdSettingsShipSeparatelyVal);
				}
				if (GlobalClass.IsMapped(sourceMapping.ProdSettingsShippingChargeMap, sourceMapping.ProdSettingsShippingChargeVal))
				{
					this.chProductShippingChargeMap.Checked = true;
					if (this.loadDefaultDataFromSource)
					{
						this.chProductShippingChargeImport.Checked = true;
					}
				}
				if (this.loadDefaultDataFromSource)
				{
					this.nmProductShippingCharge.Value = GlobalClass.StringToDecimal(sourceMapping.ProdSettingsShippingChargeVal, 0);
				}
				if (GlobalClass.IsMapped(sourceMapping.ProdSettingsDeliveryDateMap, sourceMapping.ProdSettingsDeliveryDateVal))
				{
					this.chDayDeliveryMap.Checked = true;
					if (this.loadDefaultDataFromSource)
					{
						this.chDayDeliveryImport.Checked = true;
					}
				}
				if (this.loadDefaultDataFromSource)
				{
					this.ddDeliveryDate.SelectedText = sourceMapping.ProdSettingsDeliveryDateVal;
				}
				if (GlobalClass.IsMapped(sourceMapping.ProdSettingsMinCartQtyMap, sourceMapping.ProdSettingsMinCartQtyVal))
				{
					this.chProductMinCartQtyMap.Checked = true;
					if (this.loadDefaultDataFromSource)
					{
						this.chProductMinCartQtyAction.Checked = true;
					}
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbProductMinCartQty.Text = sourceMapping.ProdSettingsMinCartQtyVal;
				}
				if (GlobalClass.IsMapped(sourceMapping.ProdSettingsMaxCartQtyMap, sourceMapping.ProdSettingsMaxCartQtyVal))
				{
					this.chProductMaxCartQtyMap.Checked = true;
					if (this.loadDefaultDataFromSource)
					{
						this.chProductMaxCartQtyAction.Checked = true;
					}
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbProductMaxCartQty.Text = sourceMapping.ProdSettingsMaxCartQtyVal;
				}
				if (GlobalClass.IsMapped(sourceMapping.ProdSettingsMinStockQtyMap, sourceMapping.ProdSettingsMinStockQtyVal))
				{
					this.chProductMinStockQtyMap.Checked = true;
					if (this.loadDefaultDataFromSource)
					{
						this.chProductMinStockQtyAction.Checked = true;
					}
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbProductMinStockQty.Text = sourceMapping.ProdSettingsMinStockQtyVal;
				}
				if (GlobalClass.IsMapped(sourceMapping.ProdSettingsNotifyQtyMap, sourceMapping.ProdSettingsNotifyQtyVal))
				{
					this.chProductNotifyQtyMap.Checked = true;
					if (this.loadDefaultDataFromSource)
					{
						this.chProductNotifyQtyAction.Checked = true;
					}
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbProductNotifyQty.Text = sourceMapping.ProdSettingsNotifyQtyVal;
				}
				if (GlobalClass.IsMapped(sourceMapping.ProdSettingsVisibleIndividuallyMap, sourceMapping.ProdSettingsVisibleIndividuallyVal))
				{
					this.chVisibleIndividuallyMap.Checked = true;
					if (this.loadDefaultDataFromSource)
					{
						this.chVisibleIndividuallyImport.Checked = true;
					}
				}
				if (this.loadDefaultDataFromSource)
				{
					this.chVisibleIndividually.Checked = GlobalClass.ObjectToBool(sourceMapping.ProdSettingsVisibleIndividuallyVal);
				}
				if (GlobalClass.IsMapped(sourceMapping.ProdSettingsIsDeletedMap, sourceMapping.ProdSettingsIsDeletedVal))
				{
					this.chProductIsDeletedMap.Checked = true;
					if (this.loadDefaultDataFromSource)
					{
						this.chProductIsDeletedImport.Checked = true;
					}
				}
				if (this.loadDefaultDataFromSource)
				{
					this.chProductIsDeleted.Checked = GlobalClass.ObjectToBool(sourceMapping.ProdSettingsIsDeletedVal);
				}
				this.ProdInfoListLoad(sourceMapping, taskMapping, (sourceData.SourceFormat == 0) ? StructureFormat.XML : StructureFormat.Position);
				this.ProdPicturesListLoad(sourceMapping, taskMapping, (sourceData.SourceFormat == 0) ? StructureFormat.XML : StructureFormat.Position);
				this.ProductPricesListLoad(sourceMapping, taskMapping, (sourceData.SourceFormat == 0) ? StructureFormat.XML : StructureFormat.Position);
				this.TierPricingListLoad(sourceMapping, taskMapping, (sourceData.SourceFormat == 0) ? StructureFormat.XML : StructureFormat.Position);
				this.ProductAttributtesListLoad(sourceMapping, taskMapping, (sourceData.SourceFormat == 0) ? StructureFormat.XML : StructureFormat.Position);
				this.ProductSpecAttributtesListLoad(sourceMapping, taskMapping, (sourceData.SourceFormat == 0) ? StructureFormat.XML : StructureFormat.Position);
				this.CustomSqlLoad(taskMapping);
			}
			if (this.entityType == EntityType.Users || this.entityType == EntityType.Products)
			{
				this.chCustomerRoleMap.Checked = GlobalClass.IsMapped(sourceMapping.CustomerRoleMap, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chCustomerRoleImport.Checked = GlobalClass.IsMapped(sourceMapping.CustomerRoleMap, sourceMapping.CustomerRoleVal);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbCustomerRole.Text = sourceMapping.CustomerRoleVal;
				}
				this.chCustomerRole1Map.Checked = GlobalClass.IsMapped(sourceMapping.CustomerRole1Map, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chCustomerRole1Import.Checked = GlobalClass.IsMapped(sourceMapping.CustomerRole1Map, sourceMapping.CustomerRole1Val);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbCustomerRole1.Text = sourceMapping.CustomerRole1Val;
				}
				this.chCustomerRole2Map.Checked = GlobalClass.IsMapped(sourceMapping.CustomerRole2Map, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chCustomerRole2Import.Checked = GlobalClass.IsMapped(sourceMapping.CustomerRole2Map, sourceMapping.CustomerRole2Val);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbCustomerRole2.Text = sourceMapping.CustomerRole2Val;
				}
				this.chCustomerRole3Map.Checked = GlobalClass.IsMapped(sourceMapping.CustomerRole3Map, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chCustomerRole3Import.Checked = GlobalClass.IsMapped(sourceMapping.CustomerRole3Map, sourceMapping.CustomerRole3Val);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbCustomerRole3.Text = sourceMapping.CustomerRole3Val;
				}
				this.chCustomerRole4Map.Checked = GlobalClass.IsMapped(sourceMapping.CustomerRole4Map, null);
				if (this.loadDefaultDataFromSource)
				{
					this.chCustomerRole4Import.Checked = GlobalClass.IsMapped(sourceMapping.CustomerRole4Map, sourceMapping.CustomerRole4Val);
				}
				if (this.loadDefaultDataFromSource)
				{
					this.tbCustomerRole4.Text = sourceMapping.CustomerRole4Val;
				}
			}
			if (this.entityType == EntityType.Users)
			{
				this.CustomerListLoad(sourceMapping, taskMapping, (sourceData.SourceFormat == 0) ? StructureFormat.XML : StructureFormat.Position);
				this.CustomerAddressListLoad(sourceMapping, taskMapping, (sourceData.SourceFormat == 0) ? StructureFormat.XML : StructureFormat.Position);
				this.CustomerRoleListLoad(sourceMapping, taskMapping, (sourceData.SourceFormat == 0) ? StructureFormat.XML : StructureFormat.Position);
				this.CustomerOthersListLoad(sourceMapping, taskMapping, (sourceData.SourceFormat == 0) ? StructureFormat.XML : StructureFormat.Position);
				this.AddressCustomAttributesListLoad(sourceMapping, taskMapping, (sourceData.SourceFormat == 0) ? StructureFormat.XML : StructureFormat.Position);
				this.CustomerCustomAttributesListLoad(sourceMapping, taskMapping, (sourceData.SourceFormat == 0) ? StructureFormat.XML : StructureFormat.Position);
				this.CustomSqlLoad(taskMapping);
			}
			if (this.entityType == EntityType.WishList)
			{
				this.WishListLoad(sourceMapping, taskMapping, (sourceData.SourceFormat == 0) ? StructureFormat.XML : StructureFormat.Position);
			}
			this.LoadTabs();
		}

		// Token: 0x0600014B RID: 331 RVA: 0x00018230 File Offset: 0x00016430
		private void LoadSourceMappingField(Control control_map, Control control_import, Control controlDefault, object map, object val)
		{
			((CheckBox)control_map).Checked = GlobalClass.IsMapped(map, null);
			if (this.loadDefaultDataFromSource)
			{
				((CheckBox)control_import).Checked = GlobalClass.IsMapped(map, val);
			}
			if (this.loadDefaultDataFromSource && controlDefault.GetType().Name == "TextBox")
			{
				((TextBox)controlDefault).Text = ((val != null) ? val.ToString() : null);
			}
			if (this.loadDefaultDataFromSource && controlDefault.GetType().Name == "NumericUpDown")
			{
				((NumericUpDown)controlDefault).Value = GlobalClass.StringToInteger(val, 0);
			}
		}

		// Token: 0x0600014C RID: 332 RVA: 0x000182E4 File Offset: 0x000164E4
		private async void btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				if (ValidateChildren(ValidationConstraints.Enabled))
				{
					TaskMapping getTaskData = GetTaskDataFromForm();
					if (taskId > 0)
					{
						taskMappingFunc.UpdateTaskMapping(GetTaskDataFromForm());
					}
					else
					{
						taskId = taskMappingFunc.InsertTaskMapping(GetTaskDataFromForm());
						_owner.gridFill();
						LoadData();
					}
					_owner.gridFill();
					loadDefaultDataFromSource = false;
					
					SetStatusArea(valid: true, "Data saved successfully. After all changes need to restart Task scheduler service.");
				}
				else
				{
					SetStatusArea(valid: false, "Data not saved.");
				}
			}
			catch (Exception ex2)
			{
				Exception ex = ex2;
				MessageBox.Show(ex.ToString());
				SetStatusArea(valid: false, "Data not saved.");
			}
		}

		// Token: 0x0600014D RID: 333 RVA: 0x00018330 File Offset: 0x00016530
		private void ddVendorSourceMap_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.loadVendorSourceMap && this.vendorSourceId != GlobalClass.StringToInteger(this.ddVendorSourceMap.SelectedValue, 0))
			{
				this.vendorSourceId = GlobalClass.StringToInteger(this.ddVendorSourceMap.SelectedValue, 0);
				this.loadDefaultDataFromSource = true;
				this.LoadSourceMappingToForm(null);
			}
			else if (!this.loadVendorSourceMap && GlobalClass.StringToInteger(this.ddVendorSourceMap.SelectedValue, 0) > 0)
			{
				this.vendorSourceId = GlobalClass.StringToInteger(this.ddVendorSourceMap.SelectedValue, 0);
				SourceMapping sourceMapping = this.sourceMappingFunc.GetSourceMapping(this.vendorSourceId);
				this.entityType = sourceMapping.EntityType;
			}
		}

		// Token: 0x0600014E RID: 334 RVA: 0x000024A0 File Offset: 0x000006A0
		private void ddFilterOp_SelectedIndexChanged(object sender, EventArgs e)
		{
		}

		// Token: 0x0600014F RID: 335 RVA: 0x000183E4 File Offset: 0x000165E4
		private async void btnTestMapping_Click(object sender, EventArgs e)
		{
			
			try
			{
				FileFormat fileFormat = FileFormat.XML;
				Cursor.Current = Cursors.WaitCursor;
				SourceData sourceData = sourceMappingFunc.GetSourceData(vendorSourceId);
				

				if(xmlModify.Checked == true)
                {
					string ticimaxSave = @"Temp\\lingerium.xml";

					byte[] fileContent = sourceMappingFunc.GetFileContent((SourceType)sourceData.SourceType, sourceData.SourceUser, sourceData.SourcePassword, ticimaxSave, sourceData.SoapAction, sourceData.SoapRequest, sourceData.SourceEncoding, sourceData.SourceFormat == 2, sourceData.SourceContent, sourceData.AuthHeader);
					Cursor.Current = Cursors.Default;
					if (fileContent != null)
					{
						Cursor.Current = Cursors.WaitCursor;
						TaskMapping taskMapping = GetTaskDataFromForm();
						SourceMapping sourceMapping = sourceMappingFunc.GetSourceMapping(vendorSourceId);
						sourceMapping.CsvDelimeter = sourceData.CsvDelimeter;
						sourceMapping.FirstRowIndex = sourceData.FirstRowIndex;
						SourceDocument sourceDocument2;
						if (sourceData.SourceType == 4)
						{
							fileFormat = FileFormat.AliExpress;
							sourceDocument2 = new SourceDocument(FileFormat.AliExpress, fileContent, sourceMapping, taskMapping, sourceData, loadByOne: true);
						}
						else if (sourceData.SourceType == 5)
						{
							fileFormat = FileFormat.BestSecret;
							sourceDocument2 = new SourceDocument(FileFormat.BestSecret, fileContent, sourceMapping, taskMapping, sourceData, loadByOne: true);
						}
						else
						{
							sourceDocument2 = new SourceDocument((FileFormat)sourceData.SourceFormat, fileContent, sourceMapping, taskMapping, sourceData);
						}
						SourceDocument taskData = taskMappingFunc.GetSourceDoucumentForTasks(sourceDocument2, taskMapping);
						Cursor.Current = Cursors.Default;
						if (entityType == EntityType.Products)
						{
							sourceDocument2 = taskMappingFunc.GetSourceDoucumentForTasks(sourceDocument2, taskMapping);
							sourceDocument2.UpdateTotal();
							itemPreview frmItemPreview3 = new itemPreview(sourceDocument2, sourceMapping, taskMapping, fileFormat);
							frmItemPreview3.StartPosition = FormStartPosition.CenterParent;
							frmItemPreview3.ShowDialog();
						}
						else if (entityType == EntityType.Users)
						{
							userPreview frmItemPreview2 = new userPreview(taskData, sourceMapping, taskMapping);
							frmItemPreview2.StartPosition = FormStartPosition.CenterParent;
							frmItemPreview2.ShowDialog();
						}
						else if (entityType == EntityType.WishList)
						{
							wishListPreview frmItemPreview = new wishListPreview(taskData, sourceMapping, taskMapping);
							frmItemPreview.StartPosition = FormStartPosition.CenterParent;
							frmItemPreview.ShowDialog();
						}
						else
						{
							MessageBox.Show("Test is failed. Please select EntityType");
						}
					}
					else
					{
						MessageBox.Show("Test is failed. Source file is empty");
					}

				}

				else
                {
					byte[] fileContent = sourceMappingFunc.GetFileContent((SourceType)sourceData.SourceType, sourceData.SourceUser, sourceData.SourcePassword, sourceData.SourceAddress, sourceData.SoapAction, sourceData.SoapRequest, sourceData.SourceEncoding, sourceData.SourceFormat == 2, sourceData.SourceContent, sourceData.AuthHeader);
					Cursor.Current = Cursors.Default;
					if (fileContent != null)
					{
						Cursor.Current = Cursors.WaitCursor;
						TaskMapping taskMapping = GetTaskDataFromForm();
						SourceMapping sourceMapping = sourceMappingFunc.GetSourceMapping(vendorSourceId);
						sourceMapping.CsvDelimeter = sourceData.CsvDelimeter;
						sourceMapping.FirstRowIndex = sourceData.FirstRowIndex;
						SourceDocument sourceDocument2;
						if (sourceData.SourceType == 4)
						{
							fileFormat = FileFormat.AliExpress;
							sourceDocument2 = new SourceDocument(FileFormat.AliExpress, fileContent, sourceMapping, taskMapping, sourceData, loadByOne: true);
						}
						else if (sourceData.SourceType == 5)
						{
							fileFormat = FileFormat.BestSecret;
							sourceDocument2 = new SourceDocument(FileFormat.BestSecret, fileContent, sourceMapping, taskMapping, sourceData, loadByOne: true);
						}
						else
						{
							sourceDocument2 = new SourceDocument((FileFormat)sourceData.SourceFormat, fileContent, sourceMapping, taskMapping, sourceData);
						}
						SourceDocument taskData = taskMappingFunc.GetSourceDoucumentForTasks(sourceDocument2, taskMapping);
						Cursor.Current = Cursors.Default;
						if (entityType == EntityType.Products)
						{
							sourceDocument2 = taskMappingFunc.GetSourceDoucumentForTasks(sourceDocument2, taskMapping);
							sourceDocument2.UpdateTotal();
							itemPreview frmItemPreview3 = new itemPreview(sourceDocument2, sourceMapping, taskMapping, fileFormat);
							frmItemPreview3.StartPosition = FormStartPosition.CenterParent;
							frmItemPreview3.ShowDialog();
						}
						else if (entityType == EntityType.Users)
						{
							userPreview frmItemPreview2 = new userPreview(taskData, sourceMapping, taskMapping);
							frmItemPreview2.StartPosition = FormStartPosition.CenterParent;
							frmItemPreview2.ShowDialog();
						}
						else if (entityType == EntityType.WishList)
						{
							wishListPreview frmItemPreview = new wishListPreview(taskData, sourceMapping, taskMapping);
							frmItemPreview.StartPosition = FormStartPosition.CenterParent;
							frmItemPreview.ShowDialog();
						}
						else
						{
							MessageBox.Show("Test is failed. Please select EntityType");
						}
					}
					else
					{
						MessageBox.Show("Test is failed. Source file is empty");
					}
				}

				
			}
			catch (Exception ex)
			{
				
				Cursor.Current = Cursors.Default;
				MessageBox.Show(this, ex.Message, "Error");
			}
			Cursor.Current = Cursors.Default;
		}

		
		private void LoadSchedulerFields()
		{
			if (this.ddScheduleType.SelectedIndex == 0 || this.ddScheduleType.SelectedIndex == 1)
			{
				this.nmInterval.Visible = true;
				this.pnSchedulerTime.Visible = false;
			}
			else if (this.ddScheduleType.SelectedIndex == 2)
			{
				this.nmInterval.Visible = false;
				this.pnSchedulerTime.Visible = true;
			}
			else
			{
				this.nmInterval.Visible = false;
				this.pnSchedulerTime.Visible = false;
			}
		}

		// Token: 0x06000151 RID: 337 RVA: 0x00002D0F File Offset: 0x00000F0F
		private void btnRunTask_Click(object sender, EventArgs e)
		{
			this.taskManager.StartAllTasks(null, true, this.taskId, this.GetTaskDataFromForm());
			MessageBox.Show("Task started. Results will appear in tasks or logs list.");
		}

		// Token: 0x06000152 RID: 338 RVA: 0x00002D35 File Offset: 0x00000F35
		private void ddScheduleType_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.LoadSchedulerFields();
		}

		// Token: 0x06000153 RID: 339 RVA: 0x00002D3D File Offset: 0x00000F3D
		private void ddTaskAction_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.ddTaskAction.SelectedIndex > 0)
			{
				this.chUnpublish.Enabled = true;
			}
			else
			{
				this.chUnpublish.Enabled = false;
				this.chUnpublish.Checked = false;
			}
		}

		// Token: 0x06000154 RID: 340 RVA: 0x000024A0 File Offset: 0x000006A0
		private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
		{
		}

		// Token: 0x06000155 RID: 341 RVA: 0x000184BC File Offset: 0x000166BC
		private async void LoadStoreData(string sqlQuery, ComboBox control, CheckedListBox control2)
		{
			if (this.ddStoreWeb.SelectedIndex > -1 && GlobalClass.IsInteger(this.ddStoreWeb.SelectedValue) && GlobalClass.StringToInteger(this.ddStoreWeb.SelectedValue, 0) > 0 && this.formLoaded)
			{
				this.tabTask.Visible = false;
				this.tableLayoutPanel4.Visible = false;
				Cursor.Current = Cursors.WaitCursor;
				this.storeId = GlobalClass.StringToInteger(this.ddStoreWeb.SelectedValue, 0);
				try
				{
					StoreData storeData = StoreFunc.GetStoreData(Convert.ToInt64(this.ddStoreWeb.SelectedValue));
					this.storeConnString = storeData.DatabaseConnString;
					
					if (control != null)
					{
						helper.FillDropDownList2(sqlQuery, control, this.storeConnString, storeData.DatabaseType);
					}
					if (control2 != null)
					{
						helper.FillCheckBoxList2(sqlQuery, control2, this.storeConnString, storeData.DatabaseType);
					}
					storeData = null;
				}
				catch (Exception ex)
				{
					if (!this.connectionErrorShow)
					{
						this.connectionErrorShow = true;
						MessageBox.Show(this, "Need provide the correct connection string to the nopCommerce store. Go to->settings->stores. Error: " + ex.Message, "Error");
					}
				}
				finally
				{
					this.tabTask.Visible = true;
					this.tableLayoutPanel4.Visible = true;
					Cursor.Current = Cursors.Arrow;
				}
			}
			else
			{
				this.storeConnString = "";
				if (control != null)
				{
					control.DataSource = null;
				}
				if (control2 != null)
				{
					control2.DataSource = null;
				}
			}
		}

		// Token: 0x06000156 RID: 342 RVA: 0x00018510 File Offset: 0x00016710
		private void LoadManageInventoryMethod()
		{
			var dataSource = (from ManageInventoryMethod mode in Enum.GetValues(typeof(ManageInventoryMethod))
			select new
			{
				Value = (int)mode,
				Title = mode
			}).ToList();
			this.ddInventoryMethod.ValueMember = "Value";
			this.ddInventoryMethod.DisplayMember = "Title";
			this.ddInventoryMethod.DataSource = dataSource;
			this.ddInventoryMethod.SelectedIndex = 0;
		}

		// Token: 0x06000157 RID: 343 RVA: 0x00018594 File Offset: 0x00016794
		private void LoadbackOrderModes()
		{
			var dataSource = (from BackorderMode mode in Enum.GetValues(typeof(BackorderMode))
			select new
			{
				Value = (int)mode,
				Title = mode
			}).ToList();
			this.ddlBackorderMode.ValueMember = "Value";
			this.ddlBackorderMode.DisplayMember = "Title";
			this.ddlBackorderMode.DataSource = dataSource;
			this.ddlBackorderMode.SelectedIndex = 0;
		}

		// Token: 0x06000158 RID: 344 RVA: 0x00018618 File Offset: 0x00016818
		private void LoadLowStockActivities()
		{
			var dataSource = (from LowStockActivity mode in Enum.GetValues(typeof(LowStockActivity))
			select new
			{
				Value = (int)mode,
				Title = mode
			}).ToList();
			this.ddLowStockActivity.ValueMember = "Value";
			this.ddLowStockActivity.DisplayMember = "Title";
			this.ddLowStockActivity.DataSource = dataSource;
			this.ddLowStockActivity.SelectedIndex = 0;
		}

		// Token: 0x06000159 RID: 345 RVA: 0x0001869C File Offset: 0x0001689C
		private void SetStatusArea(bool valid, string message)
		{
			this.statusAreaText.Text = message;
			if (valid)
			{
				this.statusArea.BackColor = ColorTranslator.FromHtml("#009D00");
			}
			else
			{
				this.statusArea.BackColor = ColorTranslator.FromHtml("#C00000");
			}
			this.statusAreaText.ForeColor = Color.White;
		}

		// Token: 0x0600015A RID: 346 RVA: 0x000024A0 File Offset: 0x000006A0
		private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
		{
		}

		// Token: 0x0600015B RID: 347 RVA: 0x000024A0 File Offset: 0x000006A0
		private void statusArea_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
		}

		// Token: 0x0600015C RID: 348 RVA: 0x000186F4 File Offset: 0x000168F4
		private async void btnQuickImport_Click(object sender, EventArgs e)
		{
			try
			{
				Cursor.Current = Cursors.WaitCursor;
				SourceData sourceData = sourceMappingFunc.GetSourceData(vendorSourceId);
				
				byte[] fileContent = sourceMappingFunc.GetFileContent((SourceType)sourceData.SourceType, sourceData.SourceUser, sourceData.SourcePassword, sourceData.SourceAddress, sourceData.SoapAction, sourceData.SoapRequest, sourceData.SourceEncoding, sourceData.SourceFormat == 2, sourceData.SourceContent, sourceData.AuthHeader);
				Cursor.Current = Cursors.Default;
				if (fileContent != null)
				{
					Cursor.Current = Cursors.WaitCursor;
					TaskMapping taskMapping = GetTaskDataFromForm();
					SourceMapping sourceMapping = sourceMappingFunc.GetSourceMapping(vendorSourceId);
					sourceMapping.CsvDelimeter = sourceData.CsvDelimeter;
					sourceMapping.FirstRowIndex = sourceData.FirstRowIndex;
					SourceDocument sourceDocument = (sourceData.SourceType == 4) ? new SourceDocument(FileFormat.AliExpress, fileContent, sourceMapping, null, sourceData, loadByOne: true) : ((sourceData.SourceType != 5) ? new SourceDocument((FileFormat)sourceData.SourceFormat, fileContent, sourceMapping, null, sourceData) : new SourceDocument(FileFormat.BestSecret, fileContent, sourceMapping, null, sourceData, loadByOne: true));
					SourceDocument taskData = taskMappingFunc.GetSourceDoucumentForTasks(sourceDocument, taskMapping);
					Cursor.Current = Cursors.Default;
					if (entityType == EntityType.Products)
					{
						productCard frmItemPreview3 = new productCard(sourceDocument, sourceMapping, null);
						frmItemPreview3.StartPosition = FormStartPosition.CenterParent;
						frmItemPreview3.ShowDialog();
					}
					else if (entityType == EntityType.Users)
					{
						userPreview frmItemPreview2 = new userPreview(taskData, sourceMapping, taskMapping);
						frmItemPreview2.StartPosition = FormStartPosition.CenterParent;
						frmItemPreview2.ShowDialog();
					}
					else if (entityType == EntityType.WishList)
					{
						wishListPreview frmItemPreview = new wishListPreview(taskData, sourceMapping, taskMapping);
						frmItemPreview.StartPosition = FormStartPosition.CenterParent;
						frmItemPreview.ShowDialog();
					}
					else
					{
						MessageBox.Show("Test is failed. Please select EntityType");
					}
				}
				else
				{
					MessageBox.Show("Test is failed. Source file is empty");
				}
			}
			catch (Exception ex)
			{
				
				Cursor.Current = Cursors.Default;
				MessageBox.Show(this, ex.Message, "Error");
			}
			Cursor.Current = Cursors.Default;
		}

		// Token: 0x0600015D RID: 349 RVA: 0x00002D75 File Offset: 0x00000F75
		private void taskNewEdit_Shown(object sender, EventArgs e)
		{
			this.tabTask.Visible = true;
			this.tableLayoutPanel4.Visible = true;
			Cursor.Current = Cursors.Arrow;
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x0600015E RID: 350 RVA: 0x00018740 File Offset: 0x00016940
		protected override CreateParams CreateParams
		{
			get
			{
				if (this.originalExStyle == -1)
				{
					this.originalExStyle = base.CreateParams.ExStyle;
				}
				CreateParams createParams = base.CreateParams;
				if (this.enableFormLevelDoubleBuffering)
				{
					createParams.ExStyle |= 33554432;
				}
				else
				{
					createParams.ExStyle = this.originalExStyle;
				}
				return createParams;
			}
		}

		// Token: 0x0600015F RID: 351 RVA: 0x00002D99 File Offset: 0x00000F99
		public void TurnOffFormLevelDoubleBuffering()
		{
			this.enableFormLevelDoubleBuffering = false;
			base.MaximizeBox = true;
		}

		// Token: 0x06000160 RID: 352 RVA: 0x0001879C File Offset: 0x0001699C
		private void TranslationLoad(TaskMapping taskMapping)
		{
			int? languageId = taskMapping.TaskTranslate.LanguageId;
			if (languageId.GetValueOrDefault() > 0 & languageId != null)
			{
				this.ddTranslateLanguagesList.SelectedValue = taskMapping.TaskTranslate.LanguageId;
			}
			this.chTranslateProductName.Checked = taskMapping.TaskTranslate.ProductName;
			this.chTranslateProductShortDesc.Checked = taskMapping.TaskTranslate.ProductShortDesc;
			this.chTranslateProductFullDesc.Checked = taskMapping.TaskTranslate.ProductFullDesc;
			this.chTranslateProductAtt.Checked = taskMapping.TaskTranslate.ProductAttributes;
			this.chTranslateProductCategory.Checked = taskMapping.TaskTranslate.CategoryName;
			this.tbTranslateApiUrl.Text = taskMapping.TaskTranslate.TranslatorApi;
			this.tbTranslateSubscribtionKey.Text = taskMapping.TaskTranslate.SubscribtionKey;
		}

		// Token: 0x06000161 RID: 353 RVA: 0x00018880 File Offset: 0x00016A80
		private void TranslationSave(TaskMapping taskMapping)
		{
			taskMapping.TaskTranslate.LanguageId = new int?(GlobalClass.StringToInteger(this.ddTranslateLanguagesList.SelectedValue, 0));
			taskMapping.TaskTranslate.ProductName = this.chTranslateProductName.Checked;
			taskMapping.TaskTranslate.ProductShortDesc = this.chTranslateProductShortDesc.Checked;
			taskMapping.TaskTranslate.ProductFullDesc = this.chTranslateProductFullDesc.Checked;
			taskMapping.TaskTranslate.ProductAttributes = this.chTranslateProductAtt.Checked;
			taskMapping.TaskTranslate.CategoryName = this.chTranslateProductCategory.Checked;
			taskMapping.TaskTranslate.TranslatorApi = this.tbTranslateApiUrl.Text;
			taskMapping.TaskTranslate.SubscribtionKey = this.tbTranslateSubscribtionKey.Text;
		}

		// Token: 0x06000162 RID: 354 RVA: 0x000024A0 File Offset: 0x000006A0
		private void chGPCustomer_map_CheckedChanged(object sender, EventArgs e)
		{
		}

		// Token: 0x06000163 RID: 355 RVA: 0x00018948 File Offset: 0x00016B48
		private void btnCopy_Click(object sender, EventArgs e)
		{
			new frmCopy(0, 0, 0, this.taskId, this.tbTaskName.Text, this.taskMappingFunc)
			{
				StartPosition = FormStartPosition.CenterParent
			}.ShowDialog();
		}

		// Token: 0x06000164 RID: 356 RVA: 0x00002DA9 File Offset: 0x00000FA9
		private void chCustomerAddress_DeleteOld_CheckedChanged(object sender, EventArgs e)
		{
			this.chCustomerAddress_DeleteOld2.Checked = this.chCustomerAddress_DeleteOld.Checked;
		}

		// Token: 0x06000167 RID: 359 RVA: 0x00002DE6 File Offset: 0x00000FE6
		private void tbTaskName_Validated(object sender, EventArgs e)
		{
			this.errorProvider1.SetError(this.tbTaskName, string.Empty);
		}

		// Token: 0x06000168 RID: 360 RVA: 0x0002C180 File Offset: 0x0002A380
		private void tbTaskName_Validating(object sender, CancelEventArgs e)
		{
			bool cancel = false;
			if (string.IsNullOrEmpty(this.tbTaskName.Text))
			{
				cancel = true;
				this.errorProvider1.SetError(this.tbTaskName, "You must provide task Name!");
				this.tabTask.SelectedTab = this.tabTaskData;
			}
			e.Cancel = cancel;
		}

		// Token: 0x06000169 RID: 361 RVA: 0x00002DFE File Offset: 0x00000FFE
		private void ddStoreWeb_Validated(object sender, EventArgs e)
		{
			this.errorProvider1.SetError(this.ddStoreWeb, string.Empty);
		}

		// Token: 0x0600016A RID: 362 RVA: 0x0002C1D4 File Offset: 0x0002A3D4
		private void ddStoreWeb_Validating(object sender, CancelEventArgs e)
		{
			bool cancel = false;
			if (string.IsNullOrEmpty(this.ddStoreWeb.Text))
			{
				cancel = true;
				this.errorProvider1.SetError(this.ddStoreWeb, "You must select store!");
				this.tabTask.SelectedTab = this.tabTaskData;
			}
			e.Cancel = cancel;
		}

		// Token: 0x0600016B RID: 363 RVA: 0x00002E16 File Offset: 0x00001016
		private void ddVendorSourceMap_Validated(object sender, EventArgs e)
		{
			this.errorProvider1.SetError(this.ddVendorSourceMap, string.Empty);
		}

		// Token: 0x0600016C RID: 364 RVA: 0x0002C228 File Offset: 0x0002A428
		private void ddVendorSourceMap_Validating(object sender, CancelEventArgs e)
		{
			bool cancel = false;
			if (string.IsNullOrEmpty(this.ddVendorSourceMap.Text))
			{
				cancel = true;
				this.errorProvider1.SetError(this.ddVendorSourceMap, "You must select source Mapping!");
				this.tabTask.SelectedTab = this.tabTaskData;
			}
			e.Cancel = cancel;
		}

		// Token: 0x0600016D RID: 365 RVA: 0x00002E2E File Offset: 0x0000102E
		private void ddTaskAction_Validated(object sender, EventArgs e)
		{
			this.errorProvider1.SetError(this.ddTaskAction, string.Empty);
		}

		// Token: 0x0600016E RID: 366 RVA: 0x0002C27C File Offset: 0x0002A47C
		private void ddTaskAction_Validating(object sender, CancelEventArgs e)
		{
			bool cancel = false;
			if (string.IsNullOrEmpty(this.ddTaskAction.Text))
			{
				cancel = true;
				this.errorProvider1.SetError(this.ddTaskAction, "You must select task action!");
				this.tabTask.SelectedTab = this.tabTaskData;
			}
			e.Cancel = cancel;
		}

		// Token: 0x0600016F RID: 367 RVA: 0x00002E46 File Offset: 0x00001046
		private void ddTaskStatus_Validated(object sender, EventArgs e)
		{
			this.errorProvider1.SetError(this.ddTaskStatus, string.Empty);
		}

		// Token: 0x06000170 RID: 368 RVA: 0x0002C2D0 File Offset: 0x0002A4D0
		private void ddTaskStatus_Validating(object sender, CancelEventArgs e)
		{
			bool cancel = false;
			if (string.IsNullOrEmpty(this.ddTaskStatus.Text))
			{
				cancel = true;
				this.errorProvider1.SetError(this.ddTaskStatus, "You must select task status!");
				this.tabTask.SelectedTab = this.tabTaskData;
			}
			e.Cancel = cancel;
		}

		// Token: 0x04000151 RID: 337
		private SourceMappingFunc sourceMappingFunc;

		// Token: 0x04000152 RID: 338
		private TaskMappingFunc taskMappingFunc;

		// Token: 0x04000153 RID: 339
		private TaskManager taskManager;

		// Token: 0x04000154 RID: 340
		private EntityType entityType;

		// Token: 0x04000155 RID: 341
		private frmTasksList _owner;

		// Token: 0x04000156 RID: 342
		private int vendorSourceId;

		// Token: 0x04000157 RID: 343
		private int storeId;

		// Token: 0x04000158 RID: 344
		private string storeConnString;

		// Token: 0x04000159 RID: 345
		private string nopConnString;

		// Token: 0x0400015A RID: 346
		private int taskId;

		// Token: 0x0400015B RID: 347
		private bool loadDefaultDataFromSource;

		// Token: 0x0400015C RID: 348
		private bool formLoaded;

		// Token: 0x0400015D RID: 349
		private bool loadVendorSourceMap;

		// Token: 0x0400015E RID: 350
		private bool connectionErrorShow;

		// Token: 0x0400015F RID: 351
		private int originalExStyle;

		// Token: 0x04000160 RID: 352
		private bool enableFormLevelDoubleBuffering;

		// Token: 0x040002A3 RID: 675
		private DataGridViewNumericUpDownColumn dataGridViewNumericUpDownColumn1;

		// Token: 0x040002A4 RID: 676
		private DataGridViewNumericUpDownColumn dataGridViewNumericUpDownColumn2;
	}
}
