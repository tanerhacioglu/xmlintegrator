﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace NopTalk
{
	public class helper
	{
		public static void FillEncodingDropDownList(ComboBox DropDownName)
		{
			List<EncodingInfo> list2 = (List<EncodingInfo>)(DropDownName.DataSource = (from o in Encoding.GetEncodings()
																					   orderby o.DisplayName
																					   select o).ToList());
			DropDownName.DisplayMember = "DisplayName";
			DropDownName.ValueMember = "CodePage";
		}

		public static void FillDropDownList(string Query, ComboBox DropDownName, string connString)
		{
			DataTable dataTable = new DataTable();
			using (SqlCeConnection sqlCeConnection = new SqlCeConnection(connString))
			{
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(Query, sqlCeConnection))
				{
					sqlCeConnection.Open();
					dataTable.Load(sqlCeCommand.ExecuteReader());
				}
			}
			DropDownName.DataSource = dataTable;
			DropDownName.ValueMember = dataTable.Columns[0].ColumnName;
			DropDownName.DisplayMember = dataTable.Columns[1].ColumnName;
		}

		public static void FillCheckBoxList(string Query, CheckedListBox CheckedListBoxName, string connString)
		{
			DataTable dataTable = new DataTable();
			using (SqlCeConnection sqlCeConnection = new SqlCeConnection(connString))
			{
				using (SqlCeCommand sqlCeCommand = new SqlCeCommand(Query, sqlCeConnection))
				{
					sqlCeConnection.Open();
					dataTable.Load(sqlCeCommand.ExecuteReader());
				}
			}
			CheckedListBoxName.DataSource = dataTable;
			CheckedListBoxName.ValueMember = dataTable.Columns[0].ColumnName;
			CheckedListBoxName.DisplayMember = dataTable.Columns[1].ColumnName;
		}

		public static void FillDropDownList2(string Query, ComboBox DropDownName, string connString, DatabaseType databaseType)
		{
			DataTable dataTable = new DataTable();
			if (databaseType == DatabaseType.MySQL)
			{
				using (MySqlConnection mySqlConnection = new MySqlConnection(connString))
				{
					using (MySqlCommand mySqlCommand = new MySqlCommand(Query.SqlQueryToMySql(), mySqlConnection))
					{
						mySqlCommand.CommandTimeout = 5;
						mySqlConnection.Open();
						dataTable.Load(mySqlCommand.ExecuteReader());
					}
				}
			}
			else
			{
				using (SqlConnection sqlConnection = new SqlConnection(connString))
				{
					using (SqlCommand sqlCommand = new SqlCommand(Query, sqlConnection))
					{
						sqlCommand.CommandTimeout = 5;
						sqlConnection.Open();
						dataTable.Load(sqlCommand.ExecuteReader());
					}
				}
			}
			DropDownName.DataSource = dataTable;
			DropDownName.ValueMember = dataTable.Columns[0].ColumnName;
			DropDownName.DisplayMember = dataTable.Columns[1].ColumnName;
		}

		public static void FillCheckBoxList2(string Query, CheckedListBox CheckedListBoxName, string connString, DatabaseType databaseType)
		{
			DataTable dataTable = new DataTable();
			if (databaseType == DatabaseType.MySQL)
			{
				using (MySqlConnection mySqlConnection = new MySqlConnection(connString))
				{
					using (MySqlCommand mySqlCommand = new MySqlCommand(Query.SqlQueryToMySql(), mySqlConnection))
					{
						mySqlCommand.CommandTimeout = 5;
						mySqlConnection.Open();
						dataTable.Load(mySqlCommand.ExecuteReader());
					}
				}
			}
			else
			{
				using (SqlConnection sqlConnection = new SqlConnection(connString))
				{
					using (SqlCommand sqlCommand = new SqlCommand(Query, sqlConnection))
					{
						sqlCommand.CommandTimeout = 5;
						sqlConnection.Open();
						dataTable.Load(sqlCommand.ExecuteReader());
					}
				}
			}
			if (dataTable != null && dataTable.Rows.Count > 0 && CheckedListBoxName != null)
			{
				CheckedListBoxName.DataSource = dataTable;
				CheckedListBoxName.ValueMember = dataTable.Columns[0].ColumnName;
				CheckedListBoxName.DisplayMember = dataTable.Columns[1].ColumnName;
			}
		}

		public static SqlCommand GetSqlUpdateCommandMSSQL(string tableName, SqlConnection conn, List<string> colomnList)
		{
			List<string> list = new List<string>();
			List<string> list2 = new List<string>();
			foreach (string colomn in colomnList)
			{
				if (!string.IsNullOrEmpty(colomn) && colomn.ToLower() != "id" && colomn.ToLower() != "updatedonutc" && colomn.ToLower() != "createdonutc")
				{
					if (colomn.IndexOf("price", StringComparison.OrdinalIgnoreCase) >= 0)
					{
						list.Add($"[{colomn}] = CONVERT(DECIMAL(18, 4), REPLACE(@{colomn}, ',', '.'))");
					}
					else
					{
						list.Add($"[{colomn}] = @{colomn}");
					}
					list2.Add(colomn);
				}
			}
			string arg = string.Join(", ", list.ToArray());
			SqlCommand sqlCommand = new SqlCommand($"UPDATE {tableName} SET {arg}, [UpdatedOnUtc] = getdate() WHERE Id = @Id", conn);
			foreach (string item in list2)
			{
				sqlCommand.Parameters.Add(new SqlParameter("@" + item, SqlDbType.NVarChar, 4000, item));
			}
			SqlParameter sqlParameter = sqlCommand.Parameters.Add("@Id", SqlDbType.Int, 4, "Id");
			sqlParameter.SourceVersion = DataRowVersion.Original;
			return sqlCommand;
		}

		public static MySqlCommand GetMySqlUpdateCommandMSSQL(string tableName, MySqlConnection conn, List<string> colomnList)
		{
			List<string> list = new List<string>();
			List<string> list2 = new List<string>();
			foreach (string colomn in colomnList)
			{
				if (!string.IsNullOrEmpty(colomn) && colomn.ToLower() != "id" && colomn.ToLower() != "updatedonutc" && colomn.ToLower() != "createdonutc")
				{
					if (colomn.IndexOf("price", StringComparison.OrdinalIgnoreCase) >= 0)
					{
						list.Add($"[{colomn}] = CAST(REPLACE(@{colomn}, ',', '.') AS DECIMAL(18, 4))");
					}
					else
					{
						list.Add($"[{colomn}] = REPLACE(REPLACE(@{colomn}, 'False', 0), 'True', 1)");
					}
					list2.Add(colomn);
				}
			}
			string arg = string.Join(", ", list.ToArray());
			MySqlCommand mySqlCommand = new MySqlCommand($"UPDATE {tableName} SET {arg}, [UpdatedOnUtc] = NOW() WHERE Id = @Id".SqlQueryToMySql(), conn);
			foreach (string item in list2)
			{
				mySqlCommand.Parameters.Add(new MySqlParameter("@" + item, MySqlDbType.String, 4000, item));
			}
			MySqlParameter mySqlParameter = mySqlCommand.Parameters.Add("@Id", MySqlDbType.Int32, 4, "Id");
			mySqlParameter.SourceVersion = DataRowVersion.Original;
			return mySqlCommand;
		}

		public static MySqlCommand GetSqlUpdateCommandMySQL(string tableName, MySqlConnection conn, List<string> colomnList)
		{
			List<string> list = new List<string>();
			List<string> list2 = new List<string>();
			foreach (string colomn in colomnList)
			{
				if (!string.IsNullOrEmpty(colomn) && colomn.ToLower() != "id" && colomn.ToLower() != "updatedonutc" && colomn.ToLower() != "createdonutc")
				{
					if (colomn.IndexOf("price", StringComparison.OrdinalIgnoreCase) >= 0)
					{
						list.Add($"[{colomn}] = CONVERT(DECIMAL(18, 4), REPLACE(@{colomn}, ',', '.'))");
					}
					else
					{
						list.Add($"[{colomn}] = @{colomn}");
					}
					list2.Add(colomn);
				}
			}
			string arg = string.Join(", ", list.ToArray());
			MySqlCommand mySqlCommand = new MySqlCommand($"UPDATE {tableName} SET {arg}, [UpdatedOnUtc] = getdate() WHERE Id = @Id", conn);
			foreach (string item in list2)
			{
				mySqlCommand.Parameters.Add(new MySqlParameter("@" + item, MySqlDbType.String, 4000, item));
			}
			MySqlParameter mySqlParameter = mySqlCommand.Parameters.Add("@Id", MySqlDbType.Int32, 4, "Id");
			mySqlParameter.SourceVersion = DataRowVersion.Original;
			return mySqlCommand;
		}

	
	}
}