﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using NopTalkCore;

namespace NopTalk
{
	public partial class manufacturerEdit : Form
	{
		public manufacturerEdit()
		{
		
			this.databaseType = DatabaseType.MSSQL;
			this.formLoaded = false;
			this.bs = new BindingSource();
			this.sqlDp = new SqlDataAdapter();
			this.mySqlDp = new MySqlDataAdapter();
			this.components = null;
			this.InitializeComponent();
		}

		private async void manufacturerEdit_Load(object sender, EventArgs e)
		{
			this.InitialData();
			
		}

		private void ddStoresConn_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.ddStoresConn.SelectedIndex > -1)
			{
				if (this.ddStoresConn.SelectedIndex > -1 && GlobalClass.IsInteger(this.ddStoresConn.SelectedValue) && this.formLoaded)
				{
					StoreData storeData = StoreFunc.GetStoreData(Convert.ToInt64(this.ddStoresConn.SelectedValue));
					this.databaseType = storeData.DatabaseType;
				}
				this.LoadStoreData(null, null, this.clbColumnsList, null, true, new EditorQueryType?(EditorQueryType.ManufacturerTableSchema));
				this.FilterEnable(true);
			}
			else
			{
				this.FilterEnable(false);
			}
		}

		private void InitialData()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			string dataConnectionString = dataSettingsManager.LoadSettings(null).DataConnectionString;
			helper.FillDropDownList("select Id, Name From EShop", this.ddStoresConn, dataConnectionString);
			this.ddStoresConn.SelectedIndex = -1;
			this.ddItemsLimit.SelectedIndex = 0;
			this.bs.ListChanged += new ListChangedEventHandler(this.ListChanged);
			this.dgDataEditor.CellBeginEdit += this.dgv_CellBeginEdit;
			this.dgDataEditor.CellEndEdit += this.dgv_CellEndEdit;
			this.FilterEnable(false);
			this.formLoaded = true;
		}

		private void dgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (this.dgDataEditor[e.ColumnIndex, e.RowIndex].Value != this.previousValue)
			{
				this.dgDataEditor[e.ColumnIndex, e.RowIndex].Style.BackColor = ColorTranslator.FromHtml("#F19FB9");
				this.SetStatusArea(false, "Data changed. To save click button Save.", true);
			}
		}

		private void dgv_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			this.previousValue = this.dgDataEditor[e.ColumnIndex, e.RowIndex].Value;
		}

		private void ListChanged(object sender, EventArgs e)
		{
			BindingSource bindingSource = this.bs;
			int? num;
			if (bindingSource == null)
			{
				num = null;
			}
			else
			{
				IList list = bindingSource.List;
				num = ((list != null) ? new int?(list.Count) : null);
			}
			int? num2 = num;
			int valueOrDefault = num2.GetValueOrDefault();
			this.lblStatistic.Text = string.Format("Items Filtered: {0}", valueOrDefault);
		}

		private void FilterEnable(bool enable)
		{
			this.clbColumnsList.Enabled = enable;
			this.tbSearch_Name.Enabled = enable;
			this.chPublished.Enabled = enable;
			this.btnFilter.Enabled = enable;
			this.btnFilterClear.Enabled = enable;
			this.btnSave.Enabled = enable;
			this.chPublished.Enabled = enable;
		}

		private void LoadManufacturersMSSQL(string sqlQuery, string storeConnString)
		{
			this.dgDataEditor.AutoGenerateColumns = true;
			SqlConnection sqlConnection = new SqlConnection(storeConnString);
			this.sqlDp.SelectCommand = new SqlCommand(sqlQuery, sqlConnection);
			List<string> list = new List<string>();
			for (int i = 0; i <= this.clbColumnsList.CheckedItems.Count - 1; i++)
			{
				string text = (this.clbColumnsList.CheckedItems[i] as DataRowView)[0].ToString();
				if (!string.IsNullOrEmpty(text) && text.ToLower() != "id" && text.ToLower() != "updatedonutc" && text.ToLower() != "createdonutc")
				{
					list.Add(text);
				}
			}
			DataTable dataTable = new DataTable();
			this.sqlDp.Fill(dataTable);
			this.sqlDp.UpdateCommand = helper.GetSqlUpdateCommandMSSQL("Manufacturer", sqlConnection, list);
			this.bs.DataSource = dataTable;
			this.dgDataEditor.DataSource = this.bs;
			this.dgDataEditor.BorderStyle = BorderStyle.FixedSingle;
			this.dgDataEditor.AllowUserToAddRows = false;
			this.dgDataEditor.AllowUserToDeleteRows = false;
			if (this.dgDataEditor.Columns.Contains("Id"))
			{
				this.dgDataEditor.Columns["Id"].ReadOnly = true;
				this.dgDataEditor.Columns["Id"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("Name"))
			{
				this.dgDataEditor.Columns["Name"].FillWeight = 10f;
				this.dgDataEditor.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			}
			if (this.dgDataEditor.Columns.Contains("Description"))
			{
				this.dgDataEditor.Columns["Description"].FillWeight = 5f;
				this.dgDataEditor.Columns["Description"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			}
			if (this.dgDataEditor.Columns.Contains("AllowCustomersToSelectPageSize"))
			{
				this.dgDataEditor.Columns["AllowCustomersToSelectPageSize"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("PageSize"))
			{
				this.dgDataEditor.Columns["PageSize"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("Published"))
			{
				this.dgDataEditor.Columns["Published"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("DisplayOrder"))
			{
				this.dgDataEditor.Columns["DisplayOrder"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("CreatedOnUtc"))
			{
				this.dgDataEditor.Columns["CreatedOnUtc"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("UpdatedOnUtc"))
			{
				this.dgDataEditor.Columns["UpdatedOnUtc"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("CreatedOnUtc"))
			{
				this.dgDataEditor.Columns["CreatedOnUtc"].ReadOnly = true;
			}
			if (this.dgDataEditor.Columns.Contains("UpdatedOnUtc"))
			{
				this.dgDataEditor.Columns["UpdatedOnUtc"].ReadOnly = true;
			}
		}

		private void LoadManufacturersMySQL(string sqlQuery, string storeConnString)
		{
			this.dgDataEditor.AutoGenerateColumns = true;
			MySqlConnection mySqlConnection = new MySqlConnection(storeConnString);
			this.mySqlDp.SelectCommand = new MySqlCommand(sqlQuery.SqlQueryToMySql<string>(), mySqlConnection);
			List<string> list = new List<string>();
			for (int i = 0; i <= this.clbColumnsList.CheckedItems.Count - 1; i++)
			{
				string text = (this.clbColumnsList.CheckedItems[i] as DataRowView)[0].ToString();
				if (!string.IsNullOrEmpty(text) && text.ToLower() != "id" && text.ToLower() != "updatedonutc" && text.ToLower() != "createdonutc")
				{
					list.Add(text);
				}
			}
			DataTable dataTable = new DataTable();
			this.mySqlDp.Fill(dataTable);
			this.mySqlDp.UpdateCommand = helper.GetMySqlUpdateCommandMSSQL("Manufacturer", mySqlConnection, list);
			this.bs.DataSource = dataTable;
			this.dgDataEditor.DataSource = this.bs;
			this.dgDataEditor.BorderStyle = BorderStyle.FixedSingle;
			this.dgDataEditor.AllowUserToAddRows = false;
			this.dgDataEditor.AllowUserToDeleteRows = false;
			if (this.dgDataEditor.Columns.Contains("Id"))
			{
				this.dgDataEditor.Columns["Id"].ReadOnly = true;
				this.dgDataEditor.Columns["Id"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("Name"))
			{
				this.dgDataEditor.Columns["Name"].FillWeight = 10f;
				this.dgDataEditor.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			}
			if (this.dgDataEditor.Columns.Contains("Description"))
			{
				this.dgDataEditor.Columns["Description"].FillWeight = 5f;
				this.dgDataEditor.Columns["Description"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			}
			if (this.dgDataEditor.Columns.Contains("AllowCustomersToSelectPageSize"))
			{
				this.dgDataEditor.Columns["AllowCustomersToSelectPageSize"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("PageSize"))
			{
				this.dgDataEditor.Columns["PageSize"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("Published"))
			{
				this.dgDataEditor.Columns["Published"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("DisplayOrder"))
			{
				this.dgDataEditor.Columns["DisplayOrder"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("CreatedOnUtc"))
			{
				this.dgDataEditor.Columns["CreatedOnUtc"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("UpdatedOnUtc"))
			{
				this.dgDataEditor.Columns["UpdatedOnUtc"].Width = 75;
			}
			if (this.dgDataEditor.Columns.Contains("CreatedOnUtc"))
			{
				this.dgDataEditor.Columns["CreatedOnUtc"].ReadOnly = true;
			}
			if (this.dgDataEditor.Columns.Contains("UpdatedOnUtc"))
			{
				this.dgDataEditor.Columns["UpdatedOnUtc"].ReadOnly = true;
			}
		}

		private void btnFilter_Click(object sender, EventArgs e)
		{
			this.LoadItems();
			this.SetStatusArea(false, "", true);
		}

		private void LoadItems()
		{
			List<string> list = new List<string>();
			for (int i = 0; i <= this.clbColumnsList.CheckedItems.Count - 1; i++)
			{
				string item = (this.clbColumnsList.CheckedItems[i] as DataRowView)[0].ToString();
				list.Add(item);
			}
			string text = string.Join(", ", list.ToArray());
			string text2 = string.Format("SELECT DISTINCT {0} ", text);
			if (this.databaseType == DatabaseType.MSSQL)
			{
				text2 = string.Format("SELECT DISTINCT TOP {0} {1} ", this.ddItemsLimit.Text, text);
			}
			text2 += string.Format(" FROM Manufacturer", new object[0]);
			text2 += string.Format(" WHERE (1=1) ", new object[0]);
			if (this.tbSearch_Name.Text.Trim().Length > 0)
			{
				text2 += string.Format("AND Name like '%{0}%'", this.tbSearch_Name.Text.Trim());
			}
			if (this.chPublished.Checked)
			{
				text2 += string.Format("AND Published = 1", new object[0]);
			}
			if (this.databaseType == DatabaseType.MySQL)
			{
				text2 += string.Format("LIMIT {0}", this.ddItemsLimit.Text);
			}
			this.LoadStoreData(text2, null, null, this.dgDataEditor, false, null);
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				if (ImportManager.productsLimits > 0)
				{
					MessageBox.Show("This functionality is not working in free version");
				}
				else
				{
					if (this.databaseType == DatabaseType.MySQL)
					{
						this.mySqlDp.Update((DataTable)this.bs.DataSource);
					}
					else
					{
						this.sqlDp.Update((DataTable)this.bs.DataSource);
					}
					this.SetStatusArea(true, "Data saved successfully.", false);
					this.LoadItems();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
				this.SetStatusArea(false, "Data not saved.", false);
			}
		}

		private void SetStatusArea(bool valid = false, string message = null, bool info = false)
		{
			if (string.IsNullOrEmpty(message))
			{
				this.statusArea.Hide();
			}
			else
			{
				this.statusArea.Show();
				this.statusAreaText.Text = message;
				if (valid)
				{
					this.statusArea.BackColor = ColorTranslator.FromHtml("#009D00");
				}
				else
				{
					this.statusArea.BackColor = ColorTranslator.FromHtml("#C00000");
				}
				if (info)
				{
					this.statusArea.BackColor = ColorTranslator.FromHtml("#C07464");
				}
				this.statusAreaText.ForeColor = Color.White;
			}
		}

		private void btnFilterClear_Click(object sender, EventArgs e)
		{
			this.ddStoresConn.SelectedIndex = -1;
			this.dgDataEditor.DataSource = null;
			this.tbSearch_Name.Text = "";
		}

		private void LoadStoreData(string sqlQuery, ComboBox control, CheckedListBox control2, DataGridView control3, bool loadEditorDefaultColumns = false, EditorQueryType? editorQueryType = null)
		{
			if (this.ddStoresConn.SelectedIndex > -1 && GlobalClass.IsInteger(this.ddStoresConn.SelectedValue) && this.formLoaded)
			{
				try
				{
					if (this.databaseType == DatabaseType.MSSQL)
					{
						EditorQueryType? editorQueryType2 = editorQueryType;
						if (editorQueryType2.GetValueOrDefault() == EditorQueryType.ManufacturerTableSchema & editorQueryType2 != null)
						{
							sqlQuery = "SELECT Distinct Name as Id, Name as Name FROM sys.columns WHERE object_id = OBJECT_ID('Manufacturer') ";
						}
					}
					if (this.databaseType == DatabaseType.MySQL)
					{
						EditorQueryType? editorQueryType2 = editorQueryType;
						if (editorQueryType2.GetValueOrDefault() == EditorQueryType.ManufacturerTableSchema & editorQueryType2 != null)
						{
							sqlQuery = "SELECT Distinct COLUMN_NAME as Id, COLUMN_NAME as Name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME LIKE 'Manufacturer' ";
						}
					}
					StoreData storeData = StoreFunc.GetStoreData(Convert.ToInt64(this.ddStoresConn.SelectedValue));
					string databaseConnString = storeData.DatabaseConnString;
					if (control != null)
					{
						helper.FillDropDownList2(sqlQuery, control, databaseConnString, storeData.DatabaseType);
					}
					if (control2 != null)
					{
						helper.FillCheckBoxList2(sqlQuery, control2, databaseConnString, storeData.DatabaseType);
					}
					if (control3 != null)
					{
						if (this.databaseType == DatabaseType.MySQL)
						{
							this.LoadManufacturersMySQL(sqlQuery, databaseConnString);
						}
						else
						{
							this.LoadManufacturersMSSQL(sqlQuery, databaseConnString);
						}
					}
					if (loadEditorDefaultColumns)
					{
						EditorConfigData editorConfigData = storeData.EditorConfigData;
						if (((editorConfigData != null) ? editorConfigData.ManufacturerColumns : null) == null)
						{
							storeData.EditorConfigData = new EditorConfigData();
						}
						if (!storeData.EditorConfigData.ManufacturerColumns.Any<string>())
						{
							storeData.EditorConfigData.ManufacturerColumns.Add("Id");
							storeData.EditorConfigData.ManufacturerColumns.Add("Name");
							storeData.EditorConfigData.ManufacturerColumns.Add("Description");
							storeData.EditorConfigData.ManufacturerColumns.Add("PageSize");
							storeData.EditorConfigData.ManufacturerColumns.Add("AllowCustomersToSelectPageSize");
							storeData.EditorConfigData.ManufacturerColumns.Add("Published");
							storeData.EditorConfigData.ManufacturerColumns.Add("DisplayOrder");
							storeData.EditorConfigData.ManufacturerColumns.Add("CreatedOnUtc");
							storeData.EditorConfigData.ManufacturerColumns.Add("UpdatedOnUtc");
						}
						for (int i = 0; i <= control2.Items.Count - 1; i++)
						{
							string value = (control2.Items[i] as DataRowView)[0].ToString();
							if (storeData.EditorConfigData.ManufacturerColumns.Exists((string x) => x == value))
							{
								control2.SetItemChecked(i, true);
							}
						}
					}
					return;
				}
				catch (Exception ex)
				{
					MessageBox.Show(this, "Need provide the correct connection string to the nopCommerce store. Go to->settings->stores. Error: " + ex.Message, "Error");
					return;
				}
			}
			if (control != null)
			{
				control.DataSource = null;
			}
			if (control2 != null)
			{
				control2.DataSource = null;
			}
		}

		private DatabaseType databaseType;

		private bool formLoaded;

		private BindingSource bs;

		private SqlDataAdapter sqlDp;

		private MySqlDataAdapter mySqlDp;

		private object previousValue;
	}
}
