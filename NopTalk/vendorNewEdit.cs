﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlServerCe;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using NopTalk.Helpers;
using NopTalkCore;

namespace NopTalk
{
	public partial class vendorNewEdit : Form
	{
		private sourceMappingDataGrid sourceMappingDgCustomerOthers { get; set; }

		private void CustomerOthersTabLoad()
		{
			if (!this.tabSourceConfig.Contains(this.tabCustomerOthersMapping))
			{
				this.tabSourceConfig.TabPages.Add(this.tabCustomerOthersMapping);
			}
		}

		private void CustomerOthersListLoad(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			this.tabCustomerOthersMapping.Controls.Clear();
			this.sourceMappingDgCustomerOthers = new sourceMappingDataGrid(SourceMappingType.CustomerOthers, structureFormat);
			this.CustomerOthersConvertOldMappingToNew(sourceMapping, structureFormat, false);
			this.sourceMappingDgCustomerOthers.SourceMappingListLoad(sourceMapping, this._sourceFieldList);
			this.sourceMappingDgCustomerOthers.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.sourceMappingDgCustomerOthers.Dock = DockStyle.Fill;
			this.tabCustomerOthersMapping.Controls.Add(this.sourceMappingDgCustomerOthers);
		}

		private void CustomerOthersListSave(SourceMapping sourceMapping)
		{
			if (this.sourceMappingDgCustomerOthers != null)
			{
				this.sourceMappingDgCustomerOthers.SourceMappingListSave(sourceMapping);
			}
		}

		private void CustomerOthersConvertOldMappingToNew(SourceMapping sourceMapping, StructureFormat structureFormat, bool force)
		{
			SourceCustomerOthersMapping sourceCustomerOthersMapping = sourceMapping.SourceCustomerOthersMapping;
			if (((sourceCustomerOthersMapping != null) ? sourceCustomerOthersMapping.Items : null) == null || !sourceMapping.SourceCustomerOthersMapping.Items.Any<SourceItemMapping>() || force)
			{
				this.CustomerOthersConvertOldMappingItemToNew(sourceMapping, structureFormat);
				SourceCustomerOthersMapping sourceCustomerOthersMapping2 = sourceMapping.SourceCustomerOthersMapping;
				if (((sourceCustomerOthersMapping2 != null) ? sourceCustomerOthersMapping2.Items : null) != null && sourceMapping.SourceCustomerOthersMapping.Items.Any<SourceItemMapping>())
				{
					this.sourceMappingFunc.UpdateSourceMapping(sourceMapping);
				}
			}
		}

		private void CustomerOthersConvertOldMappingItemToNew(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			list.AddRange(SourceMappingHelper.GetCustomerOtherNewItem());
			if (structureFormat == StructureFormat.XML)
			{
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 261).FieldMappingForXml = sourceMapping.SourceUserMapping.Address3Map;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 261).FieldRule = sourceMapping.SourceUserMapping.Address3Val;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 262).FieldMappingForXml = sourceMapping.SourceUserMapping.AddressNameMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 262).FieldRule = sourceMapping.SourceUserMapping.AddressNameVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 264).FieldMappingForXml = sourceMapping.SourceUserMapping.GPCustomerMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 264).FieldRule = sourceMapping.SourceUserMapping.GPCustomerVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 266).FieldMappingForXml = sourceMapping.SourceUserMapping.MainAddressMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 266).FieldRule = sourceMapping.SourceUserMapping.MainAddressVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 263).FieldMappingForXml = sourceMapping.SourceUserMapping.SAPCustomerMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 263).FieldRule = sourceMapping.SourceUserMapping.SAPCustomerVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 265).FieldMappingForXml = sourceMapping.SourceUserMapping.AddressSAPCustomerMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 265).FieldRule = sourceMapping.SourceUserMapping.AddressSAPCustomerVal;
			}
			if (structureFormat == StructureFormat.Position)
			{
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 261).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.Address3Map, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 261).FieldRule = sourceMapping.SourceUserMapping.Address3Val;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 262).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.AddressNameMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 262).FieldRule = sourceMapping.SourceUserMapping.AddressNameVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 264).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.GPCustomerMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 264).FieldRule = sourceMapping.SourceUserMapping.GPCustomerVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 266).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.MainAddressMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 266).FieldRule = sourceMapping.SourceUserMapping.MainAddressVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 263).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.SAPCustomerMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 263).FieldRule = sourceMapping.SourceUserMapping.SAPCustomerVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 265).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.AddressSAPCustomerMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 265).FieldRule = sourceMapping.SourceUserMapping.AddressSAPCustomerVal;
			}
			sourceMapping.SourceCustomerOthersMapping.Items.AddRange(list);
		}

		private sourceMappingDataGrid sourceMappingDgCustomerRole { get; set; }

		private void CustomerRoleTabLoad()
		{
			if (!this.tabSourceConfig.Contains(this.tabCustomerRoleMapping))
			{
				this.tabSourceConfig.TabPages.Add(this.tabCustomerRoleMapping);
			}
		}

		private void CustomerRoleListLoad(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			this.tabCustomerRoleMapping.Controls.Clear();
			this.sourceMappingDgCustomerRole = new sourceMappingDataGrid(SourceMappingType.CustomerRole, structureFormat);
			this.CustomerRoleConvertOldMappingToNew(sourceMapping, structureFormat);
			this.sourceMappingDgCustomerRole.SourceMappingListLoad(sourceMapping, this._sourceFieldList);
			this.sourceMappingDgCustomerRole.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.sourceMappingDgCustomerRole.Dock = DockStyle.Fill;
			this.tabCustomerRoleMapping.Controls.Add(this.sourceMappingDgCustomerRole);
		}

		private void CustomerRoleListSave(SourceMapping sourceMapping)
		{
			if (this.sourceMappingDgCustomerRole != null)
			{
				this.sourceMappingDgCustomerRole.SourceMappingListSave(sourceMapping);
			}
		}

		private void CustomerRoleConvertOldMappingToNew(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			SourceCustomerRoleMapping sourceCustomerRoleMapping = sourceMapping.SourceCustomerRoleMapping;
			if (((sourceCustomerRoleMapping != null) ? sourceCustomerRoleMapping.Items : null) == null || !sourceMapping.SourceCustomerRoleMapping.Items.Any<SourceItemMapping>())
			{
				this.CustomerRoleConvertOldMappingItemToNew(sourceMapping, structureFormat, sourceMapping.CustomerRoleMap, sourceMapping.CustomerRoleMap, sourceMapping.CustomerRoleVal, GlobalClass.ObjectToString(sourceMapping.CustomerRoleDelimeter), true);
				this.CustomerRoleConvertOldMappingItemToNew(sourceMapping, structureFormat, null, sourceMapping.CustomerRole1Map, sourceMapping.CustomerRole1Val, null, false);
				this.CustomerRoleConvertOldMappingItemToNew(sourceMapping, structureFormat, null, sourceMapping.CustomerRole2Map, sourceMapping.CustomerRole2Val, null, false);
				this.CustomerRoleConvertOldMappingItemToNew(sourceMapping, structureFormat, null, sourceMapping.CustomerRole3Map, sourceMapping.CustomerRole3Val, null, false);
				this.CustomerRoleConvertOldMappingItemToNew(sourceMapping, structureFormat, null, sourceMapping.CustomerRole4Map, sourceMapping.CustomerRole4Val, null, false);
				SourceCustomerRoleMapping sourceCustomerRoleMapping2 = sourceMapping.SourceCustomerRoleMapping;
				if (((sourceCustomerRoleMapping2 != null) ? sourceCustomerRoleMapping2.Items : null) != null && sourceMapping.SourceCustomerRoleMapping.Items.Any<SourceItemMapping>())
				{
					sourceMapping.CustomerRoleMap = null;
					sourceMapping.CustomerRoleVal = null;
					sourceMapping.CustomerRoleDelimeter = null;
					sourceMapping.CustomerRole1Map = null;
					sourceMapping.CustomerRole1Val = null;
					sourceMapping.CustomerRole2Map = null;
					sourceMapping.CustomerRole2Val = null;
					sourceMapping.CustomerRole3Map = null;
					sourceMapping.CustomerRole3Val = null;
					sourceMapping.CustomerRole4Map = null;
					sourceMapping.CustomerRole4Val = null;
					this.sourceMappingFunc.UpdateSourceMapping(sourceMapping);
				}
			}
		}

		private void CustomerRoleConvertOldMappingItemToNew(SourceMapping sourceMapping, StructureFormat structureFormat, string repeatNodeMap, string roleMap, string roleVal, string delimeter, bool needrepeatnode)
		{
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			if (!string.IsNullOrEmpty(roleMap) || !string.IsNullOrEmpty(roleVal))
			{
				if ((list == null || !list.Any<SourceItemMapping>()) && structureFormat == StructureFormat.XML && needrepeatnode)
				{
					list.AddRange(SourceMappingHelper.GetRepeatNode());
				}
				list.AddRange(SourceMappingHelper.GetCustomerRoleNewItem());
				if (structureFormat == StructureFormat.XML)
				{
					if (needrepeatnode)
					{
						list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1).FieldMappingForXml = repeatNodeMap;
					}
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 251).FieldMappingForXml = roleMap;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 251).FieldRule = roleVal;
					if (!string.IsNullOrEmpty(delimeter))
					{
						list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 251).FieldRule1 = delimeter;
					}
				}
				if (structureFormat == StructureFormat.Position)
				{
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 251).FieldMappingForIndex = GlobalClass.StringToInteger(roleMap, -1);
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 251).FieldRule = roleVal;
					if (!string.IsNullOrEmpty(delimeter))
					{
						list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 251).FieldRule1 = delimeter;
					}
				}
				sourceMapping.SourceCustomerRoleMapping.Items.AddRange(list);
			}
		}

		private sourceMappingDataGrid sourceMappingDgAddressCustomAtt { get; set; }

		private void AddressCustomAttributesTabLoad()
		{
			if (!this.tabSourceConfig.Contains(this.tabCustGenAttAttMapping))
			{
				this.tabSourceConfig.TabPages.Add(this.tabCustGenAttAttMapping);
			}
		}

		private void AddressCustomAttributesListLoad(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			this.tabCustGenAttAttMapping.Controls.Clear();
			this.sourceMappingDgAddressCustomAtt = new sourceMappingDataGrid(SourceMappingType.AddressCustomAttributtes, structureFormat);
			this.sourceMappingDgAddressCustomAtt.SourceMappingListLoad(sourceMapping, this._sourceFieldList);
			this.sourceMappingDgAddressCustomAtt.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.sourceMappingDgAddressCustomAtt.Dock = DockStyle.Fill;
			this.tabCustGenAttAttMapping.Controls.Add(this.sourceMappingDgAddressCustomAtt);
		}

		private void AddressCustomAttributesListSave(SourceMapping sourceMapping)
		{
			if (this.sourceMappingDgAddressCustomAtt != null)
			{
				this.sourceMappingDgAddressCustomAtt.SourceMappingListSave(sourceMapping);
			}
		}

		private sourceMappingDataGrid sourceMappingDgCustomerShippingAddress { get; set; }

		private sourceMappingDataGrid sourceMappingDgCustomerBillingAddress { get; set; }

		private void CustomerAddressTabLoad()
		{
			if (!this.tabSourceConfig.Contains(this.tabCustomerShippingAddressMapping))
			{
				this.tabSourceConfig.TabPages.Add(this.tabCustomerShippingAddressMapping);
			}
			if (!this.tabSourceConfig.Contains(this.tabCustomerBillingAddressMapping))
			{
				this.tabSourceConfig.TabPages.Add(this.tabCustomerBillingAddressMapping);
			}
		}

		private void CustomerAddressListLoad(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			this.tabCustomerShippingAddressMapping.Controls.Clear();
			this.sourceMappingDgCustomerShippingAddress = new sourceMappingDataGrid(SourceMappingType.ShippingAddress, structureFormat);
			this.CustomerAddressConvertOldMappingToNew(sourceMapping, structureFormat, false);
			this.sourceMappingDgCustomerShippingAddress.SourceMappingListLoad(sourceMapping, this._sourceFieldList);
			this.sourceMappingDgCustomerShippingAddress.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.sourceMappingDgCustomerShippingAddress.Dock = DockStyle.Fill;
			this.tabCustomerShippingAddressMapping.Controls.Add(this.sourceMappingDgCustomerShippingAddress);
			this.tabCustomerBillingAddressMapping.Controls.Clear();
			this.sourceMappingDgCustomerBillingAddress = new sourceMappingDataGrid(SourceMappingType.BillingAddress, structureFormat);
			this.CustomerAddressConvertOldMappingToNew(sourceMapping, structureFormat, false);
			this.sourceMappingDgCustomerBillingAddress.SourceMappingListLoad(sourceMapping, this._sourceFieldList);
			this.sourceMappingDgCustomerBillingAddress.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.sourceMappingDgCustomerBillingAddress.Dock = DockStyle.Fill;
			this.tabCustomerBillingAddressMapping.Controls.Add(this.sourceMappingDgCustomerBillingAddress);
		}

		private void CustomerAddressListSave(SourceMapping sourceMapping)
		{
			if (this.sourceMappingDgCustomerShippingAddress != null)
			{
				this.sourceMappingDgCustomerShippingAddress.SourceMappingListSave(sourceMapping);
			}
			if (this.sourceMappingDgCustomerBillingAddress != null)
			{
				this.sourceMappingDgCustomerBillingAddress.SourceMappingListSave(sourceMapping);
			}
		}

		private void CustomerAddressConvertOldMappingToNew(SourceMapping sourceMapping, StructureFormat structureFormat, bool force)
		{
			SourceCustomerAddressMapping sourceCustomerShippingAddressMapping = sourceMapping.SourceCustomerShippingAddressMapping;
			if (((sourceCustomerShippingAddressMapping != null) ? sourceCustomerShippingAddressMapping.Items : null) == null || !sourceMapping.SourceCustomerShippingAddressMapping.Items.Any<SourceItemMapping>() || force)
			{
				this.CustomerAddressConvertOldMappingItemToNew(sourceMapping, structureFormat);
				SourceCustomerAddressMapping sourceCustomerShippingAddressMapping2 = sourceMapping.SourceCustomerShippingAddressMapping;
				if (((sourceCustomerShippingAddressMapping2 != null) ? sourceCustomerShippingAddressMapping2.Items : null) != null && sourceMapping.SourceCustomerShippingAddressMapping.Items.Any<SourceItemMapping>())
				{
					this.sourceMappingFunc.UpdateSourceMapping(sourceMapping);
				}
			}
		}

		private void CustomerAddressConvertOldMappingItemToNew(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			if (structureFormat == StructureFormat.XML)
			{
				list.AddRange(SourceMappingHelper.GetRepeatNode());
			}
			list.AddRange(SourceMappingHelper.GetCustomerAddressNewItem());
			if (structureFormat == StructureFormat.XML)
			{
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1).FieldMappingForXml = sourceMapping.SourceUserMapping.RepeatNode;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 233).FieldMappingForXml = sourceMapping.SourceUserMapping.UserEmailMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 233).FieldRule = sourceMapping.SourceUserMapping.UserEmailVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 231).FieldMappingForXml = sourceMapping.SourceUserMapping.FirstNameMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 231).FieldRule = sourceMapping.SourceUserMapping.FirstNameVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 232).FieldMappingForXml = sourceMapping.SourceUserMapping.LastNameMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 232).FieldRule = sourceMapping.SourceUserMapping.LastNameVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 234).FieldMappingForXml = sourceMapping.SourceUserMapping.CompanyMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 234).FieldRule = sourceMapping.SourceUserMapping.CompanyVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 235).FieldMappingForXml = sourceMapping.SourceUserMapping.CountryMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 235).FieldRule = sourceMapping.SourceUserMapping.CountryVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 236).FieldMappingForXml = sourceMapping.SourceUserMapping.StateMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 236).FieldRule = sourceMapping.SourceUserMapping.StateVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 237).FieldMappingForXml = sourceMapping.SourceUserMapping.CityMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 237).FieldRule = sourceMapping.SourceUserMapping.CityVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 238).FieldMappingForXml = sourceMapping.SourceUserMapping.Address1Map;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 238).FieldRule = sourceMapping.SourceUserMapping.Address1Val;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 239).FieldMappingForXml = sourceMapping.SourceUserMapping.Address2Map;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 239).FieldRule = sourceMapping.SourceUserMapping.Address2Val;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 240).FieldMappingForXml = sourceMapping.SourceUserMapping.ZipPostalCodeMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 240).FieldRule = sourceMapping.SourceUserMapping.ZipPostalCodeVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 241).FieldMappingForXml = sourceMapping.SourceUserMapping.PhoneMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 241).FieldRule = sourceMapping.SourceUserMapping.PhoneVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 242).FieldMappingForXml = sourceMapping.SourceUserMapping.FaxMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 242).FieldRule = sourceMapping.SourceUserMapping.FaxVal;
			}
			if (structureFormat == StructureFormat.Position)
			{
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 233).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.UserEmailMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 233).FieldRule = sourceMapping.SourceUserMapping.UserEmailVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 231).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.FirstNameMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 231).FieldRule = sourceMapping.SourceUserMapping.FirstNameVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 232).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.LastNameMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 232).FieldRule = sourceMapping.SourceUserMapping.LastNameVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 234).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.CompanyMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 234).FieldRule = sourceMapping.SourceUserMapping.CompanyVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 235).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.CountryMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 235).FieldRule = sourceMapping.SourceUserMapping.CountryVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 236).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.StateMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 236).FieldRule = sourceMapping.SourceUserMapping.StateVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 237).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.CityMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 237).FieldRule = sourceMapping.SourceUserMapping.CityVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 238).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.Address1Map, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 238).FieldRule = sourceMapping.SourceUserMapping.Address1Val;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 239).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.Address2Map, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 239).FieldRule = sourceMapping.SourceUserMapping.Address2Val;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 240).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.ZipPostalCodeMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 240).FieldRule = sourceMapping.SourceUserMapping.ZipPostalCodeVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 241).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.PhoneMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 241).FieldRule = sourceMapping.SourceUserMapping.PhoneVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 242).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.FaxMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 242).FieldRule = sourceMapping.SourceUserMapping.FaxVal;
			}
			sourceMapping.SourceCustomerShippingAddressMapping.Items.AddRange(list);
		}

		private sourceMappingDataGrid sourceMappingDgCustomer { get; set; }

		private void CustomerTabLoad()
		{
			if (!this.tabSourceConfig.Contains(this.tabCustomerMapping))
			{
				this.tabSourceConfig.TabPages.Add(this.tabCustomerMapping);
			}
		}

		private void CustomerListLoad(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			this.tabCustomerMapping.Controls.Clear();
			this.sourceMappingDgCustomer = new sourceMappingDataGrid(SourceMappingType.Customer, structureFormat);
			this.CustomerConvertOldMappingToNew(sourceMapping, structureFormat, false);
			this.sourceMappingDgCustomer.SourceMappingListLoad(sourceMapping, this._sourceFieldList);
			this.sourceMappingDgCustomer.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.sourceMappingDgCustomer.Dock = DockStyle.Fill;
			this.tabCustomerMapping.Controls.Add(this.sourceMappingDgCustomer);
		}

		private void CustomerListSave(SourceMapping sourceMapping)
		{
			if (this.sourceMappingDgCustomer != null)
			{
				this.sourceMappingDgCustomer.SourceMappingListSave(sourceMapping);
			}
		}

		private void CustomerConvertOldMappingToNew(SourceMapping sourceMapping, StructureFormat structureFormat, bool force)
		{
			SourceCustomerMapping sourceCustomerMapping = sourceMapping.SourceCustomerMapping;
			if (((sourceCustomerMapping != null) ? sourceCustomerMapping.Items : null) == null || !sourceMapping.SourceCustomerMapping.Items.Any<SourceItemMapping>() || force)
			{
				this.CustomerConvertOldMappingItemToNew(sourceMapping, structureFormat);
				SourceCustomerMapping sourceCustomerMapping2 = sourceMapping.SourceCustomerMapping;
				if (((sourceCustomerMapping2 != null) ? sourceCustomerMapping2.Items : null) != null && sourceMapping.SourceCustomerMapping.Items.Any<SourceItemMapping>())
				{
					this.sourceMappingFunc.UpdateSourceMapping(sourceMapping);
				}
			}
		}

		private void CustomerConvertOldMappingItemToNew(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			if (structureFormat == StructureFormat.XML)
			{
				list.AddRange(SourceMappingHelper.GetRepeatNode());
			}
			list.AddRange(SourceMappingHelper.GetCustomerNewItem());
			if (structureFormat == StructureFormat.XML)
			{
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1).FieldMappingForXml = sourceMapping.SourceUserMapping.RepeatNode;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 210).FieldMappingForXml = sourceMapping.SourceUserMapping.UsernameMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 210).FieldRule = sourceMapping.SourceUserMapping.UsernameVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 211).FieldMappingForXml = sourceMapping.SourceUserMapping.UserEmailMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 211).FieldRule = sourceMapping.SourceUserMapping.UserEmailVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 212).FieldMappingForXml = sourceMapping.SourceUserMapping.PasswordMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 212).FieldRule = sourceMapping.SourceUserMapping.PasswordVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 213).FieldMappingForXml = sourceMapping.SourceUserMapping.FirstNameMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 213).FieldRule = sourceMapping.SourceUserMapping.FirstNameVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 214).FieldMappingForXml = sourceMapping.SourceUserMapping.LastNameMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 214).FieldRule = sourceMapping.SourceUserMapping.LastNameVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 217).FieldMappingForXml = sourceMapping.SourceUserMapping.CompanyMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 217).FieldRule = sourceMapping.SourceUserMapping.CompanyVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 235).FieldMappingForXml = sourceMapping.SourceUserMapping.CountryMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 235).FieldRule = sourceMapping.SourceUserMapping.CountryVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 236).FieldMappingForXml = sourceMapping.SourceUserMapping.StateMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 236).FieldRule = sourceMapping.SourceUserMapping.StateVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 237).FieldMappingForXml = sourceMapping.SourceUserMapping.CityMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 237).FieldRule = sourceMapping.SourceUserMapping.CityVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 238).FieldMappingForXml = sourceMapping.SourceUserMapping.Address1Map;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 238).FieldRule = sourceMapping.SourceUserMapping.Address1Val;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 239).FieldMappingForXml = sourceMapping.SourceUserMapping.Address2Map;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 239).FieldRule = sourceMapping.SourceUserMapping.Address2Val;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 240).FieldMappingForXml = sourceMapping.SourceUserMapping.ZipPostalCodeMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 240).FieldRule = sourceMapping.SourceUserMapping.ZipPostalCodeVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 241).FieldMappingForXml = sourceMapping.SourceUserMapping.PhoneMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 241).FieldRule = sourceMapping.SourceUserMapping.PhoneVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 242).FieldMappingForXml = sourceMapping.SourceUserMapping.FaxMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 242).FieldRule = sourceMapping.SourceUserMapping.FaxVal;
			}
			if (structureFormat == StructureFormat.Position)
			{
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 210).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.UsernameMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 210).FieldRule = sourceMapping.SourceUserMapping.UsernameVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 211).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.UserEmailMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 211).FieldRule = sourceMapping.SourceUserMapping.UserEmailVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 212).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.PasswordMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 212).FieldRule = sourceMapping.SourceUserMapping.PasswordVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 213).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.FirstNameMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 213).FieldRule = sourceMapping.SourceUserMapping.FirstNameVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 214).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.LastNameMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 214).FieldRule = sourceMapping.SourceUserMapping.LastNameVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 217).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.CompanyMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 217).FieldRule = sourceMapping.SourceUserMapping.CompanyVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 235).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.CountryMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 235).FieldRule = sourceMapping.SourceUserMapping.CountryVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 236).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.StateMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 236).FieldRule = sourceMapping.SourceUserMapping.StateVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 237).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.CityMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 237).FieldRule = sourceMapping.SourceUserMapping.CityVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 238).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.Address1Map, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 238).FieldRule = sourceMapping.SourceUserMapping.Address1Val;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 239).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.Address2Map, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 239).FieldRule = sourceMapping.SourceUserMapping.Address2Val;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 240).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.ZipPostalCodeMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 240).FieldRule = sourceMapping.SourceUserMapping.ZipPostalCodeVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 241).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.PhoneMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 241).FieldRule = sourceMapping.SourceUserMapping.PhoneVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 242).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.SourceUserMapping.FaxMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 242).FieldRule = sourceMapping.SourceUserMapping.FaxVal;
			}
			sourceMapping.SourceCustomerMapping.Items.AddRange(list);
		}

		private sourceMappingDataGrid sourceMappingDgCustCustomAtt { get; set; }

		private void CustomerCustomAttributesTabLoad()
		{
			if (!this.tabSourceConfig.Contains(this.tabCustCustomAttAttMapping))
			{
				this.tabSourceConfig.TabPages.Add(this.tabCustCustomAttAttMapping);
			}
		}

		private void CustomerCustomAttributesListLoad(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			this.tabCustCustomAttAttMapping.Controls.Clear();
			this.sourceMappingDgCustCustomAtt = new sourceMappingDataGrid(SourceMappingType.CustomerCustomAttributtes, structureFormat);
			this.sourceMappingDgCustCustomAtt.SourceMappingListLoad(sourceMapping, this._sourceFieldList);
			this.sourceMappingDgCustCustomAtt.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.sourceMappingDgCustCustomAtt.Dock = DockStyle.Fill;
			this.tabCustCustomAttAttMapping.Controls.Add(this.sourceMappingDgCustCustomAtt);
		}

		private void CustomerCustomAttributesListSave(SourceMapping sourceMapping)
		{
			if (this.sourceMappingDgCustCustomAtt != null)
			{
				this.sourceMappingDgCustCustomAtt.SourceMappingListSave(sourceMapping);
			}
		}

		private sourceMappingDataGrid sourceMappingDgProdPictures { get; set; }

		private void ProductPicturesTabLoad()
		{
			if (!this.tabSourceConfig.Contains(this.tabProductPictures))
			{
				this.tabSourceConfig.TabPages.Add(this.tabProductPictures);
			}
		}

		private void ProductPicturesListLoad(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			this.tabProductPictures.Controls.Clear();
			this.sourceMappingDgProdPictures = new sourceMappingDataGrid(SourceMappingType.ProductPictures, structureFormat);
			this.ProductPicturesConvertOldMappingToNew(sourceMapping, structureFormat);
			this.sourceMappingDgProdPictures.SourceMappingListLoad(sourceMapping, this._sourceFieldList);
			this.sourceMappingDgProdPictures.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.sourceMappingDgProdPictures.Dock = DockStyle.Fill;
			this.tabProductPictures.Controls.Add(this.sourceMappingDgProdPictures);
		}

		private void ProductPicturesListSave(SourceMapping sourceMapping)
		{
			if (this.sourceMappingDgProdPictures != null)
			{
				this.sourceMappingDgProdPictures.SourceMappingListSave(sourceMapping);
			}
		}

		private void ProductPicturesConvertOldMappingToNew(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			SourceProdPicturesMapping sourceProdPicturesMapping = sourceMapping.SourceProdPicturesMapping;
			if (((sourceProdPicturesMapping != null) ? sourceProdPicturesMapping.Items : null) == null || !sourceMapping.SourceProdPicturesMapping.Items.Any<SourceItemMapping>())
			{
				this.ProductPicturesConvertOldMappingItemToNew(sourceMapping, structureFormat, sourceMapping.ImageMap, sourceMapping.ImageMap, sourceMapping.ImageVal, sourceMapping.ImageSeoMap, sourceMapping.ImageSeoVal, true);
				this.ProductPicturesConvertOldMappingItemToNew(sourceMapping, structureFormat, null, sourceMapping.Image1Map, sourceMapping.Image1Val, sourceMapping.ImageSeo1Map, sourceMapping.ImageSeo1Val, true);
				this.ProductPicturesConvertOldMappingItemToNew(sourceMapping, structureFormat, null, sourceMapping.Image2Map, sourceMapping.Image2Val, sourceMapping.ImageSeo2Map, sourceMapping.ImageSeo2Val, true);
				this.ProductPicturesConvertOldMappingItemToNew(sourceMapping, structureFormat, null, sourceMapping.Image3Map, sourceMapping.Image3Val, sourceMapping.ImageSeo3Map, sourceMapping.ImageSeo3Val, true);
				this.ProductPicturesConvertOldMappingItemToNew(sourceMapping, structureFormat, null, sourceMapping.Image4Map, sourceMapping.Image4Val, sourceMapping.ImageSeo4Map, sourceMapping.ImageSeo4Val, true);
				SourceProdPicturesMapping sourceProdPicturesMapping2 = sourceMapping.SourceProdPicturesMapping;
				if (((sourceProdPicturesMapping2 != null) ? sourceProdPicturesMapping2.Items : null) != null && sourceMapping.SourceProdPicturesMapping.Items.Any<SourceItemMapping>())
				{
					sourceMapping.ImageMap = null;
					sourceMapping.ImageVal = null;
					sourceMapping.ImageSeoMap = null;
					sourceMapping.ImageSeoVal = null;
					sourceMapping.Image1Map = null;
					sourceMapping.Image1Val = null;
					sourceMapping.ImageSeo1Map = null;
					sourceMapping.ImageSeo1Val = null;
					sourceMapping.Image2Map = null;
					sourceMapping.Image2Val = null;
					sourceMapping.ImageSeo2Map = null;
					sourceMapping.ImageSeo2Val = null;
					sourceMapping.Image3Map = null;
					sourceMapping.Image3Val = null;
					sourceMapping.ImageSeo3Map = null;
					sourceMapping.ImageSeo3Val = null;
					sourceMapping.Image4Map = null;
					sourceMapping.Image4Val = null;
					sourceMapping.ImageSeo4Map = null;
					sourceMapping.ImageSeo4Val = null;
					this.sourceMappingFunc.UpdateSourceMapping(sourceMapping);
				}
			}
		}

		private void ProductPicturesConvertOldMappingItemToNew(SourceMapping sourceMapping, StructureFormat structureFormat, string picRepeatNodeMap, string picPathMap, string picPathVal, string picSeoMap, string picSeoVal, bool needrepeatnode)
		{
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			if (!string.IsNullOrEmpty(picPathMap) || !string.IsNullOrEmpty(picPathVal))
			{
				if ((list == null || !list.Any<SourceItemMapping>()) && structureFormat == StructureFormat.XML && needrepeatnode)
				{
					list.AddRange(SourceMappingHelper.GetRepeatNode());
				}
				list.AddRange(SourceMappingHelper.GetProdPictureNewItem());
				if (structureFormat == StructureFormat.XML)
				{
					if (needrepeatnode)
					{
						list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1).FieldMappingForXml = picRepeatNodeMap;
					}
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 601).FieldMappingForXml = picPathMap;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 601).FieldRule = picPathVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 602).FieldMappingForXml = picSeoMap;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 602).FieldRule = picSeoVal;
				}
				if (structureFormat == StructureFormat.Position)
				{
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 601).FieldMappingForIndex = GlobalClass.StringToInteger(picPathMap, -1);
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 601).FieldRule = picPathVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 602).FieldMappingForIndex = GlobalClass.StringToInteger(picSeoMap, -1);
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 602).FieldRule = picSeoVal;
				}
				sourceMapping.SourceProdPicturesMapping.Items.AddRange(list);
			}
		}

		private sourceMappingDataGrid sourceMappingDgProductPrice { get; set; }

		private void ProductPriceTabLoad()
		{
			if (!this.tabSourceConfig.Contains(this.tabProductPrice))
			{
				this.tabSourceConfig.TabPages.Add(this.tabProductPrice);
			}
		}

		private void ProductPriceListLoad(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			this.tabProductPrice.Controls.Clear();
			this.sourceMappingDgProductPrice = new sourceMappingDataGrid(SourceMappingType.Product_Prices, structureFormat);
			this.ProductPriceConvertOldMappingToNew(sourceMapping, structureFormat, false);
			this.sourceMappingDgProductPrice.SourceMappingListLoad(sourceMapping, this._sourceFieldList);
			this.sourceMappingDgProductPrice.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.sourceMappingDgProductPrice.Dock = DockStyle.Fill;
			this.tabProductPrice.Controls.Add(this.sourceMappingDgProductPrice);
		}

		private void ProductPriceListSave(SourceMapping sourceMapping)
		{
			if (this.sourceMappingDgProductPrice != null)
			{
				this.sourceMappingDgProductPrice.SourceMappingListSave(sourceMapping);
			}
		}

		private void ProductPriceConvertOldMappingToNew(SourceMapping sourceMapping, StructureFormat structureFormat, bool force)
		{
			SourceProdInfoMapping sourceProdInfoMapping = sourceMapping.SourceProdInfoMapping;
			if (((sourceProdInfoMapping != null) ? sourceProdInfoMapping.Items : null) == null || !sourceMapping.SourceProdPriceMapping.Items.Any<SourceItemMapping>() || force)
			{
				this.ProductPriceConvertOldMappingItemToNew(sourceMapping, structureFormat);
				SourceProdPriceMapping sourceProdPriceMapping = sourceMapping.SourceProdPriceMapping;
				if (((sourceProdPriceMapping != null) ? sourceProdPriceMapping.Items : null) != null && sourceMapping.SourceProdPriceMapping.Items.Any<SourceItemMapping>())
				{
					this.sourceMappingFunc.UpdateSourceMapping(sourceMapping);
				}
			}
		}

		private void ProductPriceConvertOldMappingItemToNew(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			list.AddRange(SourceMappingHelper.GetProdPriceNewItem(false, null));
			if (structureFormat == StructureFormat.XML)
			{
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 1001).FieldMappingForXml = sourceMapping.ProdCostMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 1001).FieldRule = sourceMapping.ProdCostVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 1002).FieldMappingForXml = sourceMapping.ProdPriceMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 1002).FieldRule = sourceMapping.ProdPriceVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 1003).FieldMappingForXml = sourceMapping.ProdOldPriceMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 1003).FieldRule = sourceMapping.ProdOldPriceVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 1009).FieldMappingForXml = sourceMapping.TaxCategoryMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 1009).FieldRule = sourceMapping.TaxCategoryVal;
			}
			if (structureFormat == StructureFormat.Position)
			{
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 1001).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.ProdCostMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 1001).FieldRule = sourceMapping.ProdCostVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 1002).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.ProdPriceMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 1002).FieldRule = sourceMapping.ProdPriceVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 1003).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.ProdOldPriceMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 1003).FieldRule = sourceMapping.ProdOldPriceVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 1009).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.TaxCategoryMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 1009).FieldRule = sourceMapping.TaxCategoryVal;
			}
			sourceMapping.SourceProdPriceMapping.Items.AddRange(list);
		}

		private sourceMappingDataGrid sourceMappingDgProdSpecAtt { get; set; }

		private void ProductSpecAttributesTabLoad()
		{
			if (!this.tabSourceConfig.Contains(this.tabProductSpecAttrbuttes))
			{
				this.tabSourceConfig.TabPages.Add(this.tabProductSpecAttrbuttes);
			}
		}

		private void ProductSpecAttributesListLoad(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			this.tabProductSpecAttrbuttes.Controls.Clear();
			this.sourceMappingDgProdSpecAtt = new sourceMappingDataGrid(SourceMappingType.ProductSpecAttributes, structureFormat);
			this.ProductSpecAttributtesConvertOldMappingToNew(sourceMapping, structureFormat);
			this.sourceMappingDgProdSpecAtt.SourceMappingListLoad(sourceMapping, this._sourceFieldList);
			this.sourceMappingDgProdSpecAtt.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.sourceMappingDgProdSpecAtt.Dock = DockStyle.Fill;
			this.tabProductSpecAttrbuttes.Controls.Add(this.sourceMappingDgProdSpecAtt);
		}

		private void ProductSpecAttributesListSave(SourceMapping sourceMapping)
		{
			if (this.sourceMappingDgProdSpecAtt != null)
			{
				this.sourceMappingDgProdSpecAtt.SourceMappingListSave(sourceMapping);
			}
		}

		private void ProductSpecAttributtesConvertOldMappingToNew(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			SourceProdSpecAttMapping sourceProdSpecAttMapping = sourceMapping.SourceProdSpecAttMapping;
			if (((sourceProdSpecAttMapping != null) ? sourceProdSpecAttMapping.SourceProdSpecAttMappingItems : null) == null || !sourceMapping.SourceProdSpecAttMapping.SourceProdSpecAttMappingItems.Any<SourceItemMapping>())
			{
				this.ProductSpecAttributtesConvertOldMappingItemToNew(sourceMapping, structureFormat, sourceMapping.SpecAttRepeatNodeMap, sourceMapping.SpecAttNameMap, sourceMapping.SpecAttNameVal, sourceMapping.SpecAttValueMap, sourceMapping.SpecAttValueVal, true);
				this.ProductSpecAttributtesConvertOldMappingItemToNew(sourceMapping, structureFormat, null, sourceMapping.SpecAtt1NameMap, sourceMapping.SpecAtt1NameVal, sourceMapping.SpecAtt1ValueMap, sourceMapping.SpecAtt1ValueVal, false);
				this.ProductSpecAttributtesConvertOldMappingItemToNew(sourceMapping, structureFormat, null, sourceMapping.SpecAtt2NameMap, sourceMapping.SpecAtt2NameVal, sourceMapping.SpecAtt2ValueMap, sourceMapping.SpecAtt2ValueVal, false);
				this.ProductSpecAttributtesConvertOldMappingItemToNew(sourceMapping, structureFormat, null, sourceMapping.SpecAtt3NameMap, sourceMapping.SpecAtt3NameVal, sourceMapping.SpecAtt3ValueMap, sourceMapping.SpecAtt3ValueVal, false);
				this.ProductSpecAttributtesConvertOldMappingItemToNew(sourceMapping, structureFormat, null, sourceMapping.SpecAtt4NameMap, sourceMapping.SpecAtt4NameVal, sourceMapping.SpecAtt4ValueMap, sourceMapping.SpecAtt4ValueVal, false);
				SourceProdSpecAttMapping sourceProdSpecAttMapping2 = sourceMapping.SourceProdSpecAttMapping;
				if (((sourceProdSpecAttMapping2 != null) ? sourceProdSpecAttMapping2.SourceProdSpecAttMappingItems : null) != null && sourceMapping.SourceProdSpecAttMapping.SourceProdSpecAttMappingItems.Any<SourceItemMapping>())
				{
					sourceMapping.SpecAttNameMap = null;
					sourceMapping.SpecAttNameVal = null;
					sourceMapping.SpecAtt1NameMap = null;
					sourceMapping.SpecAtt1NameVal = null;
					sourceMapping.SpecAtt2NameMap = null;
					sourceMapping.SpecAtt2NameVal = null;
					sourceMapping.SpecAtt3NameMap = null;
					sourceMapping.SpecAtt3NameVal = null;
					sourceMapping.SpecAtt4NameMap = null;
					sourceMapping.SpecAtt4NameVal = null;
					this.sourceMappingFunc.UpdateSourceMapping(sourceMapping);
				}
			}
		}

		private void ProductSpecAttributtesConvertOldMappingItemToNew(SourceMapping sourceMapping, StructureFormat structureFormat, string attRepeatNodeMap, string attNameMap, string attNameVal, string attValueMap, string attValueVal, bool needrepeatnode)
		{
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			if (!string.IsNullOrEmpty(attNameMap) || !string.IsNullOrEmpty(attNameVal))
			{
				if ((list == null || !list.Any<SourceItemMapping>()) && structureFormat == StructureFormat.XML && needrepeatnode)
				{
					list.AddRange(SourceMappingHelper.GetRepeatNode());
				}
				list.AddRange(SourceMappingHelper.GetProdSpecAttNewItem());
				if (structureFormat == StructureFormat.XML)
				{
					if (needrepeatnode)
					{
						list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1).FieldMappingForXml = attRepeatNodeMap;
					}
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 8).FieldMappingForXml = attNameMap;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 8).FieldRule = attNameVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 9).FieldMappingForXml = attValueMap;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 9).FieldRule = attValueVal;
				}
				if (structureFormat == StructureFormat.Position)
				{
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 8).FieldMappingForIndex = GlobalClass.StringToInteger(attNameMap, -1);
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 8).FieldRule = attNameVal;
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 9).FieldMappingForIndex = GlobalClass.StringToInteger(attValueMap, -1);
					list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 9).FieldRule = attValueVal;
				}
				sourceMapping.SourceProdSpecAttMapping.SourceProdSpecAttMappingItems.AddRange(list);
			}
		}

		private sourceMappingDataGrid sourceMappingDgProdAtt { get; set; }

		private void ProductAttributesTabLoad()
		{
			if (!this.tabSourceConfig.Contains(this.tabProductAttrbuttes))
			{
				this.tabSourceConfig.TabPages.Add(this.tabProductAttrbuttes);
			}
		}

		private void ProductAttributesListLoad(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			this.tabProductAttrbuttes.Controls.Clear();
			this.sourceMappingDgProdAtt = new sourceMappingDataGrid(SourceMappingType.ProductAttributes, structureFormat);
			this.ConvertOldMappingToNew(sourceMapping, structureFormat);
			this.sourceMappingDgProdAtt.SourceMappingListLoad(sourceMapping, this._sourceFieldList);
			this.sourceMappingDgProdAtt.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.sourceMappingDgProdAtt.Dock = DockStyle.Fill;
			this.tabProductAttrbuttes.Controls.Add(this.sourceMappingDgProdAtt);
		}

		private void ProductAttributesListSave(SourceMapping sourceMapping)
		{
			if (this.sourceMappingDgProdAtt != null)
			{
				this.sourceMappingDgProdAtt.SourceMappingListSave(sourceMapping);
			}
		}

		private void ConvertOldMappingToNew(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			if (sourceMapping.SourceProdAttMapping?.SourceProdAttMappingItems == null || !sourceMapping.SourceProdAttMapping.SourceProdAttMappingItems.Any())
			{
				ConvertOldMappingItemToNew(sourceMapping, structureFormat, sourceMapping.AttRepeatNodeMap, sourceMapping.AttSkuMap, sourceMapping.AttSkuVal, sourceMapping.AttNameMap, sourceMapping.AttNameVal, sourceMapping.AttValueMap, sourceMapping.AttValueVal, sourceMapping.AttStockMap, sourceMapping.AttStockVal, sourceMapping.AttPriceMap, sourceMapping.AttPriceVal, needrepeatnode: true);
				ConvertOldMappingItemToNew(sourceMapping, structureFormat, null, sourceMapping.Att1SkuMap, sourceMapping.Att1SkuVal, sourceMapping.Att1NameMap, sourceMapping.Att1NameVal, sourceMapping.Att1ValueMap, sourceMapping.Att1ValueVal, sourceMapping.Att1StockMap, sourceMapping.Att1StockVal, sourceMapping.Att1PriceMap, sourceMapping.Att1PriceVal, needrepeatnode: false);
				ConvertOldMappingItemToNew(sourceMapping, structureFormat, null, sourceMapping.Att2SkuMap, sourceMapping.Att1SkuVal, sourceMapping.Att2NameMap, sourceMapping.Att2NameVal, sourceMapping.Att2ValueMap, sourceMapping.Att2ValueVal, sourceMapping.Att2StockMap, sourceMapping.Att2StockVal, sourceMapping.Att2PriceMap, sourceMapping.Att2PriceVal, needrepeatnode: false);
				if (sourceMapping.SourceProdAttMapping?.SourceProdAttMappingItems != null && sourceMapping.SourceProdAttMapping.SourceProdAttMappingItems.Any())
				{
					sourceMapping.AttNameMap = null;
					sourceMapping.AttNameVal = null;
					sourceMapping.Att1NameMap = null;
					sourceMapping.Att1NameVal = null;
					sourceMapping.Att2NameMap = null;
					sourceMapping.Att2NameVal = null;
					sourceMappingFunc.UpdateSourceMapping(sourceMapping);
				}
				return;
			}
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			var list2 = (from n in sourceMapping.SourceProdAttMapping?.SourceProdAttMappingItems
						 group n by n.Id into g
						 select new
						 {
							 GroupedList = g
						 }).ToList();
			foreach (var item in list2)
			{
				list.AddRange(SourceMappingHelper.ProdAttNewItemNamingOrder(item.GroupedList.ToList()));
				List<SourceItemMapping> prodAttNewItem = SourceMappingHelper.GetProdAttNewItem(update: true, item.GroupedList.ToList());
				list.AddRange(SourceMappingHelper.ProdAttNewItemNamingOrder(prodAttNewItem));
			}
			sourceMapping.SourceProdAttMapping.SourceProdAttMappingItems = list.OrderBy((SourceItemMapping o) => o.Id).ThenBy((SourceItemMapping t) => t.Order).ToList();
		}

		private void ConvertOldMappingItemToNew(SourceMapping sourceMapping, StructureFormat structureFormat, string attRepeatNodeMap, string attSkuMap, string attSkuVal, string attNameMap, string attNameVal, string attValueMap, string attValueVal, string attStockMap, string attStockVal, string attPriceMap, string attPriceVal, bool needrepeatnode)
		{
			new List<SourceItemMapping>();
		}

		private sourceMappingDataGrid sourceMappingDgProductInfo { get; set; }

		private void ProductInfoTabLoad()
		{
			if (!this.tabSourceConfig.Contains(this.tabProductInfo))
			{
				this.tabSourceConfig.TabPages.Add(this.tabProductInfo);
			}
		}

		private void ProductInfoListLoad(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			this.tabProductInfo.Controls.Clear();
			this.sourceMappingDgProductInfo = new sourceMappingDataGrid(SourceMappingType.ProductInfo, structureFormat);
			this.sourceMappingDgProductInfo.SourceMappingListLoad(sourceMapping, this._sourceFieldList);
			this.sourceMappingDgProductInfo.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.sourceMappingDgProductInfo.Dock = DockStyle.Fill;
			this.tabProductInfo.Controls.Add(this.sourceMappingDgProductInfo);
		}

		private void ProductInfoListSave(SourceMapping sourceMapping)
		{
			if (this.sourceMappingDgProductInfo != null)
			{
				this.sourceMappingDgProductInfo.SourceMappingListSave(sourceMapping);
			}
		}

		private void ProductInfoConvertOldMappingToNew(SourceMapping sourceMapping, StructureFormat structureFormat, bool force)
		{
			SourceProdInfoMapping sourceProdInfoMapping = sourceMapping.SourceProdInfoMapping;
			if (((sourceProdInfoMapping != null) ? sourceProdInfoMapping.Items : null) == null || !sourceMapping.SourceProdInfoMapping.Items.Any<SourceItemMapping>() || force)
			{
				this.ProductInfoConvertOldMappingItemToNew(sourceMapping, structureFormat);
				SourceProdInfoMapping sourceProdInfoMapping2 = sourceMapping.SourceProdInfoMapping;
				if (((sourceProdInfoMapping2 != null) ? sourceProdInfoMapping2.Items : null) != null && sourceMapping.SourceProdInfoMapping.Items.Any<SourceItemMapping>())
				{
					this.sourceMappingFunc.UpdateSourceMapping(sourceMapping);
				}
			}
		}

		private void ProductInfoConvertOldMappingItemToNew(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			if (structureFormat == StructureFormat.XML)
			{
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == -1).FieldMappingForXml = sourceMapping.ProdInfoRepeatNode;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 501).FieldMappingForXml = sourceMapping.ProdIdMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 502).FieldMappingForXml = sourceMapping.ProdSkuMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 502).FieldRule = sourceMapping.ProdSkuVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 503).FieldMappingForXml = sourceMapping.ProdGroupByMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 504).FieldMappingForXml = sourceMapping.ProdNameMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 504).FieldRule = sourceMapping.ProdNameVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 505).FieldMappingForXml = sourceMapping.ProdShortDescMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 505).FieldRule = sourceMapping.ProdShortDescVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 518).FieldMappingForXml = sourceMapping.ProdFullDescMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 518).FieldRule = sourceMapping.ProdFullDescVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 506).FieldMappingForXml = sourceMapping.ProdManPartNumMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 506).FieldRule = sourceMapping.ProdManPartNumVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 510).FieldMappingForXml = sourceMapping.ProdStockMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 510).FieldRule = sourceMapping.ProdStockVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 512).FieldMappingForXml = sourceMapping.GtinMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 512).FieldRule = sourceMapping.GtinVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 513).FieldMappingForXml = sourceMapping.WeightMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 513).FieldRule = sourceMapping.WeightVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 515).FieldMappingForXml = sourceMapping.WidthMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 515).FieldRule = sourceMapping.WidthVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 516).FieldMappingForXml = sourceMapping.HeightMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 516).FieldRule = sourceMapping.HeightVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 514).FieldMappingForXml = sourceMapping.LengthMap;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 514).FieldRule = sourceMapping.LengthVal;
			}
			if (structureFormat == StructureFormat.Position)
			{
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 501).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.ProdIdMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 502).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.ProdSkuMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 502).FieldRule = sourceMapping.ProdSkuVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 503).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.ProdGroupByMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 504).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.ProdNameMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 504).FieldRule = sourceMapping.ProdNameVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 505).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.ProdShortDescMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 505).FieldRule = sourceMapping.ProdShortDescVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 518).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.ProdFullDescMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 518).FieldRule = sourceMapping.ProdFullDescVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 506).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.ProdManPartNumMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 506).FieldRule = sourceMapping.ProdManPartNumVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 510).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.ProdStockMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 510).FieldRule = sourceMapping.ProdStockVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 512).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.GtinMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 512).FieldRule = sourceMapping.GtinVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 513).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.WeightMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 513).FieldRule = sourceMapping.WeightVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 515).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.WidthMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 515).FieldRule = sourceMapping.WidthVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 516).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.HeightMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 516).FieldRule = sourceMapping.HeightVal;
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 514).FieldMappingForIndex = GlobalClass.StringToInteger(sourceMapping.LengthMap, -1);
				list.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 514).FieldRule = sourceMapping.LengthVal;
			}
			sourceMapping.SourceProdInfoMapping.Items.AddRange(list);
		}

		private async void LoadVendorSourceData()
		{
			if (this.firstLoad)
			{
				SourceData sourceData = this.sourceMappingFunc.GetSourceData(this.vendorSourceId);
				this.ddSourceType.SelectedIndex = sourceData.SourceType;
				this.tbSourceAddress.Text = sourceData.SourceAddress;
				this.tbSourceUserName.Text = sourceData.SourceUser;
				this.tbSourcePassword.Text = sourceData.SourcePassword;
				this.tbAuthHeader.Text = sourceData.AuthHeader;
				this.ddSourceFormat.SelectedIndex = sourceData.SourceFormat;
				this.nmFirstRowDataIndex.Value = sourceData.FirstRowIndex.GetValueOrDefault();
				this.tbCsvDelimeter.Text = GlobalClass.ObjectToString(sourceData.CsvDelimeter);
				this.tbSoapAction.Text = sourceData.SoapAction;
				this.tbSoapRequest.Text = sourceData.SoapRequest;
				this.ddProductIdInStore.SelectedIndex = sourceData.ProductIdInStore;
				this.ddUserIdInStore.SelectedIndex = sourceData.UserIdInStore;
				this.vendorSourceloaded = true;
				this.ddEntityType.SelectedIndex = (int)sourceData.EntityType;
				this.ddSourceEncoding.SelectedValue = (string.IsNullOrEmpty(sourceData.SourceEncoding) ? 65001 : Convert.ToInt32(sourceData.SourceEncoding));
				this.tbAliexpressProductsUrls.Text = sourceData.SourceContent;
				sourceData = null;
			}
		}

		public int InsertVendorSource()
		{
			string dataConnectionString = this.dataSettingsManager.LoadSettings(null).DataConnectionString;
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataConnectionString);
			string commandText = "\r\n            INSERT INTO [VendorSource]\r\n                   ([VendorId]\r\n                   ,[SourceType]\r\n                   ,[SourceAddress]\r\n                   ,[SourceUser]\r\n                   ,[SourcePassword]\r\n                   ,[SourceFormat]\r\n                   ,[FirstRowIndex]\r\n                   ,[CsvDelimeter]\r\n                   ,[SoapAction]\r\n                   ,[SoapRequest]\r\n                   ,[ProductIdInStore]\r\n                   ,[UserIdInStore]\r\n                   ,[EntityType]\r\n                   ,[SourceEncoding]\r\n                   ,[SourceContent]\r\n                   ,[AuthHeader]\r\n                    )  \r\n             ValueS\r\n                   (@VendorId\r\n                   ,@SourceType\r\n                   ,@SourceAddress\r\n                   ,@SourceUser\r\n                   ,@SourcePassword\r\n                   ,@SourceFormat\r\n                   ,@FirstRowIndex\r\n                   ,@CsvDelimeter\r\n                   ,@SoapAction\r\n                   ,@SoapRequest\r\n                   ,@ProductIdInStore\r\n                   ,@UserIdInStore\r\n                   ,@EntityType\r\n                   ,@SourceEncoding\r\n                   ,@SourceContent\r\n                   ,@AuthHeader\r\n                    )";
			SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorId", this.vendorId));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceType", this.ddSourceType.SelectedIndex));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceAddress", this.tbSourceAddress.Text.Trim()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceUser", this.tbSourceUserName.Text.Trim()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourcePassword", this.tbSourcePassword.Text.Trim()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceFormat", this.ddSourceFormat.SelectedIndex));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@FirstRowIndex", this.nmFirstRowDataIndex.Value));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@CsvDelimeter", this.tbCsvDelimeter.Text));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SoapAction", this.tbSoapAction.Text));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SoapRequest", this.tbSoapRequest.Text));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProductIdInStore", this.ddProductIdInStore.SelectedIndex));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@UserIdInStore", this.ddUserIdInStore.SelectedIndex));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@EntityType", this.ddEntityType.SelectedIndex));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceEncoding", this.ddSourceEncoding.SelectedValue));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceContent", this.tbAliexpressProductsUrls.Text.Trim()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@AuthHeader", this.tbAuthHeader.Text.Trim()));
			sqlCeConnection.Open();
			sqlCeCommand.ExecuteNonQuery();
			sqlCeCommand.CommandText = "SELECT @@IDENTITY";
			int result = Convert.ToInt32(sqlCeCommand.ExecuteScalar());
			sqlCeConnection.Close();
			if (sqlCeCommand != null)
			{
				sqlCeCommand.Dispose();
			}
			return result;
		}

		public void UpdateVendorSource()
		{
			string dataConnectionString = this.dataSettingsManager.LoadSettings(null).DataConnectionString;
			SqlCeConnection sqlCeConnection = new SqlCeConnection(dataConnectionString);
			string commandText = "\r\n                UPDATE  [VendorSource] SET\r\n                        [SourceType] = @SourceType,\r\n                        [SourceAddress] = @SourceAddress,\r\n                        [SourceUser] = @SourceUser,\r\n                        [SourcePassword] = @SourcePassword,\r\n                        [SourceFormat] = @SourceFormat,\r\n                        [FirstRowIndex] = @FirstRowIndex,\r\n                        [CsvDelimeter] = @CsvDelimeter,\r\n                        [SoapAction] = @SoapAction,\r\n                        [SoapRequest] = @SoapRequest,\r\n                        [ProductIdInStore] = @ProductIdInStore,\r\n                        [UserIdInStore] = @UserIdInStore,\r\n                        [EntityType] = @EntityType,\r\n                        [SourceEncoding] = @SourceEncoding,\r\n                        [SourceContent] = @SourceContent,\r\n                        [AuthHeader] = @AuthHeader\r\n                WHERE [Id]=@VendorSourceId";
			SqlCeCommand sqlCeCommand = new SqlCeCommand(commandText, sqlCeConnection);
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@VendorSourceId", this.vendorSourceId));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceType", this.ddSourceType.SelectedIndex));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceAddress", this.tbSourceAddress.Text.Trim()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceUser", this.tbSourceUserName.Text.Trim()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourcePassword", this.tbSourcePassword.Text.Trim()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceFormat", this.ddSourceFormat.SelectedIndex));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@FirstRowIndex", this.nmFirstRowDataIndex.Value));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@CsvDelimeter", this.tbCsvDelimeter.Text));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SoapAction", this.tbSoapAction.Text));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SoapRequest", this.tbSoapRequest.Text));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@ProductIdInStore", this.ddProductIdInStore.SelectedIndex));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@UserIdInStore", this.ddUserIdInStore.SelectedIndex));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@EntityType", this.ddEntityType.SelectedIndex));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceEncoding", this.ddSourceEncoding.SelectedValue));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@SourceContent", this.tbAliexpressProductsUrls.Text.Trim()));
			sqlCeCommand.Parameters.Add(new SqlCeParameter("@AuthHeader", this.tbAuthHeader.Text.Trim()));
			sqlCeConnection.Open();
			sqlCeCommand.ExecuteNonQuery();
			sqlCeConnection.Close();
			if (sqlCeCommand != null)
			{
				sqlCeCommand.Dispose();
			}
		}

		private void TestSourceConn()
		{
			try
			{
				if (this.tbSourceAddress.Text.Trim().Length > 0)
				{
					byte[] fileContent = this.sourceMappingFunc.GetFileContent((SourceType)this.ddSourceType.SelectedIndex, this.tbSourceUserName.Text, this.tbSourcePassword.Text, this.tbSourceAddress.Text, this.tbSoapAction.Text, this.tbSoapRequest.Text, this.ddSourceEncoding.SelectedValue.ToString(), this.ddSourceFormat.Text == "XLSX", this.tbAliexpressProductsUrls.Text, this.tbAuthHeader.Text);
					this.RefreshSourceFileTicimax(fileContent);
					MessageBox.Show("Test is successful.");
				}
				else
				{
					MessageBox.Show("Test is failed. File path is empty.");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Test is failed: " + ex.ToString());
			}
		}

		private void ddSourceFormat_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.vendorSourceloaded)
			{
				if (!this.firstLoad)
				{
					this.LoadTabs();
				}
				if (this.ddSourceFormat.Text == "CSV")
				{
					this.lblCsvDelimeter.Show();
					this.tbCsvDelimeter.Show();
				}
				else
				{
					this.tbCsvDelimeter.Hide();
					this.lblCsvDelimeter.Hide();
				}
				if (this.ddSourceFormat.Text == "XML")
				{
					this.LoadVendorSourceMappingDataForXml();
				}
				if (this.ddSourceFormat.Text == "CSV" || this.ddSourceFormat.Text == "XLSX")
				{
					this.LoadVendorSourceMappingDataForPos();
				}
			}
		}

		private void btnTestSource_Click(object sender, EventArgs e)
		{
			this.TestSourceConn();
		}

		private void ddEntityType_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.vendorSourceloaded)
			{
				if (!this.firstLoad)
				{
					this.LoadTabs();
				}
				if (this.ddEntityType.SelectedIndex == 2)
				{
					this.lblProductIdentifier.Hide();
					this.ddProductIdInStore.Hide();
					this.lblUserIdentifier.Hide();
					this.ddUserIdInStore.Hide();
				}
				else if (this.ddEntityType.SelectedIndex == 1)
				{
					this.lblProductIdentifier.Hide();
					this.ddProductIdInStore.Hide();
					this.lblUserIdentifier.Show();
					this.ddUserIdInStore.Show();
				}
				else
				{
					this.lblProductIdentifier.Show();
					this.ddProductIdInStore.Show();
					this.lblUserIdentifier.Hide();
					this.ddUserIdInStore.Hide();
				}
			}
		}

		private async void SaveSourceMappingAliExpress()
		{
			SourceMapping sourceMapping = this.GetSourceMappingForAliXpress();
			if (this.vendorSourceMappingId > 0)
			{
				this.sourceMappingFunc.UpdateSourceMapping(sourceMapping);
			}
			else
			{
				int num = await this.sourceMappingFunc.InsertSourceMapping(sourceMapping);
				this.vendorSourceMappingId = num;
			}
		}

		private async void SaveSourceMappingBestSecret()
		{
			SourceMapping sourceMapping = this.GetSourceMappingForBestSecret();
			if (this.vendorSourceMappingId > 0)
			{
				this.sourceMappingFunc.UpdateSourceMapping(sourceMapping);
			}
			else
			{
				int num = await this.sourceMappingFunc.InsertSourceMapping(sourceMapping);
				this.vendorSourceMappingId = num;
			}
		}

		private async void SaveSourceMappingXml()
		{
			SourceMapping sourceMapping = this.GetSourceMappingFromForm();
			if (this.vendorSourceMappingId > 0)
			{
				this.sourceMappingFunc.UpdateSourceMapping(sourceMapping);
			}
			else
			{
				int num = await this.sourceMappingFunc.InsertSourceMapping(sourceMapping);
				this.vendorSourceMappingId = num;
			}
		}

		private async void LoadVendorSourceMappingDataForXml()
		{
			SourceMapping sourceMapping = this.sourceMappingFunc.GetSourceMapping(this.vendorSourceId);
			this.tbProductRepeatNodeXpath.Text = sourceMapping.ProdInfoRepeatNode;
			this.tbProdIDXpath.Text = sourceMapping.ProdIdMap;
			this.tbProdGroupByXpath.Text = sourceMapping.ProdGroupByMap;
			this.tbProdSKUXpath.Text = sourceMapping.ProdSkuMap;
			this.tbDefaultProdSKU_Xpath.Text = sourceMapping.ProdSkuVal;
			this.tbProdNameXpath.Text = sourceMapping.ProdNameMap;
			this.tbDefaultProductName_xml.Text = sourceMapping.ProdNameVal;
			this.tbProdShortDescXpath.Text = sourceMapping.ProdShortDescMap;
			this.tbDefaultProdShortDesc_xml.Text = sourceMapping.ProdShortDescVal;
			this.tbProdFullDescXpath.Text = sourceMapping.ProdFullDescMap;
			this.tbDefaultProdFullDesc_xml.Text = sourceMapping.ProdFullDescVal;
			this.tbProdManNumberXpath.Text = sourceMapping.ProdManPartNumMap;
			this.tbDefaultManufacturerPnum_xml.Text = sourceMapping.ProdManPartNumVal;
			this.tbProdStockQuantityXpath.Text = sourceMapping.ProdStockMap;
			this.nmDefaultStockQuantity_xml.Text = sourceMapping.ProdStockVal;
			this.tbMetaKeywordsXpath.Text = sourceMapping.SeoMetaKeyMap;
			this.tbDefaultMetaKeywords.Text = sourceMapping.SeoMetaKeyVal;
			this.tbMetaDescriptionXpath.Text = sourceMapping.SeoMetaDescMap;
			this.tbDefaultMetaDescription.Text = sourceMapping.SeoMetaDescVal;
			this.tbMetaTitleXpath.Text = sourceMapping.SeoMetaTitleMap;
			this.tbDefaultMetaTitle.Text = sourceMapping.SeoMetaTitleVal;
			this.tbSearchEnginePageXpath.Text = sourceMapping.SeoPageMap;
			this.tbDefaultSearchEnginePage.Text = sourceMapping.SeoPageVal;
			this.tbCategoryXPath.Text = sourceMapping.CatMap;
			this.tbDefaultCategory_xml.Text = sourceMapping.CatVal;
			this.tbCategoryDelimeter_xml.Text = GlobalClass.ObjectToString(sourceMapping.CatDelimeter);
			this.tbCategoryXPath1.Text = sourceMapping.Cat1Map;
			this.tbDefaultCategory1_xml.Text = sourceMapping.Cat1Val;
			this.tbCategoryXPath2.Text = sourceMapping.Cat2Map;
			this.tbDefaultCategory2_xml.Text = sourceMapping.Cat2Val;
			this.tbCategoryXPath3.Text = sourceMapping.Cat3Map;
			this.tbDefaultCategory3_xml.Text = sourceMapping.Cat3Val;
			this.tbCategoryXPath4.Text = sourceMapping.Cat4Map;
			this.tbDefaultCategory4_xml.Text = sourceMapping.Cat4Val;
			this.tbManufacturerXPath.Text = sourceMapping.ManufactMap;
			this.tbDefaultManufacturer_xml.Text = sourceMapping.ManufactVal;
			this.tbFilterName_xml.Text = sourceMapping.FilterName;
			this.tbFilterXpath.Text = sourceMapping.FilterMap;
			this.tbFilterName1_xml.Text = sourceMapping.Filter1Name;
			this.tbFilterXpath1.Text = sourceMapping.Filter1Map;
			this.tbFilterName2_xml.Text = sourceMapping.Filter2Name;
			this.tbFilterXpath2.Text = sourceMapping.Filter2Map;
			this.tbFilterName3_xml.Text = sourceMapping.Filter3Name;
			this.tbFilterXpath3.Text = sourceMapping.Filter3Map;
			this.tbFilterName4_xml.Text = sourceMapping.Filter4Name;
			this.tbFilterXpath4.Text = sourceMapping.Filter4Map;
			this.tbFilterName5_xml.Text = sourceMapping.Filter5Name;
			this.tbFilterXpath5.Text = sourceMapping.Filter5Map;
			this.tbFilterName6_xml.Text = sourceMapping.Filter6Name;
			this.tbFilterXpath6.Text = sourceMapping.Filter6Map;
			this.tbFilterName7_xml.Text = sourceMapping.Filter7Name;
			this.tbFilterXpath7.Text = sourceMapping.Filter7Map;
			this.tbFilterName8_xml.Text = sourceMapping.Filter8Name;
			this.tbFilterXpath8.Text = sourceMapping.Filter8Map;
			this.tbFilterName9_xml.Text = sourceMapping.Filter9Name;
			this.tbFilterXpath9.Text = sourceMapping.Filter9Map;
			this.chCatId_xml.Checked = sourceMapping.CatIsId;
			this.chCat1Id_xml.Checked = sourceMapping.Cat1IsId;
			this.chCat2Id_xml.Checked = sourceMapping.Cat2IsId;
			this.chCat3Id_xml.Checked = sourceMapping.Cat3IsId;
			this.chCat4Id_xml.Checked = sourceMapping.Cat4IsId;
			this.chManufacturerId_xml.Checked = sourceMapping.ManufacturerIsId;
			this.chVendorId_xml.Checked = sourceMapping.VendorIsId;
			this.chProdSettingsDeliveryDateIsID_xml.Checked = sourceMapping.DeliveryDateIsId;
			this.tbVendorXpath.Text = sourceMapping.VendorMap;
			this.tbDefaultVendor_xml.Text = sourceMapping.VendorVal;
			this.textBox_0.Text = sourceMapping.GtinMap;
			this.tbDefaultGtinXPath.Text = sourceMapping.GtinVal;
			this.tbWeightXPath.Text = sourceMapping.WeightMap;
			this.tbDefaultWeight_xml.Text = sourceMapping.WeightVal;
			this.tbLengthXpath.Text = sourceMapping.LengthMap;
			this.tbDefaultLength_xml.Text = sourceMapping.LengthVal;
			this.tbWidthXpath.Text = sourceMapping.WidthMap;
			this.tbDefaultWidth_xml.Text = sourceMapping.WidthVal;
			this.tbHeightXpath.Text = sourceMapping.HeightMap;
			this.tbDefaultHeight_xml.Text = sourceMapping.HeightVal;
			this.tbCustomerRoleXpath.Text = sourceMapping.CustomerRoleMap;
			this.tbDefaultCustomerRole_xml.Text = sourceMapping.CustomerRoleVal;
			this.tbCustomerRoleDelimeter_xml.Text = GlobalClass.ObjectToString(sourceMapping.CustomerRoleDelimeter);
			this.tbCustomerRoleXpath1.Text = sourceMapping.CustomerRole1Map;
			this.tbDefaultCustomerRole1_xml.Text = sourceMapping.CustomerRole1Val;
			this.tbCustomerRoleXpath2.Text = sourceMapping.CustomerRole2Map;
			this.tbDefaultCustomerRole2_xml.Text = sourceMapping.CustomerRole2Val;
			this.tbCustomerRoleXpath3.Text = sourceMapping.CustomerRole3Map;
			this.tbDefaultCustomerRole3_xml.Text = sourceMapping.CustomerRole3Val;
			this.tbCustomerRoleXpath4.Text = sourceMapping.CustomerRole4Map;
			this.tbDefaultCustomerRole4_xml.Text = sourceMapping.CustomerRole4Val;
			this.chCustomerRoleID_xml.Checked = sourceMapping.CustomerRoleIsId;
			this.chCustomerRoleID1_xml.Checked = sourceMapping.CustomerRole1IsId;
			this.chCustomerRoleID2_xml.Checked = sourceMapping.CustomerRole2IsId;
			this.chCustomerRoleID3_xml.Checked = sourceMapping.CustomerRole3IsId;
			this.chCustomerRoleID4_xml.Checked = sourceMapping.CustomerRole4IsId;
			this.tbProdSettingsAllowedQty_xml.Text = sourceMapping.ProdSettingsAllowedQtyMap;
			this.tbDefaultProdSettingsAllowedQty_xml.Text = sourceMapping.ProdSettingsAllowedQtyVal;
			this.tbProdSettingsMinCartQty_xml.Text = sourceMapping.ProdSettingsMinCartQtyMap;
			this.tbDefaultProdSettingsMinCartQty_xml.Text = sourceMapping.ProdSettingsMinCartQtyVal;
			this.tbProdSettingsMaxCartQty_xml.Text = sourceMapping.ProdSettingsMaxCartQtyMap;
			this.tbDefaultProdSettingsMaxCartQty_xml.Text = sourceMapping.ProdSettingsMaxCartQtyVal;
			this.tbProdSettingsMinStockQty_xml.Text = sourceMapping.ProdSettingsMinStockQtyMap;
			this.tbDefaultProdSettingsMinStockQty_xml.Text = sourceMapping.ProdSettingsMinStockQtyVal;
			this.tbProdSettingsNotifyQty_xml.Text = sourceMapping.ProdSettingsNotifyQtyMap;
			this.tbDefaultProdSettingsNotifyQty_xml.Text = sourceMapping.ProdSettingsNotifyQtyVal;
			this.tbProdSettingsShippingEnabled_xml.Text = sourceMapping.ProdSettingsShippingEnabledMap;
			this.tbDefaultProdSettingsShippingEnabled_xml.Text = sourceMapping.ProdSettingsShippingEnabledVal;
			this.tbProdSettingsFreeShipping_xml.Text = sourceMapping.ProdSettingsFreeShippingMap;
			this.tbDefaultProdSettingsFreeShipping_xml.Text = sourceMapping.ProdSettingsFreeShippingVal;
			this.tbProdSettingsShipSeparately_xml.Text = sourceMapping.ProdSettingsShipSeparatelyMap;
			this.tbDefaultProdSettingsShipSeparately_xml.Text = sourceMapping.ProdSettingsShipSeparatelyVal;
			this.tbProdSettingsShippingCharge_xml.Text = sourceMapping.ProdSettingsShippingChargeMap;
			this.tbDefaultProdSettingsShippingCharge_xml.Text = sourceMapping.ProdSettingsShippingChargeVal;
			this.tbProdSettingsDeliveryDate_xml.Text = sourceMapping.ProdSettingsDeliveryDateMap;
			this.tbDefaultProdSettingsDeliveryDate_xml.Text = sourceMapping.ProdSettingsDeliveryDateVal;
			this.tbProdSettingsVisibleIndvidually_xml.Text = sourceMapping.ProdSettingsVisibleIndividuallyMap;
			this.tbDefaultProdSettingsVisibleIndvidually_xml.Text = sourceMapping.ProdSettingsVisibleIndividuallyVal;
			this.tbProdSettingsIsDeleted_xml.Text = sourceMapping.ProdSettingsIsDeletedMap;
			this.tbDefaultProdSettingsIsDeleted_xml.Text = sourceMapping.ProdSettingsIsDeletedVal;
			this.ProductPriceListLoad(sourceMapping, StructureFormat.XML);
			this.TierPricingListLoad(sourceMapping, StructureFormat.XML);
			this.ProductAttributesListLoad(sourceMapping, StructureFormat.XML);
			this.ProductSpecAttributesListLoad(sourceMapping, StructureFormat.XML);
			this.ProductInfoListLoad(sourceMapping, StructureFormat.XML);
			this.ProductPicturesListLoad(sourceMapping, StructureFormat.XML);
			if (this.ddEntityType.SelectedIndex == 1)
			{
				this.CustomerListLoad(sourceMapping, StructureFormat.XML);
				this.CustomerAddressListLoad(sourceMapping, StructureFormat.XML);
				this.CustomerRoleListLoad(sourceMapping, StructureFormat.XML);
				this.AddressCustomAttributesListLoad(sourceMapping, StructureFormat.XML);
				this.CustomerCustomAttributesListLoad(sourceMapping, StructureFormat.XML);
				this.CustomerOthersListLoad(sourceMapping, StructureFormat.XML);
			}
			if (this.ddEntityType.SelectedIndex == 2)
			{
				this.WishListListLoad(sourceMapping, StructureFormat.XML);
			}
		}

		private async void LoadVendorSourceMappingDataForPos()
		{
			SourceMapping sourceMapping = this.sourceMappingFunc.GetSourceMapping(this.vendorSourceId);
			
			if (this.ddEntityType.SelectedIndex == 0)
			{
				this.nmProdId_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdIdMap, -1);
				this.nmProdName_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdNameMap, -1);
				this.nmProdGroupBy_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdGroupByMap, -1);
				this.nmProdSKU_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdSkuMap, -1);
				this.nmDefaultProdSKU_pos.Text = sourceMapping.ProdSkuVal;
				this.tbDefaultProdName_pos.Text = sourceMapping.ProdNameVal;
				this.nmProdShortDesc_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdShortDescMap, -1);
				this.tbDefaultProdShortDesc_pos.Text = sourceMapping.ProdShortDescVal;
				this.nmProdFullDesc_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdFullDescMap, -1);
				this.tbDefaultProdFullDesc_pos.Text = sourceMapping.ProdFullDescVal;
				this.nmProdManPartNum_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdManPartNumMap, -1);
				this.tbProdManPartNum_pos.Text = sourceMapping.ProdManPartNumVal;
				this.nmProdStock_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdStockMap, -1);
				this.nmDefaultProdStock_pos.Text = sourceMapping.ProdStockVal;
				this.nmSeoMetaKey_pos.Value = GlobalClass.StringToInteger(sourceMapping.SeoMetaKeyMap, -1);
				this.tbDefaultSeoMetaKey_pos.Text = sourceMapping.SeoMetaKeyVal;
				this.nmSeoMetaDesc_pos.Value = GlobalClass.StringToInteger(sourceMapping.SeoMetaDescMap, -1);
				this.tbDefaultSeoMetaDesc_pos.Text = sourceMapping.SeoMetaDescVal;
				this.nmSeoMetaTitle_pos.Value = GlobalClass.StringToInteger(sourceMapping.SeoMetaTitleMap, -1);
				this.tbDefaultSeoMetaTitle_pos.Text = sourceMapping.SeoMetaTitleVal;
				this.nmSeoSearchPage_pos.Value = GlobalClass.StringToInteger(sourceMapping.SeoPageMap, -1);
				this.tbDefaultSeoSearchPage_pos.Text = sourceMapping.SeoPageVal;
				this.tbCategoryDelimeter_pos.Text = GlobalClass.ObjectToString(sourceMapping.CatDelimeter);
				this.nmCategory_pos.Value = GlobalClass.StringToInteger(sourceMapping.CatMap, -1);
				this.tbDefaultCategory_pos.Text = sourceMapping.CatVal;
				this.nmCategory1_pos.Value = GlobalClass.StringToInteger(sourceMapping.Cat1Map, -1);
				this.tbDefaultCategory1_pos.Text = sourceMapping.Cat1Val;
				this.nmCategory2_pos.Value = GlobalClass.StringToInteger(sourceMapping.Cat2Map, -1);
				this.tbDefaultCategory2_pos.Text = sourceMapping.Cat2Val;
				this.nmCategory3_pos.Value = GlobalClass.StringToInteger(sourceMapping.Cat3Map, -1);
				this.tbDefaultCategory3_pos.Text = sourceMapping.Cat3Val;
				this.nmCategory4_pos.Value = GlobalClass.StringToInteger(sourceMapping.Cat4Map, -1);
				this.tbDefaultCategory4_pos.Text = sourceMapping.Cat4Val;
				this.nmManufacturer_pos.Value = GlobalClass.StringToInteger(sourceMapping.ManufactMap, -1);
				this.tbDefaultManufacturer_pos.Text = sourceMapping.ManufactVal;
				this.tbFilterName_pos.Text = sourceMapping.FilterName;
				this.nmFilterValue_pos.Value = GlobalClass.StringToInteger(sourceMapping.FilterMap, -1);
				this.tbFilter1Name_pos.Text = sourceMapping.Filter1Name;
				this.nmFilter1Value_pos.Value = GlobalClass.StringToInteger(sourceMapping.Filter1Map, -1);
				this.tbFilter2Name_pos.Text = sourceMapping.Filter2Name;
				this.nmFilter2Value_pos.Value = GlobalClass.StringToInteger(sourceMapping.Filter2Map, -1);
				this.tbFilter3Name_pos.Text = sourceMapping.Filter3Name;
				this.nmFilter3Value_pos.Value = GlobalClass.StringToInteger(sourceMapping.Filter3Map, -1);
				this.tbFilter4Name_pos.Text = sourceMapping.Filter4Name;
				this.nmFilter4Value_pos.Value = GlobalClass.StringToInteger(sourceMapping.Filter4Map, -1);
				this.tbFilter5Name_pos.Text = sourceMapping.Filter5Name;
				this.nmFilter5Value_pos.Value = GlobalClass.StringToInteger(sourceMapping.Filter5Map, -1);
				this.tbFilter6Name_pos.Text = sourceMapping.Filter6Name;
				this.nmFilter6Value_pos.Value = GlobalClass.StringToInteger(sourceMapping.Filter6Map, -1);
				this.tbFilter7Name_pos.Text = sourceMapping.Filter7Name;
				this.nmFilter7Value_pos.Value = GlobalClass.StringToInteger(sourceMapping.Filter7Map, -1);
				this.tbFilter8Name_pos.Text = sourceMapping.Filter8Name;
				this.nmFilter8Value_pos.Value = GlobalClass.StringToInteger(sourceMapping.Filter8Map, -1);
				this.tbFilter9Name_pos.Text = sourceMapping.Filter9Name;
				this.nmFilter9Value_pos.Value = GlobalClass.StringToInteger(sourceMapping.Filter9Map, -1);
				this.chCatId_pos.Checked = sourceMapping.CatIsId;
				this.chCat1Id_pos.Checked = sourceMapping.Cat1IsId;
				this.chCat2Id_pos.Checked = sourceMapping.Cat2IsId;
				this.chCat3Id_pos.Checked = sourceMapping.Cat3IsId;
				this.chCat4Id_pos.Checked = sourceMapping.Cat4IsId;
				this.chManufacturerId_pos.Checked = sourceMapping.ManufacturerIsId;
				this.chVendorId_pos.Checked = sourceMapping.VendorIsId;
				this.chProdSettingsDeliveryDateIsID_pos.Checked = sourceMapping.DeliveryDateIsId;
				this.nmVendor_pos.Value = GlobalClass.StringToInteger(sourceMapping.VendorMap, -1);
				this.tbDefaultVendor_pos.Text = sourceMapping.VendorVal;
				this.nmGtin_pos.Value = GlobalClass.StringToInteger(sourceMapping.GtinMap, -1);
				this.tbDefaultGtin_pos.Text = sourceMapping.GtinVal;
				this.nmWeight_pos.Value = GlobalClass.StringToInteger(sourceMapping.WeightMap, -1);
				this.tbDefaultWeight_pos.Text = sourceMapping.WeightVal;
				this.nmLength_pos.Value = GlobalClass.StringToInteger(sourceMapping.LengthMap, -1);
				this.tbDefaultLength_pos.Text = sourceMapping.LengthVal;
				this.nmWidth_pos.Value = GlobalClass.StringToInteger(sourceMapping.WidthMap, -1);
				this.tbDefaultWidth_pos.Text = sourceMapping.WidthVal;
				this.nmHeight_pos.Value = GlobalClass.StringToInteger(sourceMapping.HeightMap, -1);
				this.tbDefaultHeight_pos.Text = sourceMapping.HeightVal;
				this.nmProdSettingsAllowedQty_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdSettingsAllowedQtyMap, -1);
				this.tbDefaultProdSettingsAllowedQty_pos.Text = sourceMapping.ProdSettingsAllowedQtyVal;
				this.nmProdSettingsMinCartQty_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdSettingsMinCartQtyMap, -1);
				this.tbDefaultProdSettingsMinCartQty_pos.Text = sourceMapping.ProdSettingsMinCartQtyVal;
				this.nmProdSettingsMaxCartQty_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdSettingsMaxCartQtyMap, -1);
				this.tbDefaultProdSettingsMaxCartQty_pos.Text = sourceMapping.ProdSettingsMaxCartQtyVal;
				this.nmProdSettingsMinStockQty_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdSettingsMinStockQtyMap, -1);
				this.tbDefaultProdSettingsMinStockQty_pos.Text = sourceMapping.ProdSettingsMinStockQtyVal;
				this.nmProdSettingsNotifyQty_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdSettingsNotifyQtyMap, -1);
				this.tbDefaultProdSettingsNotifyQty_pos.Text = sourceMapping.ProdSettingsNotifyQtyVal;
				this.nmProdSettingsShippingEnabled_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdSettingsShippingEnabledMap, -1);
				this.tbDefaultProdSettingsShippingEnabled_pos.Text = sourceMapping.ProdSettingsShippingEnabledVal;
				this.nmProdSettingsFreeShipping_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdSettingsFreeShippingMap, -1);
				this.tbDefaultProdSettingsFreeShipping_pos.Text = sourceMapping.ProdSettingsFreeShippingVal;
				this.nmProdSettingsShipSeparately_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdSettingsShipSeparatelyMap, -1);
				this.tbDefaultProdSettingsShipSeparately_pos.Text = sourceMapping.ProdSettingsShipSeparatelyVal;
				this.nmProdSettingsShippingCharge_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdSettingsShippingChargeMap, -1);
				this.tbDefaultProdSettingsShippingCharge_pos.Text = sourceMapping.ProdSettingsShippingChargeVal;
				this.nmProdSettingsDeliveryDate_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdSettingsDeliveryDateMap, -1);
				this.tbDefaultProdSettingsDeliveryDate_pos.Text = sourceMapping.ProdSettingsDeliveryDateVal;
				this.nmProdSettingsVisibleIndvidually_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdSettingsVisibleIndividuallyMap, -1);
				this.tbDefaultProdSettingsVisibleIndvidually_pos.Text = sourceMapping.ProdSettingsVisibleIndividuallyVal;
				this.nmProdSettingsIsDeleted_pos.Value = GlobalClass.StringToInteger(sourceMapping.ProdSettingsIsDeletedMap, -1);
				this.tbDefaultProdSettingsIsDeleted_pos.Text = sourceMapping.ProdSettingsIsDeletedVal;
				this.ProductPriceListLoad(sourceMapping, StructureFormat.Position);
				this.TierPricingListLoad(sourceMapping, StructureFormat.Position);
				this.ProductAttributesListLoad(sourceMapping, StructureFormat.Position);
				this.ProductSpecAttributesListLoad(sourceMapping, StructureFormat.Position);
				this.ProductInfoListLoad(sourceMapping, StructureFormat.Position);
				this.ProductPicturesListLoad(sourceMapping, StructureFormat.Position);
			}
			if (this.ddEntityType.SelectedIndex == 0 || this.ddEntityType.SelectedIndex == 1)
			{
				this.tbCustomerRoleDelimiter_pos.Text = GlobalClass.ObjectToString(sourceMapping.CustomerRoleDelimeter);
				this.nmCustomerRole_pos.Value = GlobalClass.StringToInteger(sourceMapping.CustomerRoleMap, -1);
				this.tbDefaultCustomerRole_pos.Text = sourceMapping.CustomerRoleVal;
				this.nmCustomerRole1_pos.Value = GlobalClass.StringToInteger(sourceMapping.CustomerRole1Map, -1);
				this.tbDefaultCustomerRole1_pos.Text = sourceMapping.CustomerRole1Val;
				this.nmCustomerRole2_pos.Value = GlobalClass.StringToInteger(sourceMapping.CustomerRole2Map, -1);
				this.tbDefaultCustomerRole2_pos.Text = sourceMapping.CustomerRole2Val;
				this.nmCustomerRole3_pos.Value = GlobalClass.StringToInteger(sourceMapping.CustomerRole3Map, -1);
				this.tbDefaultCustomerRole3_pos.Text = sourceMapping.CustomerRole3Val;
				this.nmCustomerRole4_pos.Value = GlobalClass.StringToInteger(sourceMapping.CustomerRole4Map, -1);
				this.tbDefaultCustomerRole4_pos.Text = sourceMapping.CustomerRole4Val;
				this.chCustomerRoleId_pos.Checked = sourceMapping.CustomerRoleIsId;
				this.chCustomerRoleId1_pos.Checked = sourceMapping.CustomerRole1IsId;
				this.chCustomerRoleId2_pos.Checked = sourceMapping.CustomerRole2IsId;
				this.chCustomerRoleId3_pos.Checked = sourceMapping.CustomerRole3IsId;
				this.chCustomerRoleId4_pos.Checked = sourceMapping.CustomerRole4IsId;
			}
			if (this.ddEntityType.SelectedIndex == 1)
			{
				this.CustomerListLoad(sourceMapping, StructureFormat.Position);
				this.CustomerAddressListLoad(sourceMapping, StructureFormat.Position);
				this.CustomerRoleListLoad(sourceMapping, StructureFormat.Position);
				this.AddressCustomAttributesListLoad(sourceMapping, StructureFormat.Position);
				this.CustomerCustomAttributesListLoad(sourceMapping, StructureFormat.Position);
				this.CustomerOthersListLoad(sourceMapping, StructureFormat.Position);
			}
			if (this.ddEntityType.SelectedIndex == 2)
			{
				this.WishListListLoad(sourceMapping, StructureFormat.Position);
			}
		}

		private async void SaveSourceMappingPos()
		{
			SourceMapping sourceMapping = this.GetSourceMappingFromForm();
			if (this.vendorSourceMappingId > 0)
			{
				this.sourceMappingFunc.UpdateSourceMapping(sourceMapping);
			}
			else
			{
				int num = await this.sourceMappingFunc.InsertSourceMapping(sourceMapping);
				this.vendorSourceMappingId = num;
			}
		}

		private SourceMapping GetSourceMappingForAliXpress()
		{
			return new SourceMapping
			{
				EntityType = (EntityType)this.ddEntityType.SelectedIndex,
				CsvDelimeter = null,
				FirstRowIndex = new int?(GlobalClass.StringToInteger(this.nmFirstRowDataIndex.Value, 0)),
				VendorSourceId = new int?(this.vendorSourceId),
				ProdIdMap = "AliExpress",
				ProdSkuMap = "AliExpress",
				ProdNameMap = "AliExpress",
				ProdShortDescMap = "AliExpress",
				ProdFullDescMap = "AliExpress",
				ProdPriceMap = "AliExpress",
				ProdStockMap = "AliExpress",
				ManufactMap = "AliExpress",
				ImageMap = "AliExpress",
				AttRepeatNodeMap = "AliExpress",
				AttNameMap = "AliExpress",
				AttValueMap = "AliExpress",
				AttStockMap = "AliExpress",
				Att1NameMap = "AliExpress",
				Att1ValueMap = "AliExpress",
				Att1StockMap = "AliExpress",
				Att2NameMap = "AliExpress",
				Att2ValueMap = "AliExpress",
				Att2StockMap = "AliExpress"
			};
		}

		private SourceMapping GetSourceMappingForBestSecret()
		{
			return new SourceMapping
			{
				EntityType = (EntityType)this.ddEntityType.SelectedIndex,
				CsvDelimeter = null,
				FirstRowIndex = new int?(GlobalClass.StringToInteger(this.nmFirstRowDataIndex.Value, 0)),
				VendorSourceId = new int?(this.vendorSourceId),
				ProdIdMap = "BestSecret",
				ProdSkuMap = "BestSecret",
				ProdNameMap = "BestSecret",
				ProdShortDescMap = "BestSecret",
				ProdFullDescMap = "BestSecret",
				ProdPriceMap = "BestSecret",
				ProdCostMap = "BestSecret",
				ProdOldPriceMap = "BestSecret",
				ProdStockMap = "BestSecret",
				ManufactMap = "BestSecret",
				ImageMap = "BestSecret",
				AttRepeatNodeMap = "BestSecret",
				AttNameMap = "BestSecret",
				AttValueMap = "BestSecret",
				AttStockMap = "BestSecret",
				Att1NameMap = "BestSecret",
				Att1ValueMap = "BestSecret",
				Att1StockMap = "BestSecret",
				Att2NameMap = "BestSecret",
				Att2ValueMap = "BestSecret",
				Att2StockMap = "BestSecret"
			};
		}

		private SourceMapping GetSourceMappingFromForm()
		{
			SourceMapping sourceMapping = new SourceMapping();
			sourceMapping.EntityType = (EntityType)this.ddEntityType.SelectedIndex;
			if (this.ddSourceFormat.Text == "XML")
			{
				sourceMapping.CsvDelimeter = null;
				sourceMapping.FirstRowIndex = new int?(GlobalClass.StringToInteger(this.nmFirstRowDataIndex.Value, 0));
				sourceMapping.VendorSourceId = new int?(this.vendorSourceId);
				if (this.ddEntityType.SelectedIndex == 2)
				{
					sourceMapping.ProdInfoRepeatNode = this.tbProductRepeatNodeXpath.Text.Trim();
				}
				if (this.ddEntityType.SelectedIndex == 0)
				{
					sourceMapping.ProdInfoRepeatNode = this.tbProductRepeatNodeXpath.Text.Trim();
					sourceMapping.ProdIdMap = this.tbProdIDXpath.Text.Trim();
					sourceMapping.ProdGroupByMap = this.tbProdGroupByXpath.Text.Trim();
					sourceMapping.ProdSkuMap = this.tbProdSKUXpath.Text.Trim();
					sourceMapping.ProdSkuVal = this.tbDefaultProdSKU_Xpath.Text.Trim();
					sourceMapping.ProdNameMap = this.tbProdNameXpath.Text.Trim();
					sourceMapping.ProdNameVal = this.tbDefaultProductName_xml.Text.Trim();
					sourceMapping.ProdShortDescMap = this.tbProdShortDescXpath.Text.Trim();
					sourceMapping.ProdShortDescVal = this.tbDefaultProdShortDesc_xml.Text.Trim();
					sourceMapping.ProdFullDescMap = this.tbProdFullDescXpath.Text.Trim();
					sourceMapping.ProdFullDescVal = this.tbDefaultProdFullDesc_xml.Text.Trim();
					sourceMapping.ProdManPartNumMap = this.tbProdManNumberXpath.Text.Trim();
					sourceMapping.ProdManPartNumVal = this.tbDefaultManufacturerPnum_xml.Text.Trim();
					sourceMapping.ProdStockMap = this.tbProdStockQuantityXpath.Text.ToString();
					sourceMapping.ProdStockVal = this.nmDefaultStockQuantity_xml.Text.ToString();
					sourceMapping.SeoMetaKeyMap = this.tbMetaKeywordsXpath.Text.ToString();
					sourceMapping.SeoMetaKeyVal = this.tbDefaultMetaKeywords.Text.ToString();
					sourceMapping.SeoMetaDescMap = this.tbMetaDescriptionXpath.Text.ToString();
					sourceMapping.SeoMetaDescVal = this.tbDefaultMetaDescription.Text.ToString();
					sourceMapping.SeoMetaTitleMap = this.tbMetaTitleXpath.Text.ToString();
					sourceMapping.SeoMetaTitleVal = this.tbDefaultMetaTitle.Text.ToString();
					sourceMapping.SeoPageMap = this.tbSearchEnginePageXpath.Text.ToString();
					sourceMapping.SeoPageVal = this.tbDefaultSearchEnginePage.Text.ToString();
					sourceMapping.CatMap = this.tbCategoryXPath.Text.ToString();
					sourceMapping.CatVal = this.tbDefaultCategory_xml.Text.ToString();
					sourceMapping.CatDelimeter = GlobalClass.ObjectTChar(this.tbCategoryDelimeter_xml.Text);
					sourceMapping.Cat1Map = this.tbCategoryXPath1.Text.ToString();
					sourceMapping.Cat1Val = this.tbDefaultCategory1_xml.Text.ToString();
					sourceMapping.Cat2Map = this.tbCategoryXPath2.Text.ToString();
					sourceMapping.Cat2Val = this.tbDefaultCategory2_xml.Text.ToString();
					sourceMapping.Cat3Map = this.tbCategoryXPath3.Text.ToString();
					sourceMapping.Cat3Val = this.tbDefaultCategory3_xml.Text.ToString();
					sourceMapping.Cat4Map = this.tbCategoryXPath4.Text.ToString();
					sourceMapping.Cat4Val = this.tbDefaultCategory4_xml.Text.ToString();
					sourceMapping.ManufactMap = this.tbManufacturerXPath.Text.ToString();
					sourceMapping.ManufactVal = this.tbDefaultManufacturer_xml.Text.ToString();
					sourceMapping.CatIsId = this.chCatId_xml.Checked;
					sourceMapping.Cat1IsId = this.chCat1Id_xml.Checked;
					sourceMapping.Cat2IsId = this.chCat2Id_xml.Checked;
					sourceMapping.Cat3IsId = this.chCat3Id_xml.Checked;
					sourceMapping.Cat4IsId = this.chCat4Id_xml.Checked;
					sourceMapping.ManufacturerIsId = this.chManufacturerId_xml.Checked;
					sourceMapping.VendorIsId = this.chVendorId_xml.Checked;
					sourceMapping.DeliveryDateIsId = this.chProdSettingsDeliveryDateIsID_xml.Checked;
					sourceMapping.VendorMap = this.tbVendorXpath.Text.Trim();
					sourceMapping.VendorVal = this.tbDefaultVendor_xml.Text.Trim();
					sourceMapping.GtinMap = this.textBox_0.Text.Trim();
					sourceMapping.GtinVal = this.tbDefaultGtinXPath.Text.Trim();
					sourceMapping.WeightMap = this.tbWeightXPath.Text.Trim();
					sourceMapping.WeightVal = this.tbDefaultWeight_xml.Text.ToString();
					sourceMapping.LengthMap = this.tbLengthXpath.Text.Trim();
					sourceMapping.LengthVal = this.tbDefaultLength_xml.Text.ToString();
					sourceMapping.WidthMap = this.tbWidthXpath.Text.Trim();
					sourceMapping.WidthVal = this.tbDefaultWidth_xml.Text.ToString();
					sourceMapping.HeightMap = this.tbHeightXpath.Text.Trim();
					sourceMapping.HeightVal = this.tbDefaultHeight_xml.Text.ToString();
					sourceMapping.ProdSettingsAllowedQtyMap = this.tbProdSettingsAllowedQty_xml.Text;
					sourceMapping.ProdSettingsAllowedQtyVal = this.tbDefaultProdSettingsAllowedQty_xml.Text;
					sourceMapping.ProdSettingsMinCartQtyMap = this.tbProdSettingsMinCartQty_xml.Text;
					sourceMapping.ProdSettingsMinCartQtyVal = this.tbDefaultProdSettingsMinCartQty_xml.Text;
					sourceMapping.ProdSettingsMaxCartQtyMap = this.tbProdSettingsMaxCartQty_xml.Text;
					sourceMapping.ProdSettingsMaxCartQtyVal = this.tbDefaultProdSettingsMaxCartQty_xml.Text;
					sourceMapping.ProdSettingsMinStockQtyMap = this.tbProdSettingsMinStockQty_xml.Text;
					sourceMapping.ProdSettingsMinStockQtyVal = this.tbDefaultProdSettingsMinStockQty_xml.Text;
					sourceMapping.ProdSettingsNotifyQtyMap = this.tbProdSettingsNotifyQty_xml.Text;
					sourceMapping.ProdSettingsNotifyQtyVal = this.tbDefaultProdSettingsNotifyQty_xml.Text;
					sourceMapping.ProdSettingsShippingEnabledMap = this.tbProdSettingsShippingEnabled_xml.Text.ToString();
					sourceMapping.ProdSettingsShippingEnabledVal = this.tbDefaultProdSettingsShippingEnabled_xml.Text;
					sourceMapping.ProdSettingsFreeShippingMap = this.tbProdSettingsFreeShipping_xml.Text.ToString();
					sourceMapping.ProdSettingsFreeShippingVal = this.tbDefaultProdSettingsFreeShipping_xml.Text;
					sourceMapping.ProdSettingsShipSeparatelyMap = this.tbProdSettingsShipSeparately_xml.Text.ToString();
					sourceMapping.ProdSettingsShipSeparatelyVal = this.tbDefaultProdSettingsShipSeparately_xml.Text;
					sourceMapping.ProdSettingsShippingChargeMap = this.tbProdSettingsShippingCharge_xml.Text.ToString();
					sourceMapping.ProdSettingsShippingChargeVal = this.tbDefaultProdSettingsShippingCharge_xml.Text;
					sourceMapping.ProdSettingsDeliveryDateMap = this.tbProdSettingsDeliveryDate_xml.Text.ToString();
					sourceMapping.ProdSettingsDeliveryDateVal = this.tbDefaultProdSettingsDeliveryDate_xml.Text;
					sourceMapping.ProdSettingsVisibleIndividuallyMap = this.tbProdSettingsVisibleIndvidually_xml.Text.ToString();
					sourceMapping.ProdSettingsVisibleIndividuallyVal = this.tbDefaultProdSettingsVisibleIndvidually_xml.Text;
					sourceMapping.ProdSettingsIsDeletedMap = this.tbProdSettingsIsDeleted_xml.Text.ToString();
					sourceMapping.ProdSettingsIsDeletedVal = this.tbDefaultProdSettingsIsDeleted_xml.Text;
					this.ProductInfoListSave(sourceMapping);
					this.ProductPriceListSave(sourceMapping);
					this.TierPricingListSave(sourceMapping);
					this.ProductAttributesListSave(sourceMapping);
					this.ProductSpecAttributesListSave(sourceMapping);
					this.ProductPicturesListSave(sourceMapping);
				}
				if (this.ddEntityType.SelectedIndex == 1 || this.ddEntityType.SelectedIndex == 0)
				{
					sourceMapping.CustomerRoleMap = this.tbCustomerRoleXpath.Text.ToString();
					sourceMapping.CustomerRoleVal = this.tbDefaultCustomerRole_xml.Text.ToString();
					sourceMapping.CustomerRoleDelimeter = GlobalClass.ObjectTChar(this.tbCustomerRoleDelimeter_xml.Text);
					sourceMapping.CustomerRole1Map = this.tbCustomerRoleXpath1.Text.ToString();
					sourceMapping.CustomerRole1Val = this.tbDefaultCustomerRole1_xml.Text.ToString();
					sourceMapping.CustomerRole2Map = this.tbCustomerRoleXpath2.Text.ToString();
					sourceMapping.CustomerRole2Val = this.tbDefaultCustomerRole2_xml.Text.ToString();
					sourceMapping.CustomerRole3Map = this.tbCustomerRoleXpath3.Text.ToString();
					sourceMapping.CustomerRole3Val = this.tbDefaultCustomerRole3_xml.Text.ToString();
					sourceMapping.CustomerRole4Map = this.tbCustomerRoleXpath4.Text.ToString();
					sourceMapping.CustomerRole4Val = this.tbDefaultCustomerRole4_xml.Text.ToString();
					sourceMapping.CustomerRoleIsId = this.chCustomerRoleID_xml.Checked;
					sourceMapping.CustomerRole1IsId = this.chCustomerRoleID1_xml.Checked;
					sourceMapping.CustomerRole2IsId = this.chCustomerRoleID2_xml.Checked;
					sourceMapping.CustomerRole3IsId = this.chCustomerRoleID3_xml.Checked;
					sourceMapping.CustomerRole4IsId = this.chCustomerRoleID4_xml.Checked;
					sourceMapping.FilterName = this.tbFilterName_xml.Text.ToString();
					sourceMapping.FilterMap = this.tbFilterXpath.Text.ToString();
					sourceMapping.Filter1Name = this.tbFilterName1_xml.Text.ToString();
					sourceMapping.Filter1Map = this.tbFilterXpath1.Text.ToString();
					sourceMapping.Filter2Name = this.tbFilterName2_xml.Text.ToString();
					sourceMapping.Filter2Map = this.tbFilterXpath2.Text.ToString();
					sourceMapping.Filter3Name = this.tbFilterName3_xml.Text.ToString();
					sourceMapping.Filter3Map = this.tbFilterXpath3.Text.ToString();
					sourceMapping.Filter4Name = this.tbFilterName4_xml.Text.ToString();
					sourceMapping.Filter4Map = this.tbFilterXpath4.Text.ToString();
					sourceMapping.Filter5Name = this.tbFilterName5_xml.Text.ToString();
					sourceMapping.Filter5Map = this.tbFilterXpath5.Text.ToString();
					sourceMapping.Filter6Name = this.tbFilterName6_xml.Text.ToString();
					sourceMapping.Filter6Map = this.tbFilterXpath6.Text.ToString();
					sourceMapping.Filter7Name = this.tbFilterName7_xml.Text.ToString();
					sourceMapping.Filter7Map = this.tbFilterXpath7.Text.ToString();
					sourceMapping.Filter8Name = this.tbFilterName8_xml.Text.ToString();
					sourceMapping.Filter8Map = this.tbFilterXpath8.Text.ToString();
					sourceMapping.Filter9Name = this.tbFilterName9_xml.Text.ToString();
					sourceMapping.Filter9Map = this.tbFilterXpath9.Text.ToString();
				}
				if (this.ddEntityType.SelectedIndex == 1)
				{
					this.CustomerListSave(sourceMapping);
					this.CustomerAddressListSave(sourceMapping);
					this.CustomerRoleListSave(sourceMapping);
					this.AddressCustomAttributesListSave(sourceMapping);
					this.CustomerCustomAttributesListSave(sourceMapping);
					this.CustomerOthersListSave(sourceMapping);
				}
				if (this.ddEntityType.SelectedIndex == 2)
				{
					this.WishListSave(sourceMapping);
				}
			}
			if (this.ddSourceFormat.Text == "CSV" || this.ddSourceFormat.Text == "XLSX")
			{
				sourceMapping.CsvDelimeter = GlobalClass.ObjectTChar(this.tbCsvDelimeter.Text);
				sourceMapping.FirstRowIndex = new int?(GlobalClass.StringToInteger(this.nmFirstRowDataIndex.Value, 0));
				sourceMapping.VendorSourceId = new int?(this.vendorSourceId);
				if (this.ddEntityType.SelectedIndex == 0)
				{
					sourceMapping.ProdInfoRepeatNode = string.Empty;
					sourceMapping.ProdIdMap = this.nmProdId_pos.Value.ToString();
					sourceMapping.ProdGroupByMap = this.nmProdGroupBy_pos.Value.ToString();
					sourceMapping.ProdSkuMap = this.nmProdSKU_pos.Value.ToString();
					sourceMapping.ProdSkuVal = this.nmDefaultProdSKU_pos.Text.Trim();
					sourceMapping.ProdNameMap = this.nmProdName_pos.Value.ToString();
					sourceMapping.ProdNameVal = this.tbDefaultProdName_pos.Text.Trim();
					sourceMapping.ProdShortDescMap = this.nmProdShortDesc_pos.Value.ToString();
					sourceMapping.ProdShortDescVal = this.tbDefaultProdShortDesc_pos.Text.Trim();
					sourceMapping.ProdFullDescMap = this.nmProdFullDesc_pos.Value.ToString();
					sourceMapping.ProdFullDescVal = this.tbDefaultProdFullDesc_pos.Text.Trim();
					sourceMapping.ProdManPartNumMap = this.nmProdManPartNum_pos.Value.ToString();
					sourceMapping.ProdManPartNumVal = this.tbProdManPartNum_pos.Text.Trim();
					sourceMapping.ProdStockMap = this.nmProdStock_pos.Text.ToString();
					sourceMapping.ProdStockVal = this.nmDefaultProdStock_pos.Text.ToString();
					sourceMapping.SeoMetaKeyMap = this.nmSeoMetaKey_pos.Value.ToString();
					sourceMapping.SeoMetaKeyVal = this.tbDefaultSeoMetaKey_pos.Text.ToString();
					sourceMapping.SeoMetaDescMap = this.nmSeoMetaDesc_pos.Value.ToString();
					sourceMapping.SeoMetaDescVal = this.tbDefaultSeoMetaDesc_pos.Text.ToString();
					sourceMapping.SeoMetaTitleMap = this.nmSeoMetaTitle_pos.Value.ToString();
					sourceMapping.SeoMetaTitleVal = this.tbDefaultSeoMetaTitle_pos.Text.ToString();
					sourceMapping.SeoPageMap = this.nmSeoSearchPage_pos.Value.ToString();
					sourceMapping.SeoPageVal = this.tbDefaultSeoSearchPage_pos.Text.ToString();
					sourceMapping.CatMap = this.nmCategory_pos.Value.ToString();
					sourceMapping.CatVal = this.tbDefaultCategory_pos.Text.ToString();
					sourceMapping.CatDelimeter = GlobalClass.ObjectTChar(this.tbCategoryDelimeter_pos.Text.ToString());
					sourceMapping.Cat1Map = this.nmCategory1_pos.Value.ToString();
					sourceMapping.Cat1Val = this.tbDefaultCategory1_pos.Text.ToString();
					sourceMapping.Cat2Map = this.nmCategory2_pos.Value.ToString();
					sourceMapping.Cat2Val = this.tbDefaultCategory2_pos.Text.ToString();
					sourceMapping.Cat3Map = this.nmCategory3_pos.Value.ToString();
					sourceMapping.Cat3Val = this.tbDefaultCategory3_pos.Text.ToString();
					sourceMapping.Cat4Map = this.nmCategory4_pos.Value.ToString();
					sourceMapping.Cat4Val = this.tbDefaultCategory4_pos.Text.ToString();
					sourceMapping.ManufactMap = this.nmManufacturer_pos.Value.ToString();
					sourceMapping.ManufactVal = this.tbDefaultManufacturer_pos.Text.ToString();
					sourceMapping.CatIsId = this.chCatId_pos.Checked;
					sourceMapping.Cat1IsId = this.chCat1Id_pos.Checked;
					sourceMapping.Cat2IsId = this.chCat2Id_pos.Checked;
					sourceMapping.Cat3IsId = this.chCat3Id_pos.Checked;
					sourceMapping.Cat4IsId = this.chCat4Id_pos.Checked;
					sourceMapping.ManufacturerIsId = this.chManufacturerId_pos.Checked;
					sourceMapping.VendorIsId = this.chVendorId_pos.Checked;
					sourceMapping.DeliveryDateIsId = this.chProdSettingsDeliveryDateIsID_pos.Checked;
					sourceMapping.VendorMap = this.nmVendor_pos.Value.ToString();
					sourceMapping.VendorVal = this.tbDefaultVendor_pos.Text.Trim();
					sourceMapping.GtinMap = this.nmGtin_pos.Value.ToString();
					sourceMapping.GtinVal = this.tbDefaultGtin_pos.Text.Trim();
					sourceMapping.WeightMap = this.nmWeight_pos.Value.ToString();
					sourceMapping.WeightVal = this.tbDefaultWeight_pos.Text.ToString();
					sourceMapping.LengthMap = this.nmLength_pos.Text.Trim();
					sourceMapping.LengthVal = this.tbDefaultLength_pos.Text.ToString();
					sourceMapping.WidthMap = this.nmWidth_pos.Text.Trim();
					sourceMapping.WidthVal = this.tbDefaultWidth_pos.Text.ToString();
					sourceMapping.HeightMap = this.nmHeight_pos.Text.Trim();
					sourceMapping.HeightVal = this.tbDefaultHeight_pos.Text.ToString();
					sourceMapping.ProdSettingsAllowedQtyMap = this.nmProdSettingsAllowedQty_pos.Value.ToString();
					sourceMapping.ProdSettingsAllowedQtyVal = this.tbDefaultProdSettingsAllowedQty_pos.Text;
					sourceMapping.ProdSettingsMinCartQtyMap = this.nmProdSettingsMinCartQty_pos.Value.ToString();
					sourceMapping.ProdSettingsMinCartQtyVal = this.tbDefaultProdSettingsMinCartQty_pos.Text;
					sourceMapping.ProdSettingsMaxCartQtyMap = this.nmProdSettingsMaxCartQty_pos.Value.ToString();
					sourceMapping.ProdSettingsMaxCartQtyVal = this.tbDefaultProdSettingsMaxCartQty_pos.Text;
					sourceMapping.ProdSettingsMinStockQtyMap = this.nmProdSettingsMinStockQty_pos.Value.ToString();
					sourceMapping.ProdSettingsMinStockQtyVal = this.tbDefaultProdSettingsMinStockQty_pos.Text;
					sourceMapping.ProdSettingsNotifyQtyMap = this.nmProdSettingsNotifyQty_pos.Value.ToString();
					sourceMapping.ProdSettingsNotifyQtyVal = this.tbDefaultProdSettingsNotifyQty_pos.Text.ToString();
					sourceMapping.ProdSettingsShippingEnabledMap = this.nmProdSettingsShippingEnabled_pos.Value.ToString();
					sourceMapping.ProdSettingsShippingEnabledVal = this.tbDefaultProdSettingsShippingEnabled_pos.Text;
					sourceMapping.ProdSettingsFreeShippingMap = this.nmProdSettingsFreeShipping_pos.Value.ToString();
					sourceMapping.ProdSettingsFreeShippingVal = this.tbDefaultProdSettingsFreeShipping_pos.Text;
					sourceMapping.ProdSettingsShipSeparatelyMap = this.nmProdSettingsShipSeparately_pos.Value.ToString();
					sourceMapping.ProdSettingsShipSeparatelyVal = this.tbDefaultProdSettingsShipSeparately_pos.Text;
					sourceMapping.ProdSettingsShippingChargeMap = this.nmProdSettingsShippingCharge_pos.Value.ToString();
					sourceMapping.ProdSettingsShippingChargeVal = this.tbDefaultProdSettingsShippingCharge_pos.Text;
					sourceMapping.ProdSettingsDeliveryDateMap = this.nmProdSettingsDeliveryDate_pos.Value.ToString();
					sourceMapping.ProdSettingsDeliveryDateVal = this.tbDefaultProdSettingsDeliveryDate_pos.Text;
					sourceMapping.ProdSettingsVisibleIndividuallyMap = this.nmProdSettingsVisibleIndvidually_pos.Value.ToString();
					sourceMapping.ProdSettingsVisibleIndividuallyVal = this.tbDefaultProdSettingsVisibleIndvidually_pos.Text;
					sourceMapping.ProdSettingsIsDeletedMap = this.nmProdSettingsIsDeleted_pos.Value.ToString();
					sourceMapping.ProdSettingsIsDeletedVal = this.tbDefaultProdSettingsIsDeleted_pos.Text;
					this.ProductInfoListSave(sourceMapping);
					this.ProductPriceListSave(sourceMapping);
					this.TierPricingListSave(sourceMapping);
					this.ProductAttributesListSave(sourceMapping);
					this.ProductSpecAttributesListSave(sourceMapping);
					this.ProductPicturesListSave(sourceMapping);
				}
				if (this.ddEntityType.SelectedIndex == 1 || this.ddEntityType.SelectedIndex == 0)
				{
					sourceMapping.CustomerRoleMap = this.nmCustomerRole_pos.Value.ToString();
					sourceMapping.CustomerRoleVal = this.tbDefaultCustomerRole_pos.Text.ToString();
					sourceMapping.CustomerRoleDelimeter = GlobalClass.ObjectTChar(this.tbCustomerRoleDelimiter_pos.Text.ToString());
					sourceMapping.CustomerRole1Map = this.nmCustomerRole1_pos.Value.ToString();
					sourceMapping.CustomerRole1Val = this.tbDefaultCustomerRole1_pos.Text.ToString();
					sourceMapping.CustomerRole2Map = this.nmCustomerRole2_pos.Value.ToString();
					sourceMapping.CustomerRole2Val = this.tbDefaultCustomerRole2_pos.Text.ToString();
					sourceMapping.CustomerRole3Map = this.nmCustomerRole3_pos.Value.ToString();
					sourceMapping.CustomerRole3Val = this.tbDefaultCustomerRole3_pos.Text.ToString();
					sourceMapping.CustomerRole4Map = this.nmCustomerRole4_pos.Value.ToString();
					sourceMapping.CustomerRole4Val = this.tbDefaultCustomerRole4_pos.Text.ToString();
					sourceMapping.CustomerRoleIsId = this.chCustomerRoleId_pos.Checked;
					sourceMapping.CustomerRole1IsId = this.chCustomerRoleId1_pos.Checked;
					sourceMapping.CustomerRole2IsId = this.chCustomerRoleId2_pos.Checked;
					sourceMapping.CustomerRole3IsId = this.chCustomerRoleId3_pos.Checked;
					sourceMapping.CustomerRole4IsId = this.chCustomerRoleId4_pos.Checked;
					sourceMapping.FilterName = this.tbFilterName_pos.Text.ToString();
					sourceMapping.FilterMap = this.nmFilterValue_pos.Text.ToString();
					sourceMapping.Filter1Name = this.tbFilter1Name_pos.Text.ToString();
					sourceMapping.Filter1Map = this.nmFilter1Value_pos.Text.ToString();
					sourceMapping.Filter2Name = this.tbFilter2Name_pos.Text.ToString();
					sourceMapping.Filter2Map = this.nmFilter2Value_pos.Text.ToString();
					sourceMapping.Filter3Name = this.tbFilter3Name_pos.Text.ToString();
					sourceMapping.Filter3Map = this.nmFilter3Value_pos.Text.ToString();
					sourceMapping.Filter4Name = this.tbFilter4Name_pos.Text.ToString();
					sourceMapping.Filter4Map = this.nmFilter4Value_pos.Text.ToString();
					sourceMapping.Filter5Name = this.tbFilter5Name_pos.Text.ToString();
					sourceMapping.Filter5Map = this.nmFilter5Value_pos.Text.ToString();
					sourceMapping.Filter6Name = this.tbFilter6Name_pos.Text.ToString();
					sourceMapping.Filter6Map = this.nmFilter6Value_pos.Text.ToString();
					sourceMapping.Filter7Name = this.tbFilter7Name_pos.Text.ToString();
					sourceMapping.Filter7Map = this.nmFilter7Value_pos.Text.ToString();
					sourceMapping.Filter8Name = this.tbFilter8Name_pos.Text.ToString();
					sourceMapping.Filter8Map = this.nmFilter8Value_pos.Text.ToString();
					sourceMapping.Filter9Name = this.tbFilter9Name_pos.Text.ToString();
					sourceMapping.Filter9Map = this.nmFilter9Value_pos.Text.ToString();
				}
				if (this.ddEntityType.SelectedIndex == 1)
				{
					this.CustomerListSave(sourceMapping);
					this.CustomerAddressListSave(sourceMapping);
					this.CustomerRoleListSave(sourceMapping);
					this.AddressCustomAttributesListSave(sourceMapping);
					this.CustomerCustomAttributesListSave(sourceMapping);
					this.CustomerOthersListSave(sourceMapping);
				}
				if (this.ddEntityType.SelectedIndex == 2)
				{
					this.WishListSave(sourceMapping);
				}
			}
			return sourceMapping;
		}

		private sourceMappingDataGrid sourceMappingDgTierPricing { get; set; }

		private void ProductTierPricingTabLoad()
		{
			if (!this.tabSourceConfig.Contains(this.tabTierPricing))
			{
				this.tabSourceConfig.TabPages.Add(this.tabTierPricing);
			}
		}

		private void TierPricingListLoad(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			this.tabTierPricing.Controls.Clear();
			this.sourceMappingDgTierPricing = new sourceMappingDataGrid(SourceMappingType.TierPricing, structureFormat);
			this.sourceMappingDgTierPricing.SourceMappingListLoad(sourceMapping, this._sourceFieldList);
			this.sourceMappingDgTierPricing.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.sourceMappingDgTierPricing.Dock = DockStyle.Fill;
			this.tabTierPricing.Controls.Add(this.sourceMappingDgTierPricing);
		}

		private void TierPricingListSave(SourceMapping sourceMapping)
		{
			if (this.sourceMappingDgTierPricing != null)
			{
				this.sourceMappingDgTierPricing.SourceMappingListSave(sourceMapping);
			}
		}

		private void tbProductRepeatNodeXpath_Validated(object sender, EventArgs e)
		{
			this.errorProvider1.SetError(this.tbProductRepeatNodeXpath, string.Empty);
		}

		private void tbProductRepeatNodeXpath_Validating(object sender, CancelEventArgs e)
		{
			if (this.tabSourceConfig.Contains(this.tabProductInfoMappingXml))
			{
				bool cancel = false;
				if (string.IsNullOrEmpty(this.tbProductRepeatNodeXpath.Text))
				{
					cancel = true;
					this.errorProvider1.SetError(this.tbProductRepeatNodeXpath, "You must provide repeat node xpath!");
					this.tabSourceConfig.SelectedTab = this.tabProductInfoMappingXml;
				}
				e.Cancel = cancel;
			}
		}

		private void tbProdIDXpath_Validated(object sender, EventArgs e)
		{
			this.errorProvider1.SetError(this.tbProdIDXpath, string.Empty);
		}

		private void tbProdIDXpath_Validating(object sender, CancelEventArgs e)
		{
			if (this.tabSourceConfig.Contains(this.tabProductInfoMappingXml))
			{
				bool cancel = false;
				if (string.IsNullOrEmpty(this.tbProdIDXpath.Text))
				{
					cancel = true;
					this.errorProvider1.SetError(this.tbProdIDXpath, "You must provide repeat product id xpath!");
					this.tabSourceConfig.SelectedTab = this.tabProductInfoMappingXml;
				}
				e.Cancel = cancel;
			}
		}

		private void nmProdId_pos_Validated(object sender, EventArgs e)
		{
			this.errorProvider1.SetError(this.nmProdId_pos, string.Empty);
		}

		private void nmProdId_pos_Validating(object sender, CancelEventArgs e)
		{
			if (this.tabSourceConfig.Contains(this.tabProductInfoMappingPos))
			{
				bool cancel = false;
				if (this.nmProdId_pos.Value < 0m)
				{
					cancel = true;
					this.errorProvider1.SetError(this.nmProdId_pos, "You must provide repeat product id column index!");
					this.tabSourceConfig.SelectedTab = this.tabProductInfoMappingPos;
				}
				e.Cancel = cancel;
			}
		}

		private int InsertVendor()
		{
			return this.sourceMappingFunc.InsertVendor(this.tbName.Text.Trim(), this.tbDescription.Text.Trim());
		}

		private void UpdateVendor()
		{
			this.sourceMappingFunc.UpdateVendor(this.vendorId, this.tbName.Text.Trim(), this.tbDescription.Text.Trim());
		}

		private void LoadVendorData()
		{
			if (this.vendorId > 0)
			{
				VendorData vendorData = this.sourceMappingFunc.GetVendorData(this.vendorId);
				this.vendorSourceId = vendorData.VendorSourceId.GetValueOrDefault();
				this.vendorSourceMappingId = vendorData.VendorSourceMappingId.GetValueOrDefault();
				this.tbId.Text = vendorData.VendorId.ToString();
				this.tbName.Text = vendorData.VendorName;
				this.tbDescription.Text = vendorData.VendorDescription;
			}
		}

		private void tbName_Validating(object sender, CancelEventArgs e)
		{
			bool cancel = false;
			if (string.IsNullOrEmpty(this.tbName.Text))
			{
				cancel = true;
				this.errorProvider1.SetError(this.tbName, "You must provide Name!");
			}
			e.Cancel = cancel;
		}

		private void tbName_Validated(object sender, EventArgs e)
		{
			this.errorProvider1.SetError(this.tbName, string.Empty);
		}

		private Dictionary<string, string> _sourceFieldList { get; set; }

		public vendorNewEdit(int i, frmVendorsList owner)
		{
			
			this.tabCustomerOthersMapping = new TabPage("Customer Others");
			this.tabCustomerRoleMapping = new TabPage("Customer Role");
			this.tabCustGenAttAttMapping = new TabPage("Address Custom Attributes");
			this.tabCustomerShippingAddressMapping = new TabPage("Shipping Address");
			this.tabCustomerBillingAddressMapping = new TabPage("Billing Address");
			this.tabCustomerMapping = new TabPage("Customer");
			this.tabCustCustomAttAttMapping = new TabPage("Customer Custom Attributes");
			this.tabProductPictures = new TabPage("Pictures");
			this.tabProductPrice = new TabPage("Prices");
			this.tabProductSpecAttrbuttes = new TabPage("Specification Attributes");
			this.tabProductAttrbuttes = new TabPage("Product Attributes");
			this.tabProductInfo = new TabPage("Others");
			this.dataSettingsManager = new DataSettingsManager();
			this.vendorSourceloaded = false;
			this.firstLoad = true;
			this.tabTierPricing = new TabPage("Tier Pricing");
			this.vendorId = 0;
			this.vendorSourceId = 0;
			this.vendorSourceMappingId = 0;
			this.sourceMappingFunc = new SourceMappingFunc();
			this.taskMappingFunc = new TaskMappingFunc();
			this.components = null;
			this.tabWishListMapping = new TabPage("Wish List");
			this._owner = owner;
			this.InitializeComponent();
			this.vendorId = i;
			this.AutoValidate = AutoValidate.Disable;
			base.FormClosing += this.vendorNewEdit_FormClosing;
			this.tabSourceConfig.Visible = false;
			this.tableLayoutPanel4.Visible = false;
			Cursor.Current = Cursors.WaitCursor;
		}

		private async void vendorNewEdit_Load(object sender, EventArgs e)
		{
			IEnumerable<Control> c = this.GetAll();
			foreach (Control x in c)
			{
				x.SetDoubleBuffered();
			}
			AutoSize = true;
			base.AutoSizeMode = AutoSizeMode.GrowAndShrink;
			Cursor.Current = Cursors.WaitCursor;
			LoadInitialData();
			LoadData();
			LoadUserControls();
			Cursor.Current = Cursors.Arrow;
			
		}

		private void LoadUserControls()
		{
		}

		private void LoadSourceFields(byte[] fileContent)
		{
			try
			{
				if (!string.IsNullOrEmpty(this.tbSourceAddress.Text))
				{
					FileFormat dataFormatType = FileFormat.XML;
					string fileName = Path.GetFileName(this.tbSourceAddress.Text);
					string str = string.Format("{0}_{1}_{2}", this.vendorSourceId, this.vendorSourceMappingId, fileName);
					string text = "Temp\\" + str;
					if (File.Exists(text))
					{
						if (fileContent == null)
						{
							fileContent = this.sourceMappingFunc.GetFileContent(SourceType.FileSytem, null, null, text, null, null, this.ddSourceEncoding.SelectedValue.ToString(), this.ddSourceFormat.Text == "XLSX", null, null);
						}
					}
					else
					{
						if (fileContent == null)
						{
							fileContent = this.sourceMappingFunc.GetFileContent((SourceType)this.ddSourceType.SelectedIndex, this.tbSourceUserName.Text, this.tbSourcePassword.Text, this.tbSourceAddress.Text, this.tbSoapAction.Text, this.tbSoapRequest.Text, this.ddSourceEncoding.SelectedValue.ToString(), this.ddSourceFormat.Text == "XLSX", this.tbAliexpressProductsUrls.Text, this.tbAuthHeader.Text);
						}
						if (fileContent != null)
						{
							if (!Directory.Exists("Temp\\"))
							{
								Directory.CreateDirectory("Temp\\");
							}

							File.WriteAllBytes(text, fileContent);
						}
					}
					if (this.ddSourceFormat.Text == "XML")
					{
						dataFormatType = FileFormat.XML;
					}
					if (this.ddSourceFormat.Text == "XLSX")
					{
						dataFormatType = FileFormat.XLSX;
					}
					if (this.ddSourceFormat.Text == "CSV")
					{
						dataFormatType = FileFormat.CSV;
					}
					Cursor.Current = Cursors.Default;
					if (fileContent != null)
					{

						if(xmlModify.Checked == true)
                        {
							this._sourceFieldList = this.sourceMappingFunc.GetAllSourceFieldsXmlModifyAsync(fileContent, dataFormatType, GlobalClass.StringToInteger(this.nmFirstRowDataIndex.Value, 0), this.tbCsvDelimeter.Text);
							this.LoadSourceFieldsToView();
						}
						else
                        {
							this._sourceFieldList = this.sourceMappingFunc.GetAllSourceFieldsAsync(fileContent, dataFormatType, GlobalClass.StringToInteger(this.nmFirstRowDataIndex.Value, 0), this.tbCsvDelimeter.Text);
							this.LoadSourceFieldsToView();
						}

					}
				}
			}
			catch (Exception ex)
			{
				LogManager.LogWrite(null, "Source file is not valid for parsing. Error=" + ex.ToString(), LogType.Warning, null);
				MessageBox.Show("Source file is not valid for parsing. View Logs for more details.");
			}
		}

		private void LoadInitialData()
		{
			helper.FillEncodingDropDownList(this.ddSourceEncoding);
			this.tbAliexpressProductsUrls.WordWrap = false;
			this.tbAliexpressProductsUrls.ScrollBars = ScrollBars.Both;
			this.btnCopy.Enabled = (this.vendorId > 0);
			if (ImportManager.productsLimits > 0)
			{
				this.tbAliexpressProductsUrls.ReadOnly = true;
				Label label = this.lblAliExpressUrlLabel;
				label.Text = label.Text + Environment.NewLine + "(THE LIST IS READONLY FOR THIS LICENCE. Please contact www.vasksoft.com)";
			}
		}

		private void LoadData()
		{
			this.btnCopy.Enabled = (this.vendorId > 0);
			if (this.vendorId > 0)
			{
				this.LoadVendorData();
				this.LoadVendorSourceData();
				this.LoadSourceFields(null);
				if (ddSourceFormat.Text == "XML")
				{				  
					this.LoadVendorSourceMappingDataForXml();

				}
				if (this.ddSourceFormat.Text == "CSV" || this.ddSourceFormat.Text == "XLSX")
				{
					this.LoadVendorSourceMappingDataForPos();
				}
				if (this.ddSourceFormat.Text == "CSV")
				{
					this.lblCsvDelimeter.Show();
					this.tbCsvDelimeter.Show();
				}
				else
				{
					this.tbCsvDelimeter.Hide();
					this.lblCsvDelimeter.Hide();
				}
				if (!this.tabSourceConfig.Contains(this.tabVendorSource))
				{
					this.tabSourceConfig.TabPages.Add(this.tabVendorSource);
				}
				this.btnTestMapping.Show();
			}
			else
			{
				if (this.tabSourceConfig.Contains(this.tabVendorSource))
				{
					this.tabSourceConfig.TabPages.Remove(this.tabVendorSource);
				}
				this.btnTestMapping.Hide();
			}
			if (this.firstLoad)
			{
				this.LoadTabs();
			}
			this.firstLoad = false;
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				if (this.ValidateChildren(ValidationConstraints.Enabled))
				{
					if (this.vendorId > 0)
					{
						this.UpdateVendor();
					}
					else
					{
						this.vendorId = this.InsertVendor();
						this._owner.gridFill();
					}
					if (this.tabSourceConfig.Contains(this.tabVendorSource))
					{
						if (this.vendorSourceId > 0)
						{
							this.UpdateVendorSource();
						}
						else
						{
							this.vendorSourceId = this.InsertVendorSource();
						}
						if (this.ddSourceType.SelectedIndex == 4)
						{
							this.SaveSourceMappingAliExpress();
						}
						else if (this.ddSourceType.SelectedIndex == 5)
						{
							this.SaveSourceMappingBestSecret();
						}
						else
						{
							if (this.ddSourceFormat.Text == "XML")
							{
								this.SaveSourceMappingXml();
							}
							if (this.ddSourceFormat.Text == "CSV" || this.ddSourceFormat.Text == "XLSX")
							{
								this.SaveSourceMappingPos();
							}
						}
					}
					this.firstLoad = true;
					this.LoadData();
					this.SetStatusArea(true, "Data saved successfully.");
				}
				else
				{
					this.SetStatusArea(false, "Data not saved.");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
				this.SetStatusArea(false, "Data not saved.");
			}
		}

		private void vendorNewEdit_FormClosing(object sender, FormClosingEventArgs e)
		{
			this._owner.gridFill();
		}

		private void tabSourceConfig_SelectedIndexChanged(object sender, EventArgs e)
		{
		}

		private void LoadTabs()
		{
			this.tabSourceConfig.TabPages.Clear();
			if (!this.tabSourceConfig.Contains(this.tabVendor))
			{
				this.tabSourceConfig.TabPages.Add(this.tabVendor);
			}
			if (this.ddSourceType.SelectedIndex == 4 || this.ddSourceType.SelectedIndex == 5)
			{
				if (!this.tabSourceConfig.Contains(this.tabVendorSource))
				{
					this.tabSourceConfig.TabPages.Add(this.tabVendorSource);
				}
				if (!this.tabSourceConfig.Contains(this.tabAliExpress))
				{
					this.tabSourceConfig.TabPages.Add(this.tabAliExpress);
				}
			}
			else if (this.ddSourceFormat.Text.ToUpper() == "XML")
			{
				if (this.ddEntityType.SelectedIndex == 2)
				{
					if (!this.tabSourceConfig.Contains(this.tabVendorSource))
					{
						this.tabSourceConfig.TabPages.Add(this.tabVendorSource);
					}
					this.WishListTabLoad();
				}
				else if (this.ddEntityType.SelectedIndex == 1)
				{
					if (!this.tabSourceConfig.Contains(this.tabVendorSource))
					{
						this.tabSourceConfig.TabPages.Add(this.tabVendorSource);
					}
					this.CustomerTabLoad();
					this.CustomerAddressTabLoad();
					this.CustomerRoleTabLoad();
					this.CustomerCustomAttributesTabLoad();
					this.AddressCustomAttributesTabLoad();
					this.CustomerOthersTabLoad();
				}
				else
				{
					if (!this.tabSourceConfig.Contains(this.tabVendorSource))
					{
						this.tabSourceConfig.TabPages.Add(this.tabVendorSource);
					}
					if (!this.tabSourceConfig.Contains(this.tabProductInfoMappingXml))
					{
						this.tabSourceConfig.TabPages.Add(this.tabProductInfoMappingXml);
					}
					if (!this.tabSourceConfig.Contains(this.tabProductSettingsXml))
					{
						this.tabSourceConfig.TabPages.Add(this.tabProductSettingsXml);
					}
					if (!this.tabSourceConfig.Contains(this.tabSeoMappingXml))
					{
						this.tabSourceConfig.TabPages.Add(this.tabSeoMappingXml);
					}
					if (!this.tabSourceConfig.Contains(this.tabCatMappingXml))
					{
						this.tabSourceConfig.TabPages.Add(this.tabCatMappingXml);
					}
					if (!this.tabSourceConfig.Contains(this.tabAccessRolesMappingXml))
					{
						this.tabSourceConfig.TabPages.Add(this.tabAccessRolesMappingXml);
					}
					if (!this.tabSourceConfig.Contains(this.tabManufMappingXml))
					{
						this.tabSourceConfig.TabPages.Add(this.tabManufMappingXml);
					}
					this.ProductPicturesTabLoad();
					this.ProductAttributesTabLoad();
					this.ProductSpecAttributesTabLoad();
					this.ProductPriceTabLoad();
					this.ProductTierPricingTabLoad();
					if (!this.tabSourceConfig.Contains(this.tabFilterMappingXml))
					{
						this.tabSourceConfig.TabPages.Add(this.tabFilterMappingXml);
					}
					this.ProductInfoTabLoad();
				}
			}
			else if (this.ddSourceFormat.Text.ToUpper() == "CSV" || this.ddSourceFormat.Text.ToUpper() == "XLSX")
			{
				if (this.ddEntityType.SelectedIndex == 2)
				{
					if (!this.tabSourceConfig.Contains(this.tabVendorSource))
					{
						this.tabSourceConfig.TabPages.Add(this.tabVendorSource);
					}
					this.WishListTabLoad();
				}
				else if (this.ddEntityType.SelectedIndex == 1)
				{
					if (!this.tabSourceConfig.Contains(this.tabVendorSource))
					{
						this.tabSourceConfig.TabPages.Add(this.tabVendorSource);
					}
					this.CustomerTabLoad();
					this.CustomerAddressTabLoad();
					this.CustomerRoleTabLoad();
					this.CustomerCustomAttributesTabLoad();
					this.AddressCustomAttributesTabLoad();
					this.CustomerOthersTabLoad();
				}
				else
				{
					if (!this.tabSourceConfig.Contains(this.tabVendorSource))
					{
						this.tabSourceConfig.TabPages.Add(this.tabVendorSource);
					}
					if (!this.tabSourceConfig.Contains(this.tabProductInfoMappingPos))
					{
						this.tabSourceConfig.TabPages.Add(this.tabProductInfoMappingPos);
					}
					if (!this.tabSourceConfig.Contains(this.tabProductSettingsPos))
					{
						this.tabSourceConfig.TabPages.Add(this.tabProductSettingsPos);
					}
					if (!this.tabSourceConfig.Contains(this.tabSeoMappingPos))
					{
						this.tabSourceConfig.TabPages.Add(this.tabSeoMappingPos);
					}
					if (!this.tabSourceConfig.Contains(this.tabCatMappingPos))
					{
						this.tabSourceConfig.TabPages.Add(this.tabCatMappingPos);
					}
					if (!this.tabSourceConfig.Contains(this.tabAccessRolesMappingPos))
					{
						this.tabSourceConfig.TabPages.Add(this.tabAccessRolesMappingPos);
					}
					if (!this.tabSourceConfig.Contains(this.tabManufMappingPos))
					{
						this.tabSourceConfig.TabPages.Add(this.tabManufMappingPos);
					}
					this.ProductPicturesTabLoad();
					this.ProductAttributesTabLoad();
					this.ProductSpecAttributesTabLoad();
					this.ProductPriceTabLoad();
					this.ProductTierPricingTabLoad();
					if (!this.tabSourceConfig.Contains(this.tabFilterMappingPos))
					{
						this.tabSourceConfig.TabPages.Add(this.tabFilterMappingPos);
					}
					this.ProductInfoTabLoad();
				}
			}
			if (this.tabSourceConfig.Contains(this.tabVendorSource))
			{
				this.tabSourceConfig.SelectedTab = this.tabVendorSource;
			}
		}

		private async void btnTestMapping_Click(object sender, EventArgs e)
		{
			try
			{
				FileFormat fileFormat = FileFormat.XML;
				Cursor.Current = Cursors.WaitCursor;
				if(xmlModify.Checked == true)
				{

					string ticimaxSave = @"Temp\\lingerium.xml";
					byte[] fileContent = sourceMappingFunc.GetFileContent((SourceType)ddSourceType.SelectedIndex, tbSourceUserName.Text, tbSourcePassword.Text, ticimaxSave, tbSoapAction.Text, tbSoapRequest.Text, ddSourceEncoding.SelectedValue.ToString(), ddSourceFormat.Text == "XLSX", tbAliexpressProductsUrls.Text, tbAuthHeader.Text);
					SourceDocument sourceDocument = null;
					Cursor.Current = Cursors.Default;
					if (fileContent != null)
					{
						RefreshSourceFileTicimax(fileContent);
						SourceMapping sourceMapping = GetSourceMappingFromForm();
						if (ddSourceType.SelectedIndex == 4)
						{
							fileFormat = FileFormat.AliExpress;
							sourceDocument = new SourceDocument(FileFormat.AliExpress, fileContent, sourceMapping, null, new SourceData
							{
								SourceUser = tbSourceUserName.Text,
								SourcePassword = tbSourcePassword.Text,
								SourceEncoding = ddSourceEncoding.SelectedValue.ToString()
							}, loadByOne: true);
						}
						else if (ddSourceType.SelectedIndex == 5)
						{
							fileFormat = FileFormat.BestSecret;
							sourceDocument = new SourceDocument(FileFormat.BestSecret, fileContent, sourceMapping, null, new SourceData
							{
								SourceAddress = tbSourceAddress.Text,
								SourceUser = tbSourceUserName.Text,
								SourcePassword = tbSourcePassword.Text,
								SourceEncoding = ddSourceEncoding.SelectedValue.ToString()
							}, loadByOne: true);
						}
						else
						{
							if (ddSourceFormat.Text == "XML")
							{
								sourceDocument = new SourceDocument(FileFormat.XML, fileContent, sourceMapping, null, new SourceData
								{
									SourceUser = tbSourceUserName.Text,
									SourcePassword = tbSourcePassword.Text,
									SourceEncoding = ddSourceEncoding.SelectedValue.ToString()
								});
							}
							if (ddSourceFormat.Text == "XLSX")
							{
								sourceDocument = new SourceDocument(FileFormat.XLSX, fileContent, sourceMapping, null, new SourceData
								{
									SourceUser = tbSourceUserName.Text,
									SourcePassword = tbSourcePassword.Text,
									SourceEncoding = ddSourceEncoding.SelectedValue.ToString()
								});
							}
							if (ddSourceFormat.Text == "CSV")
							{
								sourceDocument = new SourceDocument(FileFormat.CSV, fileContent, sourceMapping, null, new SourceData
								{
									SourceUser = tbSourceUserName.Text,
									SourcePassword = tbSourcePassword.Text,
									SourceEncoding = ddSourceEncoding.SelectedValue.ToString()
								});
							}
						}
						if (ddEntityType.SelectedIndex == 2)
						{
							wishListPreview frmItemPreview3 = new wishListPreview(sourceDocument, sourceMapping, null);
							frmItemPreview3.StartPosition = FormStartPosition.CenterParent;
							frmItemPreview3.ShowDialog();
						}
						else if (ddEntityType.SelectedIndex == 0)
						{
							itemPreview frmItemPreview2 = new itemPreview(sourceDocument, sourceMapping, null, fileFormat);
							frmItemPreview2.StartPosition = FormStartPosition.CenterParent;
							frmItemPreview2.ShowDialog();
						}
						else if (ddEntityType.SelectedIndex == 1)
						{
							userPreview frmItemPreview = new userPreview(sourceDocument, sourceMapping, null);
							frmItemPreview.StartPosition = FormStartPosition.CenterParent;
							frmItemPreview.ShowDialog();
						}
						else
						{
							MessageBox.Show("Test is failed. Please select EntityType");
						}
					}
					else
					{
						MessageBox.Show("Test is failed. Source file is empty");
					}

				}
				else
                {
					byte[] fileContent = sourceMappingFunc.GetFileContent((SourceType)ddSourceType.SelectedIndex, tbSourceUserName.Text, tbSourcePassword.Text, tbSourceAddress.Text, tbSoapAction.Text, tbSoapRequest.Text, ddSourceEncoding.SelectedValue.ToString(), ddSourceFormat.Text == "XLSX", tbAliexpressProductsUrls.Text, tbAuthHeader.Text);
					SourceDocument sourceDocument = null;
					Cursor.Current = Cursors.Default;
					if (fileContent != null)
					{
						RefreshSourceFile(fileContent);
						SourceMapping sourceMapping = GetSourceMappingFromForm();
						if (ddSourceType.SelectedIndex == 4)
						{
							fileFormat = FileFormat.AliExpress;
							sourceDocument = new SourceDocument(FileFormat.AliExpress, fileContent, sourceMapping, null, new SourceData
							{
								SourceUser = tbSourceUserName.Text,
								SourcePassword = tbSourcePassword.Text,
								SourceEncoding = ddSourceEncoding.SelectedValue.ToString()
							}, loadByOne: true);
						}
						else if (ddSourceType.SelectedIndex == 5)
						{
							fileFormat = FileFormat.BestSecret;
							sourceDocument = new SourceDocument(FileFormat.BestSecret, fileContent, sourceMapping, null, new SourceData
							{
								SourceAddress = tbSourceAddress.Text,
								SourceUser = tbSourceUserName.Text,
								SourcePassword = tbSourcePassword.Text,
								SourceEncoding = ddSourceEncoding.SelectedValue.ToString()
							}, loadByOne: true);
						}
						else
						{
							if (ddSourceFormat.Text == "XML")
							{
								sourceDocument = new SourceDocument(FileFormat.XML, fileContent, sourceMapping, null, new SourceData
								{
									SourceUser = tbSourceUserName.Text,
									SourcePassword = tbSourcePassword.Text,
									SourceEncoding = ddSourceEncoding.SelectedValue.ToString()
								});
							}
							if (ddSourceFormat.Text == "XLSX")
							{
								sourceDocument = new SourceDocument(FileFormat.XLSX, fileContent, sourceMapping, null, new SourceData
								{
									SourceUser = tbSourceUserName.Text,
									SourcePassword = tbSourcePassword.Text,
									SourceEncoding = ddSourceEncoding.SelectedValue.ToString()
								});
							}
							if (ddSourceFormat.Text == "CSV")
							{
								sourceDocument = new SourceDocument(FileFormat.CSV, fileContent, sourceMapping, null, new SourceData
								{
									SourceUser = tbSourceUserName.Text,
									SourcePassword = tbSourcePassword.Text,
									SourceEncoding = ddSourceEncoding.SelectedValue.ToString()
								});
							}
						}
						if (ddEntityType.SelectedIndex == 2)
						{
							wishListPreview frmItemPreview3 = new wishListPreview(sourceDocument, sourceMapping, null);
							frmItemPreview3.StartPosition = FormStartPosition.CenterParent;
							frmItemPreview3.ShowDialog();
						}
						else if (ddEntityType.SelectedIndex == 0)
						{
							itemPreview frmItemPreview2 = new itemPreview(sourceDocument, sourceMapping, null, fileFormat);
							frmItemPreview2.StartPosition = FormStartPosition.CenterParent;
							frmItemPreview2.ShowDialog();
						}
						else if (ddEntityType.SelectedIndex == 1)
						{
							userPreview frmItemPreview = new userPreview(sourceDocument, sourceMapping, null);
							frmItemPreview.StartPosition = FormStartPosition.CenterParent;
							frmItemPreview.ShowDialog();
						}
						else
						{
							MessageBox.Show("Test is failed. Please select EntityType");
						}
					}
					else
					{
						MessageBox.Show("Test is failed. Source file is empty");
					}
				}

				
			}
			catch (Exception ex)
			{
				
				Cursor.Current = Cursors.Default;
				MessageBox.Show("Test is failed: " + ex.ToString());
			}
			Cursor.Current = Cursors.Default;
		}

		private void RefreshSourceFile(byte[] fileContent)
		{
			try
			{
				string fileName = Path.GetFileName(this.tbSourceAddress.Text);
				string str = string.Format("{0}_{1}_{2}", this.vendorSourceId, this.vendorSourceMappingId, fileName);
				string path = "Temp\\" + str;
				if (File.Exists(path))
				{
					File.Delete(path);
				}
				this.LoadSourceFields(fileContent);
			}
			catch
			{
			}
		}


		private void RefreshSourceFileTicimax(byte[] fileContent)
		{
			try
			{
				string ticimaxSave = "lingerium.xml";
				string fileName = Path.GetFileName(this.tbSourceAddress.Text);
				string str = string.Format("{0}_{1}_{2}", this.vendorSourceId, this.vendorSourceMappingId, fileName);
				string path = "Temp\\" + ticimaxSave;
				if (File.Exists(path))
				{
					File.Delete(path);
				}
				this.LoadSourceFields(fileContent);
			}
			catch
			{
			}
		}


		private void SetStatusArea(bool valid, string message)
		{
			this.statusAreaText.Text = message;
			if (valid)
			{
				this.statusArea.BackColor = ColorTranslator.FromHtml("#009D00");
			}
			else
			{
				this.statusArea.BackColor = ColorTranslator.FromHtml("#C00000");
			}
			this.statusAreaText.ForeColor = Color.White;
		}

		private void ddSourceType_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSourceFileBrowse.Visible = false;
			this.btnSourceFileOpen.Visible = false;
			if (this.ddSourceType.SelectedIndex == 3)
			{
				this.tbSourceAddress.Enabled = true;
				this.tbSourceUserName.Enabled = true;
				this.tbSourcePassword.Enabled = true;
				this.pSourceFormat.Enabled = true;
				this.ddEntityType.Enabled = true;
				this.ddSourceEncoding.Enabled = true;
				this.ddProductIdInStore.Enabled = true;
				this.tbSoapAction.Visible = true;
				this.tbSoapRequest.Visible = true;
				this.xmlModify.Visible = false;
			}
			else if (this.ddSourceType.SelectedIndex == 4)
			{
				this.tbSourceAddress.Enabled = false;
				this.tbSourceUserName.Enabled = false;
				this.tbSourcePassword.Enabled = false;
				this.pSourceFormat.Enabled = false;
				this.ddEntityType.Enabled = false;
				this.ddSourceEncoding.Enabled = false;
				this.ddProductIdInStore.Enabled = false;
				this.tbSoapAction.Visible = false;
				this.tbSoapRequest.Visible = false;
				this.btnQuickImport.Visible = true;
				this.xmlModify.Visible = false;
				if (!this.firstLoad)
				{
					this.LoadTabs();
				}
			}
			else
			{
				this.tbSourceAddress.Enabled = true;
				this.tbSourceUserName.Enabled = true;
				this.tbSourcePassword.Enabled = true;
				this.pSourceFormat.Enabled = true;
				this.ddEntityType.Enabled = true;
				this.ddSourceEncoding.Enabled = true;
				this.ddProductIdInStore.Enabled = true;
				this.tbSoapAction.Visible = false;
				this.tbSoapRequest.Visible = false;
				this.xmlModify.Visible = true;
				if (this.ddSourceType.SelectedIndex == 0)
				{
					this.btnSourceFileBrowse.Visible = true;
					this.btnSourceFileOpen.Visible = true;
				}
			}
		}

		private void label5_Click(object sender, EventArgs e)
		{
		}

		private void textBox7_TextChanged(object sender, EventArgs e)
		{
		}

		private void numericUpDown2_ValueChanged(object sender, EventArgs e)
		{
		}

		private async void btnQuickImport_Click(object sender, EventArgs e)
		{
			try
			{
				Cursor.Current = Cursors.WaitCursor;
				byte[] fileContent = sourceMappingFunc.GetFileContent((SourceType)ddSourceType.SelectedIndex, tbSourceUserName.Text, tbSourcePassword.Text, tbSourceAddress.Text, tbSoapAction.Text, tbSoapRequest.Text, ddSourceEncoding.SelectedValue.ToString(), ddSourceFormat.Text == "XLSX", tbAliexpressProductsUrls.Text, tbAuthHeader.Text);
				SourceDocument sourceDocument = null;
				Cursor.Current = Cursors.Default;
				if (fileContent != null)
				{
					SourceMapping sourceMapping = GetSourceMappingFromForm();
					if (ddSourceType.SelectedIndex == 4)
					{
						sourceDocument = new SourceDocument(FileFormat.AliExpress, fileContent, sourceMapping, null, new SourceData
						{
							SourceUser = tbSourceUserName.Text,
							SourcePassword = tbSourcePassword.Text,
							SourceEncoding = ddSourceEncoding.SelectedValue.ToString()
						}, loadByOne: true);
					}
					else if (ddSourceType.SelectedIndex == 5)
					{
						sourceDocument = new SourceDocument(FileFormat.BestSecret, fileContent, sourceMapping, null, new SourceData
						{
							SourceUser = tbSourceUserName.Text,
							SourcePassword = tbSourcePassword.Text,
							SourceEncoding = ddSourceEncoding.SelectedValue.ToString()
						}, loadByOne: true);
					}
					else
					{
						if (ddSourceFormat.Text == "XML")
						{
							sourceDocument = new SourceDocument(FileFormat.XML, fileContent, sourceMapping, null, new SourceData
							{
								SourceUser = tbSourceUserName.Text,
								SourcePassword = tbSourcePassword.Text,
								SourceEncoding = ddSourceEncoding.SelectedValue.ToString()
							});
						}
						if (ddSourceFormat.Text == "XLSX")
						{
							sourceDocument = new SourceDocument(FileFormat.XLSX, fileContent, sourceMapping, null, new SourceData
							{
								SourceUser = tbSourceUserName.Text,
								SourcePassword = tbSourcePassword.Text,
								SourceEncoding = ddSourceEncoding.SelectedValue.ToString()
							});
						}
						if (ddSourceFormat.Text == "CSV")
						{
							sourceDocument = new SourceDocument(FileFormat.CSV, fileContent, sourceMapping, null, new SourceData
							{
								SourceUser = tbSourceUserName.Text,
								SourcePassword = tbSourcePassword.Text,
								SourceEncoding = ddSourceEncoding.SelectedValue.ToString()
							});
						}
					}
					if (ddEntityType.SelectedIndex == 0)
					{
						productCard frmItemPreview3 = new productCard(sourceDocument, sourceMapping, null);
						frmItemPreview3.StartPosition = FormStartPosition.CenterParent;
						frmItemPreview3.ShowDialog();
					}
					else if (ddEntityType.SelectedIndex == 1)
					{
						userPreview frmItemPreview2 = new userPreview(sourceDocument, sourceMapping, null);
						frmItemPreview2.StartPosition = FormStartPosition.CenterParent;
						frmItemPreview2.ShowDialog();
					}
					else if (ddEntityType.SelectedIndex == 2)
					{
						wishListPreview frmItemPreview = new wishListPreview(sourceDocument, sourceMapping, null);
						frmItemPreview.StartPosition = FormStartPosition.CenterParent;
						frmItemPreview.ShowDialog();
					}
					else
					{
						MessageBox.Show("Test is failed. Please select EntityType");
					}
				}
				else
				{
					MessageBox.Show("Test is failed. Source file is empty");
				}
			}
			catch (Exception ex)
			{
				
				Cursor.Current = Cursors.Default;
				MessageBox.Show("Test is failed: " + ex.ToString());
			}
			Cursor.Current = Cursors.Default;
		}

		private void vendorNewEdit_Shown(object sender, EventArgs e)
		{
			this.tabSourceConfig.Visible = true;
			this.tableLayoutPanel4.Visible = true;
			Cursor.Current = Cursors.Arrow;
		}

		private void BtnAddTierPricing_Click(object sender, EventArgs e)
		{
		}

		private void btnCopy_Click(object sender, EventArgs e)
		{
			new frmCopy(this.vendorId, this.vendorSourceId, this.vendorSourceMappingId, 0, this.tbName.Text, null)
			{
				StartPosition = FormStartPosition.CenterParent
			}.ShowDialog();
		}

		private void btnSourceFileBrowse_Click(object sender, EventArgs e)
		{
			string text = string.Empty;
			using (OpenFileDialog openFileDialog = new OpenFileDialog())
			{
				openFileDialog.InitialDirectory = ((!File.Exists(this.tbSourceAddress.Text) || string.IsNullOrEmpty(this.tbSourceAddress.Text)) ? "c:\\" : Path.GetDirectoryName(this.tbSourceAddress.Text));
				openFileDialog.Filter = "OLD EXCEL files (*.xls)|*.xls|EXCEL files (*.xlsx)|*.xlsx|CSV files (*.csv)|*.csv|txt files (*.txt)|*.txt|All files (*.*)|*.*";
				openFileDialog.FilterIndex = 2;
				openFileDialog.RestoreDirectory = true;
				if (openFileDialog.ShowDialog() == DialogResult.OK)
				{
					text = openFileDialog.FileName;
					this.tbSourceAddress.Text = text;
				}
			}
		}

		private static void OpenWithDefaultProgram(string path)
		{
			new Process
			{
				StartInfo = 
				{
					FileName = "explorer",
					Arguments = "\"" + path + "\""
				}
			}.Start();
		}

		private void btnSourceFileOpen_Click(object sender, EventArgs e)
		{
			if (File.Exists(this.tbSourceAddress.Text))
			{
				vendorNewEdit.OpenWithDefaultProgram(this.tbSourceAddress.Text);
			}
			else
			{
				MessageBox.Show("Source file not exist.");
			}
		}

		private void LoadSourceFieldsToView()
		{
			if (this._sourceFieldList != null && this._sourceFieldList.Any<KeyValuePair<string, string>>())
			{
				this.cbSourceParser.Visible = true;
				var list = (from mode in this._sourceFieldList
				select new
				{
					Value = mode.Key,
					Title = (string.IsNullOrEmpty(mode.Value) ? mode.Key : (mode.Key + " (" + mode.Value + ")"))
				}).ToList();
				list.Insert(0, new
				{
					Value = "",
					Title = "--Source Parser--"
				});
				this.cbSourceParser.ValueMember = "Value";
				this.cbSourceParser.DisplayMember = "Title";
				this.cbSourceParser.DataSource = list.ToList();
				this.cbSourceParser.SelectedIndex = 0;
			}
			else
			{
				this.cbSourceParser.Visible = false;
			}
		}

		private sourceMappingDataGrid sourceMappingDgWishList { get; set; }

		private void WishListTabLoad()
		{
			if (!this.tabSourceConfig.Contains(this.tabWishListMapping))
			{
				this.tabSourceConfig.TabPages.Add(this.tabWishListMapping);
			}
		}

		private void WishListListLoad(SourceMapping sourceMapping, StructureFormat structureFormat)
		{
			this.tabWishListMapping.Controls.Clear();
			this.sourceMappingDgWishList = new sourceMappingDataGrid(SourceMappingType.WishList, structureFormat);
			this.sourceMappingDgWishList.SourceMappingListLoad(sourceMapping, this._sourceFieldList);
			this.sourceMappingDgWishList.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.sourceMappingDgWishList.Dock = DockStyle.Fill;
			this.tabWishListMapping.Controls.Add(this.sourceMappingDgWishList);
		}

		private void WishListSave(SourceMapping sourceMapping)
		{
			if (this.sourceMappingDgWishList != null)
			{
				this.sourceMappingDgWishList.SourceMappingListSave(sourceMapping);
			}
		}

		private TabPage tabCustomerOthersMapping;

		private TabPage tabCustomerRoleMapping;

		private TabPage tabCustGenAttAttMapping;

		private TabPage tabCustomerShippingAddressMapping;

		private TabPage tabCustomerBillingAddressMapping;

		private TabPage tabCustomerMapping;

		private TabPage tabCustCustomAttAttMapping;

		private TabPage tabProductPictures;

		private TabPage tabProductPrice;

		private TabPage tabProductSpecAttrbuttes;

		private TabPage tabProductAttrbuttes;

		private TabPage tabProductInfo;

		private DataSettingsManager dataSettingsManager;

		private bool vendorSourceloaded;

		private bool firstLoad;

		private TabPage tabTierPricing;

		private frmVendorsList _owner;

		private int vendorId;

		private int vendorSourceId;

		private int vendorSourceMappingId;

		private SourceMappingFunc sourceMappingFunc;

		private TaskMappingFunc taskMappingFunc;

		private DataGridViewTextBoxColumn QuantityMap;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;

		private DataGridViewNumericUpDownColumn dataGridViewNumericUpDownColumn1;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;

		private TabPage tabWishListMapping;
	}
}
