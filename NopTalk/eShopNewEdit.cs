﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using NopTalkCore;

namespace NopTalk
{
	// Token: 0x0200004A RID: 74
	public partial class eShopNewEdit : Form
	{
		// Token: 0x060003C8 RID: 968 RVA: 0x0005630C File Offset: 0x0005450C
		public eShopNewEdit(int i, frmeShopsList owner)
		{
			
			this.components = null;
			this._owner = owner;
			this.InitializeComponent();
			this.rowId = i;
			this.AutoValidate = AutoValidate.Disable;
			base.FormClosing += this.eShopNewEdit_FormClosing;
		}

		// Token: 0x060003C9 RID: 969 RVA: 0x00003ABC File Offset: 0x00001CBC
		private void eShopNewEdit_FormClosing(object sender, FormClosingEventArgs e)
		{
			this._owner.gridFill();
		}

		// Token: 0x060003CA RID: 970 RVA: 0x000024A2 File Offset: 0x000006A2
		private void btnClose_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		// Token: 0x060003CB RID: 971 RVA: 0x00056358 File Offset: 0x00054558
		private async void eShopNewEdit_Load(object sender, EventArgs e)
		{
			
			if (this.rowId > -1)
			{
				this.tbId.Text = GlobalClass.dt.Rows[this.rowId]["Id"].ToString();
				this.tbName.Text = GlobalClass.dt.Rows[this.rowId]["Name"].ToString();
				this.tbDescription.Text = GlobalClass.dt.Rows[this.rowId]["Description"].ToString();
				this.tbDbConnString.Text = GlobalClass.dt.Rows[this.rowId]["DatabaseConnString"].ToString();
				this.cbVersion.Text = GlobalClass.dt.Rows[this.rowId]["Version"].ToString();
				this.cbDatabaseType.Text = GlobalClass.dt.Rows[this.rowId]["DatabaseType"].ToString();
			}
		}

		// Token: 0x060003CC RID: 972 RVA: 0x000563A4 File Offset: 0x000545A4
		private void btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				if (this.ValidateChildren(ValidationConstraints.Enabled))
				{
					if (this.rowId > -1)
					{
						this.UpdateNewRecord();
					}
					else
					{
						this.InsertNewRecord();
					}
					this.SetStatusArea(true, "Data saved successfully.");
				}
				else
				{
					this.SetStatusArea(false, "Data not saved.");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
				this.SetStatusArea(false, "Data not saved.");
			}
		}

		// Token: 0x060003CD RID: 973 RVA: 0x0005641C File Offset: 0x0005461C
		private void InsertNewRecord()
		{
			DataRow dataRow = GlobalClass.dt.NewRow();
			dataRow["Name"] = this.tbName.Text.Trim();
			dataRow["Description"] = this.tbDescription.Text.Trim();
			dataRow["DatabaseConnString"] = this.tbDbConnString.Text.Trim();
			dataRow["Version"] = this.cbVersion.Text.Trim();
			dataRow["DatabaseType"] = this.cbDatabaseType.Text.Trim();
			GlobalClass.dt.Rows.Add(dataRow);
			GlobalClass.adap.Update(GlobalClass.dt);
		}

		// Token: 0x060003CE RID: 974 RVA: 0x000564DC File Offset: 0x000546DC
		private void UpdateNewRecord()
		{
			GlobalClass.dt.Rows[this.rowId]["Name"] = this.tbName.Text.Trim();
			GlobalClass.dt.Rows[this.rowId]["Description"] = this.tbDescription.Text.Trim();
			GlobalClass.dt.Rows[this.rowId]["DatabaseConnString"] = this.tbDbConnString.Text.Trim();
			GlobalClass.dt.Rows[this.rowId]["Version"] = this.cbVersion.Text.Trim();
			GlobalClass.dt.Rows[this.rowId]["DatabaseType"] = this.cbDatabaseType.Text.Trim();
			GlobalClass.adap.Update(GlobalClass.dt);
		}

		// Token: 0x060003CF RID: 975 RVA: 0x000565E4 File Offset: 0x000547E4
		private void btnTestDbConn_Click(object sender, EventArgs e)
		{
			string text = this.IsServerConnected();
			if (string.IsNullOrEmpty(text))
			{
				MessageBox.Show(this, "The database connection tested successfully.", "Info");
			}
			else
			{
				MessageBox.Show(this, "The database connection test failed: " + text, "Error");
			}
		}

		// Token: 0x060003D0 RID: 976 RVA: 0x0005662C File Offset: 0x0005482C
		public string IsServerConnected()
		{
			string result;
			try
			{
				if (this.cbDatabaseType.Text == "MSSQL")
				{
					using (SqlConnection sqlConnection = new SqlConnection(this.tbDbConnString.Text))
					{
						sqlConnection.Open();
						return "";
					}
				}
				using (MySqlConnection mySqlConnection = new MySqlConnection(this.tbDbConnString.Text))
				{
					mySqlConnection.Open();
					result = "";
				}
			}
			catch (Exception ex)
			{
				result = ex.Message;
			}
			return result;
		}

		// Token: 0x060003D1 RID: 977 RVA: 0x000566D8 File Offset: 0x000548D8
		private void tbName_Validating(object sender, CancelEventArgs e)
		{
			bool cancel = false;
			if (string.IsNullOrEmpty(this.tbName.Text))
			{
				cancel = true;
				this.errorProvider1.SetError(this.tbName, "You must provide Name!");
			}
			e.Cancel = cancel;
		}

		// Token: 0x060003D2 RID: 978 RVA: 0x00003AC9 File Offset: 0x00001CC9
		private void tbName_Validated(object sender, EventArgs e)
		{
			this.errorProvider1.SetError(this.tbName, string.Empty);
		}

		// Token: 0x060003D3 RID: 979 RVA: 0x00056718 File Offset: 0x00054918
		private void tbDbConnString_Validating(object sender, CancelEventArgs e)
		{
			bool cancel = false;
			if (string.IsNullOrEmpty(this.tbDbConnString.Text))
			{
				cancel = true;
				this.errorProvider1.SetError(this.tbDbConnString, "You must provide Database Connection String!");
			}
			e.Cancel = cancel;
		}

		// Token: 0x060003D4 RID: 980 RVA: 0x00003AE1 File Offset: 0x00001CE1
		private void tbDbConnString_Validated(object sender, EventArgs e)
		{
			this.errorProvider1.SetError(this.tbDbConnString, string.Empty);
		}

		// Token: 0x060003D5 RID: 981 RVA: 0x00056758 File Offset: 0x00054958
		private void cbVersion_Validating(object sender, CancelEventArgs e)
		{
			bool cancel = false;
			if (string.IsNullOrEmpty(this.cbVersion.Text))
			{
				cancel = true;
				this.errorProvider1.SetError(this.cbVersion, "You must provide nopCommerce version!");
			}
			e.Cancel = cancel;
		}

		// Token: 0x060003D6 RID: 982 RVA: 0x00003AF9 File Offset: 0x00001CF9
		private void cbVersion_Validated(object sender, EventArgs e)
		{
			this.errorProvider1.SetError(this.cbVersion, string.Empty);
		}

		// Token: 0x060003D7 RID: 983 RVA: 0x00056798 File Offset: 0x00054998
		private void SetStatusArea(bool valid, string message)
		{
			this.statusAreaText.Text = message;
			if (valid)
			{
				this.statusArea.BackColor = ColorTranslator.FromHtml("#009D00");
			}
			else
			{
				this.statusArea.BackColor = ColorTranslator.FromHtml("#C00000");
			}
			this.statusAreaText.ForeColor = Color.White;
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060003D8 RID: 984 RVA: 0x0000E0E4 File Offset: 0x0000C2E4
		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams createParams = base.CreateParams;
				createParams.ExStyle |= 33554432;
				return createParams;
			}
		}

		// Token: 0x0400076E RID: 1902
		private int rowId;

		// Token: 0x0400076F RID: 1903
		private frmeShopsList _owner;
	}
}
