﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Windows.Forms;
using NopTalkCore;

namespace NopTalk
{
	// Token: 0x0200004C RID: 76
	public partial class frmeShopsList : Form
	{
		// Token: 0x060003DE RID: 990 RVA: 0x00003B36 File Offset: 0x00001D36
		public frmeShopsList()
		{
			
			this.dataSettingsManager = new DataSettingsManager();
			this.components = null;
			
			this.InitializeComponent();
		}

		// Token: 0x060003DF RID: 991 RVA: 0x000578AC File Offset: 0x00055AAC
		private async void eShops_Load(object sender, EventArgs e)
		{
			try
			{
				this.gridFill();
				
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		// Token: 0x060003E0 RID: 992 RVA: 0x000578F8 File Offset: 0x00055AF8
		public void gridFill()
		{
			try
			{
				string dataConnectionString = this.dataSettingsManager.LoadSettings(null).DataConnectionString;
				this.sqlConn = new SqlCeConnection(dataConnectionString);
				if (ImportManager.multipleStore)
				{
					GlobalClass.adap = new SqlCeDataAdapter("select id, name, description, version, DatabaseType, databaseconnstring from EShop", this.sqlConn);
				}
				else
				{
					GlobalClass.adap = new SqlCeDataAdapter("select TOP 1 id, name, description, version, DatabaseType, databaseconnstring from EShop", this.sqlConn);
				}
				this.sqlBui = new SqlCeCommandBuilder(GlobalClass.adap);
				GlobalClass.dt = new DataTable();
				GlobalClass.adap.Fill(GlobalClass.dt);
				this.dgEshops.AutoGenerateColumns = true;
				this.dgEshops.DataSource = GlobalClass.dt;
				this.dgEshops.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
				this.dgEshops.ReadOnly = true;
				if (!ImportManager.multipleStore && GlobalClass.dt.Rows.Count > 0)
				{
					this.btnNew.Enabled = false;
				}
			}
			catch
			{
			}
			finally
			{
				if (this.sqlConn != null && this.sqlConn.State == ConnectionState.Open)
				{
					this.sqlConn.Close();
				}
			}
		}

		// Token: 0x060003E1 RID: 993 RVA: 0x00057A2C File Offset: 0x00055C2C
		private void btnNew_Click(object sender, EventArgs e)
		{
			this.i = -1;
			new eShopNewEdit(this.i, this)
			{
				StartPosition = FormStartPosition.CenterParent
			}.ShowDialog();
		}

		// Token: 0x060003E2 RID: 994 RVA: 0x00057A5C File Offset: 0x00055C5C
		private void btnEdit_Click(object sender, EventArgs e)
		{
			if (this.dgEshops.SelectedRows.Count > 0)
			{
				this.i = Convert.ToInt32(this.dgEshops.SelectedRows[0].Index);
				new eShopNewEdit(this.i, this)
				{
					StartPosition = FormStartPosition.CenterParent
				}.ShowDialog();
			}
		}

		// Token: 0x060003E3 RID: 995 RVA: 0x00057ABC File Offset: 0x00055CBC
		private void dgEshops_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			this.i = Convert.ToInt32(e.RowIndex);
			new eShopNewEdit(this.i, this)
			{
				StartPosition = FormStartPosition.CenterParent
			}.ShowDialog();
		}

		// Token: 0x060003E4 RID: 996 RVA: 0x00057AF8 File Offset: 0x00055CF8
		private void btnDelete_Click(object sender, EventArgs e)
		{
			try
			{
				DialogResult dialogResult = MessageBox.Show("Are sure you want to delete this record?", "Delete record", MessageBoxButtons.YesNo);
				if (dialogResult == DialogResult.Yes)
				{
					this.i = Convert.ToInt32(this.dgEshops.SelectedRows[0].Index);
					GlobalClass.dt.Rows[this.i].Delete();
					GlobalClass.adap.Update(GlobalClass.dt);
					this.dgEshops.Refresh();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060003E5 RID: 997 RVA: 0x0000E0E4 File Offset: 0x0000C2E4
		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams createParams = base.CreateParams;
				createParams.ExStyle |= 33554432;
				return createParams;
			}
		}

		// Token: 0x0400078C RID: 1932
		private SqlCeConnection sqlConn;

		// Token: 0x0400078D RID: 1933
		private SqlCeCommandBuilder sqlBui;

		// Token: 0x0400078E RID: 1934
		private int i;

		// Token: 0x0400078F RID: 1935
		private DataSettingsManager dataSettingsManager;

		// Token: 0x04000793 RID: 1939
		private DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;

		// Token: 0x04000794 RID: 1940
		private DataGridViewTextBoxColumn NameDataGridViewTextBoxColumn;

		// Token: 0x04000795 RID: 1941
		private DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;

		// Token: 0x04000796 RID: 1942
		private DataGridViewTextBoxColumn databaseConnStringDataGridViewTextBoxColumn;
	}
}
