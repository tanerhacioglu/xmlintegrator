﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Windows.Forms;
using NopTalkCore;

namespace NopTalk
{
	public partial class frmTasksList : Form
	{
		public frmTasksList()
		{
			
			this.taskManager = new TaskManager();
			this.taskMappingFunc = new TaskMappingFunc();
			this.serviceName = "NopTalkService";
			this.timer1 = new Timer();
			this.components = null;			
			this.InitializeComponent();
		}

		private void btnNew_Click(object sender, EventArgs e)
		{
			this.i = -1;
			new taskNewEdit(this.i, this)
			{
				StartPosition = FormStartPosition.CenterParent,
				WindowState = FormWindowState.Maximized
			}.ShowDialog();
		}

		private async void frmTasksList_Load(object sender, EventArgs e)
		{
			try
			{
				this.gridFill();
				this.timer();
				
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		public void gridFill()
		{
			try
			{
				DataSettingsManager dataSettingsManager = new DataSettingsManager();
				string dataConnectionString = dataSettingsManager.LoadSettings(null).DataConnectionString;
				this.sqlConn = new SqlCeConnection(dataConnectionString);
				string selectCommandText = "SELECT Task.Id, EShop.Name AS Store, Vendor.Name AS Vendor, Task.TaskName, \r\n                      (CASE WHEN TaskAction = 0 THEN 'Insert' WHEN TaskAction = 1 THEN 'Update' WHEN TaskAction = 2 THEN 'Insert&Update' END) AS TaskAction, TaskStatus.Name AS TaskStatus, \r\n                      (CASE WHEN Task.ScheduleType = 0 THEN 'Minutely' WHEN Task.ScheduleType = 1 THEN 'Hourly' WHEN Task.ScheduleType = 2 THEN 'Daily' END) \r\n                      AS SchedulerType, Task.TaskLastRun, Task.TaskNextRun, Task.TotalItems, Task.ExportedItems as ImportedItems, Task.TaskRunning, Task.ModifiedDate\r\n                        FROM         Vendor INNER JOIN\r\n                      VendorSource ON Vendor.Id = VendorSource.VendorId INNER JOIN\r\n                      Task ON VendorSource.Id = Task.VendorSourceId LEFT OUTER JOIN\r\n                      EShop ON Task.EShopId = EShop.Id LEFT OUTER JOIN\r\n                      TaskStatus ON Task.TaskStatusId = TaskStatus.Id \r\n                      ORDER BY Task.Id";
				GlobalClass.adap = new SqlCeDataAdapter(selectCommandText, this.sqlConn);
				this.sqlBui = new SqlCeCommandBuilder(GlobalClass.adap);
				GlobalClass.dt = new DataTable();
				GlobalClass.adap.Fill(GlobalClass.dt);
				this.dgTasks.AutoGenerateColumns = true;
				foreach (object obj in GlobalClass.dt.Rows)
				{
					DataRow dataRow = (DataRow)obj;
					DateTime? dateTime = string.IsNullOrEmpty(dataRow["ModifiedDate"].ToString()) ? null : ((DateTime?)dataRow["ModifiedDate"]);
					if (dateTime == null || (dateTime != null && DateTime.Now.Subtract(dateTime.Value).TotalMinutes > 30.0))
					{
						dataRow["TaskRunning"] = 0;
					}
					if (GlobalClass.ObjectToString(dataRow["TaskAction"]) == "" || GlobalClass.ObjectToString(dataRow["TaskStatus"]) != "Started" || !TaskManager.IsScheduled)
					{
						dataRow["TaskNextRun"] = DBNull.Value;
					}
				}
				this.dgTasks.DataSource = GlobalClass.dt;
				this.dgTasks.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
				this.dgTasks.ReadOnly = true;
				this.dgTasks.ClearSelection();
				this.dgTasks.Columns["ModifiedDate"].Visible = false;
				if (TaskManager.IsScheduled)
				{
					this.btnTaskService.Text = "Stop task scheduler";
					this.btnTaskService.BackColor = Color.LightGreen;
				}
				else
				{
					this.btnTaskService.Text = "Start task scheduler";
					this.btnTaskService.BackColor = Color.LightPink;
				}
			}
			catch
			{
			}
			finally
			{
				if (this.sqlConn != null && this.sqlConn.State == ConnectionState.Open)
				{
					this.sqlConn.Close();
				}
				if (this.sqlBui != null)
				{
					this.sqlBui.Dispose();
				}
			}
		}

		private void btnEdit_Click(object sender, EventArgs e)
		{
			if (this.dgTasks.SelectedRows.Count > 0)
			{
				this.i = Convert.ToInt32(this.dgTasks.SelectedRows[0].Cells[0].Value.ToString());
				new taskNewEdit(this.i, this)
				{
					StartPosition = FormStartPosition.CenterParent,
					WindowState = FormWindowState.Maximized
				}.ShowDialog();
			}
			else
			{
				MessageBox.Show("Please select the task.");
			}
		}

		private void btnDelete_Click(object sender, EventArgs e)
		{
			try
			{
				if (this.dgTasks.SelectedRows.Count > 0)
				{
					int index = this.dgTasks.SelectedRows[0].Index;
					DialogResult dialogResult = MessageBox.Show("Are sure you want to delete this record?", "Delete record", MessageBoxButtons.YesNo);
					if (dialogResult == DialogResult.Yes)
					{
						this.taskMappingFunc.DeleteTask(Convert.ToInt32(this.dgTasks[0, index].Value));
						this.gridFill();
					}
				}
				else
				{
					MessageBox.Show("Please select the task.");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void dgTasks_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			this.i = Convert.ToInt32(this.dgTasks.SelectedRows[0].Cells[0].Value.ToString());
			new taskNewEdit(this.i, this)
			{
				StartPosition = FormStartPosition.CenterParent,
				WindowState = FormWindowState.Maximized
			}.ShowDialog();
		}

		private void btnRunAllTasks_Click(object sender, EventArgs e)
		{
			this.taskManager.StartAllTasks(null, true, 0, null);
			this.gridFill();
		}

		private void btnRunTask_Click(object sender, EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			if (this.dgTasks.SelectedRows.Count > 0)
			{
				this.taskManager.StartAllTasks(null, true, Convert.ToInt32(this.dgTasks[0, this.dgTasks.SelectedRows[0].Index].Value), null);
				this.gridFill();
			}
			else
			{
				MessageBox.Show("Please select the task.");
			}
			this.Cursor = Cursors.Default;
		}

		private void btnTaskService_Click(object sender, EventArgs e)
		{
			if (this.dgTasks.RowCount > 0)
			{
				if (TaskManager.IsScheduled)
				{
					this.taskManager.StopTasks(null);
				}
				else
				{
					this.taskManager.StartAllTasks(null, false, 0, null);
				}
				this.gridFill();
			}
			else
			{
				MessageBox.Show("Please insert the task.");
			}
		}

		private void timer()
		{
			this.timer1.Interval = 7000;
			this.timer1.Tick += this.timer1_Tick;
			this.timer1.Start();
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			if (this.chAutoRefresh.Checked)
			{
				int firstDisplayedScrollingRowIndex = this.dgTasks.FirstDisplayedScrollingRowIndex;
				int num = 0;
				if (this.dgTasks.SelectedRows.Count > 0)
				{
					num = this.dgTasks.SelectedRows[0].Index;
				}
				this.gridFill();
				if (firstDisplayedScrollingRowIndex >= 0 && this.dgTasks.Rows.Count - 1 >= firstDisplayedScrollingRowIndex)
				{
					this.dgTasks.FirstDisplayedScrollingRowIndex = firstDisplayedScrollingRowIndex;
				}
				if (this.dgTasks.Rows.Count - 1 >= num)
				{
					this.dgTasks.Rows[num].Selected = true;
				}
			}
		}

		private void frmTasksList_FormClosing(object sender, FormClosingEventArgs e)
		{
			this.timer1.Stop();
			this.timer1.Dispose();
		}

		private void btnCancelTask_Click(object sender, EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			if (this.dgTasks.SelectedRows.Count > 0)
			{
				this.taskManager.StopTasks(new int?(Convert.ToInt32(this.dgTasks[0, this.dgTasks.SelectedRows[0].Index].Value)));
				this.gridFill();
			}
			else
			{
				MessageBox.Show("Please select the task.");
			}
			this.Cursor = Cursors.Default;
		}

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams createParams = base.CreateParams;
				createParams.ExStyle |= 33554432;
				return createParams;
			}
		}

		private TaskManager taskManager;

		private TaskMappingFunc taskMappingFunc;

		private SqlCeConnection sqlConn;

		private SqlCeCommandBuilder sqlBui;

		private string serviceName;

		private int i;

		private Timer timer1;
	}
}
