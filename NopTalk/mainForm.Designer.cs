﻿namespace NopTalk
{
	// Token: 0x02000051 RID: 81
	public partial class mainForm : global::System.Windows.Forms.Form
	{
		// Token: 0x06000424 RID: 1060 RVA: 0x00003D8C File Offset: 0x00001F8C
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000425 RID: 1061 RVA: 0x0005C730 File Offset: 0x0005A930
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eShopsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tasksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditor = new System.Windows.Forms.ToolStripMenuItem();
            this.productsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.categoriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manufacturersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.nopTalkHomeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateNopTalkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutNopTalkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ribbon1 = new System.Windows.Forms.Ribbon();
            this.ribbonButton1 = new System.Windows.Forms.RibbonButton();
            this.ribbonButton2 = new System.Windows.Forms.RibbonButton();
            this.ribbonButton3 = new System.Windows.Forms.RibbonButton();
            this.rtNopManagerTab = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel1 = new System.Windows.Forms.RibbonPanel();
            this.rbStores = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel2 = new System.Windows.Forms.RibbonPanel();
            this.rbVendors = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel5 = new System.Windows.Forms.RibbonPanel();
            this.rbTasks = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel6 = new System.Windows.Forms.RibbonPanel();
            this.rbLogs = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel3 = new System.Windows.Forms.RibbonPanel();
            this.rbSystemTray = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel4 = new System.Windows.Forms.RibbonPanel();
            this.rbExit = new System.Windows.Forms.RibbonButton();
            this.rtNopEditorTab = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel8 = new System.Windows.Forms.RibbonPanel();
            this.rbProductsEdit = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel9 = new System.Windows.Forms.RibbonPanel();
            this.rbCategoriesEdit = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel11 = new System.Windows.Forms.RibbonPanel();
            this.ribbonButton4 = new System.Windows.Forms.RibbonButton();
            this.ribbonOrbMenuItem1 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbMenuItem2 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbRecentItem1 = new System.Windows.Forms.RibbonOrbRecentItem();
            this.ribbonOrbRecentItem2 = new System.Windows.Forms.RibbonOrbRecentItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.tasksToolStripMenuItem,
            this.menuEditor,
            this.logsToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1003, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eShopsToolStripMenuItem,
            this.vendorsToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // eShopsToolStripMenuItem
            // 
            this.eShopsToolStripMenuItem.Name = "eShopsToolStripMenuItem";
            this.eShopsToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.eShopsToolStripMenuItem.Text = "Stores";
            this.eShopsToolStripMenuItem.Click += new System.EventHandler(this.eShopsToolStripMenuItem_Click);
            // 
            // vendorsToolStripMenuItem
            // 
            this.vendorsToolStripMenuItem.Name = "vendorsToolStripMenuItem";
            this.vendorsToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.vendorsToolStripMenuItem.Text = "Source Maps";
            this.vendorsToolStripMenuItem.Click += new System.EventHandler(this.vendorsToolStripMenuItem_Click);
            // 
            // tasksToolStripMenuItem
            // 
            this.tasksToolStripMenuItem.Name = "tasksToolStripMenuItem";
            this.tasksToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.tasksToolStripMenuItem.Text = "Tasks";
            this.tasksToolStripMenuItem.Click += new System.EventHandler(this.tasksToolStripMenuItem_Click);
            // 
            // menuEditor
            // 
            this.menuEditor.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.productsToolStripMenuItem,
            this.categoriesToolStripMenuItem,
            this.manufacturersToolStripMenuItem});
            this.menuEditor.Name = "menuEditor";
            this.menuEditor.Size = new System.Drawing.Size(50, 20);
            this.menuEditor.Text = "Editor";
            // 
            // productsToolStripMenuItem
            // 
            this.productsToolStripMenuItem.Name = "productsToolStripMenuItem";
            this.productsToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.productsToolStripMenuItem.Text = "Products";
            this.productsToolStripMenuItem.Click += new System.EventHandler(this.productsToolStripMenuItem_Click);
            // 
            // categoriesToolStripMenuItem
            // 
            this.categoriesToolStripMenuItem.Name = "categoriesToolStripMenuItem";
            this.categoriesToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.categoriesToolStripMenuItem.Text = "Categories";
            this.categoriesToolStripMenuItem.Click += new System.EventHandler(this.categoriesToolStripMenuItem_Click);
            // 
            // manufacturersToolStripMenuItem
            // 
            this.manufacturersToolStripMenuItem.Name = "manufacturersToolStripMenuItem";
            this.manufacturersToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.manufacturersToolStripMenuItem.Text = "Manufacturers";
            this.manufacturersToolStripMenuItem.Click += new System.EventHandler(this.manufacturersToolStripMenuItem_Click);
            // 
            // logsToolStripMenuItem
            // 
            this.logsToolStripMenuItem.Name = "logsToolStripMenuItem";
            this.logsToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.logsToolStripMenuItem.Text = "Logs";
            this.logsToolStripMenuItem.Click += new System.EventHandler(this.logsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1,
            this.nopTalkHomeToolStripMenuItem,
            this.updateNopTalkToolStripMenuItem,
            this.aboutNopTalkToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(166, 22);
            this.helpToolStripMenuItem1.Text = "Help";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // nopTalkHomeToolStripMenuItem
            // 
            this.nopTalkHomeToolStripMenuItem.Name = "nopTalkHomeToolStripMenuItem";
            this.nopTalkHomeToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.nopTalkHomeToolStripMenuItem.Text = "Artı Global Home";
            this.nopTalkHomeToolStripMenuItem.Click += new System.EventHandler(this.nopTalkHomeToolStripMenuItem_Click);
            // 
            // updateNopTalkToolStripMenuItem
            // 
            this.updateNopTalkToolStripMenuItem.Enabled = false;
            this.updateNopTalkToolStripMenuItem.Name = "updateNopTalkToolStripMenuItem";
            this.updateNopTalkToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.updateNopTalkToolStripMenuItem.Text = "Update NopTalk";
            this.updateNopTalkToolStripMenuItem.Click += new System.EventHandler(this.updateNopTalkToolStripMenuItem_Click);
            // 
            // aboutNopTalkToolStripMenuItem
            // 
            this.aboutNopTalkToolStripMenuItem.Name = "aboutNopTalkToolStripMenuItem";
            this.aboutNopTalkToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.aboutNopTalkToolStripMenuItem.Text = "About NopTalk";
            this.aboutNopTalkToolStripMenuItem.Click += new System.EventHandler(this.aboutNopTalkToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // ribbon1
            // 
            this.ribbon1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ribbon1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.ribbon1.Location = new System.Drawing.Point(0, 24);
            this.ribbon1.Minimized = false;
            this.ribbon1.Name = "ribbon1";
            // 
            // 
            // 
            this.ribbon1.OrbDropDown.BorderRoundness = 8;
            this.ribbon1.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.OrbDropDown.Name = "";
            this.ribbon1.OrbDropDown.Size = new System.Drawing.Size(527, 447);
            this.ribbon1.OrbDropDown.TabIndex = 0;
            this.ribbon1.OrbImage = null;
            this.ribbon1.OrbStyle = System.Windows.Forms.RibbonOrbStyle.Office_2010;
            this.ribbon1.OrbVisible = false;
            // 
            // 
            // 
            this.ribbon1.QuickAcessToolbar.Items.Add(this.ribbonButton1);
            this.ribbon1.QuickAcessToolbar.Items.Add(this.ribbonButton2);
            this.ribbon1.QuickAcessToolbar.Items.Add(this.ribbonButton3);
            this.ribbon1.QuickAcessToolbar.Visible = false;
            this.ribbon1.RibbonTabFont = new System.Drawing.Font("Trebuchet MS", 9F);
            this.ribbon1.Size = new System.Drawing.Size(1003, 116);
            this.ribbon1.TabIndex = 2;
            this.ribbon1.Tabs.Add(this.rtNopManagerTab);
            this.ribbon1.Tabs.Add(this.rtNopEditorTab);
            this.ribbon1.TabsMargin = new System.Windows.Forms.Padding(12, 10, 20, 0);
            this.ribbon1.Text = "ribbon1";
            this.ribbon1.ThemeColor = System.Windows.Forms.RibbonTheme.Blue;
            this.ribbon1.ActiveTabChanged += new System.EventHandler(this.ribbon1_ActiveTabChanged);
            // 
            // ribbonButton1
            // 
            this.ribbonButton1.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.Image")));
            this.ribbonButton1.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.ribbonButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.SmallImage")));
            this.ribbonButton1.Text = "ribbonButton1";
            // 
            // ribbonButton2
            // 
            this.ribbonButton2.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.Image")));
            this.ribbonButton2.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.ribbonButton2.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.SmallImage")));
            this.ribbonButton2.Text = "ribbonButton2";
            // 
            // ribbonButton3
            // 
            this.ribbonButton3.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.Image")));
            this.ribbonButton3.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.ribbonButton3.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.SmallImage")));
            this.ribbonButton3.Text = "ribbonButton3";
            // 
            // rtNopManagerTab
            // 
            this.rtNopManagerTab.Panels.Add(this.ribbonPanel1);
            this.rtNopManagerTab.Panels.Add(this.ribbonPanel2);
            this.rtNopManagerTab.Panels.Add(this.ribbonPanel5);
            this.rtNopManagerTab.Panels.Add(this.ribbonPanel6);
            this.rtNopManagerTab.Panels.Add(this.ribbonPanel3);
            this.rtNopManagerTab.Panels.Add(this.ribbonPanel4);
            this.rtNopManagerTab.Text = "Integrator";
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.Items.Add(this.rbStores);
            this.ribbonPanel1.Text = "Stores";
            // 
            // rbStores
            // 
            this.rbStores.Image = ((System.Drawing.Image)(resources.GetObject("rbStores.Image")));
            this.rbStores.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbStores.SmallImage")));
            this.rbStores.Text = "";
            this.rbStores.Click += new System.EventHandler(this.rbStores_Click);
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.Items.Add(this.rbVendors);
            this.ribbonPanel2.Text = "Source Map";
            // 
            // rbVendors
            // 
            this.rbVendors.Image = ((System.Drawing.Image)(resources.GetObject("rbVendors.Image")));
            this.rbVendors.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbVendors.SmallImage")));
            this.rbVendors.Text = "";
            this.rbVendors.Click += new System.EventHandler(this.rbVendors_Click);
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.Items.Add(this.rbTasks);
            this.ribbonPanel5.Text = "Import Tasks";
            // 
            // rbTasks
            // 
            this.rbTasks.Image = ((System.Drawing.Image)(resources.GetObject("rbTasks.Image")));
            this.rbTasks.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbTasks.SmallImage")));
            this.rbTasks.Text = "";
            this.rbTasks.Click += new System.EventHandler(this.rbTasks_Click);
            // 
            // ribbonPanel6
            // 
            this.ribbonPanel6.Items.Add(this.rbLogs);
            this.ribbonPanel6.Text = "Event Logs";
            // 
            // rbLogs
            // 
            this.rbLogs.Image = ((System.Drawing.Image)(resources.GetObject("rbLogs.Image")));
            this.rbLogs.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbLogs.SmallImage")));
            this.rbLogs.Text = "";
            this.rbLogs.Click += new System.EventHandler(this.rbLogs_Click);
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.Items.Add(this.rbSystemTray);
            this.ribbonPanel3.Text = "System tray";
            // 
            // rbSystemTray
            // 
            this.rbSystemTray.Image = ((System.Drawing.Image)(resources.GetObject("rbSystemTray.Image")));
            this.rbSystemTray.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSystemTray.SmallImage")));
            this.rbSystemTray.Text = "";
            this.rbSystemTray.Click += new System.EventHandler(this.rbSystemTray_Click);
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.Items.Add(this.rbExit);
            this.ribbonPanel4.Text = "Exit";
            // 
            // rbExit
            // 
            this.rbExit.Image = ((System.Drawing.Image)(resources.GetObject("rbExit.Image")));
            this.rbExit.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbExit.SmallImage")));
            this.rbExit.Text = "";
            this.rbExit.Click += new System.EventHandler(this.rbExit_Click);
            // 
            // rtNopEditorTab
            // 
            this.rtNopEditorTab.Panels.Add(this.ribbonPanel8);
            this.rtNopEditorTab.Panels.Add(this.ribbonPanel9);
            this.rtNopEditorTab.Panels.Add(this.ribbonPanel11);
            this.rtNopEditorTab.Text = "Editor";
            // 
            // ribbonPanel8
            // 
            this.ribbonPanel8.Items.Add(this.rbProductsEdit);
            this.ribbonPanel8.Text = "Products";
            // 
            // rbProductsEdit
            // 
            this.rbProductsEdit.Image = ((System.Drawing.Image)(resources.GetObject("rbProductsEdit.Image")));
            this.rbProductsEdit.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbProductsEdit.SmallImage")));
            this.rbProductsEdit.Click += new System.EventHandler(this.rbProductsEdit_Click);
            // 
            // ribbonPanel9
            // 
            this.ribbonPanel9.Items.Add(this.rbCategoriesEdit);
            this.ribbonPanel9.Text = "Categories";
            // 
            // rbCategoriesEdit
            // 
            this.rbCategoriesEdit.Image = ((System.Drawing.Image)(resources.GetObject("rbCategoriesEdit.Image")));
            this.rbCategoriesEdit.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCategoriesEdit.SmallImage")));
            this.rbCategoriesEdit.Click += new System.EventHandler(this.rbCategoriesEdit_Click);
            // 
            // ribbonPanel11
            // 
            this.ribbonPanel11.Items.Add(this.ribbonButton4);
            this.ribbonPanel11.Text = "Manufacturers";
            // 
            // ribbonButton4
            // 
            this.ribbonButton4.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton4.Image")));
            this.ribbonButton4.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton4.SmallImage")));
            this.ribbonButton4.Click += new System.EventHandler(this.ribbonButton4_Click);
            // 
            // ribbonOrbMenuItem1
            // 
            this.ribbonOrbMenuItem1.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("ribbonOrbMenuItem1.Image")));
            this.ribbonOrbMenuItem1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbMenuItem1.SmallImage")));
            this.ribbonOrbMenuItem1.Text = "ribbonOrbMenuItem1";
            // 
            // ribbonOrbMenuItem2
            // 
            this.ribbonOrbMenuItem2.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("ribbonOrbMenuItem2.Image")));
            this.ribbonOrbMenuItem2.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbMenuItem2.SmallImage")));
            this.ribbonOrbMenuItem2.Text = "ribbonOrbMenuItem2";
            // 
            // ribbonOrbRecentItem1
            // 
            this.ribbonOrbRecentItem1.Image = ((System.Drawing.Image)(resources.GetObject("ribbonOrbRecentItem1.Image")));
            this.ribbonOrbRecentItem1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbRecentItem1.SmallImage")));
            this.ribbonOrbRecentItem1.Text = "ribbonOrbRecentItem1";
            // 
            // ribbonOrbRecentItem2
            // 
            this.ribbonOrbRecentItem2.Image = ((System.Drawing.Image)(resources.GetObject("ribbonOrbRecentItem2.Image")));
            this.ribbonOrbRecentItem2.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbRecentItem2.SmallImage")));
            this.ribbonOrbRecentItem2.Text = "ribbonOrbRecentItem2";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "NopTalk Pro";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseClick);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 504);
            this.Controls.Add(this.ribbon1);
            this.Controls.Add(this.menuStrip1);
            this.HelpButton = true;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nopcommerce Importer v 4.3.0.0";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.mainForm_FormClosing);
            this.Load += new System.EventHandler(this.mainForm_Load);
            this.Shown += new System.EventHandler(this.mainForm_Shown);
            this.SizeChanged += new System.EventHandler(this.mainForm_SizeChanged);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		// Token: 0x04000804 RID: 2052
		private global::System.ComponentModel.IContainer components;

		// Token: 0x04000805 RID: 2053
		private global::System.Windows.Forms.MenuStrip menuStrip1;

		// Token: 0x04000806 RID: 2054
		private global::System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;

		// Token: 0x04000807 RID: 2055
		private global::System.Windows.Forms.ToolStripMenuItem eShopsToolStripMenuItem;

		// Token: 0x04000808 RID: 2056
		private global::System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem;

		// Token: 0x04000809 RID: 2057
		private global::System.Windows.Forms.ToolStripMenuItem tasksToolStripMenuItem;

		// Token: 0x0400080A RID: 2058
		private global::System.Windows.Forms.ToolStripMenuItem logsToolStripMenuItem;

		// Token: 0x0400080B RID: 2059
		private global::System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;

		// Token: 0x0400080C RID: 2060
		private global::System.Windows.Forms.ToolStripMenuItem nopTalkHomeToolStripMenuItem;

		// Token: 0x0400080E RID: 2062
		private global::System.Windows.Forms.ToolStripMenuItem updateNopTalkToolStripMenuItem;

		// Token: 0x0400080F RID: 2063
		private global::System.Windows.Forms.ToolStripMenuItem aboutNopTalkToolStripMenuItem;

		// Token: 0x04000810 RID: 2064
		private global::System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;

		// Token: 0x04000811 RID: 2065
		private global::System.Windows.Forms.Ribbon ribbon1;

		// Token: 0x04000812 RID: 2066
		private global::System.Windows.Forms.RibbonTab rtNopManagerTab;

		// Token: 0x04000813 RID: 2067
		private global::System.Windows.Forms.RibbonPanel ribbonPanel1;

		// Token: 0x04000814 RID: 2068
		private global::System.Windows.Forms.RibbonButton rbStores;

		// Token: 0x04000815 RID: 2069
		private global::System.Windows.Forms.RibbonPanel ribbonPanel2;

		// Token: 0x04000816 RID: 2070
		private global::System.Windows.Forms.RibbonButton rbVendors;

		// Token: 0x04000817 RID: 2071
		private global::System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem1;

		// Token: 0x04000818 RID: 2072
		private global::System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem2;

		// Token: 0x04000819 RID: 2073
		private global::System.Windows.Forms.RibbonOrbRecentItem ribbonOrbRecentItem1;

		// Token: 0x0400081A RID: 2074
		private global::System.Windows.Forms.RibbonOrbRecentItem ribbonOrbRecentItem2;

		// Token: 0x0400081B RID: 2075
		private global::System.Windows.Forms.RibbonPanel ribbonPanel5;

		// Token: 0x0400081C RID: 2076
		private global::System.Windows.Forms.RibbonButton rbTasks;

		// Token: 0x0400081D RID: 2077
		private global::System.Windows.Forms.RibbonPanel ribbonPanel6;

		// Token: 0x0400081E RID: 2078
		private global::System.Windows.Forms.RibbonButton rbLogs;

		// Token: 0x0400081F RID: 2079
		private global::System.Windows.Forms.RibbonPanel ribbonPanel3;

		// Token: 0x04000820 RID: 2080
		private global::System.Windows.Forms.RibbonButton rbSystemTray;

		// Token: 0x04000821 RID: 2081
		private global::System.Windows.Forms.RibbonPanel ribbonPanel4;

		// Token: 0x04000822 RID: 2082
		private global::System.Windows.Forms.RibbonButton rbExit;

		// Token: 0x04000823 RID: 2083
		private global::System.Windows.Forms.NotifyIcon notifyIcon1;

		// Token: 0x04000824 RID: 2084
		private global::System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;

		// Token: 0x04000825 RID: 2085
		private global::System.Windows.Forms.RibbonTab rtNopEditorTab;

		// Token: 0x04000826 RID: 2086
		private global::System.Windows.Forms.RibbonPanel ribbonPanel8;

		// Token: 0x04000827 RID: 2087
		private global::System.Windows.Forms.RibbonPanel ribbonPanel9;

		// Token: 0x04000828 RID: 2088
		private global::System.Windows.Forms.RibbonButton rbProductsEdit;

		// Token: 0x04000829 RID: 2089
		private global::System.Windows.Forms.RibbonButton rbCategoriesEdit;

		// Token: 0x0400082A RID: 2090
		private global::System.Windows.Forms.RibbonButton ribbonButton1;

		// Token: 0x0400082B RID: 2091
		private global::System.Windows.Forms.RibbonButton ribbonButton2;

		// Token: 0x0400082C RID: 2092
		private global::System.Windows.Forms.RibbonButton ribbonButton3;

		// Token: 0x0400082D RID: 2093
		private global::System.Windows.Forms.RibbonPanel ribbonPanel11;

		// Token: 0x0400082E RID: 2094
		private global::System.Windows.Forms.RibbonButton ribbonButton4;

		// Token: 0x0400082F RID: 2095
		private global::System.Windows.Forms.ToolStripMenuItem menuEditor;

		// Token: 0x04000830 RID: 2096
		private global::System.Windows.Forms.ToolStripMenuItem productsToolStripMenuItem;

		// Token: 0x04000831 RID: 2097
		private global::System.Windows.Forms.ToolStripMenuItem categoriesToolStripMenuItem;

		// Token: 0x04000832 RID: 2098
		private global::System.Windows.Forms.ToolStripMenuItem manufacturersToolStripMenuItem;
	}
}
