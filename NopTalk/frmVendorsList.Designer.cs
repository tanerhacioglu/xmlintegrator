﻿namespace NopTalk
{
	// Token: 0x02000058 RID: 88
	public partial class frmVendorsList : global::System.Windows.Forms.Form
	{
		// Token: 0x06000441 RID: 1089 RVA: 0x00003DD6 File Offset: 0x00001FD6
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000442 RID: 1090 RVA: 0x0005E3F8 File Offset: 0x0005C5F8
		private void InitializeComponent()
		{
			global::System.ComponentModel.ComponentResourceManager componentResourceManager = new global::System.ComponentModel.ComponentResourceManager(typeof(global::NopTalk.frmVendorsList));
			this.btnDelete = new global::System.Windows.Forms.Button();
			this.btnEdit = new global::System.Windows.Forms.Button();
			this.btnNew = new global::System.Windows.Forms.Button();
			this.dgVendors = new global::System.Windows.Forms.DataGridView();
			((global::System.ComponentModel.ISupportInitialize)this.dgVendors).BeginInit();
			base.SuspendLayout();
			this.btnDelete.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnDelete.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnDelete.Location = new global::System.Drawing.Point(707, 337);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new global::System.Drawing.Size(75, 23);
			this.btnDelete.TabIndex = 7;
			this.btnDelete.Text = "Delete";
			this.btnDelete.UseVisualStyleBackColor = true;
			this.btnDelete.Click += new global::System.EventHandler(this.btnDelete_Click);
			this.btnEdit.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnEdit.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnEdit.Location = new global::System.Drawing.Point(626, 337);
			this.btnEdit.Name = "btnEdit";
			this.btnEdit.Size = new global::System.Drawing.Size(75, 23);
			this.btnEdit.TabIndex = 6;
			this.btnEdit.Text = "Edit";
			this.btnEdit.UseVisualStyleBackColor = true;
			this.btnEdit.Click += new global::System.EventHandler(this.btnEdit_Click);
			this.btnNew.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnNew.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnNew.Location = new global::System.Drawing.Point(545, 337);
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new global::System.Drawing.Size(75, 23);
			this.btnNew.TabIndex = 5;
			this.btnNew.Text = "Insert";
			this.btnNew.UseVisualStyleBackColor = true;
			this.btnNew.Click += new global::System.EventHandler(this.btnNew_Click);
			this.dgVendors.AllowUserToAddRows = false;
			this.dgVendors.AllowUserToDeleteRows = false;
			this.dgVendors.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.dgVendors.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgVendors.Location = new global::System.Drawing.Point(12, 12);
			this.dgVendors.MultiSelect = false;
			this.dgVendors.Name = "dgVendors";
			this.dgVendors.SelectionMode = global::System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgVendors.Size = new global::System.Drawing.Size(770, 305);
			this.dgVendors.TabIndex = 4;
			this.dgVendors.CellDoubleClick += new global::System.Windows.Forms.DataGridViewCellEventHandler(this.dgVendors_CellDoubleClick);
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(794, 372);
			base.Controls.Add(this.btnDelete);
			base.Controls.Add(this.btnEdit);
			base.Controls.Add(this.btnNew);
			base.Controls.Add(this.dgVendors);
			base.Icon = (global::System.Drawing.Icon)componentResourceManager.GetObject("$this.Icon");
			base.Name = "frmVendorsList";
			base.StartPosition = global::System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Source Maps";
			base.Load += new global::System.EventHandler(this.frmVendorsList_Load);
			((global::System.ComponentModel.ISupportInitialize)this.dgVendors).EndInit();
			base.ResumeLayout(false);
		}

		// Token: 0x04000856 RID: 2134
		private global::System.ComponentModel.IContainer components;

		// Token: 0x04000857 RID: 2135
		private global::System.Windows.Forms.Button btnDelete;

		// Token: 0x04000858 RID: 2136
		private global::System.Windows.Forms.Button btnEdit;

		// Token: 0x04000859 RID: 2137
		private global::System.Windows.Forms.Button btnNew;

		// Token: 0x0400085A RID: 2138
		private global::System.Windows.Forms.DataGridView dgVendors;
	}
}
