﻿namespace NopTalk
{
	// Token: 0x02000017 RID: 23
	public partial class frmCopy : global::System.Windows.Forms.Form
	{
		// Token: 0x0600009A RID: 154 RVA: 0x0000278A File Offset: 0x0000098A
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x0600009B RID: 155 RVA: 0x0000D120 File Offset: 0x0000B320
		private void InitializeComponent()
		{
			this.btnClose = new global::System.Windows.Forms.Button();
			this.btnSave = new global::System.Windows.Forms.Button();
			this.groupBox1 = new global::System.Windows.Forms.GroupBox();
			this.tbName = new global::System.Windows.Forms.TextBox();
			this.label2 = new global::System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			base.SuspendLayout();
			this.btnClose.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnClose.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnClose.Location = new global::System.Drawing.Point(433, 124);
			this.btnClose.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new global::System.Drawing.Size(100, 28);
			this.btnClose.TabIndex = 28;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new global::System.EventHandler(this.btnClose_Click);
			this.btnSave.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnSave.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnSave.Location = new global::System.Drawing.Point(325, 124);
			this.btnSave.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new global::System.Drawing.Size(100, 28);
			this.btnSave.TabIndex = 27;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new global::System.EventHandler(this.btnSave_Click);
			this.groupBox1.Controls.Add(this.tbName);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Location = new global::System.Drawing.Point(13, 13);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new global::System.Drawing.Size(520, 96);
			this.groupBox1.TabIndex = 29;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Copy";
			this.tbName.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tbName.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbName.Location = new global::System.Drawing.Point(75, 48);
			this.tbName.Margin = new global::System.Windows.Forms.Padding(4);
			this.tbName.Name = "tbName";
			this.tbName.Size = new global::System.Drawing.Size(438, 22);
			this.tbName.TabIndex = 5;
			this.label2.AutoSize = true;
			this.label2.Location = new global::System.Drawing.Point(7, 53);
			this.label2.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new global::System.Drawing.Size(45, 17);
			this.label2.TabIndex = 4;
			this.label2.Text = "Name";
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(8f, 16f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(546, 165);
			base.Controls.Add(this.groupBox1);
			base.Controls.Add(this.btnClose);
			base.Controls.Add(this.btnSave);
			base.Name = "frmCopy";
			this.Text = "frmCopy";
			base.Load += new global::System.EventHandler(this.frmCopy_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			base.ResumeLayout(false);
		}

		// Token: 0x0400009A RID: 154
		private global::System.ComponentModel.IContainer components;

		// Token: 0x0400009B RID: 155
		private global::System.Windows.Forms.Button btnClose;

		// Token: 0x0400009C RID: 156
		private global::System.Windows.Forms.Button btnSave;

		// Token: 0x0400009D RID: 157
		private global::System.Windows.Forms.GroupBox groupBox1;

		// Token: 0x0400009E RID: 158
		private global::System.Windows.Forms.TextBox tbName;

		// Token: 0x0400009F RID: 159
		private global::System.Windows.Forms.Label label2;
	}
}
