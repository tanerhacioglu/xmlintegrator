﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using NopTalk.Helpers;
using NopTalkCore;

namespace NopTalk
{
	public class sourceMappingDataGrid : UserControl
	{
		private SourceMappingType _sourceMappingType { get; set; }

		private StructureFormat _structureFormat { get; set; }

		private Dictionary<string, string> _sourceFieldList { get; set; }

		public sourceMappingDataGrid(SourceMappingType sourceMappingType, StructureFormat structureFormat)
		{
			
			this.components = null;
			this.InitializeComponent();
			this.dgSourceMapping.RowPrePaint += this.dgSourceMapping_RowPrePaint;
			this.dgSourceMapping.GetType().GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(this.dgSourceMapping, true, null);
			this._sourceMappingType = sourceMappingType;
			this._structureFormat = structureFormat;
			this.dgSourceMapping.DataBindingComplete += delegate(object sender, DataGridViewBindingCompleteEventArgs e)
			{
				DataGridView dataGridView = sender as DataGridView;
				if (dataGridView != null)
				{
					dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
					dataGridView.Columns[dataGridView.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
				}
			};
		}

		public void SourceMappingListLoad(SourceMapping sourceMapping, Dictionary<string, string> sourceFieldList)
		{
			this._sourceFieldList = sourceFieldList;
			this.dgSourceMapping.DataSource = null;
			List<SourceItemMapping> list = new List<SourceItemMapping>();
			this.dgSourceMapping.AutoGenerateColumns = false;
			if (this._sourceMappingType == SourceMappingType.ProductInfo || this._sourceMappingType == SourceMappingType.Customer || this._sourceMappingType == SourceMappingType.ShippingAddress || this._sourceMappingType == SourceMappingType.BillingAddress || this._sourceMappingType == SourceMappingType.CustomerOthers || this._sourceMappingType == SourceMappingType.Product_Prices)
			{
				this.btnSourceMappingAdd.Visible = false;
				this.btnSourceMappingDelete.Visible = false;
			}
			if (this._sourceMappingType == SourceMappingType.ProductInfo)
			{
				list = sourceMapping.SourceProdInfoMapping.Items.ToList<SourceItemMapping>();
			}
			if (this._sourceMappingType == SourceMappingType.Product_Prices)
			{
				list = sourceMapping.SourceProdPriceMapping.Items.ToList<SourceItemMapping>();
			}
			if (this._sourceMappingType == SourceMappingType.ProductPictures)
			{
				list = sourceMapping.SourceProdPicturesMapping.Items.ToList<SourceItemMapping>();
			}
			if (this._sourceMappingType == SourceMappingType.TierPricing)
			{
				list = sourceMapping.SourceTierPricingMapping.SourceTierPricingMappingItems.ToList<SourceItemMapping>();
			}
			if (this._sourceMappingType == SourceMappingType.ProductAttributes)
			{
				list.AddRange(sourceMapping.SourceProdAttMapping.SourceProdAttMappingItems.ToList<SourceItemMapping>());
			}
			if (this._sourceMappingType == SourceMappingType.ProductSpecAttributes)
			{
				list.AddRange(sourceMapping.SourceProdSpecAttMapping.SourceProdSpecAttMappingItems.ToList<SourceItemMapping>());
			}
			if (this._sourceMappingType == SourceMappingType.AddressCustomAttributtes)
			{
				list.AddRange(sourceMapping.SourceAddressCustomAttMapping.Items.ToList<SourceItemMapping>());
			}
			if (this._sourceMappingType == SourceMappingType.CustomerCustomAttributtes)
			{
				list.AddRange(sourceMapping.SourceCustomerCustomAttMapping.Items.ToList<SourceItemMapping>());
			}
			if (this._sourceMappingType == SourceMappingType.Customer)
			{
				list.AddRange(sourceMapping.SourceCustomerMapping.Items.ToList<SourceItemMapping>());
			}
			if (this._sourceMappingType == SourceMappingType.CustomerRole)
			{
				list.AddRange(sourceMapping.SourceCustomerRoleMapping.Items.ToList<SourceItemMapping>());
			}
			if (this._sourceMappingType == SourceMappingType.ShippingAddress)
			{
				list.AddRange(sourceMapping.SourceCustomerShippingAddressMapping.Items.ToList<SourceItemMapping>());
			}
			if (this._sourceMappingType == SourceMappingType.BillingAddress)
			{
				list.AddRange(sourceMapping.SourceCustomerBillingAddressMapping.Items.ToList<SourceItemMapping>());
			}
			if (this._sourceMappingType == SourceMappingType.CustomerOrders)
			{
				list.AddRange(sourceMapping.SourceCustomerOrdersMapping.Items.ToList<SourceItemMapping>());
			}
			if (this._sourceMappingType == SourceMappingType.CustomerOthers)
			{
				list.AddRange(sourceMapping.SourceCustomerOthersMapping.Items.ToList<SourceItemMapping>());
			}
			if (this._sourceMappingType == SourceMappingType.WishList)
			{
				list.AddRange(sourceMapping.SourceWishListMapping.Items.ToList<SourceItemMapping>());
			}
			SourceItemMapping sourceItemMapping = (from x in list
			where x.MappingItemType == -1
			select x).FirstOrDefault<SourceItemMapping>();
			if (this._structureFormat == StructureFormat.XML && sourceItemMapping == null && this._sourceMappingType != SourceMappingType.ProductInfo && this._sourceMappingType != SourceMappingType.Product_Prices)
			{
				list.InsertRange(0, SourceMappingHelper.GetRepeatNode());
			}
			if (this._structureFormat == StructureFormat.Position)
			{
				list = (from x in list
				where x.MappingItemType != -1
				select x).ToList<SourceItemMapping>();
			}
			this.SetSourceMappingFieldNames(list);
			this.dgSourceMapping.DataSource = (from o in list
			orderby o.FieldIndex, o.Order
			select o).ToList<SourceItemMapping>().ConvertToDataTable<SourceItemMapping>();
			this.DataGridSettings();
			this.SourceMappingColumnsLoad(this._structureFormat);
			this.dgSourceMapping.Update();
			this.dgSourceMapping.Refresh();
			this.SourceMappingAdd(true, this._sourceMappingType == SourceMappingType.Product_Prices || this._sourceMappingType == SourceMappingType.ProductInfo || this._sourceMappingType == SourceMappingType.TierPricing || this._sourceMappingType == SourceMappingType.ProductAttributes);
		}

		public void SourceMappingListSave(SourceMapping sourceMapping)
		{
			if (this._sourceMappingType == SourceMappingType.TierPricing)
			{
				if (this.dgSourceMapping.DataSource != null)
				{
					List<SourceItemMapping> list = ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>();
					this.SetSourceMappingFieldNames(list);
					sourceMapping.SourceTierPricingMapping.SourceTierPricingMappingItems = list;
				}
				else
				{
					sourceMapping.SourceTierPricingMapping.SourceTierPricingMappingItems = null;
				}
			}
			if (this._sourceMappingType == SourceMappingType.ProductInfo)
			{
				if (this.dgSourceMapping.DataSource != null)
				{
					List<SourceItemMapping> list2 = ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>();
					this.SetSourceMappingFieldNames(list2);
					sourceMapping.SourceProdInfoMapping.Items = list2;
				}
				else
				{
					sourceMapping.SourceProdInfoMapping.Items = null;
				}
			}
			if (this._sourceMappingType == SourceMappingType.Product_Prices)
			{
				if (this.dgSourceMapping.DataSource != null)
				{
					List<SourceItemMapping> list3 = ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>();
					this.SetSourceMappingFieldNames(list3);
					sourceMapping.SourceProdPriceMapping.Items = list3;
				}
				else
				{
					sourceMapping.SourceProdPriceMapping.Items = null;
				}
			}
			if (this._sourceMappingType == SourceMappingType.ProductPictures)
			{
				if (this.dgSourceMapping.DataSource != null)
				{
					List<SourceItemMapping> list4 = ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>();
					this.SetSourceMappingFieldNames(list4);
					sourceMapping.SourceProdPicturesMapping.Items = list4;
				}
				else
				{
					sourceMapping.SourceProdPicturesMapping.Items = null;
				}
			}
			if (this._sourceMappingType == SourceMappingType.ProductAttributes)
			{
				if (this.dgSourceMapping.DataSource != null)
				{
					List<SourceItemMapping> list5 = ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>();
					this.SetSourceMappingFieldNames(list5);
					sourceMapping.SourceProdAttMapping.SourceProdAttMappingItems = list5;
				}
				else
				{
					sourceMapping.SourceProdAttMapping.SourceProdAttMappingItems = null;
				}
			}
			if (this._sourceMappingType == SourceMappingType.ProductSpecAttributes)
			{
				if (this.dgSourceMapping.DataSource != null)
				{
					List<SourceItemMapping> list6 = ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>();
					this.SetSourceMappingFieldNames(list6);
					sourceMapping.SourceProdSpecAttMapping.SourceProdSpecAttMappingItems = list6;
				}
				else
				{
					sourceMapping.SourceProdSpecAttMapping.SourceProdSpecAttMappingItems = null;
				}
			}
			if (this._sourceMappingType == SourceMappingType.AddressCustomAttributtes)
			{
				if (this.dgSourceMapping.DataSource != null)
				{
					List<SourceItemMapping> list7 = ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>();
					this.SetSourceMappingFieldNames(list7);
					sourceMapping.SourceAddressCustomAttMapping.Items = list7;
				}
				else
				{
					sourceMapping.SourceAddressCustomAttMapping.Items = null;
				}
			}
			if (this._sourceMappingType == SourceMappingType.CustomerCustomAttributtes)
			{
				if (this.dgSourceMapping.DataSource != null)
				{
					List<SourceItemMapping> list8 = ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>();
					this.SetSourceMappingFieldNames(list8);
					sourceMapping.SourceCustomerCustomAttMapping.Items = list8;
				}
				else
				{
					sourceMapping.SourceCustomerCustomAttMapping.Items = null;
				}
			}
			if (this._sourceMappingType == SourceMappingType.Customer)
			{
				if (this.dgSourceMapping.DataSource != null)
				{
					List<SourceItemMapping> list9 = ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>();
					this.SetSourceMappingFieldNames(list9);
					sourceMapping.SourceCustomerMapping.Items = list9;
				}
				else
				{
					sourceMapping.SourceCustomerMapping.Items = null;
				}
			}
			if (this._sourceMappingType == SourceMappingType.CustomerRole)
			{
				if (this.dgSourceMapping.DataSource != null)
				{
					List<SourceItemMapping> list10 = ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>();
					this.SetSourceMappingFieldNames(list10);
					sourceMapping.SourceCustomerRoleMapping.Items = list10;
				}
				else
				{
					sourceMapping.SourceCustomerRoleMapping.Items = null;
				}
			}
			if (this._sourceMappingType == SourceMappingType.ShippingAddress)
			{
				if (this.dgSourceMapping.DataSource != null)
				{
					List<SourceItemMapping> list11 = ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>();
					this.SetSourceMappingFieldNames(list11);
					sourceMapping.SourceCustomerShippingAddressMapping.Items = list11;
				}
				else
				{
					sourceMapping.SourceCustomerShippingAddressMapping.Items = null;
				}
			}
			if (this._sourceMappingType == SourceMappingType.BillingAddress)
			{
				if (this.dgSourceMapping.DataSource != null)
				{
					List<SourceItemMapping> list12 = ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>();
					this.SetSourceMappingFieldNames(list12);
					sourceMapping.SourceCustomerBillingAddressMapping.Items = list12;
				}
				else
				{
					sourceMapping.SourceCustomerBillingAddressMapping.Items = null;
				}
			}
			if (this._sourceMappingType == SourceMappingType.CustomerOrders)
			{
				if (this.dgSourceMapping.DataSource != null)
				{
					List<SourceItemMapping> list13 = ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>();
					this.SetSourceMappingFieldNames(list13);
					sourceMapping.SourceCustomerOrdersMapping.Items = list13;
				}
				else
				{
					sourceMapping.SourceCustomerOrdersMapping.Items = null;
				}
			}
			if (this._sourceMappingType == SourceMappingType.CustomerOthers)
			{
				if (this.dgSourceMapping.DataSource != null)
				{
					List<SourceItemMapping> list14 = ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>();
					this.SetSourceMappingFieldNames(list14);
					sourceMapping.SourceCustomerOthersMapping.Items = list14;
				}
				else
				{
					sourceMapping.SourceCustomerOthersMapping.Items = null;
				}
			}
			if (this._sourceMappingType == SourceMappingType.WishList)
			{
				if (this.dgSourceMapping.DataSource != null)
				{
					List<SourceItemMapping> list15 = ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>();
					this.SetSourceMappingFieldNames(list15);
					sourceMapping.SourceWishListMapping.Items = list15;
				}
				else
				{
					sourceMapping.SourceWishListMapping.Items = null;
				}
			}
		}

		private void DataGridSettings()
		{
			this.dgSourceMapping.ShowEditingIcon = true;
			this.dgSourceMapping.AllowUserToAddRows = false;
			this.dgSourceMapping.AllowUserToDeleteRows = false;
			this.dgSourceMapping.AllowUserToOrderColumns = false;
			this.dgSourceMapping.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			this.dgSourceMapping.EditMode = DataGridViewEditMode.EditOnEnter;
			this.dgSourceMapping.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
			this.dgSourceMapping.MultiSelect = false;
			this.dgSourceMapping.Refresh();
			this.dgSourceMapping.ClearSelection();
		}

		private void SourceMappingColumnsLoad(StructureFormat structureFormat)
		{
			this.dgSourceMapping.Columns["Id"].Visible = false;
			this.dgSourceMapping.Columns["MappingItemType"].Visible = false;
			this.dgSourceMapping.Columns["FieldIndex"].Visible = false;
			this.dgSourceMapping.Columns["FieldRule1"].Visible = false;
			if (structureFormat == StructureFormat.Position)
			{
				this.dgSourceMapping.Columns["FieldMappingForXml"].Visible = false;
				this.dgSourceMapping.Columns["FieldMappingForIndex"].Visible = true;
			}
			else
			{
				this.dgSourceMapping.Columns["FieldMappingForXml"].Visible = true;
				this.dgSourceMapping.Columns["FieldMappingForIndex"].Visible = false;
			}
			if (this._sourceMappingType != SourceMappingType.TierPricing)
			{
			}
			if (this._sourceMappingType == SourceMappingType.ProductInfo || this._sourceMappingType == SourceMappingType.ProductAttributes || this._sourceMappingType == SourceMappingType.ProductSpecAttributes || this._sourceMappingType == SourceMappingType.ProductPictures || this._sourceMappingType == SourceMappingType.CustomerRole)
			{
				this.dgSourceMapping.Columns["FieldRule1"].Visible = true;
				this.dgSourceMapping.Columns["FieldRule1"].HeaderText = "Delimiter To Split";
			}
			foreach (object obj in this.dgSourceMapping.Columns)
			{
				DataGridViewColumn dataGridViewColumn = (DataGridViewColumn)obj;
				dataGridViewColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
			}
			this.dgSourceMapping.Columns["FieldName"].Width = 300;
			this.dgSourceMapping.Columns["FieldMappingForXml"].Width = 100;
			this.dgSourceMapping.Columns["FieldMappingForIndex"].Width = 80;
			this.dgSourceMapping.Columns["SourceParser"].Width = 50;
			this.dgSourceMapping.Columns["FieldRule1"].Width = 100;
			this.dgSourceMapping.Columns["SourceParser"].Visible = (this._sourceFieldList != null && this._sourceFieldList.Any<KeyValuePair<string, string>>());
			this.LoadSourceFields();
		}

		private void SetSourceMappingFieldNames(List<SourceItemMapping> listResult)
		{
			bool repeatItem = this._sourceMappingType != SourceMappingType.Product_Prices && this._sourceMappingType != SourceMappingType.ProductInfo && (this._sourceMappingType != SourceMappingType.Customer && this._sourceMappingType != SourceMappingType.ShippingAddress) && (this._sourceMappingType != SourceMappingType.BillingAddress && this._sourceMappingType != SourceMappingType.CustomerOthers);
			int index = 1;
			foreach (Guid guid in listResult.Select<SourceItemMapping, Guid>((Func<SourceItemMapping, Guid>)(x => x.Id)).ToList<Guid>().Distinct<Guid>())
			{
				Guid id = guid;
				if (listResult.FirstOrDefault<SourceItemMapping>((Func<SourceItemMapping, bool>)(x => x.Id == id)).MappingItemType > -1)
				{
					listResult.Where<SourceItemMapping>((Func<SourceItemMapping, bool>)(x => x.Id == id)).ToList<SourceItemMapping>().ForEach((Action<SourceItemMapping>)(x => x.FieldNameForDisplay = repeatItem ? x.FieldName + "(" + index.ToString() + ")" : x.FieldName));
					listResult.Where<SourceItemMapping>((Func<SourceItemMapping, bool>)(x => x.Id == id)).ToList<SourceItemMapping>().ForEach((Action<SourceItemMapping>)(x => x.FieldIndex = index));
					index++;
				}
				else
				{
					listResult.Where<SourceItemMapping>((Func<SourceItemMapping, bool>)(x => x.Id == id)).ToList<SourceItemMapping>().ForEach((Action<SourceItemMapping>)(x => x.FieldNameForDisplay = x.FieldName));
					listResult.Where<SourceItemMapping>((Func<SourceItemMapping, bool>)(x => x.Id == id)).ToList<SourceItemMapping>().ForEach((Action<SourceItemMapping>)(x => x.FieldIndex = 0));
				}
			}
			listResult.OrderBy<SourceItemMapping, int>((Func<SourceItemMapping, int>)(o => o.FieldIndex)).ThenBy<SourceItemMapping, int>((Func<SourceItemMapping, int>)(o => o.Order));
		}

		private void dgSourceMapping_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
		{
			int num = GlobalClass.StringToInteger(this.dgSourceMapping["FieldIndex", e.RowIndex].Value.ToString(), 0);
			GlobalClass.StringToInteger(this.dgSourceMapping["Group", e.RowIndex].Value.ToString(), 0);
			if (num > 0 && num % 2 == 0)
			{
				this.dgSourceMapping.Rows[e.RowIndex].DefaultCellStyle.BackColor = ColorTranslator.FromHtml("#DCDCDC");
			}
			MappingItemType mappingItemType = (MappingItemType)GlobalClass.StringToInteger(this.dgSourceMapping["MappingItemType", e.RowIndex].Value.ToString(), 0);
			if (mappingItemType != NopTalkCore.MappingItemType.Related_ProductSku && mappingItemType != NopTalkCore.MappingItemType.CrossSell_ProductSku && mappingItemType != NopTalkCore.MappingItemType.ProductInfo_ProductTag && mappingItemType != NopTalkCore.MappingItemType.ProdAttValue_Value && mappingItemType != NopTalkCore.MappingItemType.ProdSpecAtt_Value && mappingItemType != NopTalkCore.MappingItemType.ProductPicture_Path && mappingItemType != NopTalkCore.MappingItemType.CustomerRole_RoleName)
			{
				this.dgSourceMapping.Rows[e.RowIndex].Cells["FieldRule1"].Style.BackColor = ColorTranslator.FromHtml("#f5f5f5");
				this.dgSourceMapping.Rows[e.RowIndex].Cells["FieldRule1"].ReadOnly = true;
				this.dgSourceMapping.Rows[e.RowIndex].Cells["FieldRule1"].Style.Padding = new Padding(this.dgSourceMapping.Rows[e.RowIndex].Cells["FieldRule1"].OwningColumn.Width, 0, 0, 0);
			}
			if (mappingItemType == NopTalkCore.MappingItemType.ProductInfo_ProductId || mappingItemType == NopTalkCore.MappingItemType.ProductInfo_ProductSku)
			{
				this.dgSourceMapping.Rows[e.RowIndex].Cells["FieldRule"].Style.BackColor = ColorTranslator.FromHtml("#f5f5f5");
				this.dgSourceMapping.Rows[e.RowIndex].Cells["FieldRule"].ReadOnly = true;
				this.dgSourceMapping.Rows[e.RowIndex].Cells["FieldRule"].Style.Padding = new Padding(this.dgSourceMapping.Rows[e.RowIndex].Cells["FieldRule1"].OwningColumn.Width, 0, 0, 0);
			}
		}

		private void btnSourceMappingAdd_Click(object sender, EventArgs e)
		{
			this.SourceMappingAdd(false, false);
		}

		private void btnSourceMappingDelete_Click(object sender, EventArgs e)
		{
			try
			{
				List<SourceItemMapping> list = (this.dgSourceMapping.DataSource != null) ? ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>() : null;
				if (this.dgSourceMapping.SelectedRows.Count > 0)
				{
					Guid id = Guid.Parse(this.dgSourceMapping.SelectedRows[0].Cells["Id"].Value.ToString());
					DialogResult dialogResult = MessageBox.Show("Are sure you want to delete this record?", "Delete record", MessageBoxButtons.YesNo);
					if (dialogResult == DialogResult.Yes)
					{
						list.RemoveAll((SourceItemMapping x) => x.Id == id);
						this.SetSourceMappingFieldNames(list);
						this.dgSourceMapping.DataSource = list.ToList<SourceItemMapping>().ConvertToDataTable<SourceItemMapping>();
					}
				}
				else
				{
					MessageBox.Show("Please select the record.");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void SourceMappingAdd(bool firstLoad, bool force = false)
		{
			List<SourceItemMapping> list = (this.dgSourceMapping.DataSource != null) ? ((DataTable)this.dgSourceMapping.DataSource).ConvertToList<SourceItemMapping>() : null;
			if (!firstLoad || list == null || !list.Any<SourceItemMapping>() || force)
			{
				if (this._sourceMappingType == SourceMappingType.TierPricing)
				{
					if ((list == null || !list.Any<SourceItemMapping>()) && this._structureFormat == StructureFormat.XML)
					{
						list.AddRange(SourceMappingHelper.GetRepeatNode());
					}
					list.AddRange(SourceMappingHelper.GetTierPricingNewItem(force, list));
				}
				if (this._sourceMappingType == SourceMappingType.ProductInfo)
				{
					if ((list != null && list.Any<SourceItemMapping>()) || this._structureFormat != StructureFormat.XML)
					{
					}
					list.AddRange(SourceMappingHelper.GetProdInfoNewItem(force, list));
				}
				if (this._sourceMappingType == SourceMappingType.Product_Prices)
				{
					list.AddRange(SourceMappingHelper.GetProdPriceNewItem(force, list));
				}
				if (this._sourceMappingType == SourceMappingType.ProductPictures)
				{
					if ((list == null || !list.Any<SourceItemMapping>()) && this._structureFormat == StructureFormat.XML)
					{
						list.AddRange(SourceMappingHelper.GetRepeatNode());
					}
					list.AddRange(SourceMappingHelper.GetProdPictureNewItem());
				}
				if (this._sourceMappingType == SourceMappingType.ProductAttributes)
				{
					if ((list == null || !list.Any<SourceItemMapping>()) && this._structureFormat == StructureFormat.XML)
					{
						list.AddRange(SourceMappingHelper.GetRepeatNode());
					}
					list.AddRange(SourceMappingHelper.GetProdAttNewItem(force, list));
					list = SourceMappingHelper.ProdAttNewItemNamingOrder(list);
				}
				if (this._sourceMappingType == SourceMappingType.ProductSpecAttributes)
				{
					if ((list == null || !list.Any<SourceItemMapping>()) && this._structureFormat == StructureFormat.XML)
					{
						list.AddRange(SourceMappingHelper.GetRepeatNode());
					}
					list.AddRange(SourceMappingHelper.GetProdSpecAttNewItem());
				}
				if (this._sourceMappingType == SourceMappingType.AddressCustomAttributtes)
				{
					if ((list == null || !list.Any<SourceItemMapping>()) && this._structureFormat == StructureFormat.XML)
					{
						list.AddRange(SourceMappingHelper.GetRepeatNode());
					}
					list.AddRange(SourceMappingHelper.GetAddressCustomAttNewItem());
				}
				if (this._sourceMappingType == SourceMappingType.CustomerCustomAttributtes)
				{
					if ((list == null || !list.Any<SourceItemMapping>()) && this._structureFormat == StructureFormat.XML)
					{
						list.AddRange(SourceMappingHelper.GetRepeatNode());
					}
					list.AddRange(SourceMappingHelper.GetCustomerCustomAttNewItem());
				}
				if (this._sourceMappingType == SourceMappingType.Customer)
				{
					if ((list == null || !list.Any<SourceItemMapping>()) && this._structureFormat == StructureFormat.XML)
					{
						list.AddRange(SourceMappingHelper.GetRepeatNode());
					}
					list.AddRange(SourceMappingHelper.GetCustomerNewItem());
				}
				if (this._sourceMappingType == SourceMappingType.ShippingAddress || this._sourceMappingType == SourceMappingType.BillingAddress)
				{
					if ((list == null || !list.Any<SourceItemMapping>()) && this._structureFormat == StructureFormat.XML)
					{
						list.AddRange(SourceMappingHelper.GetRepeatNode());
					}
					list.AddRange(SourceMappingHelper.GetCustomerAddressNewItem());
				}
				if (this._sourceMappingType == SourceMappingType.CustomerRole)
				{
					if ((list == null || !list.Any<SourceItemMapping>()) && this._structureFormat == StructureFormat.XML)
					{
						list.AddRange(SourceMappingHelper.GetRepeatNode());
					}
					list.AddRange(SourceMappingHelper.GetCustomerRoleNewItem());
				}
				if (this._sourceMappingType == SourceMappingType.CustomerOthers)
				{
					if ((list == null || !list.Any<SourceItemMapping>()) && this._structureFormat == StructureFormat.XML)
					{
						list.AddRange(SourceMappingHelper.GetRepeatNode());
					}
					list.AddRange(SourceMappingHelper.GetCustomerOtherNewItem());
				}
				if (this._sourceMappingType == SourceMappingType.WishList)
				{
					if ((list == null || !list.Any<SourceItemMapping>()) && this._structureFormat == StructureFormat.XML)
					{
						list.AddRange(SourceMappingHelper.GetRepeatNode());
					}
					list.AddRange(SourceMappingHelper.GetWishListNewItem());
				}
				this.SetSourceMappingFieldNames(list);
				this.dgSourceMapping.DataSource = (from o in list
				orderby o.FieldIndex, o.Order
				select o).ToList<SourceItemMapping>().ConvertToDataTable<SourceItemMapping>();
			}
		}

		private void dgSourceMapping_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control is ComboBox && this.dgSourceMapping.CurrentCell.OwningColumn.Name == "SourceParser")
			{
				ComboBox comboBox = (ComboBox)e.Control;
				comboBox.SelectedIndexChanged += this.SourceParser_OnSelectIndexChanged;
			}
		}

		private void SourceParser_OnSelectIndexChanged(object sender, EventArgs e)
		{
			ComboBox comboBox = (ComboBox)sender;
			if (comboBox.SelectedIndex > 0)
			{
				if (this.dgSourceMapping.Columns["FieldMappingForXml"].Visible)
				{
					this.dgSourceMapping.CurrentRow.Cells["FieldMappingForXml"].Value = comboBox.SelectedValue;
				}
				if (this.dgSourceMapping.Columns["FieldMappingForIndex"].Visible)
				{
					this.dgSourceMapping.CurrentRow.Cells["FieldMappingForIndex"].Value = GlobalClass.StringToInteger(comboBox.SelectedValue, 0);
				}
				comboBox.SelectedIndex = -1;
			}
		}

		private void LoadSourceFields()
		{
			if (this._sourceFieldList != null && this._sourceFieldList.Any<KeyValuePair<string, string>>())
			{
				var list = (from mode in this._sourceFieldList
				select new
				{
					Value = mode.Key,
					Title = (string.IsNullOrEmpty(mode.Value) ? mode.Key : (mode.Key + " (" + mode.Value + ")"))
				}).ToList();
				list.Insert(0, new
				{
					Value = "",
					Title = ""
				});
				this.SourceParser.ValueMember = "Value";
				this.SourceParser.DisplayMember = "Title";
				this.SourceParser.DataSource = list.ToList();
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
			this.dgSourceMapping = new DataGridView();
			this.btnSourceMappingDelete = new Button();
			this.btnSourceMappingAdd = new Button();
			this.dataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn2 = new DataGridViewTextBoxColumn();
			this.dataGridViewNumericUpDownColumn1 = new DataGridViewNumericUpDownColumn();
			this.dataGridViewTextBoxColumn3 = new DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn4 = new DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn5 = new DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn6 = new DataGridViewTextBoxColumn();
			this.Id = new DataGridViewTextBoxColumn();
			this.FieldName = new DataGridViewTextBoxColumn();
			this.FieldMappingForIndex = new DataGridViewNumericUpDownColumn();
			this.FieldMappingForXml = new DataGridViewTextBoxColumn();
			this.SourceParser = new DataGridViewComboBoxColumn();
			this.FieldRule = new DataGridViewTextBoxColumn();
			this.FieldRule1 = new DataGridViewTextBoxColumn();
			this.MappingItemType = new DataGridViewTextBoxColumn();
			this.FieldIndex = new DataGridViewTextBoxColumn();
			this.Group = new DataGridViewTextBoxColumn();
			this.Empty = new DataGridViewTextBoxColumn();
			((ISupportInitialize)this.dgSourceMapping).BeginInit();
			base.SuspendLayout();
			this.dgSourceMapping.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.dgSourceMapping.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgSourceMapping.Columns.AddRange(new DataGridViewColumn[]
			{
				this.Id,
				this.FieldName,
				this.FieldMappingForIndex,
				this.FieldMappingForXml,
				this.SourceParser,
				this.FieldRule,
				this.FieldRule1,  
				this.MappingItemType,
				this.FieldIndex,
				this.Group,
				this.Empty
			});
			this.dgSourceMapping.Location = new Point(4, 4);
			this.dgSourceMapping.Margin = new Padding(4);
			this.dgSourceMapping.MultiSelect = false;
			this.dgSourceMapping.Name = "dgSourceMapping";
			this.dgSourceMapping.RowHeadersWidth = 51;
			this.dgSourceMapping.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			this.dgSourceMapping.Size = new Size(871, 255);
			this.dgSourceMapping.TabIndex = 1;
			this.dgSourceMapping.EditingControlShowing += this.dgSourceMapping_EditingControlShowing;
			this.btnSourceMappingDelete.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			this.btnSourceMappingDelete.FlatStyle = FlatStyle.Flat;
			this.btnSourceMappingDelete.Location = new Point(111, 270);
			this.btnSourceMappingDelete.Margin = new Padding(4);
			this.btnSourceMappingDelete.Name = "btnSourceMappingDelete";
			this.btnSourceMappingDelete.Size = new Size(100, 28);
			this.btnSourceMappingDelete.TabIndex = 31;
			this.btnSourceMappingDelete.Text = "Delete";
			this.btnSourceMappingDelete.UseVisualStyleBackColor = true;
			this.btnSourceMappingDelete.Click += this.btnSourceMappingDelete_Click;
			this.btnSourceMappingAdd.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			this.btnSourceMappingAdd.FlatStyle = FlatStyle.Flat;
			this.btnSourceMappingAdd.Location = new Point(4, 270);
			this.btnSourceMappingAdd.Margin = new Padding(4);
			this.btnSourceMappingAdd.Name = "btnSourceMappingAdd";
			this.btnSourceMappingAdd.Size = new Size(99, 28);
			this.btnSourceMappingAdd.TabIndex = 30;
			this.btnSourceMappingAdd.Text = "Add";
			this.btnSourceMappingAdd.UseVisualStyleBackColor = true;
			this.btnSourceMappingAdd.Click += this.btnSourceMappingAdd_Click;
			this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
			this.dataGridViewTextBoxColumn1.HeaderText = "Id";
			this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
			this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
			this.dataGridViewTextBoxColumn1.ReadOnly = true;
			this.dataGridViewTextBoxColumn1.Width = 125;
			this.dataGridViewTextBoxColumn2.DataPropertyName = "FieldNameForDisplay";
			this.dataGridViewTextBoxColumn2.HeaderText = "Field Name";
			this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
			this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
			this.dataGridViewTextBoxColumn2.ReadOnly = true;
			this.dataGridViewTextBoxColumn2.Resizable = DataGridViewTriState.True;
			this.dataGridViewTextBoxColumn2.Width = 125;
			this.dataGridViewNumericUpDownColumn1.DataPropertyName = "FieldMappingForIndex";
			this.dataGridViewNumericUpDownColumn1.HeaderText = "Field Mapping (Column Index)";
			this.dataGridViewNumericUpDownColumn1.Minimum = new decimal(new int[]
			{
				1,
				0,
				0,
				int.MinValue
			});
			this.dataGridViewNumericUpDownColumn1.MinimumWidth = 6;
			this.dataGridViewNumericUpDownColumn1.Name = "dataGridViewNumericUpDownColumn1";
			this.dataGridViewNumericUpDownColumn1.Resizable = DataGridViewTriState.True;
			this.dataGridViewNumericUpDownColumn1.SortMode = DataGridViewColumnSortMode.Automatic;
			this.dataGridViewNumericUpDownColumn1.Width = 125;
			this.dataGridViewTextBoxColumn3.DataPropertyName = "FieldMappingForXml";
			this.dataGridViewTextBoxColumn3.HeaderText = "Field Mapping (XML XPath)";
			this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			this.dataGridViewTextBoxColumn3.Width = 125;
			this.dataGridViewTextBoxColumn4.DataPropertyName = "FieldRule";
			this.dataGridViewTextBoxColumn4.HeaderText = "Custom Rule";
			this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
			this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
			this.dataGridViewTextBoxColumn4.Resizable = DataGridViewTriState.True;
			this.dataGridViewTextBoxColumn4.Width = 125;
			this.dataGridViewTextBoxColumn5.DataPropertyName = "MappingItemType";
			this.dataGridViewTextBoxColumn5.HeaderText = "MappingItemType";
			this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
			this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
			this.dataGridViewTextBoxColumn5.ReadOnly = true;
			this.dataGridViewTextBoxColumn5.Width = 125;
			this.dataGridViewTextBoxColumn6.DataPropertyName = "FieldIndex";
			this.dataGridViewTextBoxColumn6.HeaderText = "FieldIndex";
			this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
			this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
			this.dataGridViewTextBoxColumn6.ReadOnly = true;
			this.dataGridViewTextBoxColumn6.Width = 125;
			this.Id.DataPropertyName = "Id";
			this.Id.HeaderText = "Id";
			this.Id.MinimumWidth = 6;
			this.Id.Name = "Id";
			this.Id.ReadOnly = true;
			this.Id.Width = 125;
			this.FieldName.DataPropertyName = "FieldNameForDisplay";
			this.FieldName.HeaderText = "Field Name";
			this.FieldName.MinimumWidth = 6;
			this.FieldName.Name = "FieldName";
			this.FieldName.ReadOnly = true;
			this.FieldName.Resizable = DataGridViewTriState.True;
			this.FieldName.Width = 125;
			this.FieldMappingForIndex.DataPropertyName = "FieldMappingForIndex";
			this.FieldMappingForIndex.HeaderText = "Field Mapping (Column Index)";
			this.FieldMappingForIndex.Minimum = new decimal(new int[]
			{
				1,
				0,
				0,
				int.MinValue
			});
			this.FieldMappingForIndex.MinimumWidth = 6;
			this.FieldMappingForIndex.Name = "FieldMappingForIndex";
			this.FieldMappingForIndex.Resizable = DataGridViewTriState.True;
			this.FieldMappingForIndex.SortMode = DataGridViewColumnSortMode.Automatic;
			this.FieldMappingForIndex.Width = 125;
			this.FieldMappingForXml.DataPropertyName = "FieldMappingForXml";
			this.FieldMappingForXml.HeaderText = "Field Mapping (XML XPath)";
			this.FieldMappingForXml.MinimumWidth = 6;
			this.FieldMappingForXml.Name = "FieldMappingForXml";
			this.FieldMappingForXml.Width = 125;
			dataGridViewCellStyle.BackColor = Color.FromArgb(255, 255, 192);
			dataGridViewCellStyle.ForeColor = Color.Black;
			dataGridViewCellStyle.SelectionBackColor = Color.White;
			dataGridViewCellStyle.SelectionForeColor = Color.Black;
			this.SourceParser.DefaultCellStyle = dataGridViewCellStyle;
			this.SourceParser.DropDownWidth = 500;
			this.SourceParser.FlatStyle = FlatStyle.Flat;
			this.SourceParser.HeaderText = "Parser";
			this.SourceParser.Items.AddRange(new object[]
			{
				"testas testas",
				"testas testas testas",
				"testas testas testas testas",
				"testas testas",
				"testas testas testas",
				"testas testas testas testas",
				"testas testas",
				"testas testas testas",
				"testas testas testas testas",
				"testas testas",
				"testas testas testas",
				"testas testas testas testas",
				"testas testas",
				"testas testas testas",
				"testas testas testas testas"
			});
			this.SourceParser.MinimumWidth = 6;
			this.SourceParser.Name = "SourceParser";
			this.SourceParser.Resizable = DataGridViewTriState.True;
			this.SourceParser.Width = 125;
			this.FieldRule.DataPropertyName = "FieldRule";
			this.FieldRule.HeaderText = "Custom Rule";
			this.FieldRule.MinimumWidth = 6;
			this.FieldRule.Name = "FieldRule";
			this.FieldRule.Resizable = DataGridViewTriState.True;
			this.FieldRule.Width = 125;
			this.FieldRule1.DataPropertyName = "FieldRule1";
			this.FieldRule1.HeaderText = "Field Rule1";
			this.FieldRule1.MinimumWidth = 6;
			this.FieldRule1.Name = "FieldRule1";
			this.FieldRule1.Width = 125;
			this.MappingItemType.DataPropertyName = "MappingItemType";
			this.MappingItemType.HeaderText = "MappingItemType";
			this.MappingItemType.MinimumWidth = 6;
			this.MappingItemType.Name = "MappingItemType";
			this.MappingItemType.ReadOnly = true;
			this.MappingItemType.Width = 125;
			this.FieldIndex.DataPropertyName = "FieldIndex";
			this.FieldIndex.HeaderText = "FieldIndex";
			this.FieldIndex.MinimumWidth = 6;
			this.FieldIndex.Name = "FieldIndex";
			this.FieldIndex.ReadOnly = true;
			this.FieldIndex.Width = 125;
			this.Group.DataPropertyName = "Group";
			this.Group.HeaderText = "Group";
			this.Group.MinimumWidth = 6;
			this.Group.Name = "Group";
			this.Group.ReadOnly = true;
			this.Group.Visible = false;
			this.Group.Width = 125;
			this.Empty.HeaderText = "";
			this.Empty.MinimumWidth = 6;
			this.Empty.Name = "Empty";
			this.Empty.ReadOnly = true;
			this.Empty.Width = 125;
			base.AutoScaleDimensions = new SizeF(8f, 16f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.Controls.Add(this.btnSourceMappingDelete);
			base.Controls.Add(this.btnSourceMappingAdd);
			base.Controls.Add(this.dgSourceMapping);
			base.Name = "sourceMappingDataGrid";
			base.Size = new Size(879, 310);
			((ISupportInitialize)this.dgSourceMapping).EndInit();
			base.ResumeLayout(false);
		}

		private IContainer components;

		private DataGridView dgSourceMapping;

		private Button btnSourceMappingDelete;

		private Button btnSourceMappingAdd;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;

		private DataGridViewNumericUpDownColumn dataGridViewNumericUpDownColumn1;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;

		private DataGridViewNumericUpDownColumn FieldMappingForIndex;

		private DataGridViewTextBoxColumn Id;

		private DataGridViewTextBoxColumn FieldName;

		private DataGridViewTextBoxColumn FieldMappingForXml;

		private DataGridViewComboBoxColumn SourceParser;

		private DataGridViewTextBoxColumn FieldRule;

		private DataGridViewTextBoxColumn FieldRule1;

		private DataGridViewTextBoxColumn MappingItemType;

		private DataGridViewTextBoxColumn FieldIndex;

		private DataGridViewTextBoxColumn Group;

		private DataGridViewTextBoxColumn Empty;
	}
}
