﻿namespace NopTalk
{
	// Token: 0x0200000C RID: 12
	public partial class categoryEdit : global::System.Windows.Forms.Form
	{
		// Token: 0x06000053 RID: 83 RVA: 0x00002588 File Offset: 0x00000788
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000054 RID: 84 RVA: 0x00007D58 File Offset: 0x00005F58
		private void InitializeComponent()
		{
			this.statusAreaText = new global::System.Windows.Forms.ToolStripStatusLabel();
			this.btnFilterClear = new global::System.Windows.Forms.Button();
			this.chPublished = new global::System.Windows.Forms.CheckBox();
			this.label5 = new global::System.Windows.Forms.Label();
			this.btnFilter = new global::System.Windows.Forms.Button();
			this.btnClose = new global::System.Windows.Forms.Button();
			this.btnSave = new global::System.Windows.Forms.Button();
			this.dgDataEditor = new global::System.Windows.Forms.DataGridView();
			this.tbSearch_Name = new global::System.Windows.Forms.TextBox();
			this.label3 = new global::System.Windows.Forms.Label();
			this.ddItemsLimit = new global::System.Windows.Forms.ComboBox();
			this.ddStoresConn = new global::System.Windows.Forms.ComboBox();
			this.label1 = new global::System.Windows.Forms.Label();
			this.statusArea = new global::System.Windows.Forms.StatusStrip();
			this.label2 = new global::System.Windows.Forms.Label();
			this.groupBox1 = new global::System.Windows.Forms.GroupBox();
			this.clbColumnsList = new global::System.Windows.Forms.CheckedListBox();
			this.label6 = new global::System.Windows.Forms.Label();
			this.lblStatistic = new global::System.Windows.Forms.Label();
			((global::System.ComponentModel.ISupportInitialize)this.dgDataEditor).BeginInit();
			this.statusArea.SuspendLayout();
			this.groupBox1.SuspendLayout();
			base.SuspendLayout();
			this.statusAreaText.Name = "statusAreaText";
			this.statusAreaText.Size = new global::System.Drawing.Size(0, 22);
			this.btnFilterClear.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnFilterClear.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnFilterClear.Location = new global::System.Drawing.Point(1000, 42);
			this.btnFilterClear.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnFilterClear.Name = "btnFilterClear";
			this.btnFilterClear.Size = new global::System.Drawing.Size(181, 30);
			this.btnFilterClear.TabIndex = 23;
			this.btnFilterClear.Text = "Clear Filter";
			this.btnFilterClear.UseVisualStyleBackColor = true;
			this.btnFilterClear.Click += new global::System.EventHandler(this.btnFilterClear_Click);
			this.chPublished.AutoSize = true;
			this.chPublished.Location = new global::System.Drawing.Point(130, 94);
			this.chPublished.Name = "chPublished";
			this.chPublished.Size = new global::System.Drawing.Size(18, 17);
			this.chPublished.TabIndex = 22;
			this.chPublished.UseVisualStyleBackColor = true;
			this.label5.AutoSize = true;
			this.label5.Location = new global::System.Drawing.Point(6, 94);
			this.label5.Name = "label5";
			this.label5.Size = new global::System.Drawing.Size(70, 17);
			this.label5.TabIndex = 21;
			this.label5.Text = "Published";
			this.btnFilter.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnFilter.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnFilter.Location = new global::System.Drawing.Point(1000, 83);
			this.btnFilter.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnFilter.Name = "btnFilter";
			this.btnFilter.Size = new global::System.Drawing.Size(181, 32);
			this.btnFilter.TabIndex = 5;
			this.btnFilter.Text = "Filter";
			this.btnFilter.UseVisualStyleBackColor = true;
			this.btnFilter.Click += new global::System.EventHandler(this.btnFilter_Click);
			this.btnClose.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnClose.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnClose.Location = new global::System.Drawing.Point(1100, 483);
			this.btnClose.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new global::System.Drawing.Size(100, 28);
			this.btnClose.TabIndex = 34;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new global::System.EventHandler(this.btnClose_Click);
			this.btnSave.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnSave.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnSave.Location = new global::System.Drawing.Point(992, 483);
			this.btnSave.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new global::System.Drawing.Size(100, 28);
			this.btnSave.TabIndex = 33;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new global::System.EventHandler(this.btnSave_Click);
			this.dgDataEditor.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.dgDataEditor.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgDataEditor.Location = new global::System.Drawing.Point(13, 140);
			this.dgDataEditor.Name = "dgDataEditor";
			this.dgDataEditor.RowTemplate.Height = 24;
			this.dgDataEditor.Size = new global::System.Drawing.Size(1188, 336);
			this.dgDataEditor.TabIndex = 32;
			this.tbSearch_Name.Location = new global::System.Drawing.Point(130, 56);
			this.tbSearch_Name.Name = "tbSearch_Name";
			this.tbSearch_Name.Size = new global::System.Drawing.Size(376, 22);
			this.tbSearch_Name.TabIndex = 5;
			this.label3.AutoSize = true;
			this.label3.Location = new global::System.Drawing.Point(6, 56);
			this.label3.Name = "label3";
			this.label3.Size = new global::System.Drawing.Size(106, 17);
			this.label3.TabIndex = 4;
			this.label3.Text = "Category Name";
			this.ddItemsLimit.DropDownStyle = global::System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ddItemsLimit.FormattingEnabled = true;
			this.ddItemsLimit.Items.AddRange(new object[]
			{
				"20",
				"50",
				"100"
			});
			this.ddItemsLimit.Location = new global::System.Drawing.Point(429, 20);
			this.ddItemsLimit.Name = "ddItemsLimit";
			this.ddItemsLimit.Size = new global::System.Drawing.Size(77, 24);
			this.ddItemsLimit.TabIndex = 3;
			this.ddStoresConn.BackColor = global::System.Drawing.SystemColors.Window;
			this.ddStoresConn.DropDownStyle = global::System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ddStoresConn.FormattingEnabled = true;
			this.ddStoresConn.Location = new global::System.Drawing.Point(130, 20);
			this.ddStoresConn.Name = "ddStoresConn";
			this.ddStoresConn.Size = new global::System.Drawing.Size(204, 24);
			this.ddStoresConn.TabIndex = 1;
			this.ddStoresConn.SelectedIndexChanged += new global::System.EventHandler(this.ddStoresConn_SelectedIndexChanged);
			this.label1.AutoSize = true;
			this.label1.Location = new global::System.Drawing.Point(6, 23);
			this.label1.Name = "label1";
			this.label1.Size = new global::System.Drawing.Size(117, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "Store Connection";
			this.statusArea.AutoSize = false;
			this.statusArea.ImageScalingSize = new global::System.Drawing.Size(20, 20);
			this.statusArea.Items.AddRange(new global::System.Windows.Forms.ToolStripItem[]
			{
				this.statusAreaText
			});
			this.statusArea.Location = new global::System.Drawing.Point(0, 521);
			this.statusArea.Name = "statusArea";
			this.statusArea.Padding = new global::System.Windows.Forms.Padding(1, 0, 19, 0);
			this.statusArea.Size = new global::System.Drawing.Size(1213, 27);
			this.statusArea.TabIndex = 35;
			this.statusArea.Text = "statusArea";
			this.label2.AutoSize = true;
			this.label2.Location = new global::System.Drawing.Point(340, 23);
			this.label2.Name = "label2";
			this.label2.Size = new global::System.Drawing.Size(74, 17);
			this.label2.TabIndex = 2;
			this.label2.Text = "Items Limit";
			this.groupBox1.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.groupBox1.Controls.Add(this.clbColumnsList);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.btnFilterClear);
			this.groupBox1.Controls.Add(this.chPublished);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.btnFilter);
			this.groupBox1.Controls.Add(this.tbSearch_Name);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.ddItemsLimit);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.ddStoresConn);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new global::System.Drawing.Point(13, 7);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new global::System.Drawing.Size(1188, 127);
			this.groupBox1.TabIndex = 31;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Filter";
			this.clbColumnsList.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.clbColumnsList.CheckOnClick = true;
			this.clbColumnsList.Enabled = false;
			this.clbColumnsList.FormattingEnabled = true;
			this.clbColumnsList.Location = new global::System.Drawing.Point(601, 20);
			this.clbColumnsList.MultiColumn = true;
			this.clbColumnsList.Name = "clbColumnsList";
			this.clbColumnsList.Size = new global::System.Drawing.Size(392, 106);
			this.clbColumnsList.TabIndex = 27;
			this.label6.AutoSize = true;
			this.label6.Location = new global::System.Drawing.Point(533, 23);
			this.label6.Name = "label6";
			this.label6.Size = new global::System.Drawing.Size(62, 17);
			this.label6.TabIndex = 26;
			this.label6.Text = "Columns";
			this.lblStatistic.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left);
			this.lblStatistic.AutoSize = true;
			this.lblStatistic.Location = new global::System.Drawing.Point(12, 489);
			this.lblStatistic.Name = "lblStatistic";
			this.lblStatistic.Size = new global::System.Drawing.Size(108, 17);
			this.lblStatistic.TabIndex = 36;
			this.lblStatistic.Text = "Items Filtered: 0";
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(8f, 16f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(1213, 548);
			base.Controls.Add(this.lblStatistic);
			base.Controls.Add(this.btnClose);
			base.Controls.Add(this.btnSave);
			base.Controls.Add(this.dgDataEditor);
			base.Controls.Add(this.statusArea);
			base.Controls.Add(this.groupBox1);
			base.Name = "categoryEdit";
			this.Text = "Categories Editor";
			base.Load += new global::System.EventHandler(this.categoryEdit_Load);
			((global::System.ComponentModel.ISupportInitialize)this.dgDataEditor).EndInit();
			this.statusArea.ResumeLayout(false);
			this.statusArea.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		// Token: 0x04000022 RID: 34
		private global::System.ComponentModel.IContainer components;

		// Token: 0x04000023 RID: 35
		private global::System.Windows.Forms.ToolStripStatusLabel statusAreaText;

		// Token: 0x04000024 RID: 36
		private global::System.Windows.Forms.Button btnFilterClear;

		// Token: 0x04000025 RID: 37
		private global::System.Windows.Forms.CheckBox chPublished;

		// Token: 0x04000026 RID: 38
		private global::System.Windows.Forms.Label label5;

		// Token: 0x04000027 RID: 39
		private global::System.Windows.Forms.Button btnFilter;

		// Token: 0x04000028 RID: 40
		private global::System.Windows.Forms.Button btnClose;

		// Token: 0x04000029 RID: 41
		private global::System.Windows.Forms.Button btnSave;

		// Token: 0x0400002A RID: 42
		private global::System.Windows.Forms.DataGridView dgDataEditor;

		// Token: 0x0400002B RID: 43
		private global::System.Windows.Forms.TextBox tbSearch_Name;

		// Token: 0x0400002C RID: 44
		private global::System.Windows.Forms.Label label3;

		// Token: 0x0400002D RID: 45
		private global::System.Windows.Forms.ComboBox ddItemsLimit;

		// Token: 0x0400002E RID: 46
		private global::System.Windows.Forms.ComboBox ddStoresConn;

		// Token: 0x0400002F RID: 47
		private global::System.Windows.Forms.Label label1;

		// Token: 0x04000030 RID: 48
		private global::System.Windows.Forms.StatusStrip statusArea;

		// Token: 0x04000031 RID: 49
		private global::System.Windows.Forms.Label label2;

		// Token: 0x04000032 RID: 50
		private global::System.Windows.Forms.GroupBox groupBox1;

		// Token: 0x04000033 RID: 51
		private global::System.Windows.Forms.Label lblStatistic;

		// Token: 0x04000034 RID: 52
		private global::System.Windows.Forms.CheckedListBox clbColumnsList;

		// Token: 0x04000035 RID: 53
		private global::System.Windows.Forms.Label label6;
	}
}
