﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Windows.Forms;
using NopTalkCore;

namespace NopTalk
{
	// Token: 0x02000058 RID: 88
	public partial class frmVendorsList : Form
	{
		// Token: 0x06000439 RID: 1081 RVA: 0x00003DB1 File Offset: 0x00001FB1
		public frmVendorsList()
		{
			
			this.sourceMappingFunc = new SourceMappingFunc();
			this.components = null;
			this.InitializeComponent();
		}

		// Token: 0x0600043A RID: 1082 RVA: 0x0005E0E4 File Offset: 0x0005C2E4
		private async void frmVendorsList_Load(object sender, EventArgs e)
		{
			try
			{
				this.gridFill();
				
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		// Token: 0x0600043B RID: 1083 RVA: 0x0005E130 File Offset: 0x0005C330
		public void gridFill()
		{
			try
			{
				DataSettingsManager dataSettingsManager = new DataSettingsManager();
				string dataConnectionString = dataSettingsManager.LoadSettings(null).DataConnectionString;
				this.sqlConn = new SqlCeConnection(dataConnectionString);
				GlobalClass.adap = new SqlCeDataAdapter("SELECT Vendor.Id, Vendor.Name, Vendor.Description FROM Vendor Order by Vendor.Id", this.sqlConn);
				this.sqlBui = new SqlCeCommandBuilder(GlobalClass.adap);
				GlobalClass.dt = new DataTable();
				GlobalClass.adap.Fill(GlobalClass.dt);
				this.dgVendors.AutoGenerateColumns = true;
				this.dgVendors.DataSource = GlobalClass.dt;
				this.dgVendors.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
				this.dgVendors.ReadOnly = true;
			}
			catch
			{
			}
			finally
			{
				if (this.sqlConn != null && this.sqlConn.State == ConnectionState.Open)
				{
					this.sqlConn.Close();
				}
				if (this.sqlBui != null)
				{
					this.sqlBui.Dispose();
				}
			}
		}

		// Token: 0x0600043C RID: 1084 RVA: 0x0005E234 File Offset: 0x0005C434
		private void btnNew_Click(object sender, EventArgs e)
		{
			this.i = -1;
			new vendorNewEdit(this.i, this)
			{
				StartPosition = FormStartPosition.CenterParent,
				WindowState = FormWindowState.Maximized
			}.ShowDialog();
		}

		// Token: 0x0600043D RID: 1085 RVA: 0x0005E26C File Offset: 0x0005C46C
		private void btnEdit_Click(object sender, EventArgs e)
		{
			if (this.dgVendors.SelectedRows.Count > 0)
			{
				this.i = Convert.ToInt32(this.dgVendors.SelectedRows[0].Cells[0].Value.ToString());
				new vendorNewEdit(this.i, this)
				{
					StartPosition = FormStartPosition.CenterParent,
					WindowState = FormWindowState.Maximized
				}.ShowDialog();
			}
			else
			{
				MessageBox.Show("Please select the record.");
			}
		}

		// Token: 0x0600043E RID: 1086 RVA: 0x0005E2F0 File Offset: 0x0005C4F0
		private void dgVendors_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			this.i = Convert.ToInt32(this.dgVendors.SelectedRows[0].Cells[0].Value.ToString());
			new vendorNewEdit(this.i, this)
			{
				StartPosition = FormStartPosition.CenterParent,
				WindowState = FormWindowState.Maximized
			}.ShowDialog();
		}

		// Token: 0x0600043F RID: 1087 RVA: 0x0005E350 File Offset: 0x0005C550
		private void btnDelete_Click(object sender, EventArgs e)
		{
			try
			{
				if (this.dgVendors.SelectedRows.Count > 0)
				{
					int vendorId = GlobalClass.StringToInteger(this.dgVendors[0, this.dgVendors.SelectedRows[0].Index].Value, 0);
					DialogResult dialogResult = MessageBox.Show("Are sure you want to delete this record?", "Delete record", MessageBoxButtons.YesNo);
					if (dialogResult == DialogResult.Yes)
					{
						this.sourceMappingFunc.DeleteVendor(vendorId);
						this.gridFill();
					}
				}
				else
				{
					MessageBox.Show("Please select the record.");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x06000440 RID: 1088 RVA: 0x0000E0E4 File Offset: 0x0000C2E4
		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams createParams = base.CreateParams;
				createParams.ExStyle |= 33554432;
				return createParams;
			}
		}

		// Token: 0x04000852 RID: 2130
		private SqlCeConnection sqlConn;

		// Token: 0x04000853 RID: 2131
		private SqlCeCommandBuilder sqlBui;

		// Token: 0x04000854 RID: 2132
		private int i;

		// Token: 0x04000855 RID: 2133
		private SourceMappingFunc sourceMappingFunc;
	}
}
