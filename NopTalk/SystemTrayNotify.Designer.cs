﻿namespace NopTalk
{
	// Token: 0x02000038 RID: 56
	public partial class SystemTrayNotify : global::System.Windows.Forms.Form
	{
		// Token: 0x060001F4 RID: 500 RVA: 0x0000305F File Offset: 0x0000125F
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x060001F5 RID: 501 RVA: 0x00032E6C File Offset: 0x0003106C
		private void InitializeComponent()
		{
			global::System.ComponentModel.ComponentResourceManager componentResourceManager = new global::System.ComponentModel.ComponentResourceManager(typeof(global::NopTalk.SystemTrayNotify));
			this.pictureBox1 = new global::System.Windows.Forms.PictureBox();
			this.btnOk = new global::System.Windows.Forms.Button();
			this.btnCancel = new global::System.Windows.Forms.Button();
			((global::System.ComponentModel.ISupportInitialize)this.pictureBox1).BeginInit();
			base.SuspendLayout();
			this.pictureBox1.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBox1.Image = (global::System.Drawing.Image)componentResourceManager.GetObject("pictureBox1.Image");
			this.pictureBox1.InitialImage = (global::System.Drawing.Image)componentResourceManager.GetObject("pictureBox1.InitialImage");
			this.pictureBox1.Location = new global::System.Drawing.Point(12, 12);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new global::System.Drawing.Size(312, 240);
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			this.btnOk.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnOk.DialogResult = global::System.Windows.Forms.DialogResult.Cancel;
			this.btnOk.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnOk.Location = new global::System.Drawing.Point(168, 258);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new global::System.Drawing.Size(75, 23);
			this.btnOk.TabIndex = 25;
			this.btnOk.Text = "&OK";
			this.btnOk.Click += new global::System.EventHandler(this.btnOk_Click);
			this.btnCancel.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnCancel.DialogResult = global::System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.Location = new global::System.Drawing.Point(249, 259);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new global::System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 26;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new global::System.EventHandler(this.btnCancel_Click);
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(336, 285);
			base.Controls.Add(this.btnCancel);
			base.Controls.Add(this.btnOk);
			base.Controls.Add(this.pictureBox1);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "SystemTrayNotify";
			base.StartPosition = global::System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Minimize NopTalk Pro to System Tray";
			base.Load += new global::System.EventHandler(this.SystemTrayNotify_Load);
			((global::System.ComponentModel.ISupportInitialize)this.pictureBox1).EndInit();
			base.ResumeLayout(false);
		}

		// Token: 0x040003DF RID: 991
		private global::System.ComponentModel.IContainer components;

		// Token: 0x040003E0 RID: 992
		private global::System.Windows.Forms.PictureBox pictureBox1;

		// Token: 0x040003E1 RID: 993
		private global::System.Windows.Forms.Button btnOk;

		// Token: 0x040003E2 RID: 994
		private global::System.Windows.Forms.Button btnCancel;
	}
}
