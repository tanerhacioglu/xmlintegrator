﻿namespace NopTalk
{
	// Token: 0x0200003E RID: 62
	public partial class vendorNewEdit : global::System.Windows.Forms.Form
	{
		// Token: 0x060002C0 RID: 704 RVA: 0x000038E4 File Offset: 0x00001AE4
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x060002C1 RID: 705 RVA: 0x0003D65C File Offset: 0x0003B85C
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.tabSourceConfig = new System.Windows.Forms.TabControl();
            this.tabVendor = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbId = new System.Windows.Forms.TextBox();
            this.tabVendorSource = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tbAuthHeader = new System.Windows.Forms.TextBox();
            this.tbSoapRequest = new System.Windows.Forms.TextBox();
            this.tbSoapAction = new System.Windows.Forms.TextBox();
            this.lblSoapAction = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ddSourceType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbSourceUserName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pSourceFormat = new System.Windows.Forms.Panel();
            this.tbCsvDelimeter = new System.Windows.Forms.TextBox();
            this.lblCsvDelimeter = new System.Windows.Forms.Label();
            this.lblFirstDataRowIndex = new System.Windows.Forms.Label();
            this.nmFirstRowDataIndex = new System.Windows.Forms.NumericUpDown();
            this.ddSourceFormat = new System.Windows.Forms.ComboBox();
            this.tbSourcePassword = new System.Windows.Forms.TextBox();
            this.lblSoapRequest = new System.Windows.Forms.Label();
            this.lblProductIdentifier = new System.Windows.Forms.Label();
            this.ddProductIdInStore = new System.Windows.Forms.ComboBox();
            this.label211 = new System.Windows.Forms.Label();
            this.ddEntityType = new System.Windows.Forms.ComboBox();
            this.ddUserIdInStore = new System.Windows.Forms.ComboBox();
            this.ddSourceEncoding = new System.Windows.Forms.ComboBox();
            this.lblUserIdentifier = new System.Windows.Forms.Label();
            this.label272 = new System.Windows.Forms.Label();
            this.btnTestSource = new System.Windows.Forms.Button();
            this.label285 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSourceFileOpen = new System.Windows.Forms.Button();
            this.btnSourceFileBrowse = new System.Windows.Forms.Button();
            this.tbSourceAddress = new System.Windows.Forms.TextBox();
            this.tabProductInfoMappingXml = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tbProductRepeatNodeXpath = new System.Windows.Forms.TextBox();
            this.tbProdNameXpath = new System.Windows.Forms.TextBox();
            this.tbProdShortDescXpath = new System.Windows.Forms.TextBox();
            this.tbProdFullDescXpath = new System.Windows.Forms.TextBox();
            this.tbProdManNumberXpath = new System.Windows.Forms.TextBox();
            this.tbDefaultProductName_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultProdShortDesc_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultProdFullDesc_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultManufacturerPnum_xml = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.tbProdStockQuantityXpath = new System.Windows.Forms.TextBox();
            this.nmDefaultStockQuantity_xml = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbProdIDXpath = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbProdSKUXpath = new System.Windows.Forms.TextBox();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.textBox_0 = new System.Windows.Forms.TextBox();
            this.tbWeightXPath = new System.Windows.Forms.TextBox();
            this.tbDefaultGtinXPath = new System.Windows.Forms.TextBox();
            this.tbDefaultWeight_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSKU_Xpath = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.tbLengthXpath = new System.Windows.Forms.TextBox();
            this.tbWidthXpath = new System.Windows.Forms.TextBox();
            this.tbHeightXpath = new System.Windows.Forms.TextBox();
            this.tbDefaultLength_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultWidth_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultHeight_xml = new System.Windows.Forms.TextBox();
            this.label270 = new System.Windows.Forms.Label();
            this.tbProdGroupByXpath = new System.Windows.Forms.TextBox();
            this.tabSeoMappingXml = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tbDefaultSearchEnginePage = new System.Windows.Forms.TextBox();
            this.tbSearchEnginePageXpath = new System.Windows.Forms.TextBox();
            this.tbDefaultMetaTitle = new System.Windows.Forms.TextBox();
            this.tbMetaTitleXpath = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tbDefaultMetaDescription = new System.Windows.Forms.TextBox();
            this.tbDefaultMetaKeywords = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.tbMetaKeywordsXpath = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.tbMetaDescriptionXpath = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.tabCatMappingXml = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.tbCategoryXPath = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tbDefaultCategory_xml = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.tbCategoryDelimeter_xml = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.tbCategoryXPath1 = new System.Windows.Forms.TextBox();
            this.tbCategoryXPath2 = new System.Windows.Forms.TextBox();
            this.tbCategoryXPath3 = new System.Windows.Forms.TextBox();
            this.tbCategoryXPath4 = new System.Windows.Forms.TextBox();
            this.tbDefaultCategory1_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultCategory2_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultCategory3_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultCategory4_xml = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.chCatId_xml = new System.Windows.Forms.CheckBox();
            this.chCat1Id_xml = new System.Windows.Forms.CheckBox();
            this.chCat2Id_xml = new System.Windows.Forms.CheckBox();
            this.chCat3Id_xml = new System.Windows.Forms.CheckBox();
            this.chCat4Id_xml = new System.Windows.Forms.CheckBox();
            this.tabAccessRolesMappingXml = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.label159 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.tbCustomerRoleXpath = new System.Windows.Forms.TextBox();
            this.label162 = new System.Windows.Forms.Label();
            this.tbDefaultCustomerRole_xml = new System.Windows.Forms.TextBox();
            this.label163 = new System.Windows.Forms.Label();
            this.tbCustomerRoleDelimeter_xml = new System.Windows.Forms.TextBox();
            this.label164 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.tbCustomerRoleXpath1 = new System.Windows.Forms.TextBox();
            this.tbCustomerRoleXpath2 = new System.Windows.Forms.TextBox();
            this.tbCustomerRoleXpath3 = new System.Windows.Forms.TextBox();
            this.tbCustomerRoleXpath4 = new System.Windows.Forms.TextBox();
            this.tbDefaultCustomerRole1_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultCustomerRole2_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultCustomerRole3_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultCustomerRole4_xml = new System.Windows.Forms.TextBox();
            this.label168 = new System.Windows.Forms.Label();
            this.chCustomerRoleID_xml = new System.Windows.Forms.CheckBox();
            this.chCustomerRoleID1_xml = new System.Windows.Forms.CheckBox();
            this.chCustomerRoleID2_xml = new System.Windows.Forms.CheckBox();
            this.chCustomerRoleID3_xml = new System.Windows.Forms.CheckBox();
            this.chCustomerRoleID4_xml = new System.Windows.Forms.CheckBox();
            this.tabManufMappingXml = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.tbDefaultManufacturer_xml = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.tbManufacturerXPath = new System.Windows.Forms.TextBox();
            this.tbVendorXpath = new System.Windows.Forms.TextBox();
            this.label143 = new System.Windows.Forms.Label();
            this.tbDefaultVendor_xml = new System.Windows.Forms.TextBox();
            this.chVendorId_xml = new System.Windows.Forms.CheckBox();
            this.chManufacturerId_xml = new System.Windows.Forms.CheckBox();
            this.tabFilterMappingXml = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.tbFilterXpath = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.tbFilterName_xml = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.tbFilterName1_xml = new System.Windows.Forms.TextBox();
            this.tbFilterName2_xml = new System.Windows.Forms.TextBox();
            this.tbFilterName3_xml = new System.Windows.Forms.TextBox();
            this.tbFilterName4_xml = new System.Windows.Forms.TextBox();
            this.tbFilterName5_xml = new System.Windows.Forms.TextBox();
            this.tbFilterName6_xml = new System.Windows.Forms.TextBox();
            this.tbFilterName7_xml = new System.Windows.Forms.TextBox();
            this.tbFilterName8_xml = new System.Windows.Forms.TextBox();
            this.tbFilterName9_xml = new System.Windows.Forms.TextBox();
            this.tbFilterXpath1 = new System.Windows.Forms.TextBox();
            this.tbFilterXpath2 = new System.Windows.Forms.TextBox();
            this.tbFilterXpath3 = new System.Windows.Forms.TextBox();
            this.tbFilterXpath4 = new System.Windows.Forms.TextBox();
            this.tbFilterXpath5 = new System.Windows.Forms.TextBox();
            this.tbFilterXpath6 = new System.Windows.Forms.TextBox();
            this.tbFilterXpath7 = new System.Windows.Forms.TextBox();
            this.tbFilterXpath8 = new System.Windows.Forms.TextBox();
            this.tbFilterXpath9 = new System.Windows.Forms.TextBox();
            this.tabProductInfoMappingPos = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label82 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.tbDefaultProdName_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultProdShortDesc_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultProdFullDesc_pos = new System.Windows.Forms.TextBox();
            this.tbProdManPartNum_pos = new System.Windows.Forms.TextBox();
            this.nmDefaultProdStock_pos = new System.Windows.Forms.TextBox();
            this.nmProdId_pos = new System.Windows.Forms.NumericUpDown();
            this.nmProdName_pos = new System.Windows.Forms.NumericUpDown();
            this.nmProdShortDesc_pos = new System.Windows.Forms.NumericUpDown();
            this.nmProdFullDesc_pos = new System.Windows.Forms.NumericUpDown();
            this.nmProdManPartNum_pos = new System.Windows.Forms.NumericUpDown();
            this.nmProdStock_pos = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.nmProdSKU_pos = new System.Windows.Forms.NumericUpDown();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.nmGtin_pos = new System.Windows.Forms.NumericUpDown();
            this.nmWeight_pos = new System.Windows.Forms.NumericUpDown();
            this.tbDefaultWeight_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultGtin_pos = new System.Windows.Forms.TextBox();
            this.nmDefaultProdSKU_pos = new System.Windows.Forms.TextBox();
            this.label179 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.label181 = new System.Windows.Forms.Label();
            this.nmLength_pos = new System.Windows.Forms.NumericUpDown();
            this.nmWidth_pos = new System.Windows.Forms.NumericUpDown();
            this.nmHeight_pos = new System.Windows.Forms.NumericUpDown();
            this.tbDefaultLength_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultWidth_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultHeight_pos = new System.Windows.Forms.TextBox();
            this.label271 = new System.Windows.Forms.Label();
            this.nmProdGroupBy_pos = new System.Windows.Forms.NumericUpDown();
            this.tabSeoMappingPos = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.tbDefaultSeoSearchPage_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultSeoMetaTitle_pos = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.tbDefaultSeoMetaDesc_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultSeoMetaKey_pos = new System.Windows.Forms.TextBox();
            this.label92 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.nmSeoMetaKey_pos = new System.Windows.Forms.NumericUpDown();
            this.nmSeoMetaDesc_pos = new System.Windows.Forms.NumericUpDown();
            this.nmSeoMetaTitle_pos = new System.Windows.Forms.NumericUpDown();
            this.nmSeoSearchPage_pos = new System.Windows.Forms.NumericUpDown();
            this.tabCatMappingPos = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.label93 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.tbDefaultCategory_pos = new System.Windows.Forms.TextBox();
            this.label102 = new System.Windows.Forms.Label();
            this.tbCategoryDelimeter_pos = new System.Windows.Forms.TextBox();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.tbDefaultCategory1_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultCategory2_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultCategory3_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultCategory4_pos = new System.Windows.Forms.TextBox();
            this.label99 = new System.Windows.Forms.Label();
            this.nmCategory_pos = new System.Windows.Forms.NumericUpDown();
            this.nmCategory1_pos = new System.Windows.Forms.NumericUpDown();
            this.nmCategory2_pos = new System.Windows.Forms.NumericUpDown();
            this.nmCategory3_pos = new System.Windows.Forms.NumericUpDown();
            this.nmCategory4_pos = new System.Windows.Forms.NumericUpDown();
            this.label144 = new System.Windows.Forms.Label();
            this.chCatId_pos = new System.Windows.Forms.CheckBox();
            this.chCat1Id_pos = new System.Windows.Forms.CheckBox();
            this.chCat2Id_pos = new System.Windows.Forms.CheckBox();
            this.chCat3Id_pos = new System.Windows.Forms.CheckBox();
            this.chCat4Id_pos = new System.Windows.Forms.CheckBox();
            this.tabAccessRolesMappingPos = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.label169 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this.tbDefaultCustomerRole_pos = new System.Windows.Forms.TextBox();
            this.label172 = new System.Windows.Forms.Label();
            this.tbCustomerRoleDelimiter_pos = new System.Windows.Forms.TextBox();
            this.label173 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this.label175 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this.tbDefaultCustomerRole1_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultCustomerRole2_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultCustomerRole3_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultCustomerRole4_pos = new System.Windows.Forms.TextBox();
            this.label177 = new System.Windows.Forms.Label();
            this.nmCustomerRole_pos = new System.Windows.Forms.NumericUpDown();
            this.nmCustomerRole1_pos = new System.Windows.Forms.NumericUpDown();
            this.nmCustomerRole2_pos = new System.Windows.Forms.NumericUpDown();
            this.nmCustomerRole3_pos = new System.Windows.Forms.NumericUpDown();
            this.nmCustomerRole4_pos = new System.Windows.Forms.NumericUpDown();
            this.label178 = new System.Windows.Forms.Label();
            this.chCustomerRoleId_pos = new System.Windows.Forms.CheckBox();
            this.chCustomerRoleId1_pos = new System.Windows.Forms.CheckBox();
            this.chCustomerRoleId2_pos = new System.Windows.Forms.CheckBox();
            this.chCustomerRoleId3_pos = new System.Windows.Forms.CheckBox();
            this.chCustomerRoleId4_pos = new System.Windows.Forms.CheckBox();
            this.tabManufMappingPos = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.nmVendor_pos = new System.Windows.Forms.NumericUpDown();
            this.label146 = new System.Windows.Forms.Label();
            this.tbDefaultManufacturer_pos = new System.Windows.Forms.TextBox();
            this.label107 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.nmManufacturer_pos = new System.Windows.Forms.NumericUpDown();
            this.label108 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.tbDefaultVendor_pos = new System.Windows.Forms.TextBox();
            this.chManufacturerId_pos = new System.Windows.Forms.CheckBox();
            this.chVendorId_pos = new System.Windows.Forms.CheckBox();
            this.tabFilterMappingPos = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.label129 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.tbFilterName_pos = new System.Windows.Forms.TextBox();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.tbFilter1Name_pos = new System.Windows.Forms.TextBox();
            this.tbFilter2Name_pos = new System.Windows.Forms.TextBox();
            this.tbFilter3Name_pos = new System.Windows.Forms.TextBox();
            this.tbFilter4Name_pos = new System.Windows.Forms.TextBox();
            this.tbFilter5Name_pos = new System.Windows.Forms.TextBox();
            this.tbFilter6Name_pos = new System.Windows.Forms.TextBox();
            this.tbFilter7Name_pos = new System.Windows.Forms.TextBox();
            this.tbFilter8Name_pos = new System.Windows.Forms.TextBox();
            this.tbFilter9Name_pos = new System.Windows.Forms.TextBox();
            this.nmFilterValue_pos = new System.Windows.Forms.NumericUpDown();
            this.nmFilter1Value_pos = new System.Windows.Forms.NumericUpDown();
            this.nmFilter2Value_pos = new System.Windows.Forms.NumericUpDown();
            this.nmFilter3Value_pos = new System.Windows.Forms.NumericUpDown();
            this.nmFilter4Value_pos = new System.Windows.Forms.NumericUpDown();
            this.nmFilter5Value_pos = new System.Windows.Forms.NumericUpDown();
            this.nmFilter6Value_pos = new System.Windows.Forms.NumericUpDown();
            this.nmFilter7Value_pos = new System.Windows.Forms.NumericUpDown();
            this.nmFilter8Value_pos = new System.Windows.Forms.NumericUpDown();
            this.nmFilter9Value_pos = new System.Windows.Forms.NumericUpDown();
            this.label132 = new System.Windows.Forms.Label();
            this.tabProductSettingsXml = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.tbDefaultProdSettingsDeliveryDate_xml = new System.Windows.Forms.TextBox();
            this.tbProdSettingsDeliveryDate_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSettingsShippingCharge_xml = new System.Windows.Forms.TextBox();
            this.tbProdSettingsShippingCharge_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSettingsShipSeparately_xml = new System.Windows.Forms.TextBox();
            this.tbProdSettingsShipSeparately_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSettingsFreeShipping_xml = new System.Windows.Forms.TextBox();
            this.tbProdSettingsFreeShipping_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSettingsShippingEnabled_xml = new System.Windows.Forms.TextBox();
            this.tbProdSettingsShippingEnabled_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSettingsMaxCartQty_xml = new System.Windows.Forms.TextBox();
            this.tbProdSettingsMaxCartQty_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSettingsMinCartQty_xml = new System.Windows.Forms.TextBox();
            this.tbProdSettingsMinCartQty_xml = new System.Windows.Forms.TextBox();
            this.label147 = new System.Windows.Forms.Label();
            this.tbDefaultProdSettingsNotifyQty_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSettingsMinStockQty_xml = new System.Windows.Forms.TextBox();
            this.label237 = new System.Windows.Forms.Label();
            this.label238 = new System.Windows.Forms.Label();
            this.label243 = new System.Windows.Forms.Label();
            this.label244 = new System.Windows.Forms.Label();
            this.tbProdSettingsMinStockQty_xml = new System.Windows.Forms.TextBox();
            this.label245 = new System.Windows.Forms.Label();
            this.tbProdSettingsNotifyQty_xml = new System.Windows.Forms.TextBox();
            this.label246 = new System.Windows.Forms.Label();
            this.label247 = new System.Windows.Forms.Label();
            this.tbProdSettingsAllowedQty_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSettingsAllowedQty_xml = new System.Windows.Forms.TextBox();
            this.label256 = new System.Windows.Forms.Label();
            this.label257 = new System.Windows.Forms.Label();
            this.label258 = new System.Windows.Forms.Label();
            this.label259 = new System.Windows.Forms.Label();
            this.label260 = new System.Windows.Forms.Label();
            this.label266 = new System.Windows.Forms.Label();
            this.chProdSettingsDeliveryDateIsID_xml = new System.Windows.Forms.CheckBox();
            this.label51 = new System.Windows.Forms.Label();
            this.tbProdSettingsVisibleIndvidually_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSettingsVisibleIndvidually_xml = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.tbProdSettingsIsDeleted_xml = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSettingsIsDeleted_xml = new System.Windows.Forms.TextBox();
            this.tabProductSettingsPos = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.tbDefaultProdSettingsDeliveryDate_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSettingsShippingCharge_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSettingsShipSeparately_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSettingsFreeShipping_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSettingsShippingEnabled_pos = new System.Windows.Forms.TextBox();
            this.nmProdSettingsShippingEnabled_pos = new System.Windows.Forms.NumericUpDown();
            this.tbDefaultProdSettingsMaxCartQty_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSettingsMinCartQty_pos = new System.Windows.Forms.TextBox();
            this.label248 = new System.Windows.Forms.Label();
            this.tbDefaultProdSettingsNotifyQty_pos = new System.Windows.Forms.TextBox();
            this.tbDefaultProdSettingsMinStockQty_pos = new System.Windows.Forms.TextBox();
            this.label249 = new System.Windows.Forms.Label();
            this.label250 = new System.Windows.Forms.Label();
            this.label251 = new System.Windows.Forms.Label();
            this.label252 = new System.Windows.Forms.Label();
            this.label253 = new System.Windows.Forms.Label();
            this.label254 = new System.Windows.Forms.Label();
            this.nmProdSettingsMinStockQty_pos = new System.Windows.Forms.NumericUpDown();
            this.nmProdSettingsNotifyQty_pos = new System.Windows.Forms.NumericUpDown();
            this.nmProdSettingsMinCartQty_pos = new System.Windows.Forms.NumericUpDown();
            this.nmProdSettingsMaxCartQty_pos = new System.Windows.Forms.NumericUpDown();
            this.label255 = new System.Windows.Forms.Label();
            this.nmProdSettingsAllowedQty_pos = new System.Windows.Forms.NumericUpDown();
            this.tbDefaultProdSettingsAllowedQty_pos = new System.Windows.Forms.TextBox();
            this.label261 = new System.Windows.Forms.Label();
            this.label262 = new System.Windows.Forms.Label();
            this.label263 = new System.Windows.Forms.Label();
            this.label264 = new System.Windows.Forms.Label();
            this.label265 = new System.Windows.Forms.Label();
            this.nmProdSettingsFreeShipping_pos = new System.Windows.Forms.NumericUpDown();
            this.nmProdSettingsShipSeparately_pos = new System.Windows.Forms.NumericUpDown();
            this.nmProdSettingsShippingCharge_pos = new System.Windows.Forms.NumericUpDown();
            this.nmProdSettingsDeliveryDate_pos = new System.Windows.Forms.NumericUpDown();
            this.label267 = new System.Windows.Forms.Label();
            this.chProdSettingsDeliveryDateIsID_pos = new System.Windows.Forms.CheckBox();
            this.label50 = new System.Windows.Forms.Label();
            this.nmProdSettingsVisibleIndvidually_pos = new System.Windows.Forms.NumericUpDown();
            this.tbDefaultProdSettingsVisibleIndvidually_pos = new System.Windows.Forms.TextBox();
            this.nmProdSettingsIsDeleted_pos = new System.Windows.Forms.NumericUpDown();
            this.label60 = new System.Windows.Forms.Label();
            this.tbDefaultProdSettingsIsDeleted_pos = new System.Windows.Forms.TextBox();
            this.tabAliExpress = new System.Windows.Forms.TabPage();
            this.lblAliExpressUrlLabel = new System.Windows.Forms.Label();
            this.tbAliexpressProductsUrls = new System.Windows.Forms.TextBox();
            this.statusArea = new System.Windows.Forms.StatusStrip();
            this.statusAreaText = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnTestMapping = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnQuickImport = new System.Windows.Forms.Button();
            this.btnCopy = new System.Windows.Forms.Button();
            this.cbSourceParser = new System.Windows.Forms.ComboBox();
            this.xmlModify = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.tabSourceConfig.SuspendLayout();
            this.tabVendor.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tabVendorSource.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.pSourceFormat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmFirstRowDataIndex)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabProductInfoMappingXml.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabSeoMappingXml.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tabCatMappingXml.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabAccessRolesMappingXml.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.tabManufMappingXml.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tabFilterMappingXml.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tabProductInfoMappingPos.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdId_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdName_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdShortDesc_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdFullDesc_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdManPartNum_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdStock_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSKU_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmGtin_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmWeight_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmLength_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmWidth_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmHeight_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdGroupBy_pos)).BeginInit();
            this.tabSeoMappingPos.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmSeoMetaKey_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmSeoMetaDesc_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmSeoMetaTitle_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmSeoSearchPage_pos)).BeginInit();
            this.tabCatMappingPos.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmCategory_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCategory1_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCategory2_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCategory3_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCategory4_pos)).BeginInit();
            this.tabAccessRolesMappingPos.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmCustomerRole_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCustomerRole1_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCustomerRole2_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCustomerRole3_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCustomerRole4_pos)).BeginInit();
            this.tabManufMappingPos.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmVendor_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmManufacturer_pos)).BeginInit();
            this.tabFilterMappingPos.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilterValue_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter1Value_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter2Value_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter3Value_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter4Value_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter5Value_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter6Value_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter7Value_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter8Value_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter9Value_pos)).BeginInit();
            this.tabProductSettingsXml.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            this.tabProductSettingsPos.SuspendLayout();
            this.tableLayoutPanel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsShippingEnabled_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsMinStockQty_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsNotifyQty_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsMinCartQty_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsMaxCartQty_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsAllowedQty_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsFreeShipping_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsShipSeparately_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsShippingCharge_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsDeliveryDate_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsVisibleIndvidually_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsIsDeleted_pos)).BeginInit();
            this.tabAliExpress.SuspendLayout();
            this.statusArea.SuspendLayout();
            this.SuspendLayout();
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // tabSourceConfig
            // 
            this.tabSourceConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabSourceConfig.Controls.Add(this.tabVendor);
            this.tabSourceConfig.Controls.Add(this.tabVendorSource);
            this.tabSourceConfig.Controls.Add(this.tabProductInfoMappingXml);
            this.tabSourceConfig.Controls.Add(this.tabSeoMappingXml);
            this.tabSourceConfig.Controls.Add(this.tabCatMappingXml);
            this.tabSourceConfig.Controls.Add(this.tabAccessRolesMappingXml);
            this.tabSourceConfig.Controls.Add(this.tabManufMappingXml);
            this.tabSourceConfig.Controls.Add(this.tabFilterMappingXml);
            this.tabSourceConfig.Controls.Add(this.tabProductInfoMappingPos);
            this.tabSourceConfig.Controls.Add(this.tabSeoMappingPos);
            this.tabSourceConfig.Controls.Add(this.tabCatMappingPos);
            this.tabSourceConfig.Controls.Add(this.tabAccessRolesMappingPos);
            this.tabSourceConfig.Controls.Add(this.tabManufMappingPos);
            this.tabSourceConfig.Controls.Add(this.tabFilterMappingPos);
            this.tabSourceConfig.Controls.Add(this.tabProductSettingsXml);
            this.tabSourceConfig.Controls.Add(this.tabProductSettingsPos);
            this.tabSourceConfig.Controls.Add(this.tabAliExpress);
            this.tabSourceConfig.Location = new System.Drawing.Point(0, 2);
            this.tabSourceConfig.Name = "tabSourceConfig";
            this.tabSourceConfig.SelectedIndex = 0;
            this.tabSourceConfig.Size = new System.Drawing.Size(932, 575);
            this.tabSourceConfig.TabIndex = 8;
            this.tabSourceConfig.SelectedIndexChanged += new System.EventHandler(this.tabSourceConfig_SelectedIndexChanged);
            // 
            // tabVendor
            // 
            this.tabVendor.Controls.Add(this.tableLayoutPanel4);
            this.tabVendor.Location = new System.Drawing.Point(4, 22);
            this.tabVendor.Name = "tabVendor";
            this.tabVendor.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabVendor.Size = new System.Drawing.Size(924, 549);
            this.tabVendor.TabIndex = 0;
            this.tabVendor.Text = "Vendor";
            this.tabVendor.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel4.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.tbDescription, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.tbName, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.tbId, 1, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(-4, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(924, 507);
            this.tableLayoutPanel4.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Name";
            // 
            // tbDescription
            // 
            this.tbDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDescription.Location = new System.Drawing.Point(281, 86);
            this.tbDescription.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDescription.Multiline = true;
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(622, 34);
            this.tbDescription.TabIndex = 15;
            // 
            // tbName
            // 
            this.tbName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbName.Location = new System.Drawing.Point(281, 45);
            this.tbName.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(622, 20);
            this.tbName.TabIndex = 13;
            this.tbName.Validating += new System.ComponentModel.CancelEventHandler(this.tbName_Validating);
            this.tbName.Validated += new System.EventHandler(this.tbName_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Description";
            // 
            // tbId
            // 
            this.tbId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbId.Location = new System.Drawing.Point(281, 4);
            this.tbId.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbId.Name = "tbId";
            this.tbId.ReadOnly = true;
            this.tbId.Size = new System.Drawing.Size(622, 20);
            this.tbId.TabIndex = 11;
            // 
            // tabVendorSource
            // 
            this.tabVendorSource.Controls.Add(this.tableLayoutPanel5);
            this.tabVendorSource.Location = new System.Drawing.Point(4, 22);
            this.tabVendorSource.Name = "tabVendorSource";
            this.tabVendorSource.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabVendorSource.Size = new System.Drawing.Size(924, 549);
            this.tabVendorSource.TabIndex = 1;
            this.tabVendorSource.Text = "Vendor Source";
            this.tabVendorSource.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel5.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel5.Controls.Add(this.tbAuthHeader, 1, 11);
            this.tableLayoutPanel5.Controls.Add(this.tbSoapRequest, 1, 6);
            this.tableLayoutPanel5.Controls.Add(this.tbSoapAction, 1, 5);
            this.tableLayoutPanel5.Controls.Add(this.lblSoapAction, 0, 5);
            this.tableLayoutPanel5.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.ddSourceType, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.tbSourceUserName, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.label8, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.label9, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.pSourceFormat, 1, 4);
            this.tableLayoutPanel5.Controls.Add(this.tbSourcePassword, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.lblSoapRequest, 0, 6);
            this.tableLayoutPanel5.Controls.Add(this.lblProductIdentifier, 0, 8);
            this.tableLayoutPanel5.Controls.Add(this.ddProductIdInStore, 1, 8);
            this.tableLayoutPanel5.Controls.Add(this.label211, 0, 7);
            this.tableLayoutPanel5.Controls.Add(this.ddEntityType, 1, 7);
            this.tableLayoutPanel5.Controls.Add(this.ddUserIdInStore, 1, 9);
            this.tableLayoutPanel5.Controls.Add(this.ddSourceEncoding, 1, 10);
            this.tableLayoutPanel5.Controls.Add(this.lblUserIdentifier, 0, 9);
            this.tableLayoutPanel5.Controls.Add(this.label272, 0, 10);
            this.tableLayoutPanel5.Controls.Add(this.btnTestSource, 1, 12);
            this.tableLayoutPanel5.Controls.Add(this.label285, 0, 11);
            this.tableLayoutPanel5.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.xmlModify, 1, 13);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(-4, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 14;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(924, 639);
            this.tableLayoutPanel5.TabIndex = 32;
            // 
            // tbAuthHeader
            // 
            this.tbAuthHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAuthHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbAuthHeader.Location = new System.Drawing.Point(281, 402);
            this.tbAuthHeader.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbAuthHeader.Name = "tbAuthHeader";
            this.tbAuthHeader.Size = new System.Drawing.Size(622, 20);
            this.tbAuthHeader.TabIndex = 38;
            // 
            // tbSoapRequest
            // 
            this.tbSoapRequest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSoapRequest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSoapRequest.Location = new System.Drawing.Point(281, 197);
            this.tbSoapRequest.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbSoapRequest.Multiline = true;
            this.tbSoapRequest.Name = "tbSoapRequest";
            this.tbSoapRequest.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbSoapRequest.Size = new System.Drawing.Size(622, 61);
            this.tbSoapRequest.TabIndex = 28;
            // 
            // tbSoapAction
            // 
            this.tbSoapAction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSoapAction.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSoapAction.Location = new System.Drawing.Point(281, 166);
            this.tbSoapAction.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbSoapAction.Name = "tbSoapAction";
            this.tbSoapAction.Size = new System.Drawing.Size(622, 20);
            this.tbSoapAction.TabIndex = 27;
            // 
            // lblSoapAction
            // 
            this.lblSoapAction.AutoSize = true;
            this.lblSoapAction.Location = new System.Drawing.Point(4, 163);
            this.lblSoapAction.Name = "lblSoapAction";
            this.lblSoapAction.Size = new System.Drawing.Size(69, 13);
            this.lblSoapAction.TabIndex = 25;
            this.lblSoapAction.Text = "SOAP Action";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Source Type";
            // 
            // ddSourceType
            // 
            this.ddSourceType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddSourceType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddSourceType.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddSourceType.FormattingEnabled = true;
            this.ddSourceType.Items.AddRange(new object[] {
            "File system",
            "Web url",
            "Ftp",
            "Web Service"});
            this.ddSourceType.Location = new System.Drawing.Point(281, 4);
            this.ddSourceType.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddSourceType.Name = "ddSourceType";
            this.ddSourceType.Size = new System.Drawing.Size(622, 21);
            this.ddSourceType.TabIndex = 13;
            this.ddSourceType.SelectedIndexChanged += new System.EventHandler(this.ddSourceType_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Source Address";
            // 
            // tbSourceUserName
            // 
            this.tbSourceUserName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSourceUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSourceUserName.Location = new System.Drawing.Point(281, 73);
            this.tbSourceUserName.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbSourceUserName.Name = "tbSourceUserName";
            this.tbSourceUserName.Size = new System.Drawing.Size(622, 20);
            this.tbSourceUserName.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Source Username";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 101);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Source Password";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 132);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Source Format";
            // 
            // pSourceFormat
            // 
            this.pSourceFormat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pSourceFormat.Controls.Add(this.tbCsvDelimeter);
            this.pSourceFormat.Controls.Add(this.lblCsvDelimeter);
            this.pSourceFormat.Controls.Add(this.lblFirstDataRowIndex);
            this.pSourceFormat.Controls.Add(this.nmFirstRowDataIndex);
            this.pSourceFormat.Controls.Add(this.ddSourceFormat);
            this.pSourceFormat.Location = new System.Drawing.Point(281, 135);
            this.pSourceFormat.Name = "pSourceFormat";
            this.pSourceFormat.Size = new System.Drawing.Size(639, 24);
            this.pSourceFormat.TabIndex = 24;
            // 
            // tbCsvDelimeter
            // 
            this.tbCsvDelimeter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCsvDelimeter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCsvDelimeter.Location = new System.Drawing.Point(520, 1);
            this.tbCsvDelimeter.Name = "tbCsvDelimeter";
            this.tbCsvDelimeter.Size = new System.Drawing.Size(100, 20);
            this.tbCsvDelimeter.TabIndex = 30;
            // 
            // lblCsvDelimeter
            // 
            this.lblCsvDelimeter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCsvDelimeter.AutoSize = true;
            this.lblCsvDelimeter.Location = new System.Drawing.Point(433, 5);
            this.lblCsvDelimeter.Name = "lblCsvDelimeter";
            this.lblCsvDelimeter.Size = new System.Drawing.Size(71, 13);
            this.lblCsvDelimeter.TabIndex = 29;
            this.lblCsvDelimeter.Text = "CSV Delimiter";
            // 
            // lblFirstDataRowIndex
            // 
            this.lblFirstDataRowIndex.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFirstDataRowIndex.AutoSize = true;
            this.lblFirstDataRowIndex.Location = new System.Drawing.Point(177, 6);
            this.lblFirstDataRowIndex.Name = "lblFirstDataRowIndex";
            this.lblFirstDataRowIndex.Size = new System.Drawing.Size(106, 13);
            this.lblFirstDataRowIndex.TabIndex = 28;
            this.lblFirstDataRowIndex.Text = "First Data Row Index";
            // 
            // nmFirstRowDataIndex
            // 
            this.nmFirstRowDataIndex.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nmFirstRowDataIndex.Location = new System.Drawing.Point(289, 2);
            this.nmFirstRowDataIndex.Name = "nmFirstRowDataIndex";
            this.nmFirstRowDataIndex.Size = new System.Drawing.Size(120, 20);
            this.nmFirstRowDataIndex.TabIndex = 27;
            // 
            // ddSourceFormat
            // 
            this.ddSourceFormat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddSourceFormat.AutoCompleteCustomSource.AddRange(new string[] {
            "XML",
            "CSV ",
            "XLSX"});
            this.ddSourceFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddSourceFormat.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddSourceFormat.FormattingEnabled = true;
            this.ddSourceFormat.Items.AddRange(new object[] {
            "XML",
            "CSV",
            "XLSX"});
            this.ddSourceFormat.Location = new System.Drawing.Point(3, 1);
            this.ddSourceFormat.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.ddSourceFormat.Name = "ddSourceFormat";
            this.ddSourceFormat.Size = new System.Drawing.Size(139, 21);
            this.ddSourceFormat.TabIndex = 21;
            this.ddSourceFormat.SelectedIndexChanged += new System.EventHandler(this.ddSourceFormat_SelectedIndexChanged);
            // 
            // tbSourcePassword
            // 
            this.tbSourcePassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSourcePassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSourcePassword.Location = new System.Drawing.Point(281, 104);
            this.tbSourcePassword.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbSourcePassword.Name = "tbSourcePassword";
            this.tbSourcePassword.Size = new System.Drawing.Size(622, 20);
            this.tbSourcePassword.TabIndex = 19;
            // 
            // lblSoapRequest
            // 
            this.lblSoapRequest.AutoSize = true;
            this.lblSoapRequest.Location = new System.Drawing.Point(4, 194);
            this.lblSoapRequest.Name = "lblSoapRequest";
            this.lblSoapRequest.Size = new System.Drawing.Size(79, 13);
            this.lblSoapRequest.TabIndex = 26;
            this.lblSoapRequest.Text = "SOAP Request";
            // 
            // lblProductIdentifier
            // 
            this.lblProductIdentifier.AutoSize = true;
            this.lblProductIdentifier.Location = new System.Drawing.Point(4, 306);
            this.lblProductIdentifier.Name = "lblProductIdentifier";
            this.lblProductIdentifier.Size = new System.Drawing.Size(127, 13);
            this.lblProductIdentifier.TabIndex = 29;
            this.lblProductIdentifier.Text = "Product Identifier In Store";
            // 
            // ddProductIdInStore
            // 
            this.ddProductIdInStore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddProductIdInStore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ddProductIdInStore.FormattingEnabled = true;
            this.ddProductIdInStore.Items.AddRange(new object[] {
            "Product SKU",
            "Product SKU&Vendor",
            "Product Id"});
            this.ddProductIdInStore.Location = new System.Drawing.Point(281, 309);
            this.ddProductIdInStore.Name = "ddProductIdInStore";
            this.ddProductIdInStore.Size = new System.Drawing.Size(293, 21);
            this.ddProductIdInStore.TabIndex = 30;
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Location = new System.Drawing.Point(4, 275);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(60, 13);
            this.label211.TabIndex = 31;
            this.label211.Text = "Entity Type";
            // 
            // ddEntityType
            // 
            this.ddEntityType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddEntityType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ddEntityType.FormattingEnabled = true;
            this.ddEntityType.Items.AddRange(new object[] {
            "Products",
            "Users",
            "WishList"});
            this.ddEntityType.Location = new System.Drawing.Point(281, 278);
            this.ddEntityType.Name = "ddEntityType";
            this.ddEntityType.Size = new System.Drawing.Size(293, 21);
            this.ddEntityType.TabIndex = 32;
            this.ddEntityType.SelectedIndexChanged += new System.EventHandler(this.ddEntityType_SelectedIndexChanged);
            // 
            // ddUserIdInStore
            // 
            this.ddUserIdInStore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddUserIdInStore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ddUserIdInStore.FormattingEnabled = true;
            this.ddUserIdInStore.Items.AddRange(new object[] {
            "UserEmail",
            "UserName",
            "UserEmail&UserName",
            "Custom:SAPCustomer"});
            this.ddUserIdInStore.Location = new System.Drawing.Point(281, 340);
            this.ddUserIdInStore.Name = "ddUserIdInStore";
            this.ddUserIdInStore.Size = new System.Drawing.Size(293, 21);
            this.ddUserIdInStore.TabIndex = 36;
            // 
            // ddSourceEncoding
            // 
            this.ddSourceEncoding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddSourceEncoding.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ddSourceEncoding.FormattingEnabled = true;
            this.ddSourceEncoding.Items.AddRange(new object[] {
            "Product SKU",
            "Product SKU&Vendor",
            "Product Id"});
            this.ddSourceEncoding.Location = new System.Drawing.Point(281, 371);
            this.ddSourceEncoding.Name = "ddSourceEncoding";
            this.ddSourceEncoding.Size = new System.Drawing.Size(293, 21);
            this.ddSourceEncoding.TabIndex = 34;
            // 
            // lblUserIdentifier
            // 
            this.lblUserIdentifier.AutoSize = true;
            this.lblUserIdentifier.Location = new System.Drawing.Point(4, 337);
            this.lblUserIdentifier.Name = "lblUserIdentifier";
            this.lblUserIdentifier.Size = new System.Drawing.Size(112, 13);
            this.lblUserIdentifier.TabIndex = 35;
            this.lblUserIdentifier.Text = "User Identifier In Store";
            // 
            // label272
            // 
            this.label272.AutoSize = true;
            this.label272.Location = new System.Drawing.Point(3, 368);
            this.label272.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label272.Name = "label272";
            this.label272.Size = new System.Drawing.Size(89, 13);
            this.label272.TabIndex = 33;
            this.label272.Text = "Source Encoding";
            // 
            // btnTestSource
            // 
            this.btnTestSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTestSource.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTestSource.Location = new System.Drawing.Point(763, 434);
            this.btnTestSource.Name = "btnTestSource";
            this.btnTestSource.Size = new System.Drawing.Size(157, 23);
            this.btnTestSource.TabIndex = 22;
            this.btnTestSource.Text = "Test Source";
            this.btnTestSource.UseVisualStyleBackColor = true;
            this.btnTestSource.Click += new System.EventHandler(this.btnTestSource_Click);
            // 
            // label285
            // 
            this.label285.AutoSize = true;
            this.label285.Location = new System.Drawing.Point(3, 399);
            this.label285.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label285.Name = "label285";
            this.label285.Size = new System.Drawing.Size(106, 13);
            this.label285.TabIndex = 37;
            this.label285.Text = "Authorization Header";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btnSourceFileOpen);
            this.panel1.Controls.Add(this.btnSourceFileBrowse);
            this.panel1.Controls.Add(this.tbSourceAddress);
            this.panel1.Location = new System.Drawing.Point(280, 34);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(641, 32);
            this.panel1.TabIndex = 39;
            // 
            // btnSourceFileOpen
            // 
            this.btnSourceFileOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSourceFileOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSourceFileOpen.Location = new System.Drawing.Point(562, 2);
            this.btnSourceFileOpen.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSourceFileOpen.Name = "btnSourceFileOpen";
            this.btnSourceFileOpen.Size = new System.Drawing.Size(62, 27);
            this.btnSourceFileOpen.TabIndex = 20;
            this.btnSourceFileOpen.Text = "Open";
            this.btnSourceFileOpen.UseVisualStyleBackColor = true;
            this.btnSourceFileOpen.Click += new System.EventHandler(this.btnSourceFileOpen_Click);
            // 
            // btnSourceFileBrowse
            // 
            this.btnSourceFileBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSourceFileBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSourceFileBrowse.Location = new System.Drawing.Point(501, 2);
            this.btnSourceFileBrowse.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSourceFileBrowse.Name = "btnSourceFileBrowse";
            this.btnSourceFileBrowse.Size = new System.Drawing.Size(56, 27);
            this.btnSourceFileBrowse.TabIndex = 19;
            this.btnSourceFileBrowse.Text = "Browse";
            this.btnSourceFileBrowse.UseVisualStyleBackColor = true;
            this.btnSourceFileBrowse.Click += new System.EventHandler(this.btnSourceFileBrowse_Click);
            // 
            // tbSourceAddress
            // 
            this.tbSourceAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSourceAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSourceAddress.Location = new System.Drawing.Point(0, 7);
            this.tbSourceAddress.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbSourceAddress.Name = "tbSourceAddress";
            this.tbSourceAddress.Size = new System.Drawing.Size(494, 20);
            this.tbSourceAddress.TabIndex = 18;
            // 
            // tabProductInfoMappingXml
            // 
            this.tabProductInfoMappingXml.Controls.Add(this.tableLayoutPanel1);
            this.tabProductInfoMappingXml.Location = new System.Drawing.Point(4, 22);
            this.tabProductInfoMappingXml.Name = "tabProductInfoMappingXml";
            this.tabProductInfoMappingXml.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabProductInfoMappingXml.Size = new System.Drawing.Size(924, 549);
            this.tabProductInfoMappingXml.TabIndex = 2;
            this.tabProductInfoMappingXml.Text = "Product Info";
            this.tabProductInfoMappingXml.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.46602F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.53398F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 410F));
            this.tableLayoutPanel1.Controls.Add(this.label24, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label22, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label20, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label18, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label17, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label21, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbProductRepeatNodeXpath, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbProdNameXpath, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.tbProdShortDescXpath, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.tbProdFullDescXpath, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.tbProdManNumberXpath, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.tbDefaultProductName_xml, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.tbDefaultProdShortDesc_xml, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.tbDefaultProdFullDesc_xml, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.tbDefaultManufacturerPnum_xml, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.label26, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label25, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.tbProdStockQuantityXpath, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.nmDefaultStockQuantity_xml, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tbProdIDXpath, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tbProdSKUXpath, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label154, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.label155, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.textBox_0, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.tbWeightXPath, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.tbDefaultGtinXPath, 2, 10);
            this.tableLayoutPanel1.Controls.Add(this.tbDefaultWeight_xml, 2, 11);
            this.tableLayoutPanel1.Controls.Add(this.tbDefaultProdSKU_Xpath, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.label158, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.tbLengthXpath, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.tbWidthXpath, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.tbHeightXpath, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.tbDefaultLength_xml, 2, 12);
            this.tableLayoutPanel1.Controls.Add(this.tbDefaultWidth_xml, 2, 13);
            this.tableLayoutPanel1.Controls.Add(this.tbDefaultHeight_xml, 2, 14);
            this.tableLayoutPanel1.Controls.Add(this.label270, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tbProdGroupByXpath, 1, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(-4, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 15;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(923, 512);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(4, 244);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(132, 13);
            this.label24.TabIndex = 17;
            this.label24.Text = "Manufacturer Part Number";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(4, 32);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(111, 13);
            this.label22.TabIndex = 13;
            this.label22.Text = "Product Repeat Node";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(4, 1);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(89, 17);
            this.label20.TabIndex = 11;
            this.label20.Text = "Field Name";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 194);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(79, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "Full Description";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(4, 167);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Short Description";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 140);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Product Name";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(180, 1);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(135, 17);
            this.label21.TabIndex = 12;
            this.label21.Text = "XPath Expression";
            // 
            // tbProductRepeatNodeXpath
            // 
            this.tbProductRepeatNodeXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductRepeatNodeXpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProductRepeatNodeXpath.Location = new System.Drawing.Point(180, 35);
            this.tbProductRepeatNodeXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProductRepeatNodeXpath.Name = "tbProductRepeatNodeXpath";
            this.tbProductRepeatNodeXpath.Size = new System.Drawing.Size(310, 20);
            this.tbProductRepeatNodeXpath.TabIndex = 14;
            this.tbProductRepeatNodeXpath.Validating += new System.ComponentModel.CancelEventHandler(this.tbProductRepeatNodeXpath_Validating);
            this.tbProductRepeatNodeXpath.Validated += new System.EventHandler(this.tbProductRepeatNodeXpath_Validated);
            // 
            // tbProdNameXpath
            // 
            this.tbProdNameXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdNameXpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdNameXpath.Location = new System.Drawing.Point(180, 143);
            this.tbProdNameXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdNameXpath.Name = "tbProdNameXpath";
            this.tbProdNameXpath.Size = new System.Drawing.Size(310, 20);
            this.tbProdNameXpath.TabIndex = 3;
            // 
            // tbProdShortDescXpath
            // 
            this.tbProdShortDescXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdShortDescXpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdShortDescXpath.Location = new System.Drawing.Point(180, 170);
            this.tbProdShortDescXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdShortDescXpath.Name = "tbProdShortDescXpath";
            this.tbProdShortDescXpath.Size = new System.Drawing.Size(310, 20);
            this.tbProdShortDescXpath.TabIndex = 5;
            // 
            // tbProdFullDescXpath
            // 
            this.tbProdFullDescXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdFullDescXpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdFullDescXpath.Location = new System.Drawing.Point(180, 197);
            this.tbProdFullDescXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdFullDescXpath.Name = "tbProdFullDescXpath";
            this.tbProdFullDescXpath.Size = new System.Drawing.Size(310, 20);
            this.tbProdFullDescXpath.TabIndex = 7;
            // 
            // tbProdManNumberXpath
            // 
            this.tbProdManNumberXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdManNumberXpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdManNumberXpath.Location = new System.Drawing.Point(180, 247);
            this.tbProdManNumberXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdManNumberXpath.Name = "tbProdManNumberXpath";
            this.tbProdManNumberXpath.Size = new System.Drawing.Size(310, 20);
            this.tbProdManNumberXpath.TabIndex = 18;
            // 
            // tbDefaultProductName_xml
            // 
            this.tbDefaultProductName_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProductName_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProductName_xml.Location = new System.Drawing.Point(514, 143);
            this.tbDefaultProductName_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProductName_xml.Name = "tbDefaultProductName_xml";
            this.tbDefaultProductName_xml.Size = new System.Drawing.Size(388, 20);
            this.tbDefaultProductName_xml.TabIndex = 22;
            // 
            // tbDefaultProdShortDesc_xml
            // 
            this.tbDefaultProdShortDesc_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdShortDesc_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdShortDesc_xml.Location = new System.Drawing.Point(514, 170);
            this.tbDefaultProdShortDesc_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdShortDesc_xml.Multiline = true;
            this.tbDefaultProdShortDesc_xml.Name = "tbDefaultProdShortDesc_xml";
            this.tbDefaultProdShortDesc_xml.Size = new System.Drawing.Size(388, 20);
            this.tbDefaultProdShortDesc_xml.TabIndex = 23;
            // 
            // tbDefaultProdFullDesc_xml
            // 
            this.tbDefaultProdFullDesc_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdFullDesc_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdFullDesc_xml.Location = new System.Drawing.Point(514, 197);
            this.tbDefaultProdFullDesc_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdFullDesc_xml.Multiline = true;
            this.tbDefaultProdFullDesc_xml.Name = "tbDefaultProdFullDesc_xml";
            this.tbDefaultProdFullDesc_xml.Size = new System.Drawing.Size(388, 20);
            this.tbDefaultProdFullDesc_xml.TabIndex = 24;
            // 
            // tbDefaultManufacturerPnum_xml
            // 
            this.tbDefaultManufacturerPnum_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultManufacturerPnum_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultManufacturerPnum_xml.Location = new System.Drawing.Point(514, 247);
            this.tbDefaultManufacturerPnum_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultManufacturerPnum_xml.Name = "tbDefaultManufacturerPnum_xml";
            this.tbDefaultManufacturerPnum_xml.Size = new System.Drawing.Size(388, 20);
            this.tbDefaultManufacturerPnum_xml.TabIndex = 25;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(514, 1);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(199, 17);
            this.label26.TabIndex = 21;
            this.label26.Text = "Default Value /+Add Value";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(4, 271);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(77, 13);
            this.label25.TabIndex = 19;
            this.label25.Text = "Stock Quantity";
            // 
            // tbProdStockQuantityXpath
            // 
            this.tbProdStockQuantityXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdStockQuantityXpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdStockQuantityXpath.Location = new System.Drawing.Point(180, 274);
            this.tbProdStockQuantityXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdStockQuantityXpath.Name = "tbProdStockQuantityXpath";
            this.tbProdStockQuantityXpath.Size = new System.Drawing.Size(310, 20);
            this.tbProdStockQuantityXpath.TabIndex = 20;
            // 
            // nmDefaultStockQuantity_xml
            // 
            this.nmDefaultStockQuantity_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nmDefaultStockQuantity_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmDefaultStockQuantity_xml.Location = new System.Drawing.Point(514, 274);
            this.nmDefaultStockQuantity_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.nmDefaultStockQuantity_xml.Name = "nmDefaultStockQuantity_xml";
            this.nmDefaultStockQuantity_xml.Size = new System.Drawing.Size(388, 20);
            this.nmDefaultStockQuantity_xml.TabIndex = 34;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 59);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Product ID";
            // 
            // tbProdIDXpath
            // 
            this.tbProdIDXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdIDXpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdIDXpath.Location = new System.Drawing.Point(180, 62);
            this.tbProdIDXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdIDXpath.Name = "tbProdIDXpath";
            this.tbProdIDXpath.Size = new System.Drawing.Size(310, 20);
            this.tbProdIDXpath.TabIndex = 1;
            this.tbProdIDXpath.Validating += new System.ComponentModel.CancelEventHandler(this.tbProdIDXpath_Validating);
            this.tbProdIDXpath.Validated += new System.EventHandler(this.tbProdIDXpath_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(167, 26);
            this.label5.TabIndex = 35;
            this.label5.Text = "Product SKU (product identifier in store) If not found will be generated \"NT\" + P" +
    "roduct ID";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // tbProdSKUXpath
            // 
            this.tbProdSKUXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdSKUXpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdSKUXpath.Location = new System.Drawing.Point(180, 116);
            this.tbProdSKUXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdSKUXpath.Name = "tbProdSKUXpath";
            this.tbProdSKUXpath.Size = new System.Drawing.Size(310, 20);
            this.tbProdSKUXpath.TabIndex = 36;
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(4, 298);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(33, 13);
            this.label154.TabIndex = 41;
            this.label154.Text = "GTIN";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(4, 325);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(41, 13);
            this.label155.TabIndex = 42;
            this.label155.Text = "Weight";
            // 
            // textBox_0
            // 
            this.textBox_0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_0.Location = new System.Drawing.Point(180, 301);
            this.textBox_0.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.textBox_0.Name = "textBox_0";
            this.textBox_0.Size = new System.Drawing.Size(310, 20);
            this.textBox_0.TabIndex = 43;
            // 
            // tbWeightXPath
            // 
            this.tbWeightXPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbWeightXPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbWeightXPath.Location = new System.Drawing.Point(180, 328);
            this.tbWeightXPath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbWeightXPath.Name = "tbWeightXPath";
            this.tbWeightXPath.Size = new System.Drawing.Size(310, 20);
            this.tbWeightXPath.TabIndex = 44;
            // 
            // tbDefaultGtinXPath
            // 
            this.tbDefaultGtinXPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultGtinXPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultGtinXPath.Location = new System.Drawing.Point(514, 301);
            this.tbDefaultGtinXPath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultGtinXPath.Name = "tbDefaultGtinXPath";
            this.tbDefaultGtinXPath.Size = new System.Drawing.Size(388, 20);
            this.tbDefaultGtinXPath.TabIndex = 45;
            // 
            // tbDefaultWeight_xml
            // 
            this.tbDefaultWeight_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultWeight_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultWeight_xml.Location = new System.Drawing.Point(514, 328);
            this.tbDefaultWeight_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultWeight_xml.Name = "tbDefaultWeight_xml";
            this.tbDefaultWeight_xml.Size = new System.Drawing.Size(388, 20);
            this.tbDefaultWeight_xml.TabIndex = 46;
            // 
            // tbDefaultProdSKU_Xpath
            // 
            this.tbDefaultProdSKU_Xpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSKU_Xpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSKU_Xpath.Location = new System.Drawing.Point(514, 116);
            this.tbDefaultProdSKU_Xpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSKU_Xpath.Name = "tbDefaultProdSKU_Xpath";
            this.tbDefaultProdSKU_Xpath.Size = new System.Drawing.Size(388, 20);
            this.tbDefaultProdSKU_Xpath.TabIndex = 47;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 352);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 13);
            this.label10.TabIndex = 48;
            this.label10.Text = "Length";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 379);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 13);
            this.label12.TabIndex = 49;
            this.label12.Text = "Width";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(3, 406);
            this.label158.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(38, 13);
            this.label158.TabIndex = 50;
            this.label158.Text = "Height";
            // 
            // tbLengthXpath
            // 
            this.tbLengthXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLengthXpath.Location = new System.Drawing.Point(180, 355);
            this.tbLengthXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbLengthXpath.Name = "tbLengthXpath";
            this.tbLengthXpath.Size = new System.Drawing.Size(310, 20);
            this.tbLengthXpath.TabIndex = 51;
            // 
            // tbWidthXpath
            // 
            this.tbWidthXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbWidthXpath.Location = new System.Drawing.Point(180, 382);
            this.tbWidthXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbWidthXpath.Name = "tbWidthXpath";
            this.tbWidthXpath.Size = new System.Drawing.Size(310, 20);
            this.tbWidthXpath.TabIndex = 52;
            // 
            // tbHeightXpath
            // 
            this.tbHeightXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbHeightXpath.Location = new System.Drawing.Point(180, 409);
            this.tbHeightXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbHeightXpath.Name = "tbHeightXpath";
            this.tbHeightXpath.Size = new System.Drawing.Size(310, 20);
            this.tbHeightXpath.TabIndex = 53;
            // 
            // tbDefaultLength_xml
            // 
            this.tbDefaultLength_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultLength_xml.Location = new System.Drawing.Point(514, 355);
            this.tbDefaultLength_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultLength_xml.Name = "tbDefaultLength_xml";
            this.tbDefaultLength_xml.Size = new System.Drawing.Size(388, 20);
            this.tbDefaultLength_xml.TabIndex = 54;
            // 
            // tbDefaultWidth_xml
            // 
            this.tbDefaultWidth_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultWidth_xml.Location = new System.Drawing.Point(514, 382);
            this.tbDefaultWidth_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultWidth_xml.Name = "tbDefaultWidth_xml";
            this.tbDefaultWidth_xml.Size = new System.Drawing.Size(388, 20);
            this.tbDefaultWidth_xml.TabIndex = 55;
            // 
            // tbDefaultHeight_xml
            // 
            this.tbDefaultHeight_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultHeight_xml.Location = new System.Drawing.Point(514, 409);
            this.tbDefaultHeight_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultHeight_xml.Name = "tbDefaultHeight_xml";
            this.tbDefaultHeight_xml.Size = new System.Drawing.Size(388, 20);
            this.tbDefaultHeight_xml.TabIndex = 56;
            // 
            // label270
            // 
            this.label270.AutoSize = true;
            this.label270.Location = new System.Drawing.Point(3, 86);
            this.label270.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label270.Name = "label270";
            this.label270.Size = new System.Drawing.Size(91, 13);
            this.label270.TabIndex = 60;
            this.label270.Text = "Product Group By";
            // 
            // tbProdGroupByXpath
            // 
            this.tbProdGroupByXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdGroupByXpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdGroupByXpath.Location = new System.Drawing.Point(180, 89);
            this.tbProdGroupByXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdGroupByXpath.Name = "tbProdGroupByXpath";
            this.tbProdGroupByXpath.Size = new System.Drawing.Size(310, 20);
            this.tbProdGroupByXpath.TabIndex = 61;
            // 
            // tabSeoMappingXml
            // 
            this.tabSeoMappingXml.Controls.Add(this.tableLayoutPanel2);
            this.tabSeoMappingXml.Location = new System.Drawing.Point(4, 22);
            this.tabSeoMappingXml.Name = "tabSeoMappingXml";
            this.tabSeoMappingXml.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabSeoMappingXml.Size = new System.Drawing.Size(924, 549);
            this.tabSeoMappingXml.TabIndex = 3;
            this.tabSeoMappingXml.Text = "SEO";
            this.tabSeoMappingXml.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.50478F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.49522F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 409F));
            this.tableLayoutPanel2.Controls.Add(this.tbDefaultSearchEnginePage, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.tbSearchEnginePageXpath, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.tbDefaultMetaTitle, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.tbMetaTitleXpath, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.label32, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.tbDefaultMetaDescription, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.tbDefaultMetaKeywords, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label27, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label28, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label29, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label30, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tbMetaKeywordsXpath, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label31, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tbMetaDescriptionXpath, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label33, 0, 4);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(-4, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(921, 632);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tbDefaultSearchEnginePage
            // 
            this.tbDefaultSearchEnginePage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultSearchEnginePage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultSearchEnginePage.Location = new System.Drawing.Point(513, 138);
            this.tbDefaultSearchEnginePage.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultSearchEnginePage.Name = "tbDefaultSearchEnginePage";
            this.tbDefaultSearchEnginePage.Size = new System.Drawing.Size(387, 20);
            this.tbDefaultSearchEnginePage.TabIndex = 34;
            // 
            // tbSearchEnginePageXpath
            // 
            this.tbSearchEnginePageXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearchEnginePageXpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSearchEnginePageXpath.Location = new System.Drawing.Point(170, 138);
            this.tbSearchEnginePageXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbSearchEnginePageXpath.Name = "tbSearchEnginePageXpath";
            this.tbSearchEnginePageXpath.Size = new System.Drawing.Size(319, 20);
            this.tbSearchEnginePageXpath.TabIndex = 33;
            // 
            // tbDefaultMetaTitle
            // 
            this.tbDefaultMetaTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultMetaTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultMetaTitle.Location = new System.Drawing.Point(513, 107);
            this.tbDefaultMetaTitle.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultMetaTitle.Name = "tbDefaultMetaTitle";
            this.tbDefaultMetaTitle.Size = new System.Drawing.Size(387, 20);
            this.tbDefaultMetaTitle.TabIndex = 32;
            // 
            // tbMetaTitleXpath
            // 
            this.tbMetaTitleXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMetaTitleXpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMetaTitleXpath.Location = new System.Drawing.Point(170, 107);
            this.tbMetaTitleXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbMetaTitleXpath.Name = "tbMetaTitleXpath";
            this.tbMetaTitleXpath.Size = new System.Drawing.Size(319, 20);
            this.tbMetaTitleXpath.TabIndex = 31;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(4, 104);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(54, 13);
            this.label32.TabIndex = 29;
            this.label32.Text = "Meta Title";
            // 
            // tbDefaultMetaDescription
            // 
            this.tbDefaultMetaDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultMetaDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultMetaDescription.Location = new System.Drawing.Point(513, 76);
            this.tbDefaultMetaDescription.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultMetaDescription.Name = "tbDefaultMetaDescription";
            this.tbDefaultMetaDescription.Size = new System.Drawing.Size(387, 20);
            this.tbDefaultMetaDescription.TabIndex = 28;
            // 
            // tbDefaultMetaKeywords
            // 
            this.tbDefaultMetaKeywords.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultMetaKeywords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultMetaKeywords.Location = new System.Drawing.Point(513, 45);
            this.tbDefaultMetaKeywords.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultMetaKeywords.Name = "tbDefaultMetaKeywords";
            this.tbDefaultMetaKeywords.Size = new System.Drawing.Size(387, 20);
            this.tbDefaultMetaKeywords.TabIndex = 27;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(4, 1);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(89, 17);
            this.label27.TabIndex = 12;
            this.label27.Text = "Field Name";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(170, 1);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(135, 17);
            this.label28.TabIndex = 13;
            this.label28.Text = "XPath Expression";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(513, 1);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(199, 17);
            this.label29.TabIndex = 22;
            this.label29.Text = "Default Value /+Add Value";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(4, 42);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(80, 13);
            this.label30.TabIndex = 23;
            this.label30.Text = "Meta Keywords";
            // 
            // tbMetaKeywordsXpath
            // 
            this.tbMetaKeywordsXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMetaKeywordsXpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMetaKeywordsXpath.Location = new System.Drawing.Point(170, 45);
            this.tbMetaKeywordsXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbMetaKeywordsXpath.Name = "tbMetaKeywordsXpath";
            this.tbMetaKeywordsXpath.Size = new System.Drawing.Size(319, 20);
            this.tbMetaKeywordsXpath.TabIndex = 24;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(4, 73);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(87, 13);
            this.label31.TabIndex = 25;
            this.label31.Text = "Meta Description";
            // 
            // tbMetaDescriptionXpath
            // 
            this.tbMetaDescriptionXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMetaDescriptionXpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMetaDescriptionXpath.Location = new System.Drawing.Point(170, 76);
            this.tbMetaDescriptionXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbMetaDescriptionXpath.Name = "tbMetaDescriptionXpath";
            this.tbMetaDescriptionXpath.Size = new System.Drawing.Size(319, 20);
            this.tbMetaDescriptionXpath.TabIndex = 26;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(4, 135);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(147, 26);
            this.label33.TabIndex = 30;
            this.label33.Text = "Search Engine Friendly Page Name";
            // 
            // tabCatMappingXml
            // 
            this.tabCatMappingXml.Controls.Add(this.tableLayoutPanel3);
            this.tabCatMappingXml.Location = new System.Drawing.Point(4, 22);
            this.tabCatMappingXml.Name = "tabCatMappingXml";
            this.tabCatMappingXml.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabCatMappingXml.Size = new System.Drawing.Size(924, 549);
            this.tabCatMappingXml.TabIndex = 4;
            this.tabCatMappingXml.Text = "Category";
            this.tabCatMappingXml.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.05381F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 78.94619F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 205F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 145F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 259F));
            this.tableLayoutPanel3.Controls.Add(this.label35, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label36, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label38, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.tbCategoryXPath, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label37, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.tbDefaultCategory_xml, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.label34, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.tbCategoryDelimeter_xml, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.label39, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label40, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label41, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.label42, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.tbCategoryXPath1, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.tbCategoryXPath2, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.tbCategoryXPath3, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.tbCategoryXPath4, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.tbDefaultCategory1_xml, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.tbDefaultCategory2_xml, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.tbDefaultCategory3_xml, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.tbDefaultCategory4_xml, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.label14, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.chCatId_xml, 4, 1);
            this.tableLayoutPanel3.Controls.Add(this.chCat1Id_xml, 4, 2);
            this.tableLayoutPanel3.Controls.Add(this.chCat2Id_xml, 4, 3);
            this.tableLayoutPanel3.Controls.Add(this.chCat3Id_xml, 4, 4);
            this.tableLayoutPanel3.Controls.Add(this.chCat4Id_xml, 4, 5);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(-4, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 7;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(934, 547);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(4, 1);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(49, 34);
            this.label35.TabIndex = 12;
            this.label35.Text = "Field Name";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(72, 1);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(135, 17);
            this.label36.TabIndex = 13;
            this.label36.Text = "XPath Expression";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(4, 42);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(60, 26);
            this.label38.TabIndex = 23;
            this.label38.Text = "Category(ies)";
            // 
            // tbCategoryXPath
            // 
            this.tbCategoryXPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCategoryXPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCategoryXPath.Location = new System.Drawing.Point(72, 45);
            this.tbCategoryXPath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCategoryXPath.Name = "tbCategoryXPath";
            this.tbCategoryXPath.Size = new System.Drawing.Size(228, 20);
            this.tbCategoryXPath.TabIndex = 24;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(324, 1);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(199, 17);
            this.label37.TabIndex = 22;
            this.label37.Text = "Default Value /+Add Value";
            // 
            // tbDefaultCategory_xml
            // 
            this.tbDefaultCategory_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCategory_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCategory_xml.Location = new System.Drawing.Point(324, 45);
            this.tbDefaultCategory_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCategory_xml.Name = "tbDefaultCategory_xml";
            this.tbDefaultCategory_xml.Size = new System.Drawing.Size(182, 20);
            this.tbDefaultCategory_xml.TabIndex = 27;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(530, 1);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(133, 17);
            this.label34.TabIndex = 28;
            this.label34.Text = "Delimiter To Split";
            // 
            // tbCategoryDelimeter_xml
            // 
            this.tbCategoryDelimeter_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCategoryDelimeter_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCategoryDelimeter_xml.Location = new System.Drawing.Point(530, 45);
            this.tbCategoryDelimeter_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCategoryDelimeter_xml.MaxLength = 1;
            this.tbCategoryDelimeter_xml.Name = "tbCategoryDelimeter_xml";
            this.tbCategoryDelimeter_xml.Size = new System.Drawing.Size(122, 20);
            this.tbCategoryDelimeter_xml.TabIndex = 29;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(4, 73);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(55, 13);
            this.label39.TabIndex = 30;
            this.label39.Text = "Category1";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(4, 104);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(55, 13);
            this.label40.TabIndex = 31;
            this.label40.Text = "Category2";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(4, 135);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(55, 13);
            this.label41.TabIndex = 32;
            this.label41.Text = "Category3";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(4, 166);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(55, 13);
            this.label42.TabIndex = 33;
            this.label42.Text = "Category4";
            // 
            // tbCategoryXPath1
            // 
            this.tbCategoryXPath1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCategoryXPath1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCategoryXPath1.Location = new System.Drawing.Point(72, 76);
            this.tbCategoryXPath1.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCategoryXPath1.Name = "tbCategoryXPath1";
            this.tbCategoryXPath1.Size = new System.Drawing.Size(228, 20);
            this.tbCategoryXPath1.TabIndex = 34;
            // 
            // tbCategoryXPath2
            // 
            this.tbCategoryXPath2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCategoryXPath2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCategoryXPath2.Location = new System.Drawing.Point(72, 107);
            this.tbCategoryXPath2.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCategoryXPath2.Name = "tbCategoryXPath2";
            this.tbCategoryXPath2.Size = new System.Drawing.Size(228, 20);
            this.tbCategoryXPath2.TabIndex = 35;
            // 
            // tbCategoryXPath3
            // 
            this.tbCategoryXPath3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCategoryXPath3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCategoryXPath3.Location = new System.Drawing.Point(72, 138);
            this.tbCategoryXPath3.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCategoryXPath3.Name = "tbCategoryXPath3";
            this.tbCategoryXPath3.Size = new System.Drawing.Size(228, 20);
            this.tbCategoryXPath3.TabIndex = 36;
            // 
            // tbCategoryXPath4
            // 
            this.tbCategoryXPath4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCategoryXPath4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCategoryXPath4.Location = new System.Drawing.Point(72, 169);
            this.tbCategoryXPath4.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCategoryXPath4.Name = "tbCategoryXPath4";
            this.tbCategoryXPath4.Size = new System.Drawing.Size(228, 20);
            this.tbCategoryXPath4.TabIndex = 37;
            // 
            // tbDefaultCategory1_xml
            // 
            this.tbDefaultCategory1_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCategory1_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCategory1_xml.Location = new System.Drawing.Point(324, 76);
            this.tbDefaultCategory1_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCategory1_xml.Name = "tbDefaultCategory1_xml";
            this.tbDefaultCategory1_xml.Size = new System.Drawing.Size(182, 20);
            this.tbDefaultCategory1_xml.TabIndex = 38;
            // 
            // tbDefaultCategory2_xml
            // 
            this.tbDefaultCategory2_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCategory2_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCategory2_xml.Location = new System.Drawing.Point(324, 107);
            this.tbDefaultCategory2_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCategory2_xml.Name = "tbDefaultCategory2_xml";
            this.tbDefaultCategory2_xml.Size = new System.Drawing.Size(182, 20);
            this.tbDefaultCategory2_xml.TabIndex = 39;
            // 
            // tbDefaultCategory3_xml
            // 
            this.tbDefaultCategory3_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCategory3_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCategory3_xml.Location = new System.Drawing.Point(324, 138);
            this.tbDefaultCategory3_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCategory3_xml.Name = "tbDefaultCategory3_xml";
            this.tbDefaultCategory3_xml.Size = new System.Drawing.Size(182, 20);
            this.tbDefaultCategory3_xml.TabIndex = 40;
            // 
            // tbDefaultCategory4_xml
            // 
            this.tbDefaultCategory4_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCategory4_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCategory4_xml.Location = new System.Drawing.Point(324, 169);
            this.tbDefaultCategory4_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCategory4_xml.Name = "tbDefaultCategory4_xml";
            this.tbDefaultCategory4_xml.Size = new System.Drawing.Size(182, 20);
            this.tbDefaultCategory4_xml.TabIndex = 41;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(676, 1);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 17);
            this.label14.TabIndex = 42;
            this.label14.Text = "Id Value";
            // 
            // chCatId_xml
            // 
            this.chCatId_xml.AutoSize = true;
            this.chCatId_xml.Location = new System.Drawing.Point(676, 45);
            this.chCatId_xml.Name = "chCatId_xml";
            this.chCatId_xml.Size = new System.Drawing.Size(15, 14);
            this.chCatId_xml.TabIndex = 43;
            this.chCatId_xml.UseVisualStyleBackColor = true;
            // 
            // chCat1Id_xml
            // 
            this.chCat1Id_xml.AutoSize = true;
            this.chCat1Id_xml.Location = new System.Drawing.Point(676, 76);
            this.chCat1Id_xml.Name = "chCat1Id_xml";
            this.chCat1Id_xml.Size = new System.Drawing.Size(15, 14);
            this.chCat1Id_xml.TabIndex = 44;
            this.chCat1Id_xml.UseVisualStyleBackColor = true;
            // 
            // chCat2Id_xml
            // 
            this.chCat2Id_xml.AutoSize = true;
            this.chCat2Id_xml.Location = new System.Drawing.Point(676, 107);
            this.chCat2Id_xml.Name = "chCat2Id_xml";
            this.chCat2Id_xml.Size = new System.Drawing.Size(15, 14);
            this.chCat2Id_xml.TabIndex = 45;
            this.chCat2Id_xml.UseVisualStyleBackColor = true;
            // 
            // chCat3Id_xml
            // 
            this.chCat3Id_xml.AutoSize = true;
            this.chCat3Id_xml.Location = new System.Drawing.Point(676, 138);
            this.chCat3Id_xml.Name = "chCat3Id_xml";
            this.chCat3Id_xml.Size = new System.Drawing.Size(15, 14);
            this.chCat3Id_xml.TabIndex = 46;
            this.chCat3Id_xml.UseVisualStyleBackColor = true;
            // 
            // chCat4Id_xml
            // 
            this.chCat4Id_xml.AutoSize = true;
            this.chCat4Id_xml.Location = new System.Drawing.Point(676, 169);
            this.chCat4Id_xml.Name = "chCat4Id_xml";
            this.chCat4Id_xml.Size = new System.Drawing.Size(15, 14);
            this.chCat4Id_xml.TabIndex = 47;
            this.chCat4Id_xml.UseVisualStyleBackColor = true;
            // 
            // tabAccessRolesMappingXml
            // 
            this.tabAccessRolesMappingXml.Controls.Add(this.tableLayoutPanel17);
            this.tabAccessRolesMappingXml.Location = new System.Drawing.Point(4, 22);
            this.tabAccessRolesMappingXml.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabAccessRolesMappingXml.Name = "tabAccessRolesMappingXml";
            this.tabAccessRolesMappingXml.Size = new System.Drawing.Size(924, 549);
            this.tabAccessRolesMappingXml.TabIndex = 16;
            this.tabAccessRolesMappingXml.Text = "Access Roles";
            this.tabAccessRolesMappingXml.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel17.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel17.ColumnCount = 5;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.61403F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.38596F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 205F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 145F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 259F));
            this.tableLayoutPanel17.Controls.Add(this.label159, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.label160, 1, 0);
            this.tableLayoutPanel17.Controls.Add(this.label161, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.tbCustomerRoleXpath, 1, 1);
            this.tableLayoutPanel17.Controls.Add(this.label162, 2, 0);
            this.tableLayoutPanel17.Controls.Add(this.tbDefaultCustomerRole_xml, 2, 1);
            this.tableLayoutPanel17.Controls.Add(this.label163, 3, 0);
            this.tableLayoutPanel17.Controls.Add(this.tbCustomerRoleDelimeter_xml, 3, 1);
            this.tableLayoutPanel17.Controls.Add(this.label164, 0, 2);
            this.tableLayoutPanel17.Controls.Add(this.label165, 0, 3);
            this.tableLayoutPanel17.Controls.Add(this.label166, 0, 4);
            this.tableLayoutPanel17.Controls.Add(this.label167, 0, 5);
            this.tableLayoutPanel17.Controls.Add(this.tbCustomerRoleXpath1, 1, 2);
            this.tableLayoutPanel17.Controls.Add(this.tbCustomerRoleXpath2, 1, 3);
            this.tableLayoutPanel17.Controls.Add(this.tbCustomerRoleXpath3, 1, 4);
            this.tableLayoutPanel17.Controls.Add(this.tbCustomerRoleXpath4, 1, 5);
            this.tableLayoutPanel17.Controls.Add(this.tbDefaultCustomerRole1_xml, 2, 2);
            this.tableLayoutPanel17.Controls.Add(this.tbDefaultCustomerRole2_xml, 2, 3);
            this.tableLayoutPanel17.Controls.Add(this.tbDefaultCustomerRole3_xml, 2, 4);
            this.tableLayoutPanel17.Controls.Add(this.tbDefaultCustomerRole4_xml, 2, 5);
            this.tableLayoutPanel17.Controls.Add(this.label168, 4, 0);
            this.tableLayoutPanel17.Controls.Add(this.chCustomerRoleID_xml, 4, 1);
            this.tableLayoutPanel17.Controls.Add(this.chCustomerRoleID1_xml, 4, 2);
            this.tableLayoutPanel17.Controls.Add(this.chCustomerRoleID2_xml, 4, 3);
            this.tableLayoutPanel17.Controls.Add(this.chCustomerRoleID3_xml, 4, 4);
            this.tableLayoutPanel17.Controls.Add(this.chCustomerRoleID4_xml, 4, 5);
            this.tableLayoutPanel17.Location = new System.Drawing.Point(-3, -1);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 7;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(933, 505);
            this.tableLayoutPanel17.TabIndex = 2;
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label159.Location = new System.Drawing.Point(4, 1);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(49, 34);
            this.label159.TabIndex = 12;
            this.label159.Text = "Field Name";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label160.Location = new System.Drawing.Point(86, 1);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(135, 17);
            this.label160.TabIndex = 13;
            this.label160.Text = "XPath Expression";
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(4, 42);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(54, 26);
            this.label161.TabIndex = 23;
            this.label161.Text = "Customer Role(s)";
            // 
            // tbCustomerRoleXpath
            // 
            this.tbCustomerRoleXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCustomerRoleXpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCustomerRoleXpath.Location = new System.Drawing.Point(86, 45);
            this.tbCustomerRoleXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCustomerRoleXpath.Name = "tbCustomerRoleXpath";
            this.tbCustomerRoleXpath.Size = new System.Drawing.Size(213, 20);
            this.tbCustomerRoleXpath.TabIndex = 24;
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label162.Location = new System.Drawing.Point(323, 1);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(199, 17);
            this.label162.TabIndex = 22;
            this.label162.Text = "Default Value /+Add Value";
            // 
            // tbDefaultCustomerRole_xml
            // 
            this.tbDefaultCustomerRole_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCustomerRole_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCustomerRole_xml.Location = new System.Drawing.Point(323, 45);
            this.tbDefaultCustomerRole_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCustomerRole_xml.Name = "tbDefaultCustomerRole_xml";
            this.tbDefaultCustomerRole_xml.Size = new System.Drawing.Size(182, 20);
            this.tbDefaultCustomerRole_xml.TabIndex = 27;
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label163.Location = new System.Drawing.Point(529, 1);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(133, 17);
            this.label163.TabIndex = 28;
            this.label163.Text = "Delimiter To Split";
            // 
            // tbCustomerRoleDelimeter_xml
            // 
            this.tbCustomerRoleDelimeter_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCustomerRoleDelimeter_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCustomerRoleDelimeter_xml.Location = new System.Drawing.Point(529, 45);
            this.tbCustomerRoleDelimeter_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCustomerRoleDelimeter_xml.MaxLength = 1;
            this.tbCustomerRoleDelimeter_xml.Name = "tbCustomerRoleDelimeter_xml";
            this.tbCustomerRoleDelimeter_xml.Size = new System.Drawing.Size(122, 20);
            this.tbCustomerRoleDelimeter_xml.TabIndex = 29;
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(4, 73);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(54, 26);
            this.label164.TabIndex = 30;
            this.label164.Text = "Customer Role1";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Location = new System.Drawing.Point(4, 104);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(54, 26);
            this.label165.TabIndex = 31;
            this.label165.Text = "Customer Role2";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(4, 135);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(54, 26);
            this.label166.TabIndex = 32;
            this.label166.Text = "Customer Role3";
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(4, 166);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(54, 26);
            this.label167.TabIndex = 33;
            this.label167.Text = "Customer Role4";
            // 
            // tbCustomerRoleXpath1
            // 
            this.tbCustomerRoleXpath1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCustomerRoleXpath1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCustomerRoleXpath1.Location = new System.Drawing.Point(86, 76);
            this.tbCustomerRoleXpath1.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCustomerRoleXpath1.Name = "tbCustomerRoleXpath1";
            this.tbCustomerRoleXpath1.Size = new System.Drawing.Size(213, 20);
            this.tbCustomerRoleXpath1.TabIndex = 34;
            // 
            // tbCustomerRoleXpath2
            // 
            this.tbCustomerRoleXpath2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCustomerRoleXpath2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCustomerRoleXpath2.Location = new System.Drawing.Point(86, 107);
            this.tbCustomerRoleXpath2.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCustomerRoleXpath2.Name = "tbCustomerRoleXpath2";
            this.tbCustomerRoleXpath2.Size = new System.Drawing.Size(213, 20);
            this.tbCustomerRoleXpath2.TabIndex = 35;
            // 
            // tbCustomerRoleXpath3
            // 
            this.tbCustomerRoleXpath3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCustomerRoleXpath3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCustomerRoleXpath3.Location = new System.Drawing.Point(86, 138);
            this.tbCustomerRoleXpath3.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCustomerRoleXpath3.Name = "tbCustomerRoleXpath3";
            this.tbCustomerRoleXpath3.Size = new System.Drawing.Size(213, 20);
            this.tbCustomerRoleXpath3.TabIndex = 36;
            // 
            // tbCustomerRoleXpath4
            // 
            this.tbCustomerRoleXpath4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCustomerRoleXpath4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCustomerRoleXpath4.Location = new System.Drawing.Point(86, 169);
            this.tbCustomerRoleXpath4.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCustomerRoleXpath4.Name = "tbCustomerRoleXpath4";
            this.tbCustomerRoleXpath4.Size = new System.Drawing.Size(213, 20);
            this.tbCustomerRoleXpath4.TabIndex = 37;
            this.tbCustomerRoleXpath4.TextChanged += new System.EventHandler(this.textBox7_TextChanged);
            // 
            // tbDefaultCustomerRole1_xml
            // 
            this.tbDefaultCustomerRole1_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCustomerRole1_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCustomerRole1_xml.Location = new System.Drawing.Point(323, 76);
            this.tbDefaultCustomerRole1_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCustomerRole1_xml.Name = "tbDefaultCustomerRole1_xml";
            this.tbDefaultCustomerRole1_xml.Size = new System.Drawing.Size(182, 20);
            this.tbDefaultCustomerRole1_xml.TabIndex = 38;
            // 
            // tbDefaultCustomerRole2_xml
            // 
            this.tbDefaultCustomerRole2_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCustomerRole2_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCustomerRole2_xml.Location = new System.Drawing.Point(323, 107);
            this.tbDefaultCustomerRole2_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCustomerRole2_xml.Name = "tbDefaultCustomerRole2_xml";
            this.tbDefaultCustomerRole2_xml.Size = new System.Drawing.Size(182, 20);
            this.tbDefaultCustomerRole2_xml.TabIndex = 39;
            // 
            // tbDefaultCustomerRole3_xml
            // 
            this.tbDefaultCustomerRole3_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCustomerRole3_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCustomerRole3_xml.Location = new System.Drawing.Point(323, 138);
            this.tbDefaultCustomerRole3_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCustomerRole3_xml.Name = "tbDefaultCustomerRole3_xml";
            this.tbDefaultCustomerRole3_xml.Size = new System.Drawing.Size(182, 20);
            this.tbDefaultCustomerRole3_xml.TabIndex = 40;
            // 
            // tbDefaultCustomerRole4_xml
            // 
            this.tbDefaultCustomerRole4_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCustomerRole4_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCustomerRole4_xml.Location = new System.Drawing.Point(323, 169);
            this.tbDefaultCustomerRole4_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCustomerRole4_xml.Name = "tbDefaultCustomerRole4_xml";
            this.tbDefaultCustomerRole4_xml.Size = new System.Drawing.Size(182, 20);
            this.tbDefaultCustomerRole4_xml.TabIndex = 41;
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label168.Location = new System.Drawing.Point(675, 1);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(67, 17);
            this.label168.TabIndex = 42;
            this.label168.Text = "Id Value";
            // 
            // chCustomerRoleID_xml
            // 
            this.chCustomerRoleID_xml.AutoSize = true;
            this.chCustomerRoleID_xml.Location = new System.Drawing.Point(675, 45);
            this.chCustomerRoleID_xml.Name = "chCustomerRoleID_xml";
            this.chCustomerRoleID_xml.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRoleID_xml.TabIndex = 43;
            this.chCustomerRoleID_xml.UseVisualStyleBackColor = true;
            // 
            // chCustomerRoleID1_xml
            // 
            this.chCustomerRoleID1_xml.AutoSize = true;
            this.chCustomerRoleID1_xml.Location = new System.Drawing.Point(675, 76);
            this.chCustomerRoleID1_xml.Name = "chCustomerRoleID1_xml";
            this.chCustomerRoleID1_xml.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRoleID1_xml.TabIndex = 44;
            this.chCustomerRoleID1_xml.UseVisualStyleBackColor = true;
            // 
            // chCustomerRoleID2_xml
            // 
            this.chCustomerRoleID2_xml.AutoSize = true;
            this.chCustomerRoleID2_xml.Location = new System.Drawing.Point(675, 107);
            this.chCustomerRoleID2_xml.Name = "chCustomerRoleID2_xml";
            this.chCustomerRoleID2_xml.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRoleID2_xml.TabIndex = 45;
            this.chCustomerRoleID2_xml.UseVisualStyleBackColor = true;
            // 
            // chCustomerRoleID3_xml
            // 
            this.chCustomerRoleID3_xml.AutoSize = true;
            this.chCustomerRoleID3_xml.Location = new System.Drawing.Point(675, 138);
            this.chCustomerRoleID3_xml.Name = "chCustomerRoleID3_xml";
            this.chCustomerRoleID3_xml.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRoleID3_xml.TabIndex = 46;
            this.chCustomerRoleID3_xml.UseVisualStyleBackColor = true;
            // 
            // chCustomerRoleID4_xml
            // 
            this.chCustomerRoleID4_xml.AutoSize = true;
            this.chCustomerRoleID4_xml.Location = new System.Drawing.Point(675, 169);
            this.chCustomerRoleID4_xml.Name = "chCustomerRoleID4_xml";
            this.chCustomerRoleID4_xml.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRoleID4_xml.TabIndex = 47;
            this.chCustomerRoleID4_xml.UseVisualStyleBackColor = true;
            // 
            // tabManufMappingXml
            // 
            this.tabManufMappingXml.Controls.Add(this.tableLayoutPanel6);
            this.tabManufMappingXml.Location = new System.Drawing.Point(4, 22);
            this.tabManufMappingXml.Name = "tabManufMappingXml";
            this.tabManufMappingXml.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabManufMappingXml.Size = new System.Drawing.Size(924, 549);
            this.tabManufMappingXml.TabIndex = 5;
            this.tabManufMappingXml.Text = "Manufacturer&Vendor";
            this.tabManufMappingXml.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.50478F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.49522F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 259F));
            this.tableLayoutPanel6.Controls.Add(this.label13, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.tbDefaultManufacturer_xml, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.label44, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label45, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.label46, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.label47, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.tbManufacturerXPath, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.tbVendorXpath, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.label143, 3, 0);
            this.tableLayoutPanel6.Controls.Add(this.tbDefaultVendor_xml, 2, 2);
            this.tableLayoutPanel6.Controls.Add(this.chVendorId_xml, 3, 2);
            this.tableLayoutPanel6.Controls.Add(this.chManufacturerId_xml, 3, 1);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(-4, -2);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 3;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(931, 502);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(4, 73);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 13);
            this.label13.TabIndex = 39;
            this.label13.Text = "Vendor";
            // 
            // tbDefaultManufacturer_xml
            // 
            this.tbDefaultManufacturer_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultManufacturer_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultManufacturer_xml.Location = new System.Drawing.Point(422, 45);
            this.tbDefaultManufacturer_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultManufacturer_xml.Name = "tbDefaultManufacturer_xml";
            this.tbDefaultManufacturer_xml.Size = new System.Drawing.Size(227, 20);
            this.tbDefaultManufacturer_xml.TabIndex = 27;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(4, 1);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(89, 17);
            this.label44.TabIndex = 12;
            this.label44.Text = "Field Name";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(140, 1);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(135, 17);
            this.label45.TabIndex = 13;
            this.label45.Text = "XPath Expression";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(422, 1);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(199, 17);
            this.label46.TabIndex = 22;
            this.label46.Text = "Default Value /+Add Value";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(4, 42);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(70, 13);
            this.label47.TabIndex = 23;
            this.label47.Text = "Manufacturer";
            // 
            // tbManufacturerXPath
            // 
            this.tbManufacturerXPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbManufacturerXPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbManufacturerXPath.Location = new System.Drawing.Point(140, 45);
            this.tbManufacturerXPath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbManufacturerXPath.Name = "tbManufacturerXPath";
            this.tbManufacturerXPath.Size = new System.Drawing.Size(258, 20);
            this.tbManufacturerXPath.TabIndex = 24;
            // 
            // tbVendorXpath
            // 
            this.tbVendorXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbVendorXpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbVendorXpath.Location = new System.Drawing.Point(140, 76);
            this.tbVendorXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbVendorXpath.Name = "tbVendorXpath";
            this.tbVendorXpath.Size = new System.Drawing.Size(258, 20);
            this.tbVendorXpath.TabIndex = 40;
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label143.Location = new System.Drawing.Point(673, 1);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(67, 17);
            this.label143.TabIndex = 43;
            this.label143.Text = "Id Value";
            // 
            // tbDefaultVendor_xml
            // 
            this.tbDefaultVendor_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultVendor_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultVendor_xml.Location = new System.Drawing.Point(422, 76);
            this.tbDefaultVendor_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultVendor_xml.Multiline = true;
            this.tbDefaultVendor_xml.Name = "tbDefaultVendor_xml";
            this.tbDefaultVendor_xml.Size = new System.Drawing.Size(227, 20);
            this.tbDefaultVendor_xml.TabIndex = 41;
            // 
            // chVendorId_xml
            // 
            this.chVendorId_xml.AutoSize = true;
            this.chVendorId_xml.Location = new System.Drawing.Point(673, 76);
            this.chVendorId_xml.Name = "chVendorId_xml";
            this.chVendorId_xml.Size = new System.Drawing.Size(15, 14);
            this.chVendorId_xml.TabIndex = 69;
            this.chVendorId_xml.UseVisualStyleBackColor = true;
            // 
            // chManufacturerId_xml
            // 
            this.chManufacturerId_xml.AutoSize = true;
            this.chManufacturerId_xml.Location = new System.Drawing.Point(673, 45);
            this.chManufacturerId_xml.Name = "chManufacturerId_xml";
            this.chManufacturerId_xml.Size = new System.Drawing.Size(15, 14);
            this.chManufacturerId_xml.TabIndex = 70;
            this.chManufacturerId_xml.UseVisualStyleBackColor = true;
            // 
            // tabFilterMappingXml
            // 
            this.tabFilterMappingXml.Controls.Add(this.tableLayoutPanel9);
            this.tabFilterMappingXml.Location = new System.Drawing.Point(4, 22);
            this.tabFilterMappingXml.Name = "tabFilterMappingXml";
            this.tabFilterMappingXml.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabFilterMappingXml.Size = new System.Drawing.Size(924, 549);
            this.tabFilterMappingXml.TabIndex = 8;
            this.tabFilterMappingXml.Text = "Filters";
            this.tabFilterMappingXml.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel9.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel9.ColumnCount = 3;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.50218F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 212F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.49782F));
            this.tableLayoutPanel9.Controls.Add(this.tbFilterXpath, 2, 1);
            this.tableLayoutPanel9.Controls.Add(this.label65, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.label66, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.label67, 2, 0);
            this.tableLayoutPanel9.Controls.Add(this.label68, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterName_xml, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.label69, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.label70, 0, 3);
            this.tableLayoutPanel9.Controls.Add(this.label71, 0, 4);
            this.tableLayoutPanel9.Controls.Add(this.label72, 0, 5);
            this.tableLayoutPanel9.Controls.Add(this.label73, 0, 6);
            this.tableLayoutPanel9.Controls.Add(this.label74, 0, 7);
            this.tableLayoutPanel9.Controls.Add(this.label75, 0, 8);
            this.tableLayoutPanel9.Controls.Add(this.label76, 0, 9);
            this.tableLayoutPanel9.Controls.Add(this.label77, 0, 10);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterName1_xml, 1, 2);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterName2_xml, 1, 3);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterName3_xml, 1, 4);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterName4_xml, 1, 5);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterName5_xml, 1, 6);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterName6_xml, 1, 7);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterName7_xml, 1, 8);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterName8_xml, 1, 9);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterName9_xml, 1, 10);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterXpath1, 2, 2);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterXpath2, 2, 3);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterXpath3, 2, 4);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterXpath4, 2, 5);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterXpath5, 2, 6);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterXpath6, 2, 7);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterXpath7, 2, 8);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterXpath8, 2, 9);
            this.tableLayoutPanel9.Controls.Add(this.tbFilterXpath9, 2, 10);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(-4, -2);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 13;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(931, 487);
            this.tableLayoutPanel9.TabIndex = 2;
            // 
            // tbFilterXpath
            // 
            this.tbFilterXpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterXpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterXpath.Location = new System.Drawing.Point(450, 45);
            this.tbFilterXpath.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterXpath.Name = "tbFilterXpath";
            this.tbFilterXpath.Size = new System.Drawing.Size(460, 20);
            this.tbFilterXpath.TabIndex = 27;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(4, 1);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(89, 17);
            this.label65.TabIndex = 12;
            this.label65.Text = "Field Name";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(237, 1);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(91, 17);
            this.label66.TabIndex = 13;
            this.label66.Text = "Filter Name";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(450, 1);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(135, 17);
            this.label67.TabIndex = 22;
            this.label67.Text = "XPath Expression";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(4, 42);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(29, 13);
            this.label68.TabIndex = 23;
            this.label68.Text = "Filter";
            // 
            // tbFilterName_xml
            // 
            this.tbFilterName_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterName_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterName_xml.Location = new System.Drawing.Point(237, 45);
            this.tbFilterName_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterName_xml.Name = "tbFilterName_xml";
            this.tbFilterName_xml.Size = new System.Drawing.Size(189, 20);
            this.tbFilterName_xml.TabIndex = 24;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(4, 73);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(35, 13);
            this.label69.TabIndex = 28;
            this.label69.Text = "Filter1";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(4, 104);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(35, 13);
            this.label70.TabIndex = 29;
            this.label70.Text = "Filter2";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(4, 135);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(35, 13);
            this.label71.TabIndex = 30;
            this.label71.Text = "Filter3";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(4, 166);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(35, 13);
            this.label72.TabIndex = 31;
            this.label72.Text = "Filter4";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(4, 197);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(35, 13);
            this.label73.TabIndex = 32;
            this.label73.Text = "Filter5";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(4, 228);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(35, 13);
            this.label74.TabIndex = 33;
            this.label74.Text = "Filter6";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(4, 259);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(35, 13);
            this.label75.TabIndex = 34;
            this.label75.Text = "Filter7";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(4, 290);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(35, 13);
            this.label76.TabIndex = 35;
            this.label76.Text = "Filter8";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(4, 321);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(35, 13);
            this.label77.TabIndex = 36;
            this.label77.Text = "Filter9";
            // 
            // tbFilterName1_xml
            // 
            this.tbFilterName1_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterName1_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterName1_xml.Location = new System.Drawing.Point(237, 76);
            this.tbFilterName1_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterName1_xml.Name = "tbFilterName1_xml";
            this.tbFilterName1_xml.Size = new System.Drawing.Size(189, 20);
            this.tbFilterName1_xml.TabIndex = 37;
            // 
            // tbFilterName2_xml
            // 
            this.tbFilterName2_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterName2_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterName2_xml.Location = new System.Drawing.Point(237, 107);
            this.tbFilterName2_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterName2_xml.Name = "tbFilterName2_xml";
            this.tbFilterName2_xml.Size = new System.Drawing.Size(189, 20);
            this.tbFilterName2_xml.TabIndex = 38;
            // 
            // tbFilterName3_xml
            // 
            this.tbFilterName3_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterName3_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterName3_xml.Location = new System.Drawing.Point(237, 138);
            this.tbFilterName3_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterName3_xml.Name = "tbFilterName3_xml";
            this.tbFilterName3_xml.Size = new System.Drawing.Size(189, 20);
            this.tbFilterName3_xml.TabIndex = 39;
            // 
            // tbFilterName4_xml
            // 
            this.tbFilterName4_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterName4_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterName4_xml.Location = new System.Drawing.Point(237, 169);
            this.tbFilterName4_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterName4_xml.Name = "tbFilterName4_xml";
            this.tbFilterName4_xml.Size = new System.Drawing.Size(189, 20);
            this.tbFilterName4_xml.TabIndex = 40;
            // 
            // tbFilterName5_xml
            // 
            this.tbFilterName5_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterName5_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterName5_xml.Location = new System.Drawing.Point(237, 200);
            this.tbFilterName5_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterName5_xml.Name = "tbFilterName5_xml";
            this.tbFilterName5_xml.Size = new System.Drawing.Size(189, 20);
            this.tbFilterName5_xml.TabIndex = 41;
            // 
            // tbFilterName6_xml
            // 
            this.tbFilterName6_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterName6_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterName6_xml.Location = new System.Drawing.Point(237, 231);
            this.tbFilterName6_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterName6_xml.Name = "tbFilterName6_xml";
            this.tbFilterName6_xml.Size = new System.Drawing.Size(189, 20);
            this.tbFilterName6_xml.TabIndex = 42;
            // 
            // tbFilterName7_xml
            // 
            this.tbFilterName7_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterName7_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterName7_xml.Location = new System.Drawing.Point(237, 262);
            this.tbFilterName7_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterName7_xml.Name = "tbFilterName7_xml";
            this.tbFilterName7_xml.Size = new System.Drawing.Size(189, 20);
            this.tbFilterName7_xml.TabIndex = 43;
            // 
            // tbFilterName8_xml
            // 
            this.tbFilterName8_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterName8_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterName8_xml.Location = new System.Drawing.Point(237, 293);
            this.tbFilterName8_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterName8_xml.Name = "tbFilterName8_xml";
            this.tbFilterName8_xml.Size = new System.Drawing.Size(189, 20);
            this.tbFilterName8_xml.TabIndex = 44;
            // 
            // tbFilterName9_xml
            // 
            this.tbFilterName9_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterName9_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterName9_xml.Location = new System.Drawing.Point(237, 324);
            this.tbFilterName9_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterName9_xml.Name = "tbFilterName9_xml";
            this.tbFilterName9_xml.Size = new System.Drawing.Size(189, 20);
            this.tbFilterName9_xml.TabIndex = 45;
            // 
            // tbFilterXpath1
            // 
            this.tbFilterXpath1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterXpath1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterXpath1.Location = new System.Drawing.Point(450, 76);
            this.tbFilterXpath1.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterXpath1.Name = "tbFilterXpath1";
            this.tbFilterXpath1.Size = new System.Drawing.Size(460, 20);
            this.tbFilterXpath1.TabIndex = 46;
            // 
            // tbFilterXpath2
            // 
            this.tbFilterXpath2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterXpath2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterXpath2.Location = new System.Drawing.Point(450, 107);
            this.tbFilterXpath2.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterXpath2.Name = "tbFilterXpath2";
            this.tbFilterXpath2.Size = new System.Drawing.Size(460, 20);
            this.tbFilterXpath2.TabIndex = 47;
            // 
            // tbFilterXpath3
            // 
            this.tbFilterXpath3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterXpath3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterXpath3.Location = new System.Drawing.Point(450, 138);
            this.tbFilterXpath3.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterXpath3.Name = "tbFilterXpath3";
            this.tbFilterXpath3.Size = new System.Drawing.Size(460, 20);
            this.tbFilterXpath3.TabIndex = 48;
            // 
            // tbFilterXpath4
            // 
            this.tbFilterXpath4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterXpath4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterXpath4.Location = new System.Drawing.Point(450, 169);
            this.tbFilterXpath4.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterXpath4.Name = "tbFilterXpath4";
            this.tbFilterXpath4.Size = new System.Drawing.Size(460, 20);
            this.tbFilterXpath4.TabIndex = 49;
            // 
            // tbFilterXpath5
            // 
            this.tbFilterXpath5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterXpath5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterXpath5.Location = new System.Drawing.Point(450, 200);
            this.tbFilterXpath5.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterXpath5.Name = "tbFilterXpath5";
            this.tbFilterXpath5.Size = new System.Drawing.Size(460, 20);
            this.tbFilterXpath5.TabIndex = 50;
            // 
            // tbFilterXpath6
            // 
            this.tbFilterXpath6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterXpath6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterXpath6.Location = new System.Drawing.Point(450, 231);
            this.tbFilterXpath6.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterXpath6.Name = "tbFilterXpath6";
            this.tbFilterXpath6.Size = new System.Drawing.Size(460, 20);
            this.tbFilterXpath6.TabIndex = 51;
            // 
            // tbFilterXpath7
            // 
            this.tbFilterXpath7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterXpath7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterXpath7.Location = new System.Drawing.Point(450, 262);
            this.tbFilterXpath7.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterXpath7.Name = "tbFilterXpath7";
            this.tbFilterXpath7.Size = new System.Drawing.Size(460, 20);
            this.tbFilterXpath7.TabIndex = 52;
            // 
            // tbFilterXpath8
            // 
            this.tbFilterXpath8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterXpath8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterXpath8.Location = new System.Drawing.Point(450, 293);
            this.tbFilterXpath8.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterXpath8.Name = "tbFilterXpath8";
            this.tbFilterXpath8.Size = new System.Drawing.Size(460, 20);
            this.tbFilterXpath8.TabIndex = 53;
            // 
            // tbFilterXpath9
            // 
            this.tbFilterXpath9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterXpath9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterXpath9.Location = new System.Drawing.Point(450, 324);
            this.tbFilterXpath9.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterXpath9.Name = "tbFilterXpath9";
            this.tbFilterXpath9.Size = new System.Drawing.Size(460, 20);
            this.tbFilterXpath9.TabIndex = 54;
            // 
            // tabProductInfoMappingPos
            // 
            this.tabProductInfoMappingPos.Controls.Add(this.tableLayoutPanel10);
            this.tabProductInfoMappingPos.Location = new System.Drawing.Point(4, 22);
            this.tabProductInfoMappingPos.Name = "tabProductInfoMappingPos";
            this.tabProductInfoMappingPos.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabProductInfoMappingPos.Size = new System.Drawing.Size(924, 549);
            this.tabProductInfoMappingPos.TabIndex = 9;
            this.tabProductInfoMappingPos.Text = "Product Info";
            this.tabProductInfoMappingPos.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel10.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel10.ColumnCount = 3;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 262F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 188F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.label82, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.label89, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.label90, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.label86, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.label85, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.label84, 0, 5);
            this.tableLayoutPanel10.Controls.Add(this.label83, 0, 6);
            this.tableLayoutPanel10.Controls.Add(this.label80, 0, 7);
            this.tableLayoutPanel10.Controls.Add(this.label91, 0, 8);
            this.tableLayoutPanel10.Controls.Add(this.tbDefaultProdName_pos, 2, 4);
            this.tableLayoutPanel10.Controls.Add(this.tbDefaultProdShortDesc_pos, 2, 5);
            this.tableLayoutPanel10.Controls.Add(this.tbDefaultProdFullDesc_pos, 2, 6);
            this.tableLayoutPanel10.Controls.Add(this.tbProdManPartNum_pos, 2, 7);
            this.tableLayoutPanel10.Controls.Add(this.nmDefaultProdStock_pos, 2, 8);
            this.tableLayoutPanel10.Controls.Add(this.nmProdId_pos, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.nmProdName_pos, 1, 4);
            this.tableLayoutPanel10.Controls.Add(this.nmProdShortDesc_pos, 1, 5);
            this.tableLayoutPanel10.Controls.Add(this.nmProdFullDesc_pos, 1, 6);
            this.tableLayoutPanel10.Controls.Add(this.nmProdManPartNum_pos, 1, 7);
            this.tableLayoutPanel10.Controls.Add(this.nmProdStock_pos, 1, 8);
            this.tableLayoutPanel10.Controls.Add(this.label11, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.nmProdSKU_pos, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.label156, 0, 9);
            this.tableLayoutPanel10.Controls.Add(this.label157, 0, 10);
            this.tableLayoutPanel10.Controls.Add(this.nmGtin_pos, 1, 9);
            this.tableLayoutPanel10.Controls.Add(this.nmWeight_pos, 1, 10);
            this.tableLayoutPanel10.Controls.Add(this.tbDefaultWeight_pos, 2, 10);
            this.tableLayoutPanel10.Controls.Add(this.tbDefaultGtin_pos, 2, 9);
            this.tableLayoutPanel10.Controls.Add(this.nmDefaultProdSKU_pos, 2, 2);
            this.tableLayoutPanel10.Controls.Add(this.label179, 0, 11);
            this.tableLayoutPanel10.Controls.Add(this.label180, 0, 12);
            this.tableLayoutPanel10.Controls.Add(this.label181, 0, 13);
            this.tableLayoutPanel10.Controls.Add(this.nmLength_pos, 1, 11);
            this.tableLayoutPanel10.Controls.Add(this.nmWidth_pos, 1, 12);
            this.tableLayoutPanel10.Controls.Add(this.nmHeight_pos, 1, 13);
            this.tableLayoutPanel10.Controls.Add(this.tbDefaultLength_pos, 2, 11);
            this.tableLayoutPanel10.Controls.Add(this.tbDefaultWidth_pos, 2, 12);
            this.tableLayoutPanel10.Controls.Add(this.tbDefaultHeight_pos, 2, 13);
            this.tableLayoutPanel10.Controls.Add(this.label271, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.nmProdGroupBy_pos, 1, 3);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(-4, -2);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 14;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(931, 535);
            this.tableLayoutPanel10.TabIndex = 1;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(4, 1);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(89, 17);
            this.label82.TabIndex = 11;
            this.label82.Text = "Field Name";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(267, 1);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(160, 17);
            this.label89.TabIndex = 12;
            this.label89.Text = "Source Column Index";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(456, 1);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(199, 17);
            this.label90.TabIndex = 21;
            this.label90.Text = "Default Value /+Add Value";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(4, 32);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(58, 13);
            this.label86.TabIndex = 0;
            this.label86.Text = "Product ID";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(4, 113);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(75, 13);
            this.label85.TabIndex = 2;
            this.label85.Text = "Product Name";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(4, 140);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(88, 13);
            this.label84.TabIndex = 4;
            this.label84.Text = "Short Description";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(4, 167);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(79, 13);
            this.label83.TabIndex = 6;
            this.label83.Text = "Full Description";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(4, 217);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(132, 13);
            this.label80.TabIndex = 17;
            this.label80.Text = "Manufacturer Part Number";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(4, 244);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(77, 13);
            this.label91.TabIndex = 19;
            this.label91.Text = "Stock Quantity";
            // 
            // tbDefaultProdName_pos
            // 
            this.tbDefaultProdName_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdName_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdName_pos.Location = new System.Drawing.Point(456, 116);
            this.tbDefaultProdName_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdName_pos.Name = "tbDefaultProdName_pos";
            this.tbDefaultProdName_pos.Size = new System.Drawing.Size(454, 20);
            this.tbDefaultProdName_pos.TabIndex = 22;
            // 
            // tbDefaultProdShortDesc_pos
            // 
            this.tbDefaultProdShortDesc_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdShortDesc_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdShortDesc_pos.Location = new System.Drawing.Point(456, 143);
            this.tbDefaultProdShortDesc_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdShortDesc_pos.Multiline = true;
            this.tbDefaultProdShortDesc_pos.Name = "tbDefaultProdShortDesc_pos";
            this.tbDefaultProdShortDesc_pos.Size = new System.Drawing.Size(454, 20);
            this.tbDefaultProdShortDesc_pos.TabIndex = 23;
            // 
            // tbDefaultProdFullDesc_pos
            // 
            this.tbDefaultProdFullDesc_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdFullDesc_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdFullDesc_pos.Location = new System.Drawing.Point(456, 170);
            this.tbDefaultProdFullDesc_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdFullDesc_pos.Multiline = true;
            this.tbDefaultProdFullDesc_pos.Name = "tbDefaultProdFullDesc_pos";
            this.tbDefaultProdFullDesc_pos.Size = new System.Drawing.Size(454, 43);
            this.tbDefaultProdFullDesc_pos.TabIndex = 24;
            // 
            // tbProdManPartNum_pos
            // 
            this.tbProdManPartNum_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdManPartNum_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdManPartNum_pos.Location = new System.Drawing.Point(456, 220);
            this.tbProdManPartNum_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdManPartNum_pos.Name = "tbProdManPartNum_pos";
            this.tbProdManPartNum_pos.Size = new System.Drawing.Size(454, 20);
            this.tbProdManPartNum_pos.TabIndex = 25;
            // 
            // nmDefaultProdStock_pos
            // 
            this.nmDefaultProdStock_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nmDefaultProdStock_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmDefaultProdStock_pos.Location = new System.Drawing.Point(456, 247);
            this.nmDefaultProdStock_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.nmDefaultProdStock_pos.Name = "nmDefaultProdStock_pos";
            this.nmDefaultProdStock_pos.Size = new System.Drawing.Size(454, 20);
            this.nmDefaultProdStock_pos.TabIndex = 34;
            // 
            // nmProdId_pos
            // 
            this.nmProdId_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdId_pos.Location = new System.Drawing.Point(267, 35);
            this.nmProdId_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdId_pos.Name = "nmProdId_pos";
            this.nmProdId_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdId_pos.TabIndex = 35;
            this.nmProdId_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdId_pos.Validating += new System.ComponentModel.CancelEventHandler(this.nmProdId_pos_Validating);
            this.nmProdId_pos.Validated += new System.EventHandler(this.nmProdId_pos_Validated);
            // 
            // nmProdName_pos
            // 
            this.nmProdName_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdName_pos.Location = new System.Drawing.Point(267, 116);
            this.nmProdName_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdName_pos.Name = "nmProdName_pos";
            this.nmProdName_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdName_pos.TabIndex = 36;
            this.nmProdName_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmProdShortDesc_pos
            // 
            this.nmProdShortDesc_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdShortDesc_pos.Location = new System.Drawing.Point(267, 143);
            this.nmProdShortDesc_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdShortDesc_pos.Name = "nmProdShortDesc_pos";
            this.nmProdShortDesc_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdShortDesc_pos.TabIndex = 37;
            this.nmProdShortDesc_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmProdFullDesc_pos
            // 
            this.nmProdFullDesc_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdFullDesc_pos.Location = new System.Drawing.Point(267, 170);
            this.nmProdFullDesc_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdFullDesc_pos.Name = "nmProdFullDesc_pos";
            this.nmProdFullDesc_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdFullDesc_pos.TabIndex = 38;
            this.nmProdFullDesc_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmProdManPartNum_pos
            // 
            this.nmProdManPartNum_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdManPartNum_pos.Location = new System.Drawing.Point(267, 220);
            this.nmProdManPartNum_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdManPartNum_pos.Name = "nmProdManPartNum_pos";
            this.nmProdManPartNum_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdManPartNum_pos.TabIndex = 39;
            this.nmProdManPartNum_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmProdStock_pos
            // 
            this.nmProdStock_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdStock_pos.Location = new System.Drawing.Point(267, 247);
            this.nmProdStock_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdStock_pos.Name = "nmProdStock_pos";
            this.nmProdStock_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdStock_pos.TabIndex = 42;
            this.nmProdStock_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 59);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(228, 13);
            this.label11.TabIndex = 43;
            this.label11.Text = "Product SKU (product unique identifier in store)";
            // 
            // nmProdSKU_pos
            // 
            this.nmProdSKU_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdSKU_pos.Location = new System.Drawing.Point(267, 62);
            this.nmProdSKU_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdSKU_pos.Name = "nmProdSKU_pos";
            this.nmProdSKU_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdSKU_pos.TabIndex = 44;
            this.nmProdSKU_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(4, 271);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(33, 13);
            this.label156.TabIndex = 49;
            this.label156.Text = "GTIN";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(4, 298);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(41, 13);
            this.label157.TabIndex = 50;
            this.label157.Text = "Weight";
            // 
            // nmGtin_pos
            // 
            this.nmGtin_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmGtin_pos.Location = new System.Drawing.Point(267, 274);
            this.nmGtin_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmGtin_pos.Name = "nmGtin_pos";
            this.nmGtin_pos.Size = new System.Drawing.Size(120, 20);
            this.nmGtin_pos.TabIndex = 51;
            this.nmGtin_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmWeight_pos
            // 
            this.nmWeight_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmWeight_pos.Location = new System.Drawing.Point(267, 301);
            this.nmWeight_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmWeight_pos.Name = "nmWeight_pos";
            this.nmWeight_pos.Size = new System.Drawing.Size(120, 20);
            this.nmWeight_pos.TabIndex = 52;
            this.nmWeight_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // tbDefaultWeight_pos
            // 
            this.tbDefaultWeight_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultWeight_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultWeight_pos.Location = new System.Drawing.Point(456, 301);
            this.tbDefaultWeight_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultWeight_pos.Name = "tbDefaultWeight_pos";
            this.tbDefaultWeight_pos.Size = new System.Drawing.Size(454, 20);
            this.tbDefaultWeight_pos.TabIndex = 53;
            // 
            // tbDefaultGtin_pos
            // 
            this.tbDefaultGtin_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultGtin_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultGtin_pos.Location = new System.Drawing.Point(456, 274);
            this.tbDefaultGtin_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultGtin_pos.Name = "tbDefaultGtin_pos";
            this.tbDefaultGtin_pos.Size = new System.Drawing.Size(454, 20);
            this.tbDefaultGtin_pos.TabIndex = 54;
            // 
            // nmDefaultProdSKU_pos
            // 
            this.nmDefaultProdSKU_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nmDefaultProdSKU_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmDefaultProdSKU_pos.Location = new System.Drawing.Point(456, 62);
            this.nmDefaultProdSKU_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.nmDefaultProdSKU_pos.Name = "nmDefaultProdSKU_pos";
            this.nmDefaultProdSKU_pos.Size = new System.Drawing.Size(454, 20);
            this.nmDefaultProdSKU_pos.TabIndex = 55;
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(3, 325);
            this.label179.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(40, 13);
            this.label179.TabIndex = 56;
            this.label179.Text = "Length";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Location = new System.Drawing.Point(3, 352);
            this.label180.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(35, 13);
            this.label180.TabIndex = 57;
            this.label180.Text = "Width";
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Location = new System.Drawing.Point(3, 379);
            this.label181.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(38, 13);
            this.label181.TabIndex = 58;
            this.label181.Text = "Height";
            // 
            // nmLength_pos
            // 
            this.nmLength_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmLength_pos.Location = new System.Drawing.Point(267, 328);
            this.nmLength_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmLength_pos.Name = "nmLength_pos";
            this.nmLength_pos.Size = new System.Drawing.Size(120, 20);
            this.nmLength_pos.TabIndex = 59;
            this.nmLength_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmWidth_pos
            // 
            this.nmWidth_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmWidth_pos.Location = new System.Drawing.Point(267, 355);
            this.nmWidth_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmWidth_pos.Name = "nmWidth_pos";
            this.nmWidth_pos.Size = new System.Drawing.Size(120, 20);
            this.nmWidth_pos.TabIndex = 60;
            this.nmWidth_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmHeight_pos
            // 
            this.nmHeight_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmHeight_pos.Location = new System.Drawing.Point(267, 382);
            this.nmHeight_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmHeight_pos.Name = "nmHeight_pos";
            this.nmHeight_pos.Size = new System.Drawing.Size(120, 20);
            this.nmHeight_pos.TabIndex = 61;
            this.nmHeight_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // tbDefaultLength_pos
            // 
            this.tbDefaultLength_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultLength_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultLength_pos.Location = new System.Drawing.Point(456, 328);
            this.tbDefaultLength_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultLength_pos.Name = "tbDefaultLength_pos";
            this.tbDefaultLength_pos.Size = new System.Drawing.Size(454, 20);
            this.tbDefaultLength_pos.TabIndex = 62;
            // 
            // tbDefaultWidth_pos
            // 
            this.tbDefaultWidth_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultWidth_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultWidth_pos.Location = new System.Drawing.Point(456, 355);
            this.tbDefaultWidth_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultWidth_pos.Name = "tbDefaultWidth_pos";
            this.tbDefaultWidth_pos.Size = new System.Drawing.Size(454, 20);
            this.tbDefaultWidth_pos.TabIndex = 63;
            // 
            // tbDefaultHeight_pos
            // 
            this.tbDefaultHeight_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultHeight_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultHeight_pos.Location = new System.Drawing.Point(456, 382);
            this.tbDefaultHeight_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultHeight_pos.Name = "tbDefaultHeight_pos";
            this.tbDefaultHeight_pos.Size = new System.Drawing.Size(454, 20);
            this.tbDefaultHeight_pos.TabIndex = 64;
            // 
            // label271
            // 
            this.label271.AutoSize = true;
            this.label271.Location = new System.Drawing.Point(3, 86);
            this.label271.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label271.Name = "label271";
            this.label271.Size = new System.Drawing.Size(91, 13);
            this.label271.TabIndex = 68;
            this.label271.Text = "Product Group By";
            // 
            // nmProdGroupBy_pos
            // 
            this.nmProdGroupBy_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdGroupBy_pos.Location = new System.Drawing.Point(267, 89);
            this.nmProdGroupBy_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdGroupBy_pos.Name = "nmProdGroupBy_pos";
            this.nmProdGroupBy_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdGroupBy_pos.TabIndex = 69;
            this.nmProdGroupBy_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // tabSeoMappingPos
            // 
            this.tabSeoMappingPos.Controls.Add(this.tableLayoutPanel11);
            this.tabSeoMappingPos.Location = new System.Drawing.Point(4, 22);
            this.tabSeoMappingPos.Name = "tabSeoMappingPos";
            this.tabSeoMappingPos.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabSeoMappingPos.Size = new System.Drawing.Size(924, 549);
            this.tabSeoMappingPos.TabIndex = 10;
            this.tabSeoMappingPos.Text = "SEO";
            this.tabSeoMappingPos.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel11.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel11.ColumnCount = 3;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this.tbDefaultSeoSearchPage_pos, 2, 4);
            this.tableLayoutPanel11.Controls.Add(this.tbDefaultSeoMetaTitle_pos, 2, 3);
            this.tableLayoutPanel11.Controls.Add(this.label81, 0, 3);
            this.tableLayoutPanel11.Controls.Add(this.tbDefaultSeoMetaDesc_pos, 2, 2);
            this.tableLayoutPanel11.Controls.Add(this.tbDefaultSeoMetaKey_pos, 2, 1);
            this.tableLayoutPanel11.Controls.Add(this.label92, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.label94, 2, 0);
            this.tableLayoutPanel11.Controls.Add(this.label95, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.label96, 0, 2);
            this.tableLayoutPanel11.Controls.Add(this.label97, 0, 4);
            this.tableLayoutPanel11.Controls.Add(this.label98, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.nmSeoMetaKey_pos, 1, 1);
            this.tableLayoutPanel11.Controls.Add(this.nmSeoMetaDesc_pos, 1, 2);
            this.tableLayoutPanel11.Controls.Add(this.nmSeoMetaTitle_pos, 1, 3);
            this.tableLayoutPanel11.Controls.Add(this.nmSeoSearchPage_pos, 1, 4);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(-4, -2);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 6;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(931, 487);
            this.tableLayoutPanel11.TabIndex = 1;
            // 
            // tbDefaultSeoSearchPage_pos
            // 
            this.tbDefaultSeoSearchPage_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultSeoSearchPage_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultSeoSearchPage_pos.Location = new System.Drawing.Point(406, 138);
            this.tbDefaultSeoSearchPage_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultSeoSearchPage_pos.Name = "tbDefaultSeoSearchPage_pos";
            this.tbDefaultSeoSearchPage_pos.Size = new System.Drawing.Size(504, 20);
            this.tbDefaultSeoSearchPage_pos.TabIndex = 34;
            // 
            // tbDefaultSeoMetaTitle_pos
            // 
            this.tbDefaultSeoMetaTitle_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultSeoMetaTitle_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultSeoMetaTitle_pos.Location = new System.Drawing.Point(406, 107);
            this.tbDefaultSeoMetaTitle_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultSeoMetaTitle_pos.Name = "tbDefaultSeoMetaTitle_pos";
            this.tbDefaultSeoMetaTitle_pos.Size = new System.Drawing.Size(504, 20);
            this.tbDefaultSeoMetaTitle_pos.TabIndex = 32;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(4, 104);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(54, 13);
            this.label81.TabIndex = 29;
            this.label81.Text = "Meta Title";
            // 
            // tbDefaultSeoMetaDesc_pos
            // 
            this.tbDefaultSeoMetaDesc_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultSeoMetaDesc_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultSeoMetaDesc_pos.Location = new System.Drawing.Point(406, 76);
            this.tbDefaultSeoMetaDesc_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultSeoMetaDesc_pos.Name = "tbDefaultSeoMetaDesc_pos";
            this.tbDefaultSeoMetaDesc_pos.Size = new System.Drawing.Size(504, 20);
            this.tbDefaultSeoMetaDesc_pos.TabIndex = 28;
            // 
            // tbDefaultSeoMetaKey_pos
            // 
            this.tbDefaultSeoMetaKey_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultSeoMetaKey_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultSeoMetaKey_pos.Location = new System.Drawing.Point(406, 45);
            this.tbDefaultSeoMetaKey_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultSeoMetaKey_pos.Name = "tbDefaultSeoMetaKey_pos";
            this.tbDefaultSeoMetaKey_pos.Size = new System.Drawing.Size(504, 20);
            this.tbDefaultSeoMetaKey_pos.TabIndex = 27;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(4, 1);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(89, 17);
            this.label92.TabIndex = 12;
            this.label92.Text = "Field Name";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(406, 1);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(199, 17);
            this.label94.TabIndex = 22;
            this.label94.Text = "Default Value /+Add Value";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(4, 42);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(80, 13);
            this.label95.TabIndex = 23;
            this.label95.Text = "Meta Keywords";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(4, 73);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(87, 13);
            this.label96.TabIndex = 25;
            this.label96.Text = "Meta Description";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(4, 135);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(144, 26);
            this.label97.TabIndex = 30;
            this.label97.Text = "Search Engine Friendly Page Name";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(155, 1);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(160, 17);
            this.label98.TabIndex = 35;
            this.label98.Text = "Source Column Index";
            // 
            // nmSeoMetaKey_pos
            // 
            this.nmSeoMetaKey_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmSeoMetaKey_pos.Location = new System.Drawing.Point(155, 45);
            this.nmSeoMetaKey_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmSeoMetaKey_pos.Name = "nmSeoMetaKey_pos";
            this.nmSeoMetaKey_pos.Size = new System.Drawing.Size(120, 20);
            this.nmSeoMetaKey_pos.TabIndex = 36;
            this.nmSeoMetaKey_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmSeoMetaDesc_pos
            // 
            this.nmSeoMetaDesc_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmSeoMetaDesc_pos.Location = new System.Drawing.Point(155, 76);
            this.nmSeoMetaDesc_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmSeoMetaDesc_pos.Name = "nmSeoMetaDesc_pos";
            this.nmSeoMetaDesc_pos.Size = new System.Drawing.Size(120, 20);
            this.nmSeoMetaDesc_pos.TabIndex = 37;
            this.nmSeoMetaDesc_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmSeoMetaTitle_pos
            // 
            this.nmSeoMetaTitle_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmSeoMetaTitle_pos.Location = new System.Drawing.Point(155, 107);
            this.nmSeoMetaTitle_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmSeoMetaTitle_pos.Name = "nmSeoMetaTitle_pos";
            this.nmSeoMetaTitle_pos.Size = new System.Drawing.Size(120, 20);
            this.nmSeoMetaTitle_pos.TabIndex = 38;
            this.nmSeoMetaTitle_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmSeoSearchPage_pos
            // 
            this.nmSeoSearchPage_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmSeoSearchPage_pos.Location = new System.Drawing.Point(155, 138);
            this.nmSeoSearchPage_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmSeoSearchPage_pos.Name = "nmSeoSearchPage_pos";
            this.nmSeoSearchPage_pos.Size = new System.Drawing.Size(120, 20);
            this.nmSeoSearchPage_pos.TabIndex = 39;
            this.nmSeoSearchPage_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // tabCatMappingPos
            // 
            this.tabCatMappingPos.Controls.Add(this.tableLayoutPanel12);
            this.tabCatMappingPos.Location = new System.Drawing.Point(4, 22);
            this.tabCatMappingPos.Name = "tabCatMappingPos";
            this.tabCatMappingPos.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabCatMappingPos.Size = new System.Drawing.Size(924, 549);
            this.tabCatMappingPos.TabIndex = 11;
            this.tabCatMappingPos.Text = "Category";
            this.tabCatMappingPos.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel12.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel12.ColumnCount = 5;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 179F));
            this.tableLayoutPanel12.Controls.Add(this.label93, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.label100, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.label101, 2, 0);
            this.tableLayoutPanel12.Controls.Add(this.tbDefaultCategory_pos, 2, 1);
            this.tableLayoutPanel12.Controls.Add(this.label102, 3, 0);
            this.tableLayoutPanel12.Controls.Add(this.tbCategoryDelimeter_pos, 3, 1);
            this.tableLayoutPanel12.Controls.Add(this.label103, 0, 2);
            this.tableLayoutPanel12.Controls.Add(this.label104, 0, 3);
            this.tableLayoutPanel12.Controls.Add(this.label105, 0, 4);
            this.tableLayoutPanel12.Controls.Add(this.label106, 0, 5);
            this.tableLayoutPanel12.Controls.Add(this.tbDefaultCategory1_pos, 2, 2);
            this.tableLayoutPanel12.Controls.Add(this.tbDefaultCategory2_pos, 2, 3);
            this.tableLayoutPanel12.Controls.Add(this.tbDefaultCategory3_pos, 2, 4);
            this.tableLayoutPanel12.Controls.Add(this.tbDefaultCategory4_pos, 2, 5);
            this.tableLayoutPanel12.Controls.Add(this.label99, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.nmCategory_pos, 1, 1);
            this.tableLayoutPanel12.Controls.Add(this.nmCategory1_pos, 1, 2);
            this.tableLayoutPanel12.Controls.Add(this.nmCategory2_pos, 1, 3);
            this.tableLayoutPanel12.Controls.Add(this.nmCategory3_pos, 1, 4);
            this.tableLayoutPanel12.Controls.Add(this.nmCategory4_pos, 1, 5);
            this.tableLayoutPanel12.Controls.Add(this.label144, 4, 0);
            this.tableLayoutPanel12.Controls.Add(this.chCatId_pos, 4, 1);
            this.tableLayoutPanel12.Controls.Add(this.chCat1Id_pos, 4, 2);
            this.tableLayoutPanel12.Controls.Add(this.chCat2Id_pos, 4, 3);
            this.tableLayoutPanel12.Controls.Add(this.chCat3Id_pos, 4, 4);
            this.tableLayoutPanel12.Controls.Add(this.chCat4Id_pos, 4, 5);
            this.tableLayoutPanel12.Location = new System.Drawing.Point(-4, -2);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 7;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(934, 509);
            this.tableLayoutPanel12.TabIndex = 2;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(4, 1);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(89, 17);
            this.label93.TabIndex = 12;
            this.label93.Text = "Field Name";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(4, 42);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(68, 13);
            this.label100.TabIndex = 23;
            this.label100.Text = "Category(ies)";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.Location = new System.Drawing.Point(356, 1);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(199, 17);
            this.label101.TabIndex = 22;
            this.label101.Text = "Default Value /+Add Value";
            // 
            // tbDefaultCategory_pos
            // 
            this.tbDefaultCategory_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCategory_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCategory_pos.Location = new System.Drawing.Point(356, 45);
            this.tbDefaultCategory_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCategory_pos.Name = "tbDefaultCategory_pos";
            this.tbDefaultCategory_pos.Size = new System.Drawing.Size(247, 20);
            this.tbDefaultCategory_pos.TabIndex = 27;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.Location = new System.Drawing.Point(627, 1);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(101, 34);
            this.label102.TabIndex = 28;
            this.label102.Text = "Delimiter To Split";
            // 
            // tbCategoryDelimeter_pos
            // 
            this.tbCategoryDelimeter_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCategoryDelimeter_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCategoryDelimeter_pos.Location = new System.Drawing.Point(627, 45);
            this.tbCategoryDelimeter_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCategoryDelimeter_pos.Name = "tbCategoryDelimeter_pos";
            this.tbCategoryDelimeter_pos.Size = new System.Drawing.Size(106, 20);
            this.tbCategoryDelimeter_pos.TabIndex = 29;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(4, 73);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(55, 13);
            this.label103.TabIndex = 30;
            this.label103.Text = "Category1";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(4, 104);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(55, 13);
            this.label104.TabIndex = 31;
            this.label104.Text = "Category2";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(4, 135);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(55, 13);
            this.label105.TabIndex = 32;
            this.label105.Text = "Category3";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(4, 166);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(55, 13);
            this.label106.TabIndex = 33;
            this.label106.Text = "Category4";
            // 
            // tbDefaultCategory1_pos
            // 
            this.tbDefaultCategory1_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCategory1_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCategory1_pos.Location = new System.Drawing.Point(356, 76);
            this.tbDefaultCategory1_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCategory1_pos.Name = "tbDefaultCategory1_pos";
            this.tbDefaultCategory1_pos.Size = new System.Drawing.Size(247, 20);
            this.tbDefaultCategory1_pos.TabIndex = 38;
            // 
            // tbDefaultCategory2_pos
            // 
            this.tbDefaultCategory2_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCategory2_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCategory2_pos.Location = new System.Drawing.Point(356, 107);
            this.tbDefaultCategory2_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCategory2_pos.Name = "tbDefaultCategory2_pos";
            this.tbDefaultCategory2_pos.Size = new System.Drawing.Size(247, 20);
            this.tbDefaultCategory2_pos.TabIndex = 39;
            // 
            // tbDefaultCategory3_pos
            // 
            this.tbDefaultCategory3_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCategory3_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCategory3_pos.Location = new System.Drawing.Point(356, 138);
            this.tbDefaultCategory3_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCategory3_pos.Name = "tbDefaultCategory3_pos";
            this.tbDefaultCategory3_pos.Size = new System.Drawing.Size(247, 20);
            this.tbDefaultCategory3_pos.TabIndex = 40;
            // 
            // tbDefaultCategory4_pos
            // 
            this.tbDefaultCategory4_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCategory4_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCategory4_pos.Location = new System.Drawing.Point(356, 169);
            this.tbDefaultCategory4_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCategory4_pos.Name = "tbDefaultCategory4_pos";
            this.tbDefaultCategory4_pos.Size = new System.Drawing.Size(247, 20);
            this.tbDefaultCategory4_pos.TabIndex = 41;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(155, 1);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(160, 17);
            this.label99.TabIndex = 42;
            this.label99.Text = "Source Column Index";
            // 
            // nmCategory_pos
            // 
            this.nmCategory_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmCategory_pos.Location = new System.Drawing.Point(155, 45);
            this.nmCategory_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmCategory_pos.Name = "nmCategory_pos";
            this.nmCategory_pos.Size = new System.Drawing.Size(120, 20);
            this.nmCategory_pos.TabIndex = 43;
            this.nmCategory_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmCategory1_pos
            // 
            this.nmCategory1_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmCategory1_pos.Location = new System.Drawing.Point(155, 76);
            this.nmCategory1_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmCategory1_pos.Name = "nmCategory1_pos";
            this.nmCategory1_pos.Size = new System.Drawing.Size(120, 20);
            this.nmCategory1_pos.TabIndex = 44;
            this.nmCategory1_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmCategory2_pos
            // 
            this.nmCategory2_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmCategory2_pos.Location = new System.Drawing.Point(155, 107);
            this.nmCategory2_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmCategory2_pos.Name = "nmCategory2_pos";
            this.nmCategory2_pos.Size = new System.Drawing.Size(120, 20);
            this.nmCategory2_pos.TabIndex = 45;
            this.nmCategory2_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmCategory3_pos
            // 
            this.nmCategory3_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmCategory3_pos.Location = new System.Drawing.Point(155, 138);
            this.nmCategory3_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmCategory3_pos.Name = "nmCategory3_pos";
            this.nmCategory3_pos.Size = new System.Drawing.Size(120, 20);
            this.nmCategory3_pos.TabIndex = 46;
            this.nmCategory3_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmCategory4_pos
            // 
            this.nmCategory4_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmCategory4_pos.Location = new System.Drawing.Point(155, 169);
            this.nmCategory4_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmCategory4_pos.Name = "nmCategory4_pos";
            this.nmCategory4_pos.Size = new System.Drawing.Size(120, 20);
            this.nmCategory4_pos.TabIndex = 47;
            this.nmCategory4_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label144.Location = new System.Drawing.Point(757, 1);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(67, 17);
            this.label144.TabIndex = 48;
            this.label144.Text = "Id Value";
            // 
            // chCatId_pos
            // 
            this.chCatId_pos.AutoSize = true;
            this.chCatId_pos.Location = new System.Drawing.Point(757, 45);
            this.chCatId_pos.Name = "chCatId_pos";
            this.chCatId_pos.Size = new System.Drawing.Size(15, 14);
            this.chCatId_pos.TabIndex = 49;
            this.chCatId_pos.UseVisualStyleBackColor = true;
            // 
            // chCat1Id_pos
            // 
            this.chCat1Id_pos.AutoSize = true;
            this.chCat1Id_pos.Location = new System.Drawing.Point(757, 76);
            this.chCat1Id_pos.Name = "chCat1Id_pos";
            this.chCat1Id_pos.Size = new System.Drawing.Size(15, 14);
            this.chCat1Id_pos.TabIndex = 50;
            this.chCat1Id_pos.UseVisualStyleBackColor = true;
            // 
            // chCat2Id_pos
            // 
            this.chCat2Id_pos.AutoSize = true;
            this.chCat2Id_pos.Location = new System.Drawing.Point(757, 107);
            this.chCat2Id_pos.Name = "chCat2Id_pos";
            this.chCat2Id_pos.Size = new System.Drawing.Size(15, 14);
            this.chCat2Id_pos.TabIndex = 51;
            this.chCat2Id_pos.UseVisualStyleBackColor = true;
            // 
            // chCat3Id_pos
            // 
            this.chCat3Id_pos.AutoSize = true;
            this.chCat3Id_pos.Location = new System.Drawing.Point(757, 138);
            this.chCat3Id_pos.Name = "chCat3Id_pos";
            this.chCat3Id_pos.Size = new System.Drawing.Size(15, 14);
            this.chCat3Id_pos.TabIndex = 52;
            this.chCat3Id_pos.UseVisualStyleBackColor = true;
            // 
            // chCat4Id_pos
            // 
            this.chCat4Id_pos.AutoSize = true;
            this.chCat4Id_pos.Location = new System.Drawing.Point(757, 169);
            this.chCat4Id_pos.Name = "chCat4Id_pos";
            this.chCat4Id_pos.Size = new System.Drawing.Size(15, 14);
            this.chCat4Id_pos.TabIndex = 53;
            this.chCat4Id_pos.UseVisualStyleBackColor = true;
            // 
            // tabAccessRolesMappingPos
            // 
            this.tabAccessRolesMappingPos.Controls.Add(this.tableLayoutPanel18);
            this.tabAccessRolesMappingPos.Location = new System.Drawing.Point(4, 22);
            this.tabAccessRolesMappingPos.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabAccessRolesMappingPos.Name = "tabAccessRolesMappingPos";
            this.tabAccessRolesMappingPos.Size = new System.Drawing.Size(924, 549);
            this.tabAccessRolesMappingPos.TabIndex = 17;
            this.tabAccessRolesMappingPos.Text = "Access Roles";
            this.tabAccessRolesMappingPos.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel18.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel18.ColumnCount = 5;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 179F));
            this.tableLayoutPanel18.Controls.Add(this.label169, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.label170, 0, 1);
            this.tableLayoutPanel18.Controls.Add(this.label171, 2, 0);
            this.tableLayoutPanel18.Controls.Add(this.tbDefaultCustomerRole_pos, 2, 1);
            this.tableLayoutPanel18.Controls.Add(this.label172, 3, 0);
            this.tableLayoutPanel18.Controls.Add(this.tbCustomerRoleDelimiter_pos, 3, 1);
            this.tableLayoutPanel18.Controls.Add(this.label173, 0, 2);
            this.tableLayoutPanel18.Controls.Add(this.label174, 0, 3);
            this.tableLayoutPanel18.Controls.Add(this.label175, 0, 4);
            this.tableLayoutPanel18.Controls.Add(this.label176, 0, 5);
            this.tableLayoutPanel18.Controls.Add(this.tbDefaultCustomerRole1_pos, 2, 2);
            this.tableLayoutPanel18.Controls.Add(this.tbDefaultCustomerRole2_pos, 2, 3);
            this.tableLayoutPanel18.Controls.Add(this.tbDefaultCustomerRole3_pos, 2, 4);
            this.tableLayoutPanel18.Controls.Add(this.tbDefaultCustomerRole4_pos, 2, 5);
            this.tableLayoutPanel18.Controls.Add(this.label177, 1, 0);
            this.tableLayoutPanel18.Controls.Add(this.nmCustomerRole_pos, 1, 1);
            this.tableLayoutPanel18.Controls.Add(this.nmCustomerRole1_pos, 1, 2);
            this.tableLayoutPanel18.Controls.Add(this.nmCustomerRole2_pos, 1, 3);
            this.tableLayoutPanel18.Controls.Add(this.nmCustomerRole3_pos, 1, 4);
            this.tableLayoutPanel18.Controls.Add(this.nmCustomerRole4_pos, 1, 5);
            this.tableLayoutPanel18.Controls.Add(this.label178, 4, 0);
            this.tableLayoutPanel18.Controls.Add(this.chCustomerRoleId_pos, 4, 1);
            this.tableLayoutPanel18.Controls.Add(this.chCustomerRoleId1_pos, 4, 2);
            this.tableLayoutPanel18.Controls.Add(this.chCustomerRoleId2_pos, 4, 3);
            this.tableLayoutPanel18.Controls.Add(this.chCustomerRoleId3_pos, 4, 4);
            this.tableLayoutPanel18.Controls.Add(this.chCustomerRoleId4_pos, 4, 5);
            this.tableLayoutPanel18.Location = new System.Drawing.Point(-3, -2);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 7;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(930, 483);
            this.tableLayoutPanel18.TabIndex = 3;
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label169.Location = new System.Drawing.Point(4, 1);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(89, 17);
            this.label169.TabIndex = 12;
            this.label169.Text = "Field Name";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(4, 42);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(87, 13);
            this.label170.TabIndex = 23;
            this.label170.Text = "Customer Role(s)";
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label171.Location = new System.Drawing.Point(356, 1);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(199, 17);
            this.label171.TabIndex = 22;
            this.label171.Text = "Default Value /+Add Value";
            // 
            // tbDefaultCustomerRole_pos
            // 
            this.tbDefaultCustomerRole_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCustomerRole_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCustomerRole_pos.Location = new System.Drawing.Point(356, 45);
            this.tbDefaultCustomerRole_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCustomerRole_pos.Name = "tbDefaultCustomerRole_pos";
            this.tbDefaultCustomerRole_pos.Size = new System.Drawing.Size(243, 20);
            this.tbDefaultCustomerRole_pos.TabIndex = 27;
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label172.Location = new System.Drawing.Point(623, 1);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(101, 34);
            this.label172.TabIndex = 28;
            this.label172.Text = "Delimiter To Split";
            // 
            // tbCustomerRoleDelimiter_pos
            // 
            this.tbCustomerRoleDelimiter_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCustomerRoleDelimiter_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCustomerRoleDelimiter_pos.Location = new System.Drawing.Point(623, 45);
            this.tbCustomerRoleDelimiter_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbCustomerRoleDelimiter_pos.Name = "tbCustomerRoleDelimiter_pos";
            this.tbCustomerRoleDelimiter_pos.Size = new System.Drawing.Size(106, 20);
            this.tbCustomerRoleDelimiter_pos.TabIndex = 29;
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(4, 73);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(82, 13);
            this.label173.TabIndex = 30;
            this.label173.Text = "Customer Role1";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(4, 104);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(82, 13);
            this.label174.TabIndex = 31;
            this.label174.Text = "Customer Role2";
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(4, 135);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(82, 13);
            this.label175.TabIndex = 32;
            this.label175.Text = "Customer Role3";
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(4, 166);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(82, 13);
            this.label176.TabIndex = 33;
            this.label176.Text = "Customer Role4";
            // 
            // tbDefaultCustomerRole1_pos
            // 
            this.tbDefaultCustomerRole1_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCustomerRole1_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCustomerRole1_pos.Location = new System.Drawing.Point(356, 76);
            this.tbDefaultCustomerRole1_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCustomerRole1_pos.Name = "tbDefaultCustomerRole1_pos";
            this.tbDefaultCustomerRole1_pos.Size = new System.Drawing.Size(243, 20);
            this.tbDefaultCustomerRole1_pos.TabIndex = 38;
            // 
            // tbDefaultCustomerRole2_pos
            // 
            this.tbDefaultCustomerRole2_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCustomerRole2_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCustomerRole2_pos.Location = new System.Drawing.Point(356, 107);
            this.tbDefaultCustomerRole2_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCustomerRole2_pos.Name = "tbDefaultCustomerRole2_pos";
            this.tbDefaultCustomerRole2_pos.Size = new System.Drawing.Size(243, 20);
            this.tbDefaultCustomerRole2_pos.TabIndex = 39;
            // 
            // tbDefaultCustomerRole3_pos
            // 
            this.tbDefaultCustomerRole3_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCustomerRole3_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCustomerRole3_pos.Location = new System.Drawing.Point(356, 138);
            this.tbDefaultCustomerRole3_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCustomerRole3_pos.Name = "tbDefaultCustomerRole3_pos";
            this.tbDefaultCustomerRole3_pos.Size = new System.Drawing.Size(243, 20);
            this.tbDefaultCustomerRole3_pos.TabIndex = 40;
            // 
            // tbDefaultCustomerRole4_pos
            // 
            this.tbDefaultCustomerRole4_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultCustomerRole4_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultCustomerRole4_pos.Location = new System.Drawing.Point(356, 169);
            this.tbDefaultCustomerRole4_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultCustomerRole4_pos.Name = "tbDefaultCustomerRole4_pos";
            this.tbDefaultCustomerRole4_pos.Size = new System.Drawing.Size(243, 20);
            this.tbDefaultCustomerRole4_pos.TabIndex = 41;
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label177.Location = new System.Drawing.Point(155, 1);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(160, 17);
            this.label177.TabIndex = 42;
            this.label177.Text = "Source Column Index";
            // 
            // nmCustomerRole_pos
            // 
            this.nmCustomerRole_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmCustomerRole_pos.Location = new System.Drawing.Point(155, 45);
            this.nmCustomerRole_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmCustomerRole_pos.Name = "nmCustomerRole_pos";
            this.nmCustomerRole_pos.Size = new System.Drawing.Size(120, 20);
            this.nmCustomerRole_pos.TabIndex = 43;
            this.nmCustomerRole_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmCustomerRole1_pos
            // 
            this.nmCustomerRole1_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmCustomerRole1_pos.Location = new System.Drawing.Point(155, 76);
            this.nmCustomerRole1_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmCustomerRole1_pos.Name = "nmCustomerRole1_pos";
            this.nmCustomerRole1_pos.Size = new System.Drawing.Size(120, 20);
            this.nmCustomerRole1_pos.TabIndex = 44;
            this.nmCustomerRole1_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmCustomerRole2_pos
            // 
            this.nmCustomerRole2_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmCustomerRole2_pos.Location = new System.Drawing.Point(155, 107);
            this.nmCustomerRole2_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmCustomerRole2_pos.Name = "nmCustomerRole2_pos";
            this.nmCustomerRole2_pos.Size = new System.Drawing.Size(120, 20);
            this.nmCustomerRole2_pos.TabIndex = 45;
            this.nmCustomerRole2_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmCustomerRole3_pos
            // 
            this.nmCustomerRole3_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmCustomerRole3_pos.Location = new System.Drawing.Point(155, 138);
            this.nmCustomerRole3_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmCustomerRole3_pos.Name = "nmCustomerRole3_pos";
            this.nmCustomerRole3_pos.Size = new System.Drawing.Size(120, 20);
            this.nmCustomerRole3_pos.TabIndex = 46;
            this.nmCustomerRole3_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmCustomerRole4_pos
            // 
            this.nmCustomerRole4_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmCustomerRole4_pos.Location = new System.Drawing.Point(155, 169);
            this.nmCustomerRole4_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmCustomerRole4_pos.Name = "nmCustomerRole4_pos";
            this.nmCustomerRole4_pos.Size = new System.Drawing.Size(120, 20);
            this.nmCustomerRole4_pos.TabIndex = 47;
            this.nmCustomerRole4_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label178.Location = new System.Drawing.Point(753, 1);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(67, 17);
            this.label178.TabIndex = 48;
            this.label178.Text = "Id Value";
            // 
            // chCustomerRoleId_pos
            // 
            this.chCustomerRoleId_pos.AutoSize = true;
            this.chCustomerRoleId_pos.Location = new System.Drawing.Point(753, 45);
            this.chCustomerRoleId_pos.Name = "chCustomerRoleId_pos";
            this.chCustomerRoleId_pos.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRoleId_pos.TabIndex = 49;
            this.chCustomerRoleId_pos.UseVisualStyleBackColor = true;
            // 
            // chCustomerRoleId1_pos
            // 
            this.chCustomerRoleId1_pos.AutoSize = true;
            this.chCustomerRoleId1_pos.Location = new System.Drawing.Point(753, 76);
            this.chCustomerRoleId1_pos.Name = "chCustomerRoleId1_pos";
            this.chCustomerRoleId1_pos.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRoleId1_pos.TabIndex = 50;
            this.chCustomerRoleId1_pos.UseVisualStyleBackColor = true;
            // 
            // chCustomerRoleId2_pos
            // 
            this.chCustomerRoleId2_pos.AutoSize = true;
            this.chCustomerRoleId2_pos.Location = new System.Drawing.Point(753, 107);
            this.chCustomerRoleId2_pos.Name = "chCustomerRoleId2_pos";
            this.chCustomerRoleId2_pos.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRoleId2_pos.TabIndex = 51;
            this.chCustomerRoleId2_pos.UseVisualStyleBackColor = true;
            // 
            // chCustomerRoleId3_pos
            // 
            this.chCustomerRoleId3_pos.AutoSize = true;
            this.chCustomerRoleId3_pos.Location = new System.Drawing.Point(753, 138);
            this.chCustomerRoleId3_pos.Name = "chCustomerRoleId3_pos";
            this.chCustomerRoleId3_pos.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRoleId3_pos.TabIndex = 52;
            this.chCustomerRoleId3_pos.UseVisualStyleBackColor = true;
            // 
            // chCustomerRoleId4_pos
            // 
            this.chCustomerRoleId4_pos.AutoSize = true;
            this.chCustomerRoleId4_pos.Location = new System.Drawing.Point(753, 169);
            this.chCustomerRoleId4_pos.Name = "chCustomerRoleId4_pos";
            this.chCustomerRoleId4_pos.Size = new System.Drawing.Size(15, 14);
            this.chCustomerRoleId4_pos.TabIndex = 53;
            this.chCustomerRoleId4_pos.UseVisualStyleBackColor = true;
            // 
            // tabManufMappingPos
            // 
            this.tabManufMappingPos.Controls.Add(this.tableLayoutPanel13);
            this.tabManufMappingPos.Location = new System.Drawing.Point(4, 22);
            this.tabManufMappingPos.Name = "tabManufMappingPos";
            this.tabManufMappingPos.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabManufMappingPos.Size = new System.Drawing.Size(924, 549);
            this.tabManufMappingPos.TabIndex = 12;
            this.tabManufMappingPos.Text = "Manufacturer&Vendor";
            this.tabManufMappingPos.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel13.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel13.ColumnCount = 4;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 179F));
            this.tableLayoutPanel13.Controls.Add(this.nmVendor_pos, 0, 2);
            this.tableLayoutPanel13.Controls.Add(this.label146, 0, 2);
            this.tableLayoutPanel13.Controls.Add(this.tbDefaultManufacturer_pos, 2, 1);
            this.tableLayoutPanel13.Controls.Add(this.label107, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.label109, 2, 0);
            this.tableLayoutPanel13.Controls.Add(this.label110, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.nmManufacturer_pos, 1, 1);
            this.tableLayoutPanel13.Controls.Add(this.label108, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.label145, 3, 0);
            this.tableLayoutPanel13.Controls.Add(this.tbDefaultVendor_pos, 2, 2);
            this.tableLayoutPanel13.Controls.Add(this.chManufacturerId_pos, 3, 1);
            this.tableLayoutPanel13.Controls.Add(this.chVendorId_pos, 3, 2);
            this.tableLayoutPanel13.Location = new System.Drawing.Point(1, -2);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 3;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(919, 526);
            this.tableLayoutPanel13.TabIndex = 2;
            // 
            // nmVendor_pos
            // 
            this.nmVendor_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmVendor_pos.Location = new System.Drawing.Point(155, 76);
            this.nmVendor_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmVendor_pos.Name = "nmVendor_pos";
            this.nmVendor_pos.Size = new System.Drawing.Size(120, 20);
            this.nmVendor_pos.TabIndex = 52;
            this.nmVendor_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(4, 73);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(41, 13);
            this.label146.TabIndex = 50;
            this.label146.Text = "Vendor";
            // 
            // tbDefaultManufacturer_pos
            // 
            this.tbDefaultManufacturer_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultManufacturer_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultManufacturer_pos.Location = new System.Drawing.Point(406, 45);
            this.tbDefaultManufacturer_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultManufacturer_pos.Name = "tbDefaultManufacturer_pos";
            this.tbDefaultManufacturer_pos.Size = new System.Drawing.Size(312, 20);
            this.tbDefaultManufacturer_pos.TabIndex = 27;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.Location = new System.Drawing.Point(4, 1);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(89, 17);
            this.label107.TabIndex = 12;
            this.label107.Text = "Field Name";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.Location = new System.Drawing.Point(406, 1);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(199, 17);
            this.label109.TabIndex = 22;
            this.label109.Text = "Default Value /+Add Value";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(4, 42);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(70, 13);
            this.label110.TabIndex = 23;
            this.label110.Text = "Manufacturer";
            // 
            // nmManufacturer_pos
            // 
            this.nmManufacturer_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmManufacturer_pos.Location = new System.Drawing.Point(155, 45);
            this.nmManufacturer_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmManufacturer_pos.Name = "nmManufacturer_pos";
            this.nmManufacturer_pos.Size = new System.Drawing.Size(120, 20);
            this.nmManufacturer_pos.TabIndex = 44;
            this.nmManufacturer_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.Location = new System.Drawing.Point(155, 1);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(160, 17);
            this.label108.TabIndex = 45;
            this.label108.Text = "Source Column Index";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label145.Location = new System.Drawing.Point(742, 1);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(67, 17);
            this.label145.TabIndex = 49;
            this.label145.Text = "Id Value";
            // 
            // tbDefaultVendor_pos
            // 
            this.tbDefaultVendor_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultVendor_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultVendor_pos.Location = new System.Drawing.Point(406, 76);
            this.tbDefaultVendor_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultVendor_pos.Name = "tbDefaultVendor_pos";
            this.tbDefaultVendor_pos.Size = new System.Drawing.Size(312, 20);
            this.tbDefaultVendor_pos.TabIndex = 51;
            // 
            // chManufacturerId_pos
            // 
            this.chManufacturerId_pos.AutoSize = true;
            this.chManufacturerId_pos.Location = new System.Drawing.Point(742, 45);
            this.chManufacturerId_pos.Name = "chManufacturerId_pos";
            this.chManufacturerId_pos.Size = new System.Drawing.Size(15, 14);
            this.chManufacturerId_pos.TabIndex = 53;
            this.chManufacturerId_pos.UseVisualStyleBackColor = true;
            // 
            // chVendorId_pos
            // 
            this.chVendorId_pos.AutoSize = true;
            this.chVendorId_pos.Location = new System.Drawing.Point(742, 76);
            this.chVendorId_pos.Name = "chVendorId_pos";
            this.chVendorId_pos.Size = new System.Drawing.Size(15, 14);
            this.chVendorId_pos.TabIndex = 54;
            this.chVendorId_pos.UseVisualStyleBackColor = true;
            // 
            // tabFilterMappingPos
            // 
            this.tabFilterMappingPos.Controls.Add(this.tableLayoutPanel16);
            this.tabFilterMappingPos.Location = new System.Drawing.Point(4, 22);
            this.tabFilterMappingPos.Name = "tabFilterMappingPos";
            this.tabFilterMappingPos.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabFilterMappingPos.Size = new System.Drawing.Size(924, 549);
            this.tabFilterMappingPos.TabIndex = 15;
            this.tabFilterMappingPos.Text = "Filters";
            this.tabFilterMappingPos.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel16.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel16.ColumnCount = 3;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.50218F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 212F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.49782F));
            this.tableLayoutPanel16.Controls.Add(this.label129, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.label131, 1, 0);
            this.tableLayoutPanel16.Controls.Add(this.label133, 0, 1);
            this.tableLayoutPanel16.Controls.Add(this.tbFilterName_pos, 1, 1);
            this.tableLayoutPanel16.Controls.Add(this.label134, 0, 2);
            this.tableLayoutPanel16.Controls.Add(this.label135, 0, 3);
            this.tableLayoutPanel16.Controls.Add(this.label136, 0, 4);
            this.tableLayoutPanel16.Controls.Add(this.label137, 0, 5);
            this.tableLayoutPanel16.Controls.Add(this.label138, 0, 6);
            this.tableLayoutPanel16.Controls.Add(this.label139, 0, 7);
            this.tableLayoutPanel16.Controls.Add(this.label140, 0, 8);
            this.tableLayoutPanel16.Controls.Add(this.label141, 0, 9);
            this.tableLayoutPanel16.Controls.Add(this.label142, 0, 10);
            this.tableLayoutPanel16.Controls.Add(this.tbFilter1Name_pos, 1, 2);
            this.tableLayoutPanel16.Controls.Add(this.tbFilter2Name_pos, 1, 3);
            this.tableLayoutPanel16.Controls.Add(this.tbFilter3Name_pos, 1, 4);
            this.tableLayoutPanel16.Controls.Add(this.tbFilter4Name_pos, 1, 5);
            this.tableLayoutPanel16.Controls.Add(this.tbFilter5Name_pos, 1, 6);
            this.tableLayoutPanel16.Controls.Add(this.tbFilter6Name_pos, 1, 7);
            this.tableLayoutPanel16.Controls.Add(this.tbFilter7Name_pos, 1, 8);
            this.tableLayoutPanel16.Controls.Add(this.tbFilter8Name_pos, 1, 9);
            this.tableLayoutPanel16.Controls.Add(this.tbFilter9Name_pos, 1, 10);
            this.tableLayoutPanel16.Controls.Add(this.nmFilterValue_pos, 2, 1);
            this.tableLayoutPanel16.Controls.Add(this.nmFilter1Value_pos, 2, 2);
            this.tableLayoutPanel16.Controls.Add(this.nmFilter2Value_pos, 2, 3);
            this.tableLayoutPanel16.Controls.Add(this.nmFilter3Value_pos, 2, 4);
            this.tableLayoutPanel16.Controls.Add(this.nmFilter4Value_pos, 2, 5);
            this.tableLayoutPanel16.Controls.Add(this.nmFilter5Value_pos, 2, 6);
            this.tableLayoutPanel16.Controls.Add(this.nmFilter6Value_pos, 2, 7);
            this.tableLayoutPanel16.Controls.Add(this.nmFilter7Value_pos, 2, 8);
            this.tableLayoutPanel16.Controls.Add(this.nmFilter8Value_pos, 2, 9);
            this.tableLayoutPanel16.Controls.Add(this.nmFilter9Value_pos, 2, 10);
            this.tableLayoutPanel16.Controls.Add(this.label132, 2, 0);
            this.tableLayoutPanel16.Location = new System.Drawing.Point(-4, -2);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 13;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(931, 480);
            this.tableLayoutPanel16.TabIndex = 3;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label129.Location = new System.Drawing.Point(4, 1);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(89, 17);
            this.label129.TabIndex = 12;
            this.label129.Text = "Field Name";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label131.Location = new System.Drawing.Point(237, 1);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(91, 17);
            this.label131.TabIndex = 13;
            this.label131.Text = "Filter Name";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(4, 42);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(29, 13);
            this.label133.TabIndex = 23;
            this.label133.Text = "Filter";
            // 
            // tbFilterName_pos
            // 
            this.tbFilterName_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterName_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilterName_pos.Location = new System.Drawing.Point(237, 45);
            this.tbFilterName_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilterName_pos.Name = "tbFilterName_pos";
            this.tbFilterName_pos.Size = new System.Drawing.Size(189, 20);
            this.tbFilterName_pos.TabIndex = 24;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(4, 73);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(35, 13);
            this.label134.TabIndex = 28;
            this.label134.Text = "Filter1";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(4, 104);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(35, 13);
            this.label135.TabIndex = 29;
            this.label135.Text = "Filter2";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(4, 135);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(35, 13);
            this.label136.TabIndex = 30;
            this.label136.Text = "Filter3";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(4, 166);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(35, 13);
            this.label137.TabIndex = 31;
            this.label137.Text = "Filter4";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(4, 197);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(35, 13);
            this.label138.TabIndex = 32;
            this.label138.Text = "Filter5";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(4, 228);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(35, 13);
            this.label139.TabIndex = 33;
            this.label139.Text = "Filter6";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(4, 259);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(35, 13);
            this.label140.TabIndex = 34;
            this.label140.Text = "Filter7";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(4, 290);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(35, 13);
            this.label141.TabIndex = 35;
            this.label141.Text = "Filter8";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(4, 321);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(35, 13);
            this.label142.TabIndex = 36;
            this.label142.Text = "Filter9";
            // 
            // tbFilter1Name_pos
            // 
            this.tbFilter1Name_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter1Name_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter1Name_pos.Location = new System.Drawing.Point(237, 76);
            this.tbFilter1Name_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter1Name_pos.Name = "tbFilter1Name_pos";
            this.tbFilter1Name_pos.Size = new System.Drawing.Size(189, 20);
            this.tbFilter1Name_pos.TabIndex = 37;
            // 
            // tbFilter2Name_pos
            // 
            this.tbFilter2Name_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter2Name_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter2Name_pos.Location = new System.Drawing.Point(237, 107);
            this.tbFilter2Name_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter2Name_pos.Name = "tbFilter2Name_pos";
            this.tbFilter2Name_pos.Size = new System.Drawing.Size(189, 20);
            this.tbFilter2Name_pos.TabIndex = 38;
            // 
            // tbFilter3Name_pos
            // 
            this.tbFilter3Name_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter3Name_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter3Name_pos.Location = new System.Drawing.Point(237, 138);
            this.tbFilter3Name_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter3Name_pos.Name = "tbFilter3Name_pos";
            this.tbFilter3Name_pos.Size = new System.Drawing.Size(189, 20);
            this.tbFilter3Name_pos.TabIndex = 39;
            // 
            // tbFilter4Name_pos
            // 
            this.tbFilter4Name_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter4Name_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter4Name_pos.Location = new System.Drawing.Point(237, 169);
            this.tbFilter4Name_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter4Name_pos.Name = "tbFilter4Name_pos";
            this.tbFilter4Name_pos.Size = new System.Drawing.Size(189, 20);
            this.tbFilter4Name_pos.TabIndex = 40;
            // 
            // tbFilter5Name_pos
            // 
            this.tbFilter5Name_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter5Name_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter5Name_pos.Location = new System.Drawing.Point(237, 200);
            this.tbFilter5Name_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter5Name_pos.Name = "tbFilter5Name_pos";
            this.tbFilter5Name_pos.Size = new System.Drawing.Size(189, 20);
            this.tbFilter5Name_pos.TabIndex = 41;
            // 
            // tbFilter6Name_pos
            // 
            this.tbFilter6Name_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter6Name_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter6Name_pos.Location = new System.Drawing.Point(237, 231);
            this.tbFilter6Name_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter6Name_pos.Name = "tbFilter6Name_pos";
            this.tbFilter6Name_pos.Size = new System.Drawing.Size(189, 20);
            this.tbFilter6Name_pos.TabIndex = 42;
            // 
            // tbFilter7Name_pos
            // 
            this.tbFilter7Name_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter7Name_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter7Name_pos.Location = new System.Drawing.Point(237, 262);
            this.tbFilter7Name_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter7Name_pos.Name = "tbFilter7Name_pos";
            this.tbFilter7Name_pos.Size = new System.Drawing.Size(189, 20);
            this.tbFilter7Name_pos.TabIndex = 43;
            // 
            // tbFilter8Name_pos
            // 
            this.tbFilter8Name_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter8Name_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter8Name_pos.Location = new System.Drawing.Point(237, 293);
            this.tbFilter8Name_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter8Name_pos.Name = "tbFilter8Name_pos";
            this.tbFilter8Name_pos.Size = new System.Drawing.Size(189, 20);
            this.tbFilter8Name_pos.TabIndex = 44;
            // 
            // tbFilter9Name_pos
            // 
            this.tbFilter9Name_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter9Name_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter9Name_pos.Location = new System.Drawing.Point(237, 324);
            this.tbFilter9Name_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbFilter9Name_pos.Name = "tbFilter9Name_pos";
            this.tbFilter9Name_pos.Size = new System.Drawing.Size(189, 20);
            this.tbFilter9Name_pos.TabIndex = 45;
            // 
            // nmFilterValue_pos
            // 
            this.nmFilterValue_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmFilterValue_pos.Location = new System.Drawing.Point(450, 45);
            this.nmFilterValue_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmFilterValue_pos.Name = "nmFilterValue_pos";
            this.nmFilterValue_pos.Size = new System.Drawing.Size(120, 20);
            this.nmFilterValue_pos.TabIndex = 58;
            this.nmFilterValue_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmFilter1Value_pos
            // 
            this.nmFilter1Value_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmFilter1Value_pos.Location = new System.Drawing.Point(450, 76);
            this.nmFilter1Value_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmFilter1Value_pos.Name = "nmFilter1Value_pos";
            this.nmFilter1Value_pos.Size = new System.Drawing.Size(120, 20);
            this.nmFilter1Value_pos.TabIndex = 59;
            this.nmFilter1Value_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmFilter2Value_pos
            // 
            this.nmFilter2Value_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmFilter2Value_pos.Location = new System.Drawing.Point(450, 107);
            this.nmFilter2Value_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmFilter2Value_pos.Name = "nmFilter2Value_pos";
            this.nmFilter2Value_pos.Size = new System.Drawing.Size(120, 20);
            this.nmFilter2Value_pos.TabIndex = 60;
            this.nmFilter2Value_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmFilter3Value_pos
            // 
            this.nmFilter3Value_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmFilter3Value_pos.Location = new System.Drawing.Point(450, 138);
            this.nmFilter3Value_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmFilter3Value_pos.Name = "nmFilter3Value_pos";
            this.nmFilter3Value_pos.Size = new System.Drawing.Size(120, 20);
            this.nmFilter3Value_pos.TabIndex = 61;
            this.nmFilter3Value_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmFilter4Value_pos
            // 
            this.nmFilter4Value_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmFilter4Value_pos.Location = new System.Drawing.Point(450, 169);
            this.nmFilter4Value_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmFilter4Value_pos.Name = "nmFilter4Value_pos";
            this.nmFilter4Value_pos.Size = new System.Drawing.Size(120, 20);
            this.nmFilter4Value_pos.TabIndex = 62;
            this.nmFilter4Value_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmFilter5Value_pos
            // 
            this.nmFilter5Value_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmFilter5Value_pos.Location = new System.Drawing.Point(450, 200);
            this.nmFilter5Value_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmFilter5Value_pos.Name = "nmFilter5Value_pos";
            this.nmFilter5Value_pos.Size = new System.Drawing.Size(120, 20);
            this.nmFilter5Value_pos.TabIndex = 63;
            this.nmFilter5Value_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmFilter6Value_pos
            // 
            this.nmFilter6Value_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmFilter6Value_pos.Location = new System.Drawing.Point(450, 231);
            this.nmFilter6Value_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmFilter6Value_pos.Name = "nmFilter6Value_pos";
            this.nmFilter6Value_pos.Size = new System.Drawing.Size(120, 20);
            this.nmFilter6Value_pos.TabIndex = 64;
            this.nmFilter6Value_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmFilter7Value_pos
            // 
            this.nmFilter7Value_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmFilter7Value_pos.Location = new System.Drawing.Point(450, 262);
            this.nmFilter7Value_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmFilter7Value_pos.Name = "nmFilter7Value_pos";
            this.nmFilter7Value_pos.Size = new System.Drawing.Size(120, 20);
            this.nmFilter7Value_pos.TabIndex = 65;
            this.nmFilter7Value_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmFilter8Value_pos
            // 
            this.nmFilter8Value_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmFilter8Value_pos.Location = new System.Drawing.Point(450, 293);
            this.nmFilter8Value_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmFilter8Value_pos.Name = "nmFilter8Value_pos";
            this.nmFilter8Value_pos.Size = new System.Drawing.Size(120, 20);
            this.nmFilter8Value_pos.TabIndex = 66;
            this.nmFilter8Value_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmFilter9Value_pos
            // 
            this.nmFilter9Value_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmFilter9Value_pos.Location = new System.Drawing.Point(450, 324);
            this.nmFilter9Value_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmFilter9Value_pos.Name = "nmFilter9Value_pos";
            this.nmFilter9Value_pos.Size = new System.Drawing.Size(120, 20);
            this.nmFilter9Value_pos.TabIndex = 67;
            this.nmFilter9Value_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.Location = new System.Drawing.Point(450, 1);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(160, 17);
            this.label132.TabIndex = 68;
            this.label132.Text = "Source Column Index";
            // 
            // tabProductSettingsXml
            // 
            this.tabProductSettingsXml.Controls.Add(this.tableLayoutPanel23);
            this.tabProductSettingsXml.Location = new System.Drawing.Point(4, 22);
            this.tabProductSettingsXml.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabProductSettingsXml.Name = "tabProductSettingsXml";
            this.tabProductSettingsXml.Size = new System.Drawing.Size(924, 549);
            this.tabProductSettingsXml.TabIndex = 22;
            this.tabProductSettingsXml.Text = "Product Settings";
            this.tabProductSettingsXml.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel23.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel23.ColumnCount = 4;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.62855F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 59.37145F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 314F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 226F));
            this.tableLayoutPanel23.Controls.Add(this.tbDefaultProdSettingsDeliveryDate_xml, 2, 10);
            this.tableLayoutPanel23.Controls.Add(this.tbProdSettingsDeliveryDate_xml, 1, 10);
            this.tableLayoutPanel23.Controls.Add(this.tbDefaultProdSettingsShippingCharge_xml, 2, 9);
            this.tableLayoutPanel23.Controls.Add(this.tbProdSettingsShippingCharge_xml, 1, 9);
            this.tableLayoutPanel23.Controls.Add(this.tbDefaultProdSettingsShipSeparately_xml, 2, 8);
            this.tableLayoutPanel23.Controls.Add(this.tbProdSettingsShipSeparately_xml, 1, 8);
            this.tableLayoutPanel23.Controls.Add(this.tbDefaultProdSettingsFreeShipping_xml, 2, 7);
            this.tableLayoutPanel23.Controls.Add(this.tbProdSettingsFreeShipping_xml, 1, 7);
            this.tableLayoutPanel23.Controls.Add(this.tbDefaultProdSettingsShippingEnabled_xml, 2, 6);
            this.tableLayoutPanel23.Controls.Add(this.tbProdSettingsShippingEnabled_xml, 1, 6);
            this.tableLayoutPanel23.Controls.Add(this.tbDefaultProdSettingsMaxCartQty_xml, 2, 4);
            this.tableLayoutPanel23.Controls.Add(this.tbProdSettingsMaxCartQty_xml, 1, 4);
            this.tableLayoutPanel23.Controls.Add(this.tbDefaultProdSettingsMinCartQty_xml, 2, 3);
            this.tableLayoutPanel23.Controls.Add(this.tbProdSettingsMinCartQty_xml, 1, 3);
            this.tableLayoutPanel23.Controls.Add(this.label147, 0, 3);
            this.tableLayoutPanel23.Controls.Add(this.tbDefaultProdSettingsNotifyQty_xml, 2, 2);
            this.tableLayoutPanel23.Controls.Add(this.tbDefaultProdSettingsMinStockQty_xml, 2, 1);
            this.tableLayoutPanel23.Controls.Add(this.label237, 0, 0);
            this.tableLayoutPanel23.Controls.Add(this.label238, 1, 0);
            this.tableLayoutPanel23.Controls.Add(this.label243, 2, 0);
            this.tableLayoutPanel23.Controls.Add(this.label244, 0, 1);
            this.tableLayoutPanel23.Controls.Add(this.tbProdSettingsMinStockQty_xml, 1, 1);
            this.tableLayoutPanel23.Controls.Add(this.label245, 0, 2);
            this.tableLayoutPanel23.Controls.Add(this.tbProdSettingsNotifyQty_xml, 1, 2);
            this.tableLayoutPanel23.Controls.Add(this.label246, 0, 4);
            this.tableLayoutPanel23.Controls.Add(this.label247, 0, 5);
            this.tableLayoutPanel23.Controls.Add(this.tbProdSettingsAllowedQty_xml, 1, 5);
            this.tableLayoutPanel23.Controls.Add(this.tbDefaultProdSettingsAllowedQty_xml, 2, 5);
            this.tableLayoutPanel23.Controls.Add(this.label256, 0, 6);
            this.tableLayoutPanel23.Controls.Add(this.label257, 0, 7);
            this.tableLayoutPanel23.Controls.Add(this.label258, 0, 8);
            this.tableLayoutPanel23.Controls.Add(this.label259, 0, 9);
            this.tableLayoutPanel23.Controls.Add(this.label260, 0, 10);
            this.tableLayoutPanel23.Controls.Add(this.label266, 3, 0);
            this.tableLayoutPanel23.Controls.Add(this.chProdSettingsDeliveryDateIsID_xml, 3, 10);
            this.tableLayoutPanel23.Controls.Add(this.label51, 0, 11);
            this.tableLayoutPanel23.Controls.Add(this.tbProdSettingsVisibleIndvidually_xml, 1, 11);
            this.tableLayoutPanel23.Controls.Add(this.tbDefaultProdSettingsVisibleIndvidually_xml, 2, 11);
            this.tableLayoutPanel23.Controls.Add(this.label61, 0, 12);
            this.tableLayoutPanel23.Controls.Add(this.tbProdSettingsIsDeleted_xml, 1, 12);
            this.tableLayoutPanel23.Controls.Add(this.tbDefaultProdSettingsIsDeleted_xml, 2, 12);
            this.tableLayoutPanel23.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 14;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(921, 547);
            this.tableLayoutPanel23.TabIndex = 1;
            // 
            // tbDefaultProdSettingsDeliveryDate_xml
            // 
            this.tbDefaultProdSettingsDeliveryDate_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsDeliveryDate_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsDeliveryDate_xml.Location = new System.Drawing.Point(381, 324);
            this.tbDefaultProdSettingsDeliveryDate_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsDeliveryDate_xml.Name = "tbDefaultProdSettingsDeliveryDate_xml";
            this.tbDefaultProdSettingsDeliveryDate_xml.Size = new System.Drawing.Size(291, 20);
            this.tbDefaultProdSettingsDeliveryDate_xml.TabIndex = 52;
            // 
            // tbProdSettingsDeliveryDate_xml
            // 
            this.tbProdSettingsDeliveryDate_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdSettingsDeliveryDate_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdSettingsDeliveryDate_xml.Location = new System.Drawing.Point(157, 324);
            this.tbProdSettingsDeliveryDate_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdSettingsDeliveryDate_xml.Name = "tbProdSettingsDeliveryDate_xml";
            this.tbProdSettingsDeliveryDate_xml.Size = new System.Drawing.Size(200, 20);
            this.tbProdSettingsDeliveryDate_xml.TabIndex = 51;
            // 
            // tbDefaultProdSettingsShippingCharge_xml
            // 
            this.tbDefaultProdSettingsShippingCharge_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsShippingCharge_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsShippingCharge_xml.Location = new System.Drawing.Point(381, 293);
            this.tbDefaultProdSettingsShippingCharge_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsShippingCharge_xml.Name = "tbDefaultProdSettingsShippingCharge_xml";
            this.tbDefaultProdSettingsShippingCharge_xml.Size = new System.Drawing.Size(291, 20);
            this.tbDefaultProdSettingsShippingCharge_xml.TabIndex = 50;
            // 
            // tbProdSettingsShippingCharge_xml
            // 
            this.tbProdSettingsShippingCharge_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdSettingsShippingCharge_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdSettingsShippingCharge_xml.Location = new System.Drawing.Point(157, 293);
            this.tbProdSettingsShippingCharge_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdSettingsShippingCharge_xml.Name = "tbProdSettingsShippingCharge_xml";
            this.tbProdSettingsShippingCharge_xml.Size = new System.Drawing.Size(200, 20);
            this.tbProdSettingsShippingCharge_xml.TabIndex = 49;
            // 
            // tbDefaultProdSettingsShipSeparately_xml
            // 
            this.tbDefaultProdSettingsShipSeparately_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsShipSeparately_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsShipSeparately_xml.Location = new System.Drawing.Point(381, 262);
            this.tbDefaultProdSettingsShipSeparately_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsShipSeparately_xml.Name = "tbDefaultProdSettingsShipSeparately_xml";
            this.tbDefaultProdSettingsShipSeparately_xml.Size = new System.Drawing.Size(291, 20);
            this.tbDefaultProdSettingsShipSeparately_xml.TabIndex = 48;
            // 
            // tbProdSettingsShipSeparately_xml
            // 
            this.tbProdSettingsShipSeparately_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdSettingsShipSeparately_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdSettingsShipSeparately_xml.Location = new System.Drawing.Point(157, 262);
            this.tbProdSettingsShipSeparately_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdSettingsShipSeparately_xml.Name = "tbProdSettingsShipSeparately_xml";
            this.tbProdSettingsShipSeparately_xml.Size = new System.Drawing.Size(200, 20);
            this.tbProdSettingsShipSeparately_xml.TabIndex = 47;
            // 
            // tbDefaultProdSettingsFreeShipping_xml
            // 
            this.tbDefaultProdSettingsFreeShipping_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsFreeShipping_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsFreeShipping_xml.Location = new System.Drawing.Point(381, 231);
            this.tbDefaultProdSettingsFreeShipping_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsFreeShipping_xml.Name = "tbDefaultProdSettingsFreeShipping_xml";
            this.tbDefaultProdSettingsFreeShipping_xml.Size = new System.Drawing.Size(291, 20);
            this.tbDefaultProdSettingsFreeShipping_xml.TabIndex = 46;
            // 
            // tbProdSettingsFreeShipping_xml
            // 
            this.tbProdSettingsFreeShipping_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdSettingsFreeShipping_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdSettingsFreeShipping_xml.Location = new System.Drawing.Point(157, 231);
            this.tbProdSettingsFreeShipping_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdSettingsFreeShipping_xml.Name = "tbProdSettingsFreeShipping_xml";
            this.tbProdSettingsFreeShipping_xml.Size = new System.Drawing.Size(200, 20);
            this.tbProdSettingsFreeShipping_xml.TabIndex = 45;
            // 
            // tbDefaultProdSettingsShippingEnabled_xml
            // 
            this.tbDefaultProdSettingsShippingEnabled_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsShippingEnabled_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsShippingEnabled_xml.Location = new System.Drawing.Point(381, 200);
            this.tbDefaultProdSettingsShippingEnabled_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsShippingEnabled_xml.Name = "tbDefaultProdSettingsShippingEnabled_xml";
            this.tbDefaultProdSettingsShippingEnabled_xml.Size = new System.Drawing.Size(291, 20);
            this.tbDefaultProdSettingsShippingEnabled_xml.TabIndex = 44;
            // 
            // tbProdSettingsShippingEnabled_xml
            // 
            this.tbProdSettingsShippingEnabled_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdSettingsShippingEnabled_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdSettingsShippingEnabled_xml.Location = new System.Drawing.Point(157, 200);
            this.tbProdSettingsShippingEnabled_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdSettingsShippingEnabled_xml.Name = "tbProdSettingsShippingEnabled_xml";
            this.tbProdSettingsShippingEnabled_xml.Size = new System.Drawing.Size(200, 20);
            this.tbProdSettingsShippingEnabled_xml.TabIndex = 43;
            // 
            // tbDefaultProdSettingsMaxCartQty_xml
            // 
            this.tbDefaultProdSettingsMaxCartQty_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsMaxCartQty_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsMaxCartQty_xml.Location = new System.Drawing.Point(381, 138);
            this.tbDefaultProdSettingsMaxCartQty_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsMaxCartQty_xml.Name = "tbDefaultProdSettingsMaxCartQty_xml";
            this.tbDefaultProdSettingsMaxCartQty_xml.Size = new System.Drawing.Size(291, 20);
            this.tbDefaultProdSettingsMaxCartQty_xml.TabIndex = 34;
            // 
            // tbProdSettingsMaxCartQty_xml
            // 
            this.tbProdSettingsMaxCartQty_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdSettingsMaxCartQty_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdSettingsMaxCartQty_xml.Location = new System.Drawing.Point(157, 138);
            this.tbProdSettingsMaxCartQty_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdSettingsMaxCartQty_xml.Name = "tbProdSettingsMaxCartQty_xml";
            this.tbProdSettingsMaxCartQty_xml.Size = new System.Drawing.Size(200, 20);
            this.tbProdSettingsMaxCartQty_xml.TabIndex = 33;
            // 
            // tbDefaultProdSettingsMinCartQty_xml
            // 
            this.tbDefaultProdSettingsMinCartQty_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsMinCartQty_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsMinCartQty_xml.Location = new System.Drawing.Point(381, 107);
            this.tbDefaultProdSettingsMinCartQty_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsMinCartQty_xml.Name = "tbDefaultProdSettingsMinCartQty_xml";
            this.tbDefaultProdSettingsMinCartQty_xml.Size = new System.Drawing.Size(291, 20);
            this.tbDefaultProdSettingsMinCartQty_xml.TabIndex = 32;
            // 
            // tbProdSettingsMinCartQty_xml
            // 
            this.tbProdSettingsMinCartQty_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdSettingsMinCartQty_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdSettingsMinCartQty_xml.Location = new System.Drawing.Point(157, 107);
            this.tbProdSettingsMinCartQty_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdSettingsMinCartQty_xml.Name = "tbProdSettingsMinCartQty_xml";
            this.tbProdSettingsMinCartQty_xml.Size = new System.Drawing.Size(200, 20);
            this.tbProdSettingsMinCartQty_xml.TabIndex = 31;
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(4, 104);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(109, 13);
            this.label147.TabIndex = 29;
            this.label147.Text = "Minimum cart quantity";
            // 
            // tbDefaultProdSettingsNotifyQty_xml
            // 
            this.tbDefaultProdSettingsNotifyQty_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsNotifyQty_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsNotifyQty_xml.Location = new System.Drawing.Point(381, 76);
            this.tbDefaultProdSettingsNotifyQty_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsNotifyQty_xml.Name = "tbDefaultProdSettingsNotifyQty_xml";
            this.tbDefaultProdSettingsNotifyQty_xml.Size = new System.Drawing.Size(291, 20);
            this.tbDefaultProdSettingsNotifyQty_xml.TabIndex = 28;
            // 
            // tbDefaultProdSettingsMinStockQty_xml
            // 
            this.tbDefaultProdSettingsMinStockQty_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsMinStockQty_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsMinStockQty_xml.Location = new System.Drawing.Point(381, 45);
            this.tbDefaultProdSettingsMinStockQty_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsMinStockQty_xml.Name = "tbDefaultProdSettingsMinStockQty_xml";
            this.tbDefaultProdSettingsMinStockQty_xml.Size = new System.Drawing.Size(291, 20);
            this.tbDefaultProdSettingsMinStockQty_xml.TabIndex = 27;
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label237.Location = new System.Drawing.Point(4, 1);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(89, 17);
            this.label237.TabIndex = 12;
            this.label237.Text = "Field Name";
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label238.Location = new System.Drawing.Point(157, 1);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(135, 17);
            this.label238.TabIndex = 13;
            this.label238.Text = "XPath Expression";
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label243.Location = new System.Drawing.Point(381, 1);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(199, 17);
            this.label243.TabIndex = 22;
            this.label243.Text = "Default Value /+Add Value";
            // 
            // label244
            // 
            this.label244.AutoSize = true;
            this.label244.Location = new System.Drawing.Point(4, 42);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(117, 13);
            this.label244.TabIndex = 23;
            this.label244.Text = "Minimum stock quantity";
            // 
            // tbProdSettingsMinStockQty_xml
            // 
            this.tbProdSettingsMinStockQty_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdSettingsMinStockQty_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdSettingsMinStockQty_xml.Location = new System.Drawing.Point(157, 45);
            this.tbProdSettingsMinStockQty_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdSettingsMinStockQty_xml.Name = "tbProdSettingsMinStockQty_xml";
            this.tbProdSettingsMinStockQty_xml.Size = new System.Drawing.Size(200, 20);
            this.tbProdSettingsMinStockQty_xml.TabIndex = 24;
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Location = new System.Drawing.Point(4, 73);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(120, 13);
            this.label245.TabIndex = 25;
            this.label245.Text = "Notify for quantity below";
            // 
            // tbProdSettingsNotifyQty_xml
            // 
            this.tbProdSettingsNotifyQty_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdSettingsNotifyQty_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdSettingsNotifyQty_xml.Location = new System.Drawing.Point(157, 76);
            this.tbProdSettingsNotifyQty_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdSettingsNotifyQty_xml.Name = "tbProdSettingsNotifyQty_xml";
            this.tbProdSettingsNotifyQty_xml.Size = new System.Drawing.Size(200, 20);
            this.tbProdSettingsNotifyQty_xml.TabIndex = 26;
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Location = new System.Drawing.Point(4, 135);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(112, 13);
            this.label246.TabIndex = 30;
            this.label246.Text = "Maximum cart quantity";
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Location = new System.Drawing.Point(4, 166);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(92, 13);
            this.label247.TabIndex = 35;
            this.label247.Text = "Allowed quantities";
            // 
            // tbProdSettingsAllowedQty_xml
            // 
            this.tbProdSettingsAllowedQty_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdSettingsAllowedQty_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdSettingsAllowedQty_xml.Location = new System.Drawing.Point(157, 169);
            this.tbProdSettingsAllowedQty_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdSettingsAllowedQty_xml.Name = "tbProdSettingsAllowedQty_xml";
            this.tbProdSettingsAllowedQty_xml.Size = new System.Drawing.Size(200, 20);
            this.tbProdSettingsAllowedQty_xml.TabIndex = 36;
            // 
            // tbDefaultProdSettingsAllowedQty_xml
            // 
            this.tbDefaultProdSettingsAllowedQty_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsAllowedQty_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsAllowedQty_xml.Location = new System.Drawing.Point(381, 169);
            this.tbDefaultProdSettingsAllowedQty_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsAllowedQty_xml.Name = "tbDefaultProdSettingsAllowedQty_xml";
            this.tbDefaultProdSettingsAllowedQty_xml.Size = new System.Drawing.Size(291, 20);
            this.tbDefaultProdSettingsAllowedQty_xml.TabIndex = 37;
            // 
            // label256
            // 
            this.label256.AutoSize = true;
            this.label256.Location = new System.Drawing.Point(3, 197);
            this.label256.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(89, 13);
            this.label256.TabIndex = 38;
            this.label256.Text = "Shipping enabled";
            // 
            // label257
            // 
            this.label257.AutoSize = true;
            this.label257.Location = new System.Drawing.Point(3, 228);
            this.label257.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(72, 13);
            this.label257.TabIndex = 39;
            this.label257.Text = "Free Shipping";
            // 
            // label258
            // 
            this.label258.AutoSize = true;
            this.label258.Location = new System.Drawing.Point(3, 259);
            this.label258.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(81, 13);
            this.label258.TabIndex = 40;
            this.label258.Text = "Ship Separately";
            // 
            // label259
            // 
            this.label259.AutoSize = true;
            this.label259.Location = new System.Drawing.Point(3, 290);
            this.label259.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(134, 13);
            this.label259.TabIndex = 41;
            this.label259.Text = "Additional Shipping Charge";
            // 
            // label260
            // 
            this.label260.AutoSize = true;
            this.label260.Location = new System.Drawing.Point(3, 321);
            this.label260.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(71, 13);
            this.label260.TabIndex = 42;
            this.label260.Text = "Delivery Date";
            // 
            // label266
            // 
            this.label266.AutoSize = true;
            this.label266.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label266.Location = new System.Drawing.Point(696, 1);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(67, 17);
            this.label266.TabIndex = 53;
            this.label266.Text = "Id Value";
            // 
            // chProdSettingsDeliveryDateIsID_xml
            // 
            this.chProdSettingsDeliveryDateIsID_xml.AutoSize = true;
            this.chProdSettingsDeliveryDateIsID_xml.Location = new System.Drawing.Point(696, 324);
            this.chProdSettingsDeliveryDateIsID_xml.Name = "chProdSettingsDeliveryDateIsID_xml";
            this.chProdSettingsDeliveryDateIsID_xml.Size = new System.Drawing.Size(15, 14);
            this.chProdSettingsDeliveryDateIsID_xml.TabIndex = 54;
            this.chProdSettingsDeliveryDateIsID_xml.UseVisualStyleBackColor = true;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(3, 352);
            this.label51.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(92, 13);
            this.label51.TabIndex = 55;
            this.label51.Text = "Visible Individually";
            // 
            // tbProdSettingsVisibleIndvidually_xml
            // 
            this.tbProdSettingsVisibleIndvidually_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdSettingsVisibleIndvidually_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdSettingsVisibleIndvidually_xml.Location = new System.Drawing.Point(157, 355);
            this.tbProdSettingsVisibleIndvidually_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdSettingsVisibleIndvidually_xml.Name = "tbProdSettingsVisibleIndvidually_xml";
            this.tbProdSettingsVisibleIndvidually_xml.Size = new System.Drawing.Size(200, 20);
            this.tbProdSettingsVisibleIndvidually_xml.TabIndex = 56;
            // 
            // tbDefaultProdSettingsVisibleIndvidually_xml
            // 
            this.tbDefaultProdSettingsVisibleIndvidually_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsVisibleIndvidually_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsVisibleIndvidually_xml.Location = new System.Drawing.Point(381, 355);
            this.tbDefaultProdSettingsVisibleIndvidually_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsVisibleIndvidually_xml.Name = "tbDefaultProdSettingsVisibleIndvidually_xml";
            this.tbDefaultProdSettingsVisibleIndvidually_xml.Size = new System.Drawing.Size(291, 20);
            this.tbDefaultProdSettingsVisibleIndvidually_xml.TabIndex = 57;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(3, 383);
            this.label61.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(55, 13);
            this.label61.TabIndex = 58;
            this.label61.Text = "Is Deleted";
            // 
            // tbProdSettingsIsDeleted_xml
            // 
            this.tbProdSettingsIsDeleted_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdSettingsIsDeleted_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdSettingsIsDeleted_xml.Location = new System.Drawing.Point(157, 386);
            this.tbProdSettingsIsDeleted_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbProdSettingsIsDeleted_xml.Name = "tbProdSettingsIsDeleted_xml";
            this.tbProdSettingsIsDeleted_xml.Size = new System.Drawing.Size(200, 20);
            this.tbProdSettingsIsDeleted_xml.TabIndex = 59;
            // 
            // tbDefaultProdSettingsIsDeleted_xml
            // 
            this.tbDefaultProdSettingsIsDeleted_xml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsIsDeleted_xml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsIsDeleted_xml.Location = new System.Drawing.Point(381, 386);
            this.tbDefaultProdSettingsIsDeleted_xml.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsIsDeleted_xml.Name = "tbDefaultProdSettingsIsDeleted_xml";
            this.tbDefaultProdSettingsIsDeleted_xml.Size = new System.Drawing.Size(291, 20);
            this.tbDefaultProdSettingsIsDeleted_xml.TabIndex = 60;
            // 
            // tabProductSettingsPos
            // 
            this.tabProductSettingsPos.Controls.Add(this.tableLayoutPanel24);
            this.tabProductSettingsPos.Location = new System.Drawing.Point(4, 22);
            this.tabProductSettingsPos.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabProductSettingsPos.Name = "tabProductSettingsPos";
            this.tabProductSettingsPos.Size = new System.Drawing.Size(924, 549);
            this.tabProductSettingsPos.TabIndex = 23;
            this.tabProductSettingsPos.Text = "Product Settings";
            this.tabProductSettingsPos.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel24.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel24.ColumnCount = 4;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 179F));
            this.tableLayoutPanel24.Controls.Add(this.tbDefaultProdSettingsDeliveryDate_pos, 2, 10);
            this.tableLayoutPanel24.Controls.Add(this.tbDefaultProdSettingsShippingCharge_pos, 2, 9);
            this.tableLayoutPanel24.Controls.Add(this.tbDefaultProdSettingsShipSeparately_pos, 2, 8);
            this.tableLayoutPanel24.Controls.Add(this.tbDefaultProdSettingsFreeShipping_pos, 2, 7);
            this.tableLayoutPanel24.Controls.Add(this.tbDefaultProdSettingsShippingEnabled_pos, 2, 6);
            this.tableLayoutPanel24.Controls.Add(this.nmProdSettingsShippingEnabled_pos, 1, 6);
            this.tableLayoutPanel24.Controls.Add(this.tbDefaultProdSettingsMaxCartQty_pos, 2, 4);
            this.tableLayoutPanel24.Controls.Add(this.tbDefaultProdSettingsMinCartQty_pos, 2, 3);
            this.tableLayoutPanel24.Controls.Add(this.label248, 0, 3);
            this.tableLayoutPanel24.Controls.Add(this.tbDefaultProdSettingsNotifyQty_pos, 2, 2);
            this.tableLayoutPanel24.Controls.Add(this.tbDefaultProdSettingsMinStockQty_pos, 2, 1);
            this.tableLayoutPanel24.Controls.Add(this.label249, 0, 0);
            this.tableLayoutPanel24.Controls.Add(this.label250, 2, 0);
            this.tableLayoutPanel24.Controls.Add(this.label251, 0, 1);
            this.tableLayoutPanel24.Controls.Add(this.label252, 0, 2);
            this.tableLayoutPanel24.Controls.Add(this.label253, 0, 4);
            this.tableLayoutPanel24.Controls.Add(this.label254, 1, 0);
            this.tableLayoutPanel24.Controls.Add(this.nmProdSettingsMinStockQty_pos, 1, 1);
            this.tableLayoutPanel24.Controls.Add(this.nmProdSettingsNotifyQty_pos, 1, 2);
            this.tableLayoutPanel24.Controls.Add(this.nmProdSettingsMinCartQty_pos, 1, 3);
            this.tableLayoutPanel24.Controls.Add(this.nmProdSettingsMaxCartQty_pos, 1, 4);
            this.tableLayoutPanel24.Controls.Add(this.label255, 0, 5);
            this.tableLayoutPanel24.Controls.Add(this.nmProdSettingsAllowedQty_pos, 1, 5);
            this.tableLayoutPanel24.Controls.Add(this.tbDefaultProdSettingsAllowedQty_pos, 2, 5);
            this.tableLayoutPanel24.Controls.Add(this.label261, 0, 6);
            this.tableLayoutPanel24.Controls.Add(this.label262, 0, 7);
            this.tableLayoutPanel24.Controls.Add(this.label263, 0, 8);
            this.tableLayoutPanel24.Controls.Add(this.label264, 0, 9);
            this.tableLayoutPanel24.Controls.Add(this.label265, 0, 10);
            this.tableLayoutPanel24.Controls.Add(this.nmProdSettingsFreeShipping_pos, 1, 7);
            this.tableLayoutPanel24.Controls.Add(this.nmProdSettingsShipSeparately_pos, 1, 8);
            this.tableLayoutPanel24.Controls.Add(this.nmProdSettingsShippingCharge_pos, 1, 9);
            this.tableLayoutPanel24.Controls.Add(this.nmProdSettingsDeliveryDate_pos, 1, 10);
            this.tableLayoutPanel24.Controls.Add(this.label267, 3, 0);
            this.tableLayoutPanel24.Controls.Add(this.chProdSettingsDeliveryDateIsID_pos, 3, 10);
            this.tableLayoutPanel24.Controls.Add(this.label50, 0, 11);
            this.tableLayoutPanel24.Controls.Add(this.nmProdSettingsVisibleIndvidually_pos, 1, 11);
            this.tableLayoutPanel24.Controls.Add(this.tbDefaultProdSettingsVisibleIndvidually_pos, 2, 11);
            this.tableLayoutPanel24.Controls.Add(this.nmProdSettingsIsDeleted_pos, 1, 12);
            this.tableLayoutPanel24.Controls.Add(this.label60, 0, 12);
            this.tableLayoutPanel24.Controls.Add(this.tbDefaultProdSettingsIsDeleted_pos, 2, 12);
            this.tableLayoutPanel24.Location = new System.Drawing.Point(-2, 2);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 14;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(931, 547);
            this.tableLayoutPanel24.TabIndex = 2;
            // 
            // tbDefaultProdSettingsDeliveryDate_pos
            // 
            this.tbDefaultProdSettingsDeliveryDate_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsDeliveryDate_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsDeliveryDate_pos.Location = new System.Drawing.Point(406, 324);
            this.tbDefaultProdSettingsDeliveryDate_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsDeliveryDate_pos.Name = "tbDefaultProdSettingsDeliveryDate_pos";
            this.tbDefaultProdSettingsDeliveryDate_pos.Size = new System.Drawing.Size(324, 20);
            this.tbDefaultProdSettingsDeliveryDate_pos.TabIndex = 57;
            // 
            // tbDefaultProdSettingsShippingCharge_pos
            // 
            this.tbDefaultProdSettingsShippingCharge_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsShippingCharge_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsShippingCharge_pos.Location = new System.Drawing.Point(406, 293);
            this.tbDefaultProdSettingsShippingCharge_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsShippingCharge_pos.Name = "tbDefaultProdSettingsShippingCharge_pos";
            this.tbDefaultProdSettingsShippingCharge_pos.Size = new System.Drawing.Size(324, 20);
            this.tbDefaultProdSettingsShippingCharge_pos.TabIndex = 56;
            // 
            // tbDefaultProdSettingsShipSeparately_pos
            // 
            this.tbDefaultProdSettingsShipSeparately_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsShipSeparately_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsShipSeparately_pos.Location = new System.Drawing.Point(406, 262);
            this.tbDefaultProdSettingsShipSeparately_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsShipSeparately_pos.Name = "tbDefaultProdSettingsShipSeparately_pos";
            this.tbDefaultProdSettingsShipSeparately_pos.Size = new System.Drawing.Size(324, 20);
            this.tbDefaultProdSettingsShipSeparately_pos.TabIndex = 55;
            // 
            // tbDefaultProdSettingsFreeShipping_pos
            // 
            this.tbDefaultProdSettingsFreeShipping_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsFreeShipping_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsFreeShipping_pos.Location = new System.Drawing.Point(406, 231);
            this.tbDefaultProdSettingsFreeShipping_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsFreeShipping_pos.Name = "tbDefaultProdSettingsFreeShipping_pos";
            this.tbDefaultProdSettingsFreeShipping_pos.Size = new System.Drawing.Size(324, 20);
            this.tbDefaultProdSettingsFreeShipping_pos.TabIndex = 54;
            // 
            // tbDefaultProdSettingsShippingEnabled_pos
            // 
            this.tbDefaultProdSettingsShippingEnabled_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsShippingEnabled_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsShippingEnabled_pos.Location = new System.Drawing.Point(406, 200);
            this.tbDefaultProdSettingsShippingEnabled_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsShippingEnabled_pos.Name = "tbDefaultProdSettingsShippingEnabled_pos";
            this.tbDefaultProdSettingsShippingEnabled_pos.Size = new System.Drawing.Size(324, 20);
            this.tbDefaultProdSettingsShippingEnabled_pos.TabIndex = 53;
            // 
            // nmProdSettingsShippingEnabled_pos
            // 
            this.nmProdSettingsShippingEnabled_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdSettingsShippingEnabled_pos.Location = new System.Drawing.Point(155, 200);
            this.nmProdSettingsShippingEnabled_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdSettingsShippingEnabled_pos.Name = "nmProdSettingsShippingEnabled_pos";
            this.nmProdSettingsShippingEnabled_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdSettingsShippingEnabled_pos.TabIndex = 48;
            this.nmProdSettingsShippingEnabled_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // tbDefaultProdSettingsMaxCartQty_pos
            // 
            this.tbDefaultProdSettingsMaxCartQty_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsMaxCartQty_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsMaxCartQty_pos.Location = new System.Drawing.Point(406, 138);
            this.tbDefaultProdSettingsMaxCartQty_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsMaxCartQty_pos.Name = "tbDefaultProdSettingsMaxCartQty_pos";
            this.tbDefaultProdSettingsMaxCartQty_pos.Size = new System.Drawing.Size(324, 20);
            this.tbDefaultProdSettingsMaxCartQty_pos.TabIndex = 34;
            // 
            // tbDefaultProdSettingsMinCartQty_pos
            // 
            this.tbDefaultProdSettingsMinCartQty_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsMinCartQty_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsMinCartQty_pos.Location = new System.Drawing.Point(406, 107);
            this.tbDefaultProdSettingsMinCartQty_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsMinCartQty_pos.Name = "tbDefaultProdSettingsMinCartQty_pos";
            this.tbDefaultProdSettingsMinCartQty_pos.Size = new System.Drawing.Size(324, 20);
            this.tbDefaultProdSettingsMinCartQty_pos.TabIndex = 32;
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Location = new System.Drawing.Point(4, 104);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(109, 13);
            this.label248.TabIndex = 29;
            this.label248.Text = "Minimum cart quantity";
            // 
            // tbDefaultProdSettingsNotifyQty_pos
            // 
            this.tbDefaultProdSettingsNotifyQty_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsNotifyQty_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsNotifyQty_pos.Location = new System.Drawing.Point(406, 76);
            this.tbDefaultProdSettingsNotifyQty_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsNotifyQty_pos.Name = "tbDefaultProdSettingsNotifyQty_pos";
            this.tbDefaultProdSettingsNotifyQty_pos.Size = new System.Drawing.Size(324, 20);
            this.tbDefaultProdSettingsNotifyQty_pos.TabIndex = 28;
            // 
            // tbDefaultProdSettingsMinStockQty_pos
            // 
            this.tbDefaultProdSettingsMinStockQty_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsMinStockQty_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsMinStockQty_pos.Location = new System.Drawing.Point(406, 45);
            this.tbDefaultProdSettingsMinStockQty_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsMinStockQty_pos.Name = "tbDefaultProdSettingsMinStockQty_pos";
            this.tbDefaultProdSettingsMinStockQty_pos.Size = new System.Drawing.Size(324, 20);
            this.tbDefaultProdSettingsMinStockQty_pos.TabIndex = 27;
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label249.Location = new System.Drawing.Point(4, 1);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(89, 17);
            this.label249.TabIndex = 12;
            this.label249.Text = "Field Name";
            // 
            // label250
            // 
            this.label250.AutoSize = true;
            this.label250.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label250.Location = new System.Drawing.Point(406, 1);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(199, 17);
            this.label250.TabIndex = 22;
            this.label250.Text = "Default Value /+Add Value";
            // 
            // label251
            // 
            this.label251.AutoSize = true;
            this.label251.Location = new System.Drawing.Point(4, 42);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(117, 13);
            this.label251.TabIndex = 23;
            this.label251.Text = "Minimum stock quantity";
            // 
            // label252
            // 
            this.label252.AutoSize = true;
            this.label252.Location = new System.Drawing.Point(4, 73);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(120, 13);
            this.label252.TabIndex = 25;
            this.label252.Text = "Notify for quantity below";
            // 
            // label253
            // 
            this.label253.AutoSize = true;
            this.label253.Location = new System.Drawing.Point(4, 135);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(112, 13);
            this.label253.TabIndex = 30;
            this.label253.Text = "Maximum cart quantity";
            // 
            // label254
            // 
            this.label254.AutoSize = true;
            this.label254.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label254.Location = new System.Drawing.Point(155, 1);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(160, 17);
            this.label254.TabIndex = 35;
            this.label254.Text = "Source Column Index";
            // 
            // nmProdSettingsMinStockQty_pos
            // 
            this.nmProdSettingsMinStockQty_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdSettingsMinStockQty_pos.Location = new System.Drawing.Point(155, 45);
            this.nmProdSettingsMinStockQty_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdSettingsMinStockQty_pos.Name = "nmProdSettingsMinStockQty_pos";
            this.nmProdSettingsMinStockQty_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdSettingsMinStockQty_pos.TabIndex = 36;
            this.nmProdSettingsMinStockQty_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmProdSettingsNotifyQty_pos
            // 
            this.nmProdSettingsNotifyQty_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdSettingsNotifyQty_pos.Location = new System.Drawing.Point(155, 76);
            this.nmProdSettingsNotifyQty_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdSettingsNotifyQty_pos.Name = "nmProdSettingsNotifyQty_pos";
            this.nmProdSettingsNotifyQty_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdSettingsNotifyQty_pos.TabIndex = 37;
            this.nmProdSettingsNotifyQty_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdSettingsNotifyQty_pos.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
            // 
            // nmProdSettingsMinCartQty_pos
            // 
            this.nmProdSettingsMinCartQty_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdSettingsMinCartQty_pos.Location = new System.Drawing.Point(155, 107);
            this.nmProdSettingsMinCartQty_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdSettingsMinCartQty_pos.Name = "nmProdSettingsMinCartQty_pos";
            this.nmProdSettingsMinCartQty_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdSettingsMinCartQty_pos.TabIndex = 38;
            this.nmProdSettingsMinCartQty_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmProdSettingsMaxCartQty_pos
            // 
            this.nmProdSettingsMaxCartQty_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdSettingsMaxCartQty_pos.Location = new System.Drawing.Point(155, 138);
            this.nmProdSettingsMaxCartQty_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdSettingsMaxCartQty_pos.Name = "nmProdSettingsMaxCartQty_pos";
            this.nmProdSettingsMaxCartQty_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdSettingsMaxCartQty_pos.TabIndex = 39;
            this.nmProdSettingsMaxCartQty_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // label255
            // 
            this.label255.AutoSize = true;
            this.label255.Location = new System.Drawing.Point(4, 166);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(92, 13);
            this.label255.TabIndex = 40;
            this.label255.Text = "Allowed quantities";
            // 
            // nmProdSettingsAllowedQty_pos
            // 
            this.nmProdSettingsAllowedQty_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdSettingsAllowedQty_pos.Location = new System.Drawing.Point(155, 169);
            this.nmProdSettingsAllowedQty_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdSettingsAllowedQty_pos.Name = "nmProdSettingsAllowedQty_pos";
            this.nmProdSettingsAllowedQty_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdSettingsAllowedQty_pos.TabIndex = 41;
            this.nmProdSettingsAllowedQty_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // tbDefaultProdSettingsAllowedQty_pos
            // 
            this.tbDefaultProdSettingsAllowedQty_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsAllowedQty_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsAllowedQty_pos.Location = new System.Drawing.Point(406, 169);
            this.tbDefaultProdSettingsAllowedQty_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsAllowedQty_pos.Name = "tbDefaultProdSettingsAllowedQty_pos";
            this.tbDefaultProdSettingsAllowedQty_pos.Size = new System.Drawing.Size(324, 20);
            this.tbDefaultProdSettingsAllowedQty_pos.TabIndex = 42;
            // 
            // label261
            // 
            this.label261.AutoSize = true;
            this.label261.Location = new System.Drawing.Point(3, 197);
            this.label261.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label261.Name = "label261";
            this.label261.Size = new System.Drawing.Size(89, 13);
            this.label261.TabIndex = 43;
            this.label261.Text = "Shipping enabled";
            // 
            // label262
            // 
            this.label262.AutoSize = true;
            this.label262.Location = new System.Drawing.Point(3, 228);
            this.label262.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(72, 13);
            this.label262.TabIndex = 44;
            this.label262.Text = "Free Shipping";
            // 
            // label263
            // 
            this.label263.AutoSize = true;
            this.label263.Location = new System.Drawing.Point(3, 259);
            this.label263.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label263.Name = "label263";
            this.label263.Size = new System.Drawing.Size(81, 13);
            this.label263.TabIndex = 45;
            this.label263.Text = "Ship Separately";
            // 
            // label264
            // 
            this.label264.AutoSize = true;
            this.label264.Location = new System.Drawing.Point(3, 290);
            this.label264.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label264.Name = "label264";
            this.label264.Size = new System.Drawing.Size(134, 13);
            this.label264.TabIndex = 46;
            this.label264.Text = "Additional Shipping Charge";
            // 
            // label265
            // 
            this.label265.AutoSize = true;
            this.label265.Location = new System.Drawing.Point(3, 321);
            this.label265.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label265.Name = "label265";
            this.label265.Size = new System.Drawing.Size(71, 13);
            this.label265.TabIndex = 47;
            this.label265.Text = "Delivery Date";
            // 
            // nmProdSettingsFreeShipping_pos
            // 
            this.nmProdSettingsFreeShipping_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdSettingsFreeShipping_pos.Location = new System.Drawing.Point(155, 231);
            this.nmProdSettingsFreeShipping_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdSettingsFreeShipping_pos.Name = "nmProdSettingsFreeShipping_pos";
            this.nmProdSettingsFreeShipping_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdSettingsFreeShipping_pos.TabIndex = 49;
            this.nmProdSettingsFreeShipping_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmProdSettingsShipSeparately_pos
            // 
            this.nmProdSettingsShipSeparately_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdSettingsShipSeparately_pos.Location = new System.Drawing.Point(155, 262);
            this.nmProdSettingsShipSeparately_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdSettingsShipSeparately_pos.Name = "nmProdSettingsShipSeparately_pos";
            this.nmProdSettingsShipSeparately_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdSettingsShipSeparately_pos.TabIndex = 50;
            this.nmProdSettingsShipSeparately_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmProdSettingsShippingCharge_pos
            // 
            this.nmProdSettingsShippingCharge_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdSettingsShippingCharge_pos.Location = new System.Drawing.Point(155, 293);
            this.nmProdSettingsShippingCharge_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdSettingsShippingCharge_pos.Name = "nmProdSettingsShippingCharge_pos";
            this.nmProdSettingsShippingCharge_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdSettingsShippingCharge_pos.TabIndex = 51;
            this.nmProdSettingsShippingCharge_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // nmProdSettingsDeliveryDate_pos
            // 
            this.nmProdSettingsDeliveryDate_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdSettingsDeliveryDate_pos.Location = new System.Drawing.Point(155, 324);
            this.nmProdSettingsDeliveryDate_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdSettingsDeliveryDate_pos.Name = "nmProdSettingsDeliveryDate_pos";
            this.nmProdSettingsDeliveryDate_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdSettingsDeliveryDate_pos.TabIndex = 52;
            this.nmProdSettingsDeliveryDate_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // label267
            // 
            this.label267.AutoSize = true;
            this.label267.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label267.Location = new System.Drawing.Point(754, 1);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(67, 17);
            this.label267.TabIndex = 58;
            this.label267.Text = "Id Value";
            // 
            // chProdSettingsDeliveryDateIsID_pos
            // 
            this.chProdSettingsDeliveryDateIsID_pos.AutoSize = true;
            this.chProdSettingsDeliveryDateIsID_pos.Location = new System.Drawing.Point(754, 324);
            this.chProdSettingsDeliveryDateIsID_pos.Name = "chProdSettingsDeliveryDateIsID_pos";
            this.chProdSettingsDeliveryDateIsID_pos.Size = new System.Drawing.Size(15, 14);
            this.chProdSettingsDeliveryDateIsID_pos.TabIndex = 59;
            this.chProdSettingsDeliveryDateIsID_pos.UseVisualStyleBackColor = true;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(3, 352);
            this.label50.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(92, 13);
            this.label50.TabIndex = 60;
            this.label50.Text = "Visible Individually";
            // 
            // nmProdSettingsVisibleIndvidually_pos
            // 
            this.nmProdSettingsVisibleIndvidually_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdSettingsVisibleIndvidually_pos.Location = new System.Drawing.Point(155, 355);
            this.nmProdSettingsVisibleIndvidually_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdSettingsVisibleIndvidually_pos.Name = "nmProdSettingsVisibleIndvidually_pos";
            this.nmProdSettingsVisibleIndvidually_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdSettingsVisibleIndvidually_pos.TabIndex = 61;
            this.nmProdSettingsVisibleIndvidually_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // tbDefaultProdSettingsVisibleIndvidually_pos
            // 
            this.tbDefaultProdSettingsVisibleIndvidually_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsVisibleIndvidually_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsVisibleIndvidually_pos.Location = new System.Drawing.Point(406, 355);
            this.tbDefaultProdSettingsVisibleIndvidually_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsVisibleIndvidually_pos.Name = "tbDefaultProdSettingsVisibleIndvidually_pos";
            this.tbDefaultProdSettingsVisibleIndvidually_pos.Size = new System.Drawing.Size(324, 20);
            this.tbDefaultProdSettingsVisibleIndvidually_pos.TabIndex = 62;
            // 
            // nmProdSettingsIsDeleted_pos
            // 
            this.nmProdSettingsIsDeleted_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmProdSettingsIsDeleted_pos.Location = new System.Drawing.Point(155, 386);
            this.nmProdSettingsIsDeleted_pos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nmProdSettingsIsDeleted_pos.Name = "nmProdSettingsIsDeleted_pos";
            this.nmProdSettingsIsDeleted_pos.Size = new System.Drawing.Size(120, 20);
            this.nmProdSettingsIsDeleted_pos.TabIndex = 63;
            this.nmProdSettingsIsDeleted_pos.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(3, 383);
            this.label60.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(55, 13);
            this.label60.TabIndex = 64;
            this.label60.Text = "Is Deleted";
            // 
            // tbDefaultProdSettingsIsDeleted_pos
            // 
            this.tbDefaultProdSettingsIsDeleted_pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefaultProdSettingsIsDeleted_pos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDefaultProdSettingsIsDeleted_pos.Location = new System.Drawing.Point(406, 386);
            this.tbDefaultProdSettingsIsDeleted_pos.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.tbDefaultProdSettingsIsDeleted_pos.Name = "tbDefaultProdSettingsIsDeleted_pos";
            this.tbDefaultProdSettingsIsDeleted_pos.Size = new System.Drawing.Size(324, 20);
            this.tbDefaultProdSettingsIsDeleted_pos.TabIndex = 65;
            // 
            // tabAliExpress
            // 
            this.tabAliExpress.Controls.Add(this.lblAliExpressUrlLabel);
            this.tabAliExpress.Controls.Add(this.tbAliexpressProductsUrls);
            this.tabAliExpress.Location = new System.Drawing.Point(4, 22);
            this.tabAliExpress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabAliExpress.Name = "tabAliExpress";
            this.tabAliExpress.Size = new System.Drawing.Size(924, 549);
            this.tabAliExpress.TabIndex = 24;
            this.tabAliExpress.Text = "AliExpress Products Url Addresses";
            this.tabAliExpress.UseVisualStyleBackColor = true;
            // 
            // lblAliExpressUrlLabel
            // 
            this.lblAliExpressUrlLabel.AutoSize = true;
            this.lblAliExpressUrlLabel.Location = new System.Drawing.Point(7, 12);
            this.lblAliExpressUrlLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAliExpressUrlLabel.Name = "lblAliExpressUrlLabel";
            this.lblAliExpressUrlLabel.Size = new System.Drawing.Size(329, 52);
            this.lblAliExpressUrlLabel.TabIndex = 1;
            this.lblAliExpressUrlLabel.Text = "Enter url addreses of products. After each url address enter new line.\r\nExample:\r" +
    "\nhttps://www.aliexpress.com/item/item1\r\nhttps://www.aliexpress.com/item/item2";
            // 
            // tbAliexpressProductsUrls
            // 
            this.tbAliexpressProductsUrls.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAliexpressProductsUrls.Location = new System.Drawing.Point(6, 85);
            this.tbAliexpressProductsUrls.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbAliexpressProductsUrls.Multiline = true;
            this.tbAliexpressProductsUrls.Name = "tbAliexpressProductsUrls";
            this.tbAliexpressProductsUrls.Size = new System.Drawing.Size(915, 403);
            this.tbAliexpressProductsUrls.TabIndex = 0;
            // 
            // statusArea
            // 
            this.statusArea.AutoSize = false;
            this.statusArea.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusArea.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusAreaText});
            this.statusArea.Location = new System.Drawing.Point(0, 604);
            this.statusArea.Name = "statusArea";
            this.statusArea.Size = new System.Drawing.Size(935, 22);
            this.statusArea.TabIndex = 24;
            this.statusArea.Text = "statusArea";
            // 
            // statusAreaText
            // 
            this.statusAreaText.Name = "statusAreaText";
            this.statusAreaText.Size = new System.Drawing.Size(0, 17);
            // 
            // btnTestMapping
            // 
            this.btnTestMapping.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTestMapping.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTestMapping.Location = new System.Drawing.Point(585, 576);
            this.btnTestMapping.Name = "btnTestMapping";
            this.btnTestMapping.Size = new System.Drawing.Size(94, 23);
            this.btnTestMapping.TabIndex = 27;
            this.btnTestMapping.Text = "Preview Items";
            this.btnTestMapping.UseVisualStyleBackColor = true;
            this.btnTestMapping.Click += new System.EventHandler(this.btnTestMapping_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(848, 576);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 26;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(766, 576);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 25;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnQuickImport
            // 
            this.btnQuickImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuickImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuickImport.Location = new System.Drawing.Point(485, 576);
            this.btnQuickImport.Name = "btnQuickImport";
            this.btnQuickImport.Size = new System.Drawing.Size(94, 23);
            this.btnQuickImport.TabIndex = 28;
            this.btnQuickImport.Text = "Quick Import";
            this.btnQuickImport.UseVisualStyleBackColor = true;
            this.btnQuickImport.Visible = false;
            this.btnQuickImport.Click += new System.EventHandler(this.btnQuickImport_Click);
            // 
            // btnCopy
            // 
            this.btnCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCopy.Location = new System.Drawing.Point(685, 576);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(75, 23);
            this.btnCopy.TabIndex = 29;
            this.btnCopy.Text = "Copy";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // cbSourceParser
            // 
            this.cbSourceParser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbSourceParser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSourceParser.DropDownWidth = 525;
            this.cbSourceParser.FormattingEnabled = true;
            this.cbSourceParser.Location = new System.Drawing.Point(4, 579);
            this.cbSourceParser.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbSourceParser.Name = "cbSourceParser";
            this.cbSourceParser.Size = new System.Drawing.Size(116, 21);
            this.cbSourceParser.TabIndex = 40;
            this.cbSourceParser.Tag = "dd";
            // 
            // xmlModify
            // 
            this.xmlModify.AutoSize = true;
            this.xmlModify.Location = new System.Drawing.Point(281, 464);
            this.xmlModify.Name = "xmlModify";
            this.xmlModify.Size = new System.Drawing.Size(77, 17);
            this.xmlModify.TabIndex = 40;
            this.xmlModify.Text = "Xml Modify";
            this.xmlModify.UseVisualStyleBackColor = true;
            // 
            // vendorNewEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 626);
            this.Controls.Add(this.btnCopy);
            this.Controls.Add(this.btnQuickImport);
            this.Controls.Add(this.btnTestMapping);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.statusArea);
            this.Controls.Add(this.tabSourceConfig);
            this.Controls.Add(this.cbSourceParser);
            this.Name = "vendorNewEdit";
            this.Text = "Vendor Source Configuration";
            this.Load += new System.EventHandler(this.vendorNewEdit_Load);
            this.Shown += new System.EventHandler(this.vendorNewEdit_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.tabSourceConfig.ResumeLayout(false);
            this.tabVendor.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tabVendorSource.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.pSourceFormat.ResumeLayout(false);
            this.pSourceFormat.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmFirstRowDataIndex)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabProductInfoMappingXml.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabSeoMappingXml.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tabCatMappingXml.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tabAccessRolesMappingXml.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.tabManufMappingXml.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tabFilterMappingXml.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tabProductInfoMappingPos.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdId_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdName_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdShortDesc_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdFullDesc_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdManPartNum_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdStock_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSKU_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmGtin_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmWeight_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmLength_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmWidth_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmHeight_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdGroupBy_pos)).EndInit();
            this.tabSeoMappingPos.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmSeoMetaKey_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmSeoMetaDesc_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmSeoMetaTitle_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmSeoSearchPage_pos)).EndInit();
            this.tabCatMappingPos.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmCategory_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCategory1_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCategory2_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCategory3_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCategory4_pos)).EndInit();
            this.tabAccessRolesMappingPos.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmCustomerRole_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCustomerRole1_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCustomerRole2_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCustomerRole3_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCustomerRole4_pos)).EndInit();
            this.tabManufMappingPos.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmVendor_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmManufacturer_pos)).EndInit();
            this.tabFilterMappingPos.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilterValue_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter1Value_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter2Value_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter3Value_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter4Value_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter5Value_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter6Value_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter7Value_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter8Value_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFilter9Value_pos)).EndInit();
            this.tabProductSettingsXml.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            this.tableLayoutPanel23.PerformLayout();
            this.tabProductSettingsPos.ResumeLayout(false);
            this.tableLayoutPanel24.ResumeLayout(false);
            this.tableLayoutPanel24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsShippingEnabled_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsMinStockQty_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsNotifyQty_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsMinCartQty_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsMaxCartQty_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsAllowedQty_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsFreeShipping_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsShipSeparately_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsShippingCharge_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsDeliveryDate_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsVisibleIndvidually_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmProdSettingsIsDeleted_pos)).EndInit();
            this.tabAliExpress.ResumeLayout(false);
            this.tabAliExpress.PerformLayout();
            this.statusArea.ResumeLayout(false);
            this.statusArea.PerformLayout();
            this.ResumeLayout(false);

		}

		// Token: 0x0400045B RID: 1115
		private global::System.ComponentModel.IContainer components;

		// Token: 0x0400045C RID: 1116
		private global::System.Windows.Forms.ErrorProvider errorProvider1;

		// Token: 0x0400045D RID: 1117
		private global::System.Windows.Forms.TabControl tabSourceConfig;

		// Token: 0x0400045E RID: 1118
		private global::System.Windows.Forms.TabPage tabVendor;

		// Token: 0x0400045F RID: 1119
		private global::System.Windows.Forms.TextBox tbDescription;

		// Token: 0x04000460 RID: 1120
		private global::System.Windows.Forms.Label label3;

		// Token: 0x04000461 RID: 1121
		private global::System.Windows.Forms.TextBox tbName;

		// Token: 0x04000462 RID: 1122
		private global::System.Windows.Forms.Label label2;

		// Token: 0x04000463 RID: 1123
		private global::System.Windows.Forms.TextBox tbId;

		// Token: 0x04000464 RID: 1124
		private global::System.Windows.Forms.Label label1;

		// Token: 0x04000465 RID: 1125
		private global::System.Windows.Forms.TabPage tabVendorSource;

		// Token: 0x04000466 RID: 1126
		private global::System.Windows.Forms.Button btnTestSource;

		// Token: 0x04000467 RID: 1127
		private global::System.Windows.Forms.ComboBox ddSourceFormat;

		// Token: 0x04000468 RID: 1128
		private global::System.Windows.Forms.Label label9;

		// Token: 0x04000469 RID: 1129
		private global::System.Windows.Forms.TextBox tbSourcePassword;

		// Token: 0x0400046A RID: 1130
		private global::System.Windows.Forms.Label label8;

		// Token: 0x0400046B RID: 1131
		private global::System.Windows.Forms.TextBox tbSourceUserName;

		// Token: 0x0400046C RID: 1132
		private global::System.Windows.Forms.Label label7;

		// Token: 0x0400046D RID: 1133
		private global::System.Windows.Forms.Label label4;

		// Token: 0x0400046E RID: 1134
		private global::System.Windows.Forms.ComboBox ddSourceType;

		// Token: 0x0400046F RID: 1135
		private global::System.Windows.Forms.Label label6;

		// Token: 0x04000470 RID: 1136
		private global::System.Windows.Forms.TabPage tabProductInfoMappingXml;

		// Token: 0x04000471 RID: 1137
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

		// Token: 0x04000472 RID: 1138
		private global::System.Windows.Forms.Label label25;

		// Token: 0x04000473 RID: 1139
		private global::System.Windows.Forms.Label label24;

		// Token: 0x04000474 RID: 1140
		private global::System.Windows.Forms.Label label22;

		// Token: 0x04000475 RID: 1141
		private global::System.Windows.Forms.Label label20;

		// Token: 0x04000476 RID: 1142
		private global::System.Windows.Forms.Label label18;

		// Token: 0x04000477 RID: 1143
		private global::System.Windows.Forms.Label label17;

		// Token: 0x04000478 RID: 1144
		private global::System.Windows.Forms.Label label16;

		// Token: 0x04000479 RID: 1145
		private global::System.Windows.Forms.Label label15;

		// Token: 0x0400047A RID: 1146
		private global::System.Windows.Forms.Label label21;

		// Token: 0x0400047B RID: 1147
		private global::System.Windows.Forms.TextBox tbProductRepeatNodeXpath;

		// Token: 0x0400047C RID: 1148
		private global::System.Windows.Forms.TextBox tbProdIDXpath;

		// Token: 0x0400047D RID: 1149
		private global::System.Windows.Forms.TextBox tbProdNameXpath;

		// Token: 0x0400047E RID: 1150
		private global::System.Windows.Forms.TextBox tbProdShortDescXpath;

		// Token: 0x0400047F RID: 1151
		private global::System.Windows.Forms.TextBox tbProdFullDescXpath;

		// Token: 0x04000480 RID: 1152
		private global::System.Windows.Forms.TextBox tbProdManNumberXpath;

		// Token: 0x04000481 RID: 1153
		private global::System.Windows.Forms.TextBox tbProdStockQuantityXpath;

		// Token: 0x04000482 RID: 1154
		private global::System.Windows.Forms.Label label26;

		// Token: 0x04000483 RID: 1155
		private global::System.Windows.Forms.TextBox tbDefaultProductName_xml;

		// Token: 0x04000484 RID: 1156
		private global::System.Windows.Forms.TextBox tbDefaultProdShortDesc_xml;

		// Token: 0x04000485 RID: 1157
		private global::System.Windows.Forms.TextBox tbDefaultProdFullDesc_xml;

		// Token: 0x04000486 RID: 1158
		private global::System.Windows.Forms.TextBox tbDefaultManufacturerPnum_xml;

		// Token: 0x04000487 RID: 1159
		private global::System.Windows.Forms.TextBox nmDefaultStockQuantity_xml;

		// Token: 0x04000488 RID: 1160
		private global::System.Windows.Forms.TabPage tabSeoMappingXml;

		// Token: 0x04000489 RID: 1161
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;

		// Token: 0x0400048A RID: 1162
		private global::System.Windows.Forms.Label label27;

		// Token: 0x0400048B RID: 1163
		private global::System.Windows.Forms.Label label28;

		// Token: 0x0400048C RID: 1164
		private global::System.Windows.Forms.Label label29;

		// Token: 0x0400048D RID: 1165
		private global::System.Windows.Forms.TextBox tbDefaultSearchEnginePage;

		// Token: 0x0400048E RID: 1166
		private global::System.Windows.Forms.TextBox tbSearchEnginePageXpath;

		// Token: 0x0400048F RID: 1167
		private global::System.Windows.Forms.TextBox tbDefaultMetaTitle;

		// Token: 0x04000490 RID: 1168
		private global::System.Windows.Forms.TextBox tbMetaTitleXpath;

		// Token: 0x04000491 RID: 1169
		private global::System.Windows.Forms.Label label32;

		// Token: 0x04000492 RID: 1170
		private global::System.Windows.Forms.TextBox tbDefaultMetaDescription;

		// Token: 0x04000493 RID: 1171
		private global::System.Windows.Forms.TextBox tbDefaultMetaKeywords;

		// Token: 0x04000494 RID: 1172
		private global::System.Windows.Forms.Label label30;

		// Token: 0x04000495 RID: 1173
		private global::System.Windows.Forms.TextBox tbMetaKeywordsXpath;

		// Token: 0x04000496 RID: 1174
		private global::System.Windows.Forms.Label label31;

		// Token: 0x04000497 RID: 1175
		private global::System.Windows.Forms.TextBox tbMetaDescriptionXpath;

		// Token: 0x04000498 RID: 1176
		private global::System.Windows.Forms.Label label33;

		// Token: 0x04000499 RID: 1177
		private global::System.Windows.Forms.TabPage tabCatMappingXml;

		// Token: 0x0400049A RID: 1178
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;

		// Token: 0x0400049B RID: 1179
		private global::System.Windows.Forms.Label label35;

		// Token: 0x0400049C RID: 1180
		private global::System.Windows.Forms.Label label36;

		// Token: 0x0400049D RID: 1181
		private global::System.Windows.Forms.Label label38;

		// Token: 0x0400049E RID: 1182
		private global::System.Windows.Forms.TextBox tbCategoryXPath;

		// Token: 0x0400049F RID: 1183
		private global::System.Windows.Forms.Label label37;

		// Token: 0x040004A0 RID: 1184
		private global::System.Windows.Forms.TextBox tbDefaultCategory_xml;

		// Token: 0x040004A1 RID: 1185
		private global::System.Windows.Forms.Label label34;

		// Token: 0x040004A2 RID: 1186
		private global::System.Windows.Forms.TextBox tbCategoryDelimeter_xml;

		// Token: 0x040004A3 RID: 1187
		private global::System.Windows.Forms.Label label39;

		// Token: 0x040004A4 RID: 1188
		private global::System.Windows.Forms.Label label40;

		// Token: 0x040004A5 RID: 1189
		private global::System.Windows.Forms.Label label41;

		// Token: 0x040004A6 RID: 1190
		private global::System.Windows.Forms.Label label42;

		// Token: 0x040004A7 RID: 1191
		private global::System.Windows.Forms.TextBox tbCategoryXPath1;

		// Token: 0x040004A8 RID: 1192
		private global::System.Windows.Forms.TextBox tbCategoryXPath2;

		// Token: 0x040004A9 RID: 1193
		private global::System.Windows.Forms.TextBox tbCategoryXPath3;

		// Token: 0x040004AA RID: 1194
		private global::System.Windows.Forms.TextBox tbCategoryXPath4;

		// Token: 0x040004AB RID: 1195
		private global::System.Windows.Forms.TextBox tbDefaultCategory1_xml;

		// Token: 0x040004AC RID: 1196
		private global::System.Windows.Forms.TextBox tbDefaultCategory2_xml;

		// Token: 0x040004AD RID: 1197
		private global::System.Windows.Forms.TextBox tbDefaultCategory3_xml;

		// Token: 0x040004AE RID: 1198
		private global::System.Windows.Forms.TextBox tbDefaultCategory4_xml;

		// Token: 0x040004AF RID: 1199
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;

		// Token: 0x040004B0 RID: 1200
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;

		// Token: 0x040004B1 RID: 1201
		private global::System.Windows.Forms.TabPage tabManufMappingXml;

		// Token: 0x040004B2 RID: 1202
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;

		// Token: 0x040004B3 RID: 1203
		private global::System.Windows.Forms.TextBox tbDefaultManufacturer_xml;

		// Token: 0x040004B4 RID: 1204
		private global::System.Windows.Forms.Label label44;

		// Token: 0x040004B5 RID: 1205
		private global::System.Windows.Forms.Label label45;

		// Token: 0x040004B6 RID: 1206
		private global::System.Windows.Forms.Label label46;

		// Token: 0x040004B7 RID: 1207
		private global::System.Windows.Forms.Label label47;

		// Token: 0x040004B8 RID: 1208
		private global::System.Windows.Forms.TextBox tbManufacturerXPath;

		// Token: 0x040004B9 RID: 1209
		private global::System.Windows.Forms.TabPage tabFilterMappingXml;

		// Token: 0x040004BA RID: 1210
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;

		// Token: 0x040004BB RID: 1211
		private global::System.Windows.Forms.TextBox tbFilterXpath;

		// Token: 0x040004BC RID: 1212
		private global::System.Windows.Forms.Label label65;

		// Token: 0x040004BD RID: 1213
		private global::System.Windows.Forms.Label label66;

		// Token: 0x040004BE RID: 1214
		private global::System.Windows.Forms.Label label67;

		// Token: 0x040004BF RID: 1215
		private global::System.Windows.Forms.Label label68;

		// Token: 0x040004C0 RID: 1216
		private global::System.Windows.Forms.TextBox tbFilterName_xml;

		// Token: 0x040004C1 RID: 1217
		private global::System.Windows.Forms.Label label69;

		// Token: 0x040004C2 RID: 1218
		private global::System.Windows.Forms.Label label70;

		// Token: 0x040004C3 RID: 1219
		private global::System.Windows.Forms.Label label71;

		// Token: 0x040004C4 RID: 1220
		private global::System.Windows.Forms.Label label72;

		// Token: 0x040004C5 RID: 1221
		private global::System.Windows.Forms.Label label73;

		// Token: 0x040004C6 RID: 1222
		private global::System.Windows.Forms.Label label74;

		// Token: 0x040004C7 RID: 1223
		private global::System.Windows.Forms.Label label75;

		// Token: 0x040004C8 RID: 1224
		private global::System.Windows.Forms.Label label76;

		// Token: 0x040004C9 RID: 1225
		private global::System.Windows.Forms.Label label77;

		// Token: 0x040004CA RID: 1226
		private global::System.Windows.Forms.TextBox tbFilterName1_xml;

		// Token: 0x040004CB RID: 1227
		private global::System.Windows.Forms.TextBox tbFilterName2_xml;

		// Token: 0x040004CC RID: 1228
		private global::System.Windows.Forms.TextBox tbFilterName3_xml;

		// Token: 0x040004CD RID: 1229
		private global::System.Windows.Forms.TextBox tbFilterName4_xml;

		// Token: 0x040004CE RID: 1230
		private global::System.Windows.Forms.TextBox tbFilterName5_xml;

		// Token: 0x040004CF RID: 1231
		private global::System.Windows.Forms.TextBox tbFilterName6_xml;

		// Token: 0x040004D0 RID: 1232
		private global::System.Windows.Forms.TextBox tbFilterName7_xml;

		// Token: 0x040004D1 RID: 1233
		private global::System.Windows.Forms.TextBox tbFilterName8_xml;

		// Token: 0x040004D2 RID: 1234
		private global::System.Windows.Forms.TextBox tbFilterName9_xml;

		// Token: 0x040004D3 RID: 1235
		private global::System.Windows.Forms.TextBox tbFilterXpath1;

		// Token: 0x040004D4 RID: 1236
		private global::System.Windows.Forms.TextBox tbFilterXpath2;

		// Token: 0x040004D5 RID: 1237
		private global::System.Windows.Forms.TextBox tbFilterXpath3;

		// Token: 0x040004D6 RID: 1238
		private global::System.Windows.Forms.TextBox tbFilterXpath4;

		// Token: 0x040004D7 RID: 1239
		private global::System.Windows.Forms.TextBox tbFilterXpath5;

		// Token: 0x040004D8 RID: 1240
		private global::System.Windows.Forms.TextBox tbFilterXpath6;

		// Token: 0x040004D9 RID: 1241
		private global::System.Windows.Forms.TextBox tbFilterXpath7;

		// Token: 0x040004DA RID: 1242
		private global::System.Windows.Forms.TextBox tbFilterXpath8;

		// Token: 0x040004DB RID: 1243
		private global::System.Windows.Forms.TextBox tbFilterXpath9;

		// Token: 0x040004DC RID: 1244
		private global::System.Windows.Forms.TabPage tabProductInfoMappingPos;

		// Token: 0x040004DD RID: 1245
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;

		// Token: 0x040004DE RID: 1246
		private global::System.Windows.Forms.Label label82;

		// Token: 0x040004DF RID: 1247
		private global::System.Windows.Forms.Label label89;

		// Token: 0x040004E0 RID: 1248
		private global::System.Windows.Forms.Label label90;

		// Token: 0x040004E1 RID: 1249
		private global::System.Windows.Forms.Label label86;

		// Token: 0x040004E2 RID: 1250
		private global::System.Windows.Forms.Label label85;

		// Token: 0x040004E3 RID: 1251
		private global::System.Windows.Forms.Label label84;

		// Token: 0x040004E4 RID: 1252
		private global::System.Windows.Forms.Label label83;

		// Token: 0x040004E5 RID: 1253
		private global::System.Windows.Forms.Label label80;

		// Token: 0x040004E6 RID: 1254
		private global::System.Windows.Forms.Label label91;

		// Token: 0x040004E7 RID: 1255
		private global::System.Windows.Forms.TextBox tbDefaultProdName_pos;

		// Token: 0x040004E8 RID: 1256
		private global::System.Windows.Forms.TextBox tbDefaultProdShortDesc_pos;

		// Token: 0x040004E9 RID: 1257
		private global::System.Windows.Forms.TextBox tbDefaultProdFullDesc_pos;

		// Token: 0x040004EA RID: 1258
		private global::System.Windows.Forms.TextBox tbProdManPartNum_pos;

		// Token: 0x040004EB RID: 1259
		private global::System.Windows.Forms.TextBox nmDefaultProdStock_pos;

		// Token: 0x040004EC RID: 1260
		private global::System.Windows.Forms.NumericUpDown nmProdId_pos;

		// Token: 0x040004ED RID: 1261
		private global::System.Windows.Forms.NumericUpDown nmProdName_pos;

		// Token: 0x040004EE RID: 1262
		private global::System.Windows.Forms.NumericUpDown nmProdShortDesc_pos;

		// Token: 0x040004EF RID: 1263
		private global::System.Windows.Forms.NumericUpDown nmProdFullDesc_pos;

		// Token: 0x040004F0 RID: 1264
		private global::System.Windows.Forms.NumericUpDown nmProdManPartNum_pos;

		// Token: 0x040004F1 RID: 1265
		private global::System.Windows.Forms.NumericUpDown nmProdStock_pos;

		// Token: 0x040004F2 RID: 1266
		private global::System.Windows.Forms.TabPage tabSeoMappingPos;

		// Token: 0x040004F3 RID: 1267
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;

		// Token: 0x040004F4 RID: 1268
		private global::System.Windows.Forms.TextBox tbDefaultSeoSearchPage_pos;

		// Token: 0x040004F5 RID: 1269
		private global::System.Windows.Forms.TextBox tbDefaultSeoMetaTitle_pos;

		// Token: 0x040004F6 RID: 1270
		private global::System.Windows.Forms.Label label81;

		// Token: 0x040004F7 RID: 1271
		private global::System.Windows.Forms.TextBox tbDefaultSeoMetaDesc_pos;

		// Token: 0x040004F8 RID: 1272
		private global::System.Windows.Forms.TextBox tbDefaultSeoMetaKey_pos;

		// Token: 0x040004F9 RID: 1273
		private global::System.Windows.Forms.Label label92;

		// Token: 0x040004FA RID: 1274
		private global::System.Windows.Forms.Label label94;

		// Token: 0x040004FB RID: 1275
		private global::System.Windows.Forms.Label label95;

		// Token: 0x040004FC RID: 1276
		private global::System.Windows.Forms.Label label96;

		// Token: 0x040004FD RID: 1277
		private global::System.Windows.Forms.Label label97;

		// Token: 0x040004FE RID: 1278
		private global::System.Windows.Forms.Label label98;

		// Token: 0x040004FF RID: 1279
		private global::System.Windows.Forms.NumericUpDown nmSeoMetaKey_pos;

		// Token: 0x04000500 RID: 1280
		private global::System.Windows.Forms.NumericUpDown nmSeoMetaDesc_pos;

		// Token: 0x04000501 RID: 1281
		private global::System.Windows.Forms.NumericUpDown nmSeoMetaTitle_pos;

		// Token: 0x04000502 RID: 1282
		private global::System.Windows.Forms.NumericUpDown nmSeoSearchPage_pos;

		// Token: 0x04000503 RID: 1283
		private global::System.Windows.Forms.TabPage tabCatMappingPos;

		// Token: 0x04000504 RID: 1284
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;

		// Token: 0x04000505 RID: 1285
		private global::System.Windows.Forms.Label label93;

		// Token: 0x04000506 RID: 1286
		private global::System.Windows.Forms.Label label100;

		// Token: 0x04000507 RID: 1287
		private global::System.Windows.Forms.Label label101;

		// Token: 0x04000508 RID: 1288
		private global::System.Windows.Forms.TextBox tbDefaultCategory_pos;

		// Token: 0x04000509 RID: 1289
		private global::System.Windows.Forms.Label label102;

		// Token: 0x0400050A RID: 1290
		private global::System.Windows.Forms.TextBox tbCategoryDelimeter_pos;

		// Token: 0x0400050B RID: 1291
		private global::System.Windows.Forms.Label label103;

		// Token: 0x0400050C RID: 1292
		private global::System.Windows.Forms.Label label104;

		// Token: 0x0400050D RID: 1293
		private global::System.Windows.Forms.Label label105;

		// Token: 0x0400050E RID: 1294
		private global::System.Windows.Forms.Label label106;

		// Token: 0x0400050F RID: 1295
		private global::System.Windows.Forms.TextBox tbDefaultCategory1_pos;

		// Token: 0x04000510 RID: 1296
		private global::System.Windows.Forms.TextBox tbDefaultCategory2_pos;

		// Token: 0x04000511 RID: 1297
		private global::System.Windows.Forms.TextBox tbDefaultCategory3_pos;

		// Token: 0x04000512 RID: 1298
		private global::System.Windows.Forms.TextBox tbDefaultCategory4_pos;

		// Token: 0x04000513 RID: 1299
		private global::System.Windows.Forms.Label label99;

		// Token: 0x04000514 RID: 1300
		private global::System.Windows.Forms.NumericUpDown nmCategory_pos;

		// Token: 0x04000515 RID: 1301
		private global::System.Windows.Forms.NumericUpDown nmCategory1_pos;

		// Token: 0x04000516 RID: 1302
		private global::System.Windows.Forms.NumericUpDown nmCategory2_pos;

		// Token: 0x04000517 RID: 1303
		private global::System.Windows.Forms.NumericUpDown nmCategory3_pos;

		// Token: 0x04000518 RID: 1304
		private global::System.Windows.Forms.NumericUpDown nmCategory4_pos;

		// Token: 0x04000519 RID: 1305
		private global::System.Windows.Forms.TabPage tabManufMappingPos;

		// Token: 0x0400051A RID: 1306
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;

		// Token: 0x0400051B RID: 1307
		private global::System.Windows.Forms.TextBox tbDefaultManufacturer_pos;

		// Token: 0x0400051C RID: 1308
		private global::System.Windows.Forms.Label label107;

		// Token: 0x0400051D RID: 1309
		private global::System.Windows.Forms.Label label109;

		// Token: 0x0400051E RID: 1310
		private global::System.Windows.Forms.Label label110;

		// Token: 0x0400051F RID: 1311
		private global::System.Windows.Forms.NumericUpDown nmManufacturer_pos;

		// Token: 0x04000520 RID: 1312
		private global::System.Windows.Forms.Label label108;

		// Token: 0x04000521 RID: 1313
		private global::System.Windows.Forms.TabPage tabFilterMappingPos;

		// Token: 0x04000522 RID: 1314
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;

		// Token: 0x04000523 RID: 1315
		private global::System.Windows.Forms.Label label129;

		// Token: 0x04000524 RID: 1316
		private global::System.Windows.Forms.Label label131;

		// Token: 0x04000525 RID: 1317
		private global::System.Windows.Forms.Label label133;

		// Token: 0x04000526 RID: 1318
		private global::System.Windows.Forms.TextBox tbFilterName_pos;

		// Token: 0x04000527 RID: 1319
		private global::System.Windows.Forms.Label label134;

		// Token: 0x04000528 RID: 1320
		private global::System.Windows.Forms.Label label135;

		// Token: 0x04000529 RID: 1321
		private global::System.Windows.Forms.Label label136;

		// Token: 0x0400052A RID: 1322
		private global::System.Windows.Forms.Label label137;

		// Token: 0x0400052B RID: 1323
		private global::System.Windows.Forms.Label label138;

		// Token: 0x0400052C RID: 1324
		private global::System.Windows.Forms.Label label139;

		// Token: 0x0400052D RID: 1325
		private global::System.Windows.Forms.Label label140;

		// Token: 0x0400052E RID: 1326
		private global::System.Windows.Forms.Label label141;

		// Token: 0x0400052F RID: 1327
		private global::System.Windows.Forms.Label label142;

		// Token: 0x04000530 RID: 1328
		private global::System.Windows.Forms.TextBox tbFilter1Name_pos;

		// Token: 0x04000531 RID: 1329
		private global::System.Windows.Forms.TextBox tbFilter2Name_pos;

		// Token: 0x04000532 RID: 1330
		private global::System.Windows.Forms.TextBox tbFilter3Name_pos;

		// Token: 0x04000533 RID: 1331
		private global::System.Windows.Forms.TextBox tbFilter4Name_pos;

		// Token: 0x04000534 RID: 1332
		private global::System.Windows.Forms.TextBox tbFilter5Name_pos;

		// Token: 0x04000535 RID: 1333
		private global::System.Windows.Forms.TextBox tbFilter6Name_pos;

		// Token: 0x04000536 RID: 1334
		private global::System.Windows.Forms.TextBox tbFilter7Name_pos;

		// Token: 0x04000537 RID: 1335
		private global::System.Windows.Forms.TextBox tbFilter8Name_pos;

		// Token: 0x04000538 RID: 1336
		private global::System.Windows.Forms.TextBox tbFilter9Name_pos;

		// Token: 0x04000539 RID: 1337
		private global::System.Windows.Forms.NumericUpDown nmFilterValue_pos;

		// Token: 0x0400053A RID: 1338
		private global::System.Windows.Forms.NumericUpDown nmFilter1Value_pos;

		// Token: 0x0400053B RID: 1339
		private global::System.Windows.Forms.NumericUpDown nmFilter2Value_pos;

		// Token: 0x0400053C RID: 1340
		private global::System.Windows.Forms.NumericUpDown nmFilter3Value_pos;

		// Token: 0x0400053D RID: 1341
		private global::System.Windows.Forms.NumericUpDown nmFilter4Value_pos;

		// Token: 0x0400053E RID: 1342
		private global::System.Windows.Forms.NumericUpDown nmFilter5Value_pos;

		// Token: 0x0400053F RID: 1343
		private global::System.Windows.Forms.NumericUpDown nmFilter6Value_pos;

		// Token: 0x04000540 RID: 1344
		private global::System.Windows.Forms.NumericUpDown nmFilter7Value_pos;

		// Token: 0x04000541 RID: 1345
		private global::System.Windows.Forms.NumericUpDown nmFilter8Value_pos;

		// Token: 0x04000542 RID: 1346
		private global::System.Windows.Forms.NumericUpDown nmFilter9Value_pos;

		// Token: 0x04000543 RID: 1347
		private global::System.Windows.Forms.Label label132;

		// Token: 0x04000544 RID: 1348
		private global::System.Windows.Forms.Panel pSourceFormat;

		// Token: 0x04000545 RID: 1349
		private global::System.Windows.Forms.Label lblFirstDataRowIndex;

		// Token: 0x04000546 RID: 1350
		private global::System.Windows.Forms.NumericUpDown nmFirstRowDataIndex;

		// Token: 0x04000547 RID: 1351
		private global::System.Windows.Forms.TextBox tbCsvDelimeter;

		// Token: 0x04000548 RID: 1352
		private global::System.Windows.Forms.Label lblCsvDelimeter;

		// Token: 0x04000549 RID: 1353
		private global::System.Windows.Forms.StatusStrip statusArea;

		// Token: 0x0400054A RID: 1354
		private global::System.Windows.Forms.ToolStripStatusLabel statusAreaText;

		// Token: 0x0400054B RID: 1355
		private global::System.Windows.Forms.Label label5;

		// Token: 0x0400054C RID: 1356
		private global::System.Windows.Forms.TextBox tbProdSKUXpath;

		// Token: 0x0400054D RID: 1357
		private global::System.Windows.Forms.Label label11;

		// Token: 0x0400054E RID: 1358
		private global::System.Windows.Forms.NumericUpDown nmProdSKU_pos;

		// Token: 0x0400054F RID: 1359
		private global::System.Windows.Forms.TextBox tbSoapRequest;

		// Token: 0x04000550 RID: 1360
		private global::System.Windows.Forms.TextBox tbSoapAction;

		// Token: 0x04000551 RID: 1361
		private global::System.Windows.Forms.Label lblSoapAction;

		// Token: 0x04000552 RID: 1362
		private global::System.Windows.Forms.Label lblSoapRequest;

		// Token: 0x04000553 RID: 1363
		private global::System.Windows.Forms.Label label14;

		// Token: 0x04000554 RID: 1364
		private global::System.Windows.Forms.TextBox tbDefaultVendor_xml;

		// Token: 0x04000555 RID: 1365
		private global::System.Windows.Forms.Label label13;

		// Token: 0x04000556 RID: 1366
		private global::System.Windows.Forms.TextBox tbVendorXpath;

		// Token: 0x04000557 RID: 1367
		private global::System.Windows.Forms.Label label143;

		// Token: 0x04000558 RID: 1368
		private global::System.Windows.Forms.CheckBox chVendorId_xml;

		// Token: 0x04000559 RID: 1369
		private global::System.Windows.Forms.CheckBox chManufacturerId_xml;

		// Token: 0x0400055A RID: 1370
		private global::System.Windows.Forms.CheckBox chCatId_xml;

		// Token: 0x0400055B RID: 1371
		private global::System.Windows.Forms.CheckBox chCat1Id_xml;

		// Token: 0x0400055C RID: 1372
		private global::System.Windows.Forms.CheckBox chCat2Id_xml;

		// Token: 0x0400055D RID: 1373
		private global::System.Windows.Forms.CheckBox chCat3Id_xml;

		// Token: 0x0400055E RID: 1374
		private global::System.Windows.Forms.CheckBox chCat4Id_xml;

		// Token: 0x0400055F RID: 1375
		private global::System.Windows.Forms.Label label144;

		// Token: 0x04000560 RID: 1376
		private global::System.Windows.Forms.Label label145;

		// Token: 0x04000561 RID: 1377
		private global::System.Windows.Forms.CheckBox chCatId_pos;

		// Token: 0x04000562 RID: 1378
		private global::System.Windows.Forms.CheckBox chCat1Id_pos;

		// Token: 0x04000563 RID: 1379
		private global::System.Windows.Forms.CheckBox chCat2Id_pos;

		// Token: 0x04000564 RID: 1380
		private global::System.Windows.Forms.CheckBox chCat3Id_pos;

		// Token: 0x04000565 RID: 1381
		private global::System.Windows.Forms.CheckBox chCat4Id_pos;

		// Token: 0x04000566 RID: 1382
		private global::System.Windows.Forms.NumericUpDown nmVendor_pos;

		// Token: 0x04000567 RID: 1383
		private global::System.Windows.Forms.Label label146;

		// Token: 0x04000568 RID: 1384
		private global::System.Windows.Forms.TextBox tbDefaultVendor_pos;

		// Token: 0x04000569 RID: 1385
		private global::System.Windows.Forms.CheckBox chManufacturerId_pos;

		// Token: 0x0400056A RID: 1386
		private global::System.Windows.Forms.CheckBox chVendorId_pos;

		// Token: 0x0400056B RID: 1387
		private global::System.Windows.Forms.Label lblProductIdentifier;

		// Token: 0x0400056C RID: 1388
		private global::System.Windows.Forms.ComboBox ddProductIdInStore;

		// Token: 0x0400056D RID: 1389
		private global::System.Windows.Forms.Label label154;

		// Token: 0x0400056E RID: 1390
		private global::System.Windows.Forms.Label label155;

		// Token: 0x0400056F RID: 1391
		private global::System.Windows.Forms.TextBox textBox_0;

		// Token: 0x04000570 RID: 1392
		private global::System.Windows.Forms.TextBox tbWeightXPath;

		// Token: 0x04000571 RID: 1393
		private global::System.Windows.Forms.TextBox tbDefaultGtinXPath;

		// Token: 0x04000572 RID: 1394
		private global::System.Windows.Forms.TextBox tbDefaultWeight_xml;

		// Token: 0x04000573 RID: 1395
		private global::System.Windows.Forms.Label label156;

		// Token: 0x04000574 RID: 1396
		private global::System.Windows.Forms.Label label157;

		// Token: 0x04000575 RID: 1397
		private global::System.Windows.Forms.NumericUpDown nmGtin_pos;

		// Token: 0x04000576 RID: 1398
		private global::System.Windows.Forms.NumericUpDown nmWeight_pos;

		// Token: 0x04000577 RID: 1399
		private global::System.Windows.Forms.TextBox tbDefaultWeight_pos;

		// Token: 0x04000578 RID: 1400
		private global::System.Windows.Forms.TextBox tbDefaultGtin_pos;

		// Token: 0x04000579 RID: 1401
		private global::System.Windows.Forms.TextBox tbDefaultProdSKU_Xpath;

		// Token: 0x0400057A RID: 1402
		private global::System.Windows.Forms.TextBox nmDefaultProdSKU_pos;

		// Token: 0x0400057B RID: 1403
		private global::System.Windows.Forms.Label label10;

		// Token: 0x0400057C RID: 1404
		private global::System.Windows.Forms.Label label12;

		// Token: 0x0400057D RID: 1405
		private global::System.Windows.Forms.Label label158;

		// Token: 0x0400057E RID: 1406
		private global::System.Windows.Forms.TextBox tbLengthXpath;

		// Token: 0x0400057F RID: 1407
		private global::System.Windows.Forms.TextBox tbWidthXpath;

		// Token: 0x04000580 RID: 1408
		private global::System.Windows.Forms.TextBox tbHeightXpath;

		// Token: 0x04000581 RID: 1409
		private global::System.Windows.Forms.TextBox tbDefaultLength_xml;

		// Token: 0x04000582 RID: 1410
		private global::System.Windows.Forms.TextBox tbDefaultWidth_xml;

		// Token: 0x04000583 RID: 1411
		private global::System.Windows.Forms.TextBox tbDefaultHeight_xml;

		// Token: 0x04000584 RID: 1412
		private global::System.Windows.Forms.TabPage tabAccessRolesMappingXml;

		// Token: 0x04000585 RID: 1413
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;

		// Token: 0x04000586 RID: 1414
		private global::System.Windows.Forms.Label label159;

		// Token: 0x04000587 RID: 1415
		private global::System.Windows.Forms.Label label160;

		// Token: 0x04000588 RID: 1416
		private global::System.Windows.Forms.Label label161;

		// Token: 0x04000589 RID: 1417
		private global::System.Windows.Forms.TextBox tbCustomerRoleXpath;

		// Token: 0x0400058A RID: 1418
		private global::System.Windows.Forms.Label label162;

		// Token: 0x0400058B RID: 1419
		private global::System.Windows.Forms.TextBox tbDefaultCustomerRole_xml;

		// Token: 0x0400058C RID: 1420
		private global::System.Windows.Forms.Label label163;

		// Token: 0x0400058D RID: 1421
		private global::System.Windows.Forms.TextBox tbCustomerRoleDelimeter_xml;

		// Token: 0x0400058E RID: 1422
		private global::System.Windows.Forms.Label label164;

		// Token: 0x0400058F RID: 1423
		private global::System.Windows.Forms.Label label165;

		// Token: 0x04000590 RID: 1424
		private global::System.Windows.Forms.Label label166;

		// Token: 0x04000591 RID: 1425
		private global::System.Windows.Forms.Label label167;

		// Token: 0x04000592 RID: 1426
		private global::System.Windows.Forms.TextBox tbCustomerRoleXpath1;

		// Token: 0x04000593 RID: 1427
		private global::System.Windows.Forms.TextBox tbCustomerRoleXpath2;

		// Token: 0x04000594 RID: 1428
		private global::System.Windows.Forms.TextBox tbCustomerRoleXpath3;

		// Token: 0x04000595 RID: 1429
		private global::System.Windows.Forms.TextBox tbCustomerRoleXpath4;

		// Token: 0x04000596 RID: 1430
		private global::System.Windows.Forms.TextBox tbDefaultCustomerRole1_xml;

		// Token: 0x04000597 RID: 1431
		private global::System.Windows.Forms.TextBox tbDefaultCustomerRole2_xml;

		// Token: 0x04000598 RID: 1432
		private global::System.Windows.Forms.TextBox tbDefaultCustomerRole3_xml;

		// Token: 0x04000599 RID: 1433
		private global::System.Windows.Forms.TextBox tbDefaultCustomerRole4_xml;

		// Token: 0x0400059A RID: 1434
		private global::System.Windows.Forms.Label label168;

		// Token: 0x0400059B RID: 1435
		private global::System.Windows.Forms.CheckBox chCustomerRoleID_xml;

		// Token: 0x0400059C RID: 1436
		private global::System.Windows.Forms.CheckBox chCustomerRoleID1_xml;

		// Token: 0x0400059D RID: 1437
		private global::System.Windows.Forms.CheckBox chCustomerRoleID2_xml;

		// Token: 0x0400059E RID: 1438
		private global::System.Windows.Forms.CheckBox chCustomerRoleID3_xml;

		// Token: 0x0400059F RID: 1439
		private global::System.Windows.Forms.CheckBox chCustomerRoleID4_xml;

		// Token: 0x040005A0 RID: 1440
		private global::System.Windows.Forms.TabPage tabAccessRolesMappingPos;

		// Token: 0x040005A1 RID: 1441
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;

		// Token: 0x040005A2 RID: 1442
		private global::System.Windows.Forms.Label label169;

		// Token: 0x040005A3 RID: 1443
		private global::System.Windows.Forms.Label label170;

		// Token: 0x040005A4 RID: 1444
		private global::System.Windows.Forms.Label label171;

		// Token: 0x040005A5 RID: 1445
		private global::System.Windows.Forms.TextBox tbDefaultCustomerRole_pos;

		// Token: 0x040005A6 RID: 1446
		private global::System.Windows.Forms.Label label172;

		// Token: 0x040005A7 RID: 1447
		private global::System.Windows.Forms.TextBox tbCustomerRoleDelimiter_pos;

		// Token: 0x040005A8 RID: 1448
		private global::System.Windows.Forms.Label label173;

		// Token: 0x040005A9 RID: 1449
		private global::System.Windows.Forms.Label label174;

		// Token: 0x040005AA RID: 1450
		private global::System.Windows.Forms.Label label175;

		// Token: 0x040005AB RID: 1451
		private global::System.Windows.Forms.Label label176;

		// Token: 0x040005AC RID: 1452
		private global::System.Windows.Forms.TextBox tbDefaultCustomerRole1_pos;

		// Token: 0x040005AD RID: 1453
		private global::System.Windows.Forms.TextBox tbDefaultCustomerRole2_pos;

		// Token: 0x040005AE RID: 1454
		private global::System.Windows.Forms.TextBox tbDefaultCustomerRole3_pos;

		// Token: 0x040005AF RID: 1455
		private global::System.Windows.Forms.TextBox tbDefaultCustomerRole4_pos;

		// Token: 0x040005B0 RID: 1456
		private global::System.Windows.Forms.Label label177;

		// Token: 0x040005B1 RID: 1457
		private global::System.Windows.Forms.NumericUpDown nmCustomerRole_pos;

		// Token: 0x040005B2 RID: 1458
		private global::System.Windows.Forms.NumericUpDown nmCustomerRole1_pos;

		// Token: 0x040005B3 RID: 1459
		private global::System.Windows.Forms.NumericUpDown nmCustomerRole2_pos;

		// Token: 0x040005B4 RID: 1460
		private global::System.Windows.Forms.NumericUpDown nmCustomerRole3_pos;

		// Token: 0x040005B5 RID: 1461
		private global::System.Windows.Forms.NumericUpDown nmCustomerRole4_pos;

		// Token: 0x040005B6 RID: 1462
		private global::System.Windows.Forms.Label label178;

		// Token: 0x040005B7 RID: 1463
		private global::System.Windows.Forms.CheckBox chCustomerRoleId_pos;

		// Token: 0x040005B8 RID: 1464
		private global::System.Windows.Forms.CheckBox chCustomerRoleId1_pos;

		// Token: 0x040005B9 RID: 1465
		private global::System.Windows.Forms.CheckBox chCustomerRoleId2_pos;

		// Token: 0x040005BA RID: 1466
		private global::System.Windows.Forms.CheckBox chCustomerRoleId3_pos;

		// Token: 0x040005BB RID: 1467
		private global::System.Windows.Forms.CheckBox chCustomerRoleId4_pos;

		// Token: 0x040005BC RID: 1468
		private global::System.Windows.Forms.Label label179;

		// Token: 0x040005BD RID: 1469
		private global::System.Windows.Forms.Label label180;

		// Token: 0x040005BE RID: 1470
		private global::System.Windows.Forms.Label label181;

		// Token: 0x040005BF RID: 1471
		private global::System.Windows.Forms.NumericUpDown nmLength_pos;

		// Token: 0x040005C0 RID: 1472
		private global::System.Windows.Forms.NumericUpDown nmWidth_pos;

		// Token: 0x040005C1 RID: 1473
		private global::System.Windows.Forms.NumericUpDown nmHeight_pos;

		// Token: 0x040005C2 RID: 1474
		private global::System.Windows.Forms.TextBox tbDefaultLength_pos;

		// Token: 0x040005C3 RID: 1475
		private global::System.Windows.Forms.TextBox tbDefaultWidth_pos;

		// Token: 0x040005C4 RID: 1476
		private global::System.Windows.Forms.TextBox tbDefaultHeight_pos;

		// Token: 0x040005C5 RID: 1477
		private global::System.Windows.Forms.Button btnTestMapping;

		// Token: 0x040005C6 RID: 1478
		private global::System.Windows.Forms.Button btnClose;

		// Token: 0x040005C7 RID: 1479
		private global::System.Windows.Forms.Button btnSave;

		// Token: 0x040005C8 RID: 1480
		private global::System.Windows.Forms.Label label211;

		// Token: 0x040005C9 RID: 1481
		private global::System.Windows.Forms.ComboBox ddEntityType;

		// Token: 0x040005CA RID: 1482
		private global::System.Windows.Forms.TabPage tabProductSettingsXml;

		// Token: 0x040005CB RID: 1483
		private global::System.Windows.Forms.TabPage tabProductSettingsPos;

		// Token: 0x040005CC RID: 1484
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;

		// Token: 0x040005CD RID: 1485
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsMaxCartQty_xml;

		// Token: 0x040005CE RID: 1486
		private global::System.Windows.Forms.TextBox tbProdSettingsMaxCartQty_xml;

		// Token: 0x040005CF RID: 1487
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsMinCartQty_xml;

		// Token: 0x040005D0 RID: 1488
		private global::System.Windows.Forms.TextBox tbProdSettingsMinCartQty_xml;

		// Token: 0x040005D1 RID: 1489
		private global::System.Windows.Forms.Label label147;

		// Token: 0x040005D2 RID: 1490
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsNotifyQty_xml;

		// Token: 0x040005D3 RID: 1491
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsMinStockQty_xml;

		// Token: 0x040005D4 RID: 1492
		private global::System.Windows.Forms.Label label237;

		// Token: 0x040005D5 RID: 1493
		private global::System.Windows.Forms.Label label238;

		// Token: 0x040005D6 RID: 1494
		private global::System.Windows.Forms.Label label243;

		// Token: 0x040005D7 RID: 1495
		private global::System.Windows.Forms.Label label244;

		// Token: 0x040005D8 RID: 1496
		private global::System.Windows.Forms.TextBox tbProdSettingsMinStockQty_xml;

		// Token: 0x040005D9 RID: 1497
		private global::System.Windows.Forms.Label label245;

		// Token: 0x040005DA RID: 1498
		private global::System.Windows.Forms.TextBox tbProdSettingsNotifyQty_xml;

		// Token: 0x040005DB RID: 1499
		private global::System.Windows.Forms.Label label246;

		// Token: 0x040005DC RID: 1500
		private global::System.Windows.Forms.Label label247;

		// Token: 0x040005DD RID: 1501
		private global::System.Windows.Forms.TextBox tbProdSettingsAllowedQty_xml;

		// Token: 0x040005DE RID: 1502
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsAllowedQty_xml;

		// Token: 0x040005DF RID: 1503
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;

		// Token: 0x040005E0 RID: 1504
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsMaxCartQty_pos;

		// Token: 0x040005E1 RID: 1505
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsMinCartQty_pos;

		// Token: 0x040005E2 RID: 1506
		private global::System.Windows.Forms.Label label248;

		// Token: 0x040005E3 RID: 1507
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsNotifyQty_pos;

		// Token: 0x040005E4 RID: 1508
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsMinStockQty_pos;

		// Token: 0x040005E5 RID: 1509
		private global::System.Windows.Forms.Label label249;

		// Token: 0x040005E6 RID: 1510
		private global::System.Windows.Forms.Label label250;

		// Token: 0x040005E7 RID: 1511
		private global::System.Windows.Forms.Label label251;

		// Token: 0x040005E8 RID: 1512
		private global::System.Windows.Forms.Label label252;

		// Token: 0x040005E9 RID: 1513
		private global::System.Windows.Forms.Label label253;

		// Token: 0x040005EA RID: 1514
		private global::System.Windows.Forms.Label label254;

		// Token: 0x040005EB RID: 1515
		private global::System.Windows.Forms.NumericUpDown nmProdSettingsMinStockQty_pos;

		// Token: 0x040005EC RID: 1516
		private global::System.Windows.Forms.NumericUpDown nmProdSettingsNotifyQty_pos;

		// Token: 0x040005ED RID: 1517
		private global::System.Windows.Forms.NumericUpDown nmProdSettingsMinCartQty_pos;

		// Token: 0x040005EE RID: 1518
		private global::System.Windows.Forms.NumericUpDown nmProdSettingsMaxCartQty_pos;

		// Token: 0x040005EF RID: 1519
		private global::System.Windows.Forms.Label label255;

		// Token: 0x040005F0 RID: 1520
		private global::System.Windows.Forms.NumericUpDown nmProdSettingsAllowedQty_pos;

		// Token: 0x040005F1 RID: 1521
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsAllowedQty_pos;

		// Token: 0x040005F2 RID: 1522
		private global::System.Windows.Forms.Label label256;

		// Token: 0x040005F3 RID: 1523
		private global::System.Windows.Forms.Label label257;

		// Token: 0x040005F4 RID: 1524
		private global::System.Windows.Forms.Label label258;

		// Token: 0x040005F5 RID: 1525
		private global::System.Windows.Forms.Label label259;

		// Token: 0x040005F6 RID: 1526
		private global::System.Windows.Forms.Label label260;

		// Token: 0x040005F7 RID: 1527
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsDeliveryDate_xml;

		// Token: 0x040005F8 RID: 1528
		private global::System.Windows.Forms.TextBox tbProdSettingsDeliveryDate_xml;

		// Token: 0x040005F9 RID: 1529
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsShippingCharge_xml;

		// Token: 0x040005FA RID: 1530
		private global::System.Windows.Forms.TextBox tbProdSettingsShippingCharge_xml;

		// Token: 0x040005FB RID: 1531
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsShipSeparately_xml;

		// Token: 0x040005FC RID: 1532
		private global::System.Windows.Forms.TextBox tbProdSettingsShipSeparately_xml;

		// Token: 0x040005FD RID: 1533
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsFreeShipping_xml;

		// Token: 0x040005FE RID: 1534
		private global::System.Windows.Forms.TextBox tbProdSettingsFreeShipping_xml;

		// Token: 0x040005FF RID: 1535
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsShippingEnabled_xml;

		// Token: 0x04000600 RID: 1536
		private global::System.Windows.Forms.TextBox tbProdSettingsShippingEnabled_xml;

		// Token: 0x04000601 RID: 1537
		private global::System.Windows.Forms.Label label261;

		// Token: 0x04000602 RID: 1538
		private global::System.Windows.Forms.Label label262;

		// Token: 0x04000603 RID: 1539
		private global::System.Windows.Forms.Label label263;

		// Token: 0x04000604 RID: 1540
		private global::System.Windows.Forms.Label label264;

		// Token: 0x04000605 RID: 1541
		private global::System.Windows.Forms.Label label265;

		// Token: 0x04000606 RID: 1542
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsDeliveryDate_pos;

		// Token: 0x04000607 RID: 1543
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsShippingCharge_pos;

		// Token: 0x04000608 RID: 1544
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsShipSeparately_pos;

		// Token: 0x04000609 RID: 1545
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsFreeShipping_pos;

		// Token: 0x0400060A RID: 1546
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsShippingEnabled_pos;

		// Token: 0x0400060B RID: 1547
		private global::System.Windows.Forms.NumericUpDown nmProdSettingsShippingEnabled_pos;

		// Token: 0x0400060C RID: 1548
		private global::System.Windows.Forms.NumericUpDown nmProdSettingsFreeShipping_pos;

		// Token: 0x0400060D RID: 1549
		private global::System.Windows.Forms.NumericUpDown nmProdSettingsShipSeparately_pos;

		// Token: 0x0400060E RID: 1550
		private global::System.Windows.Forms.NumericUpDown nmProdSettingsShippingCharge_pos;

		// Token: 0x0400060F RID: 1551
		private global::System.Windows.Forms.NumericUpDown nmProdSettingsDeliveryDate_pos;

		// Token: 0x04000610 RID: 1552
		private global::System.Windows.Forms.Label label266;

		// Token: 0x04000611 RID: 1553
		private global::System.Windows.Forms.CheckBox chProdSettingsDeliveryDateIsID_xml;

		// Token: 0x04000612 RID: 1554
		private global::System.Windows.Forms.Label label267;

		// Token: 0x04000613 RID: 1555
		private global::System.Windows.Forms.CheckBox chProdSettingsDeliveryDateIsID_pos;

		// Token: 0x04000614 RID: 1556
		private global::System.Windows.Forms.Label label270;

		// Token: 0x04000615 RID: 1557
		private global::System.Windows.Forms.TextBox tbProdGroupByXpath;

		// Token: 0x04000616 RID: 1558
		private global::System.Windows.Forms.Label label271;

		// Token: 0x04000617 RID: 1559
		private global::System.Windows.Forms.NumericUpDown nmProdGroupBy_pos;

		// Token: 0x04000618 RID: 1560
		private global::System.Windows.Forms.Label label272;

		// Token: 0x04000619 RID: 1561
		private global::System.Windows.Forms.ComboBox ddSourceEncoding;

		// Token: 0x0400061A RID: 1562
		private global::System.Windows.Forms.TabPage tabAliExpress;

		// Token: 0x0400061B RID: 1563
		private global::System.Windows.Forms.Label lblAliExpressUrlLabel;

		// Token: 0x0400061C RID: 1564
		private global::System.Windows.Forms.TextBox tbAliexpressProductsUrls;

		// Token: 0x0400061D RID: 1565
		private global::System.Windows.Forms.Button btnQuickImport;

		// Token: 0x0400061E RID: 1566
		private global::System.Windows.Forms.ComboBox ddUserIdInStore;

		// Token: 0x0400061F RID: 1567
		private global::System.Windows.Forms.Label lblUserIdentifier;

		// Token: 0x04000620 RID: 1568
		private global::System.Windows.Forms.Label label285;

		// Token: 0x04000621 RID: 1569
		private global::System.Windows.Forms.TextBox tbAuthHeader;

		// Token: 0x0400062A RID: 1578
		private global::System.Windows.Forms.Label label50;

		// Token: 0x0400062B RID: 1579
		private global::System.Windows.Forms.NumericUpDown nmProdSettingsVisibleIndvidually_pos;

		// Token: 0x0400062C RID: 1580
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsVisibleIndvidually_pos;

		// Token: 0x0400062D RID: 1581
		private global::System.Windows.Forms.Label label51;

		// Token: 0x0400062E RID: 1582
		private global::System.Windows.Forms.TextBox tbProdSettingsVisibleIndvidually_xml;

		// Token: 0x0400062F RID: 1583
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsVisibleIndvidually_xml;

		// Token: 0x04000630 RID: 1584
		private global::System.Windows.Forms.NumericUpDown nmProdSettingsIsDeleted_pos;

		// Token: 0x04000631 RID: 1585
		private global::System.Windows.Forms.Label label60;

		// Token: 0x04000632 RID: 1586
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsIsDeleted_pos;

		// Token: 0x04000633 RID: 1587
		private global::System.Windows.Forms.Label label61;

		// Token: 0x04000634 RID: 1588
		private global::System.Windows.Forms.TextBox tbProdSettingsIsDeleted_xml;

		// Token: 0x04000635 RID: 1589
		private global::System.Windows.Forms.TextBox tbDefaultProdSettingsIsDeleted_xml;

		// Token: 0x04000636 RID: 1590
		private global::System.Windows.Forms.Button btnCopy;

		// Token: 0x04000637 RID: 1591
		private global::System.Windows.Forms.Panel panel1;

		// Token: 0x04000638 RID: 1592
		private global::System.Windows.Forms.Button btnSourceFileOpen;

		// Token: 0x04000639 RID: 1593
		private global::System.Windows.Forms.Button btnSourceFileBrowse;

		// Token: 0x0400063A RID: 1594
		private global::System.Windows.Forms.TextBox tbSourceAddress;

		// Token: 0x0400063B RID: 1595
		private global::System.Windows.Forms.ComboBox cbSourceParser;
        private System.Windows.Forms.CheckBox xmlModify;
    }
}
