﻿namespace NopTalk
{
	// Token: 0x02000021 RID: 33
	public partial class productCard : global::System.Windows.Forms.Form
	{
		// Token: 0x060000E8 RID: 232 RVA: 0x000029A1 File Offset: 0x00000BA1
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x060000E9 RID: 233 RVA: 0x00010900 File Offset: 0x0000EB00
		private void InitializeComponent()
		{
			global::System.ComponentModel.ComponentResourceManager componentResourceManager = new global::System.ComponentModel.ComponentResourceManager(typeof(global::NopTalk.productCard));
			this.tabControl1 = new global::System.Windows.Forms.TabControl();
			this.tabProductInfo = new global::System.Windows.Forms.TabPage();
			this.label8 = new global::System.Windows.Forms.Label();
			this.label7 = new global::System.Windows.Forms.Label();
			this.nmProdQuantity = new global::System.Windows.Forms.NumericUpDown();
			this.nmProdPrice = new global::System.Windows.Forms.NumericUpDown();
			this.clbCategoriesList = new global::System.Windows.Forms.CheckedListBox();
			this.label3 = new global::System.Windows.Forms.Label();
			this.tbProdShortDesc = new global::System.Windows.Forms.TextBox();
			this.label2 = new global::System.Windows.Forms.Label();
			this.tbProductTitle = new global::System.Windows.Forms.TextBox();
			this.label1 = new global::System.Windows.Forms.Label();
			this.picMain = new global::System.Windows.Forms.PictureBox();
			this.tabProductDescription = new global::System.Windows.Forms.TabPage();
			this.tabControl2 = new global::System.Windows.Forms.TabControl();
			this.tabRichEditor = new global::System.Windows.Forms.TabPage();
			this.tabTextEditor = new global::System.Windows.Forms.TabPage();
			this.tbFullDescTextEditor = new global::System.Windows.Forms.TextBox();
			this.tabProductsVariants = new global::System.Windows.Forms.TabPage();
			this.dgProdVariants = new global::System.Windows.Forms.DataGridView();
			this.tabProductImages = new global::System.Windows.Forms.TabPage();
			this.tableLayoutPanel1 = new global::System.Windows.Forms.TableLayoutPanel();
			this.imageListViewUnassigned = new global::System.Windows.Forms.ListView();
			this.label6 = new global::System.Windows.Forms.Label();
			this.imageListViewAssigned = new global::System.Windows.Forms.ListView();
			this.label5 = new global::System.Windows.Forms.Label();
			this.panel1 = new global::System.Windows.Forms.Panel();
			this.btnImageUnassign = new global::System.Windows.Forms.Button();
			this.btnImageAssign = new global::System.Windows.Forms.Button();
			this.tabProductSettings = new global::System.Windows.Forms.TabPage();
			this.tableLayoutPanel9 = new global::System.Windows.Forms.TableLayoutPanel();
			this.label71 = new global::System.Windows.Forms.Label();
			this.label101 = new global::System.Windows.Forms.Label();
			this.label102 = new global::System.Windows.Forms.Label();
			this.label104 = new global::System.Windows.Forms.Label();
			this.label118 = new global::System.Windows.Forms.Label();
			this.chProductPublishImport = new global::System.Windows.Forms.CheckBox();
			this.label117 = new global::System.Windows.Forms.Label();
			this.ddDeliveryDate = new global::System.Windows.Forms.ComboBox();
			this.chDayDeliveryImport = new global::System.Windows.Forms.CheckBox();
			this.label119 = new global::System.Windows.Forms.Label();
			this.chInventoryMethodImport = new global::System.Windows.Forms.CheckBox();
			this.chProductPublish = new global::System.Windows.Forms.CheckBox();
			this.chProductReviewEnable = new global::System.Windows.Forms.CheckBox();
			this.chProductReviewEnableImport = new global::System.Windows.Forms.CheckBox();
			this.label140 = new global::System.Windows.Forms.Label();
			this.label141 = new global::System.Windows.Forms.Label();
			this.label142 = new global::System.Windows.Forms.Label();
			this.label143 = new global::System.Windows.Forms.Label();
			this.label144 = new global::System.Windows.Forms.Label();
			this.chProductDisplayAvailability = new global::System.Windows.Forms.CheckBox();
			this.chProductDisplayAvailabilityImport = new global::System.Windows.Forms.CheckBox();
			this.tbProductMinStockQty = new global::System.Windows.Forms.TextBox();
			this.tbProductNotifyQty = new global::System.Windows.Forms.TextBox();
			this.tbProductMinCartQty = new global::System.Windows.Forms.TextBox();
			this.tbProductMaxCartQty = new global::System.Windows.Forms.TextBox();
			this.tbProductAllowQty = new global::System.Windows.Forms.TextBox();
			this.chProductMinStockQtyAction = new global::System.Windows.Forms.CheckBox();
			this.chProductNotifyQtyAction = new global::System.Windows.Forms.CheckBox();
			this.chProductMinCartQtyAction = new global::System.Windows.Forms.CheckBox();
			this.chProductMaxCartQtyAction = new global::System.Windows.Forms.CheckBox();
			this.chProductAllowQtyAction = new global::System.Windows.Forms.CheckBox();
			this.label145 = new global::System.Windows.Forms.Label();
			this.label146 = new global::System.Windows.Forms.Label();
			this.chProductDisplayStockQuantity = new global::System.Windows.Forms.CheckBox();
			this.chProductDisplayStockQuantityImport = new global::System.Windows.Forms.CheckBox();
			this.label148 = new global::System.Windows.Forms.Label();
			this.chDisableBuyButton = new global::System.Windows.Forms.CheckBox();
			this.chDisableBuyButtonImport = new global::System.Windows.Forms.CheckBox();
			this.label149 = new global::System.Windows.Forms.Label();
			this.ddlBackorderMode = new global::System.Windows.Forms.ComboBox();
			this.chBackorderModeImport = new global::System.Windows.Forms.CheckBox();
			this.label157 = new global::System.Windows.Forms.Label();
			this.label158 = new global::System.Windows.Forms.Label();
			this.label159 = new global::System.Windows.Forms.Label();
			this.label160 = new global::System.Windows.Forms.Label();
			this.chProductShippingEnabled = new global::System.Windows.Forms.CheckBox();
			this.chProductFreeShipping = new global::System.Windows.Forms.CheckBox();
			this.chProductShipSeparately = new global::System.Windows.Forms.CheckBox();
			this.chProductShippingEnabledImport = new global::System.Windows.Forms.CheckBox();
			this.chProductFreeShippingImport = new global::System.Windows.Forms.CheckBox();
			this.chProductShipSeparatelyImport = new global::System.Windows.Forms.CheckBox();
			this.chProductShippingChargeImport = new global::System.Windows.Forms.CheckBox();
			this.nmProductShippingCharge = new global::System.Windows.Forms.NumericUpDown();
			this.ddInventoryMethod = new global::System.Windows.Forms.ComboBox();
			this.lblShow = new global::System.Windows.Forms.Label();
			this.lblFoundItems = new global::System.Windows.Forms.Label();
			this.label4 = new global::System.Windows.Forms.Label();
			this.ddStoresConn = new global::System.Windows.Forms.ComboBox();
			this.btnImportToStore = new global::System.Windows.Forms.Button();
			this.btnPrev = new global::System.Windows.Forms.Button();
			this.btnNext = new global::System.Windows.Forms.Button();
			this.btnClose = new global::System.Windows.Forms.Button();
			this.statusArea = new global::System.Windows.Forms.StatusStrip();
			this.statusAreaText = new global::System.Windows.Forms.ToolStripStatusLabel();
			this.tbFullDescRichEditor = new global::YARTE.UI.HtmlEditor();
			this.tabControl1.SuspendLayout();
			this.tabProductInfo.SuspendLayout();
			((global::System.ComponentModel.ISupportInitialize)this.nmProdQuantity).BeginInit();
			((global::System.ComponentModel.ISupportInitialize)this.nmProdPrice).BeginInit();
			((global::System.ComponentModel.ISupportInitialize)this.picMain).BeginInit();
			this.tabProductDescription.SuspendLayout();
			this.tabControl2.SuspendLayout();
			this.tabRichEditor.SuspendLayout();
			this.tabTextEditor.SuspendLayout();
			this.tabProductsVariants.SuspendLayout();
			((global::System.ComponentModel.ISupportInitialize)this.dgProdVariants).BeginInit();
			this.tabProductImages.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.panel1.SuspendLayout();
			this.tabProductSettings.SuspendLayout();
			this.tableLayoutPanel9.SuspendLayout();
			((global::System.ComponentModel.ISupportInitialize)this.nmProductShippingCharge).BeginInit();
			this.statusArea.SuspendLayout();
			base.SuspendLayout();
			this.tabControl1.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tabControl1.Controls.Add(this.tabProductInfo);
			this.tabControl1.Controls.Add(this.tabProductDescription);
			this.tabControl1.Controls.Add(this.tabProductsVariants);
			this.tabControl1.Controls.Add(this.tabProductImages);
			this.tabControl1.Controls.Add(this.tabProductSettings);
			this.tabControl1.Location = new global::System.Drawing.Point(20, 43);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new global::System.Drawing.Size(1177, 626);
			this.tabControl1.TabIndex = 0;
			this.tabProductInfo.Controls.Add(this.label8);
			this.tabProductInfo.Controls.Add(this.label7);
			this.tabProductInfo.Controls.Add(this.nmProdQuantity);
			this.tabProductInfo.Controls.Add(this.nmProdPrice);
			this.tabProductInfo.Controls.Add(this.clbCategoriesList);
			this.tabProductInfo.Controls.Add(this.label3);
			this.tabProductInfo.Controls.Add(this.tbProdShortDesc);
			this.tabProductInfo.Controls.Add(this.label2);
			this.tabProductInfo.Controls.Add(this.tbProductTitle);
			this.tabProductInfo.Controls.Add(this.label1);
			this.tabProductInfo.Controls.Add(this.picMain);
			this.tabProductInfo.Location = new global::System.Drawing.Point(4, 25);
			this.tabProductInfo.Name = "tabProductInfo";
			this.tabProductInfo.Padding = new global::System.Windows.Forms.Padding(3);
			this.tabProductInfo.Size = new global::System.Drawing.Size(1169, 597);
			this.tabProductInfo.TabIndex = 0;
			this.tabProductInfo.Text = "Product";
			this.tabProductInfo.UseVisualStyleBackColor = true;
			this.label8.AutoSize = true;
			this.label8.Location = new global::System.Drawing.Point(466, 215);
			this.label8.Name = "label8";
			this.label8.Size = new global::System.Drawing.Size(114, 17);
			this.label8.TabIndex = 55;
			this.label8.Text = "Product Quantity";
			this.label7.AutoSize = true;
			this.label7.Location = new global::System.Drawing.Point(300, 215);
			this.label7.Name = "label7";
			this.label7.Size = new global::System.Drawing.Size(93, 17);
			this.label7.TabIndex = 54;
			this.label7.Text = "Product Price";
			this.nmProdQuantity.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.nmProdQuantity.Location = new global::System.Drawing.Point(469, 241);
			this.nmProdQuantity.Margin = new global::System.Windows.Forms.Padding(4);
			global::System.Windows.Forms.NumericUpDown numericUpDown = this.nmProdQuantity;
			int[] array = new int[4];
			array[0] = 999999;
			//numericUpDown.Maximum = new decimal(array);
			this.nmProdQuantity.Minimum = new decimal(new int[]
			{
				999999,
				0,
				0,
				int.MinValue
			});
			this.nmProdQuantity.Name = "nmProdQuantity";
			this.nmProdQuantity.Size = new global::System.Drawing.Size(111, 22);
			this.nmProdQuantity.TabIndex = 53;
			this.nmProdQuantity.Tag = "";
			this.nmProdPrice.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.nmProdPrice.DecimalPlaces = 2;
			this.nmProdPrice.Location = new global::System.Drawing.Point(303, 241);
			this.nmProdPrice.Margin = new global::System.Windows.Forms.Padding(4);
			global::System.Windows.Forms.NumericUpDown numericUpDown2 = this.nmProdPrice;
			int[] array2 = new int[4];
			array2[0] = 99999;
		//	numericUpDown2.Maximum = new decimal(array2);
			this.nmProdPrice.Minimum = new decimal(new int[]
			{
				99999,
				0,
				0,
				int.MinValue
			});
			this.nmProdPrice.Name = "nmProdPrice";
			this.nmProdPrice.Size = new global::System.Drawing.Size(90, 22);
			this.nmProdPrice.TabIndex = 52;
			this.clbCategoriesList.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.clbCategoriesList.CheckOnClick = true;
			this.clbCategoriesList.FormattingEnabled = true;
			this.clbCategoriesList.Location = new global::System.Drawing.Point(303, 304);
			this.clbCategoriesList.MultiColumn = true;
			this.clbCategoriesList.Name = "clbCategoriesList";
			this.clbCategoriesList.Size = new global::System.Drawing.Size(859, 174);
			this.clbCategoriesList.TabIndex = 28;
			this.label3.AutoSize = true;
			this.label3.Location = new global::System.Drawing.Point(300, 99);
			this.label3.Name = "label3";
			this.label3.Size = new global::System.Drawing.Size(170, 17);
			this.label3.TabIndex = 6;
			this.label3.Text = "Product Short Description";
			this.tbProdShortDesc.Location = new global::System.Drawing.Point(303, 119);
			this.tbProdShortDesc.Multiline = true;
			this.tbProdShortDesc.Name = "tbProdShortDesc";
			this.tbProdShortDesc.Size = new global::System.Drawing.Size(859, 74);
			this.tbProdShortDesc.TabIndex = 5;
			this.label2.AutoSize = true;
			this.label2.Location = new global::System.Drawing.Point(300, 280);
			this.label2.Name = "label2";
			this.label2.Size = new global::System.Drawing.Size(76, 17);
			this.label2.TabIndex = 3;
			this.label2.Text = "Categories";
			this.tbProductTitle.Location = new global::System.Drawing.Point(303, 37);
			this.tbProductTitle.Multiline = true;
			this.tbProductTitle.Name = "tbProductTitle";
			this.tbProductTitle.Size = new global::System.Drawing.Size(859, 42);
			this.tbProductTitle.TabIndex = 2;
			this.label1.AutoSize = true;
			this.label1.Location = new global::System.Drawing.Point(300, 17);
			this.label1.Name = "label1";
			this.label1.Size = new global::System.Drawing.Size(88, 17);
			this.label1.TabIndex = 1;
			this.label1.Text = "Product Title";
			this.picMain.Location = new global::System.Drawing.Point(7, 17);
			this.picMain.Name = "picMain";
			this.picMain.Size = new global::System.Drawing.Size(275, 393);
			this.picMain.TabIndex = 0;
			this.picMain.TabStop = false;
			this.tabProductDescription.Controls.Add(this.tabControl2);
			this.tabProductDescription.Location = new global::System.Drawing.Point(4, 25);
			this.tabProductDescription.Name = "tabProductDescription";
			this.tabProductDescription.Padding = new global::System.Windows.Forms.Padding(3);
			this.tabProductDescription.Size = new global::System.Drawing.Size(1169, 597);
			this.tabProductDescription.TabIndex = 1;
			this.tabProductDescription.Text = "Description";
			this.tabProductDescription.UseVisualStyleBackColor = true;
			this.tabControl2.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tabControl2.Controls.Add(this.tabRichEditor);
			this.tabControl2.Controls.Add(this.tabTextEditor);
			this.tabControl2.Location = new global::System.Drawing.Point(0, 0);
			this.tabControl2.Name = "tabControl2";
			this.tabControl2.SelectedIndex = 0;
			this.tabControl2.Size = new global::System.Drawing.Size(1163, 594);
			this.tabControl2.TabIndex = 0;
			this.tabControl2.SelectedIndexChanged += new global::System.EventHandler(this.tabControl2_SelectedIndexChanged);
			this.tabControl2.Selecting += new global::System.Windows.Forms.TabControlCancelEventHandler(this.tabControl2_Selecting);
			this.tabRichEditor.Controls.Add(this.tbFullDescRichEditor);
			this.tabRichEditor.Location = new global::System.Drawing.Point(4, 25);
			this.tabRichEditor.Name = "tabRichEditor";
			this.tabRichEditor.Padding = new global::System.Windows.Forms.Padding(3);
			this.tabRichEditor.Size = new global::System.Drawing.Size(1155, 565);
			this.tabRichEditor.TabIndex = 0;
			this.tabRichEditor.Text = "Rich Editor";
			this.tabRichEditor.UseVisualStyleBackColor = true;
			this.tabTextEditor.Controls.Add(this.tbFullDescTextEditor);
			this.tabTextEditor.Location = new global::System.Drawing.Point(4, 25);
			this.tabTextEditor.Name = "tabTextEditor";
			this.tabTextEditor.Padding = new global::System.Windows.Forms.Padding(3);
			this.tabTextEditor.Size = new global::System.Drawing.Size(1155, 565);
			this.tabTextEditor.TabIndex = 1;
			this.tabTextEditor.Text = "Text Editor";
			this.tabTextEditor.UseVisualStyleBackColor = true;
			this.tbFullDescTextEditor.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tbFullDescTextEditor.Location = new global::System.Drawing.Point(6, 6);
			this.tbFullDescTextEditor.Multiline = true;
			this.tbFullDescTextEditor.Name = "tbFullDescTextEditor";
			this.tbFullDescTextEditor.Size = new global::System.Drawing.Size(1143, 553);
			this.tbFullDescTextEditor.TabIndex = 6;
			this.tabProductsVariants.Controls.Add(this.dgProdVariants);
			this.tabProductsVariants.Location = new global::System.Drawing.Point(4, 25);
			this.tabProductsVariants.Name = "tabProductsVariants";
			this.tabProductsVariants.Size = new global::System.Drawing.Size(1169, 597);
			this.tabProductsVariants.TabIndex = 2;
			this.tabProductsVariants.Text = "Variants";
			this.tabProductsVariants.UseVisualStyleBackColor = true;
			this.dgProdVariants.AllowUserToAddRows = false;
			this.dgProdVariants.AllowUserToDeleteRows = false;
			this.dgProdVariants.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.dgProdVariants.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgProdVariants.Location = new global::System.Drawing.Point(4, 4);
			this.dgProdVariants.Margin = new global::System.Windows.Forms.Padding(4);
			this.dgProdVariants.MultiSelect = false;
			this.dgProdVariants.Name = "dgProdVariants";
			this.dgProdVariants.SelectionMode = global::System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgProdVariants.Size = new global::System.Drawing.Size(1160, 495);
			this.dgProdVariants.TabIndex = 9;
			this.dgProdVariants.DataBindingComplete += new global::System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgProdVariants_DataBindingComplete);
			this.tabProductImages.Controls.Add(this.tableLayoutPanel1);
			this.tabProductImages.Location = new global::System.Drawing.Point(4, 25);
			this.tabProductImages.Name = "tabProductImages";
			this.tabProductImages.Size = new global::System.Drawing.Size(1169, 597);
			this.tabProductImages.TabIndex = 3;
			this.tabProductImages.Text = "Images";
			this.tabProductImages.UseVisualStyleBackColor = true;
			this.tableLayoutPanel1.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tableLayoutPanel1.ColumnCount = 3;
			this.tableLayoutPanel1.ColumnStyles.Add(new global::System.Windows.Forms.ColumnStyle(global::System.Windows.Forms.SizeType.Percent, 40f));
			this.tableLayoutPanel1.ColumnStyles.Add(new global::System.Windows.Forms.ColumnStyle(global::System.Windows.Forms.SizeType.Percent, 20f));
			this.tableLayoutPanel1.ColumnStyles.Add(new global::System.Windows.Forms.ColumnStyle(global::System.Windows.Forms.SizeType.Percent, 40f));
			this.tableLayoutPanel1.Controls.Add(this.imageListViewUnassigned, 2, 1);
			this.tableLayoutPanel1.Controls.Add(this.label6, 2, 0);
			this.tableLayoutPanel1.Controls.Add(this.imageListViewAssigned, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.label5, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
			this.tableLayoutPanel1.Location = new global::System.Drawing.Point(3, 3);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Percent, 10f));
			this.tableLayoutPanel1.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Percent, 90f));
			this.tableLayoutPanel1.Size = new global::System.Drawing.Size(1162, 497);
			this.tableLayoutPanel1.TabIndex = 4;
			this.imageListViewUnassigned.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.imageListViewUnassigned.Location = new global::System.Drawing.Point(699, 52);
			this.imageListViewUnassigned.Name = "imageListViewUnassigned";
			this.imageListViewUnassigned.Size = new global::System.Drawing.Size(460, 442);
			this.imageListViewUnassigned.TabIndex = 1;
			this.imageListViewUnassigned.UseCompatibleStateImageBehavior = false;
			this.label6.AutoSize = true;
			this.label6.Location = new global::System.Drawing.Point(699, 0);
			this.label6.Name = "label6";
			this.label6.Size = new global::System.Drawing.Size(128, 17);
			this.label6.TabIndex = 3;
			this.label6.Text = "Unselected Images";
			this.imageListViewAssigned.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.imageListViewAssigned.Location = new global::System.Drawing.Point(3, 52);
			this.imageListViewAssigned.Name = "imageListViewAssigned";
			this.imageListViewAssigned.Size = new global::System.Drawing.Size(458, 442);
			this.imageListViewAssigned.TabIndex = 0;
			this.imageListViewAssigned.UseCompatibleStateImageBehavior = false;
			this.label5.AutoSize = true;
			this.label5.Location = new global::System.Drawing.Point(3, 0);
			this.label5.Name = "label5";
			this.label5.Size = new global::System.Drawing.Size(112, 17);
			this.label5.TabIndex = 2;
			this.label5.Text = "Selected Images";
			this.panel1.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.panel1.Controls.Add(this.btnImageUnassign);
			this.panel1.Controls.Add(this.btnImageAssign);
			this.panel1.Location = new global::System.Drawing.Point(467, 52);
			this.panel1.Name = "panel1";
			this.panel1.Size = new global::System.Drawing.Size(226, 442);
			this.panel1.TabIndex = 4;
			this.btnImageUnassign.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnImageUnassign.Location = new global::System.Drawing.Point(66, 259);
			this.btnImageUnassign.Name = "btnImageUnassign";
			this.btnImageUnassign.Size = new global::System.Drawing.Size(93, 31);
			this.btnImageUnassign.TabIndex = 5;
			this.btnImageUnassign.Text = "Unassign";
			this.btnImageUnassign.UseVisualStyleBackColor = true;
			this.btnImageUnassign.Click += new global::System.EventHandler(this.btnImageUnassign_Click);
			this.btnImageAssign.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnImageAssign.Location = new global::System.Drawing.Point(66, 84);
			this.btnImageAssign.Name = "btnImageAssign";
			this.btnImageAssign.Size = new global::System.Drawing.Size(93, 31);
			this.btnImageAssign.TabIndex = 4;
			this.btnImageAssign.Text = "Assign";
			this.btnImageAssign.UseVisualStyleBackColor = true;
			this.btnImageAssign.Click += new global::System.EventHandler(this.btnImageAssign_Click);
			this.tabProductSettings.Controls.Add(this.tableLayoutPanel9);
			this.tabProductSettings.Location = new global::System.Drawing.Point(4, 25);
			this.tabProductSettings.Name = "tabProductSettings";
			this.tabProductSettings.Size = new global::System.Drawing.Size(1169, 597);
			this.tabProductSettings.TabIndex = 4;
			this.tabProductSettings.Text = "Settings";
			this.tabProductSettings.UseVisualStyleBackColor = true;
			this.tableLayoutPanel9.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tableLayoutPanel9.AutoScroll = true;
			this.tableLayoutPanel9.CellBorderStyle = global::System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel9.ColumnCount = 3;
			this.tableLayoutPanel9.ColumnStyles.Add(new global::System.Windows.Forms.ColumnStyle(global::System.Windows.Forms.SizeType.Absolute, 200f));
			this.tableLayoutPanel9.ColumnStyles.Add(new global::System.Windows.Forms.ColumnStyle(global::System.Windows.Forms.SizeType.Absolute, 400f));
			this.tableLayoutPanel9.ColumnStyles.Add(new global::System.Windows.Forms.ColumnStyle(global::System.Windows.Forms.SizeType.Percent, 100f));
			this.tableLayoutPanel9.ColumnStyles.Add(new global::System.Windows.Forms.ColumnStyle(global::System.Windows.Forms.SizeType.Absolute, 20f));
			this.tableLayoutPanel9.Controls.Add(this.label71, 0, 4);
			this.tableLayoutPanel9.Controls.Add(this.label101, 2, 0);
			this.tableLayoutPanel9.Controls.Add(this.label102, 0, 0);
			this.tableLayoutPanel9.Controls.Add(this.label104, 1, 0);
			this.tableLayoutPanel9.Controls.Add(this.label118, 0, 1);
			this.tableLayoutPanel9.Controls.Add(this.chProductPublishImport, 2, 1);
			this.tableLayoutPanel9.Controls.Add(this.label117, 0, 2);
			this.tableLayoutPanel9.Controls.Add(this.ddDeliveryDate, 1, 2);
			this.tableLayoutPanel9.Controls.Add(this.chDayDeliveryImport, 2, 2);
			this.tableLayoutPanel9.Controls.Add(this.label119, 0, 3);
			this.tableLayoutPanel9.Controls.Add(this.chInventoryMethodImport, 2, 3);
			this.tableLayoutPanel9.Controls.Add(this.chProductPublish, 1, 1);
			this.tableLayoutPanel9.Controls.Add(this.chProductReviewEnable, 1, 4);
			this.tableLayoutPanel9.Controls.Add(this.chProductReviewEnableImport, 2, 4);
			this.tableLayoutPanel9.Controls.Add(this.label140, 0, 5);
			this.tableLayoutPanel9.Controls.Add(this.label141, 0, 6);
			this.tableLayoutPanel9.Controls.Add(this.label142, 0, 7);
			this.tableLayoutPanel9.Controls.Add(this.label143, 0, 8);
			this.tableLayoutPanel9.Controls.Add(this.label144, 0, 9);
			this.tableLayoutPanel9.Controls.Add(this.chProductDisplayAvailability, 1, 5);
			this.tableLayoutPanel9.Controls.Add(this.chProductDisplayAvailabilityImport, 2, 5);
			this.tableLayoutPanel9.Controls.Add(this.tbProductMinStockQty, 1, 6);
			this.tableLayoutPanel9.Controls.Add(this.tbProductNotifyQty, 1, 7);
			this.tableLayoutPanel9.Controls.Add(this.tbProductMinCartQty, 1, 8);
			this.tableLayoutPanel9.Controls.Add(this.tbProductMaxCartQty, 1, 9);
			this.tableLayoutPanel9.Controls.Add(this.tbProductAllowQty, 1, 10);
			this.tableLayoutPanel9.Controls.Add(this.chProductMinStockQtyAction, 2, 6);
			this.tableLayoutPanel9.Controls.Add(this.chProductNotifyQtyAction, 2, 7);
			this.tableLayoutPanel9.Controls.Add(this.chProductMinCartQtyAction, 2, 8);
			this.tableLayoutPanel9.Controls.Add(this.chProductMaxCartQtyAction, 2, 9);
			this.tableLayoutPanel9.Controls.Add(this.chProductAllowQtyAction, 2, 10);
			this.tableLayoutPanel9.Controls.Add(this.label145, 0, 10);
			this.tableLayoutPanel9.Controls.Add(this.label146, 0, 11);
			this.tableLayoutPanel9.Controls.Add(this.chProductDisplayStockQuantity, 1, 11);
			this.tableLayoutPanel9.Controls.Add(this.chProductDisplayStockQuantityImport, 2, 11);
			this.tableLayoutPanel9.Controls.Add(this.label148, 0, 12);
			this.tableLayoutPanel9.Controls.Add(this.chDisableBuyButton, 1, 12);
			this.tableLayoutPanel9.Controls.Add(this.chDisableBuyButtonImport, 2, 12);
			this.tableLayoutPanel9.Controls.Add(this.label149, 0, 13);
			this.tableLayoutPanel9.Controls.Add(this.ddlBackorderMode, 1, 13);
			this.tableLayoutPanel9.Controls.Add(this.chBackorderModeImport, 2, 13);
			this.tableLayoutPanel9.Controls.Add(this.label157, 0, 14);
			this.tableLayoutPanel9.Controls.Add(this.label158, 0, 15);
			this.tableLayoutPanel9.Controls.Add(this.label159, 0, 16);
			this.tableLayoutPanel9.Controls.Add(this.label160, 0, 17);
			this.tableLayoutPanel9.Controls.Add(this.chProductShippingEnabled, 1, 14);
			this.tableLayoutPanel9.Controls.Add(this.chProductFreeShipping, 1, 15);
			this.tableLayoutPanel9.Controls.Add(this.chProductShipSeparately, 1, 16);
			this.tableLayoutPanel9.Controls.Add(this.chProductShippingEnabledImport, 2, 14);
			this.tableLayoutPanel9.Controls.Add(this.chProductFreeShippingImport, 2, 15);
			this.tableLayoutPanel9.Controls.Add(this.chProductShipSeparatelyImport, 2, 16);
			this.tableLayoutPanel9.Controls.Add(this.chProductShippingChargeImport, 2, 17);
			this.tableLayoutPanel9.Controls.Add(this.nmProductShippingCharge, 1, 17);
			this.tableLayoutPanel9.Controls.Add(this.ddInventoryMethod, 1, 3);
			this.tableLayoutPanel9.Location = new global::System.Drawing.Point(3, 1);
			this.tableLayoutPanel9.Margin = new global::System.Windows.Forms.Padding(4);
			this.tableLayoutPanel9.Name = "tableLayoutPanel9";
			this.tableLayoutPanel9.RowCount = 19;
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 31f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 30f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 35f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 35f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 30f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 30f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 35f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 35f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 35f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 35f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 30f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 30f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 30f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 35f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 30f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 30f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 30f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 30f));
			this.tableLayoutPanel9.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Percent, 100f));
			this.tableLayoutPanel9.Size = new global::System.Drawing.Size(1166, 715);
			this.tableLayoutPanel9.TabIndex = 3;
			this.label71.AutoSize = true;
			this.label71.Location = new global::System.Drawing.Point(5, 136);
			this.label71.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label71.Name = "label71";
			this.label71.Size = new global::System.Drawing.Size(160, 17);
			this.label71.TabIndex = 66;
			this.label71.Text = "Allow Customer Reviews";
			this.label101.AutoSize = true;
			this.label101.Font = new global::System.Drawing.Font("Microsoft Sans Serif", 10f, global::System.Drawing.FontStyle.Bold, global::System.Drawing.GraphicsUnit.Point, 0);
			this.label101.Location = new global::System.Drawing.Point(607, 1);
			this.label101.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label101.Name = "label101";
			this.label101.Size = new global::System.Drawing.Size(146, 20);
			this.label101.TabIndex = 43;
			this.label101.Text = "Fields To Import";
			this.label102.AutoSize = true;
			this.label102.Font = new global::System.Drawing.Font("Microsoft Sans Serif", 10f, global::System.Drawing.FontStyle.Bold, global::System.Drawing.GraphicsUnit.Point, 0);
			this.label102.Location = new global::System.Drawing.Point(5, 1);
			this.label102.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label102.Name = "label102";
			this.label102.Size = new global::System.Drawing.Size(104, 20);
			this.label102.TabIndex = 11;
			this.label102.Text = "Field Name";
			this.label104.AutoSize = true;
			this.label104.Font = new global::System.Drawing.Font("Microsoft Sans Serif", 10f, global::System.Drawing.FontStyle.Bold, global::System.Drawing.GraphicsUnit.Point, 0);
			this.label104.Location = new global::System.Drawing.Point(206, 1);
			this.label104.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label104.Name = "label104";
			this.label104.Size = new global::System.Drawing.Size(103, 20);
			this.label104.TabIndex = 21;
			this.label104.Text = "Field Value";
			this.label118.AutoSize = true;
			this.label118.Location = new global::System.Drawing.Point(5, 33);
			this.label118.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label118.Name = "label118";
			this.label118.Size = new global::System.Drawing.Size(54, 17);
			this.label118.TabIndex = 57;
			this.label118.Text = "Publish";
			this.chProductPublishImport.AutoSize = true;
			this.chProductPublishImport.Location = new global::System.Drawing.Point(607, 37);
			this.chProductPublishImport.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductPublishImport.Name = "chProductPublishImport";
			this.chProductPublishImport.Size = new global::System.Drawing.Size(18, 17);
			this.chProductPublishImport.TabIndex = 65;
			this.chProductPublishImport.UseVisualStyleBackColor = true;
			this.label117.AutoSize = true;
			this.label117.Location = new global::System.Drawing.Point(5, 64);
			this.label117.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label117.Name = "label117";
			this.label117.Size = new global::System.Drawing.Size(91, 17);
			this.label117.TabIndex = 59;
			this.label117.Text = "Delivery date";
			this.ddDeliveryDate.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.ddDeliveryDate.DropDownStyle = global::System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ddDeliveryDate.FlatStyle = global::System.Windows.Forms.FlatStyle.Popup;
			this.ddDeliveryDate.FormattingEnabled = true;
			this.ddDeliveryDate.Location = new global::System.Drawing.Point(206, 68);
			this.ddDeliveryDate.Margin = new global::System.Windows.Forms.Padding(4, 4, 27, 4);
			this.ddDeliveryDate.Name = "ddDeliveryDate";
			this.ddDeliveryDate.Size = new global::System.Drawing.Size(369, 24);
			this.ddDeliveryDate.TabIndex = 60;
			this.chDayDeliveryImport.AutoSize = true;
			this.chDayDeliveryImport.Location = new global::System.Drawing.Point(607, 68);
			this.chDayDeliveryImport.Margin = new global::System.Windows.Forms.Padding(4);
			this.chDayDeliveryImport.Name = "chDayDeliveryImport";
			this.chDayDeliveryImport.Size = new global::System.Drawing.Size(18, 17);
			this.chDayDeliveryImport.TabIndex = 63;
			this.chDayDeliveryImport.UseVisualStyleBackColor = true;
			this.label119.AutoSize = true;
			this.label119.Location = new global::System.Drawing.Point(5, 100);
			this.label119.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label119.Name = "label119";
			this.label119.Size = new global::System.Drawing.Size(172, 17);
			this.label119.TabIndex = 61;
			this.label119.Text = "Manage inventory method";
			this.chInventoryMethodImport.AutoSize = true;
			this.chInventoryMethodImport.Location = new global::System.Drawing.Point(607, 104);
			this.chInventoryMethodImport.Margin = new global::System.Windows.Forms.Padding(4);
			this.chInventoryMethodImport.Name = "chInventoryMethodImport";
			this.chInventoryMethodImport.Size = new global::System.Drawing.Size(18, 17);
			this.chInventoryMethodImport.TabIndex = 64;
			this.chInventoryMethodImport.UseVisualStyleBackColor = true;
			this.chProductPublish.AutoSize = true;
			this.chProductPublish.Location = new global::System.Drawing.Point(206, 37);
			this.chProductPublish.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductPublish.Name = "chProductPublish";
			this.chProductPublish.Size = new global::System.Drawing.Size(18, 17);
			this.chProductPublish.TabIndex = 58;
			this.chProductPublish.UseVisualStyleBackColor = true;
			this.chProductReviewEnable.AutoSize = true;
			this.chProductReviewEnable.Location = new global::System.Drawing.Point(206, 140);
			this.chProductReviewEnable.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductReviewEnable.Name = "chProductReviewEnable";
			this.chProductReviewEnable.Size = new global::System.Drawing.Size(18, 17);
			this.chProductReviewEnable.TabIndex = 67;
			this.chProductReviewEnable.UseVisualStyleBackColor = true;
			this.chProductReviewEnableImport.AutoSize = true;
			this.chProductReviewEnableImport.Location = new global::System.Drawing.Point(607, 140);
			this.chProductReviewEnableImport.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductReviewEnableImport.Name = "chProductReviewEnableImport";
			this.chProductReviewEnableImport.Size = new global::System.Drawing.Size(18, 17);
			this.chProductReviewEnableImport.TabIndex = 68;
			this.chProductReviewEnableImport.UseVisualStyleBackColor = true;
			this.label140.AutoSize = true;
			this.label140.Location = new global::System.Drawing.Point(5, 167);
			this.label140.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label140.Name = "label140";
			this.label140.Size = new global::System.Drawing.Size(123, 17);
			this.label140.TabIndex = 69;
			this.label140.Text = "Display availability";
			this.label141.AutoSize = true;
			this.label141.Location = new global::System.Drawing.Point(5, 198);
			this.label141.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label141.Name = "label141";
			this.label141.Size = new global::System.Drawing.Size(123, 17);
			this.label141.TabIndex = 70;
			this.label141.Text = "Minimum stock qty";
			this.label142.AutoSize = true;
			this.label142.Location = new global::System.Drawing.Point(5, 234);
			this.label142.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label142.Name = "label142";
			this.label142.Size = new global::System.Drawing.Size(128, 17);
			this.label142.TabIndex = 71;
			this.label142.Text = "Notify for qty below";
			this.label143.AutoSize = true;
			this.label143.Location = new global::System.Drawing.Point(5, 270);
			this.label143.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label143.Name = "label143";
			this.label143.Size = new global::System.Drawing.Size(114, 17);
			this.label143.TabIndex = 72;
			this.label143.Text = "Minimum cart qty";
			this.label144.AutoSize = true;
			this.label144.Location = new global::System.Drawing.Point(5, 306);
			this.label144.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label144.Name = "label144";
			this.label144.Size = new global::System.Drawing.Size(117, 17);
			this.label144.TabIndex = 73;
			this.label144.Text = "Maximum cart qty";
			this.chProductDisplayAvailability.AutoSize = true;
			this.chProductDisplayAvailability.Location = new global::System.Drawing.Point(206, 171);
			this.chProductDisplayAvailability.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductDisplayAvailability.Name = "chProductDisplayAvailability";
			this.chProductDisplayAvailability.Size = new global::System.Drawing.Size(18, 17);
			this.chProductDisplayAvailability.TabIndex = 75;
			this.chProductDisplayAvailability.UseVisualStyleBackColor = true;
			this.chProductDisplayAvailabilityImport.AutoSize = true;
			this.chProductDisplayAvailabilityImport.Location = new global::System.Drawing.Point(607, 171);
			this.chProductDisplayAvailabilityImport.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductDisplayAvailabilityImport.Name = "chProductDisplayAvailabilityImport";
			this.chProductDisplayAvailabilityImport.Size = new global::System.Drawing.Size(18, 17);
			this.chProductDisplayAvailabilityImport.TabIndex = 76;
			this.chProductDisplayAvailabilityImport.UseVisualStyleBackColor = true;
			this.tbProductMinStockQty.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tbProductMinStockQty.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbProductMinStockQty.Location = new global::System.Drawing.Point(206, 202);
			this.tbProductMinStockQty.Margin = new global::System.Windows.Forms.Padding(4, 4, 27, 4);
			this.tbProductMinStockQty.Name = "tbProductMinStockQty";
			this.tbProductMinStockQty.Size = new global::System.Drawing.Size(369, 22);
			this.tbProductMinStockQty.TabIndex = 77;
			this.tbProductNotifyQty.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tbProductNotifyQty.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbProductNotifyQty.Location = new global::System.Drawing.Point(206, 238);
			this.tbProductNotifyQty.Margin = new global::System.Windows.Forms.Padding(4, 4, 27, 4);
			this.tbProductNotifyQty.Name = "tbProductNotifyQty";
			this.tbProductNotifyQty.Size = new global::System.Drawing.Size(369, 22);
			this.tbProductNotifyQty.TabIndex = 78;
			this.tbProductMinCartQty.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tbProductMinCartQty.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbProductMinCartQty.Location = new global::System.Drawing.Point(206, 274);
			this.tbProductMinCartQty.Margin = new global::System.Windows.Forms.Padding(4, 4, 27, 4);
			this.tbProductMinCartQty.Name = "tbProductMinCartQty";
			this.tbProductMinCartQty.Size = new global::System.Drawing.Size(369, 22);
			this.tbProductMinCartQty.TabIndex = 79;
			this.tbProductMaxCartQty.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tbProductMaxCartQty.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbProductMaxCartQty.Location = new global::System.Drawing.Point(206, 310);
			this.tbProductMaxCartQty.Margin = new global::System.Windows.Forms.Padding(4, 4, 27, 4);
			this.tbProductMaxCartQty.Name = "tbProductMaxCartQty";
			this.tbProductMaxCartQty.Size = new global::System.Drawing.Size(369, 22);
			this.tbProductMaxCartQty.TabIndex = 80;
			this.tbProductAllowQty.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tbProductAllowQty.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbProductAllowQty.Location = new global::System.Drawing.Point(206, 346);
			this.tbProductAllowQty.Margin = new global::System.Windows.Forms.Padding(4, 4, 27, 4);
			this.tbProductAllowQty.Name = "tbProductAllowQty";
			this.tbProductAllowQty.Size = new global::System.Drawing.Size(369, 22);
			this.tbProductAllowQty.TabIndex = 81;
			this.chProductMinStockQtyAction.AutoSize = true;
			this.chProductMinStockQtyAction.Location = new global::System.Drawing.Point(607, 202);
			this.chProductMinStockQtyAction.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductMinStockQtyAction.Name = "chProductMinStockQtyAction";
			this.chProductMinStockQtyAction.Size = new global::System.Drawing.Size(18, 17);
			this.chProductMinStockQtyAction.TabIndex = 82;
			this.chProductMinStockQtyAction.UseVisualStyleBackColor = true;
			this.chProductNotifyQtyAction.AutoSize = true;
			this.chProductNotifyQtyAction.Location = new global::System.Drawing.Point(607, 238);
			this.chProductNotifyQtyAction.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductNotifyQtyAction.Name = "chProductNotifyQtyAction";
			this.chProductNotifyQtyAction.Size = new global::System.Drawing.Size(18, 17);
			this.chProductNotifyQtyAction.TabIndex = 83;
			this.chProductNotifyQtyAction.UseVisualStyleBackColor = true;
			this.chProductMinCartQtyAction.AutoSize = true;
			this.chProductMinCartQtyAction.Location = new global::System.Drawing.Point(607, 274);
			this.chProductMinCartQtyAction.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductMinCartQtyAction.Name = "chProductMinCartQtyAction";
			this.chProductMinCartQtyAction.Size = new global::System.Drawing.Size(18, 17);
			this.chProductMinCartQtyAction.TabIndex = 84;
			this.chProductMinCartQtyAction.UseVisualStyleBackColor = true;
			this.chProductMaxCartQtyAction.AutoSize = true;
			this.chProductMaxCartQtyAction.Location = new global::System.Drawing.Point(607, 310);
			this.chProductMaxCartQtyAction.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductMaxCartQtyAction.Name = "chProductMaxCartQtyAction";
			this.chProductMaxCartQtyAction.Size = new global::System.Drawing.Size(18, 17);
			this.chProductMaxCartQtyAction.TabIndex = 85;
			this.chProductMaxCartQtyAction.UseVisualStyleBackColor = true;
			this.chProductAllowQtyAction.AutoSize = true;
			this.chProductAllowQtyAction.Location = new global::System.Drawing.Point(607, 346);
			this.chProductAllowQtyAction.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductAllowQtyAction.Name = "chProductAllowQtyAction";
			this.chProductAllowQtyAction.Size = new global::System.Drawing.Size(18, 17);
			this.chProductAllowQtyAction.TabIndex = 86;
			this.chProductAllowQtyAction.UseVisualStyleBackColor = true;
			this.label145.AutoSize = true;
			this.label145.Location = new global::System.Drawing.Point(5, 342);
			this.label145.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label145.Name = "label145";
			this.label145.Size = new global::System.Drawing.Size(121, 17);
			this.label145.TabIndex = 74;
			this.label145.Text = "Allowed quantities";
			this.label146.AutoSize = true;
			this.label146.Location = new global::System.Drawing.Point(5, 373);
			this.label146.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label146.Name = "label146";
			this.label146.Size = new global::System.Drawing.Size(150, 17);
			this.label146.TabIndex = 92;
			this.label146.Text = "Display Stock Quantity";
			this.chProductDisplayStockQuantity.AutoSize = true;
			this.chProductDisplayStockQuantity.Location = new global::System.Drawing.Point(206, 377);
			this.chProductDisplayStockQuantity.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductDisplayStockQuantity.Name = "chProductDisplayStockQuantity";
			this.chProductDisplayStockQuantity.Size = new global::System.Drawing.Size(18, 17);
			this.chProductDisplayStockQuantity.TabIndex = 93;
			this.chProductDisplayStockQuantity.UseVisualStyleBackColor = true;
			this.chProductDisplayStockQuantityImport.AutoSize = true;
			this.chProductDisplayStockQuantityImport.Location = new global::System.Drawing.Point(607, 377);
			this.chProductDisplayStockQuantityImport.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductDisplayStockQuantityImport.Name = "chProductDisplayStockQuantityImport";
			this.chProductDisplayStockQuantityImport.Size = new global::System.Drawing.Size(18, 17);
			this.chProductDisplayStockQuantityImport.TabIndex = 94;
			this.chProductDisplayStockQuantityImport.UseVisualStyleBackColor = true;
			this.label148.AutoSize = true;
			this.label148.Location = new global::System.Drawing.Point(5, 404);
			this.label148.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label148.Name = "label148";
			this.label148.Size = new global::System.Drawing.Size(128, 17);
			this.label148.TabIndex = 95;
			this.label148.Text = "Disable Buy Button";
			this.chDisableBuyButton.AutoSize = true;
			this.chDisableBuyButton.Location = new global::System.Drawing.Point(206, 408);
			this.chDisableBuyButton.Margin = new global::System.Windows.Forms.Padding(4);
			this.chDisableBuyButton.Name = "chDisableBuyButton";
			this.chDisableBuyButton.Size = new global::System.Drawing.Size(18, 17);
			this.chDisableBuyButton.TabIndex = 96;
			this.chDisableBuyButton.UseVisualStyleBackColor = true;
			this.chDisableBuyButtonImport.AutoSize = true;
			this.chDisableBuyButtonImport.Location = new global::System.Drawing.Point(607, 408);
			this.chDisableBuyButtonImport.Margin = new global::System.Windows.Forms.Padding(4);
			this.chDisableBuyButtonImport.Name = "chDisableBuyButtonImport";
			this.chDisableBuyButtonImport.Size = new global::System.Drawing.Size(18, 17);
			this.chDisableBuyButtonImport.TabIndex = 97;
			this.chDisableBuyButtonImport.UseVisualStyleBackColor = true;
			this.label149.AutoSize = true;
			this.label149.Location = new global::System.Drawing.Point(5, 435);
			this.label149.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label149.Name = "label149";
			this.label149.Size = new global::System.Drawing.Size(112, 17);
			this.label149.TabIndex = 98;
			this.label149.Text = "Backorder Mode";
			this.ddlBackorderMode.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.ddlBackorderMode.DropDownStyle = global::System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ddlBackorderMode.FlatStyle = global::System.Windows.Forms.FlatStyle.Popup;
			this.ddlBackorderMode.FormattingEnabled = true;
			this.ddlBackorderMode.Location = new global::System.Drawing.Point(206, 439);
			this.ddlBackorderMode.Margin = new global::System.Windows.Forms.Padding(4, 4, 27, 4);
			this.ddlBackorderMode.Name = "ddlBackorderMode";
			this.ddlBackorderMode.Size = new global::System.Drawing.Size(369, 24);
			this.ddlBackorderMode.TabIndex = 99;
			this.chBackorderModeImport.AutoSize = true;
			this.chBackorderModeImport.Location = new global::System.Drawing.Point(607, 439);
			this.chBackorderModeImport.Margin = new global::System.Windows.Forms.Padding(4);
			this.chBackorderModeImport.Name = "chBackorderModeImport";
			this.chBackorderModeImport.Size = new global::System.Drawing.Size(18, 17);
			this.chBackorderModeImport.TabIndex = 100;
			this.chBackorderModeImport.UseVisualStyleBackColor = true;
			this.label157.AutoSize = true;
			this.label157.Location = new global::System.Drawing.Point(4, 471);
			this.label157.Name = "label157";
			this.label157.Size = new global::System.Drawing.Size(118, 17);
			this.label157.TabIndex = 102;
			this.label157.Text = "Shipping enabled";
			this.label158.AutoSize = true;
			this.label158.Location = new global::System.Drawing.Point(4, 502);
			this.label158.Name = "label158";
			this.label158.Size = new global::System.Drawing.Size(96, 17);
			this.label158.TabIndex = 103;
			this.label158.Text = "Free Shipping";
			this.label159.AutoSize = true;
			this.label159.Location = new global::System.Drawing.Point(4, 533);
			this.label159.Name = "label159";
			this.label159.Size = new global::System.Drawing.Size(108, 17);
			this.label159.TabIndex = 104;
			this.label159.Text = "Ship Separately";
			this.label160.AutoSize = true;
			this.label160.Location = new global::System.Drawing.Point(4, 564);
			this.label160.Name = "label160";
			this.label160.Size = new global::System.Drawing.Size(179, 17);
			this.label160.TabIndex = 105;
			this.label160.Text = "Additional Shipping Charge";
			this.chProductShippingEnabled.AutoSize = true;
			this.chProductShippingEnabled.Location = new global::System.Drawing.Point(206, 475);
			this.chProductShippingEnabled.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductShippingEnabled.Name = "chProductShippingEnabled";
			this.chProductShippingEnabled.Size = new global::System.Drawing.Size(18, 17);
			this.chProductShippingEnabled.TabIndex = 110;
			this.chProductShippingEnabled.UseVisualStyleBackColor = true;
			this.chProductFreeShipping.AutoSize = true;
			this.chProductFreeShipping.Location = new global::System.Drawing.Point(206, 506);
			this.chProductFreeShipping.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductFreeShipping.Name = "chProductFreeShipping";
			this.chProductFreeShipping.Size = new global::System.Drawing.Size(18, 17);
			this.chProductFreeShipping.TabIndex = 111;
			this.chProductFreeShipping.UseVisualStyleBackColor = true;
			this.chProductShipSeparately.AutoSize = true;
			this.chProductShipSeparately.Location = new global::System.Drawing.Point(206, 537);
			this.chProductShipSeparately.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductShipSeparately.Name = "chProductShipSeparately";
			this.chProductShipSeparately.Size = new global::System.Drawing.Size(18, 17);
			this.chProductShipSeparately.TabIndex = 112;
			this.chProductShipSeparately.UseVisualStyleBackColor = true;
			this.chProductShippingEnabledImport.AutoSize = true;
			this.chProductShippingEnabledImport.Location = new global::System.Drawing.Point(607, 475);
			this.chProductShippingEnabledImport.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductShippingEnabledImport.Name = "chProductShippingEnabledImport";
			this.chProductShippingEnabledImport.Size = new global::System.Drawing.Size(18, 17);
			this.chProductShippingEnabledImport.TabIndex = 113;
			this.chProductShippingEnabledImport.UseVisualStyleBackColor = true;
			this.chProductFreeShippingImport.AutoSize = true;
			this.chProductFreeShippingImport.Location = new global::System.Drawing.Point(607, 506);
			this.chProductFreeShippingImport.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductFreeShippingImport.Name = "chProductFreeShippingImport";
			this.chProductFreeShippingImport.Size = new global::System.Drawing.Size(18, 17);
			this.chProductFreeShippingImport.TabIndex = 114;
			this.chProductFreeShippingImport.UseVisualStyleBackColor = true;
			this.chProductShipSeparatelyImport.AutoSize = true;
			this.chProductShipSeparatelyImport.Location = new global::System.Drawing.Point(607, 537);
			this.chProductShipSeparatelyImport.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductShipSeparatelyImport.Name = "chProductShipSeparatelyImport";
			this.chProductShipSeparatelyImport.Size = new global::System.Drawing.Size(18, 17);
			this.chProductShipSeparatelyImport.TabIndex = 115;
			this.chProductShipSeparatelyImport.UseVisualStyleBackColor = true;
			this.chProductShippingChargeImport.AutoSize = true;
			this.chProductShippingChargeImport.Location = new global::System.Drawing.Point(607, 568);
			this.chProductShippingChargeImport.Margin = new global::System.Windows.Forms.Padding(4);
			this.chProductShippingChargeImport.Name = "chProductShippingChargeImport";
			this.chProductShippingChargeImport.Size = new global::System.Drawing.Size(18, 17);
			this.chProductShippingChargeImport.TabIndex = 116;
			this.chProductShippingChargeImport.UseVisualStyleBackColor = true;
			this.nmProductShippingCharge.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.nmProductShippingCharge.DecimalPlaces = 2;
			this.nmProductShippingCharge.Location = new global::System.Drawing.Point(206, 568);
			this.nmProductShippingCharge.Margin = new global::System.Windows.Forms.Padding(4);
			global::System.Windows.Forms.NumericUpDown numericUpDown3 = this.nmProductShippingCharge;
			int[] array3 = new int[4];
			array3[0] = 99999;
			numericUpDown3.Maximum = new decimal(array3);
			this.nmProductShippingCharge.Minimum = new decimal(new int[]
			{
				99999,
				0,
				0,
				int.MinValue
			});
			this.nmProductShippingCharge.Name = "nmProductShippingCharge";
			this.nmProductShippingCharge.Size = new global::System.Drawing.Size(61, 22);
			this.nmProductShippingCharge.TabIndex = 117;
			this.ddInventoryMethod.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.ddInventoryMethod.DropDownStyle = global::System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ddInventoryMethod.FlatStyle = global::System.Windows.Forms.FlatStyle.Popup;
			this.ddInventoryMethod.FormattingEnabled = true;
			this.ddInventoryMethod.Location = new global::System.Drawing.Point(206, 104);
			this.ddInventoryMethod.Margin = new global::System.Windows.Forms.Padding(4, 4, 27, 4);
			this.ddInventoryMethod.Name = "ddInventoryMethod";
			this.ddInventoryMethod.Size = new global::System.Drawing.Size(369, 24);
			this.ddInventoryMethod.TabIndex = 118;
			this.lblShow.AutoSize = true;
			this.lblShow.Font = new global::System.Drawing.Font("Microsoft Sans Serif", 10.2f, global::System.Drawing.FontStyle.Bold, global::System.Drawing.GraphicsUnit.Point, 186);
			this.lblShow.Location = new global::System.Drawing.Point(588, 17);
			this.lblShow.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShow.Name = "lblShow";
			this.lblShow.Size = new global::System.Drawing.Size(102, 20);
			this.lblShow.TabIndex = 8;
			this.lblShow.Text = "Show item:";
			this.lblFoundItems.AutoSize = true;
			this.lblFoundItems.Font = new global::System.Drawing.Font("Microsoft Sans Serif", 10.2f, global::System.Drawing.FontStyle.Bold, global::System.Drawing.GraphicsUnit.Point, 186);
			this.lblFoundItems.Location = new global::System.Drawing.Point(377, 17);
			this.lblFoundItems.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFoundItems.Name = "lblFoundItems";
			this.lblFoundItems.Size = new global::System.Drawing.Size(124, 20);
			this.lblFoundItems.TabIndex = 7;
			this.lblFoundItems.Text = "Found items: ";
			this.label4.AutoSize = true;
			this.label4.Font = new global::System.Drawing.Font("Microsoft Sans Serif", 12f, global::System.Drawing.FontStyle.Bold, global::System.Drawing.GraphicsUnit.Point, 0);
			this.label4.Location = new global::System.Drawing.Point(19, 9);
			this.label4.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new global::System.Drawing.Size(77, 25);
			this.label4.TabIndex = 9;
			this.label4.Text = "Store: ";
			this.ddStoresConn.FormattingEnabled = true;
			this.ddStoresConn.Location = new global::System.Drawing.Point(94, 13);
			this.ddStoresConn.Name = "ddStoresConn";
			this.ddStoresConn.Size = new global::System.Drawing.Size(242, 24);
			this.ddStoresConn.TabIndex = 10;
			this.ddStoresConn.SelectedIndexChanged += new global::System.EventHandler(this.ddStoresConn_SelectedIndexChanged);
			this.btnImportToStore.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left);
			this.btnImportToStore.Location = new global::System.Drawing.Point(20, 672);
			this.btnImportToStore.Name = "btnImportToStore";
			this.btnImportToStore.Size = new global::System.Drawing.Size(145, 33);
			this.btnImportToStore.TabIndex = 11;
			this.btnImportToStore.Text = "Import To Store";
			this.btnImportToStore.UseVisualStyleBackColor = true;
			this.btnImportToStore.Click += new global::System.EventHandler(this.button1_Click);
			this.btnPrev.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnPrev.Location = new global::System.Drawing.Point(877, 676);
			this.btnPrev.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnPrev.Name = "btnPrev";
			this.btnPrev.Size = new global::System.Drawing.Size(100, 28);
			this.btnPrev.TabIndex = 14;
			this.btnPrev.Text = "Previuos";
			this.btnPrev.UseVisualStyleBackColor = true;
			this.btnPrev.Click += new global::System.EventHandler(this.btnPrev_Click);
			this.btnNext.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnNext.Location = new global::System.Drawing.Point(985, 676);
			this.btnNext.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnNext.Name = "btnNext";
			this.btnNext.Size = new global::System.Drawing.Size(100, 28);
			this.btnNext.TabIndex = 13;
			this.btnNext.Text = "Next";
			this.btnNext.UseVisualStyleBackColor = true;
			this.btnNext.Click += new global::System.EventHandler(this.btnNext_Click);
			this.btnClose.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnClose.Location = new global::System.Drawing.Point(1088, 676);
			this.btnClose.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new global::System.Drawing.Size(100, 28);
			this.btnClose.TabIndex = 12;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new global::System.EventHandler(this.btnClose_Click);
			this.statusArea.AutoSize = false;
			this.statusArea.ImageScalingSize = new global::System.Drawing.Size(20, 20);
			this.statusArea.Items.AddRange(new global::System.Windows.Forms.ToolStripItem[]
			{
				this.statusAreaText
			});
			this.statusArea.Location = new global::System.Drawing.Point(0, 708);
			this.statusArea.Name = "statusArea";
			this.statusArea.Padding = new global::System.Windows.Forms.Padding(1, 0, 19, 0);
			this.statusArea.Size = new global::System.Drawing.Size(1201, 27);
			this.statusArea.TabIndex = 30;
			this.statusArea.Text = "statusArea";
			this.statusAreaText.Name = "statusAreaText";
			this.statusAreaText.Size = new global::System.Drawing.Size(0, 22);
			this.tbFullDescRichEditor.AllowDrop = true;
			this.tbFullDescRichEditor.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tbFullDescRichEditor.AutoScroll = true;
//			this.tbFullDescRichEditor.Html = componentResourceManager.GetString("tbFullDescRichEditor.Html");
			this.tbFullDescRichEditor.Location = new global::System.Drawing.Point(0, 0);
			this.tbFullDescRichEditor.Margin = new global::System.Windows.Forms.Padding(4, 4, 4, 4);
			this.tbFullDescRichEditor.Name = "tbFullDescRichEditor";
			this.tbFullDescRichEditor.ReadOnly = false;
			this.tbFullDescRichEditor.ShowToolbar = true;
			this.tbFullDescRichEditor.Size = new global::System.Drawing.Size(1155, 569);
			this.tbFullDescRichEditor.TabIndex = 0;
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(8f, 16f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(1201, 735);
			base.Controls.Add(this.statusArea);
			base.Controls.Add(this.btnPrev);
			base.Controls.Add(this.btnNext);
			base.Controls.Add(this.btnClose);
			base.Controls.Add(this.btnImportToStore);
			base.Controls.Add(this.ddStoresConn);
			base.Controls.Add(this.label4);
			base.Controls.Add(this.lblShow);
			base.Controls.Add(this.lblFoundItems);
			base.Controls.Add(this.tabControl1);
			base.Name = "productCard";
			this.Text = "Quick Product Import";
			base.Load += new global::System.EventHandler(this.productCard_Load);
			this.tabControl1.ResumeLayout(false);
			this.tabProductInfo.ResumeLayout(false);
			this.tabProductInfo.PerformLayout();
			((global::System.ComponentModel.ISupportInitialize)this.nmProdQuantity).EndInit();
			((global::System.ComponentModel.ISupportInitialize)this.nmProdPrice).EndInit();
			((global::System.ComponentModel.ISupportInitialize)this.picMain).EndInit();
			this.tabProductDescription.ResumeLayout(false);
			this.tabControl2.ResumeLayout(false);
			this.tabRichEditor.ResumeLayout(false);
			this.tabTextEditor.ResumeLayout(false);
			this.tabTextEditor.PerformLayout();
			this.tabProductsVariants.ResumeLayout(false);
			((global::System.ComponentModel.ISupportInitialize)this.dgProdVariants).EndInit();
			this.tabProductImages.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.tabProductSettings.ResumeLayout(false);
			this.tableLayoutPanel9.ResumeLayout(false);
			this.tableLayoutPanel9.PerformLayout();
			((global::System.ComponentModel.ISupportInitialize)this.nmProductShippingCharge).EndInit();
			this.statusArea.ResumeLayout(false);
			this.statusArea.PerformLayout();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		// Token: 0x040000CC RID: 204
		private global::System.ComponentModel.IContainer components;

		// Token: 0x040000CD RID: 205
		private global::System.Windows.Forms.TabControl tabControl1;

		// Token: 0x040000CE RID: 206
		private global::System.Windows.Forms.TabPage tabProductInfo;

		// Token: 0x040000CF RID: 207
		private global::System.Windows.Forms.TabPage tabProductDescription;

		// Token: 0x040000D0 RID: 208
		private global::System.Windows.Forms.TabPage tabProductsVariants;

		// Token: 0x040000D1 RID: 209
		private global::System.Windows.Forms.TabPage tabProductImages;

		// Token: 0x040000D2 RID: 210
		private global::System.Windows.Forms.TabPage tabProductSettings;

		// Token: 0x040000D3 RID: 211
		private global::System.Windows.Forms.Label label1;

		// Token: 0x040000D4 RID: 212
		private global::System.Windows.Forms.PictureBox picMain;

		// Token: 0x040000D5 RID: 213
		private global::System.Windows.Forms.TextBox tbProductTitle;

		// Token: 0x040000D6 RID: 214
		private global::System.Windows.Forms.Label label2;

		// Token: 0x040000D7 RID: 215
		private global::System.Windows.Forms.Label label3;

		// Token: 0x040000D8 RID: 216
		private global::System.Windows.Forms.TextBox tbProdShortDesc;

		// Token: 0x040000D9 RID: 217
		private global::System.Windows.Forms.Label lblShow;

		// Token: 0x040000DA RID: 218
		private global::System.Windows.Forms.Label lblFoundItems;

		// Token: 0x040000DB RID: 219
		private global::System.Windows.Forms.CheckedListBox clbCategoriesList;

		// Token: 0x040000DC RID: 220
		private global::System.Windows.Forms.Label label4;

		// Token: 0x040000DD RID: 221
		private global::System.Windows.Forms.ComboBox ddStoresConn;

		// Token: 0x040000DE RID: 222
		private global::System.Windows.Forms.TabControl tabControl2;

		// Token: 0x040000DF RID: 223
		private global::System.Windows.Forms.TabPage tabRichEditor;

		// Token: 0x040000E0 RID: 224
		private global::System.Windows.Forms.TabPage tabTextEditor;

		// Token: 0x040000E1 RID: 225
		private global::System.Windows.Forms.TextBox tbFullDescTextEditor;

		// Token: 0x040000E2 RID: 226
		private global::System.Windows.Forms.Button btnImportToStore;

		// Token: 0x040000E3 RID: 227
		private global::YARTE.UI.HtmlEditor tbFullDescRichEditor;

		// Token: 0x040000E4 RID: 228
		private global::System.Windows.Forms.ListView imageListViewAssigned;

		// Token: 0x040000E5 RID: 229
		private global::System.Windows.Forms.Button btnImageUnassign;

		// Token: 0x040000E6 RID: 230
		private global::System.Windows.Forms.Button btnImageAssign;

		// Token: 0x040000E7 RID: 231
		private global::System.Windows.Forms.Label label6;

		// Token: 0x040000E8 RID: 232
		private global::System.Windows.Forms.Label label5;

		// Token: 0x040000E9 RID: 233
		private global::System.Windows.Forms.ListView imageListViewUnassigned;

		// Token: 0x040000EA RID: 234
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

		// Token: 0x040000EB RID: 235
		private global::System.Windows.Forms.Panel panel1;

		// Token: 0x040000EC RID: 236
		private global::System.Windows.Forms.DataGridView dgProdVariants;

		// Token: 0x040000ED RID: 237
		private global::System.Windows.Forms.NumericUpDown nmProdPrice;

		// Token: 0x040000EE RID: 238
		private global::System.Windows.Forms.Label label8;

		// Token: 0x040000EF RID: 239
		private global::System.Windows.Forms.Label label7;

		// Token: 0x040000F0 RID: 240
		private global::System.Windows.Forms.NumericUpDown nmProdQuantity;

		// Token: 0x040000F1 RID: 241
		private global::System.Windows.Forms.Button btnPrev;

		// Token: 0x040000F2 RID: 242
		private global::System.Windows.Forms.Button btnNext;

		// Token: 0x040000F3 RID: 243
		private global::System.Windows.Forms.Button btnClose;

		// Token: 0x040000F4 RID: 244
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;

		// Token: 0x040000F5 RID: 245
		private global::System.Windows.Forms.Label label71;

		// Token: 0x040000F6 RID: 246
		private global::System.Windows.Forms.Label label101;

		// Token: 0x040000F7 RID: 247
		private global::System.Windows.Forms.Label label102;

		// Token: 0x040000F8 RID: 248
		private global::System.Windows.Forms.Label label104;

		// Token: 0x040000F9 RID: 249
		private global::System.Windows.Forms.Label label118;

		// Token: 0x040000FA RID: 250
		private global::System.Windows.Forms.CheckBox chProductPublishImport;

		// Token: 0x040000FB RID: 251
		private global::System.Windows.Forms.Label label117;

		// Token: 0x040000FC RID: 252
		private global::System.Windows.Forms.ComboBox ddDeliveryDate;

		// Token: 0x040000FD RID: 253
		private global::System.Windows.Forms.CheckBox chDayDeliveryImport;

		// Token: 0x040000FE RID: 254
		private global::System.Windows.Forms.Label label119;

		// Token: 0x040000FF RID: 255
		private global::System.Windows.Forms.CheckBox chInventoryMethodImport;

		// Token: 0x04000100 RID: 256
		private global::System.Windows.Forms.CheckBox chProductPublish;

		// Token: 0x04000101 RID: 257
		private global::System.Windows.Forms.CheckBox chProductReviewEnable;

		// Token: 0x04000102 RID: 258
		private global::System.Windows.Forms.CheckBox chProductReviewEnableImport;

		// Token: 0x04000103 RID: 259
		private global::System.Windows.Forms.Label label140;

		// Token: 0x04000104 RID: 260
		private global::System.Windows.Forms.Label label141;

		// Token: 0x04000105 RID: 261
		private global::System.Windows.Forms.Label label142;

		// Token: 0x04000106 RID: 262
		private global::System.Windows.Forms.Label label143;

		// Token: 0x04000107 RID: 263
		private global::System.Windows.Forms.Label label144;

		// Token: 0x04000108 RID: 264
		private global::System.Windows.Forms.CheckBox chProductDisplayAvailability;

		// Token: 0x04000109 RID: 265
		private global::System.Windows.Forms.CheckBox chProductDisplayAvailabilityImport;

		// Token: 0x0400010A RID: 266
		private global::System.Windows.Forms.TextBox tbProductMinStockQty;

		// Token: 0x0400010B RID: 267
		private global::System.Windows.Forms.TextBox tbProductNotifyQty;

		// Token: 0x0400010C RID: 268
		private global::System.Windows.Forms.TextBox tbProductMinCartQty;

		// Token: 0x0400010D RID: 269
		private global::System.Windows.Forms.TextBox tbProductMaxCartQty;

		// Token: 0x0400010E RID: 270
		private global::System.Windows.Forms.TextBox tbProductAllowQty;

		// Token: 0x0400010F RID: 271
		private global::System.Windows.Forms.CheckBox chProductMinStockQtyAction;

		// Token: 0x04000110 RID: 272
		private global::System.Windows.Forms.CheckBox chProductNotifyQtyAction;

		// Token: 0x04000111 RID: 273
		private global::System.Windows.Forms.CheckBox chProductMinCartQtyAction;

		// Token: 0x04000112 RID: 274
		private global::System.Windows.Forms.CheckBox chProductMaxCartQtyAction;

		// Token: 0x04000113 RID: 275
		private global::System.Windows.Forms.CheckBox chProductAllowQtyAction;

		// Token: 0x04000114 RID: 276
		private global::System.Windows.Forms.Label label145;

		// Token: 0x04000115 RID: 277
		private global::System.Windows.Forms.Label label146;

		// Token: 0x04000116 RID: 278
		private global::System.Windows.Forms.CheckBox chProductDisplayStockQuantity;

		// Token: 0x04000117 RID: 279
		private global::System.Windows.Forms.CheckBox chProductDisplayStockQuantityImport;

		// Token: 0x04000118 RID: 280
		private global::System.Windows.Forms.Label label148;

		// Token: 0x04000119 RID: 281
		private global::System.Windows.Forms.CheckBox chDisableBuyButton;

		// Token: 0x0400011A RID: 282
		private global::System.Windows.Forms.CheckBox chDisableBuyButtonImport;

		// Token: 0x0400011B RID: 283
		private global::System.Windows.Forms.Label label149;

		// Token: 0x0400011C RID: 284
		private global::System.Windows.Forms.ComboBox ddlBackorderMode;

		// Token: 0x0400011D RID: 285
		private global::System.Windows.Forms.CheckBox chBackorderModeImport;

		// Token: 0x0400011E RID: 286
		private global::System.Windows.Forms.Label label157;

		// Token: 0x0400011F RID: 287
		private global::System.Windows.Forms.Label label158;

		// Token: 0x04000120 RID: 288
		private global::System.Windows.Forms.Label label159;

		// Token: 0x04000121 RID: 289
		private global::System.Windows.Forms.Label label160;

		// Token: 0x04000122 RID: 290
		private global::System.Windows.Forms.CheckBox chProductShippingEnabled;

		// Token: 0x04000123 RID: 291
		private global::System.Windows.Forms.CheckBox chProductFreeShipping;

		// Token: 0x04000124 RID: 292
		private global::System.Windows.Forms.CheckBox chProductShipSeparately;

		// Token: 0x04000125 RID: 293
		private global::System.Windows.Forms.CheckBox chProductShippingEnabledImport;

		// Token: 0x04000126 RID: 294
		private global::System.Windows.Forms.CheckBox chProductFreeShippingImport;

		// Token: 0x04000127 RID: 295
		private global::System.Windows.Forms.CheckBox chProductShipSeparatelyImport;

		// Token: 0x04000128 RID: 296
		private global::System.Windows.Forms.CheckBox chProductShippingChargeImport;

		// Token: 0x04000129 RID: 297
		private global::System.Windows.Forms.NumericUpDown nmProductShippingCharge;

		// Token: 0x0400012A RID: 298
		private global::System.Windows.Forms.ComboBox ddInventoryMethod;

		// Token: 0x0400012B RID: 299
		private global::System.Windows.Forms.StatusStrip statusArea;

		// Token: 0x0400012C RID: 300
		private global::System.Windows.Forms.ToolStripStatusLabel statusAreaText;
	}
}
