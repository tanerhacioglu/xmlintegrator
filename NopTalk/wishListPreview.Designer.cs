﻿namespace NopTalk
{
	// Token: 0x0200005A RID: 90
	public partial class wishListPreview : global::System.Windows.Forms.Form
	{
		// Token: 0x0600044D RID: 1101 RVA: 0x00003E84 File Offset: 0x00002084
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x0600044E RID: 1102 RVA: 0x0005EBF0 File Offset: 0x0005CDF0
		private void InitializeComponent()
		{
			this.tableLayoutPanel1 = new global::System.Windows.Forms.TableLayoutPanel();
			this.dgWishList = new global::System.Windows.Forms.DataGridView();
			this.label1 = new global::System.Windows.Forms.Label();
			this.lblFoundItems = new global::System.Windows.Forms.Label();
			this.lblShow = new global::System.Windows.Forms.Label();
			this.btnPrev = new global::System.Windows.Forms.Button();
			this.btnNext = new global::System.Windows.Forms.Button();
			this.btnClose = new global::System.Windows.Forms.Button();
			this.tableLayoutPanel1.SuspendLayout();
			((global::System.ComponentModel.ISupportInitialize)this.dgWishList).BeginInit();
			base.SuspendLayout();
			this.tableLayoutPanel1.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tableLayoutPanel1.CellBorderStyle = global::System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new global::System.Windows.Forms.ColumnStyle(global::System.Windows.Forms.SizeType.Percent, 14.91803f));
			this.tableLayoutPanel1.ColumnStyles.Add(new global::System.Windows.Forms.ColumnStyle(global::System.Windows.Forms.SizeType.Percent, 85.08197f));
			this.tableLayoutPanel1.Controls.Add(this.dgWishList, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel1.Location = new global::System.Drawing.Point(21, 52);
			this.tableLayoutPanel1.Margin = new global::System.Windows.Forms.Padding(4);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Percent, 100f));
			this.tableLayoutPanel1.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 20f));
			this.tableLayoutPanel1.Size = new global::System.Drawing.Size(511, 199);
			this.tableLayoutPanel1.TabIndex = 21;
			this.dgWishList.AllowUserToAddRows = false;
			this.dgWishList.AllowUserToDeleteRows = false;
			this.dgWishList.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.dgWishList.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgWishList.Location = new global::System.Drawing.Point(81, 5);
			this.dgWishList.Margin = new global::System.Windows.Forms.Padding(4);
			this.dgWishList.MultiSelect = false;
			this.dgWishList.Name = "dgWishList";
			this.dgWishList.RowHeadersWidth = 51;
			this.dgWishList.SelectionMode = global::System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgWishList.Size = new global::System.Drawing.Size(425, 189);
			this.dgWishList.TabIndex = 6;
			this.label1.AutoSize = true;
			this.label1.Location = new global::System.Drawing.Point(5, 1);
			this.label1.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new global::System.Drawing.Size(61, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "WishList";
			this.lblFoundItems.AutoSize = true;
			this.lblFoundItems.Font = new global::System.Drawing.Font("Microsoft Sans Serif", 12f, global::System.Drawing.FontStyle.Bold, global::System.Drawing.GraphicsUnit.Point, 0);
			this.lblFoundItems.Location = new global::System.Drawing.Point(14, 20);
			this.lblFoundItems.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFoundItems.Name = "lblFoundItems";
			this.lblFoundItems.Size = new global::System.Drawing.Size(144, 25);
			this.lblFoundItems.TabIndex = 22;
			this.lblFoundItems.Text = "Found Items: ";
			this.lblShow.AutoSize = true;
			this.lblShow.Font = new global::System.Drawing.Font("Microsoft Sans Serif", 12f, global::System.Drawing.FontStyle.Bold, global::System.Drawing.GraphicsUnit.Point, 0);
			this.lblShow.Location = new global::System.Drawing.Point(285, 20);
			this.lblShow.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShow.Name = "lblShow";
			this.lblShow.Size = new global::System.Drawing.Size(138, 25);
			this.lblShow.TabIndex = 26;
			this.lblShow.Text = "Current Item:";
			this.btnPrev.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnPrev.Location = new global::System.Drawing.Point(216, 285);
			this.btnPrev.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnPrev.Name = "btnPrev";
			this.btnPrev.Size = new global::System.Drawing.Size(100, 28);
			this.btnPrev.TabIndex = 25;
			this.btnPrev.Text = "Previuos";
			this.btnPrev.UseVisualStyleBackColor = true;
			this.btnPrev.Click += new global::System.EventHandler(this.btnPrev_Click);
			this.btnNext.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnNext.Location = new global::System.Drawing.Point(324, 285);
			this.btnNext.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnNext.Name = "btnNext";
			this.btnNext.Size = new global::System.Drawing.Size(100, 28);
			this.btnNext.TabIndex = 24;
			this.btnNext.Text = "Next";
			this.btnNext.UseVisualStyleBackColor = true;
			this.btnNext.Click += new global::System.EventHandler(this.btnNext_Click);
			this.btnClose.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnClose.Location = new global::System.Drawing.Point(432, 285);
			this.btnClose.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new global::System.Drawing.Size(100, 28);
			this.btnClose.TabIndex = 23;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new global::System.EventHandler(this.btnClose_Click);
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(8f, 16f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(545, 326);
			base.Controls.Add(this.tableLayoutPanel1);
			base.Controls.Add(this.lblFoundItems);
			base.Controls.Add(this.lblShow);
			base.Controls.Add(this.btnPrev);
			base.Controls.Add(this.btnNext);
			base.Controls.Add(this.btnClose);
			base.Name = "wishListPreview";
			this.Text = "Wish List Preview";
			base.Load += new global::System.EventHandler(this.wishListPreview_Load);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			((global::System.ComponentModel.ISupportInitialize)this.dgWishList).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		// Token: 0x04000866 RID: 2150
		private global::System.ComponentModel.IContainer components;

		// Token: 0x04000867 RID: 2151
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

		// Token: 0x04000868 RID: 2152
		private global::System.Windows.Forms.DataGridView dgWishList;

		// Token: 0x04000869 RID: 2153
		private global::System.Windows.Forms.Label label1;

		// Token: 0x0400086A RID: 2154
		private global::System.Windows.Forms.Label lblFoundItems;

		// Token: 0x0400086B RID: 2155
		private global::System.Windows.Forms.Label lblShow;

		// Token: 0x0400086C RID: 2156
		private global::System.Windows.Forms.Button btnPrev;

		// Token: 0x0400086D RID: 2157
		private global::System.Windows.Forms.Button btnNext;

		// Token: 0x0400086E RID: 2158
		private global::System.Windows.Forms.Button btnClose;
	}
}
