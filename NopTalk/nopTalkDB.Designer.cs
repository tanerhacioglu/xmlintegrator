﻿namespace NopTalk
{
	// Token: 0x02000020 RID: 32
	public partial class nopTalkDB : global::System.Windows.Forms.Form
	{
		// Token: 0x060000CC RID: 204 RVA: 0x0000297C File Offset: 0x00000B7C
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x060000CD RID: 205 RVA: 0x0000E9B8 File Offset: 0x0000CBB8
		private void InitializeComponent()
		{
			global::System.ComponentModel.ComponentResourceManager componentResourceManager = new global::System.ComponentModel.ComponentResourceManager(typeof(global::NopTalk.nopTalkDB));
			this.btnSave = new global::System.Windows.Forms.Button();
			this.tableLayoutPanel1 = new global::System.Windows.Forms.TableLayoutPanel();
			this.label3 = new global::System.Windows.Forms.Label();
			this.tbUserName = new global::System.Windows.Forms.TextBox();
			this.tbServerName = new global::System.Windows.Forms.TextBox();
			this.label1 = new global::System.Windows.Forms.Label();
			this.label2 = new global::System.Windows.Forms.Label();
			this.lblLogin = new global::System.Windows.Forms.Label();
			this.ddAuthType = new global::System.Windows.Forms.ComboBox();
			this.tbPassword = new global::System.Windows.Forms.TextBox();
			this.label4 = new global::System.Windows.Forms.Label();
			this.tbDatabaseName = new global::System.Windows.Forms.TextBox();
			this.btnExit = new global::System.Windows.Forms.Button();
			this.tableLayoutPanel1.SuspendLayout();
			base.SuspendLayout();
			this.btnSave.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnSave.Location = new global::System.Drawing.Point(280, 356);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new global::System.Drawing.Size(75, 23);
			this.btnSave.TabIndex = 4;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new global::System.EventHandler(this.btnSave_Click);
			this.tableLayoutPanel1.CellBorderStyle = global::System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new global::System.Windows.Forms.ColumnStyle(global::System.Windows.Forms.SizeType.Absolute, 150f));
			this.tableLayoutPanel1.ColumnStyles.Add(new global::System.Windows.Forms.ColumnStyle(global::System.Windows.Forms.SizeType.Percent, 100f));
			this.tableLayoutPanel1.Controls.Add(this.label3, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.tbUserName, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.tbServerName, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.lblLogin, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.ddAuthType, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.tbPassword, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
			this.tableLayoutPanel1.Controls.Add(this.tbDatabaseName, 1, 4);
			this.tableLayoutPanel1.Location = new global::System.Drawing.Point(12, 12);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 6;
			this.tableLayoutPanel1.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 30f));
			this.tableLayoutPanel1.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 30f));
			this.tableLayoutPanel1.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 30f));
			this.tableLayoutPanel1.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 30f));
			this.tableLayoutPanel1.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Absolute, 30f));
			this.tableLayoutPanel1.RowStyles.Add(new global::System.Windows.Forms.RowStyle(global::System.Windows.Forms.SizeType.Percent, 100f));
			this.tableLayoutPanel1.Size = new global::System.Drawing.Size(431, 327);
			this.tableLayoutPanel1.TabIndex = 6;
			this.label3.AutoSize = true;
			this.label3.Location = new global::System.Drawing.Point(4, 94);
			this.label3.Name = "label3";
			this.label3.Size = new global::System.Drawing.Size(53, 13);
			this.label3.TabIndex = 33;
			this.label3.Text = "Password";
			this.tbUserName.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tbUserName.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbUserName.Location = new global::System.Drawing.Point(155, 66);
			this.tbUserName.Margin = new global::System.Windows.Forms.Padding(3, 3, 20, 3);
			this.tbUserName.Name = "tbUserName";
			this.tbUserName.Size = new global::System.Drawing.Size(255, 20);
			this.tbUserName.TabIndex = 32;
			this.tbServerName.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tbServerName.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbServerName.Location = new global::System.Drawing.Point(155, 4);
			this.tbServerName.Margin = new global::System.Windows.Forms.Padding(3, 3, 20, 3);
			this.tbServerName.Name = "tbServerName";
			this.tbServerName.Size = new global::System.Drawing.Size(255, 20);
			this.tbServerName.TabIndex = 31;
			this.label1.AutoSize = true;
			this.label1.Location = new global::System.Drawing.Point(4, 1);
			this.label1.Name = "label1";
			this.label1.Size = new global::System.Drawing.Size(67, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Server name";
			this.label2.AutoSize = true;
			this.label2.Location = new global::System.Drawing.Point(4, 32);
			this.label2.Name = "label2";
			this.label2.Size = new global::System.Drawing.Size(75, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Authentication";
			this.lblLogin.AutoSize = true;
			this.lblLogin.Location = new global::System.Drawing.Point(4, 63);
			this.lblLogin.Name = "lblLogin";
			this.lblLogin.Size = new global::System.Drawing.Size(33, 13);
			this.lblLogin.TabIndex = 2;
			this.lblLogin.Text = "Login";
			this.ddAuthType.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.ddAuthType.DropDownStyle = global::System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ddAuthType.FlatStyle = global::System.Windows.Forms.FlatStyle.Popup;
			this.ddAuthType.FormattingEnabled = true;
			this.ddAuthType.Items.AddRange(new object[]
			{
				"Windows authentication",
				"Sql server authentication"
			});
			this.ddAuthType.Location = new global::System.Drawing.Point(155, 35);
			this.ddAuthType.Margin = new global::System.Windows.Forms.Padding(3, 3, 20, 3);
			this.ddAuthType.Name = "ddAuthType";
			this.ddAuthType.Size = new global::System.Drawing.Size(255, 21);
			this.ddAuthType.TabIndex = 30;
			this.ddAuthType.SelectedIndexChanged += new global::System.EventHandler(this.ddAuthType_SelectedIndexChanged);
			this.tbPassword.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tbPassword.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPassword.Location = new global::System.Drawing.Point(155, 97);
			this.tbPassword.Margin = new global::System.Windows.Forms.Padding(3, 3, 20, 3);
			this.tbPassword.Name = "tbPassword";
			this.tbPassword.Size = new global::System.Drawing.Size(255, 20);
			this.tbPassword.TabIndex = 34;
			this.label4.AutoSize = true;
			this.label4.Location = new global::System.Drawing.Point(4, 125);
			this.label4.Name = "label4";
			this.label4.Size = new global::System.Drawing.Size(82, 13);
			this.label4.TabIndex = 35;
			this.label4.Text = "Database name";
			this.tbDatabaseName.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tbDatabaseName.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbDatabaseName.Location = new global::System.Drawing.Point(155, 128);
			this.tbDatabaseName.Margin = new global::System.Windows.Forms.Padding(3, 3, 20, 3);
			this.tbDatabaseName.Name = "tbDatabaseName";
			this.tbDatabaseName.Size = new global::System.Drawing.Size(255, 20);
			this.tbDatabaseName.TabIndex = 36;
			this.btnExit.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnExit.Location = new global::System.Drawing.Point(368, 356);
			this.btnExit.Name = "btnExit";
			this.btnExit.Size = new global::System.Drawing.Size(75, 23);
			this.btnExit.TabIndex = 7;
			this.btnExit.Text = "Exit";
			this.btnExit.UseVisualStyleBackColor = true;
			this.btnExit.Click += new global::System.EventHandler(this.btnExit_Click);
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(455, 391);
			base.ControlBox = false;
			base.Controls.Add(this.btnExit);
			base.Controls.Add(this.tableLayoutPanel1);
			base.Controls.Add(this.btnSave);
			base.Icon = (global::System.Drawing.Icon)componentResourceManager.GetObject("$this.Icon");
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "nopTalkDB";
			this.Text = "NopTalk database configuration";
			base.Load += new global::System.EventHandler(this.nopTalkDB_Load);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			base.ResumeLayout(false);
		}

		// Token: 0x040000B8 RID: 184
		private global::System.ComponentModel.IContainer components;

		// Token: 0x040000B9 RID: 185
		private global::System.Windows.Forms.Button btnSave;

		// Token: 0x040000BA RID: 186
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

		// Token: 0x040000BB RID: 187
		private global::System.Windows.Forms.Label label1;

		// Token: 0x040000BC RID: 188
		private global::System.Windows.Forms.Label label2;

		// Token: 0x040000BD RID: 189
		private global::System.Windows.Forms.Label lblLogin;

		// Token: 0x040000BE RID: 190
		private global::System.Windows.Forms.ComboBox ddAuthType;

		// Token: 0x040000BF RID: 191
		private global::System.Windows.Forms.TextBox tbServerName;

		// Token: 0x040000C0 RID: 192
		private global::System.Windows.Forms.Label label3;

		// Token: 0x040000C1 RID: 193
		private global::System.Windows.Forms.TextBox tbUserName;

		// Token: 0x040000C2 RID: 194
		private global::System.Windows.Forms.TextBox tbPassword;

		// Token: 0x040000C3 RID: 195
		private global::System.Windows.Forms.Label label4;

		// Token: 0x040000C4 RID: 196
		private global::System.Windows.Forms.TextBox tbDatabaseName;

		// Token: 0x040000C5 RID: 197
		private global::System.Windows.Forms.Button btnExit;
	}
}
