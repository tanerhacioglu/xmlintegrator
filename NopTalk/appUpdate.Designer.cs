﻿namespace NopTalk
{
	// Token: 0x0200000B RID: 11
	public partial class appUpdate : global::System.Windows.Forms.Form
	{
		// Token: 0x06000040 RID: 64 RVA: 0x00002500 File Offset: 0x00000700
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000041 RID: 65 RVA: 0x00006B28 File Offset: 0x00004D28
		private void InitializeComponent()
		{
			this.downloadStatus = new global::System.Windows.Forms.TextBox();
			base.SuspendLayout();
			this.downloadStatus.Location = new global::System.Drawing.Point(24, 100);
			this.downloadStatus.Name = "downloadStatus";
			this.downloadStatus.Size = new global::System.Drawing.Size(243, 20);
			this.downloadStatus.TabIndex = 0;
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(292, 273);
			base.Controls.Add(this.downloadStatus);
			base.Name = "appUpdate";
			this.Text = "appUpdate";
			base.Load += new global::System.EventHandler(this.appUpdate_Load);
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		// Token: 0x0400001A RID: 26
		private global::System.ComponentModel.IContainer components;

		// Token: 0x0400001B RID: 27
		private global::System.Windows.Forms.TextBox downloadStatus;
	}
}
