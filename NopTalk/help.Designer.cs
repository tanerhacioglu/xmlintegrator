﻿namespace NopTalk
{
	// Token: 0x02000018 RID: 24
	public partial class help : global::System.Windows.Forms.Form
	{
		// Token: 0x060000A0 RID: 160 RVA: 0x000027DD File Offset: 0x000009DD
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x060000A1 RID: 161 RVA: 0x0000D4E0 File Offset: 0x0000B6E0
		private void InitializeComponent()
		{
			base.SuspendLayout();
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(8f, 16f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(282, 253);
			base.Name = "help";
			this.Text = "help";
			base.Load += new global::System.EventHandler(this.help_Load);
			base.Shown += new global::System.EventHandler(this.help_Shown);
			base.SizeChanged += new global::System.EventHandler(this.help_SizeChanged);
			base.ResumeLayout(false);
		}

		// Token: 0x040000A0 RID: 160
		private global::System.ComponentModel.IContainer components;
	}
}
