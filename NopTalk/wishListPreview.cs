﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using NopTalkCore;

namespace NopTalk
{
	public partial class wishListPreview : Form
	{
		public wishListPreview(SourceDocument sourceDocument, SourceMapping sourceMapping, TaskMapping taskMapping)
		{
			this.itemIndex = 0;
			this.components = null;		
			this._sourceDocument = sourceDocument;
			this._sourceMapping = sourceMapping;
			this._taskMapping = taskMapping;
			this.InitializeComponent();
		}

		private void wishListPreview_Load(object sender, EventArgs e)
		{
			this.lblFoundItems.Text = this.lblFoundItems.Text + " " + this._sourceDocument.SourceItems.Count.ToString();
			this.ShowItem();
			base.WindowState = FormWindowState.Maximized;
		}

		private void ShowItem()
		{
			if (this._sourceDocument.SourceItems.Count > 0)
			{
				SourceItem sourceItem = this._sourceDocument.SourceItems[this.itemIndex];
				this.dgWishList.DataSource = null;
				if (sourceItem.WishList != null && sourceItem.WishList.Any<SourceItem.WishListItem>())
				{
					this.dgWishList.AutoGenerateColumns = true;
					SourceItem.CustomerItem user = sourceItem.User;
					List<wishListPreview.DataForView> list = new List<wishListPreview.DataForView>();
					foreach (SourceItem.WishListItem wishListItem in sourceItem.WishList)
					{
						List<wishListPreview.DataForView> list2 = list;
						wishListPreview.DataForView dataForView = new wishListPreview.DataForView();
						SourceItemMapping sourceItemMapping = this._sourceMapping.SourceWishListMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 3001);
						dataForView.FieldName = ((sourceItemMapping != null) ? sourceItemMapping.FieldName : null);
						dataForView.FieldValue = wishListItem.ProductSKU;
						list2.Add(dataForView);
						List<wishListPreview.DataForView> list3 = list;
						wishListPreview.DataForView dataForView2 = new wishListPreview.DataForView();
						SourceItemMapping sourceItemMapping2 = this._sourceMapping.SourceWishListMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 3003);
						dataForView2.FieldName = ((sourceItemMapping2 != null) ? sourceItemMapping2.FieldName : null);
						dataForView2.FieldValue = wishListItem.CustomerEmail;
						list3.Add(dataForView2);
						List<wishListPreview.DataForView> list4 = list;
						wishListPreview.DataForView dataForView3 = new wishListPreview.DataForView();
						SourceItemMapping sourceItemMapping3 = this._sourceMapping.SourceWishListMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 3002);
						dataForView3.FieldName = ((sourceItemMapping3 != null) ? sourceItemMapping3.FieldName : null);
						dataForView3.FieldValue = wishListItem.CustomerUsername;
						list4.Add(dataForView3);
						List<wishListPreview.DataForView> list5 = list;
						wishListPreview.DataForView dataForView4 = new wishListPreview.DataForView();
						SourceItemMapping sourceItemMapping4 = this._sourceMapping.SourceWishListMapping.Items.FirstOrDefault((SourceItemMapping x) => x.MappingItemType == 3004);
						dataForView4.FieldName = ((sourceItemMapping4 != null) ? sourceItemMapping4.FieldName : null);
						dataForView4.FieldValue = wishListItem.CustomSKUID;
						list5.Add(dataForView4);
					}
					this.dgWishList.DataSource = list.ToList<wishListPreview.DataForView>();
					this.dgWishList.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
					this.dgWishList.ReadOnly = true;
				}
			}
			this.lblShow.Text = "Show item: " + (this.itemIndex + 1).ToString() + "/" + this._sourceDocument.SourceItems.Count.ToString();
			if (this.itemIndex <= 0)
			{
				this.btnPrev.Enabled = false;
			}
			else
			{
				this.btnPrev.Enabled = true;
			}
			if (this.itemIndex == this._sourceDocument.SourceItems.Count - 1 || this._sourceDocument.SourceItems.Count == 0)
			{
				this.btnNext.Enabled = false;
			}
			else
			{
				this.btnNext.Enabled = true;
			}
		}

		private void btnNext_Click(object sender, EventArgs e)
		{
			if (this.itemIndex < this._sourceDocument.SourceItems.Count - 1)
			{
				this.itemIndex++;
				this.ShowItem();
			}
		}

		private void btnPrev_Click(object sender, EventArgs e)
		{
			if (this.itemIndex > 0)
			{
				this.itemIndex--;
				this.ShowItem();
			}
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams createParams = base.CreateParams;
				createParams.ExStyle |= 33554432;
				return createParams;
			}
		}

		private SourceDocument _sourceDocument;

		private SourceMapping _sourceMapping;

		private TaskMapping _taskMapping;

		private int itemIndex;

		public class DataForView
		{
			public string FieldName { get; set; }

			public string FieldValue { get; set; }

			
		}
	}
}
