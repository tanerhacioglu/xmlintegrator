﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using NopTalkCore;
using YARTE.UI;
using YARTE.UI.Buttons;

namespace NopTalk
{
	// Token: 0x02000021 RID: 33
	public partial class productCard : Form
	{
		// Token: 0x060000CE RID: 206 RVA: 0x0000F32C File Offset: 0x0000D52C
		public productCard(SourceDocument sourceDocument, SourceMapping sourceMapping, TaskMapping taskMapping)
		{
			
			this.formLoaded = false;
			this.itemIndex = 0;
			this.components = null;
			
			this._sourceDocument = sourceDocument;
			this._sourceMapping = sourceMapping;
			this._taskMapping = taskMapping;
			this.InitializeComponent();
			PredefinedButtonSets.SetupDefaultButtons(this.tbFullDescRichEditor);
		}

		// Token: 0x060000CF RID: 207 RVA: 0x0000F380 File Offset: 0x0000D580
		private async void productCard_Load(object sender, EventArgs e)
		{
			this.InitialData();
			
			this.lblFoundItems.Text = this.lblFoundItems.Text + " " + this._sourceDocument.Total.ToString();
			Cursor.Current = Cursors.WaitCursor;
			this.LodaProductData();
			Cursor.Current = Cursors.Arrow;
			this.tbFullDescRichEditor.AutoScroll = true;
			this.tbFullDescRichEditor.HorizontalScroll.Enabled = true;
			this.tbFullDescRichEditor.HorizontalScroll.Enabled = true;
			this.formLoaded = true;
		}

		// Token: 0x060000D0 RID: 208 RVA: 0x0000F3CC File Offset: 0x0000D5CC
		private void InitialData()
		{
			DataSettingsManager dataSettingsManager = new DataSettingsManager();
			string dataConnectionString = dataSettingsManager.LoadSettings(null).DataConnectionString;
			helper.FillDropDownList("select Id, Name From EShop", this.ddStoresConn, dataConnectionString);
			this.LoadManageInventoryMethod();
			this.LoadbackOrderModes();
			this.chProductPublish.Checked = true;
			this.chProductPublishImport.Checked = true;
			this.ddStoresConn.SelectedIndex = -1;
		}

		// Token: 0x060000D1 RID: 209 RVA: 0x0000F430 File Offset: 0x0000D630
		private void LoadManageInventoryMethod()
		{
			var dataSource = (from ManageInventoryMethod mode in Enum.GetValues(typeof(ManageInventoryMethod))
			select new
			{
				Value = (int)mode,
				Title = mode
			}).ToList();
			this.ddInventoryMethod.ValueMember = "Value";
			this.ddInventoryMethod.DisplayMember = "Title";
			this.ddInventoryMethod.DataSource = dataSource;
			this.ddInventoryMethod.SelectedIndex = 0;
		}

		// Token: 0x060000D2 RID: 210 RVA: 0x0000F4B4 File Offset: 0x0000D6B4
		private void LoadbackOrderModes()
		{
			var dataSource = (from BackorderMode mode in Enum.GetValues(typeof(BackorderMode))
			select new
			{
				Value = (int)mode,
				Title = mode
			}).ToList();
			this.ddlBackorderMode.ValueMember = "Value";
			this.ddlBackorderMode.DisplayMember = "Title";
			this.ddlBackorderMode.DataSource = dataSource;
			this.ddlBackorderMode.SelectedIndex = 0;
		}

		// Token: 0x060000D3 RID: 211 RVA: 0x0000F538 File Offset: 0x0000D738
		private void ddStoresConn_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.ddStoresConn.SelectedIndex > -1)
			{
				this.LoadStoreData("SELECT Id, Name FROM Category ORDER BY Name ", null, this.clbCategoriesList, null, false);
				this.LoadStoreData("select 0 as Id, 'NoDeliveryDate' as Name UNION select Id, Name From DeliveryDate", this.ddDeliveryDate, null, null, false);
				this.btnImportToStore.Enabled = true;
			}
			else
			{
				this.btnImportToStore.Enabled = false;
			}
		}

		// Token: 0x060000D4 RID: 212 RVA: 0x0000F598 File Offset: 0x0000D798
		private void LoadStoreData(string sqlQuery, ComboBox control, CheckedListBox control2, DataGridView control3, bool loadEditorDefaultColumns = false)
		{
			if (this.ddStoresConn.SelectedIndex > -1 && GlobalClass.IsInteger(this.ddStoresConn.SelectedValue) && this.formLoaded)
			{
				try
				{
					StoreData storeData = StoreFunc.GetStoreData(Convert.ToInt64(this.ddStoresConn.SelectedValue));
					string databaseConnString = storeData.DatabaseConnString;
					if (control != null)
					{
						helper.FillDropDownList2(sqlQuery, control, databaseConnString, storeData.DatabaseType);
					}
					if (control2 != null)
					{
						helper.FillCheckBoxList2(sqlQuery, control2, databaseConnString, storeData.DatabaseType);
					}
					return;
				}
				catch (Exception ex)
				{
					MessageBox.Show(this, "Need provide the correct connection string to the nopCommerce store. Go to->settings->stores. Error: " + ex.Message, "Error");
					return;
				}
			}
			if (control != null)
			{
				control.DataSource = null;
			}
			if (control2 != null)
			{
				control2.DataSource = null;
			}
		}

		// Token: 0x060000D5 RID: 213 RVA: 0x0000F664 File Offset: 0x0000D864
		private void LodaProductData()
		{
			SourceItem sourceItem = this._sourceDocument.LoadByOne ? this._sourceDocument.GetItemByIndex(FileFormat.AliExpress, this.itemIndex) : this._sourceDocument.SourceItems[this.itemIndex];
			this._sourceItem = sourceItem;
			this.tbProductTitle.Text = sourceItem.ProdName;
			this.tbProdShortDesc.Text = sourceItem.ProdShortDesc;
			this.tbFullDescRichEditor.Html = sourceItem.ProdFullDesc;
			this.tbFullDescTextEditor.Text = sourceItem.ProdFullDesc;
			this.nmProdPrice.Value = sourceItem.ProdPrice.GetValueOrDefault();
			this.nmProdQuantity.Value = sourceItem.ProdStock.GetValueOrDefault();
			this.picMain.ImageLocation = null;
			SourceItem.ImageItem imageItem = sourceItem.Images.FirstOrDefault<SourceItem.ImageItem>();
			if (imageItem != null)
			{
				this.picMain.SizeMode = PictureBoxSizeMode.StretchImage;
				this.picMain.ClientSize = new Size(220, 220);
				try
				{
					this.picMain.Image = sourceItem.GetImage(imageItem);
				}
				catch
				{
				}
			}
			this.picMain.Refresh();
			this.LoadProductImages(sourceItem);
			this.LoadProductVariants(sourceItem);
			this.lblShow.Text = "Show item: " + (this.itemIndex + 1).ToString() + "/" + this._sourceDocument.Total.ToString();
			if (this.itemIndex <= 0)
			{
				this.btnPrev.Enabled = false;
			}
			else
			{
				this.btnPrev.Enabled = true;
			}
			if (this.itemIndex == this._sourceDocument.Total - 1 || this._sourceDocument.Total == 0)
			{
				this.btnNext.Enabled = false;
			}
			else
			{
				this.btnNext.Enabled = true;
			}
		}

		// Token: 0x060000D6 RID: 214 RVA: 0x0000F854 File Offset: 0x0000DA54
		private void LoadProductImages(SourceItem sourceItem)
		{
			ImageList imageList = new ImageList();
			ImageList imageList2 = new ImageList();
			this.imageListViewAssigned.Items.Clear();
			this.imageListViewUnassigned.Items.Clear();
			int num = 0;
			int num2 = 0;
			foreach (SourceItem.ImageItem imageItem in sourceItem.Images)
			{
				if (imageItem.IsSelected)
				{
					imageList.Images.Add(sourceItem.GetImage(imageItem));
					ListViewItem listViewItem = new ListViewItem();
					listViewItem.ImageIndex = num;
					listViewItem.Tag = imageItem.ImageId;
					this.imageListViewAssigned.Items.Add(listViewItem);
					num++;
				}
				else
				{
					imageList2.Images.Add(sourceItem.GetImage(imageItem));
					ListViewItem listViewItem2 = new ListViewItem();
					listViewItem2.ImageIndex = num2;
					listViewItem2.Tag = imageItem.ImageId;
					this.imageListViewUnassigned.Items.Add(listViewItem2);
					num2++;
				}
			}
			imageList.ImageSize = new Size(150, 150);
			imageList2.ImageSize = new Size(150, 150);
			this.imageListViewAssigned.View = View.LargeIcon;
			this.imageListViewUnassigned.View = View.LargeIcon;
			this.imageListViewAssigned.LargeImageList = imageList;
			this.imageListViewUnassigned.LargeImageList = imageList2;
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x0000F9D8 File Offset: 0x0000DBD8
		public void LoadProductVariants(SourceItem sourceItem)
		{
			this.dgProdVariants.AutoGenerateColumns = true;
			this.dgProdVariants.RowTemplate.Height = 50;
			this.dgProdVariants.DataSource = this.VariantsDatasource(sourceItem.Attributes, sourceItem.Variants);
			this.dgProdVariants.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
			this.dgProdVariants.ReadOnly = false;
			this.dgProdVariants.ShowEditingIcon = true;
			this.dgProdVariants.Refresh();
			this.dgProdVariants.ClearSelection();
		}

		// Token: 0x060000D8 RID: 216 RVA: 0x0000FA5C File Offset: 0x0000DC5C
		private void AddImageColumn()
		{
			if (this.dgProdVariants.Columns["Image"] == null)
			{
				DataGridViewImageColumn dataGridViewImageColumn = new DataGridViewImageColumn();
				dataGridViewImageColumn.Name = "Image";
				dataGridViewImageColumn.Width = 50;
				dataGridViewImageColumn.Image = null;
				dataGridViewImageColumn.ImageLayout = DataGridViewImageCellLayout.Zoom;
				dataGridViewImageColumn.DisplayIndex = 2;
				this.dgProdVariants.Columns.Add(dataGridViewImageColumn);
			}
		}

		// Token: 0x060000D9 RID: 217 RVA: 0x0000FAC4 File Offset: 0x0000DCC4
		private DataTable VariantsDatasource(List<SourceItem.AttributeValues> attrItems, List<SourceItem.VariantItem> variantItems)
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("ItemSelect", typeof(bool));
			dataTable.Columns.Add("Sku");
			foreach (SourceItem.AttributeValues attributeValues in attrItems)
			{
				if (!dataTable.Columns.Contains(attributeValues.AttInfo_Name))
				{
					dataTable.Columns.Add(attributeValues.AttInfo_Name);
				}
			}
			dataTable.Columns.Add("Price", typeof(decimal));
			dataTable.Columns.Add("Inventory", typeof(int));
			foreach (SourceItem.VariantItem variantItem in variantItems)
			{
				DataRow dataRow = dataTable.NewRow();
				dataRow["ItemSelect"] = variantItem.IsSelected;
				dataRow["Sku"] = variantItem.Sku;
				foreach (SourceItem.AttributeValues attributeValues2 in variantItem.VariantItems)
				{
					dataRow[attributeValues2.AttInfo_Name] = attributeValues2.AttValue_Value;
				}
				dataRow["Price"] = variantItem.OverriddenPrice;
				dataRow["Inventory"] = variantItem.StockQuantity;
				dataTable.Rows.Add(dataRow);
			}
			return dataTable;
		}

		// Token: 0x060000DA RID: 218 RVA: 0x0000FCA4 File Offset: 0x0000DEA4
		private static Stream GenerateStreamFromString(string s)
		{
			MemoryStream memoryStream = new MemoryStream();
			StreamWriter streamWriter = new StreamWriter(memoryStream);
			streamWriter.Write(s);
			streamWriter.Flush();
			memoryStream.Position = 0L;
			return memoryStream;
		}

		// Token: 0x060000DB RID: 219 RVA: 0x000024A0 File Offset: 0x000006A0
		private void tabControl2_Selecting(object sender, TabControlCancelEventArgs e)
		{
		}

		// Token: 0x060000DC RID: 220 RVA: 0x0000FCE0 File Offset: 0x0000DEE0
		private void tabControl2_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.tabControl2.SelectedIndex == 0)
			{
				this.tbFullDescRichEditor.Html = this.tbFullDescTextEditor.Text;
			}
			if (this.tabControl2.SelectedIndex == 1)
			{
				this.tbFullDescTextEditor.Text = this.GetFullDescription();
			}
		}

		// Token: 0x060000DD RID: 221 RVA: 0x0000FD34 File Offset: 0x0000DF34
		private string GetFullDescription()
		{
			string text = this.tbFullDescRichEditor.Html;
			string text2 = "<BODY>";
			string value = "</BODY>";
			int num = text.IndexOf(text2) + text2.Length;
			int num2 = text.IndexOf(value, num);
			text = text.Substring(num, num2 - num);
			return Regex.Replace(text, "\\r\\n?|\\n|\\t", string.Empty);
		}

		// Token: 0x060000DE RID: 222 RVA: 0x0000FD94 File Offset: 0x0000DF94
		private async void dgProdVariants_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			try
			{
				this.AddImageColumn();
			}
			catch
			{
			}
			await Task.Factory.StartNew(delegate()
			{
				this.BindItemPictures();
			});
		}

		// Token: 0x060000DF RID: 223 RVA: 0x0000FDE0 File Offset: 0x0000DFE0
		private void BindItemPictures()
		{
			if (this.dgProdVariants.Columns.Contains("Image"))
			{
				this.dgProdVariants.Columns["Image"].DisplayIndex = 2;
				for (int i = 0; i < this.dgProdVariants.Rows.Count; i++)
				{
					try
					{
						byte[] array = null;
						string itemId = this.dgProdVariants["Sku", i].Value.ToString();
						SourceItem sourceItem = this._sourceItem;
						string text;
						if (sourceItem == null)
						{
							text = null;
						}
						else
						{
							SourceItem.VariantItem variantItem = sourceItem.Variants.FirstOrDefault((SourceItem.VariantItem x) => x.Sku == itemId);
							if (variantItem == null)
							{
								text = null;
							}
							else
							{
								text = variantItem.VariantItems.FirstOrDefault((SourceItem.AttributeValues x) => x.AttValue_AssociatedPicture != null).AttValue_AssociatedPicture;
							}
						}
						string imageUrl = text;
						Image value = this._sourceItem.GetImageFromUrl(imageUrl);
						if (array != null)
						{
							value = (Bitmap)new ImageConverter().ConvertFrom(array);
						}
						this.dgProdVariants.Rows[i].Cells["Image"].Value = value;
					}
					catch
					{
					}
				}
			}
		}

		// Token: 0x060000E0 RID: 224 RVA: 0x0000FF2C File Offset: 0x0000E12C
		private void btnImageAssign_Click(object sender, EventArgs e)
		{
			foreach (object obj in this.imageListViewUnassigned.SelectedItems)
			{
				ListViewItem listViewItem = (ListViewItem)obj;
				int imageId = Convert.ToInt32(listViewItem.Tag.ToString());
				(from x in this._sourceItem.Images
				where x.ImageId == imageId
				select x).ToList<SourceItem.ImageItem>().ForEach(delegate(SourceItem.ImageItem x)
				{
					x.IsSelected = true;
				});
			}
			this.LoadProductImages(this._sourceItem);
		}

		// Token: 0x060000E1 RID: 225 RVA: 0x0000FFF4 File Offset: 0x0000E1F4
		private void btnImageUnassign_Click(object sender, EventArgs e)
		{
			foreach (object obj in this.imageListViewAssigned.SelectedItems)
			{
				ListViewItem listViewItem = (ListViewItem)obj;
				int imageId = Convert.ToInt32(listViewItem.Tag.ToString());
				(from x in this._sourceItem.Images
				where x.ImageId == imageId
				select x).ToList<SourceItem.ImageItem>().ForEach(delegate(SourceItem.ImageItem x)
				{
					x.IsSelected = false;
				});
			}
			this.LoadProductImages(this._sourceItem);
		}

		// Token: 0x060000E2 RID: 226 RVA: 0x000100BC File Offset: 0x0000E2BC
		private void btnNext_Click(object sender, EventArgs e)
		{
			if (this.itemIndex < this._sourceDocument.Total - 1)
			{
				Cursor.Current = Cursors.WaitCursor;
				this.SetStatusArea(false, "", true);
				this.GetDataFromForm();
				this.itemIndex++;
				this.LodaProductData();
				Cursor.Current = Cursors.Arrow;
			}
		}

		// Token: 0x060000E3 RID: 227 RVA: 0x0001011C File Offset: 0x0000E31C
		private void btnPrev_Click(object sender, EventArgs e)
		{
			if (this.itemIndex > 0)
			{
				Cursor.Current = Cursors.WaitCursor;
				this.SetStatusArea(false, "", true);
				this.GetDataFromForm();
				this.itemIndex--;
				this.LodaProductData();
				Cursor.Current = Cursors.Arrow;
			}
		}

		// Token: 0x060000E4 RID: 228 RVA: 0x000024A2 File Offset: 0x000006A2
		private void btnClose_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		// Token: 0x060000E5 RID: 229 RVA: 0x00010170 File Offset: 0x0000E370
		private void button1_Click(object sender, EventArgs e)
		{
			try
			{
				StoreData storeData = StoreFunc.GetStoreData(Convert.ToInt64(this.ddStoresConn.SelectedValue));
				ImportManager importManager = new ImportManager();
				if (this._taskMapping == null)
				{
					this._taskMapping = new TaskMapping();
					this._taskMapping.ProdPriceAddPercentVal = new int?(0);
					this._taskMapping.ProdPriceAddUnitVal = new decimal?(0m);
					this._taskMapping.TaskAction = 2;
					this._taskMapping.SavePicturesInFile = false;
					this._taskMapping.ProdNameAction = true;
					this._taskMapping.ProdShortDescAction = true;
					this._taskMapping.ProdFullDescAction = true;
					this._taskMapping.ProdPriceAction = true;
					this._taskMapping.ProdStockAction = true;
					this._taskMapping.CatAction = true;
				}
				this._taskMapping.ProdSettingsAllowedQtyVal = this.tbProductAllowQty.Text;
				this._taskMapping.ProdSettingsAllowedQtyAction = this.chProductAllowQtyAction.Checked;
				this._taskMapping.ProdSettingsMinCartQtyVal = this.tbProductMinCartQty.Text;
				this._taskMapping.ProdSettingsMinCartQtyAction = this.chProductMinCartQtyAction.Checked;
				this._taskMapping.ProdSettingsMaxCartQtyVal = this.tbProductMaxCartQty.Text;
				this._taskMapping.ProdSettingsMaxCartQtyAction = this.chProductMaxCartQtyAction.Checked;
				this._taskMapping.ProdSettingsMinStockQtyVal = this.tbProductMinStockQty.Text;
				this._taskMapping.ProdSettingsMinStockQtyAction = this.chProductMinStockQtyAction.Checked;
				this._taskMapping.ProdSettingsNotifyQtyVal = this.tbProductNotifyQty.Text;
				this._taskMapping.ProdSettingsNotifyQtyAction = this.chProductNotifyQtyAction.Checked;
				this._taskMapping.ProdSettingsShippingEnabledVal = this.chProductShippingEnabled.Checked;
				this._taskMapping.ProdSettingsShippingEnabledAction = this.chProductShippingEnabledImport.Checked;
				this._taskMapping.ProdSettingsFreeShippingVal = this.chProductFreeShipping.Checked;
				this._taskMapping.ProdSettingsFreeShippingAction = this.chProductFreeShippingImport.Checked;
				this._taskMapping.ProdSettingsShipSeparatelyVal = this.chProductShipSeparately.Checked;
				this._taskMapping.ProdSettingsShipSeparatelyAction = this.chProductShipSeparatelyImport.Checked;
				this._taskMapping.ProdSettingsShippingChargeVal = new decimal?(this.nmProductShippingCharge.Value);
				this._taskMapping.ProdSettingsShippingChargeAction = this.chProductShippingChargeImport.Checked;
				this._taskMapping.ProdSettingsDeliveryDateAction = this.chDayDeliveryImport.Checked;
				this._taskMapping.ProductPublishAction = this.chProductPublishImport.Checked;
				if (this.chProductPublishImport.Checked)
				{
					this._taskMapping.ProductPublish = new bool?(this.chProductPublish.Checked);
				}
				if (this.chDayDeliveryImport.Checked)
				{
					this._taskMapping.DeliveryDayId = new int?(GlobalClass.StringToInteger(this.ddDeliveryDate.SelectedValue, 0));
				}
				this._taskMapping.ManageInventoryMethodAction = this.chInventoryMethodImport.Checked;
				if (this.chInventoryMethodImport.Checked)
				{
					this._taskMapping.ManageInventoryMethodId = new int?(GlobalClass.StringToInteger(this.ddInventoryMethod.SelectedValue, 0));
				}
				this._taskMapping.BackorderModeAction = this.chBackorderModeImport.Checked;
				if (this.chBackorderModeImport.Checked)
				{
					this._taskMapping.BackorderModeId = new int?(GlobalClass.StringToInteger(this.ddlBackorderMode.SelectedValue, 0));
				}
				if (this.chProductReviewEnableImport.Checked)
				{
					this._taskMapping.AllowCustomerReviews = new bool?(this.chProductReviewEnable.Checked);
				}
				if (this.chProductDisplayAvailabilityImport.Checked)
				{
					this._taskMapping.DisplayAvailability = new bool?(this.chProductDisplayAvailability.Checked);
				}
				if (this.chProductDisplayStockQuantityImport.Checked)
				{
					this._taskMapping.DisplayStockQuantity = new bool?(this.chProductDisplayStockQuantity.Checked);
				}
				this._sourceMapping.CatIsId = true;
				this.GetDataFromForm();
				importManager.ImportItem(this._sourceItem, storeData, this._taskMapping, this._sourceMapping);
				this.SetStatusArea(true, "Product imported successfully.", false);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
				this.SetStatusArea(false, "Product not imported.", false);
			}
		}

		// Token: 0x060000E6 RID: 230 RVA: 0x000105C4 File Offset: 0x0000E7C4
		private void GetDataFromForm()
		{
			this._sourceItem.ProdName = this.tbProductTitle.Text;
			this._sourceItem.ProdShortDesc = this.tbProdShortDesc.Text;
			this._sourceItem.ProdFullDesc = this.GetFullDescription();
			this._sourceItem.ProdPrice = new decimal?(this.nmProdPrice.Value);
			this._sourceItem.ProdStock = new int?(Convert.ToInt32(this.nmProdQuantity.Value));
			this._sourceItem.Categories = new List<SourceItem.CategoryItem>();
			new List<string>();
			for (int i = 0; i <= this.clbCategoriesList.CheckedItems.Count - 1; i++)
			{
				string text = (this.clbCategoriesList.CheckedItems[i] as DataRowView)[0].ToString();
				if (!string.IsNullOrEmpty(text) && text.ToLower() != "id")
				{
					this._sourceItem.Categories.Add(new SourceItem.CategoryItem
					{
						CatId = 0,
						Category = text
					});
				}
			}
			foreach (object obj in ((IEnumerable)this.dgProdVariants.Rows))
			{
				DataGridViewRow dataGridViewRow = (DataGridViewRow)obj;
				string sku = GlobalClass.ObjectToString(dataGridViewRow.Cells["Sku"].Value);
				bool isSelected = GlobalClass.ObjectToBool(dataGridViewRow.Cells["ItemSelect"].Value);
				decimal value = GlobalClass.StringToDecimal(dataGridViewRow.Cells["Price"].Value, 0);
				int value2 = GlobalClass.StringToInteger(dataGridViewRow.Cells["Inventory"].Value, 0);
				if (this._sourceItem.Variants.FirstOrDefault((SourceItem.VariantItem x) => x.Sku == sku) != null)
				{
					this._sourceItem.Variants.FirstOrDefault((SourceItem.VariantItem x) => x.Sku == sku).IsSelected = isSelected;
					this._sourceItem.Variants.FirstOrDefault((SourceItem.VariantItem x) => x.Sku == sku).OverriddenPrice = new decimal?(value);
					this._sourceItem.Variants.FirstOrDefault((SourceItem.VariantItem x) => x.Sku == sku).StockQuantity = new int?(value2);
				}
			}
			this._sourceDocument.SaveItemByIndex(this.itemIndex, this._sourceItem);
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x00010870 File Offset: 0x0000EA70
		private void SetStatusArea(bool valid = false, string message = null, bool info = false)
		{
			if (string.IsNullOrEmpty(message))
			{
				this.statusArea.Hide();
			}
			else
			{
				this.statusArea.Show();
				this.statusAreaText.Text = message;
				if (valid)
				{
					this.statusArea.BackColor = ColorTranslator.FromHtml("#009D00");
				}
				else
				{
					this.statusArea.BackColor = ColorTranslator.FromHtml("#C00000");
				}
				if (info)
				{
					this.statusArea.BackColor = ColorTranslator.FromHtml("#C07464");
				}
				this.statusAreaText.ForeColor = Color.White;
			}
		}

		// Token: 0x040000C6 RID: 198
		private bool formLoaded;

		// Token: 0x040000C7 RID: 199
		private SourceDocument _sourceDocument;

		// Token: 0x040000C8 RID: 200
		private SourceMapping _sourceMapping;

		// Token: 0x040000C9 RID: 201
		private TaskMapping _taskMapping;

		// Token: 0x040000CA RID: 202
		private int itemIndex;

		// Token: 0x040000CB RID: 203
		private SourceItem _sourceItem;
	}
}
