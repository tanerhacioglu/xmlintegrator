﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Security.AccessControl;
using System.Windows.Forms;
using NopTalk.Properties;
using NopTalkCore;

namespace NopTalk
{
	public partial class mainForm : Form
	{
		public mainForm()
		{
			
			this.taskManager = new TaskManager();
			this.components = null;
			this.InitializeComponent();
			if (ImportManager.productsLimits == 50)
			{
				this.Text += " Starter";
			}
			if (ImportManager.productsLimits == 1000)
			{
				this.Text += " Standard";
			}
			if (ImportManager.productsLimits == 5000)
			{
				this.Text += " Professional";
			}
			if (ImportManager.productsLimits == 0)
			{
				this.Text += " Enterprise";
			}
			base.WindowState = FormWindowState.Maximized;
			this.OpenNewChildForm(new help());
		}

		private void eShopsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.CloseAllOpenForms();
			this.OpenNewChildForm(new frmeShopsList());
		}

		private void vendorsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.CloseAllOpenForms();
			this.OpenNewChildForm(new frmVendorsList());
		}

		private void OpenNewChildForm(Form form_0)
		{
			form_0.MdiParent = this;
			form_0.WindowState = FormWindowState.Maximized;
			form_0.Icon = Resources.nop_example3;
			form_0.Show();
		}

		private void OpenNewForm(Form form_0)
		{
			form_0.WindowState = FormWindowState.Maximized;
			form_0.Icon = Resources.nop_example3;
			form_0.Show();
		}

		private void CloseAllOpenForms()
		{
			for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
			{
				if (Application.OpenForms[i].Name != "mainForm")
				{
					Application.OpenForms[i].Close();
				}
			}
		}

		private void tasksToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.CloseAllOpenForms();
			this.OpenNewChildForm(new frmTasksList());
		}

		private void aboutNopTalkToolStripMenuItem_Click(object sender, EventArgs e)
		{
			new AboutBox1
			{
				StartPosition = FormStartPosition.CenterParent
			}.ShowDialog();
		}

		private void logsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.CloseAllOpenForms();
			this.OpenNewChildForm(new logList());
		}

		private async void mainForm_Load(object sender, EventArgs e)
		{
			
			this.taskManager.StartAllTasks(null, false, 0, null);
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		private void nopTalkHomeToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ProcessStartInfo startInfo = new ProcessStartInfo("http:/artiglobal.net/");
			Process.Start(startInfo);
		}

		private void nopTalkProjectPageToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ProcessStartInfo startInfo = new ProcessStartInfo("http://www.artiglobal.net/");
			Process.Start(startInfo);
		}

		private void mainForm_Shown(object sender, EventArgs e)
		{
			
			DatabaseFunc.DatabaseInitial();
			Cursor.Current = Cursors.Default;
		}

		private async void mainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			
			TaskManager taskManager = new TaskManager();
			DialogResult result;
			if (taskManager.TasksListCount > 0)
			{
				result = MessageBox.Show("There are some running Tasks in this application context. Do you really want to exit from NopTalk Pro?", "Are you sure?", MessageBoxButtons.YesNo);
			}
			else if (TaskManager.IsScheduled)
			{
				result = MessageBox.Show("Some Tasks are scheduled to run. Do you really want to exit from NopTalk Pro?", "Are you sure?", MessageBoxButtons.YesNo);
			}
			else
			{
				result = MessageBox.Show("Do you really want to exit from NopTalk Pro?", "Are you sure?", MessageBoxButtons.YesNo);
			}
			if (result == DialogResult.No)
			{
				e.Cancel = true;
			}
		}

		private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
		}

		public static void AddFileSecurity(string fileName, string account, FileSystemRights rights, AccessControlType controlType)
		{
			FileSecurity accessControl = File.GetAccessControl(fileName);
			accessControl.AddAccessRule(new FileSystemAccessRule(account, rights, controlType));
			File.SetAccessControl(fileName, accessControl);
		}

		private void rbSystemTray_Click(object sender, EventArgs e)
		{
			new SystemTrayNotify(this)
			{
				StartPosition = FormStartPosition.CenterParent
			}.ShowDialog();
		}

		private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
		{
			base.Show();
			base.WindowState = FormWindowState.Normal;
			base.ShowInTaskbar = true;
			this.notifyIcon1.Visible = false;
		}

		private void rbExit_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		public void GotoSystemTray()
		{
			base.Hide();
			this.notifyIcon1.Visible = true;
		}

		private void rbLogs_Click(object sender, EventArgs e)
		{
			this.CloseAllOpenForms();
			this.OpenNewChildForm(new logList());
		}

		private void rbTasks_Click(object sender, EventArgs e)
		{
			this.CloseAllOpenForms();
			this.OpenNewChildForm(new frmTasksList());
		}

		private void rbVendors_Click(object sender, EventArgs e)
		{
			this.CloseAllOpenForms();
			this.OpenNewChildForm(new frmVendorsList());
		}

		private void rbStores_Click(object sender, EventArgs e)
		{
			this.CloseAllOpenForms();
			this.OpenNewChildForm(new frmeShopsList());
		}

		private void updateNopTalkToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.OpenNewChildForm(new appUpdate());
		}

		private void mainForm_SizeChanged(object sender, EventArgs e)
		{
		}

		private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			this.OpenNewChildForm(new help());
		}

		private void rbEditor_Click(object sender, EventArgs e)
		{
		}

		private void rbProductsEdit_Click(object sender, EventArgs e)
		{
			this.OpenNewForm(new productsEdit());
		}

		private void rbCategoriesEdit_Click(object sender, EventArgs e)
		{
			this.OpenNewForm(new categoryEdit());
		}

		private void ribbon1_ActiveTabChanged(object sender, EventArgs e)
		{
			this.CloseAllOpenForms();
		}

		private void ribbonButton4_Click(object sender, EventArgs e)
		{
			this.OpenNewForm(new manufacturerEdit());
		}

		private void productsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.OpenNewForm(new productsEdit());
		}

		private void categoriesToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.OpenNewForm(new categoryEdit());
		}

		private void manufacturersToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.OpenNewForm(new manufacturerEdit());
		}

		private TaskManager taskManager;
	}
}
