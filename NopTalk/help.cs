﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using NopTalk.Properties;

namespace NopTalk
{
	// Token: 0x02000018 RID: 24
	public partial class help : Form
	{
		// Token: 0x0600009C RID: 156 RVA: 0x000027AF File Offset: 0x000009AF
		public help()
		{
			
			this.components = null;
		
			this.InitializeComponent();
		}

		// Token: 0x0600009D RID: 157 RVA: 0x000024A0 File Offset: 0x000006A0
		private void help_Load(object sender, EventArgs e)
		{
		}

		// Token: 0x0600009E RID: 158 RVA: 0x000024A0 File Offset: 0x000006A0
		private void help_SizeChanged(object sender, EventArgs e)
		{
		}

		// Token: 0x0600009F RID: 159 RVA: 0x000027C9 File Offset: 0x000009C9
		private void help_Shown(object sender, EventArgs e)
		{
			this.BackgroundImageLayout = ImageLayout.Zoom;
			this.BackgroundImage = Resources.noptalk_diagram;
		}
	}
}
