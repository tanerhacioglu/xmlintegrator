﻿namespace NopTalk
{
	// Token: 0x0200004A RID: 74
	public partial class eShopNewEdit : global::System.Windows.Forms.Form
	{
		// Token: 0x060003D9 RID: 985 RVA: 0x00003B11 File Offset: 0x00001D11
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x060003DA RID: 986 RVA: 0x000567F0 File Offset: 0x000549F0
		private void InitializeComponent()
		{
			this.components = new global::System.ComponentModel.Container();
			global::System.ComponentModel.ComponentResourceManager componentResourceManager = new global::System.ComponentModel.ComponentResourceManager(typeof(global::NopTalk.eShopNewEdit));
			this.groupBox1 = new global::System.Windows.Forms.GroupBox();
			this.label7 = new global::System.Windows.Forms.Label();
			this.label5 = new global::System.Windows.Forms.Label();
			this.cbVersion = new global::System.Windows.Forms.ComboBox();
			this.label6 = new global::System.Windows.Forms.Label();
			this.tbDbConnString = new global::System.Windows.Forms.TextBox();
			this.label4 = new global::System.Windows.Forms.Label();
			this.tbDescription = new global::System.Windows.Forms.TextBox();
			this.label3 = new global::System.Windows.Forms.Label();
			this.tbName = new global::System.Windows.Forms.TextBox();
			this.label2 = new global::System.Windows.Forms.Label();
			this.tbId = new global::System.Windows.Forms.TextBox();
			this.label1 = new global::System.Windows.Forms.Label();
			this.btnSave = new global::System.Windows.Forms.Button();
			this.btnClose = new global::System.Windows.Forms.Button();
			this.btnTestDbConn = new global::System.Windows.Forms.Button();
			this.errorProvider1 = new global::System.Windows.Forms.ErrorProvider(this.components);
			this.statusArea = new global::System.Windows.Forms.StatusStrip();
			this.statusAreaText = new global::System.Windows.Forms.ToolStripStatusLabel();
			this.cbDatabaseType = new global::System.Windows.Forms.ComboBox();
			this.label8 = new global::System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			((global::System.ComponentModel.ISupportInitialize)this.errorProvider1).BeginInit();
			this.statusArea.SuspendLayout();
			base.SuspendLayout();
			this.groupBox1.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.groupBox1.Controls.Add(this.cbDatabaseType);
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.cbVersion);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.tbDbConnString);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.tbDescription);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.tbName);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.tbId);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new global::System.Drawing.Point(16, 15);
			this.groupBox1.Margin = new global::System.Windows.Forms.Padding(4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Padding = new global::System.Windows.Forms.Padding(4);
			this.groupBox1.Size = new global::System.Drawing.Size(1000, 458);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Store connection";
			this.label7.AutoSize = true;
			this.label7.Location = new global::System.Drawing.Point(42, 417);
			this.label7.Name = "label7";
			this.label7.Size = new global::System.Drawing.Size(692, 17);
			this.label7.TabIndex = 13;
			this.label7.Text = "Example: \"Data Source=localhost;Initial Catalog=Nop40;Integrated Security=True;Persist Security Info=False\"";
			this.label5.AutoSize = true;
			this.label5.Location = new global::System.Drawing.Point(31, 397);
			this.label5.Name = "label5";
			this.label5.Size = new global::System.Drawing.Size(825, 17);
			this.label5.TabIndex = 12;
			this.label5.Text = "* Database Connection string can be found in \\yourwebfolder\\App_Data\\dataSettings.json or \\yourwebfolder\\App_Data\\Settings.txt";
			this.cbVersion.DropDownStyle = global::System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbVersion.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.cbVersion.FormattingEnabled = true;
			this.cbVersion.Items.AddRange(new object[]
			{
				"3.5",
				"3.6",
				"3.7",
				"3.8",
				"3.9",
				"4.0",
				"4.1",
				"4.2",
				"4.3"
			});
			this.cbVersion.Location = new global::System.Drawing.Point(269, 310);
			this.cbVersion.Margin = new global::System.Windows.Forms.Padding(4);
			this.cbVersion.Name = "cbVersion";
			this.cbVersion.Size = new global::System.Drawing.Size(160, 24);
			this.cbVersion.TabIndex = 11;
			this.cbVersion.Validating += new global::System.ComponentModel.CancelEventHandler(this.cbVersion_Validating);
			this.cbVersion.Validated += new global::System.EventHandler(this.cbVersion_Validated);
			this.label6.AutoSize = true;
			this.label6.Location = new global::System.Drawing.Point(33, 310);
			this.label6.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label6.Name = "label6";
			this.label6.Size = new global::System.Drawing.Size(149, 17);
			this.label6.TabIndex = 10;
			this.label6.Text = "nopCommerce version";
			this.tbDbConnString.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tbDbConnString.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbDbConnString.Location = new global::System.Drawing.Point(269, 221);
			this.tbDbConnString.Margin = new global::System.Windows.Forms.Padding(4);
			this.tbDbConnString.Multiline = true;
			this.tbDbConnString.Name = "tbDbConnString";
			this.tbDbConnString.Size = new global::System.Drawing.Size(703, 72);
			this.tbDbConnString.TabIndex = 7;
			this.tbDbConnString.Validating += new global::System.ComponentModel.CancelEventHandler(this.tbDbConnString_Validating);
			this.tbDbConnString.Validated += new global::System.EventHandler(this.tbDbConnString_Validated);
			this.label4.AutoSize = true;
			this.label4.Location = new global::System.Drawing.Point(31, 221);
			this.label4.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new global::System.Drawing.Size(190, 17);
			this.label4.TabIndex = 6;
			this.label4.Text = "Database Connection String*";
			this.tbDescription.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tbDescription.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbDescription.Location = new global::System.Drawing.Point(269, 135);
			this.tbDescription.Margin = new global::System.Windows.Forms.Padding(4);
			this.tbDescription.Multiline = true;
			this.tbDescription.Name = "tbDescription";
			this.tbDescription.Size = new global::System.Drawing.Size(703, 64);
			this.tbDescription.TabIndex = 5;
			this.label3.AutoSize = true;
			this.label3.Location = new global::System.Drawing.Point(32, 144);
			this.label3.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new global::System.Drawing.Size(79, 17);
			this.label3.TabIndex = 4;
			this.label3.Text = "Description";
			this.tbName.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tbName.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbName.Location = new global::System.Drawing.Point(269, 87);
			this.tbName.Margin = new global::System.Windows.Forms.Padding(4);
			this.tbName.Name = "tbName";
			this.tbName.Size = new global::System.Drawing.Size(703, 22);
			this.tbName.TabIndex = 3;
			this.tbName.Validating += new global::System.ComponentModel.CancelEventHandler(this.tbName_Validating);
			this.tbName.Validated += new global::System.EventHandler(this.tbName_Validated);
			this.label2.AutoSize = true;
			this.label2.Location = new global::System.Drawing.Point(33, 96);
			this.label2.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new global::System.Drawing.Size(45, 17);
			this.label2.TabIndex = 2;
			this.label2.Text = "Name";
			this.tbId.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.tbId.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbId.Location = new global::System.Drawing.Point(269, 42);
			this.tbId.Margin = new global::System.Windows.Forms.Padding(4);
			this.tbId.Name = "tbId";
			this.tbId.ReadOnly = true;
			this.tbId.Size = new global::System.Drawing.Size(703, 22);
			this.tbId.TabIndex = 1;
			this.label1.AutoSize = true;
			this.label1.Location = new global::System.Drawing.Point(32, 50);
			this.label1.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new global::System.Drawing.Size(19, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "Id";
			this.btnSave.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnSave.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnSave.Location = new global::System.Drawing.Point(808, 480);
			this.btnSave.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new global::System.Drawing.Size(100, 28);
			this.btnSave.TabIndex = 1;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new global::System.EventHandler(this.btnSave_Click);
			this.btnClose.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnClose.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnClose.Location = new global::System.Drawing.Point(916, 480);
			this.btnClose.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new global::System.Drawing.Size(100, 28);
			this.btnClose.TabIndex = 2;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new global::System.EventHandler(this.btnClose_Click);
			this.btnTestDbConn.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnTestDbConn.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnTestDbConn.Location = new global::System.Drawing.Point(700, 480);
			this.btnTestDbConn.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnTestDbConn.Name = "btnTestDbConn";
			this.btnTestDbConn.Size = new global::System.Drawing.Size(100, 28);
			this.btnTestDbConn.TabIndex = 3;
			this.btnTestDbConn.Text = "Test";
			this.btnTestDbConn.UseVisualStyleBackColor = true;
			this.btnTestDbConn.Click += new global::System.EventHandler(this.btnTestDbConn_Click);
			this.errorProvider1.ContainerControl = this;
			this.statusArea.AutoSize = false;
			this.statusArea.ImageScalingSize = new global::System.Drawing.Size(20, 20);
			this.statusArea.Items.AddRange(new global::System.Windows.Forms.ToolStripItem[]
			{
				this.statusAreaText
			});
			this.statusArea.Location = new global::System.Drawing.Point(0, 512);
			this.statusArea.Name = "statusArea";
			this.statusArea.Padding = new global::System.Windows.Forms.Padding(1, 0, 19, 0);
			this.statusArea.Size = new global::System.Drawing.Size(1032, 27);
			this.statusArea.TabIndex = 4;
			this.statusArea.Text = "statusArea";
			this.statusAreaText.Name = "statusAreaText";
			this.statusAreaText.Size = new global::System.Drawing.Size(0, 21);
			this.cbDatabaseType.DropDownStyle = global::System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbDatabaseType.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.cbDatabaseType.FormattingEnabled = true;
			this.cbDatabaseType.Items.AddRange(new object[]
			{
				"MSSQL",
				"MySQL"
			});
			this.cbDatabaseType.Location = new global::System.Drawing.Point(269, 351);
			this.cbDatabaseType.Margin = new global::System.Windows.Forms.Padding(4);
			this.cbDatabaseType.Name = "cbDatabaseType";
			this.cbDatabaseType.Size = new global::System.Drawing.Size(160, 24);
			this.cbDatabaseType.TabIndex = 15;
			this.label8.AutoSize = true;
			this.label8.Location = new global::System.Drawing.Point(33, 351);
			this.label8.Margin = new global::System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label8.Name = "label8";
			this.label8.Size = new global::System.Drawing.Size(105, 17);
			this.label8.TabIndex = 14;
			this.label8.Text = "Database Type";
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(8f, 16f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(1032, 539);
			base.Controls.Add(this.statusArea);
			base.Controls.Add(this.btnTestDbConn);
			base.Controls.Add(this.btnClose);
			base.Controls.Add(this.btnSave);
			base.Controls.Add(this.groupBox1);
			base.Icon = (global::System.Drawing.Icon)componentResourceManager.GetObject("$this.Icon");
			base.Margin = new global::System.Windows.Forms.Padding(4);
			base.Name = "eShopNewEdit";
			this.Text = "Store settings";
			base.Load += new global::System.EventHandler(this.eShopNewEdit_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((global::System.ComponentModel.ISupportInitialize)this.errorProvider1).EndInit();
			this.statusArea.ResumeLayout(false);
			this.statusArea.PerformLayout();
			base.ResumeLayout(false);
		}

		// Token: 0x04000770 RID: 1904
		private global::System.ComponentModel.IContainer components;

		// Token: 0x04000771 RID: 1905
		private global::System.Windows.Forms.GroupBox groupBox1;

		// Token: 0x04000772 RID: 1906
		private global::System.Windows.Forms.TextBox tbDbConnString;

		// Token: 0x04000773 RID: 1907
		private global::System.Windows.Forms.Label label4;

		// Token: 0x04000774 RID: 1908
		private global::System.Windows.Forms.TextBox tbDescription;

		// Token: 0x04000775 RID: 1909
		private global::System.Windows.Forms.Label label3;

		// Token: 0x04000776 RID: 1910
		private global::System.Windows.Forms.TextBox tbName;

		// Token: 0x04000777 RID: 1911
		private global::System.Windows.Forms.Label label2;

		// Token: 0x04000778 RID: 1912
		private global::System.Windows.Forms.TextBox tbId;

		// Token: 0x04000779 RID: 1913
		private global::System.Windows.Forms.Label label1;

		// Token: 0x0400077A RID: 1914
		private global::System.Windows.Forms.Button btnSave;

		// Token: 0x0400077B RID: 1915
		private global::System.Windows.Forms.Button btnClose;

		// Token: 0x0400077C RID: 1916
		private global::System.Windows.Forms.Button btnTestDbConn;

		// Token: 0x0400077D RID: 1917
		private global::System.Windows.Forms.ErrorProvider errorProvider1;

		// Token: 0x0400077E RID: 1918
		private global::System.Windows.Forms.ComboBox cbVersion;

		// Token: 0x0400077F RID: 1919
		private global::System.Windows.Forms.Label label6;

		// Token: 0x04000780 RID: 1920
		private global::System.Windows.Forms.StatusStrip statusArea;

		// Token: 0x04000781 RID: 1921
		private global::System.Windows.Forms.ToolStripStatusLabel statusAreaText;

		// Token: 0x04000782 RID: 1922
		private global::System.Windows.Forms.Label label7;

		// Token: 0x04000783 RID: 1923
		private global::System.Windows.Forms.Label label5;

		// Token: 0x04000784 RID: 1924
		private global::System.Windows.Forms.ComboBox cbDatabaseType;

		// Token: 0x04000785 RID: 1925
		private global::System.Windows.Forms.Label label8;
	}
}
