﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace NopTalk
{
	// Token: 0x02000019 RID: 25
	public static class Extensions
	{
		// Token: 0x060000A2 RID: 162 RVA: 0x0000D578 File Offset: 0x0000B778
		public static void SetDoubleBuffered(this Control c)
		{
			if (!SystemInformation.TerminalServerSession)
			{
				PropertyInfo property = typeof(Control).GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic);
				property.SetValue(c, true, null);
			}
		}

		// Token: 0x060000A3 RID: 163 RVA: 0x0000D5B4 File Offset: 0x0000B7B4
		public static IEnumerable<Control> GetAll(this Control control, Type type)
		{
			IEnumerable<Control> enumerable = control.Controls.Cast<Control>();
			return from c in enumerable.SelectMany((Control ctrl) => ctrl.GetAll(type)).Concat(enumerable)
			where c.GetType() == type
			select c;
		}

		// Token: 0x060000A4 RID: 164 RVA: 0x0000D608 File Offset: 0x0000B808
		public static IEnumerable<Control> GetAll(this Control control)
		{
			IEnumerable<Control> enumerable = control.Controls.Cast<Control>();
			return enumerable.SelectMany((Control ctrl) => ctrl.GetAll()).Concat(enumerable);
		}
	}
}
