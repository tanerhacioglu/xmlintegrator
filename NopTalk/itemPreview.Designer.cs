﻿namespace NopTalk
{
	// Token: 0x0200004E RID: 78
	public partial class itemPreview : global::System.Windows.Forms.Form
	{
		// Token: 0x060003F3 RID: 1011 RVA: 0x00003BF3 File Offset: 0x00001DF3
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x060003F4 RID: 1012 RVA: 0x00058B6C File Offset: 0x00056D6C
		private void InitializeComponent()
		{
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tbProdGroupBy = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbProdName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbProdShortDesc = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbProdFullDesc = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbProdManPartNum = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbProdCost = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbProdPrice = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbStockQuantity = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbMetaKeys = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbMetaDescription = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbMetaTitle = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbSearchPage = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.tbManufacturer = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tbVendor = new System.Windows.Forms.TextBox();
            this.tbTaxCategory = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tbGtin = new System.Windows.Forms.TextBox();
            this.tbWeight = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.tbLength = new System.Windows.Forms.TextBox();
            this.tbWidth = new System.Windows.Forms.TextBox();
            this.tbHeight = new System.Windows.Forms.TextBox();
            this.tbOldPrice = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.tbMinStockQty = new System.Windows.Forms.TextBox();
            this.tbNotifyForQty = new System.Windows.Forms.TextBox();
            this.tbMinCartQty = new System.Windows.Forms.TextBox();
            this.tbMaxCartQty = new System.Windows.Forms.TextBox();
            this.tbAllowedQty = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbProdId = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label34 = new System.Windows.Forms.Label();
            this.dgTierPricing = new System.Windows.Forms.DataGridView();
            this.dgCustomerRoles = new System.Windows.Forms.DataGridView();
            this.label17 = new System.Windows.Forms.Label();
            this.dgCategories = new System.Windows.Forms.DataGridView();
            this.label26 = new System.Windows.Forms.Label();
            this.dgAttributes = new System.Windows.Forms.DataGridView();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dgFilters = new System.Windows.Forms.DataGridView();
            this.lblFoundItems = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.lblShow = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTierPricing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgCustomerRoles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgCategories)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAttributes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgFilters)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.AutoScroll = true;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.08104F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.91896F));
            this.tableLayoutPanel1.Controls.Add(this.tbProdGroupBy, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label21, 0, 18);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tbProdName, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tbProdShortDesc, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.tbProdFullDesc, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.tbProdManPartNum, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label19, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.tbProdCost, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.tbProdPrice, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.tbStockQuantity, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.tbMetaKeys, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.tbMetaDescription, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.tbMetaTitle, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.tbSearchPage, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.label47, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.tbManufacturer, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.label18, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.tbVendor, 1, 15);
            this.tableLayoutPanel1.Controls.Add(this.tbTaxCategory, 1, 16);
            this.tableLayoutPanel1.Controls.Add(this.label20, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.tbGtin, 1, 17);
            this.tableLayoutPanel1.Controls.Add(this.tbWeight, 1, 18);
            this.tableLayoutPanel1.Controls.Add(this.label22, 0, 17);
            this.tableLayoutPanel1.Controls.Add(this.label23, 0, 19);
            this.tableLayoutPanel1.Controls.Add(this.label24, 0, 20);
            this.tableLayoutPanel1.Controls.Add(this.label25, 0, 21);
            this.tableLayoutPanel1.Controls.Add(this.tbLength, 1, 19);
            this.tableLayoutPanel1.Controls.Add(this.tbWidth, 1, 20);
            this.tableLayoutPanel1.Controls.Add(this.tbHeight, 1, 21);
            this.tableLayoutPanel1.Controls.Add(this.tbOldPrice, 1, 22);
            this.tableLayoutPanel1.Controls.Add(this.label27, 0, 22);
            this.tableLayoutPanel1.Controls.Add(this.label28, 0, 23);
            this.tableLayoutPanel1.Controls.Add(this.label29, 0, 24);
            this.tableLayoutPanel1.Controls.Add(this.label30, 0, 25);
            this.tableLayoutPanel1.Controls.Add(this.label31, 0, 26);
            this.tableLayoutPanel1.Controls.Add(this.label32, 0, 27);
            this.tableLayoutPanel1.Controls.Add(this.tbMinStockQty, 1, 23);
            this.tableLayoutPanel1.Controls.Add(this.tbNotifyForQty, 1, 24);
            this.tableLayoutPanel1.Controls.Add(this.tbMinCartQty, 1, 25);
            this.tableLayoutPanel1.Controls.Add(this.tbMaxCartQty, 1, 26);
            this.tableLayoutPanel1.Controls.Add(this.tbAllowedQty, 1, 27);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbProdId, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label33, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(16, 43);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 28;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(393, 621);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tbProdGroupBy
            // 
            this.tbProdGroupBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdGroupBy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdGroupBy.Location = new System.Drawing.Point(146, 59);
            this.tbProdGroupBy.Margin = new System.Windows.Forms.Padding(4);
            this.tbProdGroupBy.Name = "tbProdGroupBy";
            this.tbProdGroupBy.ReadOnly = true;
            this.tbProdGroupBy.Size = new System.Drawing.Size(221, 22);
            this.tbProdGroupBy.TabIndex = 55;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(5, 503);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 17);
            this.label21.TabIndex = 34;
            this.label21.Text = "Weight";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 1);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Field";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(146, 1);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Value";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 83);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Product Name";
            // 
            // tbProdName
            // 
            this.tbProdName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdName.Location = new System.Drawing.Point(146, 87);
            this.tbProdName.Margin = new System.Windows.Forms.Padding(4);
            this.tbProdName.Name = "tbProdName";
            this.tbProdName.ReadOnly = true;
            this.tbProdName.Size = new System.Drawing.Size(221, 22);
            this.tbProdName.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 111);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 27);
            this.label6.TabIndex = 6;
            this.label6.Text = "Product Short Description";
            // 
            // tbProdShortDesc
            // 
            this.tbProdShortDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdShortDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdShortDesc.Location = new System.Drawing.Point(146, 115);
            this.tbProdShortDesc.Margin = new System.Windows.Forms.Padding(4);
            this.tbProdShortDesc.Name = "tbProdShortDesc";
            this.tbProdShortDesc.ReadOnly = true;
            this.tbProdShortDesc.Size = new System.Drawing.Size(221, 22);
            this.tbProdShortDesc.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 139);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 27);
            this.label7.TabIndex = 8;
            this.label7.Text = "Product Full Description";
            // 
            // tbProdFullDesc
            // 
            this.tbProdFullDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdFullDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdFullDesc.Location = new System.Drawing.Point(146, 143);
            this.tbProdFullDesc.Margin = new System.Windows.Forms.Padding(4);
            this.tbProdFullDesc.Name = "tbProdFullDesc";
            this.tbProdFullDesc.ReadOnly = true;
            this.tbProdFullDesc.Size = new System.Drawing.Size(221, 22);
            this.tbProdFullDesc.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 167);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(126, 27);
            this.label8.TabIndex = 10;
            this.label8.Text = "Manufacturer Part Number";
            // 
            // tbProdManPartNum
            // 
            this.tbProdManPartNum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdManPartNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdManPartNum.Location = new System.Drawing.Point(146, 171);
            this.tbProdManPartNum.Margin = new System.Windows.Forms.Padding(4);
            this.tbProdManPartNum.Name = "tbProdManPartNum";
            this.tbProdManPartNum.ReadOnly = true;
            this.tbProdManPartNum.Size = new System.Drawing.Size(221, 22);
            this.tbProdManPartNum.TabIndex = 11;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(5, 195);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(89, 17);
            this.label19.TabIndex = 12;
            this.label19.Text = "Product Cost";
            // 
            // tbProdCost
            // 
            this.tbProdCost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdCost.Location = new System.Drawing.Point(146, 199);
            this.tbProdCost.Margin = new System.Windows.Forms.Padding(4);
            this.tbProdCost.Name = "tbProdCost";
            this.tbProdCost.ReadOnly = true;
            this.tbProdCost.Size = new System.Drawing.Size(221, 22);
            this.tbProdCost.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 223);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 17);
            this.label9.TabIndex = 14;
            this.label9.Text = "Product Price";
            // 
            // tbProdPrice
            // 
            this.tbProdPrice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdPrice.Location = new System.Drawing.Point(146, 227);
            this.tbProdPrice.Margin = new System.Windows.Forms.Padding(4);
            this.tbProdPrice.Name = "tbProdPrice";
            this.tbProdPrice.ReadOnly = true;
            this.tbProdPrice.Size = new System.Drawing.Size(221, 22);
            this.tbProdPrice.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 251);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 17);
            this.label10.TabIndex = 16;
            this.label10.Text = "Stock Quantity";
            // 
            // tbStockQuantity
            // 
            this.tbStockQuantity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbStockQuantity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbStockQuantity.Location = new System.Drawing.Point(146, 255);
            this.tbStockQuantity.Margin = new System.Windows.Forms.Padding(4);
            this.tbStockQuantity.Name = "tbStockQuantity";
            this.tbStockQuantity.ReadOnly = true;
            this.tbStockQuantity.Size = new System.Drawing.Size(221, 22);
            this.tbStockQuantity.TabIndex = 17;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 279);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(104, 17);
            this.label11.TabIndex = 18;
            this.label11.Text = "Meta Keywords";
            // 
            // tbMetaKeys
            // 
            this.tbMetaKeys.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMetaKeys.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMetaKeys.Location = new System.Drawing.Point(146, 283);
            this.tbMetaKeys.Margin = new System.Windows.Forms.Padding(4);
            this.tbMetaKeys.Name = "tbMetaKeys";
            this.tbMetaKeys.ReadOnly = true;
            this.tbMetaKeys.Size = new System.Drawing.Size(221, 22);
            this.tbMetaKeys.TabIndex = 19;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 307);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(114, 17);
            this.label12.TabIndex = 20;
            this.label12.Text = "Meta Description";
            // 
            // tbMetaDescription
            // 
            this.tbMetaDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMetaDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMetaDescription.Location = new System.Drawing.Point(146, 311);
            this.tbMetaDescription.Margin = new System.Windows.Forms.Padding(4);
            this.tbMetaDescription.Name = "tbMetaDescription";
            this.tbMetaDescription.ReadOnly = true;
            this.tbMetaDescription.Size = new System.Drawing.Size(221, 22);
            this.tbMetaDescription.TabIndex = 21;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 335);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 17);
            this.label13.TabIndex = 22;
            this.label13.Text = "Meta Title";
            // 
            // tbMetaTitle
            // 
            this.tbMetaTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMetaTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMetaTitle.Location = new System.Drawing.Point(146, 339);
            this.tbMetaTitle.Margin = new System.Windows.Forms.Padding(4);
            this.tbMetaTitle.Name = "tbMetaTitle";
            this.tbMetaTitle.ReadOnly = true;
            this.tbMetaTitle.Size = new System.Drawing.Size(221, 22);
            this.tbMetaTitle.TabIndex = 23;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(5, 363);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(105, 27);
            this.label14.TabIndex = 24;
            this.label14.Text = "Search Engine Friendly Page Name";
            // 
            // tbSearchPage
            // 
            this.tbSearchPage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearchPage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSearchPage.Location = new System.Drawing.Point(146, 367);
            this.tbSearchPage.Margin = new System.Windows.Forms.Padding(4);
            this.tbSearchPage.Name = "tbSearchPage";
            this.tbSearchPage.ReadOnly = true;
            this.tbSearchPage.Size = new System.Drawing.Size(221, 22);
            this.tbSearchPage.TabIndex = 25;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(5, 391);
            this.label47.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(92, 17);
            this.label47.TabIndex = 26;
            this.label47.Text = "Manufacturer";
            // 
            // tbManufacturer
            // 
            this.tbManufacturer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbManufacturer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbManufacturer.Location = new System.Drawing.Point(146, 395);
            this.tbManufacturer.Margin = new System.Windows.Forms.Padding(4);
            this.tbManufacturer.Name = "tbManufacturer";
            this.tbManufacturer.ReadOnly = true;
            this.tbManufacturer.Size = new System.Drawing.Size(221, 22);
            this.tbManufacturer.TabIndex = 27;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 419);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 17);
            this.label18.TabIndex = 28;
            this.label18.Text = "Vendor";
            // 
            // tbVendor
            // 
            this.tbVendor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbVendor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbVendor.Location = new System.Drawing.Point(146, 423);
            this.tbVendor.Margin = new System.Windows.Forms.Padding(4);
            this.tbVendor.Name = "tbVendor";
            this.tbVendor.ReadOnly = true;
            this.tbVendor.Size = new System.Drawing.Size(221, 22);
            this.tbVendor.TabIndex = 29;
            // 
            // tbTaxCategory
            // 
            this.tbTaxCategory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTaxCategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbTaxCategory.Location = new System.Drawing.Point(146, 451);
            this.tbTaxCategory.Margin = new System.Windows.Forms.Padding(4);
            this.tbTaxCategory.Name = "tbTaxCategory";
            this.tbTaxCategory.ReadOnly = true;
            this.tbTaxCategory.Size = new System.Drawing.Size(221, 22);
            this.tbTaxCategory.TabIndex = 31;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(5, 447);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(92, 17);
            this.label20.TabIndex = 30;
            this.label20.Text = "Tax Category";
            // 
            // tbGtin
            // 
            this.tbGtin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbGtin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbGtin.Location = new System.Drawing.Point(146, 479);
            this.tbGtin.Margin = new System.Windows.Forms.Padding(4);
            this.tbGtin.Name = "tbGtin";
            this.tbGtin.ReadOnly = true;
            this.tbGtin.Size = new System.Drawing.Size(221, 22);
            this.tbGtin.TabIndex = 32;
            // 
            // tbWeight
            // 
            this.tbWeight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbWeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbWeight.Location = new System.Drawing.Point(146, 507);
            this.tbWeight.Margin = new System.Windows.Forms.Padding(4);
            this.tbWeight.Name = "tbWeight";
            this.tbWeight.ReadOnly = true;
            this.tbWeight.Size = new System.Drawing.Size(221, 22);
            this.tbWeight.TabIndex = 33;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(5, 475);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 17);
            this.label22.TabIndex = 35;
            this.label22.Text = "GTIN";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(5, 531);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(52, 17);
            this.label23.TabIndex = 36;
            this.label23.Text = "Length";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(5, 559);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(44, 17);
            this.label24.TabIndex = 37;
            this.label24.Text = "Width";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(5, 587);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(49, 17);
            this.label25.TabIndex = 38;
            this.label25.Text = "Height";
            // 
            // tbLength
            // 
            this.tbLength.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbLength.Location = new System.Drawing.Point(146, 535);
            this.tbLength.Margin = new System.Windows.Forms.Padding(4);
            this.tbLength.Name = "tbLength";
            this.tbLength.ReadOnly = true;
            this.tbLength.Size = new System.Drawing.Size(221, 22);
            this.tbLength.TabIndex = 39;
            // 
            // tbWidth
            // 
            this.tbWidth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbWidth.Location = new System.Drawing.Point(146, 563);
            this.tbWidth.Margin = new System.Windows.Forms.Padding(4);
            this.tbWidth.Name = "tbWidth";
            this.tbWidth.ReadOnly = true;
            this.tbWidth.Size = new System.Drawing.Size(221, 22);
            this.tbWidth.TabIndex = 40;
            // 
            // tbHeight
            // 
            this.tbHeight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbHeight.Location = new System.Drawing.Point(146, 591);
            this.tbHeight.Margin = new System.Windows.Forms.Padding(4);
            this.tbHeight.Name = "tbHeight";
            this.tbHeight.ReadOnly = true;
            this.tbHeight.Size = new System.Drawing.Size(221, 22);
            this.tbHeight.TabIndex = 41;
            // 
            // tbOldPrice
            // 
            this.tbOldPrice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOldPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbOldPrice.Location = new System.Drawing.Point(146, 619);
            this.tbOldPrice.Margin = new System.Windows.Forms.Padding(4);
            this.tbOldPrice.Name = "tbOldPrice";
            this.tbOldPrice.ReadOnly = true;
            this.tbOldPrice.Size = new System.Drawing.Size(221, 22);
            this.tbOldPrice.TabIndex = 43;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(4, 615);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(66, 17);
            this.label27.TabIndex = 42;
            this.label27.Text = "Old Price";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(4, 643);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(90, 17);
            this.label28.TabIndex = 44;
            this.label28.Text = "Min stock qty";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(4, 671);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(128, 17);
            this.label29.TabIndex = 45;
            this.label29.Text = "Notify for qty below";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(4, 699);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(81, 17);
            this.label30.TabIndex = 46;
            this.label30.Text = "Min cart qty";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(4, 727);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(84, 17);
            this.label31.TabIndex = 47;
            this.label31.Text = "Max cart qty";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(4, 755);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(121, 17);
            this.label32.TabIndex = 48;
            this.label32.Text = "Allowed quantities";
            // 
            // tbMinStockQty
            // 
            this.tbMinStockQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMinStockQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMinStockQty.Location = new System.Drawing.Point(146, 647);
            this.tbMinStockQty.Margin = new System.Windows.Forms.Padding(4);
            this.tbMinStockQty.Name = "tbMinStockQty";
            this.tbMinStockQty.ReadOnly = true;
            this.tbMinStockQty.Size = new System.Drawing.Size(221, 22);
            this.tbMinStockQty.TabIndex = 49;
            // 
            // tbNotifyForQty
            // 
            this.tbNotifyForQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNotifyForQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbNotifyForQty.Location = new System.Drawing.Point(146, 675);
            this.tbNotifyForQty.Margin = new System.Windows.Forms.Padding(4);
            this.tbNotifyForQty.Name = "tbNotifyForQty";
            this.tbNotifyForQty.ReadOnly = true;
            this.tbNotifyForQty.Size = new System.Drawing.Size(221, 22);
            this.tbNotifyForQty.TabIndex = 50;
            // 
            // tbMinCartQty
            // 
            this.tbMinCartQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMinCartQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMinCartQty.Location = new System.Drawing.Point(146, 703);
            this.tbMinCartQty.Margin = new System.Windows.Forms.Padding(4);
            this.tbMinCartQty.Name = "tbMinCartQty";
            this.tbMinCartQty.ReadOnly = true;
            this.tbMinCartQty.Size = new System.Drawing.Size(221, 22);
            this.tbMinCartQty.TabIndex = 51;
            // 
            // tbMaxCartQty
            // 
            this.tbMaxCartQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMaxCartQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMaxCartQty.Location = new System.Drawing.Point(146, 731);
            this.tbMaxCartQty.Margin = new System.Windows.Forms.Padding(4);
            this.tbMaxCartQty.Name = "tbMaxCartQty";
            this.tbMaxCartQty.ReadOnly = true;
            this.tbMaxCartQty.Size = new System.Drawing.Size(221, 22);
            this.tbMaxCartQty.TabIndex = 52;
            // 
            // tbAllowedQty
            // 
            this.tbAllowedQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAllowedQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbAllowedQty.Location = new System.Drawing.Point(146, 759);
            this.tbAllowedQty.Margin = new System.Windows.Forms.Padding(4);
            this.tbAllowedQty.Name = "tbAllowedQty";
            this.tbAllowedQty.ReadOnly = true;
            this.tbAllowedQty.Size = new System.Drawing.Size(221, 22);
            this.tbAllowedQty.TabIndex = 53;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 27);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Product Id";
            // 
            // tbProdId
            // 
            this.tbProdId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProdId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProdId.Location = new System.Drawing.Point(146, 31);
            this.tbProdId.Margin = new System.Windows.Forms.Padding(4);
            this.tbProdId.Name = "tbProdId";
            this.tbProdId.ReadOnly = true;
            this.tbProdId.Size = new System.Drawing.Size(221, 22);
            this.tbProdId.TabIndex = 3;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(4, 55);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(121, 17);
            this.label33.TabIndex = 54;
            this.label33.Text = "Product Group By";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox5);
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(104, 5);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(799, 123);
            this.panel1.TabIndex = 2;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new System.Drawing.Point(317, 0);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(151, 132);
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new System.Drawing.Point(477, 0);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(151, 132);
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(633, 0);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(160, 132);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(160, 0);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(151, 132);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(3, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(151, 132);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 1);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Images";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.8631F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 89.1369F));
            this.tableLayoutPanel2.Controls.Add(this.label34, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.dgTierPricing, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.dgCustomerRoles, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.label17, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.dgCategories, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label26, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.dgAttributes, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label16, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label15, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.dgFilters, 1, 5);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(417, 43);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 131F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(908, 621);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(5, 521);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(46, 17);
            this.label34.TabIndex = 7;
            this.label34.Text = "Filters";
            // 
            // dgTierPricing
            // 
            this.dgTierPricing.AllowUserToAddRows = false;
            this.dgTierPricing.AllowUserToDeleteRows = false;
            this.dgTierPricing.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgTierPricing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTierPricing.Location = new System.Drawing.Point(104, 428);
            this.dgTierPricing.Margin = new System.Windows.Forms.Padding(4);
            this.dgTierPricing.MultiSelect = false;
            this.dgTierPricing.Name = "dgTierPricing";
            this.dgTierPricing.RowHeadersWidth = 51;
            this.dgTierPricing.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgTierPricing.Size = new System.Drawing.Size(799, 88);
            this.dgTierPricing.TabIndex = 5;
            // 
            // dgCustomerRoles
            // 
            this.dgCustomerRoles.AllowUserToAddRows = false;
            this.dgCustomerRoles.AllowUserToDeleteRows = false;
            this.dgCustomerRoles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgCustomerRoles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCustomerRoles.Location = new System.Drawing.Point(104, 331);
            this.dgCustomerRoles.Margin = new System.Windows.Forms.Padding(4);
            this.dgCustomerRoles.MultiSelect = false;
            this.dgCustomerRoles.Name = "dgCustomerRoles";
            this.dgCustomerRoles.RowHeadersWidth = 51;
            this.dgCustomerRoles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCustomerRoles.Size = new System.Drawing.Size(799, 88);
            this.dgCustomerRoles.TabIndex = 5;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(5, 424);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(80, 17);
            this.label17.TabIndex = 4;
            this.label17.Text = "Tier Pricing";
            // 
            // dgCategories
            // 
            this.dgCategories.AllowUserToAddRows = false;
            this.dgCategories.AllowUserToDeleteRows = false;
            this.dgCategories.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgCategories.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCategories.Location = new System.Drawing.Point(104, 234);
            this.dgCategories.Margin = new System.Windows.Forms.Padding(4);
            this.dgCategories.MultiSelect = false;
            this.dgCategories.Name = "dgCategories";
            this.dgCategories.RowHeadersWidth = 51;
            this.dgCategories.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCategories.Size = new System.Drawing.Size(799, 88);
            this.dgCategories.TabIndex = 5;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(5, 327);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(72, 34);
            this.label26.TabIndex = 4;
            this.label26.Text = "Customer Roles";
            // 
            // dgAttributes
            // 
            this.dgAttributes.AllowUserToAddRows = false;
            this.dgAttributes.AllowUserToDeleteRows = false;
            this.dgAttributes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgAttributes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAttributes.Location = new System.Drawing.Point(104, 137);
            this.dgAttributes.Margin = new System.Windows.Forms.Padding(4);
            this.dgAttributes.MultiSelect = false;
            this.dgAttributes.Name = "dgAttributes";
            this.dgAttributes.RowHeadersWidth = 51;
            this.dgAttributes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgAttributes.Size = new System.Drawing.Size(799, 88);
            this.dgAttributes.TabIndex = 5;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(5, 230);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(76, 17);
            this.label16.TabIndex = 4;
            this.label16.Text = "Categories";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 133);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 17);
            this.label15.TabIndex = 4;
            this.label15.Text = "Attributes";
            // 
            // dgFilters
            // 
            this.dgFilters.AllowUserToAddRows = false;
            this.dgFilters.AllowUserToDeleteRows = false;
            this.dgFilters.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgFilters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFilters.Location = new System.Drawing.Point(104, 525);
            this.dgFilters.Margin = new System.Windows.Forms.Padding(4);
            this.dgFilters.MultiSelect = false;
            this.dgFilters.Name = "dgFilters";
            this.dgFilters.RowHeadersWidth = 51;
            this.dgFilters.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgFilters.Size = new System.Drawing.Size(799, 91);
            this.dgFilters.TabIndex = 6;
            // 
            // lblFoundItems
            // 
            this.lblFoundItems.AutoSize = true;
            this.lblFoundItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFoundItems.Location = new System.Drawing.Point(16, 11);
            this.lblFoundItems.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFoundItems.Name = "lblFoundItems";
            this.lblFoundItems.Size = new System.Drawing.Size(143, 25);
            this.lblFoundItems.TabIndex = 2;
            this.lblFoundItems.Text = "Found items: ";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(1168, 676);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 28);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(1060, 676);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(100, 28);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrev.Location = new System.Drawing.Point(952, 676);
            this.btnPrev.Margin = new System.Windows.Forms.Padding(4);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(100, 28);
            this.btnPrev.TabIndex = 5;
            this.btnPrev.Text = "Previuos";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // lblShow
            // 
            this.lblShow.AutoSize = true;
            this.lblShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShow.Location = new System.Drawing.Point(400, 11);
            this.lblShow.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShow.Name = "lblShow";
            this.lblShow.Size = new System.Drawing.Size(119, 25);
            this.lblShow.TabIndex = 6;
            this.lblShow.Text = "Show item:";
            // 
            // itemPreview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1347, 719);
            this.Controls.Add(this.lblShow);
            this.Controls.Add(this.btnPrev);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblFoundItems);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "itemPreview";
            this.Text = "Item(s) preview";
            this.Load += new System.EventHandler(this.itemPreview_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTierPricing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgCustomerRoles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgCategories)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAttributes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgFilters)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		// Token: 0x040007A6 RID: 1958
		private global::System.ComponentModel.IContainer components;

		// Token: 0x040007A7 RID: 1959
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

		// Token: 0x040007A8 RID: 1960
		private global::System.Windows.Forms.Label label3;

		// Token: 0x040007A9 RID: 1961
		private global::System.Windows.Forms.Label label1;

		// Token: 0x040007AA RID: 1962
		private global::System.Windows.Forms.Label label2;

		// Token: 0x040007AB RID: 1963
		private global::System.Windows.Forms.Panel panel1;

		// Token: 0x040007AC RID: 1964
		private global::System.Windows.Forms.PictureBox pictureBox5;

		// Token: 0x040007AD RID: 1965
		private global::System.Windows.Forms.PictureBox pictureBox4;

		// Token: 0x040007AE RID: 1966
		private global::System.Windows.Forms.PictureBox pictureBox3;

		// Token: 0x040007AF RID: 1967
		private global::System.Windows.Forms.PictureBox pictureBox2;

		// Token: 0x040007B0 RID: 1968
		private global::System.Windows.Forms.PictureBox pictureBox1;

		// Token: 0x040007B1 RID: 1969
		private global::System.Windows.Forms.Label label4;

		// Token: 0x040007B2 RID: 1970
		private global::System.Windows.Forms.TextBox tbProdId;

		// Token: 0x040007B3 RID: 1971
		private global::System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;

		// Token: 0x040007B4 RID: 1972
		private global::System.Windows.Forms.Label lblFoundItems;

		// Token: 0x040007B5 RID: 1973
		private global::System.Windows.Forms.Button btnClose;

		// Token: 0x040007B6 RID: 1974
		private global::System.Windows.Forms.Button btnNext;

		// Token: 0x040007B7 RID: 1975
		private global::System.Windows.Forms.Button btnPrev;

		// Token: 0x040007B8 RID: 1976
		private global::System.Windows.Forms.Label lblShow;

		// Token: 0x040007B9 RID: 1977
		private global::System.Windows.Forms.Label label5;

		// Token: 0x040007BA RID: 1978
		private global::System.Windows.Forms.TextBox tbProdName;

		// Token: 0x040007BB RID: 1979
		private global::System.Windows.Forms.Label label6;

		// Token: 0x040007BC RID: 1980
		private global::System.Windows.Forms.TextBox tbProdShortDesc;

		// Token: 0x040007BD RID: 1981
		private global::System.Windows.Forms.Label label7;

		// Token: 0x040007BE RID: 1982
		private global::System.Windows.Forms.TextBox tbProdFullDesc;

		// Token: 0x040007BF RID: 1983
		private global::System.Windows.Forms.Label label8;

		// Token: 0x040007C0 RID: 1984
		private global::System.Windows.Forms.TextBox tbProdManPartNum;

		// Token: 0x040007C1 RID: 1985
		private global::System.Windows.Forms.Label label19;

		// Token: 0x040007C2 RID: 1986
		private global::System.Windows.Forms.TextBox tbProdCost;

		// Token: 0x040007C3 RID: 1987
		private global::System.Windows.Forms.Label label9;

		// Token: 0x040007C4 RID: 1988
		private global::System.Windows.Forms.TextBox tbProdPrice;

		// Token: 0x040007C5 RID: 1989
		private global::System.Windows.Forms.Label label10;

		// Token: 0x040007C6 RID: 1990
		private global::System.Windows.Forms.TextBox tbStockQuantity;

		// Token: 0x040007C7 RID: 1991
		private global::System.Windows.Forms.Label label11;

		// Token: 0x040007C8 RID: 1992
		private global::System.Windows.Forms.TextBox tbMetaKeys;

		// Token: 0x040007C9 RID: 1993
		private global::System.Windows.Forms.Label label12;

		// Token: 0x040007CA RID: 1994
		private global::System.Windows.Forms.TextBox tbMetaDescription;

		// Token: 0x040007CB RID: 1995
		private global::System.Windows.Forms.Label label13;

		// Token: 0x040007CC RID: 1996
		private global::System.Windows.Forms.TextBox tbMetaTitle;

		// Token: 0x040007CD RID: 1997
		private global::System.Windows.Forms.Label label14;

		// Token: 0x040007CE RID: 1998
		private global::System.Windows.Forms.TextBox tbSearchPage;

		// Token: 0x040007CF RID: 1999
		private global::System.Windows.Forms.Label label15;

		// Token: 0x040007D0 RID: 2000
		private global::System.Windows.Forms.DataGridView dgAttributes;

		// Token: 0x040007D1 RID: 2001
		private global::System.Windows.Forms.DataGridView dgCategories;

		// Token: 0x040007D2 RID: 2002
		private global::System.Windows.Forms.Label label16;

		// Token: 0x040007D3 RID: 2003
		private global::System.Windows.Forms.Label label47;

		// Token: 0x040007D4 RID: 2004
		private global::System.Windows.Forms.TextBox tbManufacturer;

		// Token: 0x040007D5 RID: 2005
		private global::System.Windows.Forms.DataGridView dgTierPricing;

		// Token: 0x040007D6 RID: 2006
		private global::System.Windows.Forms.Label label17;

		// Token: 0x040007D7 RID: 2007
		private global::System.Windows.Forms.Label label18;

		// Token: 0x040007D8 RID: 2008
		private global::System.Windows.Forms.TextBox tbVendor;

		// Token: 0x040007D9 RID: 2009
		private global::System.Windows.Forms.TextBox tbTaxCategory;

		// Token: 0x040007DA RID: 2010
		private global::System.Windows.Forms.Label label20;

		// Token: 0x040007DB RID: 2011
		private global::System.Windows.Forms.Label label21;

		// Token: 0x040007DC RID: 2012
		private global::System.Windows.Forms.TextBox tbGtin;

		// Token: 0x040007DD RID: 2013
		private global::System.Windows.Forms.TextBox tbWeight;

		// Token: 0x040007DE RID: 2014
		private global::System.Windows.Forms.Label label22;

		// Token: 0x040007DF RID: 2015
		private global::System.Windows.Forms.Label label23;

		// Token: 0x040007E0 RID: 2016
		private global::System.Windows.Forms.Label label24;

		// Token: 0x040007E1 RID: 2017
		private global::System.Windows.Forms.Label label25;

		// Token: 0x040007E2 RID: 2018
		private global::System.Windows.Forms.TextBox tbLength;

		// Token: 0x040007E3 RID: 2019
		private global::System.Windows.Forms.TextBox tbWidth;

		// Token: 0x040007E4 RID: 2020
		private global::System.Windows.Forms.TextBox tbHeight;

		// Token: 0x040007E5 RID: 2021
		private global::System.Windows.Forms.DataGridView dgCustomerRoles;

		// Token: 0x040007E6 RID: 2022
		private global::System.Windows.Forms.Label label26;

		// Token: 0x040007E7 RID: 2023
		private global::System.Windows.Forms.Label label27;

		// Token: 0x040007E8 RID: 2024
		private global::System.Windows.Forms.TextBox tbOldPrice;

		// Token: 0x040007E9 RID: 2025
		private global::System.Windows.Forms.Label label28;

		// Token: 0x040007EA RID: 2026
		private global::System.Windows.Forms.Label label29;

		// Token: 0x040007EB RID: 2027
		private global::System.Windows.Forms.Label label30;

		// Token: 0x040007EC RID: 2028
		private global::System.Windows.Forms.Label label31;

		// Token: 0x040007ED RID: 2029
		private global::System.Windows.Forms.Label label32;

		// Token: 0x040007EE RID: 2030
		private global::System.Windows.Forms.TextBox tbMinStockQty;

		// Token: 0x040007EF RID: 2031
		private global::System.Windows.Forms.TextBox tbNotifyForQty;

		// Token: 0x040007F0 RID: 2032
		private global::System.Windows.Forms.TextBox tbMinCartQty;

		// Token: 0x040007F1 RID: 2033
		private global::System.Windows.Forms.TextBox tbMaxCartQty;

		// Token: 0x040007F2 RID: 2034
		private global::System.Windows.Forms.TextBox tbAllowedQty;

		// Token: 0x040007F3 RID: 2035
		private global::System.Windows.Forms.TextBox tbProdGroupBy;

		// Token: 0x040007F4 RID: 2036
		private global::System.Windows.Forms.Label label33;

		// Token: 0x040007F5 RID: 2037
		private global::System.Windows.Forms.Label label34;

		// Token: 0x040007F6 RID: 2038
		private global::System.Windows.Forms.DataGridView dgFilters;
	}
}
