﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Windows.Forms;
using NopTalkCore;

namespace NopTalk
{
	// Token: 0x0200001F RID: 31
	public partial class logList : Form
	{
		// Token: 0x060000B9 RID: 185 RVA: 0x0000286C File Offset: 0x00000A6C
		public logList()
		{
			
			this.timer1 = new Timer();
			this.components = null;
		
			this.InitializeComponent();
		}

		// Token: 0x060000BA RID: 186 RVA: 0x00002891 File Offset: 0x00000A91
		private void logList_Load(object sender, EventArgs e)
		{
			LogManager.LogClear(Convert.ToInt32(this.nmKeepDays.Value));
			this.gridFill();
			this.timer();
		}

		// Token: 0x060000BB RID: 187 RVA: 0x0000DEF4 File Offset: 0x0000C0F4
		public void gridFill()
		{
			try
			{
				DataSettingsManager dataSettingsManager = new DataSettingsManager();
				string dataConnectionString = dataSettingsManager.LoadSettings(null).DataConnectionString;
				this.sqlConn = new SqlCeConnection(dataConnectionString);
				GlobalClass.adap = new SqlCeDataAdapter(string.Format("select TOP(200) Log.Id, Log.TaskId, Log.MessageText, LogType.LogTypeName, Log.CreateDate from Log LEFT JOIN LogType ON Log.LogTypeId=LogType.Id Where Log.LogTypeId >-1 AND Log.CreateDate >= '{0}' AND Log.CreateDate<='{1}' order by Log.CreateDate desc", this.dtDateFrom.Value.Date.ToString("yyy-MM-dd HH:mm:ss"), this.dtDateTo.Value.Date.Add(new TimeSpan(23, 59, 59)).ToString("yyy-MM-dd HH:mm:ss")), this.sqlConn);
				this.sqlBui = new SqlCeCommandBuilder(GlobalClass.adap);
				GlobalClass.dt = new DataTable();
				GlobalClass.adap.Fill(GlobalClass.dt);
				this.dgLogs.AutoGenerateColumns = true;
				this.dgLogs.DataSource = GlobalClass.dt;
				this.dgLogs.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
				DataGridViewColumn dataGridViewColumn = this.dgLogs.Columns[0];
				dataGridViewColumn.MinimumWidth = 20;
				dataGridViewColumn.FillWeight = 50f;
				dataGridViewColumn = this.dgLogs.Columns[1];
				dataGridViewColumn.MinimumWidth = 20;
				dataGridViewColumn.FillWeight = 50f;
				dataGridViewColumn = this.dgLogs.Columns[2];
				dataGridViewColumn.MinimumWidth = 200;
				dataGridViewColumn.FillWeight = 500f;
				this.dgLogs.ReadOnly = true;
				this.dgLogs.ClearSelection();
			}
			catch
			{
			}
			finally
			{
				if (this.sqlConn != null && this.sqlConn.State == ConnectionState.Open)
				{
					this.sqlConn.Close();
				}
				if (this.sqlBui != null)
				{
					this.sqlBui.Dispose();
				}
			}
		}

		// Token: 0x060000BC RID: 188 RVA: 0x000028B4 File Offset: 0x00000AB4
		private void timer()
		{
			this.timer1.Interval = 10000;
			this.timer1.Tick += this.timer1_Tick;
			this.timer1.Start();
		}

		// Token: 0x060000BD RID: 189 RVA: 0x000028E8 File Offset: 0x00000AE8
		private void timer1_Tick(object sender, EventArgs e)
		{
			if (this.chAutoRefresh.Checked)
			{
				this.gridFill();
			}
		}

		// Token: 0x060000BE RID: 190 RVA: 0x000028FD File Offset: 0x00000AFD
		private void logList_FormClosed(object sender, FormClosedEventArgs e)
		{
			this.timer1.Stop();
			this.timer1.Dispose();
		}

		// Token: 0x060000BF RID: 191 RVA: 0x00002915 File Offset: 0x00000B15
		private void dtDateFrom_ValueChanged(object sender, EventArgs e)
		{
			this.gridFill();
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x00002915 File Offset: 0x00000B15
		private void dtDateTo_ValueChanged(object sender, EventArgs e)
		{
			this.gridFill();
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x0000291D File Offset: 0x00000B1D
		private void btnClearLogs_Click(object sender, EventArgs e)
		{
			LogManager.LogClear(0);
			this.gridFill();
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x060000C2 RID: 194 RVA: 0x0000E0E4 File Offset: 0x0000C2E4
		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams createParams = base.CreateParams;
				createParams.ExStyle |= 33554432;
				return createParams;
			}
		}

		// Token: 0x040000A8 RID: 168
		private SqlCeConnection sqlConn;

		// Token: 0x040000A9 RID: 169
		private SqlCeCommandBuilder sqlBui;

		// Token: 0x040000AA RID: 170
		private Timer timer1;
	}
}
