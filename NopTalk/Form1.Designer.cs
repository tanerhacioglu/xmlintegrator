﻿namespace NopTalk
{
	// Token: 0x02000016 RID: 22
	public partial class Form1 : global::System.Windows.Forms.Form
	{
		// Token: 0x0600008E RID: 142 RVA: 0x0000271E File Offset: 0x0000091E
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x0600008F RID: 143 RVA: 0x0000CA5C File Offset: 0x0000AC5C
		private void InitializeComponent()
		{
			this.checkBox1 = new global::System.Windows.Forms.CheckBox();
			this.button1 = new global::System.Windows.Forms.Button();
			base.SuspendLayout();
			this.checkBox1.AutoSize = true;
			this.checkBox1.Location = new global::System.Drawing.Point(444, 289);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new global::System.Drawing.Size(98, 21);
			this.checkBox1.TabIndex = 0;
			this.checkBox1.Text = "checkBox1";
			this.checkBox1.UseVisualStyleBackColor = true;
			this.button1.Location = new global::System.Drawing.Point(473, 134);
			this.button1.Name = "button1";
			this.button1.Size = new global::System.Drawing.Size(75, 23);
			this.button1.TabIndex = 1;
			this.button1.Text = "button1";
			this.button1.UseVisualStyleBackColor = true;
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(8f, 16f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(800, 450);
			base.Controls.Add(this.button1);
			base.Controls.Add(this.checkBox1);
			base.Name = "Form1";
			this.Text = "Form1";
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		// Token: 0x04000091 RID: 145
		private global::System.ComponentModel.IContainer components;

		// Token: 0x04000092 RID: 146
		private global::System.Windows.Forms.CheckBox checkBox1;

		// Token: 0x04000093 RID: 147
		private global::System.Windows.Forms.Button button1;
	}
}
