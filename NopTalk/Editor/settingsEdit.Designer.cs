﻿namespace NopTalk.Editor
{
	// Token: 0x02000078 RID: 120
	public partial class settingsEdit : global::System.Windows.Forms.Form
	{
		// Token: 0x06000501 RID: 1281 RVA: 0x0000454D File Offset: 0x0000274D
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000502 RID: 1282 RVA: 0x00064540 File Offset: 0x00062740
		private void InitializeComponent()
		{
			this.lblStatistic = new global::System.Windows.Forms.Label();
			this.clbColumnsList = new global::System.Windows.Forms.CheckedListBox();
			this.btnFilterClear = new global::System.Windows.Forms.Button();
			this.chPublished = new global::System.Windows.Forms.CheckBox();
			this.label5 = new global::System.Windows.Forms.Label();
			this.btnFilter = new global::System.Windows.Forms.Button();
			this.statusArea = new global::System.Windows.Forms.StatusStrip();
			this.statusAreaText = new global::System.Windows.Forms.ToolStripStatusLabel();
			this.groupBox1 = new global::System.Windows.Forms.GroupBox();
			this.label6 = new global::System.Windows.Forms.Label();
			this.tbSearch_Name = new global::System.Windows.Forms.TextBox();
			this.label3 = new global::System.Windows.Forms.Label();
			this.ddItemsLimit = new global::System.Windows.Forms.ComboBox();
			this.label2 = new global::System.Windows.Forms.Label();
			this.ddStoresConn = new global::System.Windows.Forms.ComboBox();
			this.label1 = new global::System.Windows.Forms.Label();
			this.dgDataEditor = new global::System.Windows.Forms.DataGridView();
			this.btnClose = new global::System.Windows.Forms.Button();
			this.btnSave = new global::System.Windows.Forms.Button();
			this.statusArea.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((global::System.ComponentModel.ISupportInitialize)this.dgDataEditor).BeginInit();
			base.SuspendLayout();
			this.lblStatistic.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left);
			this.lblStatistic.AutoSize = true;
			this.lblStatistic.Location = new global::System.Drawing.Point(12, 486);
			this.lblStatistic.Name = "lblStatistic";
			this.lblStatistic.Size = new global::System.Drawing.Size(108, 17);
			this.lblStatistic.TabIndex = 42;
			this.lblStatistic.Text = "Items Filtered: 0";
			this.clbColumnsList.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.clbColumnsList.CheckOnClick = true;
			this.clbColumnsList.Enabled = false;
			this.clbColumnsList.FormattingEnabled = true;
			this.clbColumnsList.Location = new global::System.Drawing.Point(601, 20);
			this.clbColumnsList.MultiColumn = true;
			this.clbColumnsList.Name = "clbColumnsList";
			this.clbColumnsList.Size = new global::System.Drawing.Size(392, 106);
			this.clbColumnsList.TabIndex = 27;
			this.btnFilterClear.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnFilterClear.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnFilterClear.Location = new global::System.Drawing.Point(1000, 42);
			this.btnFilterClear.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnFilterClear.Name = "btnFilterClear";
			this.btnFilterClear.Size = new global::System.Drawing.Size(181, 30);
			this.btnFilterClear.TabIndex = 23;
			this.btnFilterClear.Text = "Clear Filter";
			this.btnFilterClear.UseVisualStyleBackColor = true;
			this.chPublished.AutoSize = true;
			this.chPublished.Location = new global::System.Drawing.Point(130, 94);
			this.chPublished.Name = "chPublished";
			this.chPublished.Size = new global::System.Drawing.Size(18, 17);
			this.chPublished.TabIndex = 22;
			this.chPublished.UseVisualStyleBackColor = true;
			this.label5.AutoSize = true;
			this.label5.Location = new global::System.Drawing.Point(6, 94);
			this.label5.Name = "label5";
			this.label5.Size = new global::System.Drawing.Size(70, 17);
			this.label5.TabIndex = 21;
			this.label5.Text = "Published";
			this.btnFilter.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnFilter.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnFilter.Location = new global::System.Drawing.Point(1000, 83);
			this.btnFilter.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnFilter.Name = "btnFilter";
			this.btnFilter.Size = new global::System.Drawing.Size(181, 32);
			this.btnFilter.TabIndex = 5;
			this.btnFilter.Text = "Filter";
			this.btnFilter.UseVisualStyleBackColor = true;
			this.statusArea.AutoSize = false;
			this.statusArea.ImageScalingSize = new global::System.Drawing.Size(20, 20);
			this.statusArea.Items.AddRange(new global::System.Windows.Forms.ToolStripItem[]
			{
				this.statusAreaText
			});
			this.statusArea.Location = new global::System.Drawing.Point(0, 521);
			this.statusArea.Name = "statusArea";
			this.statusArea.Padding = new global::System.Windows.Forms.Padding(1, 0, 19, 0);
			this.statusArea.Size = new global::System.Drawing.Size(1213, 27);
			this.statusArea.TabIndex = 41;
			this.statusArea.Text = "statusArea";
			this.statusArea.ItemClicked += new global::System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusArea_ItemClicked);
			this.statusAreaText.Name = "statusAreaText";
			this.statusAreaText.Size = new global::System.Drawing.Size(0, 22);
			this.groupBox1.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.groupBox1.Controls.Add(this.clbColumnsList);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.btnFilterClear);
			this.groupBox1.Controls.Add(this.chPublished);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.btnFilter);
			this.groupBox1.Controls.Add(this.tbSearch_Name);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.ddItemsLimit);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.ddStoresConn);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new global::System.Drawing.Point(13, 4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new global::System.Drawing.Size(1188, 127);
			this.groupBox1.TabIndex = 37;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Filter";
			this.label6.AutoSize = true;
			this.label6.Location = new global::System.Drawing.Point(533, 23);
			this.label6.Name = "label6";
			this.label6.Size = new global::System.Drawing.Size(62, 17);
			this.label6.TabIndex = 26;
			this.label6.Text = "Columns";
			this.tbSearch_Name.Location = new global::System.Drawing.Point(130, 56);
			this.tbSearch_Name.Name = "tbSearch_Name";
			this.tbSearch_Name.Size = new global::System.Drawing.Size(376, 22);
			this.tbSearch_Name.TabIndex = 5;
			this.label3.AutoSize = true;
			this.label3.Location = new global::System.Drawing.Point(6, 56);
			this.label3.Name = "label3";
			this.label3.Size = new global::System.Drawing.Size(45, 17);
			this.label3.TabIndex = 4;
			this.label3.Text = "Name";
			this.ddItemsLimit.DropDownStyle = global::System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ddItemsLimit.FormattingEnabled = true;
			this.ddItemsLimit.Items.AddRange(new object[]
			{
				"20",
				"50",
				"100"
			});
			this.ddItemsLimit.Location = new global::System.Drawing.Point(429, 20);
			this.ddItemsLimit.Name = "ddItemsLimit";
			this.ddItemsLimit.Size = new global::System.Drawing.Size(77, 24);
			this.ddItemsLimit.TabIndex = 3;
			this.label2.AutoSize = true;
			this.label2.Location = new global::System.Drawing.Point(340, 23);
			this.label2.Name = "label2";
			this.label2.Size = new global::System.Drawing.Size(74, 17);
			this.label2.TabIndex = 2;
			this.label2.Text = "Items Limit";
			this.ddStoresConn.BackColor = global::System.Drawing.SystemColors.Window;
			this.ddStoresConn.DropDownStyle = global::System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ddStoresConn.FormattingEnabled = true;
			this.ddStoresConn.Location = new global::System.Drawing.Point(130, 20);
			this.ddStoresConn.Name = "ddStoresConn";
			this.ddStoresConn.Size = new global::System.Drawing.Size(204, 24);
			this.ddStoresConn.TabIndex = 1;
			this.label1.AutoSize = true;
			this.label1.Location = new global::System.Drawing.Point(6, 23);
			this.label1.Name = "label1";
			this.label1.Size = new global::System.Drawing.Size(117, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "Store Connection";
			this.dgDataEditor.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.dgDataEditor.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgDataEditor.Location = new global::System.Drawing.Point(13, 137);
			this.dgDataEditor.Name = "dgDataEditor";
			this.dgDataEditor.RowTemplate.Height = 24;
			this.dgDataEditor.Size = new global::System.Drawing.Size(1188, 336);
			this.dgDataEditor.TabIndex = 38;
			this.btnClose.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnClose.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnClose.Location = new global::System.Drawing.Point(1100, 480);
			this.btnClose.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new global::System.Drawing.Size(100, 28);
			this.btnClose.TabIndex = 40;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnSave.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnSave.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnSave.Location = new global::System.Drawing.Point(992, 480);
			this.btnSave.Margin = new global::System.Windows.Forms.Padding(4);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new global::System.Drawing.Size(100, 28);
			this.btnSave.TabIndex = 39;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(8f, 16f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(1213, 548);
			base.Controls.Add(this.lblStatistic);
			base.Controls.Add(this.statusArea);
			base.Controls.Add(this.groupBox1);
			base.Controls.Add(this.dgDataEditor);
			base.Controls.Add(this.btnClose);
			base.Controls.Add(this.btnSave);
			base.Name = "settingsEdit";
			this.Text = "Settings Editor";
			base.Load += new global::System.EventHandler(this.settingsEdit_Load);
			this.statusArea.ResumeLayout(false);
			this.statusArea.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((global::System.ComponentModel.ISupportInitialize)this.dgDataEditor).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		// Token: 0x040008C8 RID: 2248
		private global::System.ComponentModel.IContainer components;

		// Token: 0x040008C9 RID: 2249
		private global::System.Windows.Forms.Label lblStatistic;

		// Token: 0x040008CA RID: 2250
		private global::System.Windows.Forms.CheckedListBox clbColumnsList;

		// Token: 0x040008CB RID: 2251
		private global::System.Windows.Forms.Button btnFilterClear;

		// Token: 0x040008CC RID: 2252
		private global::System.Windows.Forms.CheckBox chPublished;

		// Token: 0x040008CD RID: 2253
		private global::System.Windows.Forms.Label label5;

		// Token: 0x040008CE RID: 2254
		private global::System.Windows.Forms.Button btnFilter;

		// Token: 0x040008CF RID: 2255
		private global::System.Windows.Forms.StatusStrip statusArea;

		// Token: 0x040008D0 RID: 2256
		private global::System.Windows.Forms.ToolStripStatusLabel statusAreaText;

		// Token: 0x040008D1 RID: 2257
		private global::System.Windows.Forms.GroupBox groupBox1;

		// Token: 0x040008D2 RID: 2258
		private global::System.Windows.Forms.Label label6;

		// Token: 0x040008D3 RID: 2259
		private global::System.Windows.Forms.TextBox tbSearch_Name;

		// Token: 0x040008D4 RID: 2260
		private global::System.Windows.Forms.Label label3;

		// Token: 0x040008D5 RID: 2261
		private global::System.Windows.Forms.ComboBox ddItemsLimit;

		// Token: 0x040008D6 RID: 2262
		private global::System.Windows.Forms.Label label2;

		// Token: 0x040008D7 RID: 2263
		private global::System.Windows.Forms.ComboBox ddStoresConn;

		// Token: 0x040008D8 RID: 2264
		private global::System.Windows.Forms.Label label1;

		// Token: 0x040008D9 RID: 2265
		private global::System.Windows.Forms.DataGridView dgDataEditor;

		// Token: 0x040008DA RID: 2266
		private global::System.Windows.Forms.Button btnClose;

		// Token: 0x040008DB RID: 2267
		private global::System.Windows.Forms.Button btnSave;
	}
}
