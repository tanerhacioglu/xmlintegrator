﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Reflection;
using System.Security.Principal;
using System.Threading;
using System.Windows.Forms;
using NopTalkCore;

namespace NopTalk
{

	static class Program
	{
		/// <summary>
		///  The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			WindowsIdentity current = WindowsIdentity.GetCurrent();
			WindowsPrincipal windowsPrincipal = new WindowsPrincipal(current);
			ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
			if (!windowsPrincipal.IsInRole(WindowsBuiltInRole.Administrator) && false)
			{
				Program.processInfo = new ProcessStartInfo(Assembly.GetExecutingAssembly().CodeBase);
				Program.processInfo.UseShellExecute = true;
				Program.processInfo.Verb = "runas";
				try
				{
					Process.Start(Program.processInfo);
				}
				catch (Exception)
				{
					MessageBox.Show("Sorry, but I don't seem to be able to start this program with administrator rights!");
				}
				Application.Exit();
			}
			else
			{
				Application.EnableVisualStyles();
				Application.SetCompatibleTextRenderingDefault(false);
				AppDomain.CurrentDomain.ProcessExit += Program.OnProcessExit;
				Application.ThreadException += Program.Application_ThreadException;
				Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
				LicenseManager.Validate(typeof(Program));
				
				AppDomain.CurrentDomain.UnhandledException += Program.CurrentDomain_UnhandledException;
				Application.Run(new mainForm());
			}
		}

		private static async void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
		{
			
			MessageBox.Show(e.Exception.Message, "Unhandled Thread Exception");
		}

		private static async void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			
			MessageBox.Show((e.ExceptionObject as Exception).Message, "Unhandled UI Exception");
		}

		private static async void OnProcessExit(object sender, EventArgs e)
		{
			TaskManager taskManager = new TaskManager();
			taskManager.StopTasks(null);
			Console.WriteLine("Application exit");
			
		}

		private static ProcessStartInfo processInfo;
	}
}
