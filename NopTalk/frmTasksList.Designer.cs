﻿namespace NopTalk
{
	// Token: 0x02000039 RID: 57
	public partial class frmTasksList : global::System.Windows.Forms.Form
	{
		// Token: 0x06000205 RID: 517 RVA: 0x000030E7 File Offset: 0x000012E7
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000206 RID: 518 RVA: 0x0003388C File Offset: 0x00031A8C
		private void InitializeComponent()
		{
			global::System.ComponentModel.ComponentResourceManager componentResourceManager = new global::System.ComponentModel.ComponentResourceManager(typeof(global::NopTalk.frmTasksList));
			this.btnDelete = new global::System.Windows.Forms.Button();
			this.btnEdit = new global::System.Windows.Forms.Button();
			this.btnNew = new global::System.Windows.Forms.Button();
			this.dgTasks = new global::System.Windows.Forms.DataGridView();
			this.btnRunAllTasks = new global::System.Windows.Forms.Button();
			this.btnRunTask = new global::System.Windows.Forms.Button();
			this.btnTaskService = new global::System.Windows.Forms.Button();
			this.groupBox1 = new global::System.Windows.Forms.GroupBox();
			this.label1 = new global::System.Windows.Forms.Label();
			this.chAutoRefresh = new global::System.Windows.Forms.CheckBox();
			this.btnCancelTask = new global::System.Windows.Forms.Button();
			((global::System.ComponentModel.ISupportInitialize)this.dgTasks).BeginInit();
			this.groupBox1.SuspendLayout();
			base.SuspendLayout();
			this.btnDelete.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnDelete.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnDelete.Location = new global::System.Drawing.Point(724, 351);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new global::System.Drawing.Size(75, 23);
			this.btnDelete.TabIndex = 11;
			this.btnDelete.Text = "Delete";
			this.btnDelete.UseVisualStyleBackColor = true;
			this.btnDelete.Click += new global::System.EventHandler(this.btnDelete_Click);
			this.btnEdit.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnEdit.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnEdit.Location = new global::System.Drawing.Point(643, 351);
			this.btnEdit.Name = "btnEdit";
			this.btnEdit.Size = new global::System.Drawing.Size(75, 23);
			this.btnEdit.TabIndex = 10;
			this.btnEdit.Text = "Edit";
			this.btnEdit.UseVisualStyleBackColor = true;
			this.btnEdit.Click += new global::System.EventHandler(this.btnEdit_Click);
			this.btnNew.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnNew.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnNew.Location = new global::System.Drawing.Point(562, 351);
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new global::System.Drawing.Size(75, 23);
			this.btnNew.TabIndex = 9;
			this.btnNew.Text = "Insert";
			this.btnNew.UseVisualStyleBackColor = true;
			this.btnNew.Click += new global::System.EventHandler(this.btnNew_Click);
			this.dgTasks.AllowUserToAddRows = false;
			this.dgTasks.AllowUserToDeleteRows = false;
			this.dgTasks.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.dgTasks.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgTasks.Location = new global::System.Drawing.Point(12, 63);
			this.dgTasks.MultiSelect = false;
			this.dgTasks.Name = "dgTasks";
			this.dgTasks.SelectionMode = global::System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgTasks.Size = new global::System.Drawing.Size(787, 276);
			this.dgTasks.TabIndex = 8;
			this.dgTasks.CellDoubleClick += new global::System.Windows.Forms.DataGridViewCellEventHandler(this.dgTasks_CellDoubleClick);
			this.btnRunAllTasks.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnRunAllTasks.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnRunAllTasks.Location = new global::System.Drawing.Point(251, 351);
			this.btnRunAllTasks.Name = "btnRunAllTasks";
			this.btnRunAllTasks.Size = new global::System.Drawing.Size(119, 23);
			this.btnRunAllTasks.TabIndex = 12;
			this.btnRunAllTasks.Text = "Run now all tasks";
			this.btnRunAllTasks.UseVisualStyleBackColor = true;
			this.btnRunAllTasks.Click += new global::System.EventHandler(this.btnRunAllTasks_Click);
			this.btnRunTask.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnRunTask.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnRunTask.Location = new global::System.Drawing.Point(469, 351);
			this.btnRunTask.Name = "btnRunTask";
			this.btnRunTask.Size = new global::System.Drawing.Size(87, 23);
			this.btnRunTask.TabIndex = 13;
			this.btnRunTask.Text = "Run now task";
			this.btnRunTask.UseVisualStyleBackColor = true;
			this.btnRunTask.Click += new global::System.EventHandler(this.btnRunTask_Click);
			this.btnTaskService.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnTaskService.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnTaskService.Location = new global::System.Drawing.Point(126, 351);
			this.btnTaskService.Name = "btnTaskService";
			this.btnTaskService.Size = new global::System.Drawing.Size(119, 23);
			this.btnTaskService.TabIndex = 14;
			this.btnTaskService.Text = "Start task scheduler";
			this.btnTaskService.UseVisualStyleBackColor = true;
			this.btnTaskService.Click += new global::System.EventHandler(this.btnTaskService_Click);
			this.groupBox1.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Left | global::System.Windows.Forms.AnchorStyles.Right);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.chAutoRefresh);
			this.groupBox1.Location = new global::System.Drawing.Point(13, 13);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new global::System.Drawing.Size(786, 44);
			this.groupBox1.TabIndex = 15;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Filter";
			this.label1.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Right);
			this.label1.AutoSize = true;
			this.label1.Location = new global::System.Drawing.Point(677, 16);
			this.label1.Name = "label1";
			this.label1.Size = new global::System.Drawing.Size(79, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Auto refresh list";
			this.chAutoRefresh.Anchor = (global::System.Windows.Forms.AnchorStyles.Top | global::System.Windows.Forms.AnchorStyles.Right);
			this.chAutoRefresh.AutoSize = true;
			this.chAutoRefresh.Checked = true;
			this.chAutoRefresh.CheckState = global::System.Windows.Forms.CheckState.Checked;
			this.chAutoRefresh.Location = new global::System.Drawing.Point(759, 16);
			this.chAutoRefresh.Name = "chAutoRefresh";
			this.chAutoRefresh.Size = new global::System.Drawing.Size(15, 14);
			this.chAutoRefresh.TabIndex = 0;
			this.chAutoRefresh.UseVisualStyleBackColor = true;
			this.btnCancelTask.Anchor = (global::System.Windows.Forms.AnchorStyles.Bottom | global::System.Windows.Forms.AnchorStyles.Right);
			this.btnCancelTask.FlatStyle = global::System.Windows.Forms.FlatStyle.Flat;
			this.btnCancelTask.Location = new global::System.Drawing.Point(376, 351);
			this.btnCancelTask.Name = "btnCancelTask";
			this.btnCancelTask.Size = new global::System.Drawing.Size(87, 23);
			this.btnCancelTask.TabIndex = 16;
			this.btnCancelTask.Text = "Cancel task";
			this.btnCancelTask.UseVisualStyleBackColor = true;
			this.btnCancelTask.Click += new global::System.EventHandler(this.btnCancelTask_Click);
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(811, 380);
			base.Controls.Add(this.btnCancelTask);
			base.Controls.Add(this.groupBox1);
			base.Controls.Add(this.btnTaskService);
			base.Controls.Add(this.btnRunTask);
			base.Controls.Add(this.btnRunAllTasks);
			base.Controls.Add(this.btnDelete);
			base.Controls.Add(this.btnEdit);
			base.Controls.Add(this.btnNew);
			base.Controls.Add(this.dgTasks);
			base.Icon = (global::System.Drawing.Icon)componentResourceManager.GetObject("$this.Icon");
			base.Name = "frmTasksList";
			this.Text = "Sources import tasks list";
			base.FormClosing += new global::System.Windows.Forms.FormClosingEventHandler(this.frmTasksList_FormClosing);
			base.Load += new global::System.EventHandler(this.frmTasksList_Load);
			((global::System.ComponentModel.ISupportInitialize)this.dgTasks).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			base.ResumeLayout(false);
		}

		// Token: 0x040003EA RID: 1002
		private global::System.ComponentModel.IContainer components;

		// Token: 0x040003EB RID: 1003
		private global::System.Windows.Forms.Button btnDelete;

		// Token: 0x040003EC RID: 1004
		private global::System.Windows.Forms.Button btnEdit;

		// Token: 0x040003ED RID: 1005
		private global::System.Windows.Forms.Button btnNew;

		// Token: 0x040003EE RID: 1006
		private global::System.Windows.Forms.DataGridView dgTasks;

		// Token: 0x040003EF RID: 1007
		private global::System.Windows.Forms.Button btnRunAllTasks;

		// Token: 0x040003F0 RID: 1008
		private global::System.Windows.Forms.Button btnRunTask;

		// Token: 0x040003F1 RID: 1009
		private global::System.Windows.Forms.Button btnTaskService;

		// Token: 0x040003F2 RID: 1010
		private global::System.Windows.Forms.GroupBox groupBox1;

		// Token: 0x040003F3 RID: 1011
		private global::System.Windows.Forms.Label label1;

		// Token: 0x040003F4 RID: 1012
		private global::System.Windows.Forms.CheckBox chAutoRefresh;

		// Token: 0x040003F5 RID: 1013
		private global::System.Windows.Forms.Button btnCancelTask;
	}
}
