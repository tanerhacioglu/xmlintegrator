﻿using System;
using System.ComponentModel;
using System.Deployment.Application;
using System.Drawing;
using System.Windows.Forms;

namespace NopTalk
{
	public partial class appUpdate : Form
	{
		public appUpdate()
		{
			
			this.sizeOfUpdate = 0L;
			this.components = null;
			this.InitializeComponent();
		}

		private void UpdateApplication()
		{
			if (ApplicationDeployment.IsNetworkDeployed)
			{
				ApplicationDeployment currentDeployment = ApplicationDeployment.CurrentDeployment;
				currentDeployment.CheckForUpdateCompleted += this.ad_CheckForUpdateCompleted;
				currentDeployment.CheckForUpdateProgressChanged += this.ad_CheckForUpdateProgressChanged;
				currentDeployment.CheckForUpdateAsync();
			}
		}

		private void ad_CheckForUpdateProgressChanged(object sender, DeploymentProgressChangedEventArgs e)
		{
			this.downloadStatus.Text = string.Format("Downloading: {0}. {1:D}K of {2:D}K downloaded.", this.GetProgressString(e.State), e.BytesCompleted / 1024L, e.BytesTotal / 1024L);
		}

		private string GetProgressString(DeploymentProgressState state)
		{
			string result;
			if (state == DeploymentProgressState.DownloadingApplicationFiles)
			{
				result = "application files";
			}
			else if (state == DeploymentProgressState.DownloadingApplicationInformation)
			{
				result = "application manifest";
			}
			else
			{
				result = "deployment manifest";
			}
			return result;
		}

		private void ad_CheckForUpdateCompleted(object sender, CheckForUpdateCompletedEventArgs e)
		{
			if (e.Error != null)
			{
				MessageBox.Show("ERROR: Could not retrieve new version of the application. Reason: \n" + e.Error.Message + "\nPlease report this error to the system administrator.");
			}
			else
			{
				if (e.Cancelled)
				{
					MessageBox.Show("The update was cancelled.");
				}
				if (e.UpdateAvailable)
				{
					this.sizeOfUpdate = e.UpdateSizeBytes;
					if (!e.IsUpdateRequired)
					{
						DialogResult dialogResult = MessageBox.Show("An update is available. Would you like to update the application now?\n\nEstimated Download Time: ", "Update Available", MessageBoxButtons.OKCancel);
						if (DialogResult.OK == dialogResult)
						{
							this.BeginUpdate();
						}
					}
					else
					{
						MessageBox.Show("A mandatory update is available for your application. We will install the update now, after which we will save all of your in-progress data and restart your application.");
						this.BeginUpdate();
					}
				}
			}
		}

		private void BeginUpdate()
		{
			ApplicationDeployment currentDeployment = ApplicationDeployment.CurrentDeployment;
			currentDeployment.UpdateCompleted += this.ad_UpdateCompleted;
			currentDeployment.UpdateProgressChanged += this.ad_UpdateProgressChanged;
			currentDeployment.UpdateAsync();
		}

		private void ad_UpdateProgressChanged(object sender, DeploymentProgressChangedEventArgs e)
		{
			string text = string.Format("{0:D}K out of {1:D}K downloaded - {2:D}% complete", e.BytesCompleted / 1024L, e.BytesTotal / 1024L, e.ProgressPercentage);
			this.downloadStatus.Text = text;
		}

		private void ad_UpdateCompleted(object sender, AsyncCompletedEventArgs e)
		{
			if (e.Cancelled)
			{
				MessageBox.Show("The update of the application's latest version was cancelled.");
			}
			else if (e.Error != null)
			{
				MessageBox.Show("ERROR: Could not install the latest version of the application. Reason: \n" + e.Error.Message + "\nPlease report this error to the system administrator.");
			}
			else
			{
				DialogResult dialogResult = MessageBox.Show("The application has been updated. Restart? (If you do not restart now, the new version will not take effect until after you quit and launch the application again.)", "Restart Application", MessageBoxButtons.OKCancel);
				if (DialogResult.OK == dialogResult)
				{
					Application.Restart();
				}
			}
		}

		private void appUpdate_Load(object sender, EventArgs e)
		{
			this.UpdateApplication();
		}

		private long sizeOfUpdate;
	}
}
